<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
// $active_record = TRUE;
$query_builder = TRUE;

// $db['default']['hostname'] = '172.19.18.129';
// $db['default']['username'] = 'root';
// $db['default']['password'] = 'rhythm_123';
// $db['default']['database'] = 'rhythm_final';

// $db['default']['hostname'] = 'rhythmdb.ctzwque3zyis.us-east-1.rds.amazonaws.com';
// $db['default']['username'] = 'root_admin';
// $db['default']['password'] = 'Rhythm_Aw5!!';
// $db['default']['database'] = 'rhythm';

// $db['default']['hostname'] = '10.10.18.98';
// $db['default']['username'] = 'admin';
// $db['default']['password'] = '@dm1n';
// $db['default']['database'] = 'rhythm_live2';
// $db['default']['database'] = 'rhythm_new_qa';
// $db['default']['database'] = 'rhythm_prepod';
// $db['default']['database'] = 'rhythm_new_dev';
// $db['default']['database'] = 'rhythm_dev2';

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'shivaprasad';
$db['default']['password'] = 'Sprasad!123';
$db['default']['database'] = 'rhythm';
//$db['default']['database'] = 'rhythm2';


$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

/* DC Test DB */
$db['dctest']['hostname'] = '172.19.18.98';
$db['dctest']['username'] = 'admin';
$db['dctest']['password'] = '@dm1n';
$db['dctest']['database'] = 'domain_competency';
$db['dctest']['dbdriver'] = 'mysqli';
$db['dctest']['dbprefix'] = '';
$db['dctest']['pconnect'] = TRUE;
$db['dctest']['db_debug'] = TRUE;
$db['dctest']['cache_on'] = FALSE;
$db['dctest']['cachedir'] = '';
$db['dctest']['char_set'] = 'utf8';
$db['dctest']['dbcollat'] = 'utf8_general_ci';
$db['dctest']['swap_pre'] = '';
$db['dctest']['autoinit'] = TRUE;
$db['dctest']['stricton'] = FALSE;

/* ProhanceStats DB */

$db['prohance']['hostname'] = '172.19.18.98';
$db['prohance']['username'] = 'admin';
$db['prohance']['password'] = '@dm1n';
$db['prohance']['database'] = 'prohancestats';
$db['prohance']['dbdriver'] = 'mysqli';
$db['prohance']['dbprefix'] = '';
$db['prohance']['pconnect'] = TRUE;
$db['prohance']['db_debug'] = TRUE;
$db['prohance']['cache_on'] = FALSE;
$db['prohance']['cachedir'] = '';
$db['prohance']['char_set'] = 'utf8';
$db['prohance']['dbcollat'] = 'utf8_general_ci';
$db['prohance']['swap_pre'] = '';
$db['prohance']['autoinit'] = TRUE;
$db['prohance']['stricton'] = FALSE;
/* End of file database.php */
/* Location: ./application/config/database.php */