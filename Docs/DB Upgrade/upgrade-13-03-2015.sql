ALTER TABLE process_billability 
ADD approved_hours INT(5) AFTER approved_count , 
ADD billed_hours FLOAT AFTER approved_hours, 
ADD remarks VARCHAR(100) AFTER billed_hours, 
ADD billable_hours FLOAT AFTER remarks;