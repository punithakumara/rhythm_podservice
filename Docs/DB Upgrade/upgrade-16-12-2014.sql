DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `accuracy_util_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `accuracy_util_month_view` AS (
SELECT DISTINCT
  `process`.`ProcessID`         AS `ProcessID`,
  `process`.`ClientID`          AS `ClientID`,
  `client`.`Client_Name`        AS `Client_Name`,
  `process`.`Name`              AS `name`,
  FORMAT(AVG(`accuracy`.`Accuracy`),2) AS `Accuracy`,
  FORMAT(AVG(`utilization_process`.`Utilization`),2) AS `Utilization`,
  `utilization_process`.`Month` AS `Month`,
  `utilization_process`.`Year`  AS `Year`
FROM (((`process`
     JOIN `client`
       ON ((`client`.`ClientID` = `process`.`ClientID`)))
    JOIN `utilization_process`
      ON ((`utilization_process`.`ProcessID` = `process`.`ProcessID`)))
   JOIN `accuracy`
     ON ((`accuracy`.`ProcessID` = `process`.`ProcessID`)))
GROUP BY `process`.`ProcessID`,`utilization_process`.`Month`,`utilization_process`.`Year`
ORDER BY `utilization_process`.`Month`,`utilization_process`.`Year`,`process`.`ProcessID`)$$

DELIMITER ;

/*--*/

DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `accuracy_util_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `accuracy_util_week_view` AS (
SELECT DISTINCT
  `process`.`ProcessID`               AS `ProcessID`,
  `process`.`ClientID`                AS `ClientID`,
  `client`.`Client_Name`              AS `Client_Name`,
  `process`.`Name`                    AS `name`,
  FORMAT(AVG(`accuracy`.`Accuracy`),2) AS `Accuracy`,
  FORMAT(AVG(`utilization_weekly_process`.`Utilization`),2) AS `Utilization`,
  `utilization_weekly_process`.`Week` AS `Week`,
  `utilization_weekly_process`.`Year` AS `Year`
FROM (((`process`
     JOIN `client`
       ON ((`client`.`ClientID` = `process`.`ClientID`)))
    JOIN `utilization_weekly_process`
      ON ((`utilization_weekly_process`.`ProcessID` = `process`.`ProcessID`)))
   JOIN `accuracy`
     ON ((`accuracy`.`ProcessID` = `process`.`ProcessID`)))
GROUP BY `process`.`ProcessID`,`utilization_weekly_process`.`Week`,`utilization_weekly_process`.`Year`
ORDER BY `utilization_weekly_process`.`Week`,`utilization_weekly_process`.`Year`,`process`.`ProcessID`)$$

DELIMITER ;

/*--*/

DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `billable_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `billable_view` AS (
SELECT DISTINCT
  `process`.`ProcessID` AS `ProcessID`,
  `process`.`ClientID`  AS `ClientID`,
  WEEK(`employ_billability`.`StartDate`,0) AS `Week`,
  YEAR(`employ_billability`.`StartDate`) AS `Year`,
  COUNT(`employ_billability`.`EmployID`) AS `Billable`
FROM ((`process`
    JOIN `client`
      ON ((`client`.`ClientID` = `process`.`ClientID`)))
   JOIN `employ_billability`
     ON ((`employ_billability`.`ProcessID` = `process`.`ProcessID`)))
WHERE (`process`.`ProcessStatus` = 0)
GROUP BY `process`.`ProcessID`
ORDER BY YEAR(`employ_billability`.`StartDate`),WEEK(`employ_billability`.`StartDate`,0),`process`.`ProcessID`)$$

DELIMITER ;

/*--*/
DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `nonbillable_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `nonbillable_view` AS (
SELECT DISTINCT
  `process`.`ProcessID` AS `ProcessID`,
  `process`.`ClientID`  AS `ClientID`,
  WEEK(`employprocess`.`StartDate`,0) AS `Week`,
  YEAR(`employprocess`.`StartDate`) AS `Year`,
  COUNT(`employprocess`.`EmployID`) AS `NonBillable`
FROM ((`process`
    JOIN `client`
      ON ((`client`.`ClientID` = `process`.`ClientID`)))
   JOIN `employprocess`
     ON ((`employprocess`.`ProcessID` = `process`.`ProcessID`)))
WHERE ((`process`.`ProcessStatus` = 0)
       AND (`employprocess`.`Billable` = 0))
GROUP BY `process`.`ProcessID`
ORDER BY YEAR(`employprocess`.`StartDate`),WEEK(`employprocess`.`StartDate`,0),`process`.`ProcessID`)$$

DELIMITER ;

/*--*/
DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `incidentlog_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `incidentlog_month_view` AS (
SELECT DISTINCT
  `process`.`ProcessID` AS `ProcessID`,
  `process`.`ClientID`  AS `ClientID`,
  MONTH(`incidentlog`.`Date`) AS `Month`,
  YEAR(`incidentlog`.`Date`) AS `Year`,
  COUNT((CASE WHEN (`incidentlog`.`LogType` = 'Error') THEN 'Error' END)) AS `Error`,
  COUNT((CASE WHEN (`incidentlog`.`LogType` = 'Appreciation') THEN 'Appreciation' END)) AS `Appreciation`,
  COUNT((CASE WHEN (`incidentlog`.`LogType` = 'Escalation') THEN 'Escalation' END)) AS `Escalation`
FROM ((`process`
    JOIN `client`
      ON ((`client`.`ClientID` = `process`.`ClientID`)))
   JOIN `incidentlog`
     ON ((`incidentlog`.`ProcessID` = `process`.`ProcessID`)))
WHERE (`process`.`ProcessStatus` = 0)
GROUP BY `process`.`ProcessID`,YEAR(`incidentlog`.`Date`),MONTH(`incidentlog`.`Date`)
ORDER BY YEAR(`incidentlog`.`Date`),MONTH(`incidentlog`.`Date`),`process`.`ProcessID`)$$

DELIMITER ;

/*--*/
DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `incidentlog_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `incidentlog_week_view` AS (
SELECT DISTINCT
  `process`.`ProcessID` AS `ProcessID`,
  `process`.`ClientID`  AS `ClientID`,
  WEEK(`incidentlog`.`Date`,0) AS `Week`,
  YEAR(`incidentlog`.`Date`) AS `Year`,
  COUNT((CASE WHEN (`incidentlog`.`LogType` = 'Error') THEN 'Error' END)) AS `Error`,
  COUNT((CASE WHEN (`incidentlog`.`LogType` = 'Appreciation') THEN 'Appreciation' END)) AS `Appreciation`,
  COUNT((CASE WHEN (`incidentlog`.`LogType` = 'Escalation') THEN 'Escalation' END)) AS `Escalation`
FROM ((`process`
    JOIN `client`
      ON ((`client`.`ClientID` = `process`.`ClientID`)))
   JOIN `incidentlog`
     ON ((`incidentlog`.`ProcessID` = `process`.`ProcessID`)))
WHERE (`process`.`ProcessStatus` = 0)
GROUP BY `process`.`ProcessID`,YEAR(`incidentlog`.`Date`),WEEK(`incidentlog`.`Date`,0)
ORDER BY YEAR(`incidentlog`.`Date`),WEEK(`incidentlog`.`Date`,0),`process`.`ProcessID`)$$

DELIMITER ;

/*--*/

DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `reports_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `reports_month_view` AS (
SELECT
  `tablea`.`ProcessID`   AS `ProcessID`,
  `tablea`.`ClientID`    AS `ClientID`,
  `tablea`.`Client_Name` AS `Client_Name`,
  `tablea`.`name`        AS `Process_Name`,
  `tablea`.`Month`       AS `Month`,
  `tablea`.`Year`        AS `Year`,
  `tablea`.`Accuracy`    AS `Accuracy`,
  `tablea`.`Utilization` AS `Utilization`,
  IF((`tableb`.`Error` IS NOT NULL),`tableb`.`Error`,0) AS `Error`,
  IF((`tableb`.`Appreciation` IS NOT NULL),`tableb`.`Appreciation`,0) AS `Appreciation`,
  IF((`tableb`.`Escalation` IS NOT NULL),`tableb`.`Escalation`,0) AS `Escalation`,
  IF((`tablec`.`Billable` IS NOT NULL),`tablec`.`Billable`,0) AS `Billable`,
  IF((`tabled`.`NonBillable` IS NOT NULL),`tabled`.`NonBillable`,0) AS `NonBillable`
FROM (((`accuracy_util_month_view` `tablea`
     LEFT JOIN `incidentlog_month_view` `tableb`
       ON (((`tablea`.`ProcessID` = `tableb`.`ProcessID`)
            AND (`tablea`.`Month` = `tableb`.`Month`)
            AND (`tablea`.`Year` = `tableb`.`Year`))))
    LEFT JOIN `billable_view` `tablec`
      ON ((`tablea`.`ProcessID` = `tablec`.`ProcessID`)))
   LEFT JOIN `nonbillable_view` `tabled`
     ON ((`tablea`.`ProcessID` = `tabled`.`ProcessID`)))
ORDER BY `tablea`.`Year`,`tablea`.`Month`,`tablea`.`ProcessID`)$$

DELIMITER ;

/*--*/

DELIMITER $$

USE `rhythm_final`$$

DROP VIEW IF EXISTS `reports_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `reports_week_view` AS (
SELECT
  `tablea`.`ProcessID`   AS `ProcessID`,
  `tablea`.`ClientID`    AS `ClientID`,
  `tablea`.`Client_Name` AS `Client_Name`,
  `tablea`.`name`        AS `Process_Name`,
  `tablea`.`Week`        AS `Week`,
  `tablea`.`Year`        AS `Year`,
  `tablea`.`Accuracy`    AS `Accuracy`,
  `tablea`.`Utilization` AS `Utilization`,
  IF((`tableb`.`Error` IS NOT NULL),`tableb`.`Error`,0) AS `Error`,
  IF((`tableb`.`Appreciation` IS NOT NULL),`tableb`.`Appreciation`,0) AS `Appreciation`,
  IF((`tableb`.`Escalation` IS NOT NULL),`tableb`.`Escalation`,0) AS `Escalation`,
  IF((`tablec`.`Billable` IS NOT NULL),`tablec`.`Billable`,0) AS `Billable`,
  IF((`tabled`.`NonBillable` IS NOT NULL),`tabled`.`NonBillable`,0) AS `NonBillable`
FROM (((`accuracy_util_week_view` `tablea`
     LEFT JOIN `incidentlog_week_view` `tableb`
       ON (((`tablea`.`ProcessID` = `tableb`.`ProcessID`)
            AND (`tablea`.`Week` = `tableb`.`Week`)
            AND (`tablea`.`Year` = `tableb`.`Year`))))
    LEFT JOIN `billable_view` `tablec`
      ON ((`tablea`.`ProcessID` = `tablec`.`ProcessID`)))
   LEFT JOIN `nonbillable_view` `tabled`
     ON ((`tablea`.`ProcessID` = `tabled`.`ProcessID`)))
ORDER BY `tablea`.`Year`,`tablea`.`Week`,`tablea`.`ProcessID`)$$

DELIMITER ;

