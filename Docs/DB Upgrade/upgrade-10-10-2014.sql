ALTER TABLE meeting_minutes 
ADD COLUMN ParticipantType VARCHAR(35) NULL AFTER EmployID, 
ADD COLUMN SubjectLine VARCHAR(255) NULL AFTER ParticipantType;