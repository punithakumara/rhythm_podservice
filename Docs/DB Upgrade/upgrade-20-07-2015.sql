Query for creating `process_billablenonbillable_cron` table
---------------------------------------------------

CREATE TABLE `process_billablenonbillable_cron` (
  `ProcessID` int(11) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  `Billable` int(11) DEFAULT NULL,
  `NonBillable` int(11) DEFAULT NULL,
  `Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


Reports: Billable & Nonbillable for Monthly query
---------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `billable_nonbillable_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `billable_nonbillable_month_view` AS 
SELECT
  `process_billablenonbillable_cron`.`ProcessID` AS `ProcessID`,
  MONTH(MAX(`process_billablenonbillable_cron`.`Date`)) AS `Month`,
  YEAR(`process_billablenonbillable_cron`.`Date`) AS `YEAR`,
  SUBSTRING_INDEX(GROUP_CONCAT(`process_billablenonbillable_cron`.`Billable` SEPARATOR ','),',',-(1)) AS `Billable`,
  SUBSTRING_INDEX(GROUP_CONCAT(`process_billablenonbillable_cron`.`NonBillable` SEPARATOR ','),',',-(1)) AS `NonBillable`
FROM `process_billablenonbillable_cron`
GROUP BY `process_billablenonbillable_cron`.`ProcessID`,MONTH(`process_billablenonbillable_cron`.`Date`),YEAR(`process_billablenonbillable_cron`.`Date`)$$

DELIMITER ;


Reports: Billable & Nonbillable for Weekly query
---------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `billable_nonbillable_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `billable_nonbillable_week_view` AS 
SELECT
  `process_billablenonbillable_cron`.`ProcessID` AS `ProcessID`,
  WEEK(MAX(`process_billablenonbillable_cron`.`Date`),0) AS `Week`,
  MONTH(`process_billablenonbillable_cron`.`Date`) AS `Month`,
  YEAR(`process_billablenonbillable_cron`.`Date`) AS `YEAR`,
  SUBSTRING_INDEX(GROUP_CONCAT(`process_billablenonbillable_cron`.`Billable` SEPARATOR ','),',',-(1)) AS `Billable`,
  SUBSTRING_INDEX(GROUP_CONCAT(`process_billablenonbillable_cron`.`NonBillable` SEPARATOR ','),',',-(1)) AS `NonBillable`
FROM `process_billablenonbillable_cron`
GROUP BY `process_billablenonbillable_cron`.`ProcessID`,WEEK(`process_billablenonbillable_cron`.`Date`,0),YEAR(`process_billablenonbillable_cron`.`Date`)$$

DELIMITER ;
