-- set the database
USE rhythm_local;


SET FOREIGN_KEY_CHECKS = 0;


-- Step 1. Create Grades in `employprocess` table

ALTER TABLE `employprocess` ADD COLUMN `Grades` INT(11) NULL AFTER `Primary_Lead_ID`; 



-- Step 2. Combine all three tables data 

INSERT INTO employprocess(processid,employid,shiftid,billable,primaryprocess,billablepercentage,billabletype,startdate,enddate,primary_lead_id)
SELECT p.processid,p.employid,p.shiftid,p.billable,p.primaryprocess,p.billablepercentage,p.billabletype,p.startdate,p.enddate,p.primary_lead_id 
FROM processlead p

INSERT INTO employprocess(processid,employid,shiftid,billable,primaryprocess,billablepercentage,billabletype,startdate,enddate,primary_lead_id)
SELECT p.processid,p.employid,p.shiftid,p.billable,p.primaryprocess,p.billablepercentage,p.billabletype,p.startdate,p.enddate,p.primary_lead_id 
FROM processmanager p



-- Step 3. Update Grades in employprocess table

UPDATE employprocess ep SET Grades=(SELECT grades FROM employ e, designation d 
WHERE  e.designationid = d.designationid AND e.employid = ep.employid)


SET FOREIGN_KEY_CHECKS = 1;


-- Step 4. Delete processlead and processmanager table

DROP TABLE IF EXISTS processlead, processmanager



-- Step 5. Alter views `allempprocess` and `employ_process_view`



-- Step 6. custom report related db changes 
-- Excecute queries in Rhythm/Docs/DB Upgrade/upgrade-11-03-2016.sql, Rhythm/Docs/DB Upgrade/upgrade-10-03-2016.sql



-- Step 7. In attributes list table status column has to create
ALTER TABLE `attributes_list` ADD COLUMN `Status` TINYINT(1) DEFAULT 1 NULL COMMENT '1-Active,0-Disabled' AFTER `Value`; 




-- Step 8. Update client contact db changes
-- Excecute queries in Rhythm/Docs/DB Upgrade/upgrade_client_contact-30-03-2016.sql


