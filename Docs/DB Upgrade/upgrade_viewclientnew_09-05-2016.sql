DELIMITER $$


DROP VIEW IF EXISTS `view_client_new`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_client_new` AS (
SELECT
  `temp`.`ClientID`      AS `ClientID`,
  `temp`.`Client_Name`   AS `Client_Name`,
  `temp`.`ProcessID`     AS `ProcessID`,
  `temp`.`resourceCount` AS `resourceCount`,
  `temp`.`ForcastNRR`    AS `ForcastNRR`,
  `temp`.`ForcastRDR`    AS `ForcastRDR`,
  `temp`.`ClientTier`    AS `ClientTier`,
  `temp`.`industryType`  AS `industryType`,
  `temp`.`accManager`    AS `accManager`,
  `temp`.`STATUS`        AS `STATUS`,
  `temp`.`Created_Date`  AS `Created_Date`,
  `temp`.`Country`       AS `Country`,
  `temp`.`Logo`          AS `Logo`,
  `temp`.`shifts`        AS `shifts`,
  `temp`.`followAccount` AS `followAccount`,
  `temp`.`clientPartner` AS `clientPartner`,
   `temp`.`inactiveDate` AS `inactiveDate`,
  GROUP_CONCAT(DISTINCT `technology`.`Name` SEPARATOR ',') AS `technologies`
FROM ((`view_client_dossier` `temp`
    LEFT JOIN `processtechnology`
      ON ((`temp`.`ProcessID` = `processtechnology`.`ProcessID`)))
   LEFT JOIN `technology`
     ON ((`technology`.`TechnologyID` = `processtechnology`.`TechnologyID`)))
GROUP BY `temp`.`ClientID`,`temp`.`ProcessID`)$$

DELIMITER ;