/*
SQLyog Community v10.00 Beta1
MySQL - 5.5.24 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `employ_reports` (
	`ReportID` int ,
	`EmpID` int ,
	`ReportName` varchar ,
	`ReportColumns` text (196605),
	`CreatedDate` datetime ,
	`ModifiedDate` datetime ,
	`Default` tinyint ,
	`ReportImagePath` varchar 
); 
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('1',NULL,'Buffer Report','Vertical,Process,ApprovedFTE,Billable,NonBillable,NonBillableBuffer,Client','2016-02-29 19:31:58','2016-03-11 10:08:09','1','/resources/images/menu/buffer.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('2',NULL,'Consolidated Utilization Report','Vertical,Client,Process,Utilization,NoOfTasks,NoOfHours,ExpHours,NoOfResourcesWorked,BillableResources,NoOfDays,Month,Year','2016-02-29 18:29:17','2016-02-29 18:29:20','1','/resources/images/menu/con_utilization.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('3',NULL,'Active Client-Process Report','Client,Type,Status,Billable,ApprovedFTE,ClientSince,ProcessOwner,AssociateManager,AccountManager,Vertical,Process,ClientStatus','2016-02-29 18:31:19','2016-03-10 13:54:08','1','/resources/images/menu/client.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('4',NULL,'Night Shift Report','EmployID,EmployName,Shift,Vertical,Process','2016-02-29 18:33:29','2016-02-29 18:33:31','1','/resources/images/menu/nightshiftEmployeeReport.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('5',NULL,'Ops Dashboard','EmployID,EmployName,Location,Shift,Vertical,Client,Process,Grade,Designation,ReportingTeamLeader,ReportingManager,VerticalManager,BillableNonBillable,NoticePeriod\r\n','2016-02-29 18:35:19','2016-02-29 18:35:21','1','/resources/images/tree/hierarchy.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('6',NULL,'Escalation Report','Vertical, Client, Process, Type, EmployName, Escalation, Date','2016-03-10 16:59:46','2016-03-10 17:00:05','1','/resources/images/tree/Escalation.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('7',NULL,'Appreciation Report','Vertical, Client, Process, Appreciation, AppreciatedBy, Date','2016-03-10 16:59:52','2016-03-10 17:00:09','1','/resources/images/tree/Appreciation.png');
insert into `employ_reports` (`ReportID`, `EmpID`, `ReportName`, `ReportColumns`, `CreatedDate`, `ModifiedDate`, `Default`, `ReportImagePath`) values('8',NULL,'Error Report','Vertical, Client, Process, Error, EmployName, Date, TeamLeader','2016-03-10 16:59:56','2016-03-10 17:00:11','1','/resources/images/error.png');

UPDATE `employ_reports` SET `Default` = '2' WHERE `ReportID` = '4'