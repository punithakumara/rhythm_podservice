ALTER TABLE `client` ADD COLUMN `clientPartner` INT(11) NULL AFTER `accManager`; 
ALTER TABLE `client` ADD COLUMN `clientPartner` INT(11) NULL AFTER `accManager`;


ALTER TABLE `client_contact` ADD COLUMN `status` SMALLINT(2) NULL COMMENT '0 for active and 1 for inactive' AFTER `Contact_Mobile`; 



ALTER TABLE `client_contact` ADD COLUMN `Contact_Vertical` INT(11) NULL COMMENT 'client contact vertical' 
AFTER `Contact_Email`, ADD COLUMN `Contact_Process` INT(11) NULL COMMENT 'client contact process' AFTER `Contact_Vertical`; 

ALTER TABLE `client` CHANGE `accManager` `accManager` INT(11) NULL; 

ALTER TABLE `employonboarding` CHANGE `Training_Status` `Training_Status` TINYINT(1) DEFAULT 0 NULL COMMENT '1 is for new joinees, 2 available for transition', CHANGE `Training_Complete` `Training_Complete` TINYINT(1) DEFAULT 0 NULL COMMENT '0 is for undergoing, 1 for completed';
UPDATE `client_contact` client_contact SET STATUS = 0;