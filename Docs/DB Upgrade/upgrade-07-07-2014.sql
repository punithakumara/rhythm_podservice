CREATE TABLE `employ_billability`( 
	`BillabilityID` INT NOT NULL AUTO_INCREMENT, 
	`ProcessID` INT, 
	`EmployID` INT, 
	`StartDate` DATE,
	`EndDate` DATE, 
	PRIMARY KEY (`BillabilityID`), 
	FOREIGN KEY (`ProcessID`) REFERENCES `process`(`ProcessID`) ON UPDATE NO ACTION ON DELETE NO ACTION, 
	FOREIGN KEY (`EmployID`) REFERENCES `employ`(`EmployID`) ON UPDATE NO ACTION ON DELETE NO ACTION );
	
	
ALTER TABLE `notification` ADD COLUMN `ReadDate` DATETIME NULL AFTER `Read`; 
ALTER TABLE `process` ADD COLUMN `Approver` INT NULL AFTER `CreatedDate`, ADD FOREIGN KEY (`Approver`) REFERENCES `employ`(`EmployID`) ON UPDATE NO ACTION ON DELETE NO ACTION; 