CREATE TABLE `emp_shift` (
  `EmployID` int(11) NOT NULL,
  `ProcessID` int(11) NOT NULL,
  `ClientID` int(11) NOT NULL,
  `VerticalID` int(11) NOT NULL,
  `FromDate` date NOT NULL,
  `ToDate` date NOT NULL,
  `ShiftID` int(11) NOT NULL,
  KEY `emp_shift_ibfk_1` (`EmployID`),
  KEY `emp_shift_ibfk_2` (`ProcessID`),
  KEY `emp_shift_ibfk_3` (`ClientID`),
  KEY `emp_shift_ibfk_4` (`VerticalID`),
  KEY `emp_shift_ibfk_5` (`ShiftID`),
  CONSTRAINT `emp_shift_ibfk_1` FOREIGN KEY (`EmployID`) REFERENCES `employ` (`EmployID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `emp_shift_ibfk_2` FOREIGN KEY (`ProcessID`) REFERENCES `process` (`ProcessID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `emp_shift_ibfk_3` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `emp_shift_ibfk_4` FOREIGN KEY (`VerticalID`) REFERENCES `vertical` (`VerticalID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `emp_shift_ibfk_5` FOREIGN KEY (`ShiftID`) REFERENCES `shift` (`ShiftID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)