SELECT `CompanyEmployID` AS EmployeeID, `FirstName`, `LastName`, `location`.`Name` AS Location, `vertical`.Name AS Vertical, `designation`.Name AS Designation, `designation`.`grades` AS Grade, DATE_FORMAT(`DateOfJoin`, '%d-%m-%Y') AS DateOfJoin, DATE_FORMAT(`RelieveDate`, '%d-%m-%Y') AS DateOfRelieve FROM `employ`
JOIN `designation` ON (`employ`.`DesignationID` = `designation`.`DesignationID`)
LEFT JOIN `location` ON (`employ`.`LocationID` = `location`. `LocationID`)
LEFT JOIN `vertical` ON (`employ`.`AssignedVertical` = `vertical`.`VerticalID`)
ORDER BY FirstName;



SELECT `employ`.`CompanyEmployID` AS EmployeeID, `employ`.`FirstName`, `employ`.`LastName`, `employ`.`Email`, `location`.`Name` AS Location, `vertical`.Name AS Vertical, `designation`.Name AS Designation, `designation`.`grades` AS Grade FROM `employ`
JOIN `designation` ON (`employ`.`DesignationID` = `designation`.`DesignationID`)
LEFT JOIN `location` ON (`employ`.`LocationID` = `location`. `LocationID`)
LEFT JOIN `vertical` ON (`employ`.`AssignedVertical` = `vertical`.`VerticalID`)
WHERE employ.EmployID IN (SELECT DISTINCT EmployID FROM processlead WHERE processid IN (SELECT ProcessID FROM Processmanager WHERE Employid = 40)
UNION
SELECT DISTINCT EmployID FROM employprocess WHERE processid IN (SELECT ProcessID FROM Processmanager WHERE Employid = 40))
AND employ.`RelieveFlag` = 0
ORDER BY FirstName