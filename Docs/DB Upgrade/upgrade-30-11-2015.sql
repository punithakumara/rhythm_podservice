 ALTER TABLE client ADD COLUMN ClientTier TINYINT(1) DEFAULT 3 NULL AFTER followAccount
 
 
 DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `view_client_dossier`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_client_dossier` AS 
SELECT
  `client`.`ClientID`      AS `ClientID`,
  `client`.`Country`       AS `Country`,
  `client`.`Client_Name`   AS `Client_Name`,
  `process`.`ProcessID`    AS `ProcessID`,
  `process`.`ForcastNRR`   AS `ForcastNRR`,
  `process`.`ForcastRDR`   AS `ForcastRDR`,
  `client`.`ClientTier`    AS `ClientTier`,
  `process`.`HeadCount`    AS `resourceCount`,
  `client`.`industryType`  AS `industryType`,
  `client`.`accManager`    AS `accManager`,
  `client`.`status`        AS `STATUS`,
  `client`.`followAccount` AS `followAccount`,
  `client`.`clientPartner` AS `clientPartner`,
  DATE_FORMAT(`client`.`Created_Date`,'%d-%m-%Y') AS `Created_Date`,
  `client`.`Logo`          AS `Logo`,
  GROUP_CONCAT(DISTINCT `shift`.`ShiftCode` SEPARATOR ',') AS `shifts`
FROM ((((`client`
      LEFT JOIN `process`
        ON ((`process`.`ClientID` = `client`.`ClientID`)))
     LEFT JOIN `employ_process_view`
       ON (((`employ_process_view`.`ProcessID` = `process`.`ProcessID`)
            AND (`process`.`ProcessStatus` = 0)
            AND (`employ_process_view`.`PrimaryProcess` = 1))))
    LEFT JOIN `shift`
      ON ((`shift`.`ShiftID` = `employ_process_view`.`ShiftID`)))
   LEFT JOIN `employ`
     ON ((`employ`.`EmployID` = `employ_process_view`.`EmployID`)))
GROUP BY `client`.`ClientID`,`process`.`ProcessID`$$

DELIMITER ;



DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `view_client_new`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_client_new` AS (
SELECT
  `temp`.`ClientID`      AS `ClientID`,
  `temp`.`Client_Name`   AS `Client_Name`,
  `temp`.`ProcessID`     AS `ProcessID`,
  `temp`.`resourceCount` AS `resourceCount`,
  `temp`.`ForcastNRR`    AS `ForcastNRR`,
  `temp`.`ForcastRDR`    AS `ForcastRDR`,
  `temp`.`ClientTier`    AS `ClientTier`,
  `temp`.`industryType`  AS `industryType`,
  `temp`.`accManager`    AS `accManager`,
  `temp`.`STATUS`        AS `STATUS`,
  `temp`.`Created_Date`  AS `Created_Date`,
  `temp`.`Country`       AS `Country`,
  `temp`.`Logo`          AS `Logo`,
  `temp`.`shifts`        AS `shifts`,
  `temp`.`followAccount` AS `followAccount`,
  `temp`.`clientPartner` AS `clientPartner`,
  GROUP_CONCAT(DISTINCT `technology`.`Name` SEPARATOR ',') AS `technologies`
FROM ((`view_client_dossier` `temp`
    LEFT JOIN `processtechnology`
      ON ((`temp`.`ProcessID` = `processtechnology`.`ProcessID`)))
   LEFT JOIN `technology`
     ON ((`technology`.`TechnologyID` = `processtechnology`.`TechnologyID`)))
GROUP BY `temp`.`ClientID`,`temp`.`ProcessID`)$$

DELIMITER ;