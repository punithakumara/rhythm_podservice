ALTER TABLE `employprocess` ADD COLUMN `Approved` INT DEFAULT 0 NULL COMMENT '0-Approved, 1-Not Approved' AFTER `createdOn`;
UPDATE employprocess SET Approved =1 WHERE processid = 223;
UPDATE employprocess SET Approved =0 WHERE employid IN (SELECT employid FROM employ WHERE trainingneeded =1 AND relieveflag=0);
