DELIMITER $$

DROP VIEW IF EXISTS `view_client`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_client` AS 
SELECT
  `client`.`ClientID`     AS `ClientID`,
  `client`.`Client_Name`  AS `Client_Name`,
  `client`.`industryType` AS `industryType`,
  `client`.`accManager`   AS `accManager`,
  `client`.`status`       AS `status`,
  `client`.`Created_Date` AS `Created_Date`,
  `client`.`Logo`         AS `Logo`,
  `process`.`ProcessID`   AS `ProcessID`,
  COUNT(DISTINCT `employprocess`.`EmployID`) AS `resourceCount`,
  GROUP_CONCAT(DISTINCT `shift`.`ShiftCode` SEPARATOR ',') AS `shifts`,
  GROUP_CONCAT(DISTINCT `technology`.`Name` SEPARATOR ',') AS `technologies`
FROM (((((`process`
       LEFT JOIN `client`
         ON ((`process`.`ClientID` = `client`.`ClientID`)))
      LEFT JOIN `employprocess`
        ON ((`employprocess`.`ProcessID` = `process`.`ProcessID`)))
     LEFT JOIN `shift`
       ON ((`shift`.`ShiftID` = `employprocess`.`ShiftID`)))
    LEFT JOIN `processtechnology`
      ON ((`process`.`ProcessID` = `processtechnology`.`ProcessID`)))
   LEFT JOIN `technology`
     ON ((`technology`.`TechnologyID` = `processtechnology`.`TechnologyID`)))
GROUP BY `client`.`ClientID`,`employprocess`.`ProcessID`$$

DELIMITER ;