ALTER TABLE `client_rdr` ADD COLUMN `CancelReason` VARCHAR(255) NULL AFTER `AddedBy`;
ALTER TABLE `client_nrr` ADD COLUMN `CancelReason` VARCHAR(255) NULL AFTER `AddedBy`;