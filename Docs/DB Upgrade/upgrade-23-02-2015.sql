CREATE TABLE `process_billability` (
  `ProcessID` INT(11) DEFAULT NULL,
  `year` INT(4) DEFAULT NULL,
  `month` CHAR(2) DEFAULT NULL,
  `billable_count` INT(4) DEFAULT NULL,
  `assigned_billable_count` INT(4) DEFAULT NULL,
  `approved_count` INT(4) DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;