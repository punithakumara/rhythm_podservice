CREATE TABLE `finance_reports`( 
  `Finace_Report_ID` INT(11) NOT NULL AUTO_INCREMENT, 
  `ClientID` INT(11), 
  `ProcessID` INT(11), 
  `No_of_Approved_HC` INT(11), 
  `Shift` INT(2), 
  `Assigned_Billable` INT(3), 
  `Assigned_Non_Billable` INT(3), 
  `Type` VARCHAR(20), 
  PRIMARY KEY (`Finace_Report_ID`),
  FOREIGN KEY (`ClientID`) REFERENCES `client`(`ClientID`) ON UPDATE NO ACTION ON DELETE NO ACTION, 
  FOREIGN KEY (`ProcessID`) REFERENCES `process`(`ProcessID`) ON UPDATE NO ACTION ON DELETE NO ACTION
  );
