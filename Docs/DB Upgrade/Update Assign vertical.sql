/* Update assign vertical to employ table */
UPDATE employ emp
JOIN (SELECT emppro.EmployID,pro.VerticalID FROM `verticalmanager` emppro
JOIN PROCESS pro ON emppro.VerticalID = pro.VerticalID 
GROUP BY emppro.EmployID) AS temp ON emp.EmployID = temp.EmployID  
SET emp.AssignedVertical = temp.VerticalID;

UPDATE employ emp
JOIN (SELECT emppro.EmployID,pro.VerticalID FROM `processmanager` emppro
JOIN PROCESS pro ON emppro.ProcessID = pro.ProcessID 
WHERE emppro.PrimaryProcess = 1 GROUP BY emppro.EmployID) AS temp ON emp.EmployID = temp.EmployID  
SET emp.AssignedVertical = temp.VerticalID;

UPDATE employ emp
JOIN (SELECT emppro.EmployID,pro.VerticalID FROM `processlead` emppro
JOIN PROCESS pro ON emppro.ProcessID = pro.ProcessID 
WHERE emppro.PrimaryProcess = 1 GROUP BY emppro.EmployID) AS temp ON emp.EmployID = temp.EmployID  
SET emp.AssignedVertical = temp.VerticalID;

UPDATE employ emp
JOIN (SELECT emppro.EmployID,pro.VerticalID FROM employprocess emppro
JOIN PROCESS pro ON emppro.ProcessID = pro.ProcessID 
WHERE emppro.PrimaryProcess = 1) AS temp ON emp.EmployID = temp.EmployID  
SET emp.AssignedVertical = temp.VerticalID;