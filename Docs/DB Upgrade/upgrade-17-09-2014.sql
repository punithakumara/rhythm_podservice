ALTER TABLE `employprocess` DROP COLUMN `BillableDate`, ADD COLUMN `StartDate` DATE NULL AFTER `BillableType`, ADD COLUMN `EndDate` DATE NULL AFTER `StartDate`, DROP INDEX `BillableDate`; 

ALTER TABLE `processmanager` ADD COLUMN `ShiftID` INT NULL AFTER `EmployID`, ADD COLUMN `Billable` TINYINT(1) DEFAULT 0 NULL AFTER `ShiftID`, ADD COLUMN `BillablePercentage` INT NULL AFTER `Billable`, ADD COLUMN `BillableType` VARCHAR(20) NULL AFTER `BillablePercentage`, ADD COLUMN `StartDate` DATE NULL AFTER `BillableType`, ADD COLUMN `EndDate` DATE NULL AFTER `StartDate`; 
ALTER TABLE `processlead` ADD COLUMN `ShiftID` INT NULL AFTER `EmployID`, ADD COLUMN `Billable` TINYINT(1) DEFAULT 0 NULL AFTER `ShiftID`, ADD COLUMN `BillablePercentage` INT NULL AFTER `Billable`, ADD COLUMN `BillableType` VARCHAR(20) NULL AFTER `BillablePercentage`, ADD COLUMN `StartDate` DATE NULL AFTER `BillableType`, ADD COLUMN `EndDate` DATE NULL AFTER `StartDate`; 

ALTER TABLE `processlead` ADD COLUMN `PrimanyProcess` TINYINT(1) DEFAULT 1 NULL AFTER `Billable`; 
ALTER TABLE `processmanager` ADD COLUMN `PrimanyProcess` TINYINT(1) DEFAULT 1 NULL AFTER `Billable`; 

ALTER TABLE `processmanager` ADD FOREIGN KEY (`ShiftID`) REFERENCES `shift`(`ShiftID`) ON UPDATE NO ACTION ON DELETE NO ACTION; 
ALTER TABLE `processlead` ADD FOREIGN KEY (`ShiftID`) REFERENCES `shift`(`ShiftID`) ON UPDATE NO ACTION ON DELETE NO ACTION; 


DELETE emp FROM employprocess emp JOIN employ e ON emp.EmployID = e.EmployID
JOIN `designation` d ON d.`DesignationID` = e.`DesignationID`
WHERE d.grades > 2