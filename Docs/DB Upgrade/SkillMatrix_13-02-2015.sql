
/*Table structure for table `certification_tbl` */

DROP TABLE IF EXISTS `certification_tbl`;

CREATE TABLE `certification_tbl` (
  `Certification_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Skill_ID` int(11) NOT NULL,
  `Certification_NAME` varchar(50) NOT NULL,
  `CRTD_DT` datetime DEFAULT NULL,
  `CRTD_BY` int(11) DEFAULT NULL,
  `LST_UPD_DT` datetime DEFAULT NULL,
  `LST_UPD_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`Certification_NAME`),
  UNIQUE KEY `Certification_ID` (`Certification_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

/*Table structure for table `employee_certification_tbl` */

DROP TABLE IF EXISTS `employee_certification_tbl`;

CREATE TABLE `employee_certification_tbl` (
  `Employee_Certification_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployID` int(11) NOT NULL,
  `Certification_ID` int(11) NOT NULL,
  `Year_Passed` int(11) DEFAULT NULL,
  `Month_Passed` varchar(5) DEFAULT NULL,
  `CRTD_DT` datetime DEFAULT NULL,
  `CRTD_BY` int(11) DEFAULT NULL,
  `LST_UPD_DT` datetime DEFAULT NULL,
  `LST_UPD_BY` int(11) DEFAULT NULL,
  UNIQUE KEY `Employee_Certification_ID` (`Employee_Certification_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Table structure for table `employee_skills_tbl` */

DROP TABLE IF EXISTS `employee_skills_tbl`;

CREATE TABLE `employee_skills_tbl` (
  `Employee_Skill_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Employee_ID` int(11) NOT NULL DEFAULT '0',
  `Skill_ID` int(11) NOT NULL DEFAULT '0',
  `Experience_Month` int(11) DEFAULT NULL,
  `CRTD_DT` date DEFAULT NULL,
  `CRTD_BY` int(11) DEFAULT NULL,
  `LST_UPD_DT` date DEFAULT NULL,
  `LST_UPD_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`Employee_ID`,`Skill_ID`),
  UNIQUE KEY `Employee_Skill_ID` (`Employee_Skill_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Table structure for table `skill_category_tbl` */

DROP TABLE IF EXISTS `skill_category_tbl`;

CREATE TABLE `skill_category_tbl` (
  `Category_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Category_Name` varchar(50) NOT NULL,
  `CRTD_DT` datetime DEFAULT NULL,
  `CRTD_BY` int(11) DEFAULT NULL,
  `LST_UPD_DT` datetime DEFAULT NULL,
  `LST_UPD_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`Category_Name`),
  UNIQUE KEY `Category_ID` (`Category_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Table structure for table `skill_map_tbl` */

DROP TABLE IF EXISTS `skill_map_tbl`;

CREATE TABLE `skill_map_tbl` (
  `Employee_ID` int(11) DEFAULT NULL,
  `[Javas]` int(11) DEFAULT NULL,
  `[PHP]` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `skill_tbl` */

DROP TABLE IF EXISTS `skill_tbl`;

CREATE TABLE `skill_tbl` (
  `Skill_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Category_ID` int(11) NOT NULL,
  `Skill_NAME` varchar(50) NOT NULL,
  `CRTD_DT` datetime DEFAULT NULL,
  `CRTD_BY` int(11) DEFAULT NULL,
  `LST_UPD_DT` datetime DEFAULT NULL,
  `LST_UPD_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`Skill_NAME`),
  UNIQUE KEY `Skill_ID` (`Skill_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

/* Procedure structure for procedure `Certification_Add_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Certification_Add_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Certification_Add_SP`(IN `iSkill_ID` INT, IN `iCertification_Name` VARCHAR(50), IN `iUser_id` INT)
    NO SQL
Begin
    INSERT INTO Certification_Tbl (Skill_ID, Certification_Name, CRTD_DT, CRTD_BY, LST_UPD_DT, LST_UPD_BY)
    SELECT 
        iSkill_ID, iCertification_Name, Now(), iUser_id, Now(), iUser_id;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Certification_Update_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Certification_Update_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Certification_Update_SP`(IN `iCertification_ID` INT, IN `iSkill_ID` INT, IN `iCertification_Name` VARCHAR(50), IN `iUser_id` INT)
    NO SQL
Begin
    Update 
        Certification_Tbl 
    SET 
	Skill_ID = iSkill_ID ,
        Certification_Name = iCertification_Name, 
        LST_UPD_DT = Now(), 
        LST_UPD_BY = iUser_id
    Where 
        Certification_ID = iCertification_ID;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Employee_Certification_Add_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Employee_Certification_Add_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Employee_Certification_Add_SP`(IN `iEmployID` INT, IN `iCertification_ID` INT, IN `iYear_Passed` INT, IN `iMonth_Passed` VARCHAR(5))
    NO SQL
Begin
    INSERT INTO employee_certification_tbl (EmployID , Certification_ID ,Year_Passed,Month_Passed,  CRTD_DT, CRTD_BY, LST_UPD_DT, LST_UPD_BY)
    SELECT 
         iEmployID, iCertification_ID, iYear_Passed , iMonth_Passed , Now(),iEmployID, Now(),iEmployID;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Employee_Skills_Add_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Employee_Skills_Add_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Employee_Skills_Add_SP`(IN `iEmployee_ID` INT, IN `iSkill_ID` INT, IN `iExperience_Year` INT, IN `iExperience_Month` INT, IN `iUser_ID` INT)
    NO SQL
Begin
		INSERT INTO 
        Employee_Skills_Tbl (Employee_ID, Skill_ID, Experience_Month, CRTD_DT, CRTD_BY, LST_UPD_DT, LST_UPD_BY)
		SELECT 
        iEmployee_ID, iSkill_ID, (iExperience_Year*12)+iExperience_Month, Now(), iUser_ID, Now(), iUser_ID ;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Employee_Skills_Update_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Employee_Skills_Update_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Employee_Skills_Update_SP`(IN `iEmployee_Skill_ID` INT, IN `iEmployee_ID` INT, IN `iSkill_ID` INT, IN `iExperience_Year` INT, IN `iExperience_Month` INT, IN `iUser_ID` INT)
    NO SQL
Begin
    Update 
        Employee_Skills_Tbl 
    Set 
        Employee_ID = iEmployee_ID,
        Skill_ID = iSkill_ID,
        Experience_Month = (iExperience_Year*12)+iExperience_Month,
        LST_UPD_DT = Now(), 
        LST_UPD_BY = iUser_id
    Where 
        Employee_Skill_ID = iEmployee_Skill_ID;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Skill_Add_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Skill_Add_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Skill_Add_SP`(IN `iSkill_Name` VARCHAR(50), IN `iCategory_ID` INT, IN `iUser_id` INT)
    NO SQL
Begin
    INSERT INTO Skill_Tbl (Skill_Name, Category_ID , CRTD_DT, CRTD_BY, LST_UPD_DT, LST_UPD_BY)
    SELECT 
        iSkill_Name, iCategory_ID , Now(),iUser_id, Now(),iUser_id;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Skill_Category_Add_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Skill_Category_Add_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Skill_Category_Add_SP`(IN `iCategory_Name` VARCHAR(50), IN `iUser_id` INT)
    NO SQL
Begin
    INSERT INTO Skill_Category_Tbl (Category_Name, CRTD_DT, CRTD_BY, LST_UPD_DT, LST_UPD_BY)
    SELECT 
        iCategory_Name , Now(),iUser_id, Now(),iUser_id;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Skill_Category_Update_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Skill_Category_Update_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Skill_Category_Update_SP`(IN `iCategory_ID` INT, IN `iCategory_Name` VARCHAR(50), IN `iUser_id` INT)
    NO SQL
Begin
    Update 
        Skill_Category_Tbl 
    SET 
        Category_Name = iCategory_Name, 
        LST_UPD_DT = Now(), 
        LST_UPD_BY = iUser_id
    Where 
        Category_ID = iCategory_ID;
End */$$
DELIMITER ;

/* Procedure structure for procedure `Skill_Update_SP` */

/*!50003 DROP PROCEDURE IF EXISTS  `Skill_Update_SP` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `Skill_Update_SP`(IN `iSkill_ID` INT, IN `iSkill_Name` VARCHAR(50), IN `iCategory_ID` INT, IN `iUser_id` INT)
    NO SQL
Begin
    Update 
        Skill_Tbl 
    SET 
        Skill_Name = iSkill_Name, 
	Category_ID = iCategory_ID,
        LST_UPD_DT = Now(), 
        LST_UPD_BY = iUser_id
    Where 
        Skill_ID = iSkill_ID;
End */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
