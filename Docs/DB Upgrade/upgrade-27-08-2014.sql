ALTER TABLE `client_nrr` CHANGE `Flag` `Flag` VARCHAR(11) NULL;

ALTER TABLE `process` ADD COLUMN `HeadCount` INT(11) DEFAULT 0 NULL AFTER `ProcessStatus`; 


UPDATE PROCESS JOIN (SELECT COUNT(employprocess.EmployID) AS Employid,employprocess.ProcessID FROM employprocess
JOIN employ ON (employprocess.employid=employ.employid)
JOIN designation ON (employ.`DesignationID`=designation.`DesignationID`)
WHERE employprocess.Billable = 1 AND employ.`RelieveFlag`= 0 AND designation.`grades` IN (1,2)  GROUP BY employprocess.ProcessID) emp 
    ON PROCESS.ProcessID = emp.ProcessID
SET PROCESS.HeadCount = emp.Employid;



CREATE TABLE `process_approver`( 
	`ProcessID` INT, 
	`EmployID` INT, 
	FOREIGN KEY (`ProcessID`) REFERENCES `process`(`ProcessID`) ON UPDATE NO ACTION ON DELETE NO ACTION, 
	FOREIGN KEY (`EmployID`) REFERENCES `employ`(`EmployID`) ON UPDATE NO ACTION ON DELETE NO ACTION 
); 