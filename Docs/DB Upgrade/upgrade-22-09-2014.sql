ALTER TABLE  notification DROP FOREIGN KEY notification_ibfk_2;


ALTER TABLE notification CHANGE ReceiverEmployee ReceiverEmployee VARCHAR(255) NULL;


UPDATE `employprocess` JOIN (SELECT employprocess.`EmployID`, employ_billability.`StartDate`,employ_billability.`BillableType`, employ_billability.ProcessID FROM employprocess
JOIN `employ_billability` ON (employprocess.`ProcessID`=`employ_billability`.`ProcessID` AND employprocess.`EmployID`=`employ_billability`.`EmployID`)
WHERE employprocess.Billable = 1 GROUP BY employ_billability.ProcessID, employ_billability.`EmployID`) emp 
    ON (employprocess.`ProcessID`=`emp`.`ProcessID` AND employprocess.`EmployID` = emp.EmployID)
SET employprocess.`StartDate` = emp.`StartDate`, `employprocess`.`BillableType` = emp.`BillableType`;

UPDATE employprocess SET`StartDate` = '2014-01-01' WHERE StartDate IS NULL AND Billable=1;