/*Table structure for table `clients_wishlist_favourites` */

DROP TABLE IF EXISTS `clients_wishlist_favourites`;

CREATE TABLE `clients_wishlist_favourites` (
  `Fid` int(10) NOT NULL AUTO_INCREMENT,
  `ClientID` int(10) DEFAULT NULL,
  `FavData` text,
  `AddedDate` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Fid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `client_sentimental_indicator`   
ADD COLUMN `comment` TEXT NULL  COMMENT 'To store latest comment' AFTER `ModifiedDate`,
ADD COLUMN `commentHistory` TEXT NULL  COMMENT 'To store comments history.' AFTER `comment`,
ADD COLUMN `FTECount` INT(8) NULL AFTER `commentHistory`;
UPDATE `client_sentimental_indicator` SET `FTECount` = 0;
ALTER TABLE `client_sentimental_indicator` CHANGE `FTECount` `FTECount` INT(8) DEFAULT 0 NULL; 