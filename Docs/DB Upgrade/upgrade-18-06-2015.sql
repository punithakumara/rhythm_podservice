Modified View :: accuracy_util_month_view table
------------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `accuracy_util_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `accuracy_util_month_view` AS (
SELECT DISTINCT
  `process`.`ProcessID`         AS `ProcessID`,
  `process`.`ClientID`          AS `ClientID`,
  `process`.`VerticalID`        AS `VerticalID`,
  `client`.`Client_Name`        AS `Client_Name`,
  `process`.`Name`              AS `name`,
  `vertical`.`Name`              AS `Vertical_Name`,
  FORMAT(AVG(`accuracy`.`Accuracy`),2) AS `Accuracy`,
  FORMAT(AVG(`utilization_process`.`Utilization`),2) AS `Utilization`,
  `utilization_process`.`Month` AS `Month`,
  `utilization_process`.`Year`  AS `Year`
FROM ((((`process`
     JOIN `client`
       ON ((`client`.`ClientID` = `process`.`ClientID`)))
      JOIN `vertical`
       ON ((`vertical`.`VerticalID` = `process`.`VerticalID`)))
    JOIN `utilization_process`
      ON ((`utilization_process`.`ProcessID` = `process`.`ProcessID`)))
   JOIN `accuracy`
     ON ((`accuracy`.`ProcessID` = `process`.`ProcessID`)))
GROUP BY `process`.`ProcessID`,`utilization_process`.`Month`,`utilization_process`.`Year`
ORDER BY `utilization_process`.`Month`,`utilization_process`.`Year`,`process`.`ProcessID`)$$

DELIMITER ;

Modified View :: reports_month_view table
------------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `reports_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `reports_month_view` AS (
SELECT
  `tablea`.`ProcessID`   AS `ProcessID`,
  `tablea`.`ClientID`    AS `ClientID`,
  `tablea`.`VerticalID`  AS `VerticalID`,
  `tablea`.`Client_Name` AS `Client_Name`,
  `tablea`.`name`        AS `Process_Name`,
  `tablea`.`Vertical_Name`        AS `Vertical_Name`,
  `tablea`.`Month`       AS `Month`,
  `tablea`.`Year`        AS `Year`,
  `tablea`.`Accuracy`    AS `Accuracy`,
  `tablea`.`Utilization` AS `Utilization`,
  IF((`tableb`.`Error` IS NOT NULL),`tableb`.`Error`,0) AS `Error`,
  IF((`tableb`.`Appreciation` IS NOT NULL),`tableb`.`Appreciation`,0) AS `Appreciation`,
  IF((`tableb`.`Escalation` IS NOT NULL),`tableb`.`Escalation`,0) AS `Escalation`,
  IF((`tablec`.`Billable` IS NOT NULL),`tablec`.`Billable`,0) AS `Billable`,
  IF((`tabled`.`NonBillable` IS NOT NULL),`tabled`.`NonBillable`,0) AS `NonBillable`
FROM (((`accuracy_util_month_view` `tablea`
     LEFT JOIN `incidentlog_month_view` `tableb`
       ON (((`tablea`.`ProcessID` = `tableb`.`ProcessID`)
            AND (`tablea`.`Month` = `tableb`.`Month`)
            AND (`tablea`.`Year` = `tableb`.`Year`))))
    LEFT JOIN `billable_view` `tablec`
      ON ((`tablea`.`ProcessID` = `tablec`.`ProcessID`)))
   LEFT JOIN `nonbillable_view` `tabled`
     ON ((`tablea`.`ProcessID` = `tabled`.`ProcessID`)))
ORDER BY `tablea`.`Year`,`tablea`.`Month`,`tablea`.`ProcessID`)$$

DELIMITER ;

Modified View :: accuracy_util_week_view table
------------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `accuracy_util_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `accuracy_util_week_view` AS (
SELECT DISTINCT
  `process`.`ProcessID`               AS `ProcessID`,
  `process`.`ClientID`                AS `ClientID`,
  `process`.`VerticalID`              AS `VerticalID`,
  `client`.`Client_Name`              AS `Client_Name`,
  `process`.`Name`                    AS `name`,
  `vertical`.`Name`              AS `Vertical_Name`,
  FORMAT(AVG(`accuracy`.`Accuracy`),2) AS `Accuracy`,
  FORMAT(AVG(`utilization_weekly_process`.`Utilization`),2) AS `Utilization`,
  `utilization_weekly_process`.`Week` AS `Week`,
  `utilization_weekly_process`.`Year` AS `Year`
FROM ((((`process`
     JOIN `client`
       ON ((`client`.`ClientID` = `process`.`ClientID`)))
      JOIN `vertical`
       ON ((`vertical`.`VerticalID` = `process`.`VerticalID`)))
    JOIN `utilization_weekly_process`
      ON ((`utilization_weekly_process`.`ProcessID` = `process`.`ProcessID`)))
   JOIN `accuracy`
     ON ((`accuracy`.`ProcessID` = `process`.`ProcessID`)))
GROUP BY `process`.`ProcessID`,`utilization_weekly_process`.`Week`,`utilization_weekly_process`.`Year`
ORDER BY `utilization_weekly_process`.`Week`,`utilization_weekly_process`.`Year`,`process`.`ProcessID`)$$

DELIMITER ;



Modified View :: reports_week_view table
------------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `reports_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `reports_week_view` AS (
SELECT
  `tablea`.`ProcessID`   AS `ProcessID`,
  `tablea`.`ClientID`    AS `ClientID`,
  `tablea`.`VerticalID`  AS `VerticalID`,
  `tablea`.`Client_Name` AS `Client_Name`,
  `tablea`.`name`        AS `Process_Name`,
  `tablea`.`Vertical_Name`              AS `Vertical_Name`,
  `tablea`.`Week`        AS `Week`,
  DATE_FORMAT((CONCAT(`tablea`.`Year`,'-01-01') + INTERVAL `tablea`.`Week` WEEK),'%m') AS `Month`,
  `tablea`.`Year`        AS `Year`,
  `tablea`.`Accuracy`    AS `Accuracy`,
  `tablea`.`Utilization` AS `Utilization`,
  IF((`tableb`.`Error` IS NOT NULL),`tableb`.`Error`,0) AS `Error`,
  IF((`tableb`.`Appreciation` IS NOT NULL),`tableb`.`Appreciation`,0) AS `Appreciation`,
  IF((`tableb`.`Escalation` IS NOT NULL),`tableb`.`Escalation`,0) AS `Escalation`,
  IF((`tablec`.`Billable` IS NOT NULL),`tablec`.`Billable`,0) AS `Billable`,
  IF((`tabled`.`NonBillable` IS NOT NULL),`tabled`.`NonBillable`,0) AS `NonBillable`
FROM (((`accuracy_util_week_view` `tablea`
     LEFT JOIN `incidentlog_week_view` `tableb`
       ON (((`tablea`.`ProcessID` = `tableb`.`ProcessID`)
            AND (`tablea`.`Week` = `tableb`.`Week`)
            AND (`tablea`.`Year` = `tableb`.`Year`))))
    LEFT JOIN `billable_view` `tablec`
      ON ((`tablea`.`ProcessID` = `tablec`.`ProcessID`)))
   LEFT JOIN `nonbillable_view` `tabled`
     ON ((`tablea`.`ProcessID` = `tabled`.`ProcessID`)))
ORDER BY `tablea`.`Year`,`tablea`.`Week`,`tablea`.`ProcessID`)$$

DELIMITER ;
