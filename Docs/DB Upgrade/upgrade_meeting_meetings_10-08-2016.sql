ALTER TABLE `action_meetings` ADD COLUMN `Public` TINYINT(1) DEFAULT 1 NULL COMMENT '0-private 1- public' AFTER `Nstatus`; 
ALTER TABLE `action_meetings` ADD COLUMN `CompletedDate` DATE NULL AFTER `Public`; 
ALTER TABLE `topic_table` CHANGE `Name` `Name` VARCHAR(200) CHARSET utf8 COLLATE utf8_general_ci NULL; 