/*
SQLyog Community v10.00 Beta1
MySQL - 5.5.24 : Database - rhythm_dev2
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rhythm_dev2` /*!40100 DEFAULT CHARACTER SET utf8 */;



/*Table structure for table `client_sentimental_indicator` */

DROP TABLE IF EXISTS `client_sentimental_indicator`;

CREATE TABLE `client_sentimental_indicator` (
  `Sid` int(8) NOT NULL AUTO_INCREMENT,
  `ClientContactID` int(8) DEFAULT NULL COMMENT 'RatingID is refered to the client_sentimental_ratings primary id',
  `Communication` varchar(255) DEFAULT NULL,
  `ResponseTime` varchar(255) DEFAULT NULL,
  `LeadershipStyle` varchar(255) DEFAULT NULL,
  `Trust` varchar(255) DEFAULT NULL,
  `Sentiment` int(4) DEFAULT NULL,
  `AddedBy` int(4) DEFAULT NULL COMMENT 'This column save employee id added this record',
  `ModifiedBy` int(4) DEFAULT NULL,
  `AddedDate` date DEFAULT NULL,
  `ModifiedDate` date DEFAULT NULL,
  PRIMARY KEY (`Sid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
