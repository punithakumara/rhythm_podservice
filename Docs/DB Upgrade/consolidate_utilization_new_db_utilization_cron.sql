ALTER TABLE `employwise_utilization_process` ADD COLUMN `expectedHours` VARCHAR(10) NULL AFTER `WeekendSupport`; 

ALTER TABLE `employwise_utilization_process` CHANGE `Utilization` `Utilization` VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci NULL;