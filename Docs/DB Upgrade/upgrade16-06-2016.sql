DROP TABLE IF EXISTS `verticals`;

CREATE TABLE `verticals` (
  `VerticalID` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(50) DEFAULT '',
  `Description` VARCHAR(2000) DEFAULT '',
  `Folder` VARCHAR(15) DEFAULT '',
  `BID` INT(11) DEFAULT '0',
  `Parent_VerticalID` INT(11) DEFAULT NULL,
  `shortName` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`VerticalID`),
  KEY `Name` (`Name`)
) ENGINE=INNODB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT  INTO `verticals`(`VerticalID`,`Name`,`Description`,`Folder`,`BID`,`Parent_VerticalID`,`shortName`) VALUES (1,'Client Services Group','','csg',0,21,'CSG'),(2,'IT','','it',0,21,'IT'),(3,'Product Engineering Group','','peg',0,21,'PEG'),(4,'Administration & Facilities','Administration; Facility Management; Transport management','FM',0,21,'A&F'),(5,'Delivery Excellence','Education Excellence, Process Excellence, V&V','delex',0,21,'Delex'),(6,'Adops1','','adops',0,22,'Adops1'),(7,'Tech Services','Technology Services (Email & Web Development)','tech_services',0,22,'TechServ'),(8,'Reporting','Manual Reporting','reporting',0,22,'Reporting'),(9,'Star Client Group','','scg',0,22,'SCG'),(10,'HR','','hr',0,21,'HR'),(11,'Search','Search Operations','search',0,22,'Search'),(12,'Media Review','Content & Creative Review (Audit)','media_review',0,22,'MediaRev'),(13,'Finance','','finance',0,21,'Finance'),(14,'PMO','','comp',0,21,'PMO'),(17,'CSG - Account Management','','act',0,21,'CSG-AM'),(18,'Adops2','Adops2','adops2',0,22,'Adops2'),(20,'TheoremIndia Ltd','TheoremIndia Ltd','',0,0,NULL),(21,'Support','Support','',0,20,NULL),(22,'Operations','Operations','',0,20,NULL);

