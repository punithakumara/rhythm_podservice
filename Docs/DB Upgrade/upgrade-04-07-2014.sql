CREATE TABLE `utilization_process`( 
	`UID` INT NOT NULL AUTO_INCREMENT, 
	`ProcessID` INT, 
	`Month` INT, 
	`Year` INT, 
	`ExpectedHours` VARCHAR(10), 
	`ActualHours` VARCHAR(10), 
	`Utilization` FLOAT, 
	PRIMARY KEY (`UID`), 
	FOREIGN KEY (`ProcessID`) REFERENCES `process`(`ProcessID`) ON UPDATE NO ACTION ON DELETE NO ACTION ); 
	
CREATE TABLE `notification`( 
	`NotificationID` INT NOT NULL AUTO_INCREMENT, 
	`Title` VARCHAR(100), 
	`Description` TEXT, 
	`SenderEmployee` INT, 
	`ReceiverEmployee` INT, 
	`URL` TEXT, 
	`NotificationDate` DATETIME,
	`Read` TINYINT(1) DEFAULT 0, 
	PRIMARY KEY (`NotificationID`), 
	FOREIGN KEY (`SenderEmployee`) REFERENCES `employ`(`EmployID`) ON UPDATE NO ACTION ON DELETE NO ACTION, 
	FOREIGN KEY (`ReceiverEmployee`) REFERENCES `employ`(`EmployID`) ON UPDATE NO ACTION ON DELETE NO ACTION );
	
	
ALTER TABLE `employ` ADD COLUMN `UserDashboard` TEXT NULL AFTER `TrainingNeeded`; 