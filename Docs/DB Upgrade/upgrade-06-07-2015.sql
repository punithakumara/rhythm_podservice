DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `accuracy_util_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `accuracy_util_month_view` AS (
SELECT DISTINCT
  `process`.`ProcessID`         AS `ProcessID`,
  `process`.`ClientID`          AS `ClientID`,
  `process`.`VerticalID`        AS `VerticalID`,
  `client`.`Client_Name`        AS `Client_Name`,
  `process`.`Name`              AS `name`,
  `vertical`.`Name`             AS `Vertical_Name`,
  FORMAT(AVG(`accuracy`.`Accuracy`),2) AS `Accuracy`,
  FORMAT(AVG(`utilization_process`.`Utilization`),2) AS `Utilization`,
  `utilization_process`.`Month` AS `Month`,
  `utilization_process`.`Year`  AS `Year`
FROM ((((`process`
      JOIN `client`
        ON ((`client`.`ClientID` = `process`.`ClientID`)))
     JOIN `vertical`
       ON ((`vertical`.`VerticalID` = `process`.`VerticalID`)))
    JOIN `utilization_process`
      ON ((`utilization_process`.`ProcessID` = `process`.`ProcessID`)))
   LEFT JOIN `accuracy`
     ON ((`accuracy`.`ProcessID` = `process`.`ProcessID`)))
GROUP BY `process`.`ProcessID`,`utilization_process`.`Month`,`utilization_process`.`Year`
ORDER BY `utilization_process`.`Month`,`utilization_process`.`Year`,`process`.`ProcessID`)$$

DELIMITER ;


-----------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `accuracy_util_week_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `accuracy_util_week_view` AS (
SELECT DISTINCT
  `process`.`ProcessID`               AS `ProcessID`,
  `process`.`ClientID`                AS `ClientID`,
  `process`.`VerticalID`              AS `VerticalID`,
  `client`.`Client_Name`              AS `Client_Name`,
  `process`.`Name`                    AS `name`,
  `vertical`.`Name`                   AS `Vertical_Name`,
  FORMAT(AVG(`accuracy`.`Accuracy`),2) AS `Accuracy`,
  FORMAT(AVG(`utilization_weekly_process`.`Utilization`),2) AS `Utilization`,
  `utilization_weekly_process`.`Week` AS `Week`,
  `utilization_weekly_process`.`Year` AS `Year`
FROM ((((`process`
      JOIN `client`
        ON ((`client`.`ClientID` = `process`.`ClientID`)))
     JOIN `vertical`
       ON ((`vertical`.`VerticalID` = `process`.`VerticalID`)))
    JOIN `utilization_weekly_process`
      ON ((`utilization_weekly_process`.`ProcessID` = `process`.`ProcessID`)))
   LEFT JOIN `accuracy`
     ON ((`accuracy`.`ProcessID` = `process`.`ProcessID`)))
GROUP BY `process`.`ProcessID`,`utilization_weekly_process`.`Week`,`utilization_weekly_process`.`Year`
ORDER BY `utilization_weekly_process`.`Week`,`utilization_weekly_process`.`Year`,`process`.`ProcessID`)$$

DELIMITER ;

--------------------------------------------------------------------------


DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `nonbillable_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `nonbillable_view` AS (
SELECT DISTINCT
  `process`.`ProcessID` AS `ProcessID`,
  `process`.`ClientID`  AS `ClientID`,
  WEEK(`employprocess`.`StartDate`,0) AS `Week`,
  YEAR(`employprocess`.`StartDate`) AS `Year`,
  COUNT(`employprocess`.`EmployID`) AS `NonBillable`,
  GROUP_CONCAT(`employprocess`.`EmployID` SEPARATOR ',') AS `EmployID`
FROM ((`process`
    JOIN `client`
      ON ((`client`.`ClientID` = `process`.`ClientID`)))
   JOIN `employprocess`
     ON ((`employprocess`.`ProcessID` = `process`.`ProcessID`)))
WHERE ((`process`.`ProcessStatus` = 0)
       AND (`employprocess`.`Billable` = 0))
GROUP BY `process`.`ProcessID`
ORDER BY YEAR(`employprocess`.`StartDate`),WEEK(`employprocess`.`StartDate`,0),`process`.`ProcessID`)$$

DELIMITER ;


-------------------------------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `billable_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `billable_view` AS (
SELECT DISTINCT
  `process`.`ProcessID` AS `ProcessID`,
  `process`.`ClientID`  AS `ClientID`,
  WEEK(`employprocess`.`StartDate`,0) AS `Week`,
  YEAR(`employprocess`.`StartDate`) AS `Year`,
  COUNT(`employprocess`.`EmployID`) AS `Billable`,
  GROUP_CONCAT(`employprocess`.`EmployID` SEPARATOR ',') AS `EmployID`
FROM ((`process`
    JOIN `client`
      ON ((`client`.`ClientID` = `process`.`ClientID`)))
   JOIN `employprocess`
     ON ((`employprocess`.`ProcessID` = `process`.`ProcessID`)))
WHERE ((`process`.`ProcessStatus` = 0)
       AND (`employprocess`.`Billable` = 1))
GROUP BY `process`.`ProcessID`
ORDER BY YEAR(`employprocess`.`StartDate`),WEEK(`employprocess`.`StartDate`,0),`process`.`ProcessID`)$$

DELIMITER ;


--------------------------------------------------------------

DELIMITER $$

USE `rhythm_dev`$$

DROP VIEW IF EXISTS `reports_month_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `reports_month_view` AS (
SELECT
  `tablea`.`ProcessID`     AS `ProcessID`,
  `tablea`.`ClientID`      AS `ClientID`,
  `tablea`.`VerticalID`    AS `VerticalID`,
  `tablea`.`Client_Name`   AS `Client_Name`,
  `tablea`.`name`          AS `Process_Name`,
  `tablea`.`Vertical_Name` AS `Vertical_Name`,
  `tablea`.`Month`         AS `Month`,
  `tablea`.`Year`          AS `Year`,
  `tablea`.`Accuracy`      AS `Accuracy`,
  `tablea`.`Utilization`   AS `Utilization`,
  IF((`tableb`.`Error` IS NOT NULL),`tableb`.`Error`,0) AS `Error`,
  IF((`tableb`.`Appreciation` IS NOT NULL),`tableb`.`Appreciation`,0) AS `Appreciation`,
  IF((`tableb`.`Escalation` IS NOT NULL),`tableb`.`Escalation`,0) AS `Escalation`,
  IF((`tablec`.`Billable` IS NOT NULL),`tablec`.`Billable`,0) AS `Billable`,
  IF((`tabled`.`NonBillable` IS NOT NULL),`tabled`.`NonBillable`,0) AS `NonBillable`,
  IF((`tablec`.`EmployID` IS NOT NULL),`tablec`.`EmployID`,0) AS `BillableEmployees`,
  IF((`tabled`.`EmployID` IS NOT NULL),`tabled`.`EmployID`,0) AS `NonBillableEmployees`
FROM (((`accuracy_util_month_view` `tablea`
     LEFT JOIN `incidentlog_month_view` `tableb`
       ON (((`tablea`.`ProcessID` = `tableb`.`ProcessID`)
            AND (`tablea`.`Month` = `tableb`.`Month`)
            AND (`tablea`.`Year` = `tableb`.`Year`))))
    LEFT JOIN `billable_view` `tablec`
      ON ((`tablea`.`ProcessID` = `tablec`.`ProcessID`)))
   LEFT JOIN `nonbillable_view` `tabled`
     ON ((`tablea`.`ProcessID` = `tabled`.`ProcessID`)))
ORDER BY `tablea`.`Year`,`tablea`.`Month`,`tablea`.`ProcessID`)$$

DELIMITER ;