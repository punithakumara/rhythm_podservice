DELIMITER $$



DROP VIEW IF EXISTS `view_client_dossier`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_client_dossier` AS 
SELECT
  `client`.`ClientID`      AS `ClientID`,
  `client`.`Country`       AS `Country`,
  `client`.`Client_Name`   AS `Client_Name`,
  `process`.`ProcessID`    AS `ProcessID`,
  `process`.`ForcastNRR`   AS `ForcastNRR`,
  `process`.`ForcastRDR`   AS `ForcastRDR`,
  `client`.`ClientTier`    AS `ClientTier`,
  `process`.`HeadCount`    AS `resourceCount`,
  `client`.`industryType`  AS `industryType`,
  `client`.`accManager`    AS `accManager`,
  `client`.`status`        AS `STATUS`,
  `client`.`followAccount` AS `followAccount`,
  `client`.`clientPartner` AS `clientPartner`,
  `client`.`inactiveDate` AS `inactiveDate`,
  DATE_FORMAT(`client`.`Created_Date`,'%d-%m-%Y') AS `Created_Date`,
  `client`.`Logo`          AS `Logo`,
  GROUP_CONCAT(DISTINCT `shift`.`ShiftCode` SEPARATOR ',') AS `shifts`
FROM ((((`client`
      LEFT JOIN `process`
        ON ((`process`.`ClientID` = `client`.`ClientID`) AND `process`.`ProcessStatus` = 0))
     LEFT JOIN `employ_process_view`
       ON (((`employ_process_view`.`ProcessID` = `process`.`ProcessID`)
            AND (`process`.`ProcessStatus` = 0)
            AND (`employ_process_view`.`PrimaryProcess` = 1))))
    LEFT JOIN `shift`
      ON ((`shift`.`ShiftID` = `employ_process_view`.`ShiftID`)))
   LEFT JOIN `employ`
     ON ((`employ`.`EmployID` = `employ_process_view`.`EmployID`))) 
GROUP BY `client`.`ClientID`,`process`.`ProcessID`$$

DELIMITER ;