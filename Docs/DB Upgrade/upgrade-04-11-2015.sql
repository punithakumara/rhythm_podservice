CREATE TABLE `verticals` (
  `VerticalID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT '',
  `Description` varchar(2000) DEFAULT '',
  `Folder` varchar(15) DEFAULT '',
  `BID` int(11) DEFAULT '0',
  `Parent_VerticalID` int(11) DEFAULT NULL,
  PRIMARY KEY (`VerticalID`),
  KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

insert  into `verticals`(`VerticalID`,`Name`,`Description`,`Folder`,`BID`,`Parent_VerticalID`) values (1,'Client Services Group','','csg',0,21),(2,'IT','','it',0,21),(3,'Product Engineering Group','','peg',0,21),(4,'Administration & Facilities','Administration; Facility Management; Transport management','FM',0,21),(5,'Delivery Excellence','Education Excellence, Process Excellence, V&V','delex',0,21),(6,'Adops1','','adops',0,22),(7,'Tech Services','Technology Services (Email & Web Development)','tech_services',0,22),(8,'Reporting','Manual Reporting','reporting',0,22),(9,'Star Client Group','','scg',0,22),(10,'HR','','hr',0,21),(11,'Search','Search Operations','search',0,22),(12,'Media Review','Content & Creative Review (Audit)','media_review',0,22),(13,'Finance','','finance',0,21),(14,'PMO','','comp',0,21),(16,'Verification & Validation','','vv',0,22),(17,'CSG - Account Management','','act',0,21),(18,'Adops2','Adops2','adops2',0,22),(20,'TheoremIndia Ltd','TheoremIndia Ltd','',0,0),(21,'Support','Support','',0,20),(22,'Operations','Operations','',0,20);
