DROP TABLE IF EXISTS `document_templates`;

CREATE TABLE `document_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'pk',
  `template_name` varchar(200) DEFAULT NULL,
  `uploaded_default_name` varchar(250) DEFAULT NULL,
  `upload_by` int(11) DEFAULT NULL COMMENT 'uploaded empolyee id',
  `template_type` tinyint(1) DEFAULT NULL COMMENT '1-company,2-process,3-vertical',
  `template_access_level` varchar(50) DEFAULT NULL,
  `verticalID` int(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  `processID` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0-inactive,1-active,2-delete',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;