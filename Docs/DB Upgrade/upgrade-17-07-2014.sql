CREATE TABLE `client_users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(25) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `ClientID` (`ClientID`),
  CONSTRAINT `client_users_ibfk_1` FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DELETE FROM `verticalmanager` WHERE `VerticalID` = '15' AND`EmployID` = '937';
UPDATE `process` SET `VerticalID` = '10' WHERE `ProcessID` = '223'; 
DELETE FROM `vertical` WHERE `VerticalID` = '15'; 
ALTER TABLE `vertical` AUTO_INCREMENT=15;
UPDATE `processlead` SET `EmployID` = '288' WHERE `ProcessID` = '223' AND`EmployID` = '67'; 