DELIMITER $$

DROP VIEW IF EXISTS `dct_employees`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `dct_employees` AS 
SELECT
  `e`.`EmployID`        AS `EmployID`,
  `e`.`CompanyEmployID` AS `CompanyEmployID`,
  `e`.`FirstName`       AS `FirstName`,
  `e`.`LastName`        AS `LastName`,
  `e`.`Email`           AS `Email`,
  `e`.`RelieveDate`     AS `RelieveDate`,
  `e`.`DesignationID`   AS `DesignationID`,
  `e`.`DateOfBirth`     AS `DateOfBirth`,
  `e`.`DateOfJoin`      AS `DateOfJoin`,
  `d`.`Name`            AS `Designation`,
  `d`.`grades`          AS `Grades`,
  `v`.`VerticalID`      AS `VerticalID`,
  `v`.`Name`            AS `VerticalName`
FROM ((((`employ` `e`
      JOIN `designation` `d`
        ON ((`e`.`DesignationID` = `d`.`DesignationID`)))
     JOIN `employ_process_view` `ep`
       ON ((`e`.`EmployID` = `ep`.`EmployID`)))
    JOIN `process` `p`
      ON ((`ep`.`ProcessID` = `p`.`ProcessID`)))
   JOIN `vertical` `v`
     ON ((`p`.`VerticalID` = `v`.`VerticalID`)))
WHERE (`e`.`RelieveFlag` = 0)
ORDER BY `e`.`EmployID`$$

DELIMITER ;