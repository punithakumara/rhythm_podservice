/*
SQLyog Community v10.00 Beta1
MySQL - 5.5.24 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `custom_report_columns` (
	`ColumnID` int ,
	`ColumnName` varchar ,
	`ReportID` varchar ,
	`Status` int 
); 
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('1','AccountManager',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('2','AppreciatedBy',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('3','Appreciation',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('4','ApprovedFTE','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('5','AssociateManager',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('6','Billable','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('8','BillableNonBillable',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('9','Client','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('10','ClientSince',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('11','ClientStatus',NULL,'1');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('12','Date',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('13','Designation',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('14','EmployName',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('15','EmployID',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('19','Error',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('20','Escalation',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('23','ExpHours',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('24','Grade',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('26','Location ',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('27','Month',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('28','NoOfDays',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('29','NoOfHours',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('30','NoOfResourcesWorked',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('31','NoOfTasks',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('32','NonBillable','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('33','NonBillableBuffer','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('34','NoticePeriod',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('35','Process','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('36','ProcessOwner',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('37','ReportingManager',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('38','TeamLeader',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('39','Shift',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('40','Utilization ',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('41','Vertical','2','0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('42','VerticalManager',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('43','Year',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('44','Type',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('45','Status',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('46','ClientType',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('47','NoOfErrors',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('48','Email',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('49','DOJ',NULL,'0');
insert into `custom_report_columns` (`ColumnID`, `ColumnName`, `ReportID`, `Status`) values('50','Gender',NULL,'0');
insert into `custom_report_columns`(`ColumnID` , `ColumnName`, `ReportID`, `Status`) values ('51','TaskDate',NULL,0);
