CREATE TABLE `executive_dashboard`( 
		`ExecutiveID` INT NOT NULL AUTO_INCREMENT, 
		`ProcessID` INT, 
		`week` DATE, 
		`attributes` TEXT, 
		`challenges` TEXT, 
		`risk` TEXT, 
		`EmployID` INT, 
		`showutil` TINYINT(1) DEFAULT 0, 
		`showaccuracy` TINYINT(1) DEFAULT 0, 
		`createdDate` DATETIME, 
		PRIMARY KEY (`ExecutiveID`), 
		FOREIGN KEY (`ProcessID`) REFERENCES `process`(`ProcessID`) ON UPDATE NO ACTION ON DELETE NO ACTION, 
		FOREIGN KEY (`EmployID`) REFERENCES `employ`(`EmployID`) ON UPDATE NO ACTION ON DELETE NO ACTION ); 