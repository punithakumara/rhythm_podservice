DROP VIEW IF EXISTS view_client_new

DROP VIEW IF EXISTS view_client_dossier

DROP VIEW IF EXISTS employ_process_view


CREATE VIEW view_client_new AS 
SELECT
   temp . ClientID       AS  ClientID ,
   temp . Client_Name    AS  Client_Name ,
   temp . ProcessID      AS  ProcessID ,
   temp . resourceCount  AS  resourceCount ,
   temp . industryType   AS  industryType ,
   temp . accManager     AS  accManager ,
   temp . STATUS         AS  STATUS ,
   temp . Created_Date   AS  Created_Date ,
   temp . Logo           AS  Logo ,
   temp . shifts         AS  shifts ,
  GROUP_CONCAT(DISTINCT    technology . Name  SEPARATOR ',') AS  technologies 
FROM ((   view_client_dossier   temp 
    LEFT JOIN    processtechnology 
      ON (( temp . ProcessID  =    processtechnology . ProcessID )))
   LEFT JOIN    technology 
     ON ((   technology . TechnologyID  =    processtechnology . TechnologyID )))
GROUP BY  temp . ClientID , temp . ProcessID


CREATE VIEW view_client_dossier AS 
SELECT
   client . ClientID      AS  ClientID ,
   client . Client_Name   AS  Client_Name ,
   process . ProcessID    AS  ProcessID ,
  SUM(( employ . RelieveFlag  = 0)) AS  resourceCount ,
   client . industryType  AS  industryType ,
   client . accManager    AS  accManager ,
   client . status        AS  STATUS ,
  DATE_FORMAT( client . Created_Date ,'%d-%m-%Y') AS  Created_Date ,
   client . Logo          AS  Logo ,
  GROUP_CONCAT(DISTINCT  shift . ShiftCode  SEPARATOR ',') AS  shifts 
FROM (((( client 
      LEFT JOIN  process 
        ON (( process . ClientID  =  client . ClientID )))
     LEFT JOIN  employ_process_view 
       ON ((( employ_process_view . ProcessID  =  process . ProcessID )
            AND ( employ_process_view . PrimaryProcess  = 1))))
    LEFT JOIN  shift 
      ON (( shift . ShiftID  =  employ_process_view . ShiftID )))
   LEFT JOIN  employ 
     ON (( employ . EmployID  =  employ_process_view . EmployID )))
GROUP BY  client . ClientID , process . ProcessID 


CREATE VIEW employ_process_view AS 
SELECT   employprocess . ProcessID  AS  ProcessID ,  employprocess . EmployID  AS  EmployID ,  employprocess . PrimaryProcess  AS  PrimaryProcess ,  employprocess . ShiftID  AS  ShiftID 
FROM   employprocess 
 UNION 
(SELECT   processmanager . ProcessID  AS  ProcessID ,  processmanager . EmployID  AS  EmployID ,  processmanager . PrimaryProcess  AS  PrimaryProcess ,  processmanager . ShiftID  AS  ShiftID  
FROM   processmanager  
WHERE (  processmanager . Billable  = 1)) 
UNION
 (SELECT   processlead . ProcessID  AS  ProcessID ,  processlead . EmployID  AS  EmployID ,  processlead . PrimaryProcess  AS  PrimaryProcess ,  processlead . ShiftID  AS  ShiftID  
FROM   processlead  
WHERE (  processlead . Billable  = 1))


