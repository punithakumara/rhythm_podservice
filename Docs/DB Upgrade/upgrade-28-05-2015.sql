ALTER TABLE `employ` ADD COLUMN `Last_ModifiedBy` INT  NULL AFTER `TransitionStatus`, ADD COLUMN `Last_Modified_Date` DATETIME  NULL AFTER `Last_ModifiedBy`;
ALTER TABLE `employ` ADD COLUMN `Comments` TEXT NULL AFTER `Last_Modified_Date`;
ALTER TABLE `feedback` ADD COLUMN `created_date` DATETIME NULL AFTER `ClientUserId`; 