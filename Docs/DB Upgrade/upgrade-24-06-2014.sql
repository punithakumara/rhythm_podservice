ALTER TABLE `meeting_minutes` CHANGE  `Attendees`  `InternalAttendees` TEXT NULL DEFAULT '';

ALTER TABLE `meeting_minutes` ADD COLUMN `ExternalAttendees` TEXT NULL DEFAULT '' AFTER `MeetingMinID`;

ALTER TABLE `client_contact` CHANGE `Contact_ID` `Contact_ID` INT(11) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (`Contact_ID`); 

ALTER TABLE `utilization` ADD COLUMN `Attribuites` MEDIUMTEXT NULL AFTER `Min`; 
	
DROP table TABLE IF EXISTS `utilization_attributes`;
