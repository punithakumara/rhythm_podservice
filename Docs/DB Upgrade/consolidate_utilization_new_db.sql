DROP TABLE IF EXISTS `employwise_utilization_process`;

CREATE TABLE `employwise_utilization_process` (
  `Emp_UT_ProcessID` int(11) NOT NULL AUTO_INCREMENT,
  `UtilizationID` int(11) NOT NULL COMMENT 'FK of Utilization table',
  `ProcessID` int(11) NOT NULL COMMENT 'FK of Process table',
  `ProcessName` varchar(256) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `clientName` varchar(100) DEFAULT NULL,
  `Utilization` varchar(6) DEFAULT NULL,
  `Num_Hours` time DEFAULT NULL,
  `EmployID` int(11) NOT NULL COMMENT 'FK employ Table',
  `TaskDate` date DEFAULT NULL,
  `VerticalID` int(11) DEFAULT NULL,
  `VerticalName` varchar(250) DEFAULT NULL,
  `errors` varchar(50) DEFAULT NULL,
  `Created_date` datetime DEFAULT NULL,
  `shiftID` varchar(20) DEFAULT NULL,
  `WeekendSupport` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Emp_UT_ProcessID`),
  KEY `UtilizationID` (`UtilizationID`),
  KEY `ProcessID` (`ProcessID`),
  KEY `EmployID` (`EmployID`),
  KEY `TaskDate` (`TaskDate`)
) ENGINE=InnoDB AUTO_INCREMENT=111243 DEFAULT CHARSET=utf8;