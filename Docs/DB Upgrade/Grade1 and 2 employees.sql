/* Get Grade 1 and 2 employee */

SELECT `employ`.`CompanyEmployID` AS EmployeeID, `employ`.`FirstName`, `employ`.`LastName`, `employ`.`Email`, `location`.`Name` AS Location, `shift`.`ShiftCode` AS Shift, `vertical`.Name AS Vertical, `process`.`Name` AS PROCESS, `client`.`Client_Name` AS CLIENT, `designation`.Name AS Designation, `designation`.`grades` AS Grade, `Report`.`Companyemployid` AS `Reporting Manager ID`, CONCAT(`Report`.`FirstName`," ",`Report`.`LastName`) AS `Reporting Manager` FROM `employ`
JOIN `designation` ON (`employ`.`DesignationID` = `designation`.`DesignationID`)
JOIN `location` ON (`employ`.`LocationID` = `location`. `LocationID`)
JOIN employprocess ON (employ.employid = employprocess.employid)
JOIN PROCESS ON (employprocess.processid = process.processid)
JOIN `vertical` ON (`process`.`VerticalID` = `vertical`.`VerticalID`)
JOIN shift ON (shift.shiftID = employprocess.shiftid)
JOIN CLIENT ON (process.clientid = client.clientid)
JOIN employ AS Report ON (employ.`Primary_Lead_ID` = Report.`EmployID`)
WHERE employprocess.`PrimaryProcess` = 1 AND employ.`RelieveFlag` = 0 AND designation.grades IN (1, 2)
ORDER BY Employ.Employid

/* Get Team lead with client and process */

SELECT DISTINCT (`employ`.`CompanyEmployID`) AS EmployeeID, `employ`.`FirstName`, `employ`.`LastName`, `employ`.`Email`, `location`.`Name` AS Location, `shift`.`ShiftCode` AS Shift, `vertical`.Name AS Vertical, `process`.`Name` AS PROCESS, `client`.`Client_Name` AS CLIENT, `designation`.Name AS Designation, `designation`.`grades` AS Grade FROM `employ`
JOIN `designation` ON (`employ`.`DesignationID` = `designation`.`DesignationID`)
JOIN `location` ON (`employ`.`LocationID` = `location`. `LocationID`)
JOIN processlead ON (employ.employid = processlead.employid)
JOIN PROCESS ON (processlead.processid = process.processid)
JOIN `vertical` ON (`process`.`VerticalID` = `vertical`.`VerticalID`)
JOIN shift ON (shift.shiftID = processlead.shiftid)
JOIN CLIENT ON (process.clientid = client.clientid)
WHERE processlead.`PrimaryProcess` = 1 AND `RelieveFlag` = 0 AND designation.grades IN (3)
ORDER BY Employ.Employid