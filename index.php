<?php
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}
header('X-Frame-Options: SAMEORIGIN');
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Theorem Rhythm</title>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
	<meta http-equiv="X-Frame-Options" content="deny">
	<link rel="icon" href="resources/images/theorem-favicon.png">
    <!-- <script type="text/javascript" src="https://cdn.creativesontap.net/extjs/ext-all1.js?v=1.2.4"></script>
	<script src="https://cdn.creativesontap.net/extjs/ext-theme-neptune.js?v=1.2.4"></script>
	<link rel="stylesheet" href="https://cdn.creativesontap.net/extjs/resources/ext-theme-neptune/ext-theme-neptune-all.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.creativesontap.net/extjs/src/BoxSelect.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/style.css" />
    <script type="text/javascript" src="https://cdn.creativesontap.net/extjs/src/BoxSelect.js?v=1.2.4"></script> -->
	
	<script type="text/javascript" src="extjs/ext-all.js?v=3.5"></script>
	<script src="extjs/ext-theme-neptune.js?v=3.5"></script>
	<link rel="stylesheet" href="extjs/resources/ext-theme-neptune/ext-theme-neptune-all.css">
    <link rel="stylesheet" type="text/css" href="extjs/src/BoxSelect.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/style.css" />
    <script type="text/javascript" src="extjs/src/BoxSelect.js?v=3.5"></script>  
	
	<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/extjs/4.2.1/ext-all.js?v=1.2.4"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/extjs/4.2.1/ext-theme-neptune.js?v=1.2.4"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/extjs/4.2.1/resources/ext-theme-neptune/ext-theme-neptune-all.css">
    <link rel="stylesheet" type="text/css" href="extjs/src/BoxSelect.css" />
    <link rel="stylesheet" type="text/css" href="resources/css/style.css" />
    <script type="text/javascript" src="extjs/src/BoxSelect.js?v=1.2.4"></script>-->
	
	<script type="text/javascript">
		window.open('', '_self', '');
	</script>
    <script type="text/javascript" src="app.js?v=3.5"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript">
		$(function() {
			$(this).bind("contextmenu", function(e) {
				e.preventDefault();
			});
		});
	</script>	
</head>
<body>
</body>
</html>