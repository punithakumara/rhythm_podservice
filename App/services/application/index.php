<?php
error_reporting(E_ALL);

// Insert the path where you unpacked log4php
require_once(.BASE_URL."services/log4php/Logger.php");

// Tell log4php to use our configuration file.
Logger::configure(.BASE_URL."services/config.xml");

// Module 1
spl_autoload_register(function ($class_name) {
    include_once .BASE_URL."services/module1/".$class_name . ".php";
});

include_once .BASE_URL."services/TestLog.php";
//header("Location:".BASE_URL."services/index.php/Authenticate/logout");

$val = 10;
$logger = Logger::getLogger("customLogger");

$logger->info("Inside index.php page.");

$logger->trace("This is a TRACE level message.");
$logger->debug("This is a DEBUG level message. The value of variable 'val' is " . $val);
$logger->info("This is an INFO level message. Demostrating INFO level logger.");
$logger->warn("This is a WARN level message. Too many unnecessary iterations happenning.");
$logger->error("This is a ERROR level message. Dividing by zero will throw ArithmeticException.");
$logger->fatal("This is a FATAL level message. exit() command in middle of script, will abruptly end the process.");

$testLog = new TestLog();

$testLog->profession();

$parent  = new Myparent(); // ParentClass1
$child   = new Child(); // ChildClass1  (Which inherits ParentClass1)
$hobbies = new Hobbies(); // IndependentClass1

$child->getFatherName();
$child->getMotherName();
$parent->profession();

$child->myName();

$hobbies->indoorGame();
$hobbies->outdoorGame();



// Module 2
spl_autoload_register(function ($class_name) {
    include_once .BASE_URL."services/module2/".$class_name . ".php";
});

$vehicle  = new Vehicle(); // ParentClass1
$trip     = new Trip(); // ChildClass2  (Which inherits ParentClass2)
$activity = new Activity(); // __IndependentClass2

$trip->getCar();
$trip->getBike();
$trip->getBicycle();
$trip->myTrip();
$activity->lake();
$activity->cycling();


?>
