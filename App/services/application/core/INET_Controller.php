<?php

require APPPATH.'/libraries/REST_Controller.php';

class INET_Controller extends REST_Controller
{
	function __construct()
    {
        parent::__construct();
    }
    function waterMark($retVal)
    {
    	if(is_array($retVal))
        {
            return array_merge($retVal, array(array('')), array(array('')), array(array('', '', "Theorem Inc. Confidential(Not To Be Shared)")));
        }
    }   

    function userLogs($logmsg='')
    {
        // $date = date_default_timezone_set('Asia/Kolkata');
        // $filename = 'LOG_'.date('dmY');
        // $myfile = fopen('userlogs/'.$filename,'a')or die("Unable to open log file $filename!");
        // $txt = date('h:i:sA').": $logmsg\n";
        // fwrite($myfile, $txt);
        // fclose($myfile);
        $day = date('w');
        $week_start = date('d-m-Y', strtotime('-'.$day.' days'));
        $week_end = date('d-m-Y', strtotime('+'.(6-$day).' days'));
        $date = date_default_timezone_set('Asia/Kolkata');
        $filename = 'LOG_'.$week_start.'-'.$week_end;
        $myfile = fopen('userlogs/'.$filename,'a')or die("Unable to open log file $filename!");
        $txt = date('d-m-Y h:i:sA').": $logmsg\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }	
}
?>