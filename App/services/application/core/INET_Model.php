<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class INET_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public $table;
	public $insertId;
	/*
	 * getValues : returns all the fileds value based on conditon
	 * @Param
	 * --------------
	 * All the fileds
	 * limit                 limits the number of returned records
	 * offset                how many records to bypass before returning a record (limit required)
	 * sortBy                determines which column the sort takes place
	 * sortDirection         (asc, desc) sort ascending or descending (sortBy required)
	 */
	public function getValues($options = array(),$tablename="")
	{
		if($tablename=="")
		$tablename = $this->table;

		$options = $this->_default(array('sortDirection' => 'asc'), $options);
		$qualificationArray = $this->db->list_fields($tablename);

		foreach($qualificationArray as $qualifier)	//If option array given then adding where clause
		if(isset($options[$qualifier])) $this->db->where($qualifier,$options[$qualifier]);
	  
		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if($filterField != '') {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			if($filterWhere != '') {
				$this->db->where('('.$filterWhere.')');
			}
		}
	  
		// If select particular fields
		if(isset($options['select']) && ($options['select'] != '')) {
			$this->db->select($options['select']);
		}
	  
		// If limit / offset are declared
		if(isset($options['limit']) && isset($options['offset']))
		$this->db->limit($options['limit'], $options['offset']);
		else if(isset($options['limit']))
		$this->db->limit($options['limit']);
			
		if(isset($options['sortBy']))  // sort
		$this->db->order_by($options['sortBy'], $options['sortDirection']);

		$query = $this->db->get($tablename);
		if($query->num_rows() == 0)
		return false;
	  
		return $query->result_array();
	}

	/*
	 * Total records count
	 */

	public function getTotalRecordCount($options = array(),$tablename="")
	{
		if($tablename=="")
		$tablename = $this->table;

		$options = $this->_default(array('sortDirection' => 'asc'), $options);
		$qualificationArray = $this->db->list_fields($tablename);

		foreach($qualificationArray as $qualifier)	//If option array given then adding where clause
		if(isset($options[$qualifier])) $this->db->where($qualifier,$options[$qualifier]);

		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if($filterField != '') {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			if($filterWhere != '') {
				$this->db->where('('.$filterWhere.')');
			}
		}

		$query = $this->db->get($tablename);

		return $query->num_rows();
	}

	/*
	 * Inserting Data into table return last inserted ID
	 * @param:
	 * -----------
	 * $data = array()
	 * $tablename (Optional) Note: If tablename not given then set the table name in  _getTableNames()
	 */
	public function insert($data = array(),$tablename="")
	{
		if($tablename=="")
		$tablename = $this->table;
			
		$qualificationArray = $this->db->list_fields($tablename);

		if($this->_chk_primiary_key($tablename))
		$fields = array_shift($qualificationArray);
			
		foreach($qualificationArray as $qualifier)
		if(isset($data[$qualifier])) $this->db->set($qualifier, trim($data[$qualifier]));

		$this->db->insert($tablename);
	  
		$this->insertId = $this->db->insert_id();
	  
	  
		return $this->db->affected_rows();
	}

	/*
	 *  Recently Inserted Row Id
	 */

	public function getInsertId()
	{
		return $this->insertId;
	}


	/*
	 * Update the Table based on the condition
	 * @param:
	 * -----------
	 * $data = array()
	 * $conditions = array() Required
	 * $tablename (Optional) Note: If tablename not given then set the table name in  _getTableNames()
	 */
	public function update($data = array(),$conditions = array(),$tablename="")
	{
		if($tablename=="")
		$tablename = $this->table;

		if(count($conditions)>0)
		{
			$qualificationArray = $this->db->list_fields($tablename);
			if($this->_chk_primiary_key($tablename))
			$fields = array_shift($qualificationArray);
			if(is_array($qualificationArray))
			{
				foreach($qualificationArray as $qualifier)
				if(isset($data[$qualifier])) $this->db->set($qualifier, trim($data[$qualifier]));
			}
			$this->db->where($conditions);
			$this->db->update($tablename);
			return $this->db->affected_rows();
		}
		else
		{
			echo "Error: Condition parameter not given";
			exit;
		}
	}

	/*
	 * Delete the Table based on the condition
	 * @param:
	 * -----------
	 * $conditions = array() Required
	 * $tablename (Optional) Note: If tablename not given then set the table name in  _getTableNames()
	 */
	public function delete($conditions=array(), $tablename="")
	{
		if(count($conditions)>0)
		{
			if($tablename=="")
			$tablename = $this->table;
				
			$this->db->where($conditions);
			$this->db->delete($tablename);

			return $this->db->affected_rows();
		}
		else
		{
			return "Error: Condition parameter not given";
		}
	}


	/*
	 *  Upload files
	 */

	function upload_files($data, $upload_path = "", $allowed_types = "*", $max_size = 0, $max_width = 0, $max_height = 0)
	{
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;
		$config['max_width'] = $max_width;
		$config['max_height'] = $max_height;
		$this->load->library('upload', $config);

		foreach ($data as $key => $value) {
			if (!empty($value['tmp_name'])) {

				if (!$this->upload->do_upload($key)) {

					$errors = $this->upload->display_errors();
					return array('upload_error' => 'Upload file Error. '.$errors, 'upload_success' => false);

				} else {
					return array('upload_success' => true);
				}
			}
		}
	}


	/* _default method combines the options array with a set of defaults giving the values in the options array priority. */
	function _default($defaults, $options)
	{
		return array_merge($defaults, $options);
	}

	function _chk_primiary_key($tablename)
	{
		$fields = $this->db->field_data($tablename);
		foreach ($fields as $field)
		{
			if($field->primary_key ==1)
			{
				return TRUE;
				break;
			}
		}
		return FALSE;
	}

	/* Assigning Table Names based on medel class */
	function _getTableNames($model_class_name)
	{
		switch($model_class_name)
		{
			case 'Client_model':
				$this->table = 'process';
				break;
			case 'Topics_model':
				$this->table = 'topics';
				break;
			case 'Users_model':
				$this->table = 'users';
				break;
			case 'Participate_model':
				$this->table = 'participates';
		}
	}
	/**
	 * Get clients based on emp_id and grade
	 */
	function getClients($EmployID = "")
	{
		$empData = $this->session->userdata('user_data');
		if(isset($empData['employee_id']) != "")
		$EmployID = $empData['employee_id'];
		if(isset($empData['Grade']) != "") 
		{
			$grade = $empData['Grade'];
			if($grade==1 || $grade==2)
			{
				$sql = "SELECT client_id FROM employee_client WHERE employee_id =".$EmployID." AND grades < 3";
			}
			if($grade >=3 && $grade < 4 )
			{
				$sql = "SELECT client AS client_id FROM wor WHERE team_lead LIKE '%".$EmployID."%'";
			}
			if($grade==4)
			{
				$sql = "SELECT client AS client_id FROM wor WHERE associate_manager LIKE '%".$EmployID."%'";
			}
			if($grade>=5)
			{
				$sql = "SELECT client_id FROM client WHERE status = 1";
			}
		}
		
		if(isset($sql))
		$result_select = $this->db->query($sql);
		$processArray = array();
		if(isset($result_select) && count($result_select->result_array())!=0)
		{
			foreach($result_select->result_array() as $key => $val)
			array_push($processArray, $val['client_id']);
		}
		return $processArray;
	}
	
	/**
	 * Get all employee clients based on emp_id and grade
	 */
	function getAllEmployee($comboClient=0)
	{
		$clientArray = $this->getClients();
		
		if(is_array($clientArray))
			$clientArray = implode(',', $clientArray);
		
		$empData = $this->session->userdata('user_data');
		$grade = $empData['Grade'];
		$employArray = array();

		if($clientArray!="")
		{
			$where = "";
			
			$sql = "(SELECT employee_id FROM employee_client WHERE client_id IN (".$clientArray.") ";
			
			if($grade > 0 && $grade < 3)
			{
				$sql .= " AND employee_client.grades < 3)";
			}
			else if($grade > 2 && $grade < 4)
			{
				$sql .= " AND employee_client.grades < 4)";
			}
			else if($grade >= 4)
			{
				$sql .= " AND employee_client.grades >= 1)";
			}
				
			if(isset($sql))
			$result_select = $this->db->query($sql);
			if(isset($result_select) && count($result_select->result_array())!=0)
			{
				foreach($result_select->result_array() as $key => $val)
				array_push($employArray, $val['employee_id']);
			}
		}
		
		return $employArray;
	}

	function getClientProcess($comboClient)
	{
		$query = $this->db->select('ProcessID');
		$this->db->from("Process");
		$this->db->where('ClientID', $comboClient);
		$totQuery = $this->db->get();
		$processArray = array();
		if(isset($totQuery) && count($totQuery->result_array())!=0)
		{
			foreach($totQuery->result_array() as $key => $val)
			array_push($processArray, $val['ProcessID']);
		}
		return $processArray;
	}
	
	function getClientProcessForLocation($comboClient)
	{
		$comboClient = str_replace("'", "", $comboClient);
		$query = $this->db->select('ProcessID');
		$this->db->from("Process");
		$this->db->where_in('ClientID', explode(",",$comboClient));
		$totQuery = $this->db->get();
		$processArray = array();
		if(isset($totQuery) && count($totQuery->result_array())!=0)
		{
			foreach($totQuery->result_array() as $key => $val)
				array_push($processArray, $val['ProcessID']);
		}
		
		return $processArray;
	}
	
	

	/**
	 * Get vertical based on emp_id and grade
	 */
	function getVertical()
	{
		$empData = $this->session->userdata('user_data');
		$EmployID = $empData['EmployID'];
		$grade = $empData['Grade'];
		$processArray = $this->getProcess();
		if(count($processArray)!=0)
		{
			if($grade <= 4)
			$sql = "SELECT VerticalID FROM Process WHERE ProcessID IN (".implode(',', $processArray).")";
			if($grade == 5 || $grade == 6 )
			$sql = "SELECT verticalmanager.VerticalID FROM verticalmanager JOIN process ON verticalmanager.VerticalID = process.VerticalID WHERE EmployID =".$EmployID;
		}
		if(isset($sql))
		$result_select = $this->db->query($sql);
		$verticalArray = array();
		if(isset($result_select) && count($result_select->result_array())!=0)
		{
			foreach($result_select->result_array() as $key => $val)
			array_push($verticalArray, $val['VerticalID']);
		}
		return $verticalArray;
	}
	/**
	 * Get the employess under login user
	 */
	function getEmployeeList($EmployID, $employArray = array())
	{
		if(is_array($EmployID))
		$EmployID = implode(',', $EmployID);
		$select_query = "SELECT employee_id FROM employees WHERE primary_lead_id IN (".$EmployID.") AND status!=6 ORDER BY first_name";
		
		$result_select = $this->db->query($select_query);
		$employList = array();
		if(isset($result_select) && count($result_select->result_array())!=0)
		{
			foreach($result_select->result_array() as $key => $val)
			{
				array_push($employList, $val['employee_id']);
			}
			$employArray = array_merge($employArray, $employList);
			$employArray = $this->getEmployeeList($employList, $employArray);
		}
		return $employArray;

	}
	
	/**
		Process List of Employees EmployID and Grade as Parameter
	*/
	public function getEmployProcessList($EmployID,$grade) {
		if($grade < 5)
			$sql = "SELECT ProcessID FROM employprocess WHERE EmployID =".$EmployID;
   		if($grade == 5)
   			$sql = "SELECT ProcessID from Process where ProcessStatus = 0 AND VerticalID 
   					IN (SELECT VerticalID from verticalmanager WHERE EmployID=".$EmployID.")";
  		if(isset($sql))
  			$result_select = $this->db->query($sql);
  			$processArray = array();
  		if(isset($result_select) && count($result_select->result_array()) != 0) {
  			foreach($result_select->result_array() as $key => $val)
  				array_push($processArray, $val['ProcessID']);
  		}
  		if(empty($processArray)) {
  				if($grade >=6){
     			$sql = "(SELECT ep.ProcessID FROM employprocess ep JOIN PROCESS ON process.processid = ep.processID 
				WHERE process.processStatus = 0 AND ep.employID IN ((SELECT employId FROM employ WHERE Primary_Lead_ID = $EmployID))) ";      
      			$result_select = $this->db->query($sql);
      			if(isset($result_select) && count($result_select->result_array()) != 0) {
       				foreach($result_select->result_array() as $key => $val)
       					array_push($processArray, $val['ProcessID']);
       			}
     		}
     		if(empty($processArray)) {
		     		$sql = " SELECT ProcessID FROM PROCESS
		     				 JOIN `verticalmanager` ON `PROCESS`.`VerticalID`=verticalmanager.`VerticalID`
		     				 WHERE verticalmanager.`EmployID`='".$EmployID."' AND PROCESS.`ProcessStatus`=0"; 
		     		$result_select = $this->db->query($sql);
		     		if(isset($result_select) && count($result_select->result_array())!=0) {
		      			foreach($result_select->result_array() as $key => $val)
		      				array_push($processArray, $val['ProcessID']);
		      		}
       		}
       	} 
       	return $processArray;
 	}
	
	/**
	 * Get the all my reportees ProcessID
	 */
	function getReporteesProcessIdList($EmployID, $employnewArray = array())
	{
		
		$employList = array();
		$processArray = array();
		$employArray = $this->getEmployeeList($EmployID, $employArray = array());
		$empids = implode(",",$employArray);
		$EmployList = '';
		$EmployList1 = '';
		$EmployList2 = '';
		if(!empty($empids)){
			$EmployList = " AND ep.employID IN ($empids)";
		}
		$sql = "(SELECT DISTINCT ep.ProcessID FROM employprocess ep JOIN PROCESS ON process.processid = ep.processID 
							WHERE process.processStatus = 0  $EmployList ) ";
     			     
      			$result_select = $this->db->query($sql);
      			if(isset($result_select) && count($result_select->result_array()) != 0) {
       				foreach($result_select->result_array() as $key => $val)
       					array_push($processArray, $val['ProcessID']);
       			}
   
     		if(empty($processArray)) {
		     		$sql = " SELECT ProcessID FROM PROCESS
		     				 JOIN `verticalmanager` ON `PROCESS`.`VerticalID`=verticalmanager.`VerticalID`
		     				 WHERE verticalmanager.`EmployID`='".$EmployID."' AND PROCESS.`ProcessStatus`=0"; 
		     		$result_select = $this->db->query($sql);
		     		if(isset($result_select) && count($result_select->result_array())!=0) {
		      			foreach($result_select->result_array() as $key => $val)
		      				array_push($processArray, $val['ProcessID']);
		      		}
       		}
		return $processArray;

	}
	
	/**
	 * Insert the values to notification table
	 * $data = array(
	 * 					"Title"=>value,
	 * 					"Description"=>value,
	 * 					"SenderEmployee"=>value,
	 * 					"ReceiverEmployee"=>value,
	 *					"URL"=>value
	 * 				);
	 */
	function addNotification($data)
	{
		$retVal = false;
		if(is_array($data))
		{
			date_default_timezone_set('Asia/Kolkata');
			$data['NotificationDate']	= date('Y-m-d H:i:s');
			$retVal						= $this->insert($data, 'notification');
		}
		return $retVal;
	}
	/**
		Function for notification read
	*/
	function notificationRead($notificationID)
	{
		$retVal = false;
		if($notificationID!="")
		{
			$data['Read'] = 1;
			date_default_timezone_set('Asia/Kolkata');
			$data['ReadDate'] = date('Y-m-d H:i:s');
			$where['NotificationID'] = $notificationID;
			$retVal = $this->update($data, $where, 'notification');
		}
		return $retVal;
	}
	/**
	 * Function for get all the notification
	 */
	function notification($EmployID)
	{
		$option = array('ReceiverEmployee'=>$EmployID,'Read'=>0,'sortBy'=>'NotificationDate','sortDirection'=>'DESC');
		return $this->getValues($options, "notification");
	}
	/**
	 * Log Action for all module
	 */
	public function log_action($module, $action, $id='', $newRecord=array())
	{
		$empData  = $this->session->userdata('user_data');
		$EmployID = $empData['EmployID'];
		$message  = '';
		switch($action)
		{
			case 'add':
				{
					$message = "New record (".$id.") has been added on ".$module." module at ".date('Y-m-d H:i:s');
					break;
				}
			case 'update':
				{
					/*print "<pre>";
					 print_r($newRecord[2]);*/

					$getDiff = $this->db->query("Select * from ".$module." where ".$newRecord[0]." = '".$newRecord[1]."'");
					$resultSet = $getDiff->result_array();

					/*print "<pre>";
					 print_r($resultSet[0]);*/

					$arrDif=array();

					$i=0;
					foreach($newRecord[2] as $key=>$value)
					{
						if(!in_array($value, $resultSet[0]))
						{
							$arrDif[$key]=$value;
							$i++;
						}
					}
					$message = "The record ".$id." has been updated on ".$module." module at ".date('Y-m-d H:i:s');
					if(is_array($resultSet)){
						$message .=" Changes: (";
						foreach($arrDif as $k=>$v)
						{
							$message .= " Old value on ".$k." is (".$resultSet[0][$k].") And new value is ".$newRecord[2][$k];
						}
						$message .=")";
					}
					break;
				}
			default:
				{
					break;
				}
		}

		$query = $this->db->select('*')->from('action_log')->where("employid ",$EmployID)->where("date ",date('Y-m-d'));
		$query = $this->db->get();
		if($query->num_rows() != 0)
		{
			$result = $query->result_array();
			$previousAction = array();
			if(count($result)!=0)
			{
				$previousAction[] = json_decode($result[0]['actions'], true);
			}
			if(is_array($previousAction))
			$previousAction[]=array('action'=>$message);
			else
			$previousAction[] = array('action'=>$message);

			$this->db->query("Update action_log set actions = '".json_encode($previousAction)."' where employid = '".$EmployID."' AND date = '".date('Y-m-d')."'");
		} else {
			$logMessage = array('employid'=> $EmployID,'date'=>date('Y-m-d'),'actions'=>json_encode(array('action'=>$message)));
			$retVal = $this->insert($logMessage, 'action_log');
		}

	}
	/**
	 * 
	 */
	public function dateRange($monthRange)
	{
		$date = array();
		if($monthRange!=0)
		{
			$date['StartDate'] = date("Y-m-d", strtotime('first day of this month', strtotime("-".$monthRange ."months")));
			$date['EndDate'] = date("Y-m-d", strtotime('last day of this month', strtotime("-1 months")));
		}
		else
		{
			$date['StartDate'] = date("Y-m-d", strtotime('first day of this month'));
			$date['EndDate'] = date("Y-m-d");
		}
		return $date;
	}
	
	/*********** Convert Multidimention array to Single ****/
	public function arrayFilter($array = '', $column = ''){
		$EmpArray = array();
		if(!is_array($array))
			return array();
			
		foreach($array as $key => $value){
				$EmpArray[$key] = $value[$column];
		}
		return $EmpArray;
	}
	/******** Get Days Between Two Dates ******/
	
	function dateDiff($start, $end) {
		$start_ts = strtotime($start);
		$end_ts = strtotime($end);
		$diff = $end_ts - $start_ts;
		return round($diff / 86400);
	}

	public function getTeamLeads()
    {
        $getProcess = $this->getProcess();
        $sql        = "SELECT EmployID FROM employprocess WHERE ProcessID IN (". implode(',', $getProcess).") AND Grades > 2 AND Grades < 4 GROUP BY EmployID";
        if(isset($sql)) {
            $result_select = $this->db->query($sql);
            $leadsArray    = array();
            if(isset($result_select) && count($result_select->result_array())!=0) {
                foreach($result_select->result_array() as $key => $val)
                    array_push($leadsArray, $val['EmployID']);
            }
        }
        return $leadsArray;
    }
    public function getTeamManagers()
    {
        $getProcess = $this->getProcess();
        $sql        = "SELECT EmployID FROM employprocess WHERE ProcessID IN (". implode(',', $getProcess).") AND Grades >= 4 GROUP BY EmployID";
        if(isset($sql)) {
            $result_select   = $this->db->query($sql);
            $managerArray    = array();
            if(isset($result_select) && count($result_select->result_array())!=0) {
                foreach($result_select->result_array() as $key => $val)
                    array_push($managerArray, $val['EmployID']);
            }
        }
        return $managerArray;
    }
  
  	/**
		Function To Get Vertical Managers For Grade 5 and Above
  	*/
  	public function getVerticalManagers()
  	{
  		$empData	= $this->session->userdata('user_data');
  		if(isset($empData['Grade']) && $empData['Grade'] > 5) {
  			$sql = " SELECT EmployID FROM employ WHERE  employ.relieveflag	= 0
    				 AND employ.primary_lead_id	= ".$empData['EmployID']."";
    		if(isset($sql)) {
    			$result_select   = $this->db->query($sql);
    			$verticalmanager = array();
    			if(isset($result_select) && count($result_select->result_array())!=0) {
    				foreach($result_select->result_array() as $key => $val)
                    array_push($verticalmanager, $val['EmployID']);
            	}
        	}
  		}
  		return $verticalmanager;
  	}
  	/**
  	 Function To Display Common Date Control
  	*/
  	 public function commonDateSetting($dateParam = "") {
  	 	if(isset($dateParam) && !empty($dateParam)){  	 	 	 	
  	 		if($dateParam == '00-00-0000'){
  	 			return "";
  	 		}
  	 		return date(COMMON_DATE_CONTROL, strtotime($dateParam));  	 		
  	 	}
  	 	 else {
  	 		return $dateParam;
  	 	}
  	 }
  	 /**
  	 Function To get Client for the Login user
  	 */

  	 public function getLoginClient() {
  	 	$process		= $this->getProcess();
  	 	if(isset($process) && !empty($process)) { 
	  	 	$processdata	= implode(',', $process);
	  	 	$sql			= " SELECT ClientID FROM process WHERE ProcessID IN ($processdata)";
	  	 	$res			= $this->db->query($sql);
	  	 	$clientcount	= $res->num_rows();
			$clientid		= array();
			if($clientcount > 0) {
				if(isset($res) && count($res->result_array()) != 0) {
					foreach($res->result_array() as $key => $val)
					array_push($clientid, $val['ClientID']);
				}
				return $clientid;
			}
		}
  	 }

  	function userLogs($logmsg='')
    {
    	$day = date('w');
		$week_start = date('d-m-Y', strtotime('-'.$day.' days'));
		$week_end = date('d-m-Y', strtotime('+'.(6-$day).' days'));
        $date = date_default_timezone_set('Asia/Kolkata');
        $filename = 'LOG_'.$week_start.'-'.$week_end;
        $myfile = fopen('userlogs/'.$filename,'a')or die("Unable to open log file $filename!");
        $txt = date('d-m-Y h:i:sA').": $logmsg\n";
        fwrite($myfile, $txt);
        fclose($myfile);
    }
}