<!DOCTYPE html>
<html>
<head>
    <title>Client View</title>
<style>
body {
    width: 770px;
    margin: 10px auto;
    font-family: 'trebuchet MS', 'Lucida sans', Arial;
    font-size: 13px;
    color: #444;
}
table {
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
span{
float:left;
}
h3{
color:#157fcc;
}


#tabs {
	border-bottom: .8em solid #add2ed;
	margin: 0;
	padding: 0;
	height:14px;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
}
#tabs li { 
	display:inline; 
	border-top: .1em solid #add2ed;
	border-left: .1em solid #add2ed;
	border-right: .1em solid #add2ed;
}
#tabs li a {
	text-decoration: none;
	margin-bottom:0px;
	color: #000;
}
#page1 #tabs li#tab1 a,#page2 #tabs li#tab1 a,#page3 #tabs li#tab1 a{
	padding: 0.15em 1em;
	background-color: #add2ed;
	border-radius:2px !important;
	-webkit-border-radius:2px !important;
	color: #157fcc;
	font-size: 18px;
}
	#watermark {
		display: block;
		position: fixed;
		top: 30%;
		left: 105px;
		transform: rotate(-45deg);
		transform-origin: 50% 50%;
		opacity: .2;
		font-size: 40px;
		color: #666;
		width: 480px;
		text-align: center;
	}
</style>
</head>

<body>
	<div id="watermark"> Theorem Inc. Confidential(Not To Be Shared) </div>
<?php
$LogoPath = $this->config->item('REPOSITORY_PATH').'resources/images/TheoremLogo.jpg';
?>
<div align="left">	
	<img src="<?php echo $LogoPath?>" alt="" width="150px" height="25px"/>
</div>
<?php 
if(isset($Logo)){
$this->config->load('admin_config');
$uploadPath = $this->config->item('REPOSITORY_PATH').'resources/uploads/client/';
if (!is_file($uploadPath.$Logo))
{
	$uploadPath = $this->config->item('REPOSITORY_PATH').'resources/images/';
	$Logo = 'nologo.png';
}
	?>

<div align="center">	
	<img src="<?php echo $uploadPath.$Logo?>" alt="" width="100px" height="33px"/>
</div>	<br></br>
<?php }
?>
<table>
<tr><td>
<?php

$result = $clientDescription[0];
$num    = count($result);
$i      = 0;
if (isset($vertDescription)) {
    $verticals = $vertDescription;
    
    //print_r($verticals);
    $numvertical = count($verticals);
}
?>
<table>
	
	<?php
	foreach ($result as $key => $val) {
            
            if (0 == $i % 2) {
?>
		       <tr>
		<?php
                $open = true;
            }
?>
<?php  if ($key == 'ClientName') {?>
    	<td width="130px"><b><?php
    	
		   $key = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $key);    
            echo trim($key);
             ?>
            </b></td><td><h3><?php
            echo $val;
?></h3></td>
<?php
            }
            else
            {?>
            	<td width="130px;"><b><?php
			$key = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $key);    
            echo trim($key);
            ?>
            </b></td><td height="20px;"><?php
            echo $val;
?></td>
           <?php  }
            ?>

							    <?php
            if (2 == $i % 2) {
?>
	        </tr>
	        <?php
                $open = false;
            }
            
            $i++;
            

			
        }
        if ($open)
            echo '</tr>';
?>
</table>

</td></tr>
</table>
<br></br>
<br></br>
<div id="page1">
<ul id="tabs">
<li id="tab1"><a href="">Vertical Description</a></li>
</ul>
</div>
<br></br><br></br>
<?php
if (isset($vertDescription)) {
?>
<!--<div><h2>Vertical Description</h2></div>
--><?php
    
    for ($j = 0; $j < count($verticals); $j++) {
        $i    = 0;
        $open = false;
?>	
				
				<table border="1" cellpadding="0" cellspacing="0" width="750px">
				<tr bgcolor="#add2ed"><td colspan ='4'><b><?php
        echo $verticalNames[$j];
?><b></b></td></tr>
				<?php
				
		$verticals[$j][0] = array_swap_assoc('Resources_FTE', 'Total_ResourcesCount', $verticals[$j][0]);
        $verticals[$j][0] = array_swap_assoc('Resources_FTE', 'Resources_HC', $verticals[$j][0]);
        $verticals[$j][0] = array_swap_assoc('Resources_FTE', 'Shifts', $verticals[$j][0]);
        $verticals[$j][0] = array_swap_assoc('Resources_HC', 'billable_items', $verticals[$j][0]);
        $verticals[$j][0]['TotalBufferCount'] = $verticals[$j][0]['buffer_items'];
		unset($verticals[$j][0]['Resources_HRS']);
        unset($verticals[$j][0]['ApprovedBufferCount']);
        unset($verticals[$j][0]['FTEContract']);
        unset($verticals[$j][0]['buffer_items']);
        $k = 0;
        $n = 0;
    
        foreach ($verticals[$j][0] as $kvert => $vert) {
            
?>
							<?php
        	if ($kvert == 'Resources_FTE') {
                $kvert = '';
                $vert ='';
            }
            if ($kvert == 'Total_ResourcesCount') {
                $kvert = 'Total Resources';
            }
            if ($kvert == 'Resources_HC') {
                $kvert = 'Total # Approved Resource';
            }
            if ($kvert == 'TotalBufferCount') {
                $kvert = 'Buffer Details';                
            }
            if ($kvert == 'billable_items') {
                $kvert = 'Billable Details';
                
            }
            if ($kvert == 'Tasks') {
                $kvert = 'Process and Tasks';
            }
            if ($kvert == 'Technologies') {
                $kvert = 'Tools and Technologies';
                
            }
            if (0 == $i % 2) {
?>
		        						<tr>
								    <?php
                $open = true;
            }
?>
    							<td style="padding-left:2px;"><?php
            echo $kvert;
?></td><td style="padding-left:4px;"><?php
            echo $vert;
?></td>

							    <?php
            if (2 == $i % 2) {
?>
							        </tr>
							        <?php
                $open = false;
            }
            
            $i++;
            
?>
						<?php
            
?>
					  		<?php //}
?>
					  		
					  		
						<?php
            
?>
				<?php
        }
        if ($open)
            echo '</tr>';
?>
			</table><br/>
		
	<?php
    }
}
?>

<?php
if (count($internalprocess['rows']) >= 1) {
?>
<div id="page2">
<ul id="tabs">
<li id="tab1"><a href="">Internal Process Contacts</a></li>
</ul>
</div>
<br></br><br></br>
<table border='1' width ="750px">	
<?php
    $columnsHead = array_keys($internalprocess['rows'][0]);
    $columnsHead = swapByValues($columnsHead, 'email', 'Vertical');
    
?><tr bgcolor="#f5f5f5">
	<?php
    for ($ip = 0; $ip < count($columnsHead); $ip++) {
        unset($columnsHead[6]);
        $columnsHead[1] = 'Contact Name';
        $columnsHead[4] = 'Email';
        $columnsHead[5] = 'Contact Number';
        echo '<td align="center">' . $columnsHead[$ip] . '</td>';
    }
?>	
	</tr>
	<?php
    $InProcessData = subval_sort($internalprocess['rows'], 'Designation', 'asc');
    //print_r($InProcessData);
    foreach ($InProcessData as $kip => $intprocess) {
?>
		<tr>
				<td align="center"><?php
        echo $intprocess['EmployID'];
?></td>
				<td align="center"><?php
        echo $intprocess['FirstName'];
?></td>
				<td align="center"><?php
        echo $intprocess['Designation'];
?></td>
				<td align="center"><?php
        echo $intprocess['Vertical'];
?></td>
				<td align="center"><?php
        echo $intprocess['email'];
?></td>
				<td align="center"><?php
        echo $intprocess['mobile'];
?></td>
			
			 </tr>
		
	<?php
    }
?>
</table>	
	
<?php
}
?>
<?php
if (count($clientContacts['rows']) > 0) {
?>
<br></br><br></br>
<div id="page3">
<ul id="tabs">
<li id="tab1"><a href="">Client Contacts</a></li>
</ul>
</div>
<br></br><br></br>
<table border='1' width ="750px">	
<?php
   
    $columnscHead = array_keys($clientContacts['rows'][0]);
    $columnsHeadfinal = array_combine(array_keys(array_flip($columnscHead)), $columnscHead);
    if(!empty($columnsHeadfinal)){
    	unset($columnsHeadfinal['status']);
        unset($columnsHeadfinal['ProcessID']);
        unset($columnsHeadfinal['VerticalID']);
        unset($columnsHeadfinal['SlNo']);
        unset($columnsHeadfinal['Contact_Vertical']);
        unset($columnsHeadfinal['Contact_Process']);
        unset($columnsHeadfinal['Contact_ID']);
        unset($columnsHeadfinal['Client_ID']);
        unset($columnsHeadfinal['ContactIDs']);
      
    }
?><tr bgcolor="#f5f5f5">
	<?php
	$columnsHeadfinal = array_swap_assoc('Role', 'VerticalName', $columnsHeadfinal);
	foreach($columnsHeadfinal as $cl=>$cls) {
        //unset($columnscHead[7]);        
        $columnsHeadfinal['Contact_Name'] = 'Client Contact Name';
        $columnsHeadfinal['Contact_Title'] = 'Designation';
?>
  		<td align="center"><b><?php
        echo $columnsHeadfinal[$cl];
?></b></td>
	<?php
    }
 
?>	
	</tr>
	<?php
    $ClientContactData = subval_sort($clientContacts['rows'], 'Contact_ID', 'desc');
    foreach ($ClientContactData as $kclient => $vclient) {
?>
		<tr>
				<td align="center"><?php
        echo $vclient['Contact_Name'];
?></td>
				<td align="center"><?php
        echo $vclient['Contact_Title'];
?></td>
				<td align="center"><?php
        echo $vclient['Contact_Email'];
?></td>
				<td align="center"><?php
        echo $vclient['Contact_Phone'];
?></td>
				<td align="center"><?php
        echo $vclient['Contact_Mobile'];
?></td>
				<td align="center"><?php
        echo $vclient['VerticalName'];
?></td>
				<td align="center"><?php
        echo $vclient['ProcessName'];
?></td>
				<td align="center"><?php
        echo getRole($vclient['Role']);
?></td>			
		</tr>
		
	<?php
    }
?>
<?php
}
?>
</table>
<?php
function subval_sort($a, $subkey, $sort)
{
    foreach ($a as $k => $v) {
        $b[$k] = strtolower($v[$subkey]);
    }
    ($sort == "asc") ? asort($b) : rsort($b);
    foreach ($b as $key => $val) {
        $c[] = $a[$key];
    }
    return $c;
}
function swapByValues($values, $value1, $value2)
{
    $index1          = array_search($value1, $values);
    $index2          = array_search($value2, $values);
    $values[$index1] = $value2;
    $values[$index2] = $value1;
    return $values;
}
function array_swap_assoc($key1, $key2, $array) {
  $newArray = array ();
  foreach ($array as $key => $value) {
    if ($key == $key1) {
      $newArray[$key2] = $array[$key2];
    } elseif ($key == $key2) {
      $newArray[$key1] = $array[$key1];
    } else {
      $newArray[$key] = $value;
    }
  }
  return $newArray;
}
function getRole($role){
	switch($role){
		    		case 1: return "Day to Day in charge";
		    				break;
		    		case 2: return"Day to Day in charge(UK)";
		    				break;
		    		case 3: return "Day to Day in charge(US)";
		    				break;
		    		case 4: return "Main POC";
		    				break;
		    		default:"";
		    	}
}
?>
</body>
</html>