<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Rhythm Password Reset</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	
<script>
function validate(){
	var NewPwd=document.getElementById("NewPwd").value;
	var ConfirmPwd=document.getElementById("ConfirmPwd").value;
	var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*_])[a-zA-Z0-9!@#$%^&*_]{7,15}$/;  
	if(NewPwd.match(paswd)){
		//alert(" Password must contain 7 to 15 characters, at least one numeric digit and a special character");
	}else{
		alert(" Password must contain 7 to 15 characters, at least one numeric digit and a special character");
		return false;
	}
		
	if(NewPwd==ConfirmPwd){
		//document.getElementById("err_msg").innerHTML="Password matched";
		return true;
	}else{
		//document.getElementById("err_msg").innerHTML="Password Doesn't match";
		alert("Password Doesn't match");
		return false;
	}
	
}
</script>	
</head>
<body>
<form id="ResetPwdPage" method="post" action="<?php	echo site_url('index.php/authenticate/LdapConnect');?>" >
	<div id="container">
		<h1>Reset Password</h1>
	
		<div id="body">
			<p>Email</p>
			<p><input type="text" id="EmailId" name="EmailId" readonly=true value="<?php echo $emailid?>"/></p>
	
			<p>Enter New Password</p>
			<p><input type="password" id="NewPwd" name="NewPwd" /> &nbsp;[<b>Password Hint</b>:7 to 15 characters which contain at least one numeric digit and a special character]</p>
	
			<p>Confirm Password</p>
			<p><input type="password" id="ConfirmPwd" name="ConfirmPwd" /></p>		
	
			<p id="err_msg">&nbsp;</p>
			
			
			<p><input type="submit" value="Submit" onclick="return validate();"/>
		</div>
	
		
	</div>
</form>
</body>
</html>