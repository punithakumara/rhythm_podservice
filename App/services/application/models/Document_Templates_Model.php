<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_Templates_Model extends INET_Model
{
	public $empData;
	
	function __construct() 
	{
		parent::__construct();
		$this->config->load('admin_config');
		$this->empData = $this->input->cookie();
	}

	function build_query() 
	{
		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;
		
		$documentName = $this->input->get('Access') ? $this->input->get('Access') : '';
		$template_type= $this->input->get('template_type') ? $this->input->get('template_type') : '';
		
		if($documentName == 'All')
			$documentName ='("All","All managers")';
		else
			$documentName ='("'.$documentName.'")';			
		// here				
		$pro_ver = '';
		if($template_type == '3' && $this->empData['Grade']<5)
		{
			$pro_ver = " AND document_templates.verticalID IN(".$this->empData['VertId'].")";
		}
		
		$sql = "SELECT document_templates.template_id,document_templates.template_name,document_templates.uploaded_default_name,verticals.name as VerticalName, DATE_FORMAT(document_templates.created,'%d-%m-%Y') AS TemplateDate,document_templates.upload_by,document_templates.template_type,CONCAT(IFNULL(employees.first_name, ''), ' ', IFNULL(employees.last_name, '')) AS FirstName 
		FROM document_templates JOIN employees ON employees.employee_id = document_templates.upload_by LEFT JOIN verticals on document_templates.verticalID = verticals.vertical_id WHERE  1 = 1 ";
		if($template_type!='')
			$sql .= " and document_templates.template_type=".$template_type;
		if($pro_ver!='')
			$sql .= $pro_ver;
		
		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField)
				{
					if($filterField != '') {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') {					
				$sql .= " AND (".$filterWhere.")";					
			} 
		}
		
		return $sql;
	}

	function view() 
	{
		$options = array();
		$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;

		$sort = json_decode($this->input->get('sort'),true);
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$sql = $this->build_query();
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$sql = $this->build_query();
		if(isset($options['sortBy'])) 
		{
			$sortBy=$options['sortBy'];
			if($options['sortBy']=="TemplateDate")
			{
				$sortBy = "document_templates.created";
			}
			$sql .= " ORDER BY ".$sortBy." ".$options['sortDirection'];
		}
		
		if(isset($options['limit']) && isset($options['offset'])) {
			$sql .= " LIMIT ".$options['offset'].", ".$options['limit'];
		}
		else if(isset($options['limit'])) {
			$sql .= " LIMIT ".$options['limit'];
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		$data = $query->result_array();		
		foreach ($data as $key => $value) 
		{
			$data[$key]['TemplateDate']	= $this->commonDateSetting($value['TemplateDate']);
		}
		
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));			
		return $results;
	}

	function addTemplates( $data = '' ) 
	{
		// Load the configuration
		$this->ci->load->config('documents-config');
		
		$flag = 0;
		
		if(isset($_FILES['NewDocument']['name']))
		{
			$verticalid = "";
			if(isset($data['template_type']))
			{
				$accessLevel = $data['template_type'];
				if(isset($data['VerticalID']))
					$verticalid = $data['VerticalID'];
			}
			
			$exists = $this->checkForOverwrite($_FILES['NewDocument']['name'],$verticalid,$accessLevel);
			
			if($exists == 0){
				$flag = 1;
			}
			else
				$flag = 0;
		}
		if($data['template_type'] =='1')
		{
			if(is_dir($this->ci->config->item('company_templates_path')) == false) {
				$newcompDir = mkdir($this->ci->config->item('company_templates_path'), 0777, true);
			}
		}
		else if($data['template_type'] =='3'){	
			if(is_dir($this->ci->config->item('vertical_templates_path')) == false) {
				$newvertDir = mkdir($this->ci->config->item('vertical_templates_path'), 0777, true);
			}
		}

		// upload file to a directry			
		$file='';
		$id = isset($data['DocumentID']) ? $data['DocumentID'] : '';
		$id = isset($data['template_id']) ? $data['template_id'] : '';
		$verticalID = isset($data['VerticalID']) ? $data['VerticalID'] : NULL;

		$access = isset($data['Access']) ? $data['Access'] : '';
		$target_file ='';
		if(isset($_FILES['NewDocument']))
		{	
			$file = $_FILES['NewDocument']['name'];
			
			$target_file = $this->comDocTempCreate(basename($_FILES['NewDocument']['name']),$data['template_type']);

			move_uploaded_file($_FILES['NewDocument']['tmp_name'], $target_file);						
			//saving in to DB
		}
		
		$DocDataset = array(
			'uploaded_default_name'=>$file,
			'upload_by'=>$this->empData['employee_id'],
			'template_type'=>$data['template_type'],
			'verticalID'=>$verticalID,
			'template_access_level'=>"all",
			'status'=>'1'
		);

		$DocisDataset = array(
			
			'upload_by'=>$this->empData['employee_id'],
			'template_type'=>$data['template_type'],
			'verticalID'=>$verticalID,
			'template_access_level'=>"all",
			'status'=>'1'
		);
		
		if (!empty($id))
		{
			$where['DocumentID'] = $id;
			
			$retVal = $this->update($DocisDataset, $where, 'document_templates');
		}
		else
		{	
			$retVal = $this->insert($DocDataset, 'document_templates');
		}
		
		return $retVal;	
	}
	
	
	function getFolderName($vertId="")
	{
		$sql = "SELECT folder as Folder FROM verticals WHERE vertical_id =".$vertId;
		
		$query = $this->db->query($sql);
		
		$data = $query->result_array();	
		
		return $data[0]['Folder'];
	}
	
	function verDocCreate($verticalID,$uploadFile)
	{
		
		$getFolderName = $this->getFolderName($verticalID);

		if($getFolderName ==""){
			$getFolderName ="temp";
		}
		
		if(is_dir($this->ci->config->item('vertical_documents_path').$getFolderName) == false) {
			$newvertspDir = mkdir($this->ci->config->item('vertical_documents_path').$getFolderName, 0777, true);
		}
		
		$to_send = $this->ci->config->item('vertical_documents_path').$getFolderName."/".$uploadFile;
		
		return $to_send;
		
	}
	
	
	function comDocTempCreate($uploadFile,$templ_type='')
	{
		if($templ_type=='1')
		{
			$to_send = $this->ci->config->item('company_templates_path').$uploadFile;
		}
		else if($templ_type=='3')
		{
			$to_send = $this->ci->config->item('vertical_templates_path').$uploadFile;
		}
		return $to_send;
		
	}
	
	
	public function template_dele($data)
	{
		$options = array();
		$options['template_id'] 	= $data['temp_id'];
		if($data['template_type'] == "1"){
			$retVal = $this->delete($options, 'document_templates');
			$to_remove = $this->ci->config->item('company_templates_path').$data['uploaded_default_name'];
			if (is_file($to_remove))
			{
				unlink($to_remove);
			}
		}
		if($data['template_type'] == "3"){
			$retVal = $this->delete($options, 'document_templates');
			$to_remove = $this->ci->config->item('vertical_templates_path').$data['uploaded_default_name'];
			if (is_file($to_remove))
			{
				unlink($to_remove);

				if  (count(scandir(dirname ( $to_remove ))) == 2) 
				{
					rmdir(dirname ( $to_remove ))  ;
				}
			}
		}
		return $retVal;
	}
	
	
	function checkForOverwrite($uploadname,$vid,$access)
	{
		$retVal = 0;
		$getFolder = "";
		$sql = " SELECT template_id,uploaded_default_name as uploadName,template_type as doctype,VerticalID FROM document_templates WHERE uploaded_default_name = '".$uploadname."'";
		
		if($vid != "")
		{
			$sql.=" AND verticalID = ".$vid;
			$getFolderName = $this->getFolderName($vid);	
			if($getFolderName ==""){
				$getFolder ="temp";
			}
			else
			{
				$getFolder = $getFolderName;
			}
		}	
		if($access != "")
			$sql.=" AND template_type = '".$access."'";
		
		$query = $this->db->query($sql);			
		$data = $query->result_array();
		if(count($data)>=1)
		{
			if($data[0]['template_id'] != "")
			{
				$delData = array(
					'template_id' => $data[0]['template_id'],
					'uploadName' => $data[0]['uploadName'],
					'VerticalID' => $data[0]['VerticalID'],
					'doctype' => $data[0]['doctype'],
					'getFolder'=>$getFolder
				);
				$retVal = $this->deleDocs($delData);
			}
		}	
		
		return $retVal;
	}
	
	public function deleDocs($data)
	{
		$options = array();
		$options['template_id'] 	= $data['template_id'];
		if($data['doctype'] == "1" )
		{
			$retVal = $this->delete($options, 'document_templates');
			$to_remove = $this->ci->config->item('company_documents_path').$data['uploadName'];
			if (is_file($to_remove))
			{
				unlink($to_remove);
			}
		}
		if($data['doctype'] == "3")
		{
			$retVal = $this->delete($options, 'document_templates');
			$to_remove = $this->ci->config->item('vertical_documents_path').$data['getFolder']."/".$data['uploadName'];
			if (is_file($to_remove))
			{
				unlink($to_remove);
				
				if  (count(scandir(dirname ( $to_remove ))) == 2) {
					rmdir(dirname ( $to_remove ))  ;
				}
			}
		}
		return $retVal;
	}
}
?>