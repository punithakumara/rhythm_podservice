<?php defined('BASEPATH') OR exit('No direct script access allowed');

class NewIncidentLog_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
		$this->load->model(array('Rhythm_Model','Vertical_Model'));
	}

	function view() 
	{
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];
		
		$filter	= json_decode($this->input->get('filter'),true);
		
		$start = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$end = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
		$options = array();
		
		$options['filter']	= $filter;
		
		$sort = json_decode($this->input->get('sort'),true);
		
		$sortBy = $sort[0]['property'];
		$sortDirection = $sort[0]['direction'];

		$select = "SELECT id,error_id,error_category,error_subcategory,error_categories.category_name as cat_name,err_cat.category_name AS subcat_name,client.client_id, external_errors.wor_id, wor.work_order_id as wor_name, error_date,
		DATE_FORMAT(log_date,'%d-%m-%Y') AS log_date,qa_member,
		DATE_FORMAT(error_notified_date,'%d-%m-%Y') AS error_notified_date,DATE_FORMAT(correction_date,'%d-%m-%Y') AS correction_date,attachment,
		DATE_FORMAT(corrective_action_date,'%d-%m-%Y') AS corrective_action_date,production_member AS responsiblePerson,
		qa_member AS responsibleQA,external_errors.client_partner AS responsibleCP,
		error_categories.error_category_id,
		production_member,client.client_name AS Client,activity,problem_summary,
		units_impacted,impact,impact_business,impact_revenue,impact_relationship,rootcause_id,rootcause_analysis,correction, CONCAT(IFNULL(rb.`first_name`, ''),' ',IFNULL(rb.`last_name`, '')) AS reviewed_by, CONCAT(IFNULL(ab.`first_name`, ''),' ',IFNULL(ab.`last_name`, '')) AS approved_by, corrective_action,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS createdBy, external_errors.team_lead as team_leader, external_errors.associate_manager, CONCAT(IFNULL(etl.`first_name`, ''),' ',IFNULL(etl.`last_name`, '')) AS team_lead_name,
		CONCAT(IFNULL(eam.`first_name`, ''),' ',IFNULL(eam.`last_name`, '')) AS associate_manager_name,createdOn , missed_tat";
		$from = " FROM external_errors";
		$join = " LEFT JOIN error_categories ON external_errors.error_category = error_categories.error_category_id";
		$join .= "  LEFT JOIN error_categories AS err_cat ON external_errors.error_subcategory = err_cat.error_category_id";
		$join .= " LEFT JOIN client ON external_errors.client_id = client.client_id";
		$join .= " LEFT JOIN wor ON wor.wor_id = external_errors.wor_id";
		$join .= " LEFT JOIN employees ON external_errors.createdBy = employees.employee_id";
		$join .= " LEFT JOIN employees rb ON external_errors.reviewed_by = rb.employee_id";
		$join .= " LEFT JOIN employees ab ON external_errors.approved_by = ab.employee_id";
		$join .= " LEFT JOIN employees etl ON external_errors.team_lead = etl.employee_id";
		$join .= " LEFT JOIN employees eam ON external_errors.associate_manager = eam.employee_id";
		$where = " WHERE external_errors.status=0";
		
		if($empData['Grade']==1 || $empData['Grade']==2)
		{
			$where .= " AND (production_member LIKE '%".$EmpID."%' OR qa_member LIKE '%".$EmpID."%' OR domain_expert LIKE '%".$EmpID."%')";
		}
		if($empData['Grade']>2 && $empData['Grade']<5)
		{
			$where .= " AND (external_errors.client_id IN( ".$empData['ClientId'].") OR external_errors.createdBy = ".$empData['employee_id'].")";
		}
		
		$filterWhere = '';
		
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if($filterField != '') {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}

		}
		if($filterWhere !="")
		{
			$where .= " and (".$filterWhere.")";    			
		}
		
		if($sortBy!="" && $sortDirection!="")
		{
			$order = " ORDER BY ". $sortBy." ".$sortDirection;
		}
		else
		{
			$order = " ORDER BY external_errors.error_date DESC";
		}
		
		
		$query = $select . $from . $join . $where . $order;
		//echo $query; exit;
		$totQuery = $this->db->query($query);
		$totCount = $totQuery->num_rows();

		if($this->input->get('downloadCsv')!=1)
			$query .= " LIMIT $start, $end";

		$totQuery = $this->db->query($query);
		$data = $totQuery->result_array();
		
		foreach ($data as $key => $entry) 
		{
			$data[$key]['log_date']	= $this->commonDateSetting($entry['log_date']);
			$data[$key]['error_date']	= $this->commonDateSetting($entry['error_date']);
			$data[$key]['error_notified_date']	= $this->commonDateSetting($entry['error_notified_date']);
			$data[$key]['correction_date']	= $this->commonDateSetting($entry['correction_date']);
			$data[$key]['corrective_action_date']	= $this->commonDateSetting($entry['corrective_action_date']);

			$data[$key]['responsiblePerson']	= $this->getEmployeeNames($entry['responsiblePerson']);
			$data[$key]['responsibleQA']	= $this->getEmployeeNames($entry['responsibleQA']);
			//$data[$key]['responsibleExpert']	= $this->getEmployeeNames($entry['responsibleExpert']);
			$data[$key]['responsibleCP']	= $this->getEmployeeNames($entry['responsibleCP']);
			
			// $data[$key]['reviewed_by_name']	= $this->getEmployeeNames($entry['reviewed_by']);
			// $data[$key]['approved_by_name']	= $this->getEmployeeNames($entry['approved_by']);
			$data[$key]['impact_business']	= ucfirst($entry['impact_business']);
			$data[$key]['impact_relationship']	= ucfirst($entry['impact_relationship']);
		}
		
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));
		// debug($results);exit;
		return $results;
	}

	function DashboardData($LogType, $year, $month, $ClientID='', $groupvertical='')
	{
		$ClientID = str_replace("'", "", $ClientID);
		$empData      = $this->input->cookie();
		$cnd="";
		$searchKey    = $this->input->get('searchKey');
		
		/*if($empData['Grade']>2 && ($empData['Grade']<5))
		{
			$cnd= " AND c.client_id IN ('".$empData['ClientId']."')";
		}*/
		if (isset($searchKey) && $searchKey != 'empty') 
		{
			$cnd.=	" AND `client_name` LIKE '%" . $searchKey . "%'";
		}
		$sql_client="SELECT `c`.`client_name` as Name, `c`.`client_id` FROM `client` `c` WHERE `c`.`status` = 1  $cnd GROUP BY `c`.`client_id` ORDER BY `client_name` ASC	";
						//echo $sql_client;exit;
		$query = $this->db->query($sql_client);
		$result=$query->result_array();
		if ($query->num_rows() > 0) 
		{
			foreach ($result as $key => $val) 
			{
				$cid               = $val['client_id'];
				$sql_err="SELECT count(e.id) as err_cnt FROM external_errors e where  e.status=0 and e.client_id=".$cid;
				$sql_err.= " AND  DATE_FORMAT(error_date, '%Y')=".$year;
				$query_err = $this->db->query($sql_err);
				$result_err=$query_err->result_array();

				$sql_apr="SELECT count(a.id) as apr_cnt FROM incidentlog_appreciations a where a.status=0 and a.client_id=".$cid;
				$sql_apr.= " AND  DATE_FORMAT(appr_date, '%Y')=".$year;
				$query_apr = $this->db->query($sql_apr);
				$result_apr=$query_apr->result_array();

				$sql_esc="SELECT count(es.id) as esc_cnt FROM incidentlog_escalation es where es.status=0 and es.client_id=".$cid;
				$sql_esc.= " AND  DATE_FORMAT(escalation_date, '%Y')=".$year;
				$query_esc = $this->db->query($sql_esc);
				$result_esc=$query_esc->result_array();
				
				$result[$key]['Value1']    =  $result_err[0]['err_cnt']; 
				$result[$key]['Value2']    =  $result_apr[0]['apr_cnt'];
				$result[$key]['Value3']    =  $result_esc[0]['esc_cnt'];
			}
		}
		
		$result = array(
			'rows' => $result,
			'totalCount' => $query->num_rows()
		);
		
		return $result;
	}
	
	
	function view_appr() 
	{
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];
		
		$filter	= json_decode($this->input->get('filter'),true);
		
		$start = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$end = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
		$options = array();
		
		$options['filter']	= $filter;
		
		$sort = json_decode($this->input->get('sort'),true);
		
		$sortBy = $sort[0]['property'];
		// echo $sortBy;
		$sortDirection = $sort[0]['direction'];

		$select = "SELECT id,appr_id,client.client_id, appr_date,
		incidentlog_appreciations.wor_id, wor.work_order_id as wor_name, 
		DATE_FORMAT(log_date,'%d-%m-%Y') AS log_date,responsible_member,incidentlog_appreciations.team_lead as team_leader,
		client.client_name AS Client,incidentlog_appreciations.description,appr_by
		,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS createdBy,CONCAT(IFNULL(atl.`first_name`, ''),' ',IFNULL(atl.`last_name`, '')) AS team_lead_name,attachment,createdOn";
		$from = " FROM incidentlog_appreciations";
		$join = " LEFT JOIN client ON incidentlog_appreciations.client_id = client.client_id";
		$join .= " LEFT JOIN employees ON incidentlog_appreciations.createdBy = employees.employee_id";
		$join .= " LEFT JOIN employees atl ON incidentlog_appreciations.team_lead = atl.employee_id";
		$join .= " LEFT JOIN wor ON wor.wor_id = incidentlog_appreciations.wor_id";
		$where = " WHERE incidentlog_appreciations.status=0";
		//$where = " WHERE 1=1";
		if($empData['Grade']==1 || $empData['Grade']==2)
		{
			$where .= " AND responsible_member LIKE '%".$EmpID."%'";
		}
		/*if($empData['Grade']>2 && $empData['Grade']<5)
		{
			$where .= " AND (createdBy = $EmpID OR incidentlog_appreciations.team_lead LIKE '%".$EmpID."%')";
		}*/
		if($empData['Grade']>2 && $empData['Grade']<5)
		{
			$where .= " AND (incidentlog_appreciations.client_id IN( ".$empData['ClientId'].") OR incidentlog_appreciations.createdBy = ".$empData['employee_id'].")";
		}
		
		$filterWhere = '';
		
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if($filterField != '') {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
		}
		if($filterWhere !="")
		{
			$where .= " and (".$filterWhere.")";    			
		}
		
		if($sortBy!="" && $sortDirection!="")
		{
			$order = " ORDER BY ". $sortBy." ".$sortDirection;
		}
		else
		{
			$order = " ORDER BY appr_date DESC ";
		}
		
		$query = $select . $from . $join . $where . $order;
		// echo $query; exit;
		$totQuery = $this->db->query($query);
		$totCount = $totQuery->num_rows();

		$query .= " LIMIT $start, $end";
		$totQuery = $this->db->query($query);
		$data = $totQuery->result_array();
		
		foreach ($data as $key => $entry) 
		{
			$data[$key]['log_date']	= $this->commonDateSetting($entry['log_date']);
			$data[$key]['appr_date']	= $this->commonDateSetting($entry['appr_date']);
			
			$data[$key]['responsible_member_name']	= $this->getEmployeeNames($entry['responsible_member']);
		}
		
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));
	//echo '<pre>';print_r($results);exit;	
		return $results;
	}

	
	function view_esacalation() 
	{
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];
		$filter				= json_decode($this->input->get('filter'),true);
		
		$resp_emp_id = ($this->input->get('resp_emp_id') != '')? $this->input->get('resp_emp_id') : "";
		$text = ($this->input->get('text') != '')? $this->input->get('text') : "";
		
		$start = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$end = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
		$options = array();
		
		$options['filter']	= $filter;
		
		$sort = json_decode($this->input->get('sort'),true);
		
		$sortBy = $sort[0]['property'];
		//echo $sortBy;
		$sortDirection = $sort[0]['direction'];

		
		$select = "SELECT id,error_id,incidentlog_escalation.created_on,client.client_id,DATE_FORMAT(log_date,'%d-%m-%Y') AS log_date, DATE_FORMAT(mitigation_plan_date,'%d-%m-%Y') AS mitigation_plan_date,incidentlog_escalation.wor_id, wor.work_order_id as wor_name, DATE_FORMAT(escalation_date,'%d-%m-%Y') AS escalation_date,incidentlog_escalation.primary_lead_id as manager , production_member,client.client_name AS client,problem_summary,	 impact, impact_business, impact_revenue, impact_relationship, rootcause_id, rootcause_analysis,mitigation_plan,attachment,	CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS created_by,CONCAT(IFNULL(rb.`first_name`, ''),' ',IFNULL(rb.`last_name`, '')) AS reviewed_by, CONCAT(IFNULL(ab.`first_name`, ''),' ',IFNULL(ab.`last_name`, '')) AS approved_by";
		$from = " FROM incidentlog_escalation";
		$join = " LEFT JOIN client ON incidentlog_escalation.client_id = client.client_id";
		$join .= " LEFT JOIN employees ON incidentlog_escalation.created_by = employees.employee_id";
		$join .= " LEFT JOIN employees rb ON incidentlog_escalation.reviewed_by = rb.employee_id";
		$join .= " LEFT JOIN employees ab ON incidentlog_escalation.approved_by = ab.employee_id";
		$join .= " LEFT JOIN wor ON wor.wor_id = incidentlog_escalation.wor_id";
		$where = " WHERE incidentlog_escalation.status = 0";
		
		if($empData['Grade']==1 || $empData['Grade']==2)
		{
			$where .= " AND production_member LIKE '%".$EmpID."%'";
		}
		if($empData['Grade']>2 && $empData['Grade']<5)
		{
			$where .= " AND (incidentlog_escalation.client_id IN( ".$empData['ClientId'].") OR incidentlog_escalation.created_by = ".$empData['employee_id'].")";
		}
		
		if($resp_emp_id!="")
		{
			$where .= " AND production_member LIKE '%".$resp_emp_id."%'";
		}
		
		if($text !="")
		{
			$where .= " AND (error_id LIKE '%".$text."%' OR client_name LIKE '%".$text."%' OR first_name LIKE '%".$text."%' OR last_name LIKE '%".$text."%')";    			
		}

		if($sortBy!="" && $sortDirection!="")
		{
			$order = " ORDER BY ". $sortBy." ".$sortDirection;
		}
		else
		{
			$order = " ORDER BY incidentlog_escalation.log_date DESC ";
		}
		
		// $order = " ORDER BY incidentlog_escalation.log_date DESC";
		$query = $select . $from . $join . $where . $order;
		// echo $query; exit;
		$totQuery = $this->db->query($query);
		$totCount = $totQuery->num_rows();

		if($this->input->get('downloadCsv')!=1)
			$query .= " LIMIT $start, $end";

		$totQuery = $this->db->query($query);
		$data = $totQuery->result_array();
		
		foreach ($data as $key => $entry) 
		{
			$data[$key]['log_date']	= $this->commonDateSetting($entry['log_date']);
			$data[$key]['mitigation_plan_date']	= $this->commonDateSetting($entry['mitigation_plan_date']);
			$data[$key]['escalation_date']	= $this->commonDateSetting($entry['escalation_date']);
			$data[$key]['production_member_name']	= $this->getEmployeeNames($entry['production_member']);
			$data[$key]['manager_name']	= $this->getEmployeeNames($entry['manager']);
			// $data[$key]['reviewed_by']	= $this->getEmployeeNames($entry['reviewed_by']);
			// $data[$key]['approved_by']	= $this->getEmployeeNames($entry['approved_by']);
			$data[$key]['impact_business']	= ucfirst($entry['impact_business']);
			$data[$key]['impact_relationship']	= ucfirst($entry['impact_relationship']);
		}
		
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));
		// echo "<PRE>";print_r($results);exit;
		return $results;
	}



	/**
	* Function to add & edit error log
	*/
	function saveErrorLog($data = '', $idVal = '') 
	{
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];

		// debug($_REQUEST);exit;
		$setData = $_REQUEST;
		$attid=0;
		if(isset($setData['attachid']) && ($setData['attachid']!='')){
			$attid=$this->deletefile($setData['id'],'error');
		}
		if($setData['id']!="" && $attid==1){

			$setData['attachment']='';

		}
		if(!isset($setData['error_id'])){
			
			return 0;exit;
		}
		if($_FILES['file']['size']>5000000){
			return 'big size';
			exit;
		} 
		$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$allow_ext = array("doc","docx","pdf","xlsx","xls","pst","txt","rtf","csv","zip","msg");
		if (!in_array($ext, $allow_ext)) 
		{
			$_FILES['file']['name'] = '';
			$_FILES['file']['type'] = '';
			$_FILES['file']['tmp_name'] = '';
			$_FILES['file']['error'] = 4;
			$_FILES['file']['size'] = 0;

		}
		$image = isset($setData['attachment']) ? $setData['attachment'] : '';
		
		if($_FILES['file']['name']!="" && $_FILES['file']['name']!="null")
		{
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			$image=basename($_FILES['file']['name'],".".$ext).'_'.$_REQUEST['error_id'].".".$ext;
			//$image = $_FILES['file']['name'];
			move_uploaded_file($_FILES["file"]["tmp_name"], $this->config->item('REPOSITORY_PATH')."resources/uploads/incidentLogDocs/errorDocs/". $image);
		}
		
		$dataset = array();
		$dataset = array(
			'client_id'				=> $setData['Client'],
			'wor_id'				=> $setData['wor_id'],
			'team_lead'				=> $setData['team_leader'],
			'associate_manager'		=> $setData['associate_manager'],
			'production_member'		=> implode(",", $setData['production_member']),
			'qa_member'				=> ($setData['qa_member'][0]!="Select Employee") ? implode(",", $setData['qa_member']) : '',
			//'domain_expert'			=> ($setData['domain_expert'][0]!="Select Employee") ? implode(",", $setData['domain_expert']) : '',
			//'client_partner'		=> implode(",", $setData['client_partner']),
			'error_category'	    => $setData['error_category'],
			'error_subcategory'	    => $setData['error_subcategory'],
			'error_date'			=> date('Y-m-d', strtotime($setData['error_date'])),
			'error_notified_date'	=> date('Y-m-d', strtotime($setData['error_notified_date'])),
			'problem_summary'		=> $setData['problem_summary'],
			'units_impacted'		=> $setData['units_impacted'],
			'impact'				=> $setData['impact'],
			'impact_business'		=> $setData['impact_business'],
			'impact_revenue'		=> $setData['impact_revenue'],
			'impact_relationship'	=> $setData['impact_relationship'],
			'rootcause_id'			=> $setData['rootcause_id'],
			'rootcause_analysis'	=> $setData['rootcause_analysis'],
			'correction'			=> $setData['correction'],
			'corrective_action'		=> $setData['corrective_action'],
			'correction_date'		=> date('Y-m-d', strtotime($setData['correction_date'])),
			'corrective_action_date'=> date('Y-m-d', strtotime($setData['corrective_action_date'])),
			'attachment'			=> $image,
			'missed_tat'			=> $setData['missed_tat']
		);
		// echo "<pre>"; print_r($dataset); exit;
		$newLogID = "";
		if($setData['id']!="")
		{
			$dataset ['modifiedOn'] = date("Y-m-d h:i:s");
			$dataset ['modifiedBy'] = $EmpID;
			
			$where['id'] = $newLogID = $setData['id'];
			$this->update($dataset, $where, 'external_errors');
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(External Error ID:'.$setData['id'].') has been updated external error successfully.';
			$this->userLogs($logmsg);
		}
		else
		{
			$dataset['error_id'] = $setData['error_id'];
			$dataset['log_date'] = date('Y-m-d', strtotime($setData['log_date']));
			$dataset['createdBy'] = $EmpID;
			
			$retVal = $this->insert($dataset, 'external_errors');
			$newLogID = $this->insertId;
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(External Error ID:'.$newLogID.') has been added external error successfully.';
			$this->userLogs($logmsg);
		}
		
		return $newLogID;
	}
	
	
	/**
	* Function to delete log based on $idVal
	*/
	function deleteErrorLog( $idVal = '' ) 
	{
		$this->db->set('status', 1);
		$this->db->where('id', $idVal);
		$this->db->update('external_errors');
		$retVal = $this->db->affected_rows();

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(External Error ID:'.$idVal.') has been removed external error successfully.';
		$this->userLogs($logmsg);
		
		return $retVal;
	}
	
	function deleteAppr( $idVal = '' ) 
	{
		$this->db->set('status', 1);
		$this->db->where('id', $idVal);
		$this->db->update('incidentlog_appreciations');
		$retVal = $this->db->affected_rows();

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Appreciation ID:'.$idVal.') has been removed appreciation successfully.';
		$this->userLogs($logmsg);
		
		return $retVal;
	}
	
	/**
	* Function to get employee names
	*/
	private function getEmployeeNames($empArr)
	{
		$empNameArr = str_replace(",", "','", $empArr);
		if($empArr != ''){
			$empRst = $this->db->query("SELECT GROUP_CONCAT(IFNULL(employees.`first_name`, ''), ' ', IFNULL(employees.`last_name`, '')) AS empname FROM employees 
				WHERE employee_id IN ($empArr)");
			$empNameArr = $empRst->row_array();

			return $empNameArr['empname'];
		}
		else{
			return '';
		}
	}

	function saveApprLog($data = '', $idVal = '') 
	{
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];

		$setData = $_REQUEST;
		$attid=0;
		if(isset($setData['attachid']) && ($setData['attachid']!='')){
			$attid=$this->deletefile($setData['id'],'appr');
		}
		if($setData['id']!="" && $attid==1){

			$setData['attachment']='';

		}
		$image = isset($setData['attachment']) ? $setData['attachment'] : '';
		if(!isset($setData['appr_id'])){
			
			return 0;exit;
		}
		
		if($_FILES['apprFile']['size']>5000000){
			return 'big size';
			exit;
		}  
		$ext = pathinfo($_FILES['apprFile']['name'], PATHINFO_EXTENSION);
		$allow_ext = array("doc","docx","pdf","xlsx","xls","pst","txt","rtf","csv","zip","msg");
		if (!in_array($ext, $allow_ext)) 
		{	
			$_FILES['apprFile']['name'] = '';
			$_FILES['apprFile']['type'] = '';
			$_FILES['apprFile']['tmp_name'] = '';
			$_FILES['apprFile']['error'] = 4;
			$_FILES['apprFile']['size'] = 0;
		}
		
		if($_FILES['apprFile']['name']!="" && $_FILES['apprFile']['name']!="null")
		{
			$ext = pathinfo($_FILES['apprFile']['name'], PATHINFO_EXTENSION);
			
			$image=basename($_FILES['apprFile']['name'],".".$ext).'_'.$_REQUEST['appr_id'].".".$ext;
			
			move_uploaded_file($_FILES["apprFile"]["tmp_name"], $this->config->item('REPOSITORY_PATH')."resources/uploads/incidentLogDocs/ApprDocs/". $image);
		}
		$ext = pathinfo($_FILES['apprFile']['name'], PATHINFO_EXTENSION);
		
		move_uploaded_file($_FILES["apprFile"]["tmp_name"], $this->config->item('REPOSITORY_PATH')."resources/uploads/incidentLogDocs/ApprDocs/". basename($_FILES['apprFile']['name'],".".$ext).'_'.$_REQUEST['appr_id'].".".$ext);
		
		$dataset = array();
		$dataset = array(
			'client_id'				=> $setData['client_id'],
			'wor_id'				=> $setData['wor_id'],
			'team_lead'				=> $setData['team_leader'],
			'responsible_member'	=> implode(",",$setData['responsible_member']),
			'appr_by'				=> $setData['appr_by'],
			'appr_date'				=> date('Y-m-d', strtotime($setData['appr_date'])),
			'description'			=> $setData['description'],
			'attachment'    		=> $image
		);
		
		$newLogID = "";
		if($setData['id']!="")
		{
			$dataset ['modifiedOn'] = date("Y-m-d h:i:s");
			$dataset ['modifiedBy'] = $EmpID;
			
			$where['id'] = $newLogID = $setData['id'];
			$this->update($dataset, $where, 'incidentlog_appreciations');
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Appreciation ID:'.$setData['id'].') has been updated appreciation successfully.';
			$this->userLogs($logmsg);
		}
		else
		{
			$dataset['appr_id'] = $setData['appr_id'];
			$dataset['log_date'] = date('Y-m-d', strtotime($setData['log_date']));
			$dataset['createdBy'] = $EmpID;
			
			$retVal = $this->insert($dataset, 'incidentlog_appreciations');
			$newLogID = $this->insertId;
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Appreciation ID:'.$newLogID.') has been added appreciation successfully.';
			$this->userLogs($logmsg);
		}
		
		
		return $newLogID;
	}
	
	/**
	* Function to add & edit Escalation log
	*/
	function saveEscLog($data = '', $idVal = '') 
	{
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];

		// echo "<pre>"; print_r($_REQUEST);print_r($_FILES); exit;
		$setData = $_REQUEST;
		$attid=0;
		if(isset($setData['attachid']) && ($setData['attachid']!='')){
			$attid=$this->deletefile($setData['id'],'esc');
		}
		if($setData['id']!="" && $attid==1){

			$setData['attachment']='';

		}
		if(!isset($setData['error_id'])){
			
			return 0;exit;
		}
		if($_FILES['escFile']['size']>5000000){
			return 'big size';
			exit;
		} 
		$ext = pathinfo($_FILES['escFile']['name'], PATHINFO_EXTENSION);
		$allow_ext = array("doc","docx","pdf","xlsx","xls","pst","txt","rtf","csv","zip","msg");
		if (!in_array($ext, $allow_ext)) 
		{
			$_FILES['escFile']['name'] = '';
			$_FILES['escFile']['type'] = '';
			$_FILES['escFile']['tmp_name'] = '';
			$_FILES['escFile']['error'] = 4;
			$_FILES['escFile']['size'] = 0;
		}
		$image = isset($setData['attachment']) ? $setData['attachment'] : '';
		if($_FILES['escFile']['name']!="" && $_FILES['escFile']['name']!="null")
		{
			$ext = pathinfo($_FILES['escFile']['name'], PATHINFO_EXTENSION);
			$image=basename($_FILES['escFile']['name'],".".$ext).'_'.$_REQUEST['error_id'].".".$ext;
			//$image = $_FILES['escFile']['name'];
			move_uploaded_file($_FILES["escFile"]["tmp_name"], $this->config->item('REPOSITORY_PATH')."resources/uploads/incidentLogDocs/EscalationDocs/". $image);
		}

		$dataset = array();
		$dataset = array(
			'client_id'				=> $setData['client'],
			'wor_id'				=> $setData['wor_id'],
			'primary_lead_id'		=> $setData['manager'],
			'production_member'		=> implode(",", $setData['production_member']),
			'problem_summary'		=> $setData['problem_summary'],
			'impact'				=> $setData['impact'],
			'impact_business'		=> $setData['impact_business'],
			'impact_revenue'		=> $setData['impact_revenue'],
			'impact_relationship'	=> $setData['impact_relationship'],
			'rootcause_id'			=> $setData['rootcause_id'],
			'rootcause_analysis'	=> $setData['rootcause_analysis'],
			'mitigation_plan'		=> $setData['mitigation_plan'],
			'escalation_date'		=> date('Y-m-d', strtotime($setData['escalation_date'])),
			'mitigation_plan_date'	=> date('Y-m-d', strtotime($setData['mitigation_plan_date'])),
			'attachment'			=> $image		
		);
		
		$newLogID = "";
		if($setData['id']!="")
		{
			$dataset ['modified_on'] = date("Y-m-d h:i:s");
			$dataset ['modified_by'] = $EmpID;
			
			$where['id'] = $newLogID = $setData['id'];
			$this->update($dataset, $where, 'incidentlog_escalation');
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Escalation ID:'.$setData['id'].') has been updated escalation successfully.';
			$this->userLogs($logmsg);
			
		}
		else
		{
			$dataset['error_id'] = $setData['error_id'];
			$dataset['log_date'] = date('Y-m-d', strtotime($setData['log_date']));
			$dataset['created_by'] = $EmpID;
			$dataset ['created_on'] = date("Y-m-d h:i:s");

			$retVal = $this->insert($dataset, 'incidentlog_escalation');
			$newLogID = $this->insertId;
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Escalation ID:'.$newLogID.') has been added escalation successfully.';
			$this->userLogs($logmsg);
		}
		
		return $newLogID;
	}
	
	/**
	* Function to delete Escalation log based on $idVal
	*/
	function deleteEscLog( $idVal = '' ) 
	{
		$this->db->set('status', 1);
		$this->db->where('id', $idVal);
		$this->db->update('incidentlog_escalation');
		$retVal = $this->db->affected_rows();

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Escalation ID:'.$idVal.') has been removed escalation successfully.';
		$this->userLogs($logmsg);
		
		return $retVal;
	}
	
	/* Approve/Review external error */
	function reviewApproveError($data = '')
	{    	    	 	
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];
		
		if($data['type'] == 'review')
		{
			$dataset = array(
				'reviewed_by'   => $EmpID,
				'reviewed_date' => date("Y-m-d h:i:s")
			);
		}
		
		if($data['type'] == 'approve')
		{
			$dataset = array(
				'approved_by'   => $EmpID,
				'approved_date' => date("Y-m-d h:i:s")
			);
		}
		
		$this->db->where('id', $data['id']);
		$retVal = $this->db->update('external_errors',$dataset);

		if($data['type'] == 'review')
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(External Error ID:'.$data['id'].') has been reviewed external error successfully.';
			$this->userLogs($logmsg);
		}

		if($data['type'] == 'approve')
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(External Error ID:'.$data['id'].') has been approved external error successfully.';
			$this->userLogs($logmsg);
		}

		return $retVal;
	}
	
	/* Approve/Review escalation & Appericiation */
	function reviewApprove($data = '')
	{    	    	 	
		$empData = $this->input->cookie();
		$EmpID  = $empData['employee_id'];
		
		
		if($data['type'] == 'review')
		{
			$dataset = array(
				'reviewed_by'   => $EmpID,
				//'reviewed_date' => date("Y-m-d H:i:s")
			);
		}
		
		if($data['type'] == 'approve')
		{
			$dataset = array(
				'approved_by'   => $EmpID,
				//'approved_date' => date("Y-m-d H:i:s")
			);
		}
		
		$this->db->where('id', $data['id']);
		$retVal = $this->db->update('incidentlog_escalation',$dataset);

		if($data['type'] == 'review')
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Escalation ID:'.$data['id'].') has been reviewed escalation successfuly.';
			$this->userLogs($logmsg);
		}

		if($data['type'] == 'approve')
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Escalation ID:'.$data['id'].') has been approved escalation successfuly.';
			$this->userLogs($logmsg);
		}
		
    //echo $retVal;
		return $retVal;
	}
	
	
	function deletefile($atid = '',$type='')
	{  
		if($type=='appr'){
			$dbname='incidentlog_appreciations';

		}else if($type=='esc'){
			$dbname='incidentlog_escalation';

		}else if($type=='error'){
			$dbname='external_errors';

		}
		$sql = 'SELECT attachment from '.$dbname.' where id='.$atid;						
		$query = $this->db->query($sql);
		$res=$query->row_array();
		if($type=='appr'){
			$path=$_SERVER['DOCUMENT_ROOT'].'/UI/resources/uploads/incidentLogDocs/ApprDocs/'.$res['attachment'];
		}else if($type=='esc'){
			$path=$_SERVER['DOCUMENT_ROOT'].'/UI/resources/uploads/incidentLogDocs/EscalationDocs/'.$res['attachment'];
		}else if($type=='error'){
			$path=$_SERVER['DOCUMENT_ROOT'].'/UI/resources/uploads/incidentLogDocs/errorDocs/'.$res['attachment'];
		}
		if (unlink($path)) 
		{
			$retVal=1;
		}
		else
		{
			$retVal =0;	
		}
		
		return $retVal;
	}
	
	/* Get Root Cause details */
	function get_root_cause($id='')
	{
		$sql = 'SELECT id as RootCauseID, name as Description from root_cause';	
		if($id!='')
		{
			$sql .= " WHERE id='$id'";
		}					
		$query = $this->db->query($sql);

		if($id!='')
		{
			$sql .= " WHERE id='$id'";
			return $query->row_array();
		}
		
		$results = array_merge(
			array('totalCount' => $query->num_rows()), 
			array('data' => $query->result_array())
		);
		
		return $results;
	}
	
	/* Get Responsible person details */
	function viewResponsiblePerson()
	{
		$empData = $this->input->cookie();
		
		$sql = "SELECT GROUP_CONCAT(DISTINCT production_member SEPARATOR ',') AS employee_id FROM `incidentlog_escalation` WHERE status=0 ";
		if($empData['Grade']<5)
		{
			$sql .= " AND client_id IN (".$empData['ClientId'].")";
		}
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$resp_str = $result[0]['employee_id'];
		
		$results = array();
		if($resp_str!="")
		{
			$sql2 = "SELECT employee_id, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS emp_name 
			FROM employees WHERE employee_id IN($resp_str) ORDER BY first_name ASC";
			$query2 = $this->db->query($sql2);
			
			$results = array_merge(
				array('totalCount' => $query2->num_rows()), 
				array('data' => $query2->result_array())
			);
		}
		
		return $results;
	}

	public function downloadExternalErrorCsv()
	{
		$data = $this->view();
		$data = $data['rows'];
		$loop = 0;
		foreach ($data as $data_key => $data_value) {
			$csvdata[$loop]['Log Date'] = $data[$data_key]['log_date'];
			$csvdata[$loop]['Error ID'] = $data[$data_key]['error_id'];
			$csvdata[$loop]['Client'] = $data[$data_key]['Client'];
			$csvdata[$loop]['Error Category'] = $data[$data_key]['cat_name'];
			$csvdata[$loop]['Error Subcategory'] = $data[$data_key]['subcat_name'];
			$csvdata[$loop]['Work Order'] = $data[$data_key]['wor_name'];
			$csvdata[$loop]['Associate Manager'] = $data[$data_key]['associate_manager_name'];
			$csvdata[$loop]['Team Lead'] = $data[$data_key]['team_lead_name'];
			$csvdata[$loop]['Production Member(s)'] = $data[$data_key]['responsiblePerson'];
			$csvdata[$loop]['QA Member(s)'] = $data[$data_key]['responsibleQA'];
			$csvdata[$loop]['Error Date'] = $data[$data_key]['error_date'];
			$csvdata[$loop]['Error Notified Date'] = $data[$data_key]['error_notified_date'];
			$csvdata[$loop]['Error Statement'] = $data[$data_key]['problem_summary'];
			$csvdata[$loop]['No Of Units Impacted'] = $data[$data_key]['units_impacted'];
			$csvdata[$loop]['Error Summary'] = $data[$data_key]['impact'];
			$csvdata[$loop]['Missed TAT'] = $data[$data_key]['missed_tat'];
			$csvdata[$loop]['Impact Business'] = $data[$data_key]['impact_business'];
			$csvdata[$loop]['Impact Revenue($)'] = $data[$data_key]['impact_revenue'];
			$csvdata[$loop]['impact relationship'] = $data[$data_key]['impact_relationship'];

			$Root_Cause = $this->get_root_cause($data[$data_key]['rootcause_id']);

			$csvdata[$loop]['Root Cause'] = $Root_Cause['Description'];
			$csvdata[$loop]['Root Cause Analysis'] = $data[$data_key]['rootcause_analysis'];
			$csvdata[$loop]['Correction'] = $data[$data_key]['correction'];
			$csvdata[$loop]['Correction Date'] = $data[$data_key]['correction_date'];
			$csvdata[$loop]['Corrective Action Date'] = $data[$data_key]['corrective_action_date'];
			$csvdata[$loop]['Corrective Action'] = $data[$data_key]['corrective_action'];
			$loop++;
		}
		// debug($data[0]);exit;
		// debug($csvdata);exit;

		$columns = array(
			array_keys($csvdata[0])
		);

		$results = array_merge($columns, $csvdata);
		// debug($results);exit;

		return $results;
	}

	public function downloadEscalationCsv()
	{
		$data = $this->view_esacalation();
		$data = $data['rows'];
		$loop = 0;
		foreach ($data as $data_key => $data_value) {
			$csvdata[$loop]['Log Date'] = $data[$data_key]['log_date'];
			$csvdata[$loop]['Escalation ID'] = $data[$data_key]['error_id'];
			$csvdata[$loop]['Clients'] = $data[$data_key]['client'];
			$csvdata[$loop]['Work Order'] = $data[$data_key]['wor_name'];
			$csvdata[$loop]['Managers'] = $data[$data_key]['manager_name'];
			$csvdata[$loop]['Employees'] = $data[$data_key]['production_member_name'];
			$csvdata[$loop]['Escalation Raised Date'] = $data[$data_key]['escalation_date'];
			$csvdata[$loop]['Escalation Statement'] = $data[$data_key]['problem_summary'];
			$csvdata[$loop]['Escalation Summary'] = $data[$data_key]['impact'];

			$csvdata[$loop]['Impact Business'] = $data[$data_key]['impact_business'];
			$csvdata[$loop]['Impact Revenue($)'] = $data[$data_key]['impact_revenue'];
			$csvdata[$loop]['Impact relationship'] = $data[$data_key]['impact_relationship'];

			$Root_Cause = $this->get_root_cause($data[$data_key]['rootcause_id']);

			$csvdata[$loop]['Root Cause'] = $Root_Cause['Description'];
			$csvdata[$loop]['Root Cause Analysis'] = $data[$data_key]['rootcause_analysis'];
			$csvdata[$loop]['Mitigation Plan'] = $data[$data_key]['mitigation_plan'];
			$csvdata[$loop]['Mitigation Plan Date'] = $data[$data_key]['mitigation_plan_date'];
			$loop++;
		}
			// debug($data[0]);exit;
			// debug($csvdata);exit;

		$columns = array(
			array_keys($csvdata[0])
		);

		$results = array_merge($columns, $csvdata);
		// debug($results);exit;

		return $results;
	}
}
?>