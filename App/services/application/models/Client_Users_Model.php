<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_Users_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
	}

	public function checkIfClient($username,$password){

		$checkClient_sql = "SELECT COUNT(cu.Username) AS count, cu.UserID, cu.Username, cu.FullName, cu.Password, cu.ClientID,c.Client_Name,c.Logo,cu.Email 
		FROM client_users AS cu LEFT JOIN client AS c ON c.ClientID=cu.ClientID WHERE cu.Username='".$username."'  AND cu.PASSWORD ='".md5($password)."'";	
		$checkClient_query = $this->db->query($checkClient_sql);			
		return $checkClient_query->result_array();		
	}

	public function saveUserClient($data){

		$clientUserData = json_decode($data['client'],true);
		$this->db->select('Username,Email')
		->from('client_users');
		if($clientUserData["UserID"] == "")
			$where = 'Username = "'.$clientUserData["Username"].'" or Email = "'.$clientUserData["Email"].'" ';
		else
			$where = '(Username = "'.$clientUserData["Username"].'" or Email = "'.$clientUserData["Email"].'") AND UserID != "'.$clientUserData["UserID"].'" ';

		$this->db->where($where);
		$getrecord = $this->db->get();

		if($getrecord->num_rows() >= 1)
			$saveResp = -1;
		else
		{
			$password = $this->generatePassword();
			$clientUserDataSave = array(
				'Username' =>$clientUserData["Username"],
				'Password' =>md5($password),
				'FullName' =>$clientUserData["FullName"],
				'Email' =>$clientUserData["Email"],
				'ClientID' =>$clientUserData["ClientID"],
			);
			if($clientUserData["UserID"] == "")						
				$saveResp = $this->insert($clientUserDataSave, 'client_users');
			else
				$saveResp = $this->update($clientUserDataSave, array('UserID' => $clientUserData["UserID"]), 'client_users');

			$this->config->load("email-body");
			$string=array("<<FullName>>","<<Username>>","<<Password>>");
			$stringReplce=array($clientUserData["FullName"], $clientUserData["Username"],$password);
			$summary = str_replace ($string, $stringReplce, $this->config->item('ClientUser'));
			$this->load->model('Email_Model');
			$from_user = 'Theorem India';
			$subject = 'Rhythm Login Credentials';
			/* Email Commented */
			$this->Email_Model->sendMail(THEOREM_MAIL, $clientUserData["Email"], $from_user, $subject, $summary);
		}
		return $saveResp;
	}
	public function getClientUserList($clientId){
		$options = array();
		$sort = json_decode($this->input->get('sort'),true);
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		$this->db->select('*')
		->from('client_users');
		$this->db->where("ClientID", $clientId);
		if(isset($options['sortBy'])) {
			$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		}else
		$this->db->order_by("FullName", "asc");

		$getrecord = $this->db->get();
		$totCount = $getrecord->num_rows();
		$data = $getrecord->result_array();
		return array('data'=> $data,'totalCount' => $totCount);
	}
	public function generatePassword(){
		$length = 8;
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$additional_characters = array('_','.');
		$final_array = array_merge($alphabets,$numbers,$additional_characters);
		$password = '';
		while($length--) {
			$key = array_rand($final_array);
			$password .= $final_array[$key];
		}
		return $password;
	}
	public function getCustomerProcessData(){
		$sessionclient = $this->input->cookie();
		$clientId = $sessionclient['ClientID']; 
		$options = array();
		$sort = json_decode($this->input->get('sort'),true);

		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		$sql = "SELECT tab1.Name,COUNT(tab1.EmployID) AS Billable  FROM 
		((SELECT employprocess.EmployID ,process.Name FROM (`employprocess`)
		LEFT JOIN PROCESS ON employprocess.ProcessID = process.ProcessID AND process.ProcessStatus = 0
		WHERE process.`ClientID` =  $clientId AND employprocess.Billable =1)
		) AS tab1
		JOIN employ ON (tab1.EmployID = employ.EmployID)
		WHERE employ.relieveflag = 0
		GROUP BY tab1.Name ";
		if(isset($options['sortBy'])) {
			$sql.="Order by ".$options['sortBy']." ".$options['sortDirection']." ";
		}else{
			$sql.="Order by tab1.Name ASC";
		}
		$query = $this->db->query($sql, false);
		$results = array_merge(array('totalCount' => $query->num_rows()),array('rows' =>$query->result_array()));
		return $results;
	}

	public function getCustomerFeedData(){
		$get_url = "http://feeds.feedburner.com/NdtvNews-TopStories";  
		$output = file_get_contents("http://feeds.feedburner.com/NdtvNews-TopStories");
		$feeds = $this->render_feed_newz_caption($output); 

		$feedItems = "<div style = 'height:80px;'>
		<h4><a  href='#'>Varanasi Has Free Wifi. But the Monkeys are a Big Problem.</a></h4>
		<h5>NDTV: "  . date('Y-m-d') . "</h5>
		<p>As India launches an $18 billion or Rs 1.12 lakh crore plan to spread the information revolution to its provinces, some of the problems it faces are a holdover from the past - electricity shortages</p>
		</div>
		<br>"; 
		$results = array_merge(array('totalCount' => 0),array('rows' =>array('feeditems'=>$feeds)));
		return $results;
	}

	public function render_feed_newz_caption($feed){
		$xml = simplexml_load_string($feed);
		libxml_use_internal_errors(true);
		$feedItems = '';
		if(isset($xml->channel->item))
			foreach($xml->channel->item as $YODEL){
				$title= $YODEL->title;
				$description = $YODEL->description;
				$link = $YODEL->link;
		        $pubDate = $YODEL->pubDate;     //should be able to use Rutgers pubDate to sort newest.... get a better Drudge feed to do the same
		        $image = $YODEL->image;
		        $feedItems=  $feedItems."

		        <div style = 'height:80px;'>
		        <h4><a  target = '_blank' href='" . $link . "'>"  . $title . "</a></h4>
		        <h5>NDTV: "  . $pubDate . "</h5>
		        <p>" . $description .  "</p>
		        </div>
		        <br>";
		        
		    }
		    return '<marquee  behavior="scroll"  direction="up" scrollamount=2" id="myMarquee" onmouseout="this.start();" onmouseover="this.stop()">'.$feedItems.'</marquee>';
		}

	}
	?>