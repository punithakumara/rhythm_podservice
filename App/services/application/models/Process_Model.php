<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Process_Model extends INET_Model
{
	public $super_admin =  array();
	public $clientLoginId;
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Rhythm_Model');
		$this->load->model('Employee_Model','Emp');
		$empData = $this->input->cookie();
		if(isset($empData['EmployID'])){
			$this->EmpId = $empData['EmployID'];
			$this->EmpProcessId = $empData['Proc'];
		}
		$this->config->load('admin_config');
		//$this->ci =& get_instance();
		$this->super_admin = $this->config->item('super_admin');
	}
	
	function build_query() 
	{
		$empData 			= $this->input->cookie();
		$grade 				= isset($empData['Grade']);
		$filter 			= json_decode($this->input->get('filter'),true);
		$options['filter']	= $filter;

		// conditions
		$gridProcess			= $this->input->get('gridProcess') ? $this->input->get('gridProcess') : 0;
		$VerticalID 			= $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';
		$processID 				= $this->input->get('processID') ? $this->input->get('processID') : '';
		$comboNOAMRes			= $this->input->get('comboNOAMRes') ? $this->input->get('comboNOAMRes') : '';
		$isUtilzation			= $this->input->get('isUtilzation') ? $this->input->get('isUtilzation') : '';
		$comboAMRes				= $this->input->get('comboAMRes') ? $this->input->get('comboAMRes') : '';
		$comboProcess			= $this->input->get('comboProcess') ? $this->input->get('comboProcess') : '';
		$clientContProcess		= $this->input->get('clientContProcess') ? $this->input->get('clientContProcess') : '';			
		$multigraph 			= $this->input->get('multigraph');	

		if($multigraph) 
		{ 
			$comboClient	= ($this->input->get('ClientID') != '') ? implode("','",json_decode($this->input->get('ClientID'))) : '';	 
		} 
		else 
		{ 
			$comboClient	= $this->input->get('ClientID') ? $this->input->get('ClientID') : '';
		}						
		$filterName				= $this->input->get('filterName')?$this->input->get('filterName'):"";
		$filterQuery			= $this->input->get('query');
		$this->clientLoginId	= $this->input->get('ClientLoginID') ? $this->input->get('ClientLoginID') : 0;
		$processStatusCombo 	= $this->input->get('processStatusCombo') ? $this->input->get('processStatusCombo') : 0;
		$conUtil				= $this->input->get('conUtil') ? $this->input->get('conUtil') : '';
		$pverticalCombo			= $this->input->get('pverticalCombo') ? $this->input->get('pverticalCombo') : '';

		$isHrPool				= false;
		$onselectedfromutil		= $this->input->get('onselectedfromutil') ? $this->input->get('onselectedfromutil') : '';
		$fromUtilHrpoolEmp		= false;	
		
		if(isset($empData['loginType']) && $empData['loginType'] != 'Client') 
		{
			$empDataRel			= $this->Emp->getEmployeegrade($this->EmpId);
			
			if(($empDataRel[0]['TrainingNeeded']	== 0) && ($empDataRel[0]['TransitionStatus']	== 2) && ($isUtilzation != "")) 
			{
				$isHrPool = true;
			}
		}

		if($comboProcess != "")
		{
			$sql = " SELECT process.Name,process.Name AS ProcessName ,process.process_id,ForcastNRR,ForcastRDR,process.head_count,process.process_description from process WHERE 1=1  ";
		}
		else
		{
			$sql = "SELECT process.Name,process.process_id, process.fk_client_id, verticals.vertical_id, 
			GROUP_CONCAT(DISTINCT CASE WHEN employprocess.grades = 4 THEN `employprocess`.employee_id END) AS AM,
			GROUP_CONCAT(DISTINCT CASE WHEN employprocess.grades >=3 AND employprocess.grades < 4 THEN `employprocess`.employee_id END)AS Process_leads,
			client.Client_Name, verticals.Name AS Vertical_Name, COUNT(DISTINCT `employprocess`.employee_id) AS ProcessEmployees
			FROM PROCESS 
			JOIN `client` ON `process`.`fk_client_id` = `client`.`client_id`
			JOIN `verticals` ON `process`.`vertical_id` = `verticals`.`vertical_id` 
			LEFT JOIN `employprocess` ON `process`.`process_id` = `employprocess`.`process_id` 
			LEFT JOIN `employees` ON (`employees`.`employee_id` = `employprocess`.`employee_id` AND `employees`.`relieve_flag`=0)
			WHERE 1=1";
		}
		
		
		if($comboClient != "")
		{
			if($multigraph)
			{
				$sql .= " AND process.fk_client_id IN('".$comboClient."')";
			}
			else 
			{
				$sql .=" AND process.fk_client_id=".$comboClient;
			}
		}

		if($VerticalID != "")
		{
			// added for Group VH
			$this->load->model('Vertical_Model');
			
			$chksubverticals = $this->vertical_model->getVerticalsForParentID($VerticalID);  
			if($chksubverticals != NULL)
			{
				$sql .=" AND  process.vertical_id IN ($chksubverticals) ";
			}
		}

		if($processID != "")
		{
			$sql .=" AND process.process_id=".$processID;
		}

		if($comboProcess!="")
		{
			//display allprocess w.r .t to client even Loggedin employee in HRpool
			if($onselectedfromutil != "" && $comboClient !='')
			{
				if($grade < 4)
				{							
					$processArray = $this->getProcess();
				}
				else
					$processArray = array();
				if(count($processArray)!=0 && !empty($processArray))
				{
					for($procssHr = 0; $procssHr < count($processArray); $procssHr++)
					{
						$HrpoolProcessfromUtil[] = $processArray[$procssHr];
					}
					if(in_array(INI_PROCESS_ID,$HrpoolProcessfromUtil) || in_array(ONBOARD_PROCESS_ID,$HrpoolProcessfromUtil))
					{
						$fromUtilHrpoolEmp = true;
					}
					else
					{
						$fromUtilHrpoolEmp = false;
					}
				}
				else
					$fromUtilHrpoolEmp = false;
			}

			//Condition made Con Utilization process list issue.
			if(($conUtil != 1 || $grade <= 4) && $clientContProcess != 1) 
			{
				//get process based on login employee
				$processArray = $this->getProcess();
				if(count($processArray)!=0  && !(in_array(INI_PROCESS_ID, $processArray) && $isHrPool))
				{
					//added to exclude processlist for loggedin Hrpoolemp
					if(!$fromUtilHrpoolEmp)
						$sql .= " AND process.process_id IN(".implode(',', $processArray).")";
				}
			}
		}
		
		if($gridProcess == 1)
		{
			$processArray = $this->getProcess();
			if(count($processArray)!=0)
				$sql .= " AND process.process_id IN(".implode(',', $processArray).")";
		}
		
		if($comboAMRes !="")
		{
			$sql .= " AND process.processID IN (SELECT process_id FROM  employprocess WHERE employee_id = '".$comboAMRes."') ";
		}
		
		if($pverticalCombo != '')
		{
			$this->db->select("GROUP_CONCAT(DISTINCT verticals.vertical_id) AS VerticalIDs ", FALSE)->from("verticals")
			->join('process', 'process.vertical_id = verticals.vertical_id')
			->where('verticals.Parent_VerticalID =', $pverticalCombo);
			/*if($loggedInVerticals != ''){
				$this->db->where_in('verticals.vertical_id ',$loggedInVerticals);
			}*/	        
			$this->db->order_by('verticals.Name');	        
			$query = $this->db->get();
			if ($query->num_rows() >0)
			{ 
				$VerticalFromParent = $query->row()->VerticalIDs;	        	
				$sql .=  " AND process.vertical_id IN(" . $VerticalFromParent . ")";
			}
		}
		
		/* if($this->input->get('dashboardProcess') !=1)
			$sql .=" AND process.process_status=".$processStatusCombo;
		else if ($this->input->get('dashboardProcess') == 1 && $multigraph == 1)
			$sql .=" AND process.process_status=0";		 */	
		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if($filterField != '') 
					{
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			if($filterWhere != '') 
			{
				$sql .= " AND (".$filterWhere.")";
			}
		}
		
		if($filterName !="" && $filterQuery!="" )
		{
			$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			$sql .= " AND ".$filterWhere;
		}	
		$sql .=" GROUP BY process_id";
		//echo $sql; exit;
		if($this->clientLoginId != 0 && isset($this->clientLoginId) ) 
		{
			$sql = $this->clientExecutiveReport($this->clientLoginId);
			$filterWhere = '';
			if(isset($options['filter']) && $options['filter'] != '') 
			{
				foreach ($options['filter'] as $filterArray) 
				{
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) 
					{
						if($filterField != '') 
						{
							$filterWhere .= ($filterWhere != '')?' OR ':'';
							$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
						}
					}
				}
				if($filterWhere != '') 
				{
					$sql .= " AND (".$filterWhere.")";
				}
			}
			if($filterName !="" && $filterQuery!="" ) 
			{
				$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
				$sql .= " AND ".$filterWhere;
			}
		}
		//echo $sql;
		//exit;
		return $sql;
	}

	function getApproverDetails()
	{
		$userData = $this->session->userdata;
		$EmployID = $userData["user_data"]['EmployID'];
		$ProcessID = $this->input->get('ProcessID');
		
		$sql = "SELECT CONCAT(employees.first_name,' ' , employees.last_name) AS Name,employees.employee_id FROM employees 
				INNER JOIN employprocess
				WHERE employees.employee_id = employprocess.employee_id AND employprocess.process_id = '".$ProcessID."' 
				AND employprocess.Grades>= 2 AND employees.relieve_flag=0";
		
		$options = array();
		
		if($this->input->get_post('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}

		$sort = json_decode($this->input->get('sort'),true);

		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		
		$totQuery = $this->db->query($sql);
		$totCount = $totQuery->num_rows();				

		if($this->input->get('query')!="") 
		{
			$sql .= " and (employees.first_name like '".$this->input->get('query')."%')";
		}
		if(isset($options['sortBy'])) 
		{
			$sort = $options['sortBy'].' '.$options['sortDirection'];
			if($options['sortBy'] == "ProductionStartDate")
				$sort = " CAST(".$options['sortBy']." AS DATETIME) ".$options['sortDirection'];
			$sql .= " order by ".$sort;
		}

		$query = $this->db->query($sql);
		$data = $query->result_array();
		$results = array_merge(array('totalCount' => $totCount),array('rows' =>$data));
		
		return $results;
	}
	
	function getProcessData() 
	{
		$options = array();
		$comboProcess = $this->input->get('comboProcess') ? $this->input->get('comboProcess') : '';
		
		if($this->input->get_post('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}

		$sort = json_decode($this->input->get('sort'),true);

		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$sql = $this->build_query();
		
		$totQuery = $this->db->query($sql);
		$totCount = $totQuery->num_rows();

		$sql = $this->build_query();
		if(isset($options['sortBy'])) 
		{
			$sort = $options['sortBy'].' '.$options['sortDirection'];
			if($options['sortBy'] == "ProductionStartDate")
				$sort = " CAST(".$options['sortBy']." AS DATETIME) ".$options['sortDirection'];
			$sql .= " order by ".$sort;
		}
			
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " limit ".$limit;
		}
		else if(isset($options['limit'])) 
		{
			$sql .= " limit ".$options['limit'];
		}

		$query = $this->db->query($sql);

		$data  = $query->result_array();
		
		if($comboProcess =="" && $this->clientLoginId == 0) 
		{
			foreach ($data as $key => $entry) 
			{
				$AM = explode(",",$data[$key]['AM']);
				$PL = explode(",",$data[$key]['Process_leads']);
				$PT = explode(",",$data[$key]['TechnologyID']);
				$PA = explode(",",$data[$key]['Attribute_Names']);
				//$PAO = explode(",",$data[$key]['OrderofAttributes']);
				$Approver = explode(",",$data[$key]['Approver']);
				$data[$key]['AM'] = $AM;
				$data[$key]['Process_leads'] = $PL;
				$data[$key]['TechnologyID'] = $PT;
				$data[$key]['Attribute_Names'] = $PA;
				//$data[$key]['OrderofAttributes'] = $PAO;
				$data[$key]['Approver'] = $Approver;
				$data[$key]['ProductionStartDate'] = $this->commonDateSetting($entry['ProductionStartDate']);
			}
		}

		if(isset($this->clientLoginId) && $this->clientLoginId != 0 ) 
		{
			$indexed = array();
			$data  = "";
			$query = $this->db->query($sql);
			$data  = $query->result_array();
			foreach ($data as $key => $value) 
			{
				$data[$key]['ProductionStartDate'] = $this->commonDateSetting($value['ProductionStartDate']);
				if(isset($value['fm']) && $value['fm'] != null ) 
				{
					$fm = explode(",", $value['fm']);
					$data[$key]['fm_util']   = $fm[0];
					$data[$key]['fm_error']  = $fm[1];
					$data[$key]['fm_month']  = $fm[2];
				}
				if(isset($value['sm']) && $value['sm'] != null ) 
				{
					$sm = explode(",", $value['sm']);
					$data[$key]['sm_util']   = $sm[0];
					$data[$key]['sm_error']  = $sm[1];
					$data[$key]['sm_month']  = $sm[2];
				}
				if(isset($value['tm']) && $value['tm'] != null ) 
				{
					$tm = explode(",", $value['tm']);
					$data[$key]['tm_util']   = $tm[0];
					$data[$key]['tm_error']  = $tm[1];
					$data[$key]['tm_month']  = $tm[2];
				}
				unset($data[$key]['fm']);
				unset($data[$key]['sm']);
				unset($data[$key]['tm']);
			}
		}
		$VerticalID = $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';
		$TransPro   = $this->input->get('TransPro') ? $this->input->get('TransPro') : '';
		$LeftPro    = $this->input->get('LeftPro') ? $this->input->get('LeftPro') : '';
		if($TransPro == 1 && $VerticalID == INI_VERTICAL_ID && count($data)== 0)
		{
			$empData = $this->input->cookie();
			$SqlPro = "SELECT Name, process_id AS processID FROM process WHERE vertical_id = ".$VerticalID;
			if($empData['Vert'] != HR_VERTICAL_ID) 
			{
				$SqlPro .=" AND process_id=".INI_PROCESS_ID;
			}
			if($LeftPro == INI_PROCESS_ID)
				$SqlPro .=" AND process_id!=".INI_PROCESS_ID;
			$queryPro = $this->db->query($SqlPro);
			$totCount = $queryPro->num_rows();
			$data = $queryPro->result_array();
		}
		$results = array_merge(array('totalCount' => $totCount),array('data' => $data));

		return $results;
	}

	function deleteProcess( $idVal = '' ) 
	{
		
		$options = array();
		$options['ProcessID'] =  $idVal;
		
		$retVal = $this->delete($options, 'process');
		
		return $retVal;
	}
	
	function saveProcessFromNrr($processID = '', $data = '')
	{
		
		$processDataset = array(
			'ClientID'=>$data['ClientID'],
			'VerticalID'=>$data['VerticalID'],
			'Name'=>$data['Name'],
			'CreatedDate'=>date('Y-m-d h:i:s'),
		);
		
		$checkProcess = $this->getValues(array('ClientID' => $data['ClientID'], 'vertical_id' => $data['VerticalID']), 'process');
		if($checkProcess)
		{
			foreach($checkProcess as $key => $val)
			{
				if(trim($val['Name']) == trim($data['Name']))
				{
					return 2;
				}
			}
		}
		$retVal = $this->insert($processDataset, 'process');
		return 1;
	}
	
	function saveProcess($processID = '', $data = '') 
	{
		$addProcess = array();
		$editBool = false;
		
		$addProcess['Name'] = $data['process']['Name']; 
		$addProcess['ClientID'] = $data['process']['ClientID'];
		$addProcess['Process_type'] = $data['process']['Process_type'];
		$addProcess['VerticalID'] = $data['process']['VerticalID'];
		$addProcess['Approver'] = $data['process']['Approver'] ? $data['process']['Approver'] : NULL; //Approver
		$addProcess['process_status'] = $data['process']['process_status'][0];
		$addProcess['process_description'] = $data['process']['process_description'] ? $data['process']['process_description'] : "";			
		$ProductionStartDate =date('Y-m-d', strtotime($data['process']['ProductionStartDate']));
		
		$addProcess['ProductionStartDate'] = $data['process']['ProductionStartDate'] ? $ProductionStartDate : '0000-00-00';
		$processAM = is_array($data['process']['AM']) ? $data['process']['AM'] : array($data['process']['AM']);
		if(!empty($data['process']['Process_leads']))
			$processLeads = implode(',', $data['process']['Process_leads']);
		$addProcess['min_hours'] = $data['process']['min_hours'] ? $data['process']['min_hours'] : NULL;
		
		if (empty($processID)) 
		{
			$checkProcess = $this->getValues(array('ClientID' => $addProcess['ClientID'], 'VerticalID' => $addProcess['VerticalID']), 'process');
			if($checkProcess)
			{
				foreach($checkProcess as $key => $val)
				{
					if(strtolower(trim($val['Name'])) == strtolower(trim($addProcess['Name']))){
						return 2;
					}
				}
			}
			$retVal = $this->insert($addProcess, 'process');
			$query_last_id = $this->db->query('SELECT LAST_INSERT_ID()');
			$this->log_action('process','add',$addProcess['Name']);
			$last_rec = $query_last_id->row_array();
			$insertId = $last_rec['LAST_INSERT_ID()'];
		}
		else 
		{
			$where = array();
			$where['ProcessID'] = $processID;
			$insertId = $processID;
				//add the action log table.
			$diffProcess = $addProcess;
			unset($diffProcess['Approver']);
			$differenceArray = array('ProcessID',$processID,$diffProcess);
			$this->log_action('process','update',$addProcess['Name'],$differenceArray);
			
			//$this->db->set('Approver', $addProcess['Approver']);
			$this->db->set('production_start_date', $addProcess['ProductionStartDate']);
			$this->db->set('Process_type', $addProcess['Process_type']);
			$this->db->set('process_description', $addProcess['process_description']);
			$this->db->set('process_status', $addProcess['process_status'][0]);
			$this->db->set('min_hours', $addProcess['min_hours']);
			
			if($addProcess['process_status'][0] == 1)
				$this->db->set('InactiveDate', date('Y-m-d'));
			
			$this->db->where('process_id', $processID);
			$this->db->update('process');
			
			$retVal = $this->db->affected_rows();			
			
			$editBool = true;
			if(is_array(json_decode($data['process']['TechnologyID'])))
				$this->delete($where, 'processtechnology');
			if(is_array(json_decode($data['process']['Attribute_Names'])))
				$this->delete($where, 'processattributes');
			$this->delete($where, 'process_approver');
		}
		if (empty($processID)) {
			$VerticalManager = $this->Rhythm_Model->getVerticalManager($addProcess['VerticalID']);
			if($VerticalManager)
			{
				if($VerticalManager[0]['EmployID'] != $processAM[0])
					$this->db->query("UPDATE `employees` SET `Primary_Lead_ID`=".$VerticalManager[0]['EmployID']."  WHERE `employee_id` = ".$processAM[0]);
			}
			if(!empty($processLeads))
			{
				$Leads = explode(',', $processLeads);
				if(($key = array_search($processAM[0], $Leads)) !== false) {
					unset($Leads[$key]);
				}
				$Leads = implode(',', $Leads);
				if($Leads!="")
					$this->db->query("UPDATE `employees` SET `Primary_Lead_ID`= ".$processAM[0]." WHERE `employee_id` IN($Leads)");
			}
		}
		 if ($retVal || $editBool) {
			if (empty($processID)) 
			{					
				$this->load->model('Employee_Model');
				$Grade = $this->Emp->getEmployeegrade($processAM[0]);
				$AmShiftId = $this->getAmShift($processAM[0]); //get AM ShiftID					
				$TlShiftId = $this->getTlShift($data['process']['Process_leads'][0]); //get TL shiftID										
				$Grades =  $Grade[0]['grades'];
				$this->db->query("INSERT INTO employprocess (process_id, employee_id, Grades,ShiftID) VALUES($insertId, '".$processAM[0]."',$Grades,'".$AmShiftId[0]['ShiftID']."')");					
				$leadGrade = $this->Emp->getEmployeegrade($data['process']['Process_leads'][0]);
				$LeadGrades =  $leadGrade[0]['grades'];
				if(!empty($processLeads))				
					$this->db->query("INSERT INTO employprocess (process_id, employee_id, Grades,ShiftID) VALUES($insertId, '".$data['process']['Process_leads'][0]."',$LeadGrades,'".$AmShiftId[0]['ShiftID']."')");	
				//Commented this in 2.0 version
				//	$this->__addProcessChilds($insertId, $data['process']['Process_leads'], 'employprocess', 'EmployID');
			}
			if(is_array(json_decode($data['process']['TechnologyID'])))
			{
				$techNames = json_decode($data['process']['TechnologyID']); 
									
				/* START Code to get multiple Technologies */
				$techArray = array();
				foreach ($techNames as $techName=>$techValues)
				{
					if(is_array($techValues))
					{
						foreach($techValues as $techValue)
						{
							$techArray[] =  $techValue;
						}
					}
					else
					{
						$techArray[] = $techValues;
					}
				}
				
				$techNames = $techArray;																
				/* END Code to get multiple Technologies */
				
				//echo '<pre>'; print_r($techNames); exit;
									
				if (empty($processID))//for insert and update
				{
					$query = $this->db->query('SELECT t.technology_id, t.`vertical_id`, t.Name FROM technology t LEFT JOIN PROCESS p ON t.`vertical_id` = p.`vertical_id` WHERE t.Name IN ("' . implode('", "', $techNames) . '") GROUP BY Name');						
				}
				else 
				{
					$query = $this->db->query('SELECT t.technology_id, t.`vertical_id`, t.Name FROM technology t LEFT JOIN PROCESS p ON t.`vertical_id` = p.`vertical_id` WHERE t.Name IN ("' . implode('", "', $techNames) . '") AND p.`process_id` = "'.$data['process']['id'].'" GROUP BY Name');
					
				}										
				
				$techNames = $query->result_array();
				
				//echo $this->db->last_query(); exit;
									
				$techIDs = $this->arrayFilter($techNames, 'technology_id');
									
				$this->__addProcessChilds($insertId, $techIDs, 'processtechnology', 'technology_id');
			}
			if(is_array(json_decode($data['process']['Attribute_Names'])))
			{	
				$attrNames = json_decode($data['process']['Attribute_Names']);
				/* START Code to get mulitple process attributes */
				$attrArray = array();					
				foreach ($attrNames as $attrName=>$attrValues)
				{						
					if(is_array($attrValues))
					{
						foreach($attrValues as $attrValue)
						{
							$attrArray[] =  $attrValue;
						}
					}
					else
					{
						$attrArray[] = $attrValues;
					}
				}					
				//$attrNames = $attrArray;
				/* END Code to get mulitple process attributes */
				$query = $this->db->query('SELECT attributes_id FROM attributes WHERE Name IN ("' . implode('", "', $attrArray) . '") GROUP BY Name ORDER BY FIELD(NAME, "' . implode('", "', $attrArray) . '" )');				
				$attrNames = $query->result_array();
				$attrIDs = $this->arrayFilter($attrNames, 'attributes_id');
				/* added and commented to check array of columns*/
				//$this->__addProcessChilds($insertId, $attrIDs, 'processattributes', array('attributes_id','OrderOfAttributes'));
				$this->__addProcessChilds($insertId, $attrIDs, 'processattributes', 'attributes_id');
			}
			
			if(is_array($data['process']['Approver']))
			{
				 $processApprover = array_filter($data['process']['Approver']);
				 $this->__addProcessChilds($insertId, $processApprover, 'process_approver', 'EmployID');
			}
		} 
			
		if($editBool) {
			return 1;
		} else {
			return $retVal;
		}
	}
	
	/*get AM shiftID */		 
	function getAmShift($employId)
	{
		$sql = " SELECT `ShiftID` FROM `employprocess` WHERE `employee_id` ='" . $employId . "' AND `ShiftID` != '' LIMIT 0,1";			
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return '';
	}
	
	/*get TL shiftID */
	function getTlShift($employId)
	{
		$sql = " SELECT `ShiftID` FROM `employprocess` WHERE `employee_id` ='" . $employId . "' AND `ShiftID` != '' LIMIT 0,1";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return '';
	}
							
	function __addProcessChilds( $idVal = '', $data = '', $table, $columnName) {
			
		$array_data =array();
		if(is_array($data) && count($data)!=0)
		{	
			/* added to test array of columns in  Process Attributes*/
			$counterattribues = 1;		
			$columns = array();	
			/* added to test array of columns in  Process Attributes*/
			foreach($data as $asctMngID){
				/* added to test array of columns in  Process Attributes*/
				if(is_array($columnName)){
		
					$columns['ProcessID'] = $idVal;
					$columns[$columnName[0]] = $asctMngID;
					$columns[$columnName[1]] = $counterattribues;
					$counterattribues++;
					array_push($array_data,$columns);
				}
				/* added to test array of columns in  Process Attributes*/					
				else
				array_push($array_data,array('ProcessID' =>$idVal, $columnName => $asctMngID));
			}
				//print_r($array_data);
			$retVal = $this->db->insert_batch($table, $array_data);
			return $retVal;
		}
	}
	
	
	/**
	* Function to get leads(3 & 3.1)
	*/
	function getLeads() 
	{
		$AM 		= $this->input->get('EmplyID') ? $this->input->get('EmplyID') : '';
		$ProID 		= $this->input->get('ProID') ? $this->input->get('ProID') : '';
		$filterName = $this->input->get('filterName') ? $this->input->get('filterName') : '';
		$filterVal 	= $this->input->get('query') ? $this->input->get('query') : '';
		$field		= array(); 
		$join	= ""; 
		$where = "";

		//Added to filter leads for HR pool who are also leads in HR Ops in transition screen
		$HR_OpsID 	= HR_PROCESS_ID;
		$ProcessId 	= ($ProID == INI_PROCESS_ID) ? $HR_OpsID : $ProID;

		if($ProID !="") 
		{
			$field = array(
				'Name' => " DISTINCT( employees.`employee_id` ), CONCAT(IFNULL(employees.`first_name`, ''), ' ',IFNULL(employees.`last_name`, '')) AS Name ",
				'Grade'	=> "designation.grades as Grade"			   
			);

			$join  = " LEFT JOIN employprocess ON employees.employee_id = employprocess.employee_id ";
			$where = " AND employprocess.process_id = ".$ProcessId." AND employprocess.grades >= 3 ";
			/* Added to exclude new grade4 and employees in onboarding ,for Reporting Leads*/
			if($ProID == ONBOARD_PROCESS_ID)
			{
				$where.= " AND employees.TrainingNeeded = 0 " ;
			}
			/* Added to exclude new grade4 and employees in onboarding ,for Reporting Leads*/
		} 
		else 
		{
			$field = array(
				'Name' => " DISTINCT( employees.`employee_id` ), CONCAT(Ifnull(employees.`first_name`, ''), ' ', Ifnull(employees.`last_name`, '')) AS Name ",
			);

			// $join  = " JOIN PROCESS ON process.vertical_id = employees.assignedvertical  ";
			$where = " AND designation.grades >= 3 AND designation.grades < 4  ";

			if($AM != "") 
			{
				$where .=	" AND employees.`Primary_Lead_ID` = $AM ";
			}
		}

		if($filterVal!="") 
		{
			if($filterName == 'Name') 
			{
				$where .= " AND CONCAT(IFNULL(employees.`first_name`, ''), ' ', IFNULL(employees.`last_name`, '')) LIKE '%".$filterVal."%'";
			} 
			else 
			{
				$where .= " AND $filterName LIKE '%".$filterVal."%'";
			}
		}

		$sql = " SELECT ".implode(",", $field)." FROM employees 
				LEFT JOIN designation ON designation.designation_id = employees.designation_id $join 
				WHERE employees.relieve_flag = 0 $where ORDER BY Name";	 
				// echo $sql; exit;
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}
	
	function getProcessAttributes() {
	
		$resultSet = $this->getValues(array(),'attributes');
		$totCount = $this->getTotalRecordCount(array(),'attributes');
			
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $resultSet));
			
		return $results;
	}
	
	function getActiveProcessCount($data) {
		$ActiveProCnt_sql = "SELECT COUNT(*) AS active_count FROM PROCESS WHERE client_id = '".$data['ClientID']."' AND process_status = 0";
		$query = $this->db->query($ActiveProCnt_sql);
		$result = $query->result_array();
		return $result[0]['active_count'];	
	}

	public function clientExecutiveReport($clientLoginId) {
		if(isset($clientLoginId) && $clientLoginId != 0 ) {
				$cm_month  = date("m",strtotime("-1 month"));
				$cm_year   = date("Y",strtotime("-1 month"));
				$pm_month  = date("m",strtotime("-2 month"));
				$pm_year   = date("Y",strtotime("-2 month"));				
				$pm1_month = date("m",strtotime("-3 month"));
				$pm1_year  = date("Y",strtotime("-3 month"));

			return "SELECT Group_concat(cm.utilization, ',', cm.error, ',', cm.month) AS tm, 
				   Group_concat(pm.utilization, ',', pm.error, ',', pm.month) AS sm, 
				   Group_concat(pm1.utilization, ',', pm1.error, ',', pm1.month) AS fm, 
				   process.name AS Name, process.process_id as processID,
				   COUNT(ed.PDFdoc) AS TotalDoc,
				   Date_format(process.production_start_date, '%d-%m-%Y') AS 
				   production_start_date 
			FROM   process 
				LEFT JOIN `executive_dashboard` ed
				 ON `process`.`process_id` = ed.`process_id`
				   JOIN `reports_month_view` cm 
					 ON `process`.`process_id` = cm.`process_id` 
						AND cm.month = '$cm_month' 
						AND cm.year  = '$cm_year'
				   LEFT JOIN `reports_month_view` pm 
						  ON `process`.`process_id` = pm.`process_id` 
							 AND pm.month = '$pm_month' 
							 AND pm.year  = '$pm_year'  
				   LEFT JOIN `reports_month_view` pm1 
						  ON `process`.`process_id` = pm1.`process_id` 
							 AND pm1.month = '$pm1_month' 
							 AND pm1.year  = '$pm1_year'
			WHERE  cm.client_id = '$clientLoginId' 
			GROUP  BY process.name ";
		}
	}
	
	public function getProcessByClient($clientID)
	{
		return	$this->getAllProcess($clientID);
	}
	
	function getAllProcess($comboClient)
	{
		$empData = $this->input->cookie();
		if(isset($empData['EmployID']) != "")
		$EmployID = $empData['EmployID'];
		if(isset($empData['Grade']) != "") 
		{
			$grade = $empData['Grade'];
			if($grade==1 || $grade==2)
			$sql = "SELECT process_id FROM employprocess WHERE employee_id =".$EmployID." AND (grades=1 OR grades=2)";
			if($grade >=3 && $grade < 4)
			$sql = "SELECT process_id FROM employprocess WHERE employee_id =".$EmployID." AND grades>=3 AND grades<4";
			if($grade==4)
			$sql = "SELECT process_id FROM employprocess WHERE employee_id =".$EmployID." AND grades=4";
			if($grade==5)
			$sql = "SELECT process_id FROM process WHERE process_status = 0 AND vertical_id IN (SELECT vertical_id FROM verticalmanager WHERE EmployID=".$EmployID.")";
		}
		if(isset($sql))
		$result_select = $this->db->query($sql);
		$processArray = array();
		if(isset($result_select) && count($result_select->result_array())!=0)
		{
			foreach($result_select->result_array() as $key => $val)
			array_push($processArray, $val['ProcessID']);
		}
		
		return $processArray;
	}

	public function getClientPhaseProcess($ClientID	= "") 
	{
		$results		= array();
		if(isset($ClientID) && !empty($ClientID)) 
		{
			$sql = "SELECT process_id AS processID, name AS Name FROM process WHERE process_status = 0 AND client_id = $ClientID ";

			$res		= $this->db->query($sql);
			$data   	= $res->result_array();
			$results	= array_merge(
				array('totalCount' => $res->num_rows()),
				array('data' => $data)
			);
			if(!empty($results)) 
			{
				return $results;
			} 
			else 
			{
				return $results;
			}
		}
	}
	
	public function getProIdFrmName($processName, $clientID = "") {
		 $res = "";
		if($processName) {
			if(!empty($clientID)) {
				$cond	= " AND client_id = $clientID ";
			} else {
				$cond	= "";
			}
			$sql_proID         = "SELECT process_id, Name FROM process 
									WHERE process_status=0 AND Name = '".$processName."' $cond ";
			$sql_proID_query   = $this->db->query($sql_proID);
			$sql_proID_data    = $sql_proID_query->result_array();
			if(!empty($sql_proID_data)) {
				$res    = array_shift($sql_proID_data);
				return $res['process_id'];
			} else {
				return $res;
			}
		}
	
	}
}

?>