<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designation_Model extends INET_Model
{

	function __construct()
	{
		$this->load->model('Employee_Model');
		$this->load->model('Hcm_Model');
		parent::__construct();
	}



	function build_query() 
	{
		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;
		$filterName = $this->input->get('filterName')?$this->input->get('filterName'):"Name";
		$DesignNameCombo = $this->input->get('DesignNameCombo');
		$filterQuery = $this->input->get('query');

		$this->db->select("*");
		$this->db->select("CONCAT(name, ' (', hcm_grade, ')') AS SkillDesignation");

		$this->db->from('designation');

		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField;
						$filterWhere .=" LIKE '%".$filterArray['value']."%'";
					}
				}
			}

			if($filterWhere != '') {
				$this->db->where('('.$filterWhere.')');
			} 
		}

		if($this->input->get('DesignationID') != '')
		{
			$this->db->where('designation_id',$this->input->get('DesignationID'));
		}
		if($this->input->get('Grade') != '')
		{
			$this->db->where('hcm_grade',$this->input->get('Grade'));
		}
		if($this->input->get('DesignNameCombo') != '')
		{
			$this->db->where('designation_id !=', $DesignNameCombo);
		}
		if($this->input->get('PromoteToGrades') != '')
		{
			$Promoteto=$this->input->get('PromoteToGrades');
			$gradess = "grades >= '".$Promoteto."' AND designation_id != '".$this->input->get('noToGrades')."'";
			$this->db->where($gradess);
		}
		if($filterName !="" && $filterQuery!=""){

			if($filterName == 'SkillDesignation')
			{
				$filterWhere = " name LIKE '%".$filterQuery."%' OR hcm_grade LIKE '%".$filterQuery."%'";
			}
			else
			{

				$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			}
			$this->db->where($filterWhere);
		}
		$this->db->where('status', '1');

	}

	function getDesignationData()
	{
		$options = array();
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}

		$sort = json_decode($this->input->get('sort'),true);
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$this->build_query();
		$totQuery = $this->db->get();
		$totCount = $totQuery->num_rows();

		$this->build_query();
		if(isset($options['sortBy'])) 
		{
			$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		}

		if($this->input->get('hasNoLimit') != '1' && isset($options['limit']) && isset($options['offset'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}

		$query = $this->db->get();
//echo $this->db->last_query();
//exit;
		$results = array_merge(array('totalCount' => $totCount),array('designation' => $query->result_array()));

		return $results;
	}


	function getGradeData()
	{
		$filterName = $this->input->get('filterName')?$this->input->get('filterName'):"hcm_grade";
		$filterQuery = $this->input->get('query');
		$options = array();

		$sort = json_decode($this->input->get('sort'),true);

		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$this->db->distinct('hcm_grade');
		$this->db->select('hcm_grade');
		$this->db->where('status', '1');
		$where = "hcm_grade IS  NOT NULL";
		$this->db->where($where);
		if($this->input->get('PromoteToGrades') != '')
		{
			$Promoteto=$this->input->get('PromoteToGrades');
			$gradess = "grades >= '".$Promoteto."' AND designation_id != '".$this->input->get('noToGrades')."'";
			$this->db->where($gradess);
		}
		if($filterName !="" && $filterQuery!="")
		{
			$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			$this->db->where($filterWhere);
		}
		$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		$totQuery = $this->db->get('designation');
		$totCount = $totQuery->num_rows();

		$this->db->distinct('hcm_grade');
		$this->db->select('hcm_grade');
		$this->db->where('status', '1');
		$where = "hcm_grade IS  NOT NULL";
		$this->db->where($where);
		if($this->input->get('PromoteToGrades') != '')
		{
			$Promoteto=$this->input->get('PromoteToGrades');
			$gradess = "grades >= '".$Promoteto."' AND designation_id != '".$this->input->get('noToGrades')."'";
			$this->db->where($gradess);
		}
		if($filterName !="" && $filterQuery!="")
		{
			$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			$this->db->where($filterWhere);
		}
		$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		$query = $this->db->get('designation');

		$results = array_merge(array('totalCount' => $totCount),array('grade' => $query->result_array()));
		return $results;
	}

	function addDesignation( $data = '' ) 
	{
		$addDesignation = json_decode(trim($data), true);

		if($addDesignation['PromoteToFlag']==0)
		{
			$dataset = array(
				'name'=> $addDesignation['name'],
				'grades'=>$addDesignation['grades']
			);

			//check designation duplicates
			$checkDuplicate = $this->checkDesignationDuplicates($addDesignation['name'], $addDesignation['grades'], 'add');				
			$duplicateDesignationFlag = is_array($checkDuplicate) ? '1' : '0';				
			if($duplicateDesignationFlag == 1)
			{ 
				return 3;
			}

			$retVal = $this->insert($dataset, 'designation');
		}
		else
		{
			return 3;
		}

		return $retVal;
	}

	//updateDesignation
	function updateDesignation( $idVal = '', $data = '' ) 
	{
		$where = array();
		$set = array();
		$where['designation_id'] = $idVal;
		$setData = json_decode(trim($data), true);

		$dataset = array(
			'designation_id'=> $setData['designation_id'],
			'name'=> $setData['name'],
			'grades'=>$setData['grades']
		);

		//check designation duplicates
		$checkDuplicate = $this->checkDesignationDuplicates($setData['name'], $setData['grades'], 'edit');
		$duplicateDesignationFlag = is_array($checkDuplicate) ? '1' : '0';
		if($duplicateDesignationFlag == 1)
		{ 
			return 3;
		}

		$retVal = $this->update($dataset, $where, 'designation');
		return $retVal;
	}

	function deleteDesignation( $idVal = '' ) 
	{
		$options = array();
		$options['designation_id'] =  $idVal;
		$retVal = $this->delete($options, 'designation');
		return $retVal;
	}

	function checkDesignationDuplicates($designationName, $grade, $action)
	{
		if ($action == 'add')
		{
			$sql = "SELECT * FROM `designation` WHERE `name` LIKE '" . $designationName . "'";				
		}
		else
		{
			$sql = "SELECT * FROM `designation` WHERE grades = $grade AND `name` LIKE '" . $designationName . "'";				
		}

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return '';
	}

}
?>