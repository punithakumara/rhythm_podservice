<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qualification_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function build_query() 
	{
		$filterName = $this->input->get('filterName')?$this->input->get('filterName'):"name";
		$filterQuery = $this->input->get('query');
		
		$this->db->from('qualifications');
		
		$filterWhere = '';
		if(($filterName !="") && ($filterQuery != ''))
		{
			$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			$this->db->where($filterWhere);
		}
		
		if($this->input->get('QualificationID') != '')
		{
			$this->db->where('QualificationID',$this->input->get('QualificationID'));
		}
		
	}

	function getQualificationData()
	{
		$options = array();
		$sort = json_decode($this->input->get('sort'),true);

		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		
		$this->build_query();
		$totQuery = $this->db->get();
		$totCount = $totQuery->num_rows();
		
		$this->build_query();
		if(isset($options['sortBy'])) 
		{
			$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		}
			
		$query = $this->db->get();
		
		$results = array_merge(array('totalCount' => $totCount),array('qualifications' => $query->result_array()));

		return $results;
	}
}
?>