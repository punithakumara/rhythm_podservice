<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Employees_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_employees()
	{
		$empData    = $this->input->cookie();
       // echo'<pre>';print_r($empData );exit;
		$filter				= json_decode($this->input->get('filter'),true);
		$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";
		$empStatusCombo		= $this->input->get('empStatusCombo')?$this->input->get('empStatusCombo'):"";
		$comboClient		= $this->input->get('comboClient')?$this->input->get('comboClient'):"";
		$projectCode		= $this->input->get('projectCode')?$this->input->get('projectCode'):"";
		$filterQuery 		= $this->input->get('query');
		$sort				= json_decode($this->input->get('sort'),true);
		$empTypes      		= $this->input->get('empTypes') ? $this->input->get('empTypes') : "";
		$utilCombo      	= $this->input->get('utilCombo') ? $this->input->get('utilCombo') : "";
		$wor_id      	= $this->input->get('wor_id') ? $this->input->get('wor_id') : "";
		if($wor_id != "") { $addWorSql = ' and ec.wor_id = '.$wor_id;} else { $addWorSql = '';}

		$options = array();
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}

		$options['filter']	= $filter;
		
		$sortBy = $sort[0]['property'];
		$sortDirection = $sort[0]['direction'];
		

		$sql = "SELECT employees.employee_id,employees.company_employ_id,employees.primary_lead_id,employees.first_name,employees.last_name,s.shift_code,employees.service_id, 
		GROUP_CONCAT(v.name) AS vertical_name,employees.email,employees.mobile,DATE_FORMAT(employees.doj, '%d-%b-%Y') AS doj, d.grades AS grade_level,
		DATE_FORMAT(employees.dob, '%d-%b-%Y') AS dob,employees.gender,q.name AS qualification_name,ev.vertical_id, sv.name AS service_name,
		employees.location_id,employees.shift_id,employees.qualification_id,loc.name AS location_name, d.hcm_grade as Grade,
		employees.relieve_date,employees.personal_email,employees.alternate_number,employees.permanent_address,employees.permanent_city,
		employees.permanent_state,employees.permanent_zipcode,employees.same_as,employees.present_address,employees.present_city,
		employees.present_state,employees.present_zipcode,employees.status as status, d.designation_id AS designation_id,
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `full_name`,
		CONCAT(m.first_name, ' ', m.last_name) AS lead_name, employees.is_onshore, d.name as Designation_name 
		FROM employees ";
		$sql .= " LEFT JOIN designation d ON d.designation_id = employees.designation_id";	
		$sql .= " LEFT JOIN employee_vertical ev ON ev.employee_id = employees.employee_id";
		$sql .= " LEFT JOIN verticals v ON v.vertical_id = ev.vertical_id";
		$sql .= " LEFT JOIN qualifications q ON q.qualification_id = employees.qualification_id ";
		$sql .= " LEFT JOIN tbl_rhythm_services sv ON sv.service_id = employees.service_id ";
		$sql .= " LEFT JOIN location loc ON loc.location_id = employees.location_id ";
		$sql .= " LEFT JOIN employees m  ON employees.primary_lead_id = m.employee_id ";
		$sql .= " LEFT JOIN shift s ON s.shift_id = employees.shift_id ";
		

		if ($utilCombo != "")
		{
			$sql .= " LEFT JOIN `employee_client` ec ON ec.`employee_id` = employees.`employee_id` ";
		}

		$sql .= " WHERE 1=1 ";
		
		if($empStatusCombo == 0)
		{
			$sql .= ' AND employees.status IN(1,2,3,4,7)';
		}
		if($empData['is_onshore']==1)
		{
			$sql .= ' AND employees.location_id=4 ';
		}
		if($empStatusCombo == 1 )
		{
			$sql .= ' AND employees.status = 6 ';
		}
		if($empStatusCombo == 2 )
		{
			$sql .= ' AND employees.status = 5';
		}   
		if($utilCombo != "" && ($comboClient!="")){
			$sql .= ' AND  ec.client_id = '.$comboClient;
		}
		
		if($comboClient != "" && ($projectCode!="")){

			$projectId = $this->getProjectId($projectCode);		   		   
			$sql .= ' AND  ec.process_id = '.$projectId['0']['process_id'];
		}

		/* if ($utilCombo != "")
		{
			$sql .= ' and ec.grades < 3 '.$addWorSql;
		} */
		if (is_array($this->getEmployeeList($empData['employee_id'])))
			$employeeArray = implode(',', $this->getEmployeeList($empData['employee_id']));
		
		if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['VertId']!='10')){
			$sql .= '  AND employees.employee_id IN ('.$employeeArray.')';
		}
		
		$filterWhere = '';
		
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if($filterField != '') 
					{
						if(trim($filterField) == "employees.first_name") 
						{
							$filterField = " REPLACE(CONCAT(employees.first_name, ' ',employees.last_name), ' ', ' ') " ;
						}
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						if(isset($filterArray['value']))
						{
							$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
						}
					}
				}
			}
		}
		
		if ($filterName != "") 
		{
			$filterWhere .= $filterName . " LIKE '%" . $filterQuery . "%'";
		}
		
		//if($wor_id == "")
		//{
		if($filterWhere !="")
		{
			$sql .= " and (".$filterWhere.")";    			
		}
		//}
		
		if (isset($empTypes) && $empTypes != "" && $empTypes == "grade5AndAbove") {
			$sql .= ' and d.grades  >= 5';
		}
		if (isset($empTypes) && $empTypes != "" && $empTypes == "grade5") {
			$sql .= ' and d.grades  >= 5 and d.grades  <= 6';
		}

		if($sortBy == "company_employ_id")
		{
			$sortBy = "CAST(employees.company_employ_id AS SIGNED INTEGER)";
		}
		$sql .= " GROUP BY employees.employee_id";
		$sql .= " ORDER BY ". $sortBy." ".$sortDirection;
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " limit ".$limit;
		}
		//echo $sql;exit;

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		// echo $this->db->last_query(); exit;
		//echo "<PRE>";print_r($results);exit;
		return $results;
	}	

	function getEmployeeNotifications($id) {     
		if(is_numeric($id)) {

			$countNot = 0;
			$res          = array();    		
			$Valid_Date   = date('Y-m-d', strtotime('-7 day'));
			$sql         = "SELECT NotificationID, NotificationDate, ReadFlag, Type FROM notification
			WHERE ReceiverEmployee REGEXP '(^|,)".$id."(,|$)' AND ReadFlag = '0'"; 
			
			$query = $this->db->query($sql);
			/* START code for resticting future date MOM notifications count */
			$data = $query->result_array();

			if(count($data) > 0)
			{
				$countNot = count($data);
			}

			$res['count'] = count($data);
			$res['rflag'] = $data;
			return $res;
		}
	}

	function getNotifications()
	{
		$limitoten = $this->input->get('limitoten');
		$options = array();
		if ($this->input->get('hasNoLimit') != 1) {
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 15;
		}
		$sort                     = json_decode($this->input->get('sort'), true);
		$options['sortBy']        = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$res        = $this->buildNotify_query();
		$totquery   = $this->db->query($res);
		$totCount   = $totquery->num_rows();

		$res        = $this->buildNotify_query();

    	//This limit for Notfication pop up display only top ten records
		if($limitoten == 'limitoten' && isset($limitoten)) {
			$Valid_Date = date('Y-m-d', strtotime('-7 day'));
    		// $res .= " AND n.NotificationDate >= ". $Valid_Date . " ORDER BY  n.NotificationDate DESC ";
			$res .= " ORDER BY  n.NotificationDate DESC ";
			$options['offset'] = 0;
			$options['limit']  = 10;
		}

		if ($this->input->get('sort') != "") {
			$sort                     = json_decode($this->input->get('sort'), true);
			$options['sortBy']        = $sort[0]['property'];
			$options['sortDirection'] = $sort[0]['direction'];
			$sort                     = $options['sortBy'] . ' ' . $options['sortDirection'];
			$res .= " order by " .  $sort;
		}

		if (isset($options['limit']) && isset($options['offset'])) {
			$limit = $options['offset'] . ',' . $options['limit'];
			$res .= " limit " . $limit;
		} elseif (isset($options['limit'])) {
			$res .= " limit " . $options['limit'];
		}
		$query = $this->db->query($res);
		$data  = $query->result_array();

		/* START code for resticting future date MOM notifications */
		$finalData = array();
		date_default_timezone_set('Asia/Kolkata');

    	//Construct new array containing only current date and below Mom notifications
		foreach ($data as $key=>$value)
		{
			$splitTimeStamp = explode(" ", $value['NotificationDate']);
			$form           = str_replace('-', '/', $splitTimeStamp[0]);
			$nd             = date("d-M-Y", strtotime($form));

			if($value['Type']==5)
			{
				$Description = $value['Description'];
			}
			else
			{
				$Description = strlen($value['Description']) > 80 ? substr($value['Description'],0,80)."..." : $value['Description'];
			}

			$finalData[$key]['NotificationID']      = $value['NotificationID'];
			$finalData[$key]['Title'] = $value['Title'];
			$finalData[$key]['Description'] = $Description;
			$finalData[$key]['SenderEmployee'] = $value['SenderEmployee'];
			$finalData[$key]['ReceiverEmployee'] = $value['ReceiverEmployee'];
			$finalData[$key]['NotificationDate'] = $nd .' '. $splitTimeStamp[1];;
			$finalData[$key]['ReadFlag'] = $value['ReadFlag'];
			$finalData[$key]['ReadDate'] = $value['ReadDate'];
			$finalData[$key]['Type'] = $value['Type'];
			$finalData[$key]['employee_id'] = $value['employee_id'];
			$finalData[$key]['first_name'] = $value['first_name'];
			$finalData[$key]['last_name'] = $value['last_name'];
		}

		/* END code for resticting future date MOM notifications */
    	//ob_start();

		$finalData = array_values($finalData);

		$results = array_merge(array(
    			'totalCount' => count($finalData) //$totCount
    		), array(
    			'rows' => $finalData
    		));
		return $results;
	}

	public function buildNotify_query()
	{
		$filter     = json_decode($this->input->get('filter'), true);
		$empData    = $this->input->cookie();
		if(isset($empData['employee_id'])) {
			$sql = 'SELECT `n`.`NotificationID`,
			`n`.`Title`, `n`.`Description`, `n`.`SenderEmployee`,
			`n`.`ReceiverEmployee`, DATE_FORMAT(`n`.NotificationDate, "%m-%d-%Y %T") AS NotificationDate,
			`n`.`ReadFlag`, `n`.`ReadDate`,`n`.`Type`, `e`.`employee_id`,
			`e`.`first_name`, `e`.`last_name`, n.Type FROM (`notification` n)
			LEFT JOIN `employees` e ON `e`.`employee_id` = `n`.`SenderEmployee`
			WHERE (LOCATE(",'.$empData["employee_id"].',", CONCAT(",",n.ReceiverEmployee,","))) AND ReadFlag=0';

			return $sql;
		}

	}
	
	function getReportToDetails()
	{
		$empData    = $this->input->cookie();
		$empid = $this->input->get('EmployId');

		$sql = "SELECT employees.employee_id,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS Name
		FROM employees 
		JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
		JOIN designation ON designation.designation_id=employees.designation_id 
		WHERE employees.status !=6  AND designation.grades>2 ";
		
		if($empid != '')
		{
			$sql.= " AND employees.employee_id NOT IN($empid)";
		}
		

		if($empData['Grade']>2 && ($empData['Grade']<4) && ($empData['VertId']!='10'))
		{
			$sql .= " AND designation.grades<4";
		}
		else if($empData['Grade']==4 && ($empData['VertId']!='10'))
		{
			$sql .= " AND designation.grades<5";
		}
		else if($empData['is_onshore']==1 && ($empData['VertId']!='29'))
		{
			$sql .= " AND designation.grades=7";
		}
		else if($empData['is_onshore']==1 && ($empData['VertId']!='30'))
		{
			$sql .= " AND designation.grades=7";
		}
		if ($this->input->get('PodId')) 
		{
			$sql .= " AND employee_pod.pod_id ='" . $this->input->get('PodId') . "'";
		}
		$options = array();

		if ($this->input->get_post('hasNoLimit') != '1') 
		{
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
		}

		$sort = json_decode($this->input->get('sort'), true);

		$options['sortBy']        = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$totQuery = $this->db->query($sql);
		$totCount = $totQuery->num_rows();
		
		if ($this->input->get('query') != "") 
		{
			$sql .= " AND (employees.first_name like '" . $this->input->get('query') . "%')";
		}
		if (isset($options['sortBy'])) 
		{
			$sort = $options['sortBy'] . ' ' . $options['sortDirection'];
			$sql .= " ORDER BY " . $sort;
		}
		// echo $sql;
		$query   = $this->db->query($sql);
		$data    = $query->result_array();
		
		$results = array_merge(array(
			'totalCount' => $totCount
		), array(
			'rows' => $data
		));
		
		return $results;
	}

	function getAllReportDetails()
	{
		$options = array();
		$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
		
		$wor_id = ($this->input->get('wor_id') != '')? $this->input->get('wor_id') : '';
		//$project_id = ($this->input->get('ProjectID') != '')? $this->input->get('ProjectID') : '';
		$project_id = $this->get_project_id($wor_id);
		
		$client_id = ($this->input->get('client_id') != '')? $this->input->get('client_id') : '';
		

		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;

		$sort = json_decode($this->input->get('sort'),true);					
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		$empData = $this->input->cookie();
		$searchsql = "";

		$manager = $empData['employee_id'] ? $empData['employee_id']:0;	
		$searchTxt = $this->input->get('searchTxt') ? $this->input->get('searchTxt'):"";
		$NoHrPoolEmployees = $this->input->get('NoHrPoolEmployees') ? $this->input->get('NoHrPoolEmployees'):"";	
		$extraSQL = '';
		$noemployeesSQL = '';
		if($empData['Grade'] < 6 && $empData['VertId'] != 10)
		{			
			$employeesids = $this->getReportEmployeeIds($manager, $searchTxt);													
			if (is_array($employeesids))
			{
				$oneDimensionalArray = array_map('current', $employeesids);		
				$employids = implode(",",$oneDimensionalArray);	
			}
			else
			{
				$employids = 0;
			}
			// echo "<pre>"; print_r($employids); exit;
			$extraSQL = ' AND temp.employee_id IN ('.$employids.')';
		}
		
		if($searchTxt <> "")
		{
			$searchsql = "AND (e.company_employ_id LIKE '%".$searchTxt."%' OR e.first_name LIKE '%".$searchTxt."%' OR e.last_name LIKE '%".$searchTxt."%')";
		}
		
		if($project_id <> "" && $client_id <> "" && $wor_id <> "")
		{
			$getResourcesTwo = $this->getCurrentAllocatedResources($project_id, $client_id, $wor_id);
			if ($getResourcesTwo <> 0)
			{
				$getResourcesOne = array_map('current', $getResourcesTwo);
				$noemployids = implode(",",$getResourcesOne);	
				$noemployeesSQL = ' AND temp.employee_id NOT IN ('.$noemployids.')';
			}
		}
		
		$noHrPoolSQL="";
		if($NoHrPoolEmployees!="")
		{
			$noHrPoolSQL = " AND e.status NOT IN(1,2,7)";
		}
		$sql = "SELECT CAST(e.employee_id AS UNSIGNED INTEGER) as employee_id, CONCAT_WS(' ',e.first_name,e.last_name) AS FullName, 
		e.Email, d.grades, d.Name AS designationName,e.company_employ_id AS company_employ_id, m.employee_id AS supervisor_id,
		CONCAT(m.first_name, ' ', m.last_name) AS supervisor, e.training_status,e.shift_id, if(e.status=7,1,0) AS training_needed,s.shift_code AS current_shift, p.pod_name AS podName, se.name AS serviceName
		FROM (SELECT employee_id FROM employees) AS temp 
		JOIN employees e ON e.employee_id = temp.employee_id
		LEFT JOIN shift s ON s.shift_id = e.shift_id
		LEFT JOIN employee_pod ep ON ep.employee_id = e.employee_id
		LEFT JOIN tbl_rhythm_services se ON se.service_id = e.service_id
		LEFT JOIN tbl_rhythm_pod p ON p.pod_id = ep.pod_id
		LEFT JOIN  employees m  ON e.primary_lead_id = m.employee_id
		JOIN designation d ON e.designation_id = d.designation_id 
		WHERE e.status != 6 AND e.location_id <> 4 $searchsql  $extraSQL $noemployeesSQL $noHrPoolSQL				
				GROUP BY e.employee_id ";// ORDER BY d.grades DESC,CID ASC
				//echo $sql;exit;

				$filterWhere = '';
				if(isset($options['filter']) && $options['filter'] != '') 
				{
					foreach ($options['filter'] as $filterArray) 
					{
						$filterFieldExp = explode(',', $filterArray['property']);
						foreach($filterFieldExp as $filterField) {
							if($filterField != '') {
								$filterWhere .= ($filterWhere != '')?' OR ':'';
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}
					if($filterWhere != '') 
					{
						$filterWhere = ' ( '.$filterWhere.' ) ';
						$sql .= " and (".$filterWhere.")";
					}
				}

				if(isset($options['sortBy'])) 
				{
					if ($options['sortBy'] == 'FullName') $options['sortBy'] = 'e.first_name';

					if($options['sortBy'] == "company_employ_id")
					{
						$options['sortBy'] = "CAST(e.company_employ_id AS SIGNED INTEGER)";
					}

					$sort = $options['sortBy'].' '.$options['sortDirection'];
					$sql .= " ORDER BY ".$sort;
				}

				$totQuery = $this->db->query($sql);
				$totCount = $totQuery->num_rows();

				if (isset($options['limit']) && isset($options['offset'])) 
				{
					$limit = $options['offset'] . ',' . $options['limit'];
					$sql .= " LIMIT " . $limit;
				} 
				else if (isset($options['limit'])) 
				{
					$sql .= " LIMIT " . $options['limit'];
				}

				$query = $this->db->query($sql);
				$data = $query->result_array();

				$results = array_merge(array('totalCount' => $totCount),array('data' => $data));
				return $results;
			}
	
	
	function getAllReportDetails_1()
	{
		// debug($_GET);exit;
		$options = array();
		$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
		
		$wor_id = ($this->input->get('wor_id') != '')? $this->input->get('wor_id') : '';
		//$project_id = ($this->input->get('ProjectID') != '')? $this->input->get('ProjectID') : '';
		$project_id = $this->get_project_id($wor_id);
		
		$client_id = ($this->input->get('client_id') != '')? $this->input->get('client_id') : '';
		

		$filter = json_decode($this->input->get('filter'),true);
		// debug($filter);exit;
		$options['filter'] = $filter;

		$sort = json_decode($this->input->get('sort'),true);					
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		$empData = $this->input->cookie();
		$searchsql = "";

		$manager = $empData['employee_id'] ? $empData['employee_id']:0;	
		$searchTxt = $this->input->get('searchTxt') ? $this->input->get('searchTxt'):"";
		$NoHrPoolEmployees = $this->input->get('NoHrPoolEmployees') ? $this->input->get('NoHrPoolEmployees'):"";	
		// $extraSQL = '';
		$noemployeesSQL = '';
		
		if($searchTxt <> "")
		{
			$searchsql = "AND (e.company_employ_id LIKE '%".$searchTxt."%' OR e.first_name LIKE '%".$searchTxt."%' OR e.last_name LIKE '%".$searchTxt."%' OR v.name LIKE '%".$searchTxt."%' OR d.name LIKE '%".$searchTxt."%' OR e.email LIKE '%".$searchTxt."%' OR m.first_name LIKE '%".$searchTxt."%')";
		}
		
		if($project_id <> "" && $client_id <> "" && $wor_id <> "")
		{
			$getResourcesTwo = $this->getCurrentAllocatedResources($project_id, $client_id, $wor_id);
			if ($getResourcesTwo <> 0)
			{
				$getResourcesOne = array_map('current', $getResourcesTwo);
				$noemployids = implode(",",$getResourcesOne);	
				$noemployeesSQL = ' AND temp.employee_id NOT IN ('.$noemployids.')';
			}
		}
		
		$noHrPoolSQL="";
		if($NoHrPoolEmployees!="")
		{
			$noHrPoolSQL = " AND e.status NOT IN(1,2,7)";
		}
		$sql = "SELECT CAST(e.employee_id AS UNSIGNED INTEGER) as employee_id, CONCAT_WS(' ',e.first_name,e.last_name) AS FullName, 
		e.Email, d.grades, d.Name AS designationName,e.company_employ_id AS company_employ_id, m.employee_id AS supervisor_id,
		CONCAT(m.first_name, ' ', m.last_name) AS supervisor, e.training_status,e.shift_id, if(e.status=7,1,0) AS training_needed,s.shift_code AS current_shift, se.name AS serviceName, p.pod_name AS podName
		FROM (SELECT employee_id FROM employees) AS temp 
		JOIN employees e ON e.employee_id = temp.employee_id
		LEFT JOIN shift s ON s.shift_id = e.shift_id
		-- LEFT JOIN employee_vertical ev ON ev.employee_id = e.employee_id
		LEFT JOIN employee_pod ep ON ep.employee_id = e.employee_id
		LEFT JOIN tbl_rhythm_services se ON se.service_id = e.service_id
		LEFT JOIN tbl_rhythm_pod p ON p.pod_id = ep.pod_id
		LEFT JOIN  employees m  ON e.primary_lead_id = m.employee_id
		JOIN designation d ON e.designation_id = d.designation_id 
		WHERE e.status != 6 AND e.location_id <> 4 $searchsql $noemployeesSQL $noHrPoolSQL				
				GROUP BY e.employee_id ";// ORDER BY d.grades DESC,CID ASC
				

				$filterWhere = '';
				if(isset($options['filter']) && $options['filter'] != '') 
				{
					foreach ($options['filter'] as $filterArray) 
					{
						$filterFieldExp = explode(',', $filterArray['property']);
						foreach($filterFieldExp as $filterField) {
							if($filterField != '') {
								$filterWhere .= ($filterWhere != '')?' OR ':'';
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}
					if($filterWhere != '') 
					{
						$filterWhere = ' ( '.$filterWhere.' ) ';
						$sql .= " and (".$filterWhere.")";
					}
				}

				if(isset($options['sortBy'])) 
				{
					if ($options['sortBy'] == 'FullName') $options['sortBy'] = 'e.first_name';

					if($options['sortBy'] == "company_employ_id")
					{
						$options['sortBy'] = "CAST(e.company_employ_id AS SIGNED INTEGER)";
					}

					$sort = $options['sortBy'].' '.$options['sortDirection'];
					$sql .= " ORDER BY ".$sort;
				}

				$totQuery = $this->db->query($sql);
				$totCount = $totQuery->num_rows();

				if (isset($options['limit']) && isset($options['offset'])) 
				{
					$limit = $options['offset'] . ',' . $options['limit'];
					$sql .= " LIMIT " . $limit;
				} 
				else if (isset($options['limit'])) 
				{
					$sql .= " LIMIT " . $options['limit'];
				}

				$query = $this->db->query($sql);
				$data = $query->result_array();

				$results = array_merge(array('totalCount' => $totCount),array('data' => $data));
				return $results;
			}

			/* Get Currently avalible Resources from HR Pool*/
			function getPullFromHRDetails()
			{
				$options = array();
				$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
				$searchsql = "";

				$searchTxt = $this->input->get('searchTxt') ? $this->input->get('searchTxt'):"";

				$filter = json_decode($this->input->get('filter'),true);
				$options['filter'] = $filter;

				$sort = json_decode($this->input->get('sort'),true);					
				$options['sortBy'] = $sort[0]['property'];
				$options['sortDirection'] = $sort[0]['direction'];
				$empData = $this->input->cookie();

				if($searchTxt <> "")
				{
					$searchsql = "AND (e.company_employ_id LIKE '%".$searchTxt."%' OR e.first_name LIKE '%".$searchTxt."%' OR e.last_name LIKE '%".$searchTxt."%')";

					$options['offset'] = 0;

				}


				$sql = "SELECT CAST(e.employee_id AS UNSIGNED INTEGER) as employee_id, CONCAT_WS(' ',e.first_name,e.last_name) AS FullName, 
				e.Email, d.grades, d.Name AS designationName,e.company_employ_id AS company_employ_id, se.name AS serviceName, p.pod_name AS podName,
				CONCAT(m.first_name, ' ', m.last_name) AS supervisor, e.training_status,e.shift_id, if(e.status=7,1,0) AS training_needed,s.shift_code AS current_shift,e.status AS status
				FROM (SELECT employee_id FROM employees) AS temp 
				JOIN employees e ON e.employee_id = temp.employee_id
				LEFT JOIN shift s ON s.shift_id = e.shift_id
				LEFT JOIN employee_pod ep ON ep.employee_id = e.employee_id
				LEFT JOIN tbl_rhythm_services se ON se.service_id = e.service_id
				LEFT JOIN tbl_rhythm_pod p ON p.pod_id = ep.pod_id
				LEFT JOIN  employees m  ON e.primary_lead_id = m.employee_id
				JOIN designation d ON e.designation_id = d.designation_id 
				WHERE e.status IN(2,4) AND e.location_id <> 4 $searchsql				
				GROUP BY e.employee_id ";// ORDER BY d.grades DESC,CID ASC
			//echo $sql; exit;	

				$filterWhere = '';
				if(isset($options['filter']) && $options['filter'] != '') 
				{
					foreach ($options['filter'] as $filterArray) 
					{
						$filterFieldExp = explode(',', $filterArray['property']);
						foreach($filterFieldExp as $filterField) {
							if($filterField != '') {
								$filterWhere .= ($filterWhere != '')?' OR ':'';
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}
					if($filterWhere != '') 
					{
						$filterWhere = ' ( '.$filterWhere.' ) ';
						$sql .= " and (".$filterWhere.")";
					}
				}

				if(isset($options['sortBy'])) 
				{
					if ($options['sortBy'] == 'FullName') $options['sortBy'] = 'e.first_name';

					if($options['sortBy'] == "company_employ_id")
					{
						$options['sortBy'] = "CAST(e.company_employ_id AS SIGNED INTEGER)";
					}

					$sort = $options['sortBy'].' '.$options['sortDirection'];
					$sql .= " ORDER BY ".$sort;
				}

		//echo $sql; exit;
				$totQuery = $this->db->query($sql);
				$totCount = $totQuery->num_rows();

				if (isset($options['limit']) && isset($options['offset'])) 
				{
					$limit = $options['offset'] . ',' . $options['limit'];
					$sql .= " LIMIT " . $limit;
				} 
				else if (isset($options['limit'])) 
				{
					$sql .= " LIMIT " . $options['limit'];
				}

				$query = $this->db->query($sql);
				$data = $query->result_array();

				$results = array_merge(array('totalCount' => $totCount),array('data' => $data));

				return $results;
			}

			/*Get project ID for wor by wor_id*/
			function get_project_id($wor_id)
			{
				$sql = "SELECT  DISTINCT project_type_id FROM employee_client WHERE wor_id = '".$wor_id."'";                            
				$query = $this->db->query($sql);
				$data =  $query->result_array();
				$process_id_data = array();
				foreach ($data as $key => $value) {
					$process_id_data[] = $value['project_type_id'];
				}
				$data = implode(',',$process_id_data);
				return $data;
			}
			/*Get lead ID for zoho data*/
			function get_lead_id($zoho_lead_id)
			{
				$sql = "SELECT  employee_id FROM employees WHERE company_employ_id ='".$zoho_lead_id."'";                            
				$query = $this->db->query($sql);
				$data =  $query->result_array();
				return $data[0]['employee_id'];
			}
			
			/*Get designation ID for zoho data*/
			function get_designation_id($designation)
			{
				$sql = "SELECT designation_id FROM designation WHERE name LIKE '%".$designation."%'";                            
				$query = $this->db->query($sql);
				$data =  $query->result_array();
				return $data[0]['designation_id'];
			}
			
			/*Get vertical ID for zoho data*/
			function get_vertical_id($vertical)
			{
				$sql = "SELECT vertical_id FROM verticals WHERE name LIKE '%".$vertical."%'"; 
				echo $sql;
				$query = $this->db->query($sql);
				$data =  $query->result_array();
				return $data[0]['vertical_id'];
			}
			
			/*Get location ID for zoho data*/
			function get_location_id($location)
			{
				$sql = "SELECT location_id FROM location WHERE name LIKE '%".$location."%'"; 		
				$query = $this->db->query($sql);
				$data =  $query->result_array();
				return $data[0]['location_id'];
			}

			/* Get Currently Allocated Resources*/
			function getCurrentAllocatedResources($project_id, $client_id, $wor_id)
			{
				$sql = "SELECT employee_id FROM employee_client WHERE `client_id` = $client_id AND `project_type_id` IN (".$project_id.") AND `wor_id` = $wor_id";

				$query = $this->db->query($sql);
				if ($query->num_rows () > 0) 
				{
					return $query->result_array ();
				}

				return 0;
			}

			function getReportEmployeeIds($manager)
			{
        //LEVEL2			
				$level2 = "SELECT  DISTINCT `employee_id` 
				FROM `employees`
				WHERE `primary_lead_id` IN (SELECT `employee_id` FROM `employees` WHERE `primary_lead_id` = $manager )";

				$query2 = $this->db->query($level2);

				$oneDimensionalArray = array_map('current', $query2->result_array());
				if (is_array($oneDimensionalArray) && count($oneDimensionalArray))
				{	
					$teamleads = implode(",",$oneDimensionalArray);	
				}
				else
				{
					$teamleads = 999999;
				}
		//LEVEL3			
				$level3 = "SELECT DISTINCT `employee_id` FROM `employees`WHERE `primary_lead_id` = $manager 
				UNION ALL SELECT  DISTINCT `employee_id` FROM `employees` 
				WHERE `primary_lead_id` IN (SELECT `employee_id` FROM `employees`WHERE `primary_lead_id` = $manager ) 
				UNION ALL SELECT  DISTINCT `employee_id` FROM `employees` 
				WHERE `primary_lead_id` IN ($teamleads)";

				$query3 = $this->db->query($level3);

				if ($query3->num_rows() > 0) {
					return $query3->result_array();
				}
				else
				{
					return 0;
				}
			}


			function addEmployee($data = '')
			{
				$addEmployee = json_decode(trim($data), true);
        		//check vertcal start
				$assigned_vertical = $addEmployee['assigned_vertical'];
				$ops_constant = json_decode(OPS_VERT);
				//check vertical end
				$grade_value = $addEmployee['designation_id'];
				$grades = $this->getGrade($grade_value);
				$grades = $grades[0]['grades'];

				if($addEmployee['is_onshore'] == "" || $addEmployee['is_onshore'] == null){
					$addEmployee['is_onshore'] = 0;
				}

				if($addEmployee['mobile']==$addEmployee['alternate_number'])
					return -3;

				if($addEmployee['employee_id']=="")
				{
					if ($this->_isRecordExist("", $addEmployee['company_employ_id'], $addEmployee['email']) == 0) 
					{	
						unset($addEmployee['deligate_to']);
						unset($addEmployee['resign_flag']);
						unset($addEmployee['resigned_notes']);
						unset($addEmployee['resigned_date']);

						$addEmployee['doj']  = date("Y-m-d", strtotime($addEmployee['doj']));
						$addEmployee['dob']  = date("Y-m-d", strtotime($addEmployee['dob']));
						unset($addEmployee['relieve_flag']);
						$addEmployee['relieve_date'] = $addEmployee['relieve_date'] ? $addEmployee['relieve_date'] : null;
						if (!$addEmployee['company_employ_id'])
							unset($addEmployee['company_employ_id']);
						$empData     = $this->input->cookie();
						$this->EmpId = $empData['employee_id'];

						/* Added code for Chatham Employees */
						$chatnamflag = false;
						if($addEmployee['location_id'] == 4 )
						{
							// $addEmployee['assigned_vertical'] = NULL;
							$chatnamflag = true;
							//$addEmployee['primary_lead_id'] = 1297;
						}

						$addEmployee['created_by']   = $this->EmpId;
						$addEmployee['created_on'] = date("Y-m-d H:i:s");
						$addEmployee['transition_status'] = 0;
						$addEmployee['status'] = 3;
						$addEmployee['training_needed']  = 1;

						unset($addEmployee['assigned_vertical']);
						// debug($addEmployee);exit;

						$retVal         = $this->insert($addEmployee, 'employees');
						$lastInsertedID = $this->db->insert_id();

						$emp_vert['employee_id'] = $lastInsertedID;
						$emp_vert['vertical_id'] = $assigned_vertical;
						$this->insert($emp_vert,'employee_vertical');

						$addEmployee['assigned_vertical'] = $assigned_vertical;

				// Inserts a record in emp_shift 
						if($addEmployee['location_id'] != 4 )
						{
							$shift_data =  array(
								'employee_id' => $lastInsertedID,
								'from_date' => date("Y-m-d"),
								'shift_id' => $addEmployee['shift_id'],
								'to_date' => '1970-01-01',
								'created_by' => $this->EmpId,
							);

							$this->db->insert('emp_shift', $shift_data);
						}

						// Inserts new record in employee_vertical_history
						if($addEmployee['location_id'] != 4 )
						{
							$vertical_data =  array(
								'employee_id' => $lastInsertedID,
								'vertical_id' =>$addEmployee['assigned_vertical'],
								'start_date' => date("Y-m-d"),
								'created_by' => $this->EmpId,
							);
							$this->db->insert('employee_vertical_history', $vertical_data);
						}

						if(!in_array($assigned_vertical, $ops_constant))
						{
							// Inserts new record in employee_client
							if($addEmployee['location_id'] != 4 )
							{
								$this->db->set('employee_id', $lastInsertedID);
								$this->db->set('primary_lead_id', $addEmployee['primary_lead_id']);
								$this->db->set('client_id', 221);
								$this->db->set('grades', $grades);
								$this->db->set('billable', 0);
								$this->db->set('billable_type', 'Buffer');
								$this->db->set('start_date', $addEmployee['doj']);
								$this->db->where_in('employee_id', $lastInsertedID);
								$resVal = $this->db->insert('employee_client');
							}

							//Inserts new record in employee client history
							if($addEmployee['location_id'] != 4 )
							{
								$this->db->set('employee_id', $lastInsertedID);
								$this->db->set('client_id', 221);
								$this->db->set('created_by', $this->EmpId);
								$this->db->set('start_date', $addEmployee['doj']);
								$this->db->where_in('employee_id', $lastInsertedID);
								$resVal = $this->db->insert('employee_client_history');
							}
						}

						$empData = $this->input->cookie();
						$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been added employee(Employee ID:'.$lastInsertedID.') Successfully.';
						$this->userLogs($logmsg);
						

						return $retVal;
					}
					else
					return -1;
					//echo $this->db->last_query();
				}
				else
				{
					$setData = json_decode(trim($data), true);
					$idVal = $setData['employee_id'];
					if($setData['same_as'] != ''){
						$setData['same_as'];
					} else {
						$setData['same_as'] = 0;
					}

					if($setData['is_onshore'] != ''){
						$setData['is_onshore'];
					} else {
						$setData['is_onshore'] = 0;
					}
					//var_dump($setData);exit;
//print_r($setData); exit;
					if ($this->_isRecordExist($idVal, $setData['company_employ_id'], $setData['email']) == 0) 
					{
						$getprevious_shift= $this->getpreviousshift($idVal);
						$newData = array(
							'company_employ_id'	=> $setData['company_employ_id'],
							'first_name' 		=> $setData['first_name'],
							'last_name' 		=> $setData['last_name'],
							'email' 			=> $setData['email'],
							'mobile' 			=> $setData['mobile'],
							'service_id' 		=> $setData['service_id'],
							'location_id' 		=> $setData['location_id'],
							'doj' 				=> date("Y-m-d", strtotime($setData['doj'])),
							'dob' 				=> date("Y-m-d", strtotime($setData['dob'])),
							'gender' 			=> $setData['gender'],
							'qualification_id' 	=> $setData['qualification_id'],
							'personal_email' 	=> $setData['personal_email'],
							'alternate_number' 	=> $setData['alternate_number'],
							'present_address' 	=> $setData['present_address'],
							'present_city' 		=> $setData['present_city'],
							'present_state' 	=> $setData['present_state'],
							'present_zipcode' 	=> $setData['present_zipcode'],
							'same_as' 			=> $setData['same_as'],
							'permanent_address' => $setData['permanent_address'],
							'permanent_city' 	=> $setData['permanent_city'],
							'permanent_state' 	=> $setData['permanent_state'],
							'permanent_zipcode' => $setData['permanent_zipcode'],
							'is_onshore' => $setData['is_onshore'],
						);

						$where             = array();
						$set               = array();
						$empData           = $this->input->cookie();
						$EmpId             = $empData['employee_id'];
						$where['employee_id'] = $idVal;

				//Added for date updation
						$empData     = $this->input->cookie();
						$this->EmpId = $empData['employee_id'];

						$newData['updated_by']   = $this->EmpId;
						$newData['updated_date'] = date("Y-m-d H:i:s");

						/* Added code for Chatnam Employees */
						$chatnamflag = false;
						if($setData['location_id'] == 4 )
						{
							// $newData['assigned_vertical'] = NULL;
							$chatnamflag = true;
							//$newData['primary_lead_id'] = 1297;
						}

						$retVal = $this->update($newData, $where, 'employees');

						if($setData['location_id'] != 4 )
						{			
							if($getprevious_shift != $setData['shift_id'])
							{
								$updateshift_data = array(
									'to_date' => date("Y-m-d"),
								);
								$where['to_date'] = null;

								$update_previous_shift = $this->update($updateshift_data, $where, 'emp_shift');

								$shift_data =  array(
									'employee_id' => $idVal,
									'from_date' => date("Y-m-d"),
									//'shift_id' => $setData['shift_id'],
									'to_date' => '1970-01-01'
								);

								$this->db->insert('emp_shift', $shift_data);

							}
						}
						$empData = $this->input->cookie();
						$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee(Employee ID:'.$idVal.') Successfully.';
						$this->userLogs($logmsg);
						return -2;
					} else
					return -1;
				}
			}

			function updateEmployee($idVal = '', $data = '')
			{   
				$setData = json_decode(trim($data), true);
				$idVal = $setData['employee_id'];

				if ($this->_isRecordExist($idVal, $setData['company_employ_id'], $setData['email']) == 0) 
				{
					$getprevious_shift= $this->getpreviousshift($idVal);

					unset($setData['deligate_to']);        
					$where             = array();
					$set               = array();
					$empData           = $this->input->cookie();
					$EmpId             = $empData['employee_id'];
					$where['employee_id'] = $idVal;


            // Code for convertion of date formate 
					$JDay = $setData['doj'];
					$RDay = $setData['relieve_date'];

					$setData['doj']  = date("Y-m-d", strtotime($JDay));
					$setData['relieve_date'] = date("Y-m-d", strtotime($RDay));

            //Added for date updation
					$empData     = $this->input->cookie();
					$this->EmpId = $empData['employee_id'];

					$setData['updated_by']   = $this->EmpId;
					$setData['updated_date'] = date("Y-m-d H:i:s");

					/* Added code for Chatnam Employees */
					$chatnamflag = false;
					if($setData['location_id'] == 4 )
					{
						$setData['assigned_vertical'] = NULL;
						$chatnamflag = true;
						//$setData['primary_lead_id'] = 1297;
					}

					$retVal = $this->update($setData, $where, 'employees');

					if($setData['location_id'] != 4 )
					{			
						if($getprevious_shift != $setData['shift_id'])
						{
							$updateshift_data = array(
								'to_date' => date("Y-m-d"),
							);
							$where['to_date'] = '0000-00-00';

							$update_previous_shift = $this->update($updateshift_data, $where, 'emp_shift');

							$shift_data =  array(
								'employee_id' => $idVal,
								'from_date' => date("Y-m-d"),
								'shift_id' => $setData['shift_id'],
								'to_date' => '0000-00-00'
							);

							$this->db->insert('emp_shift', $shift_data);

						}
					}
					return $retVal;


				} else
				return -1;
			}


			private function _isRecordExist($id, $companyEmpID, $email)
			{
				$where = " (company_employ_id =  '" . trim($companyEmpID) . "' OR LOWER(email) =  '" . trim(strtolower($email)) . "') ";
				$query = $this->db->select("employee_id")->where($where, null, false);
				if ($id != "")
					$this->db->where("employee_id  != ", trim($id));
				$query = $this->db->get('employees');

				return $query->num_rows();
			} 

			private function getpreviousshift($id)
			{
				$output = $this->db->query("SELECT shift_id FROM employees WHERE employee_id = '$id'")->row()->shift_id;

				return $output;
			}

			function getverticalData($VerticalID)
			{
				$sql = "SELECT  employee_id FROM verticalmanager  where vertical_id ='" . $VerticalID . "'";

				$query = $this->db->query($sql);
				if ($query->num_rows() > 0) {
					return $query->result_array();
				}
				return '';
			}

			function getGrade($DesignationID)
			{
				$sql = "SELECT  grades FROM designation  where designation_id ='" . $DesignationID . "'";

				$query = $this->db->query($sql);
				if ($query->num_rows() > 0) {
					return $query->result_array();
				}
				return '';
			}

			public function getTlBillableEmployees($client_id) {
				$empData = $this->input->cookie();
		// echo"<PRE>";print_r($empData);
				$EmpID  = $empData['employee_id'];
				$grade = $empData['Grade']; 
				$select = "SELECT e.employee_id,e.primary_lead_id,CONCAT_WS(' ',e.first_name,e.last_name) as first_name,ec.billable,e.comments as comment,e.email, e.designation_id ,d.name,d.grades";
				$from = " FROM employees e";
				$join = " LEFT JOIN employee_client ec ON e.employee_id = ec.employee_id";
				$join .= " LEFT JOIN designation d ON e.designation_id = d.designation_id";
				$where = " WHERE e.status = 3 AND ec.billable =1 AND  ec.grades < 3 AND  ec.client_id = $client_id ";

				if($empData['Grade'] != 7)
				{
					$where .= " AND e.primary_lead_id = $EmpID ";
				}

				$order = " ORDER BY e.first_name ";

				$query = $select . $from . $join . $where . $order;

				$totQuery = $this->db->query($query);

				if($totQuery->num_rows() == 0) return FALSE;
				return $totQuery->result_array();  
			}

			function getAssociateManagers()
			{
				$VerticalID = $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';
				$ManagerID  = $this->input->get('EmployID') ? $this->input->get('EmployID') : '';
				$empData    = $this->input->cookie();

	   // echo $VerticalID;
	   // echo $ManagerID;exit;
	   // echo "<PRE>";print_r($empData);
				if($VerticalID)
				{
					if($VerticalID != ''){

						$sql = "SELECT DISTINCT(employees.`employee_id`), CONCAT(IFNULL(employees.first_name, ''),' 	',IFNULL(employees.`last_name`, '')) AS `Name` FROM employees AS employees
						JOIN designation ON designation.`designation_id` = employees.designation_id
						JOIN employee_client ON employees.employee_id = employee_client.employee_id
						JOIN client_vertical ON employee_client.client_id = client_vertical.client_id
						WHERE employees.status=3 AND designation.grades =4 AND client_vertical.vertical_id = $VerticalID ORDER BY employees.first_name";       	
						$query = $this->db->query($sql);
            // echo $sql;exit;
						$resultData = $query->result_array();

            if(!empty($resultData)) //if Associate Managers are available for Vertical
            { 
            	$results = $query->result_array();            	
            } 
            else //get Vertical Managers Reportees AM'S if Associate Managers are not available for Vertical  
            { 
            	$verMgrId = $this->getVerticalManagerById($VerticalID); //get vertical managers by vertical-id           	
            	$amIdsArray = $this->getAmsbyMid($verMgrId['0']['employee_id']); //get AM's by manager-id         

            	$amDetailsSql= "SELECT DISTINCT(employees.`employee_id`), CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name` FROM employees
            	JOIN designation ON designation.`designation_id` = employees.`designation_id`
            	WHERE employees.status=3 AND employees.employee_id IN (". implode(",",$amIdsArray ).") ORDER BY first_name";				
            	$amDetailsquery = $this->db->query($amDetailsSql);
            	$results = $amDetailsquery->result_array();
				// echo "<PRE>";print_r($results);	
            }
            
            return $results;
        }
    } else if($ManagerID) {
    	$sql = "SELECT DISTINCT(employees.`employee_id`), CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name` FROM employees
    	JOIN designation ON designation.`designation_id` = employees.`designation_id`
    	WHERE employees.status=3 AND designation.grades = 4 AND 
    	employees.primary_lead_id = $ManagerID ";

    	if($empData['Grade'] == 4 || $empData['Grade'] < 5) {
    		$sql    .= " AND employees.employee_id = ".$empData['employee_id']."";
    	}

    	$sql        .= " ORDER BY employees.first_name ";

    	$query = $this->db->query($sql);
			// echo $sql;exit;
    	return $query->result_array();

    } else {
    	return '';
    }
}

function  getVerticalManagerById($VerticalID)
{
	$sql = "SELECT `employee_id` FROM `verticalmanager` WHERE `vertical_id` ='" . $VerticalID . "'";
	$query = $this->db->query($sql);
	if ($query->num_rows() > 0) {
		return $query->result_array();
	}
	return '';
}

function getAmsbyMid($verMgrId)
{
	$sql = "SELECT d.`grades` FROM `designation` d LEFT JOIN employees e ON e.`designation_id` = d.`designation_id` WHERE e.employee_id  = '" . $verMgrId . "' AND STATUS = 3";

	$query = $this->db->query($sql);
	if ($query->num_rows() > 0) 
	{
		$empGradeArray = $query->result_array();   		
		$grade = $empGradeArray[0]['grades'];    		
		if ($grade == 5) 
		{   			
			$amSql = "SELECT employee_id FROM employees JOIN designation d ON d.designation_id = employees.designation_id WHERE `primary_lead_id` = '" . $verMgrId . "'  AND grades >=4 AND grades <=5 AND STATUS = 3";
			$amquery = $this->db->query($amSql);
			if ($amquery->num_rows() > 0) {
    				//return $amquery->result_array();   				
				return array_map('current', $amquery->result_array());

			}
		}
		if ($grade == 7) 
		{
			$amSql = "SELECT employee_id FROM employees JOIN designation d ON d.designation_id = employees.designation_id WHERE grades >=4 AND grades <5 AND STATUS = 3";
			$amquery = $this->db->query($amSql);
			if ($amquery->num_rows() > 0) {
    				//return $amquery->result_array();
				return array_map('current', $amquery->result_array());
			}
		}
		if ($grade == 6)
		{
			$associateManagerArray = array();
			$managerSql = "SELECT e.employee_id, d.grades   FROM employees e JOIN designation d ON d.designation_id = e.designation_id WHERE e.`primary_lead_id` = '" . $verMgrId . "'  AND d.grades >= 4 AND d.grades <= 6 AND  e.STATUS = 3";  			   		  			
			$managerquery = $this->db->query($managerSql);
			if ($managerquery->num_rows() > 0) {
				$managers = $managerquery->result_array();			
				if(!empty($managers))
				{
					foreach ($managers as $key=>$value)
					{
						if($value['grades'] == '4')
						{
							$associateManagerArray[] = $value['employee_id'];
						}
						else
						{
							$managerArray[] = $value['employee_id'];
						}	
					}
				}

				if(!empty($managerArray))
				{
					$associateManagerSql = "SELECT e.employee_id FROM employees e JOIN designation d ON d.designation_id = e.designation_id WHERE e.`primary_lead_id` IN (". implode(",",$managerArray ).")  AND d.grades >= 4 AND d.grades < 5 AND  e.STATUS = 3";
					$associateManagerquery = $this->db->query($associateManagerSql);
					if ($associateManagerquery->num_rows() > 0) {

						$associatemanagers = $associateManagerquery->result_array();
    						//return $associatemanagers;
						$associatemanagers = array_map('current', $associatemanagers);

						return array_merge($associatemanagers,$associateManagerArray);
					}
					else
					{
						return $associateManagerArray;
					}
				}
				else
				{
					return $associateManagerArray;
				}    				 				
			}
			else { return ''; }
		}  		
	}
}


public function getTlEmployees(){
	$empData = $this->input->cookie();
	$EmpID  = $empData['employee_id'];
	$this->db->select("employees.employee_id,CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,employees.email, employees.designation_id ,designation.name,designation.grades",FALSE)->from("employees")
	->join('designation', 'employees.designation_id = designation.designation_id')
       // ->where('employees.employee_id NOT IN','(SELECT employee_id FROM employprocess WHERE ProcessID IN('.INI_PROCESS_ID.','.ONBOARD_PROCESS_ID.'))', false)
	->where('employees.primary_lead_id =',$EmpID)
	->where('employees.primary_lead_id !=1')
	->where('employees.status = 3')
	->order_by('employees.first_name');
	$query = $this->db->get();
	if($query->num_rows() == 0) 
		return $query->result_array();      
}

	/**
	* Get list of stakeholders
	*
	* @param 
	* @return array
	*/ 
	function list_stakeholders()
	{
		$filterQuery 		= $this->input->get('query');

		$select = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name`";
		$from = " FROM employees e ";
		$join = " LEFT JOIN designation d ON e.designation_id = d.designation_id ";
		
		$where = "WHERE e.status!=6 AND ((d.grades>2 and d.grades<7) || (e.`location_id` = 4))";
		
		if($filterQuery !="")
		{
			$where .=" AND (e.first_name LIKE '%".$filterQuery."%' OR e.last_name LIKE '%".$filterQuery."%')";
		}
		
		$order = " ORDER BY e.first_name ";	
		
		$sql = $select . $from . $join. $where . $order;

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		return $results;
	}	
	
	
	/**
	* Get list of stakeholders
	*
	* @param 
	* @return array
	*/ 
	function list_timeEntryApprover()
	{
		$wor_id = $this->input->get('wor_id');
		$filterQuery = $this->input->get('query');

		$worQuery = $this->db->query("SELECT GROUP_CONCAT(DISTINCT employee_id SEPARATOR ',') AS employee_id FROM employee_client WHERE wor_id = $wor_id");
		$worData = $worQuery->row_array();

		if(isset($worData['employee_id']))
		{
			$sql = "SELECT employee_id, CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, '')) AS `full_name`
			FROM employees 
			WHERE status!=6 AND `employee_id`IN(".$worData['employee_id'].")";
			if($filterQuery !="")
			{
				$sql .=" AND (first_name LIKE '%".$filterQuery."%' OR last_name LIKE '%".$filterQuery."%')";
			}
			$sql .= " ORDER BY first_name ";	

			$query = $this->db->query($sql);
			$totCount = $query->num_rows();

			$query = $this->db->query($sql);
			$data = $query->result_array();
			
			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);
			return $results;
		}
		else
		{
			return [];
		}
	}	

    /**
	* Get list of delivery managers
	*
	* @param 
	* @return array
	*/ 
	function list_delivery_managers()
	{  	
    	// $sql = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name` FROM employees e WHERE designation_id IN (135,136)
				// ORDER BY e.first_name"; 

		$filterQuery 		= $this->input->get('query');
		

		$select = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name`";
		$from = " FROM employees e
				LEFT JOIN designation d ON d.designation_id = e.designation_id ";
		
		$where = " WHERE (d.grades IN(5,6,7,8) OR e.employee_id='14') AND e.status != 6 AND e.location_id !=4 ";
		
		if($filterQuery !="")
		{
			$where .=" AND (e.first_name LIKE '%".$filterQuery."%' OR e.last_name LIKE '%".$filterQuery."%')";
		}
		
		$order = " ORDER BY e.first_name ";	
		
		$sql = $select . $from . $where . $order;
		//echo $sql;exit;

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		return $results;
	}

    /**
	* Get list of Engagement Managers
	*
	* @param 
	* @return array
	*/ 
	function list_engagament_managers()
	{  	
    	// $sql = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name` FROM employees e 
				// WHERE (designation_id IN (135,136)) || (e.`location_id` = 4)
				// ORDER BY e.first_name";    	

		$filterQuery 		= $this->input->get('query');
		

		$select = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name`";
		$from = " FROM employees e 
				LEFT JOIN designation d ON d.designation_id = e.designation_id";
		
		$where = " WHERE d.grades IN(5,6,7,8) AND e.status != 6";
		
		if($filterQuery !="")
		{
			$where .=" AND (e.first_name LIKE '%".$filterQuery."%' OR e.last_name LIKE '%".$filterQuery."%')";
		}
		
		$order = " ORDER BY e.first_name ";	
		
		$sql = $select . $from . $where . $order;

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		return $results;
	}

	/**
	* Get list of Client Partner
	*
	* @param 
	* @return array
	*/ 
	function list_client_partner()
	{  	

    	/* $sql = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name` FROM employees e 
				WHERE e.`location_id` = 4
				ORDER BY e.first_name";   */

				$filterQuery 		= $this->input->get('query');


				$select = "SELECT *, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `full_name`";
				$from = " FROM employees e
						LEFT JOIN designation d ON d.designation_id = e.designation_id ";

				$where = " WHERE d.grades IN(5,6,7,8) AND e.status != 6";

				if($filterQuery !="")
				{
					$where .=" AND (e.first_name LIKE '%".$filterQuery."%' OR e.last_name LIKE '%".$filterQuery."%')";
				}

				$order = " ORDER BY e.first_name ";	

				$sql = $select . $from . $where . $order;


				$query = $this->db->query($sql);
				$totCount = $query->num_rows();

				$data = $query->result_array();

				$results = array_merge(
					array('totalCount' => $totCount), 
					array('data' => $data)
				);

				return $results;
			}


	/** 
    * Get Team lead and AM details of Work Order 
    * 
    * @modules  WOR, & COR
    */
	function list_tl_am_details()
	{		
		$employee_type      = $this->input->get('employee_type') ? $this->input->get('employee_type') : "";
		$client_id      	= $this->input->get('client_id') ? $this->input->get('client_id') : "";
		$wor_id      	= $this->input->get('wor_id') ? $this->input->get('wor_id') : "";

		$filterQuery 		= $this->input->get('query');
		
    	//echo $assigned_vertical;exit;		   	
		if($employee_type == 'team_leader') { $tloramSQL = 'AND d.grades >2 AND d.grades<4'; } else { $tloramSQL = 'AND d.grades=4'; }

		$sql = "SELECT  employees.employee_id, employees.`first_name`, employees.`last_name`,
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `full_name`
		FROM employees 
		LEFT JOIN `designation` d ON d.`designation_id` = employees.`designation_id` 
		WHERE employees.status IN(1,2,3,4,7) $tloramSQL ";

		if($client_id)  
		{
			$sql .=" AND client IN (".$client_id.")";
		}
		if($wor_id)  
		{
			$sql .=" AND wor_id IN (".$wor_id.")";
		}
		
		if($filterQuery !="")
		{
			$sql .=" AND (employees.first_name LIKE '%".$filterQuery."%' OR employees.last_name LIKE '%".$filterQuery."%')";
		}
		$sql .=" ORDER BY first_name ASC";      

		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);

		return $results;
		
	}

	function list_am_lead_details()
	{
		$supervisor   = $this->input->get('supervisor') ? str_replace(",", "','",$this->input->get('supervisor')) : "";		
		$grades   = $this->input->get('grades') ? str_replace(",", "','",$this->input->get('grades')) : "";
		$assigned_vertical = $this->input->get('assigned_vertical') ? $this->input->get('assigned_vertical') : "";
		$empData    = $this->input->cookie();
        // echo'<pre>';print_r($empData);exit;
		$filterQuery= $this->input->get('query');
		
		if ($grades >= '5') 
		{
			$tloramSQL = 'AND d.grades >=5';
		}
		else if ($grades == '4') 
		{
			$tloramSQL = 'AND d.grades IN(5,6)';
		}
		elseif ($grades >2 && $grades<4) 
		{
			$tloramSQL = 'AND d.grades IN(4,5)';
		}
		else 
		{
			$tloramSQL = 'AND d.grades>2 AND d.grades<4';
		}

		$sql = "SELECT  employees.employee_id, employees.`first_name`, employees.`last_name`,
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `full_name`
		FROM employees 
		LEFT JOIN `designation` d ON d.`designation_id` = employees.`designation_id`
		JOIN employee_vertical ON employee_vertical.employee_id = employees.employee_id
		WHERE employees.status IN(1,2,3,4,7) AND employees.employee_id NOT IN ('".$supervisor."') $tloramSQL ";
		if($empData['Grade'] < '5') 
		{
			$sql .= " AND employee_vertical.vertical_id IN(".$empData['VertId'].")";
		}
		
		if($filterQuery !="")
		{
			$sql .=" AND (employees.first_name LIKE '%".$filterQuery."%' OR employees.last_name LIKE '%".$filterQuery."%')";
		}
		$sql .=" ORDER BY first_name ASC";  

		//echo $sql;exit;    

		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);

		return $results;
		
	}
        /** 
    * Get Team lead and AM details 
    * 
    * @modules Incident Log
    */
        function list_tl_am_details_incident()
        {
        	$results = array();
        	$employee_type      = $this->input->get('employee_type') ? $this->input->get('employee_type') : "";
        	$client_id          = $this->input->get('client_id') ? $this->input->get('client_id') : "";
        	$wor_id         = $this->input->get('wor_id') ? $this->input->get('wor_id') : "";

        	if($employee_type == 'associate_manager')
        	{
        		$sql_am="SELECT associate_manager FROM wor WHERE wor_id=".$wor_id;
        		$query_am = $this->db->query($sql_am);
        		$data_am = $query_am->row_array();
        		$am_ids=$data_am['associate_manager'];
        		$sql = "SELECT employees.employee_id,employees.`first_name`, employees.`last_name`,
        		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `full_name` FROM  employees  WHERE  employees.employee_id IN (".$am_ids.")";
        		$sql .=" ORDER BY first_name ASC"; 
        		$query = $this->db->query($sql);
        		$totCount = $query->num_rows();

        		$query = $this->db->query($sql);
        		$data = $query->result_array();

        		$results = array_merge(
        			array('totalCount' => $totCount), 
        			array('data' => $data)
        		);

        		return $results;
        	}
        	if($employee_type == 'team_leader')
        	{
        		$sql_lead="SELECT team_lead FROM wor WHERE wor_id=".$wor_id;
        		$query_lead = $this->db->query($sql_lead);
        		$data_lead = $query_lead->row_array();
        		$lead_ids=$data_lead['team_lead'];

        		if($lead_ids!="")
        		{
        			$sql = "SELECT employees.employee_id,employees.`first_name`, employees.`last_name`,
        			CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `full_name` FROM  employees WHERE employees.employee_id IN (".$lead_ids.")";
        			$sql .=" ORDER BY first_name ASC"; 
        			$query = $this->db->query($sql);
        			$totCount = $query->num_rows();

        			$query = $this->db->query($sql);
        			$data = $query->result_array();

        			$results = array_merge(
        				array('totalCount' => $totCount), 
        				array('data' => $data)
        			);
        		}
        		else
        		{
        			$results = array_merge(
        				array('totalCount' => 0), 
        				array('data' => [])
        			);
        		}

        		return $results;
        	}

        }

        /* Get Support Coverage */
        function list_supportcoverage()
        {                   
        	$sql = "SELECT support_id,support_type from support_coverage";

        	$query = $this->db->query($sql);
        	$totCount = $query->num_rows();

        	$query = $this->db->query($sql);
        	$data = $query->result_array();

        	$results = array_merge(
        		array('totalCount' => $totCount), 
        		array('rows' => $data)
        	);

        	return $results;

        }

        function getProjectId($projectCode)
        {
        	$sql = "SELECT process_id FROM process WHERE name = '".$projectCode."'";                            
        	$query = $this->db->query($sql);
        	return $query->result_array();
        }

        function update_promotion($data = '')
        {
        	$empData    = $this->input->cookie();

		//update employees
        	$designation_data = array(
        		'designation_id'   => $data['new_designation'],
        	);
        	$this->db->where('employee_id', $data['employee_id']);
        	$retVal = $this->db->update('employees',$designation_data);

		//update employee client
        	$grades_data = array(
        		'grades'   => $data['grade'],
        	);
        	$this->db->where('employee_id', $data['employee_id']);
        	$retVal = $this->db->update('employee_client',$grades_data);

        	if(isset($data['deligate_to']) && $data['deligate_to']!="")
        	{
        		$reportId_query = "SELECT DISTINCT `employee_id` FROM `employees` WHERE `primary_lead_id` = '".$data['employee_id']."' AND status !=6";
        		$query = $this->db->query($reportId_query);
        		$reportIdArr = $query->result_array();
        		$reportIdArr = array_column($reportIdArr, 'employee_id');
			// echo "<pre>"; print_r($reportIdArr); exit;
			//update delegation
        		if(!empty($reportIdArr))
        		{
        			$delegate_data = array(
        				'primary_lead_id'   => $data['deligate_to'],
        			);
        			$this->db->where_in('employee_id', $reportIdArr);
        			$retVal = $this->db->update('employees',$delegate_data);

        			$this->db->where_in('employee_id', $reportIdArr);
        			$retVal = $this->db->update('employee_client',$delegate_data); 
        		}


        		foreach($reportIdArr as $val)
        		{
        			$logArray = array(
        				'employee_id'		=> $val,
        				'from_reporting' 	=> $data['employee_id'],
        				'to_reporting' 		=> $data['deligate_to'],			
        				'transition_date' 	=> date('Y-m-d'),
        				'comment'			=> 'Supervisor Changed',
        				'modified_by'		=> $empData['employee_id'],	
        			);

        			$this->insert($logArray, 'employee_transition');
        		}
        	}

		//insert into history
        	$promotion_history = array(
        		'promotion_date'   	=> date("Y-m-d",strtotime($data['promotion_date'])),
        		'employee_id'   	=> $data['employee_id'],
        		'grade'				=> $data['hcm_grade'],
        		'new_designation'   => $data['new_designation'],
        		'promoted_by'   	=> $empData['employee_id'],
        	);
        	$retVal = $this->db->insert('promotions_history',$promotion_history);

			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee promotion(Employee ID:'.$data['employee_id'].') (New Grade:'.$data['hcm_grade'].') (New Designation:'.$data['new_designation'].').';
			$this->userLogs($logmsg);

        	return $retVal;
        }

        function update_resignation($data = '')
        {
		//update employees
        	$resign_data = array(
        		'status'			=> 5,
        		'resigned_date'		=> date("Y-m-d",strtotime($data['resigned_date'])),
        		'resigned_notes'	=> $data['resignation_reason'],
        	);
        	$this->db->where('employee_id', $data['employee_id']);
        	$retVal = $this->db->update('employees',$resign_data);

        	$empData    = $this->input->cookie();
        	$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee resignation(Employee ID:'.$data['employee_id'].'). ';
			$this->userLogs($logmsg);

        	return $retVal;
        }

        function update_relieve($data = '')
        {
        	$empData    = $this->input->cookie();

		//update employees
        	$relieve_data = array(
        		'status'		 => 6,
        		'relieve_date'   => date("Y-m-d",strtotime($data['relieve_date'])),
        		'relieve_type'   => $data['relieve_type'],
        		'relieve_notes'  => $data['relieve_notes'],
        		'updated_by'	=> $empData['employee_id'],
        		'updated_date' 	=> date('Y-m-d H:i:s'),
        	);
        	$this->db->where('employee_id', $data['employee_id']);
        	$retVal = $this->db->update('employees',$relieve_data);

        	$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee resignation(Employee ID:'.$data['employee_id'].') (Relieve Type:'.$data['relieve_type'].'). ';
			$this->userLogs($logmsg);

		//delete employee client
        	$this->db->where('employee_id', $data['employee_id']);
        	$retVal = $this->db->delete('employee_client');

        	if(isset($data['deligate_to']) && $data['deligate_to']!="")
        	{
        		$reportId_query = "SELECT DISTINCT `employee_id` FROM `employees` WHERE `primary_lead_id` = '".$data['employee_id']."' AND status !=6";
        		$query = $this->db->query($reportId_query);
        		$reportIdArr = $query->result_array();
        		$reportIdArr = array_column($reportIdArr, 'employee_id');

        		if(!empty($reportIdArr))
        		{
    			//update delegation
        			$delegate_data = array(
        				'primary_lead_id'   => $data['deligate_to'],
        			);
        			$this->db->where_in('employee_id', $reportIdArr);
        			$retVal = $this->db->update('employees',$delegate_data);

        			$this->db->where_in('employee_id', $reportIdArr);
        			$retVal = $this->db->update('employee_client',$delegate_data);
        		}

        		foreach($reportIdArr as $val)
        		{
        			$logArray = array(
        				'employee_id'		=> $val,
        				'from_reporting' 	=> $data['employee_id'],
        				'to_reporting' 		=> $data['deligate_to'],			
        				'transition_date' 	=> date('Y-m-d'),
        				'comment'			=> 'Supervisor Changed',
        				'modified_by'		=> $empData['employee_id'],	
        			);

        			$this->insert('employee_transition', $logArray);
        		}
        	}

		//update employee billable history
        	$billabality_update_data = array(
        		'end_date'		=> date('Y-m-d'),
        		'updated_by'	=> $empData['employee_id'],
        	);
        	$this->db->where('employee_id', $data['employee_id']);
        	$this->db->where('end_date IS NULL');
        	$retVal = $this->db->update('employee_billabality_history',$billabality_update_data);

        	return $retVal;
        }

        public function getUtilEmployees()
        {
        	// debug($this->input->get());exit;
        	$client_id = $this->input->get('comboClient');
        	$search_filter = '';
        	if($this->input->get('query'))
        	{
        		$name_filter = $this->input->get('query');
        		$search_filter = " AND (E.first_name LIKE '%$name_filter%' OR E.last_name LIKE '%$name_filter%')";
        	}

        	$sql = "SELECT DISTINCT E.employee_id, CONCAT(E.first_name,' ',E.last_name) AS full_name
        	FROM utilization U
        	LEFT JOIN employees E ON E.employee_id = U.employee_id
        	WHERE U.fk_client_id = '$client_id' AND E.employee_id != '' $search_filter";

        	$start_date = ($this->input->get('start_date')?$this->input->get('start_date'):'');
        	$end_date = ($this->input->get('end_date')?$this->input->get('end_date'):'');

        	if($start_date != '' && $end_date != '')
            $sql .= " AND U.task_date BETWEEN '$start_date' AND '$end_date'";

        	if($this->input->get('wor_id'))
        	{
        		$wor_id = $this->input->get('wor_id');
        		$sql .= " AND U.fk_wor_id = '$wor_id'";
        	}

        	if($this->input->get('wor_type_id'))
        	{
        		$wor_type_id = $this->input->get('wor_type_id');
        		$sql .= " AND U.fk_type_id = '$wor_type_id'";
        	}

        	if($this->input->get('project_type_id'))
        	{
        		$project_type_id = $this->input->get('project_type_id');
        		$sql .= " AND U.fk_project_id = '$project_type_id'";
        	}

        	if($this->input->get('supervisor'))
        	{
        		$supervisor = $this->input->get('supervisor');
        		$sql .= " AND E.primary_lead_id = '$supervisor'";
        	}

        	$sql .= " ORDER BY E.first_name ASC";
        	// echo $sql;exit;
        	$query = $this->db->query($sql);
        	$result = $query->result_array();
        	$data['rows'] = $result;
        	$data['totalCount'] = count($result);
        	// debug($result);exit;
        	return $data;
        }
    }
    ?>