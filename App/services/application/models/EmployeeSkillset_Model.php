<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeSkillset_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_skillsets()

	{	
		if($this->input->get('employee_id') != null && $this->input->get('employee_id') != '')
		{
			$employee_id = $this->input->get('employee_id');
		}
		else
		{
			$empData     = $this->input->cookie();
			$employee_id = $empData['employee_id'];
		}

		$sql="SELECT * FROM employee_skillset 
		INNER JOIN skills ON employee_skillset.skill_id=skills.skill_id
		WHERE employee_id = '$employee_id'
		ORDER BY id DESC";


		$query = $this->db->query($sql);

		$results['totalCount'] = $query->num_rows();

		$data = $query->result_array();
		$results['data'] = $data;

		return $results;

	}	

	
	function addSkillset($data = '')
	{
		$skillsetData = $data;
		//print_r($skillsetData);
		//exit;
		$empData     = $this->input->cookie();

		$this->db->set('employee_id',$empData['employee_id']);
		$this->db->set('skill_id', $skillsetData['skill_name']);
		$this->db->set('version', $skillsetData['version']);
		$this->db->set('last_used_month', $skillsetData['last_used_month']);
		$this->db->set('last_used_year', $skillsetData['last_used_year']);
		$this->db->set('experience', $skillsetData['experience']);
		$this->db->set('self_rating', $skillsetData['self_rating']);
		$this->db->set('created_on', date("Y-m-d H:i:s"));								
		$resVal = $this->db->insert('employee_skillset');

		//$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been added employee skills (Skill ID:'.$this->db->insert_id().').';
		$this->userLogs($logmsg); 

		return $resVal;
	}
	
	function updateSkillset($idVal = '',$data = '')
	{
		$empData     = $this->input->cookie();			
		$setData = $data;		
		$where = array();
		$set = array();
		$where['id'] = $setData['id'];
		$sql="SELECT skills.skill_id FROM employee_skillset INNER JOIN skills ON employee_skillset.skill_id=skills.skill_id WHERE skills.skill_name='".$setData['skill_name']."'";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		if(count($data)>0) {
			$skillId=$data[0]["skill_id"];
		}
		else {
			$skillId=$setData["skill_name"];
		}
		$dataset = array(
			'skill_id'			=> $skillId,
			'version'	=> $setData['version'],
			'last_used_month'	=> $setData['last_used_month'],
			'last_used_year'	=> $setData['last_used_year'],
			'experience'	=> $setData['experience'],
			'self_rating'	=> $setData['self_rating'],
			'updated_on'	=> date("Y-m-d H:i:s")
		);			
		$retVal = $this->update($dataset, $where, 'employee_skillset');	

		//$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee skills (Skill ID:'.$setData['id'].').';
		$this->userLogs($logmsg); 

		return $retVal;		
	}		
	
	function deleteSkillset( $idVal = '' ) 
	{
		$options = array();
		$options['id'] =  $idVal;
		$retVal = $this->delete($options, 'employee_skillset');

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been deleted employee skills (Skill ID:'.$idVal.').';
		$this->userLogs($logmsg); 

		//echo $this->db->last_query(); exit;
		return $retVal;
	}


}
?>