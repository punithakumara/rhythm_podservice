<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ErrorCategory_Model extends INET_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
    * Get list of employees
    *
    * @param 
    * @return array
    */ 
    function list_error_category()
    {
         $parent_id=0;
         if($this->input->get('parent_id')){
         $parent_id=$this->input->get('parent_id');  
         }     
        $select = "SELECT * from error_categories";
        $where = " WHERE 1=1";
       // if($this->input->get('parent_id')){
         $where .= " AND parent_category_id=$parent_id";
       // }
        /*if($this->input->get('parent_cat_id')==0){
              $where .= " AND parent_category_id=0";
        }*/
        
        $sql = $select . $where;
      //  echo $sql;
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $totCount = $query->num_rows();
        
        $results = array_merge(
            array('totalCount' => $totCount), 
            array('rows' => $data)
        );
        // echo $this->db->last_query(); exit;
       // echo "<PRE>";print_r($results);
        //exit;
        return $results;
    }   
    }