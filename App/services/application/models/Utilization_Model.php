<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilization_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
		$this->load->model('Rhythm_Model');
		$this->load->helper('cleanse');
		$this->config->load('admin_config');
	}

	function view($params) 
	{
		// debug($_GET);exit;
		$empData    = $this->input->cookie();
		
		$emp_id = ($this->input->get('teamMember')!='') ? $this->input->get('teamMember') : $empData['employee_id'];
		
		$week_range = (isset($params['week_range'])) ? $params['week_range'] : "";

		$project_id = (isset($params['project_type'])) ? $params['project_type'] : "";
		
		$output = false;
		
		$output["metaData"]["idProperty"]="timesheetID";
		$output["metaData"]["successProperty"]="success";
		$output["metaData"]["root"]="data";
		
		// you can parse field values via your database schema
		$output["metaData"]["fields"][] = array('name' => 'past', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'ClientName', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'ProjectType', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'WorkOrderName', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Task', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'SubTask', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day7', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'unit7', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment7', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Status', 'type' => 'string');
		
		$output["columns"]=$this->get_weekDays($week_range,$project_id);
		if(isset($params['week_range']))
			$week_range = $params['week_range'];
		
		$monthResult = explode(":",$this->get_totalMonthHours($emp_id, $week_range));
		$hours = $monthResult[0];
		$minutes = $monthResult[1];
		$output['TotalMonthHours'] = "<b>Month Total</b> = ".$hours." Hrs  ".$minutes." Min";
		
		$weekResult = explode(":",$this->get_totalWeekHours($emp_id, $week_range));
		$overall_hours = $weekResult[0];
		$overall_minutes = $weekResult[1];
		$output['TotalWeekHours'] = "<b>Week Total</b> = ".$overall_hours." Hrs  ".$overall_minutes." Min";
		
		$output["success"] = true;
		$output["data"] = array();
		$output["data"]= $this->get_timesheet_data($emp_id,$week_range);
		//print_r($output); exit;
		return $output;

	}

	function get_totalMonthHours($emp_id,$week_range)
	{
		$sql1 = "SELECT * FROM task_week WHERE week_code_id ='".$week_range."'";
		$query1 = $this->db->query($sql1);
		$result1 = $query1->row_array();
		$dateArr = explode('-',$result1['week_date']);
		$year = $dateArr[0];
		$month = $dateArr[1];
		$startDate = date("$year-$month-01");
		$endDate = date("$year-$month-31");
		
		$sql2 = "SELECT * FROM task_week WHERE week_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$query2 = $this->db->query($sql2);
		$result2 = $query2->result_array();
		
		$dayTotalHrs = 0;
		foreach($result2 AS $val)
		{
			$sql = "SELECT";
			if($val['week_name']=="Mon")
			{
				$sql .= " SUM(TIME_TO_SEC(day1)) AS dayHrs";
			}
			if($val['week_name']=="Tue")
			{
				$sql .= " SUM(TIME_TO_SEC(day2)) AS dayHrs";
			}
			if($val['week_name']=="Wed")
			{
				$sql .= " SUM(TIME_TO_SEC(day3)) AS dayHrs";
			}
			if($val['week_name']=="Thu")
			{
				$sql .= " SUM(TIME_TO_SEC(day4)) AS dayHrs";
			}
			if($val['week_name']=="Fri")
			{
				$sql .= " SUM(TIME_TO_SEC(day5)) AS dayHrs";
			}
			if($val['week_name']=="Sat")
			{
				$sql .= " SUM(TIME_TO_SEC(day6)) AS dayHrs";
			}
			if($val['week_name']=="Sun")
			{
				$sql .= " SUM(TIME_TO_SEC(day7)) AS dayHrs";
			}
			$sql .= " FROM `timesheet_task_mapping` WHERE task_week_id='".$val['week_code_id']."' AND emp_id='".$emp_id."'";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			
			$dayTotalHrs += $result['dayHrs'];
		}
		
		$result3 = $this->getProdutionHoursDetails($emp_id,$startDate,$endDate);
		$dayTotalHrs += $result3;
		
		$hours = floor($dayTotalHrs / 3600);
		$minutes = floor(($dayTotalHrs / 60) % 60);
		
		return ($dayTotalHrs!="0") ? "$hours:$minutes" : "0:0";
	}
	
	function get_totalWeekHours($emp_id,$week_range)
	{
		$weeklyHrs = 0;
		$sql = "SELECT SUM(TIME_TO_SEC(day1))+ SUM(TIME_TO_SEC(day2))+SUM(TIME_TO_SEC(day3))+SUM(TIME_TO_SEC(day4))+SUM(TIME_TO_SEC(day5))+SUM(TIME_TO_SEC(day6))+SUM(TIME_TO_SEC(day7)) AS weeklyHrs FROM `timesheet_task_mapping` WHERE task_week_id=$week_range AND emp_id=$emp_id";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		$weeklyHrs += $result['weeklyHrs'];
		
		$sqldates = "SELECT week_date FROM task_week WHERE week_code_id='".$week_range."'";
		$querydates = $this->db->query($sqldates);
		$dates =  $querydates->result_array();
		
		$result2 = $this->getProdutionHoursDetails($emp_id,$dates[0]['week_date'],$dates[6]['week_date']);
		$weeklyHrs += $result2;
		
		$hours = floor($weeklyHrs / 3600);
		$minutes = floor(($weeklyHrs / 60) % 60);
		
		return ($weeklyHrs!="0") ? "$hours:$minutes" : "0:0";
	}

	private function getProdutionHoursDetails($employee_id, $startDate, $endDate)
	{
		$sql = "SELECT SUM(TIME_TO_SEC(theorem_hrs)) AS TimeTaken
		FROM `utilization` 
		WHERE utilization.task_date BETWEEN '".$startDate."' AND '".$endDate."' AND employee_id IN( ".$employee_id." ) AND status NOT IN(0,2)  
		 GROUP BY employee_id ";

		$result = $this->db->query($sql);
		$val = $result->row_array();
		
		return isset($val['TimeTaken'])?$val['TimeTaken']:'0';
	}

	function get_weekDays($weekrange="",$project_type)
	{
		$sql = "select task_week_id,week_name,week_date from task_week where week_code_id='".$weekrange."'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$totCount = $query->num_rows();

		$i = 1;
		
		foreach($result as $key=>$value)
		{
			$output[]=array('header'=>$value['week_name']." ".date( 'd', strtotime($value['week_date'])),
				'width'=>100, 'columns'=>[['dataIndex'=> 'day'.$i,'text'=>'Hours','draggable'=>'','width'=>100, 'menuDisabled' => '1','sortable' =>'','editor'=>array('xtype'=> 'timefield','minValue' => '0:01','maxValue' => '12:00','format' => 'H:i','increment' => 15),'summaryType'=> 'sum','flex'=>1],['dataIndex'=> 'unit'.$i,'text'=>'Units', 'editor'=>array('xtype'=>'textfield','disabled'=> '1','id'=>'no_of_unit')]]);      
			$i++;
		}
		
		return $output;	
	}

	function get_timesheet_data($emp_id,$week_range_id)
	{
		$sql="SELECT m.task_code_id,t.task_name,s.sub_task_name,c.client_name,m.timesheet_task_id,m.task_week_id,IF(m.day1!='00:00', TIME_FORMAT(m.day1, '%H:%i'),'') AS day1,IF(m.day2!='00:00', TIME_FORMAT(m.day2, '%H:%i'),'') AS day2,IF(m.day3!='00:00', TIME_FORMAT(m.day3, '%H:%i'),'') AS day3,
		IF(m.day4!='00:00', TIME_FORMAT(m.day4, '%H:%i'),'') AS day4,IF(m.day5!='00:00', TIME_FORMAT(m.day5, '%H:%i'),'') AS day5,IF(m.day6!='00:00', TIME_FORMAT(m.day6, '%H:%i'),'') AS day6,
		IF(m.day7!='00:00', TIME_FORMAT(m.day7, '%H:%i'),'') AS day7,comment1,comment2,comment3,comment4,comment5,comment6,comment7,m.status 
		FROM timesheet_task_mapping m
		JOIN timesheet_task_codes t ON t.timesheet_task_code_id=m.task_code_id
		LEFT JOIN timesheet_subtasks s ON s.timesheet_subtask_id=m.sub_task_id
		JOIN client c ON c.client_id=m.client_id
		WHERE emp_id='$emp_id' AND task_week_id='$week_range_id' ORDER BY m.task_code_id DESC";
		//echo $sql; exit;
		$query=$this->db->query($sql);
		$result=$query->result_array();
		
		$data_util = array();
		
		$output= $this->get_production_hours($week_range_id,$emp_id);

		$i=1;
		foreach($result as $key=>$value)
		{
			$data_util['timesheetID'] = $value['timesheet_task_id'];
			//$data_util['past'] = $this->checkpastweek($week_range_id);
			$data_util['ClientName'] = $value['client_name'];
			$data_util['WorkOrderName'] = $value['work_order_name'];
			$data_util['ProjectType'] = $value['project_name'];
			$data_util['Task'] = $value['task_name'];
			$data_util['SubTask'] = isset($value['sub_task_name'])?$value['sub_task_name']:"";
			$data_util['day1'] = isset($value['day1'])?$value['day1']:"03:00";
			$data_util['day2'] = isset($value['day2'])?$value['day2']:"03:00";
			$data_util['day3'] = isset($value['day3'])?$value['day3']:"03:00";
			$data_util['day4'] = isset($value['day4'])?$value['day4']:"03:00";
			$data_util['day5'] = isset($value['day5'])?$value['day5']:"03:00";
			$data_util['day6'] = isset($value['day6'])?$value['day6']:"03:00";
			$data_util['day7'] = isset($value['day7'])?$value['day7']:"03:00";
			$data_util['comment1'] = $value['comment1'];
			$data_util['comment2'] = $value['comment2'];
			$data_util['comment3'] = $value['comment3'];
			$data_util['comment4'] = $value['comment4'];
			$data_util['comment5'] = $value['comment5'];
			$data_util['comment6'] = $value['comment6'];
			$data_util['comment7'] = $value['comment7'];
			$data_util['Status'] = $value['status'];
			
			$i++;
			
			$output[]= $data_util;
		}

		return $output;
	}

	public function duplicateRow()
	{
		// debug($_POST);exit;
		$row_id = $this->input->post('UtilizationID');
		$week_id = $this->input->post('duplicateTo');
		$empData = $this->input->cookie();
		$emp_id  = ($this->input->post('teamMember')!='') ? $this->input->post('teamMember') : $empData['employee_id'];

		$utilization_task_id = explode(',',$row_id);

		$id_length = count($utilization_task_id);

		$affectedRows = $recordExists = 0;
		$dulicatedRecords = array();

		foreach ($utilization_task_id as $key => $tt_id) 
		{

			$row_data_sql = "SELECT * FROM client_utilization_mapping WHERE utilization_id= '$tt_id'";
			$row_data = $this->db->query($row_data_sql)->row_array();

			$client_id = $row_data['client_id'];
			$wor_id = $row_data['wor_id'];
			$project_id = $row_data['project_id'];
			$task_code_id = $row_data['task_id'];
			$sub_task_id = $row_data['sub_task_id'];

			$check_sql = "SELECT * FROM client_utilization_mapping 
			WHERE week_code_id = '$week_id'
			AND client_id = '$client_id'
			$wor_id = $row_data['wor_id'];
			$project_id = $row_data['project_id'];
			AND task_id = '$task_code_id'
			AND sub_task_id = '$sub_task_id'
			AND emp_id = '$emp_id'";
			if($this->db->query($check_sql)->num_rows()>0)
				$recordExists++;

			if($recordExists == 0)
			{
				$row_data['week_code_id'] = $week_id;
				$row_data['status'] = 1;
				$row_data['created_by'] = $emp_id;
				unset($row_data['utilization_id']);
				unset($row_data['created_on']);
				unset($row_data['updated_on']);
				unset($row_data['updated_by']);
				unset($row_data['approved_on']);
				unset($row_data['approved_by']);

				$dulicatedRecords[] = $row_data;
			}
			else
			{
				return -1;
			}
		}

		if(count($dulicatedRecords)>0)
		{
			foreach ($dulicatedRecords as $key => $row_values) {
				$this->db->insert('client_utilization_mapping',$row_values);
				if($this->db->affected_rows()>0)
				{
					$empData    = $this->input->cookie();
					$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$this->db->insert_id().') has been added Utilization duplicate entry Successfully.';
					$this->userLogs($logmsg);
					$affectedRows++;
				}
			}
		}

		return $affectedRows;
	}

	function update_task_comments($UtilizationID, $params)
	{
		$update	= "UPDATE client_utilization_mapping SET updated_on=NOW()";
		
		if(isset($params['comment1']))
		{
			$update.=",comment1='".addslashes($params['comment1'])."'";
		}
		if(isset($params['comment2']))
		{
			$update.=",comment2='".addslashes($params['comment2'])."'";
		}
		if(isset($params['comment3']))
		{
			$update.=",comment3='".addslashes($params['comment3'])."'";
		}
		if(isset($params['comment4']))
		{
			$update.=",comment4='".addslashes($params['comment4'])."'";
		}
		if(isset($params['comment5']))
		{
			$update.=",comment5='".addslashes($params['comment5'])."'";
		}
		if(isset($params['comment6']))
		{
			$update.=",comment6='".addslashes($params['comment6'])."'";
		}
		if(isset($params['comment7']))
		{
			$update.=",comment7='".$params['comment7']."'";
		}
		$update.= " WHERE utilization_id='".$UtilizationID."'";
		// echo $update; exit;
		$this->db->query($update);
		if ($this->db->affected_rows() > 0)
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(UtilizationID:'.$UtilizationID.') has been updated timesheet comments.';
			$this->userLogs($logmsg);
			return 1;
	}

	function get_production_hours($week_range_id,$emp_id='')
	{
		$empData    = $this->input->cookie();
		$emp_id		= ($emp_id=='')?$empData['employee_id']:$emp_id;
		
		$sqldates = "SELECT week_date FROM task_week WHERE week_code_id='".$week_range_id."'";
		$querydates = $this->db->query($sqldates);
		$dates =  $querydates->result_array();
		
		foreach($dates as $key=>$value)
		{
			$dateArr[] = $value['week_date'];
		}
		$datesArr = implode("', '", $dateArr);
		
		$empClient = "SELECT GROUP_CONCAT(DISTINCT fk_client_id SEPARATOR ',') AS client_id FROM utilization WHERE employee_id='".$emp_id."' AND task_date IN('".$datesArr."')";
		$empClient = $this->db->query($empClient);
		$clientId =  $empClient->row_array();
		$client_id = $clientId['client_id'];

		$data_util = array();

		$clientArr = explode(",",$client_id);
		$clientArr = array_unique($clientArr);
		$i=0;
		
		foreach($clientArr as $val)
		{
			$data_util['timesheetID'] = $i.'a';
			$data_util['past'] = 'hide';
			$data_util['ClientName'] = $this->getClientName($val);
			$data_util['Task'] = "Production Work";
			$data_util['SubTask'] = "";
			$data_util['day1'] = $this->getClientHours($val, $dateArr[0], $emp_id);
			$data_util['day2'] = $this->getClientHours($val, $dateArr[1], $emp_id);
			$data_util['day3'] = $this->getClientHours($val, $dateArr[2], $emp_id);
			$data_util['day4'] = $this->getClientHours($val, $dateArr[3], $emp_id);
			$data_util['day5'] = $this->getClientHours($val, $dateArr[4], $emp_id);
			$data_util['day6'] = $this->getClientHours($val, $dateArr[5], $emp_id);
			$data_util['day7'] = $this->getClientHours($val, $dateArr[6], $emp_id);
			$output[]= $data_util;
			$i++;
		}
		
		return $output;
		
	}

	function get_future_week_range()
	{
		$sql = "SELECT * FROM timesheet_week_range WHERE status='1' AND timesheet_week_range_id IN(SELECT DISTINCT week_code_id AS timesheet_week_range_id FROM task_week WHERE week_date<=DATE_ADD(CURDATE(), INTERVAL 15 DAY) ORDER BY week_code_id DESC) ORDER BY timesheet_week_range_id DESC LIMIT 2";
		
		$query=$this->db->query($sql);
		$result=$query->result_array();
		$totCount = $query->num_rows();
		$output["metaData"]["idProperty"]="weekrangeID";
		$output["metaData"]["successProperty"]="success";
		$output["metaData"]["root"]="data";
		foreach($result as $key=>$value)
		{
			$response[$key]['id']=$value['timesheet_week_range_id'];
			$response[$key]['week']=$value['week_range']." (".$value['week_code'].")";
			
		}
		$output["data"] = $response;
		return $output;

	}
	
	private function getClientName($cid='')
	{
		if($cid!='')
		{
			$sql = "SELECT client_name FROM client WHERE client_id=".$cid;
			$query = $this->db->query($sql);
			$list = $query->row_array();
			
			return $list['client_name'];
		}
		else
		{
			return "";
		}
	}
	
	private function getClientHours($cid='', $date='', $emp_id)
	{
		if($cid!='' && $date!='')
		{
			$sql = "SELECT TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') AS hours 
			FROM utilization
			WHERE employee_id='".$emp_id."' AND status NOT IN(0,2) AND task_date in ('".$date."') AND utilization.fk_client_id=".$cid; 
			$sql.=" GROUP BY task_date";
			
			$query = $this->db->query($sql);
			$list = $query->row_array();
			
			return isset($list['hours'])?$list['hours']:'';
		}
		else
		{
			return "";
		}
	}

	function build_query($start_date, $end_date, $date, $EmployID, $clientID) 
	{
		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;					
		$sql_utilization = "SELECT CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,wor.work_order_id, employees.company_employ_id, utilization.* ,task_codes.task_code AS task_code_value, process.name AS process_name 
		FROM  `utilization`
		LEFT JOIN `employees` ON utilization.employee_id = `employees`.`employee_id`
		LEFT JOIN `wor` ON utilization.fk_wor_id = `wor`.`wor_id` 
		LEFT JOIN task_codes  ON task_codes.task_id = utilization.task_code
		LEFT JOIN process ON process.process_id = utilization.fk_role_id
		WHERE utilization.client_id=".$clientID;
		if($EmployID!=0)
			$sql_utilization .=" AND utilization.employee_id =". $EmployID;
		if($date!="")
			$sql_utilization .=" AND utilization.task_date='".$date."'";
		if($start_date!="" && $end_date!="")
			$sql_utilization .=" AND utilization.task_date BETWEEN '".$start_date."' AND '".$end_date."'";

		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) 
					{
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .=$filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') 
			{
				$sql_utilization .= " and (".$filterWhere.")";
			} 
		} 
		return $sql_utilization;
	}
	
	function build_Insight_query($start_date, $end_date, $date, $EmployID, $clientID, $workOrder, $project_code, $utilstatus, $tab_type, $wor_type_id, $project_type_id,$supervisor) 
	{
		error_reporting(E_ALL);
		$empData = $this->input->cookie();
		$sess_EmpID = $empData['employee_id'];
		$grade = $empData['Grade'];
		
		$filter = json_decode($this->input->get('filter'),true);

		$options['filter'] = $filter;	
		$sql_utilization = "SELECT DATE_FORMAT(utilization.task_date, '%d-%b-%Y') AS task_date ,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Employee Name`,wor.work_order_id, project_type, task_codes.task_code AS task_id  ,utilization.fk_sub_task_id AS sub_task , utilization.utilization_id, utilization.fk_client_id AS client_id ,client.client_name, process.name AS role_code, utilization.request_name ,  utilization.client_hrs ,utilization.theorem_hrs ,utilization.remarks ,  utilization.status,task_codes.task_code AS task_code_value 
		FROM  `utilization`
		LEFT JOIN `employees` ON utilization.employee_id = `employees`.`employee_id`
		LEFT JOIN `wor` ON utilization.fk_wor_id = `wor`.`wor_id` 
		LEFT JOIN task_codes  ON task_codes.task_id = utilization.fk_task_id
		LEFT JOIN `client` ON utilization.fk_client_id = `client`.`client_id`
		LEFT JOIN project_types ON project_types.project_type_id = utilization.fk_project_id
		LEFT JOIN process ON process.process_id = utilization.fk_role_id
		WHERE utilization.fk_client_id=".$clientID; 
		if($workOrder!="")
			$sql_utilization .=" AND utilization.fk_wor_id ='".$workOrder."'";				
		if($project_code!="")
			$sql_utilization .=" AND utilization.fk_role_id ='".$project_code."'";				
		if($wor_type_id!="")
			$sql_utilization .=" AND utilization.fk_type_id ='".$wor_type_id."'";				
		if($project_type_id!="")
			$sql_utilization .=" AND utilization.fk_project_id ='".$project_type_id."'";			
		if($EmployID!=0){
			$sql_utilization .=" AND utilization.employee_id =". $EmployID;				
		}/*else{
			if (is_array($this->getEmployeeList($sess_EmpID)))
				$employeeArray = implode(',', $this->getEmployeeList($sess_EmpID));

			if($grade>2 && ($grade<5))
			{
				$sql_utilization .= ' AND utilization.employee_id IN ('.$employeeArray.')';
			}
		}*/
		if($grade>2 && $grade<5)
			$sql_utilization .=" AND utilization.employee_id !=". $sess_EmpID;					
		if($date!="")
			$sql_utilization .=" AND utilization.task_date='".$date."'";
		if($start_date!="" && $end_date!="")
			$sql_utilization .=" AND utilization.task_date BETWEEN '".$start_date."' AND '".$end_date."'";
		if($tab_type == 'weekend_coverage')
			$sql_utilization .=" AND utilization.support_type IN(1) ";
		if($tab_type == 'holiday_coverage')
			$sql_utilization .=" AND utilization.support_type IN(2) ";
		if($utilstatus == '3')
			$sql_utilization .=" AND utilization.status IN(3,4) ";
		else if($utilstatus != 'All')
			$sql_utilization .=" AND utilization.status IN('".$utilstatus."') ";

		if($supervisor!='')
		{
			$emp_ids = $this->getEmpForSupervisor($supervisor);
			$sql_utilization .= " AND utilization.employee_id IN ($emp_ids)";
		}
		// debug($emp_list);exit;
		// echo "$sql_utilization";exit;
		
		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) 
					{
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .=$filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') 
			{
				$sql_utilization .= " and (".$filterWhere.")";
			} 
		}
		
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$sql_utilization .= " limit ".$options['offset']." , ".$options['limit'];
		}
		else if(isset($options['limit'])) 
		{
			$sql_utilization .= " limit ".$options['limit'];
		}
		
		// echo $sql_utilization;exit;
		return $sql_utilization;
	}


	public function getEmployUtilization($empId)
	{
		$result = array();
		$queryToGetUtilization = $this->db->select('utilization_id, processid, task_id, employid, description, task_date, hours, min')->get("utilization");
		$resultSetUtilization = $queryToGetUtilization->result_array();  
		$loop1=0;
		foreach($resultSetUtilization as $key1=>$value1) 
		{ 
			$result[$loop1] = $value1;
			$queryToGetAttributes = $this->db->select('attributes.name, utilization_attributes.value')->join('attributes','attributes.attributesid = utilization_attributes.attributesid')->where("utilization_attributes.utilization_id",$value1['utilization_id'])->get("utilization_attributes");
			$attributesList = $queryToGetAttributes->result_array();
			$arrayAttributes = array();
			$loop2=0;
			foreach($attributesList as $key=>$value2) 
			{
				$result[$loop1][$attributesList[$loop2]['name']] = $attributesList[$loop2]['value'];
				$loop2++;
			}
			$loop1++;
		}
		return $result;
	}

	function insightview($params) 
	{
		$clientID = (isset($params['client_id'])) ? $params['client_id'] : "";
		$output = false;
		$dashboard = (isset($params['dashboard'])) ? $params['dashboard'] : "";
		$approver = TRUE;
		if($dashboard==1)
			//$approver = $this->checkApprover($ProcessID);
			if(1)
			{
				$EmployID = (isset($params['EmployID'])) ? $params['EmployID'] : "";
				$date = $start_date = $end_date = $workOrder = $project_code = $tab_type = $tab_type = $wor_type_id = $project_type_id = $supervisor = "";

				if(isset($params['utilization_date']))
					$date = date('Y-m-d', strtotime($params['utilization_date']));
				if(isset($params['utilization_start_date']))
					$start_date = date('Y-m-d', strtotime($params['utilization_start_date']));
				if(isset($params['utilization_end_date']))
					$end_date = date('Y-m-d', strtotime($params['utilization_end_date']));
				if(isset($params['wor_id']))
					$workOrder = $params['wor_id'];
				if(isset($params['project_code']))
					$project_code = $params['project_code'];
				if(isset($params['tab_type']))
					$tab_type = $params['tab_type'];
				if(isset($params['wor_type_id']))
					$wor_type_id = $params['wor_type_id'];
				if(isset($params['project_type_id']))
					$project_type_id = $params['project_type_id'];
				if(isset($params['supervisor']))
					$supervisor = $params['supervisor'];

				$utilstatus = isset($params['utilstatus'])?$params['utilstatus']:'All';

				$output["metaData"]["idProperty"]="utilization_id";
				$output["metaData"]["totalProperty"]="total";
				$output["metaData"]["successProperty"]="success";
				$output["metaData"]["root"]="data";

				if($dashboard==1)
				{
					$options = array();
					if($this->input->get('hasNoLimit') != '1')
					{
						$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
						$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
					}
					$sort = json_decode($this->input->get('sort'),true);

					$options['sortBy'] = $sort[0]['property'];
					$options['sortDirection'] = $sort[0]['direction'];
				}

				//get total count
				$sql_utilization = $this->build_Insight_query($start_date, $end_date, $date, $EmployID, $clientID, $workOrder, $project_code, $utilstatus, $tab_type, $wor_type_id, $project_type_id, $supervisor);
				// echo $sql_utilization;exit;
				$totQuery = $this->db->query($sql_utilization);

				//$client_attrib = $this->Client_Attriutes($clientID);
				//unset($client_attrib['status']);
				$totQuery = $this->db->query($sql_utilization);
				$total_res = $totQuery->result_array();
				// debug($total_res);exit;
				
				$timearray1 = array();
				foreach($total_res as $k1 => $v1)
				{
					$timearray1[] = $v1['client_hrs'];
				} 

				if($dashboard==1)
				{
					$sql_utilization .= " order by utilization.task_date DESC,utilization.utilization_id DESC";

					if(isset($options['limit']) && isset($options['offset'])) 
					{
						if($options['offset'] < 0){	$options['offset'] = 0;}	

						$sql_utilization .= " limit ".$options['offset']." , ".$options['limit'];
					}
					else if(isset($options['limit'])) 
					{
						$sql_utilization .= " limit ".$options['limit'];
					}
				}

				// echo $sql_utilization;exit;
				$output["data"] = array();
				$query = $this->db->query($sql_utilization);

				$timearray = array();
				$keys = array();
				$keyss = array();
				$dyn_ata = array();
				$dyn_ata = array();
				$checktask_date = array();

				$result = $query->result_array();
				// debug($result);exit;

				$client_id_attr = array();
				$utilization_id_attr = array();

				foreach($result as $key => $value)
				{
					$client_id_attr[] = $value['client_id'];
					$utilization_id_attr[] = $value['utilization_id'];
				}
		
				$result1 = implode(',',$client_id_attr);
				$result2 = implode(',',$utilization_id_attr);
				$client_attrib = $this->Client_Attriutes($result1, $result2);

				foreach($result as $key => $value)
				{
					unset($result[$key]['task_code_value']);											
				}
				foreach($result as $key => $value)
				{
					$keys = array_keys($value);
				}	
				foreach($keys as $k2 => $v2)
				{
					$ret = str_replace("_"," ",$v2);									
					$keyss[]= ucwords($ret);
				}	
				foreach($keys as $k1 => $v1)
				{
					$output["metaData"]["fields"][]= $v1;
				}	

				foreach($client_attrib as $cli_key => $cli_val)
				{
					$output["metaData"]["fields"][]= $cli_key;
				}	

				if($dashboard==1)
				{
					$output["metaData"]["fields"][]="Action";
					$output["metaData"]["fields"][]="approved";
				}

				if(count($result)>0)
				{
					$out = array_combine($keys,$keyss);
				}
				else
				{
					$out['task_date'] = 'Task Date';
					$out['Employee Name'] = 'Employee Name';
					$out['work_order_id'] = 'Work Order Id';
					$out['project_type'] = 'Project Type';
					$out['task_id'] = 'Task Id';
					$out['sub_task'] = 'Sub Task';
					$out['utilization_id'] = 'Utilization Id';
					$out['client_id'] = 'Client Id';
					$out['client_name'] = 'Client Name';
					$out['role_code'] = 'Role Code';
					$out['request_name'] = 'Request Name';
					$out['client_hrs'] = 'Client Hrs';
					$out['theorem_hrs'] = 'Theorem Hrs';
					$out['remarks'] = 'Remarks';
					$out['status'] = 'Status';			
				}

				// debug($out);exit;
				// foreach($result as $key => $value)
				// {
					// $attributes = $this->GetAttributesValues($value['client_id'] , $value['utilization_id']);
					// ?foreach($attributes as $attr){
						// if($attr['attributes_value'] > 0){
							$fine_out= array_merge_recursive($out,$client_attrib);
							//var_dump($fine_out);exit;
						// } else {
						// 	$fine_out= array_merge_recursive($out);
						// }
					// }
				// }
				unset($fine_out['client_id']);
				foreach($fine_out as $fk => $fv)
				{
					unset($fine_out['approved']);
					unset($fine_out['utilization_id']);
				}
				foreach($fine_out as $out_key => $out_val)
				{
					if($out_key=="client_hrs")
					{
						$out_val = "Client Hours";
					}
					if($out_key=="theorem_hrs")
					{
						$out_val = "Theorem Hours";
					}
					if($out_key!="wor_id" && $out_key!='')
					{
						$dyn_ata[]=   array(
							'header' =>$out_val, 
							'dataIndex' => $out_key, 
							'editor' => array('xtype'=>'displayfield'),  
							'width'=>'164x',
							'menuDisabled'=>true,
							'groupable'=>false,
							'draggable'=>false,
						);
					}
				}
				//debug($dyn_ata);exit;
				//unset($dyn_ata);
				
				$output["columns"] = $dyn_ata;


				$this->db->from('employees');
				$this->db->where('employee_id',$EmployID);
				$emp = $this->db->get(); 
				$emp_result = $emp->result_array();
				$FullName = "";
				$CmpnyEmpID = "";
				foreach($emp_result as $k => $v)
				{
					$FullName = $v['first_name'];
					$CmpnyEmpID = $v['company_employ_id'];
				}

				$output["success"]=true;
				$output["message"]="success";
				foreach($result as $key => $value)
				{
					$result[$key]['AdditionalAttributes'] = $this->GetAttributesValues($value['client_id'] , $value['utilization_id']);
// 
					/*Regex added to avoid Unwanted sting passing in the Query*/
					$regex="/^[0-9,]+$/";
					if(!preg_match($regex,$value['sub_task']))
					{
						$sub_task = "";
					}
					else
					{
						$sub_task = $value['sub_task'];
					}

					$result[$key]['sub_task']	= $this->getSubtaskNames($sub_task);
					$timearray[] = $value['client_hrs'];
				}


				//debug($result);exit;
				foreach($result as $k => $v)
				{
					foreach($v as $k1 => $v1)
					{
						if($k1 == "AdditionalAttributes")
						{
							foreach($v1 as $k12 => $v12)
							{
								$result[$k][$k12] = $v12;
							}					
						}
					}
				}

				//debug($result);exit;

				foreach($result as $key => $value)
				{
					if($value['status']==0)
						$result[$key]['status'] = "Pending";
					if($value['status']==1)
						$result[$key]['status'] = "Approved";
					if($value['status']==2)
						$result[$key]['status'] = "Rejected";
					if($value['status']==3)
						$result[$key]['status'] = "Weekend Hours";
					if($value['status']==4)
						$result[$key]['status'] = "Client Holiday Hours";
					unset($result[$key]['AdditionalAttributes'] );
				}
				//debug($result);exit;

				foreach($result as $key => $val)
				{
					$data_util = array();
					foreach($val as $k1 => $v1)
					{
						if($k1 == "approved" && $v1 == 0){
							$v1 = "Pending";
						}
						if($k1 == "approved" && $v1 ==1){
							$v1 = "Approved";
						}
						if($k1 == "approved" && $v1 ==2){
							$v1 = "Rejected";
						}
						if($k1 == "approved" && $v1 ==3){
							$v1 = "weekend_billable";
						}
						if($k1 == "approved" && $v1 ==4){
							$v1 = "holiday_billable";
						}
						$data_util[$k1] = $v1;
					}
					$output["data"][]= $data_util;
				}

				//debug($output["data"]);exit;

				$hours = "";
				$minutes = "";
				//var_dump($timearray);exit;

				foreach ($timearray as $v) 
				{
					$chunks= explode(":", $v);
					$hours += $chunks[0];
					$minutes += $chunks[1];
				}

				$hours += floor($minutes /60);
				$minutes %= 60;

				$overall_hours = "";
				$overall_minutes = "";
				
				foreach ($timearray1 as $v2) 
				{
					$overall_chunks= explode(":", $v2);
					$overall_hours += $overall_chunks[0];
					$overall_minutes += $overall_chunks[1];
				}

				$overall_hours += floor($overall_minutes /60);
				$overall_minutes %= 60;

				$output['TotalHours'] = "<b>Grid Total</b> = ".$hours." Hrs  ".$minutes." Min";
				$output['OverallTime'] = "<b>Overall Time</b> = ".$overall_hours." Hrs  ".$overall_minutes." Min";

				$output["total"]=$totQuery->num_rows();
				//debug($totQuery->num_rows());
			}

			 //debug($output);exit;

			return $output;
		}

		function Client_Attriutes($clientID,$utilizationID,$type='')
		{
			/* added to display in order of Process Attributes*/
			$sqlattri = "SELECT label_name,field_name 
							FROM client_attributes  
							LEFT JOIN new_attributes ON new_attributes.attributes_id = client_attributes.attributes_id 
							LEFT JOIN utilization_client_attributes ON utilization_client_attributes.attributes_id = 
								client_attributes.attributes_id
							WHERE client_id = '$clientID' AND client_attributes.wor_id IS NOT NULL AND utilization_client_attributes.utilization_id IN('$utilizationID') AND utilization_client_attributes.attributes_value <> '' GROUP BY new_attributes.label_name
							";
							//echo $sqlattri;
			$attrires = $this->db->query($sqlattri);

			$attribuites = array();
				if($type == 'exportcsv')
				{
					foreach($attrires->result_array() as $attr => $attri)
					{
						$attribuites[$attri['label_name']] = '';

					}
				}
				else
				{
					foreach($attrires->result_array() as $attr => $attri)
					{
						$attribuites[$attri['field_name']] = $attri['label_name'];

					}
				}
			return $attribuites;
		}


		function getattrib($client_id,$attributes_id)
		{
			$sql = "SELECT attributes_id FROM client_attributes WHERE client_id = ".$client_id." AND attributes_id = " .$attributes_id;
			$query = $this->db->query($sql);
			if ($query->num_rows () > 0) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}


		function list_Utilization()
		{
			error_reporting(E_ALL);
			$empData = $this->input->cookie();
			$EmpID = $empData['employee_id'];

			$filter				= json_decode($this->input->get('filter'),true);
			$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";
			$filterQuery 		= $this->input->get('query');
			$sort				= json_decode($this->input->get('sort'),true);
			$client_id      	= $this->input->get('client_id') ? $this->input->get('client_id') : "";
			$wor_id      		= $this->input->get('wor_id') ? $this->input->get('wor_id') : "";
			$combo_date      	= $this->input->get('date') ? $this->input->get('date') : "0";

			$options = array();
			if($this->input->get('hasNoLimit') != '1')
			{
				$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
			}

			$options['filter']	= $filter;

			$sortBy = $sort[0]['property'];
			$sortDirection = $sort[0]['direction'];

			$sql = "SELECT utilization_id,date_format(task_date, '%d-%b-%Y') AS task_date,fk_wor_id ,utilization.fk_wor_id AS wor_id,utilization.fk_client_id ,employee_id ,project_types.project_type AS project_type, utilization.fk_project_id,process.name AS process_name , utilization.fk_role_id,utilization.fk_task_id ,fk_sub_task_id ,task_codes.task_code As task_code_value , TIME_FORMAT(client_hrs, '%H:%i' ) as client_hrs, TIME_FORMAT(theorem_hrs, '%H:%i' ) as theorem_hrs, request_name, utilization.status, utilization.remarks FROM utilization ";
			$sql .= " LEFT JOIN task_codes ON task_codes.task_id = utilization.fk_task_id";
			$sql .= " LEFT JOIN process ON process.process_id = utilization.fk_role_id";
			$sql .= " LEFT JOIN project_types ON project_types.project_type_id = utilization.fk_project_id";	
			$sql .= " WHERE utilization.fk_client_id = '$client_id' ";

			if($EmpID == 2800 || $EmpID == 2485){
				$sql .= " AND DATE(task_date) > (NOW() - INTERVAL 18 DAY)";
			}
			else if($combo_date=="1")
			{
				$sql .= " AND DATE(task_date) > (NOW() - INTERVAL 16 DAY)";
			}
			else
			{
				$sql .= " AND DATE(task_date) > (NOW() - INTERVAL 8 DAY)";
			}


			$sql .= " AND employee_id  = '$EmpID' ";
			if($wor_id != "")
			{
				$sql .= " AND fk_wor_id  = $wor_id ";
			}

			// echo $sql; exit;
			$filterWhere = '';

			if(isset($options['filter']) && $options['filter'] != '') 
			{
				foreach ($options['filter'] as $filterArray) 
				{
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) 
					{
						if($filterField != '') 
						{
							$filterWhere .= ($filterWhere != '')?' OR ':'';
							$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
						}
					}
				}

			}

			if ($filterName != "") 
			{
				$filterWhere .= $filterName . " LIKE '%" . $filterQuery . "%'";
			}

			$query = $this->db->query($sql);
			$totCount = $query->num_rows();
			$query1 = $this->db->query($sql);
			$data1 = $query1->result_array();

			$timearray1 = array();

			foreach($data1 as $k1 => $v1)
			{
				$timearray1[] = $v1['client_hrs'];
			}

			$sql .= " ORDER BY utilization_id DESC";

			if(isset($options['limit']) && isset($options['offset'])) 
			{
				$limit = $options['offset'].','.$options['limit'];
				$sql .= " limit ".$limit;
			}

			// echo $sql;exit;

			$query = $this->db->query($sql);
			$data = $query->result_array();

			// echo"<pre>";print_r($data);exit;

			$timearray = array();

			foreach($data as $key => $value)
			{

				$data[$key]['AdditionalAttributes'] = $this->GetAttributesValues($client_id , $value['utilization_id'],'list');

				/*Regex added to avoid Unwanted sting passing in the Query*/
				$regex="/^[0-9,]+$/";
				if(!preg_match($regex,$value['fk_sub_task_id']))
				{
					$sub_task = "";
				}
				else
				{
					$sub_task = $value['fk_sub_task_id'];
				}

				$data[$key]['Sub_task_Values']	= $this->getSubtaskNames($sub_task);

				if($value['status'] == 0){
					$v1 = "Pending";
				}
				if($value['status'] ==1){
					$v1 = "Approved";
				}
				if($value['status'] ==2){
					$v1 = "Rejected";
				}
				$data[$key]['status'] = $v1; 
				$timearray[] = $value['client_hrs'];
			}

			$hours = ''; $minutes = '';$seconds = '';

			// echo"<pre>";print_r($data);exit;

			foreach ($timearray as $v) 
			{
				$chunks= explode(":", $v);
				$hours += @$chunks[0];
				$minutes += @$chunks[1];
				$seconds += @$chunks[2];
			}
			$minutes += floor($seconds / 60);
			$seconds %= 60;

			$hours += floor($minutes /60);
			$minutes %= 60;

			$overall_hours = ''; $overall_minutes = '';$overall_seconds = '';

			// print_r($timearray1);exit;

			foreach ($timearray1 as $v2) 
			{
				$overall_chunks= explode(":", $v2);
				$overall_hours += @$overall_chunks[0];
				$overall_minutes += @$overall_chunks[1];
				$overall_seconds += @$overall_chunks[2];
			}
			$overall_minutes += floor($overall_seconds / 60);
			$overall_seconds %= 60;

			$overall_hours += floor($overall_minutes /60);
			$overall_minutes %= 60;

			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);

			$results['TotalHours'] = "<b>Grid Total</b> = ".$hours." Hrs  ".$minutes." Min";
			$results['OverallTime'] = "<b>Overall Time</b> = ".$overall_hours." Hrs  ".$overall_minutes." Min";
			return $results;
		}

		function GetAttributesValues($client_id,$uitlization_id,$type='') 
		{
			$sqlattri = "SELECT utilization_client_attributes.attributes_id, label_name, field_name, attributes_value 
			FROM utilization_client_attributes 
			JOIN client_attributes ON client_attributes.attributes_id = utilization_client_attributes.attributes_id 
			JOIN new_attributes ON new_attributes.attributes_id = client_attributes.attributes_id  
			WHERE utilization_id  =".$uitlization_id." AND client_attributes.client_id=".$client_id;

			$attrires = $this->db->query($sqlattri);
			$attribuites = array();
			
			foreach($attrires->result_array() as $attr => $attri)
			{
				if($type == "export")
				{
					$attribuites[$attri['label_name']] = $attri['attributes_value'];
				}
				elseif($type == 'list')
				{
					$attribuites["addtional_".$attri['attributes_id']] = $attri['attributes_value'];
				}
				else
				{	
					$attribuites[$attri["field_name"]] = $attri['attributes_value'];
				}
			}

			return $attribuites;
		}

		private function getSubtaskNames($SubArr)
		{
			$SubtaskNameArr = str_replace(",", "','", $SubArr);
			$SubArr = rtrim($SubArr,',');
			if($SubArr != '')
			{
				$sql = "SELECT GROUP_CONCAT(sub_tasks.`task_name`) AS sub_tasks 
				FROM sub_tasks 	
				WHERE sub_task_id IN ($SubArr)";
				$empRst = $this->db->query($sql);
				$SubtaskNameArr = $empRst->row_array();
				// echo"<pre>";print_r($SubtaskNameArr);exit;
				return $SubtaskNameArr['sub_tasks'];
			}
			else
			{
				return '';
			}
		}


		function checkApprover($ProcessID)
		{
			$empData = $this->input->cookie();

			$this->config->load('admin_config');
			$super_admin = $this->config->item('super_admin');

			if(in_array($empData['Email'], $super_admin))
			{
				return true;
			}
			else if(isset($empData['employee_id']))
			{
				$employee_id = $empData['employee_id'];
				$select_grade = "SELECT `grades` FROM `designation` WHERE `designation_id` = (SELECT `designation_id` FROM `employees` WHERE `employee_id` = ".$employee_id.")";
				$query_grade = $this->db->query($select_grade);
				if(is_array($query_grade->result_array()))
				{
					foreach ($query_grade->result_array() as $k => $v)
						$grade = $v['grades'];
					$result = true;

					return $result;
				}
			}
		}


		function deleteAttributesValues( $idVal = '' ) 
		{
			$options = array();
			$options['utilization_id'] =  $idVal;
			$retVal = $this->delete($options, 'utilization_client_attributes');

			return $retVal;
		}


		function getTotalHours( $data = '' ) 
		{
			$timearray = array();
			$task_date	= date('Y-m-d', strtotime($data['task_date']));

			$hrsQry = "SELECT TIME_FORMAT(theorem_hrs, '%H:%i' ) as theorem_hrs FROM utilization WHERE fk_client_id = '".$data['client_id']."' AND fk_wor_id = '".$data['wor_id']."' AND task_date = '".$task_date."'";
			$hrsResult = $this->db->query($hrsQry);
			foreach($hrsResult->result_array() as $k1 => $v1)
			{
				$timearray[] = $v1['theorem_hrs'];
			}
			foreach ($timearray as $v2) 
			{
				$overall_chunks= explode(":", $v2);
				$overall_hours += $overall_chunks[0];
				$overall_minutes += $overall_chunks[1];
				$overall_seconds += $overall_chunks[2];
			}
			$overall_minutes += floor($overall_seconds / 60);
			$overall_seconds %= 60;

			$overall_hours += floor($overall_minutes /60);
			$overall_minutes %= 60;

			return $overall_hours.":".$overall_minutes;
		}

		function GetTotalUtilizedHours($task_date,$new_hrs,$employee_id,$utilization_id) 
		{
			$task_date	= date('Y-m-d', strtotime($task_date));
			$current_hrs = date('H:i', strtotime($new_hrs));

			sscanf($new_hrs, "%d:%d", $hours, $minutes);

			$time_seconds = $hours * 3600 + $minutes * 60;

			if($utilization_id == "" || $utilization_id == 0){
				$hrsQry = "SELECT SUM(TIME_TO_SEC(theorem_hrs)) as theorem_hrs FROM utilization WHERE employee_id = '".$employee_id."' AND task_date = '".$task_date."'";
			}else{
				$hrsQry = "SELECT SUM(TIME_TO_SEC(theorem_hrs)) as theorem_hrs FROM utilization WHERE employee_id = '".$employee_id."' AND task_date = '".$task_date."' AND utilization_id NOT IN ('$utilization_id')";
			}

			$query  = $this->db->query($hrsQry);
			$row = $query->row();
			if (isset($row))
			{		
				$dbhrs = $row->theorem_hrs;
				$comp_util = $dbhrs + $time_seconds;
				if($comp_util >= 86400){
					return 1;
				}else{
					return 0;
				}
			}
			else 
			{
				return  0;
			}	

		}


		function saveUtilization($data='')
		{
			$utilization = $data;
			$empData = $this->input->cookie();

			$attrib_array = array();
			$utilization_id = (isset($utilization['utilization_id']) && $utilization['utilization_id']!="0") ? $data['utilization_id'] : 0;

			if(isset($utilization['duplicate'])){
				$utilization_id= "";
			}

			$total_hrs_day = $this->GetTotalUtilizedHours($utilization['task_date'],$utilization['theorem_hrs'],$empData['employee_id'],$utilization_id);

			if ($total_hrs_day == 1){
				$ret = -1; 
				return $ret;
			}
			
			foreach($utilization as $key=>$value)
			{
				if(isset($utilization['project_code']))
				{
					$project_code = (explode("_",$this->input->post('roleName')));
					$model = $project_code[0];
					$support = $project_code[3];
				}

				if (strpos($key, 'addtional_') !== false) 
				{
					$attrib_array[str_replace("addtional_","",$key)] = $value;
				}
			}

			$timestamp =  strtotime($utilization['task_date']);
			$day = date('D', $timestamp);
			$weekend_support = $this->WeekendSupport($day,$support);

			if($weekend_support == 0)
			{
				$project_id = $this->GetProjectIdFromCode($this->input->post('roleName'),$utilization['client_id']);
				$support_type = $this->HolidayCoverage($utilization['client_id'],$project_id,date('Y-m-d', strtotime($utilization['task_date'])));
			}
			else
			{
				$support_type = $weekend_support;
			}
			
			date_default_timezone_set('Asia/Kolkata');
			$final_data = array(
				'employee_id' 			=> $empData['employee_id'],
				'task_date' 			=> date('Y-m-d', strtotime($utilization['task_date'])),
				'fk_client_id' 			=> $utilization['client_id'],
				'fk_wor_id' 			=> $utilization['wor_id'],
				'fk_type_id'			=> $this->getWorTypeId($utilization['project_type_id']),
				'fk_project_id'			=> $utilization['project_type_id'],
				'fk_role_id' 			=> $utilization['project_code'],
				'fk_task_id' 			=> $utilization['task_code'],
				'fk_sub_task_id' 		=> $utilization['subtaskval'],
				'request_name' 			=> $utilization['request_name'],
				'client_hrs' 			=> (isset($utilization['client_hrs']))?date('H:i', strtotime($utilization['client_hrs'])):'00:00',
				'theorem_hrs' 			=> date('H:i', strtotime($utilization['theorem_hrs'])),
				'remarks' 				=> $utilization['remarks'],
				'support_type' 			=> $support_type,
				'attributes' 			=> '',
				'created_by' 			=> $empData['employee_id'],
				'created_on' 			=> date("Y-m-d H:i:s"),
				'status'				=> 0
			);

			if(isset($utilization_id) && $utilization_id != 0)
			{		
				$final_data['updated_by'] = $empData['employee_id'];
				$final_data['updated_on'] = date("Y-m-d H:i:s");
				$retVal = $this->update($final_data, array("utilization_id"=>$utilization_id), 'utilization');

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization_id.') has been updated utilization Successfully.';
				$this->userLogs($logmsg);

				$this->deleteAttributesValues($utilization_id);

				foreach($attrib_array as $key=>$value)
				{
					$dataset = array(
						'utilization_id'		=> $utilization_id,
						'attributes_id' 	    => $key,
						'attributes_value' 	    => $value,
					);

					$this->insert($dataset, 'utilization_client_attributes');
				}
			}
			else
			{
				$retVal = $this->insert($final_data, 'utilization');
				$insertid = $this->db->insert_id();

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$insertid.') has been added utilization Successfully.';
				$this->userLogs($logmsg);

				foreach($attrib_array as $key=>$value)
				{
					$dataset = array(
						'utilization_id'		=> $insertid,
						'attributes_id' 	    => $key,
						'attributes_value' 	    => $value,
					);

					$this->insert($dataset, 'utilization_client_attributes');
				}
			}

			return $retVal;
		}

		function saveDashboardUtilization($data)
		{
			$utilization = $data;
			$utilization_id = $data['utilization_id'];
			$utildata = json_decode($data['utilization'], true);
			$data['status'] = $utildata['status'];

			/* added to display in order of Process Attributes*/
			$sqlattri = "SELECT attributes_id FROM client_attributes WHERE client_id = ".$utilization['client_id'];
			$queryatri = $this->db->query($sqlattri);

			$attrbutesIDs = array();
			foreach($queryatri->result_array() as $attr => $attri)
			{
				$attrbutesIDs[] = $attri['attributes_id'];
			}
			/* added to display in order of Process Attributes*/

			/* Modified attributesIds and added Order by field to display in order of Process Attributes*/
			$sql = ' SELECT * FROM attributes WHERE attributes_id IN ("' . implode('", "', $attrbutesIDs) . '") ORDER BY FIELD(attributes_id,"' . implode('", "', $attrbutesIDs) . '")';
			$query = $this->db->query($sql);						

			$attValue =""; 
			$data_attribuits = array();

			if($utildata['status']== 0)
			{ 
				$apprStatus = "Pending"; 
			}
			else if($utildata['status']== 1)
			{ 
				$apprStatus = "Approved"; 
			} 
			else if($utildata['status']== 2)
			{ 
				$apprStatus = "Rejected"; 
			}

			$data = array(
				'client_id' 	=> $utilization['client_id'],
				'task_id' 		=> cleanse(htmlspecialchars ($utildata['task_id'])),
				'description' 	=> cleanse(htmlentities ($utildata['description']), ENT_COMPAT, 'UTF-8'),
				'hours' 		=> $utildata['hours'],
				'min' 			=> $utildata['minutes'],
				'status'		=> $utildata['status'],
			);
			$retVal = false;
			if(isset($utilization_id) && $utilization_id != 0)
			{
				$retVal = $this->update($data, array("utilization_id"=>$utilization_id), 'utilization');
			}			
			return $retVal;
		}

		function downloadCsv($inputValues) 
		{
			$empData = $this->input->cookie();
			$EmpID = $empData['employee_id'];

		/**
			0 => Last 7 Days
			1 => Last 15 Days
		*/
			// debug($inputValues);exit;

			// Centaur Media(371), Haymarket Business Media(283), SkyMedia(363), Viacom(313)

			$TemplateImport = (isset($inputValues['Export'])) ? $inputValues['Export'] : "";
			$combo_date = (isset($inputValues['utilization_date'])) ? $inputValues['utilization_date'] : 0;

			$clientID = $inputValues['client_id'];

			if($TemplateImport=="Template")
			{    
				$data = array();
				$result = array();

				$return_array = array("task_date","wor_type_id","project_type_id","role_code","task_code","sub_task","request_name","client_hours","theorem_hours","remarks");
				
				$sqlattri = "SELECT client_attributes.attributes_id,label_name,field_name,field_type,`new_attributes`.values 
				FROM `client_attributes` 
				JOIN `new_attributes` ON new_attributes.attributes_id = client_attributes.attributes_id  
				WHERE visible=1 AND client_id = ".$inputValues['client_id'];

				// echo $sqlattri;exit;

				$queryatri = $this->db->query($sqlattri);

				if($queryatri->num_rows() > 0)
				{
					foreach($queryatri->result_array() as $uk => $val)
					{
						$query = $this->db->query("SELECT attributes_id, field_name FROM new_attributes WHERE attributes_id = ".$val['attributes_id']." ORDER BY FIELD(attributes_id, ".$val['attributes_id'].") ");
						$attrData = $query->result_array();	
						array_push($return_array, $val['field_name']);
					}		
				}
			}
			else 
			{
				$data = array();
				$return_array = array();

				$sql = "SELECT employees.company_employ_id As Employee_id, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Full_name`, utilization.utilization_id , DATE_FORMAT(utilization.task_date,'%d-%b-%Y') as task_date,client.client_id,client.client_name As Client , wor.work_order_id  ,project_types.project_type AS Project, process.name AS role_code , task_codes.task_code AS task_code ,utilization.fk_sub_task_id,utilization.request_name ,utilization.client_hrs as client_hours, utilization.theorem_hrs as theorem_hours, remarks,utilization.status 
				FROM  `utilization`
				LEFT JOIN `employees` ON `utilization`.`employee_id` = `employees`.`employee_id`
				LEFT JOIN `client` ON `client`.`client_id` = `utilization`.`fk_client_id`
				LEFT JOIN `wor` ON utilization.fk_wor_id = `wor`.`wor_id`
				LEFT JOIN project_types ON project_types.project_type_id = utilization.fk_project_id
				LEFT JOIN sub_tasks ON sub_tasks.sub_task_id = utilization.fk_sub_task_id
				LEFT JOIN task_codes  ON task_codes.task_id = utilization.fk_task_id
				LEFT JOIN process ON process.process_id = utilization.fk_role_id  
				WHERE utilization.fk_client_id='".$inputValues['client_id']."' AND utilization.employee_id =$EmpID ";

				if($combo_date=="1")
				{
					$sql .= " AND DATE(utilization.task_date) > (NOW() - INTERVAL 16 DAY)";
				}
				else
				{
					$sql .= " AND DATE(utilization.task_date) > (NOW() - INTERVAL 8 DAY)";
				}

				// echo $sql; exit;

				$query = $this->db->query($sql);
				$result = $query->result_array();

				$project= array();
				$task_code_array= array();
				
				foreach($result as $key => $value)
				{
					$additional_attributes = $this->GetAttributesValues($value['client_id'] , $value['utilization_id'],'export');

					$result[$key]['AdditionalAttributes'] = $additional_attributes;

					/*Regex added to avoid Unwanted sting passing in the Query*/				
					$regex="/^[0-9,]+$/";
					if(!preg_match($regex,$value['fk_sub_task_id']))
					{
						$sub_task = "";
					}
					else
					{
						$sub_task = $value['fk_sub_task_id'];
					}

					$result[$key]['sub_task']	= $this->getSubtaskNames($sub_task);
					$task_code_array = $this->get_task_detils($value['task_code']);
					$project = $this->get_pro_detils($value['project_code']);
				}

				// echo "<pre>"; print_r($result);exit;

				foreach($result as $k => $v)
				{
					foreach($v as $k1 => $v1)
					{
						if($k1 == "AdditionalAttributes"){
							foreach($v1 as $k12 => $v12)
							{
								$result[$k][$k12] = $v12;
							}
						}
					}
				}	

				foreach($result as $key => $value)
				{
					if($value['support_type'] == 1){
						$sc = "Weekend Support";
					}
					if($value['support_type'] == 2){
						$sc = "Holiday Coverage";
					}
					if($value['status'] == 0){
						$v1 = "Pending";
					}
					if($value['status'] ==1){
						$v1 = "Approved";
					}
					if($value['status'] ==2){
						$v1 = "Rejected";
					}

					$result[$key]['Status'] = $v1; 

					unset($result[$key]['AdditionalAttributes'] );
					unset($result[$key]['utilization_id'] );
					unset($result[$key]['client_id'] );
					unset($result[$key]['status'] );
					unset($result[$key]['support_type'] );
					unset($result[$key]['fk_sub_task_id'] );
				}

				// debug($result);exit;

				foreach($result as $key => $value)
				{
					$return_array = array_keys($value);
					$return_array = str_replace("_"," ",$return_array);	
					$return_array = array_map('ucfirst', $return_array);				
				}
			}

			$result1 = array_merge(array(array_unique($return_array)), $result);

			return array($result1);
		}	


		function ApproveUtilization($input_values)
		{
			$empData = $this->input->cookie();
			$emp_id  = $empData['employee_id'];
			if(isset($input_values['UtilizationArray']))
			{
				$utilization = json_decode($input_values['UtilizationArray']);
				if(is_array($utilization))
					$utilization = implode(", ", $utilization);
				$sql_utilization ="UPDATE utilization SET status=".$input_values['value'].", approved_by = ".$emp_id.", approved_on ='".date('Y-m-d H:i:s')."'  WHERE utilization_id IN (".$utilization.")";
				$this->db->query($sql_utilization);
				if($input_values['value']==1)
				{
					$empData    = $this->input->cookie();
					$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization.') has been approved utilization Successfully.';
					$this->userLogs($logmsg);
				}
				if($input_values['value']==2)
				{
					$empData    = $this->input->cookie();
					$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization.') has been rejected utilization Successfully.';
					$this->userLogs($logmsg);
				}
				return  $this->db->affected_rows();
			}
		}


		function insightBillableApprove($input_values)
		{
			if(isset($input_values['UtilizationArray']))
			{
				$utilization = json_decode($input_values['UtilizationArray']);
				if(is_array($utilization))
					$utilization = implode(", ", $utilization);
				$sql_utilization ="UPDATE utilization SET status=".$input_values['value']." WHERE utilization_id IN (".$utilization.")";
				$this->db->query($sql_utilization);
				if($input_values['tab_type'] == 'weekend_list'){
					if($input_values['value']==3){
						$empData    = $this->input->cookie();
						$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization.') has been approved billable hours weekend utilization Successfully.';
						$this->userLogs($logmsg);
					}
					if($input_values['value']==1){
						$empData    = $this->input->cookie();
						$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization.') has been excluded billable hours weekend utilization Successfully.';
						$this->userLogs($logmsg);
					}
				}
				if($input_values['tab_type'] == 'holiday_list'){
					if($input_values['value']==4){
						$empData    = $this->input->cookie();
						$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization.') has been approved billable hours holiday utilization Successfully.';
						$this->userLogs($logmsg);
					}
					if($input_values['value']==1){
						$empData    = $this->input->cookie();
						$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$utilization.') has been excluded billable hours holiday utilization Successfully.';
						$this->userLogs($logmsg);
					}
				}

				return  $this->db->affected_rows();
			}
		}


		function getUtilAttributes($params)
		{
			/* added to display in order of Process Attributes*/
			$sqlattri = "SELECT `attributes_id` FROM `client_attributes` WHERE `client_id` =".$params['clientID'];
			$attrires = $this->db->query($sqlattri);
			$attrbutesIDs = array();
			foreach($attrires->result_array() as $attr => $attri)
			{
				$attrbutesIDs[] = $attri['attributes_id'];
			}
			/* added to display in order of Process Attributes*/

			/* Modified and added to test if only attributes list status  only active*/
			$sql = 'SELECT `attributes`.*, GROUP_CONCAT(`attributes_list`.`Value`) AS `List` 
			FROM `attributes` 
			JOIN `attributes_list` ON (`attributes`.`attributes_id` = `attributes_list`.`attributes_id`)
			WHERE attributes_list.Status = 1 AND `attributes`.`attributes_id` IN ("' . implode('", "', $attrbutesIDs) . '") 
			GROUP BY `attributes_list`.`attributes_id`';
			$res = $this->db->query($sql);
			$attribuites = $res->result_array();

			$sql_util = "Select attribuites from utilization where utilization_id = ".$params['utilID'];
			$res_util = $this->db->query($sql_util);
			$util =  $res_util->result_array();
			$attr = unserialize($util[0]['attribuites']);

			foreach($attribuites as $key => $val)
			{
				$value = "";
				if(is_array($attr) && count($attr)!=0)
				{
					if(array_key_exists($val['attributes_id'],$attr))
						$value = $attr[$val['attributes_id']];
				}
				$attribuites[$key]['Value'] = $value;
			}

			return $attribuites;
		}


		function utilization_insightCSV($params)
		{
			// debug($params);exit;
			$return_array = array();
			$clientID = (isset($params['clientId'])) ? $params['clientId'] : ""; 
			$output = false;
			$dashboard = (isset($params['dashboard'])) ? $params['dashboard'] : "";
			$EmployID = (isset($params['EmployID'])) ? $params['EmployID'] : "";
			$date = $start_date = $end_date = "";
			$empData = $this->input->cookie();
			$sess_EmpID = $empData['employee_id'];
			$grade = $empData['Grade'];

			if(isset($params['utilization_date']))
				$date = date('Y-m-d', strtotime($params['utilization_date']));
			if(isset($params['StartDate']))
				$start_date = date('Y-m-d', strtotime($params['StartDate']));
			if(isset($params['EndDate']))
				$end_date = date('Y-m-d', strtotime($params['EndDate']));
			if(isset($params['projectCode']))
				$project_code = $params['projectCode'];
			if(isset($params['insightType']))
				$tab_type = $params['insightType'];

			if(isset($params['wor_id']))
				$workOrder = $params['wor_id'];
			if(isset($params['wor_type_id']))
				$wor_type_id = $params['wor_type_id'];
			if(isset($params['project_type_id']))
				$project_type_id = $params['project_type_id'];
			if(isset($params['utilstatus']))
				$utilstatus = $params['utilstatus'];
			if(isset($params['supervisor']))
				$supervisor = $params['supervisor'];


			$sql_utilization = "SELECT DATE_FORMAT(utilization.task_date, '%d-%b-%Y') AS task_date ,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Employee Name`,wor.work_order_id, project_type, task_codes.task_code AS task_code  ,sub_tasks.task_name AS sub_task ,client.client_id,client_name,utilization.utilization_id ,process.name AS role_code,   utilization.request_name , utilization.client_hrs,utilization.theorem_hrs ,utilization.remarks ,utilization.utilization_id, utilization.fk_client_id,
			CASE
			WHEN utilization.status = 0 THEN 'Pending'
			WHEN utilization.status = 1 THEN 'Approved'
			WHEN utilization.status = 2 THEN 'Rejected'
			WHEN utilization.status = 3 THEN 'Weekend Billable'
			WHEN utilization.status = 4 THEN 'Client Holiday Billable'
			END AS status				
			FROM  `utilization`
			LEFT JOIN `employees` ON utilization.employee_id = `employees`.`employee_id`
			LEFT JOIN `wor` ON utilization.fk_wor_id = `wor`.`wor_id` 
			LEFT JOIN `client` ON utilization.fk_client_id = `client`.`client_id`
			LEFT JOIN task_codes  ON task_codes.task_id = utilization.fk_task_id
			LEFT JOIN process ON process.process_id = utilization.fk_role_id
			LEFT JOIN project_types ON project_types.project_type_id = utilization.fk_project_id
			LEFT JOIN sub_tasks ON sub_tasks.`sub_task_id` = `utilization`.`fk_sub_task_id`
			WHERE utilization.fk_client_id=".$clientID; 

			if($workOrder!="")
				$sql_utilization .=" AND utilization.fk_wor_id ='".$workOrder."'";				
			if($project_code!="")
				$sql_utilization .=" AND utilization.fk_role_id ='".$project_code."'";				
			if($wor_type_id!="")
				$sql_utilization .=" AND utilization.fk_type_id ='".$wor_type_id."'";				
			if($project_type_id!="")
				$sql_utilization .=" AND utilization.fk_project_id ='".$project_type_id."'";
		
			
			/*if($grade>2 && $grade<5)
			{
				if (is_array($this->getEmployeeList($sess_EmpID)))
					$employeeArray = implode(',', $this->getEmployeeList($sess_EmpID));
				$sql_utilization .= ' AND utilization.employee_id IN ('.$employeeArray.')';
			}*/
			else if($EmployID!=0)
				$sql_utilization .=" AND utilization.employee_id =". $EmployID;					
			if($date!="")
				$sql_utilization .=" AND utilization.task_date='".$date."'";
			if($start_date!="" && $end_date!="")
				$sql_utilization .=" AND utilization.task_date BETWEEN '".$start_date."' AND '".$end_date."'";
			if($tab_type == 'weekend_coverage')
				$sql_utilization .=" AND utilization.support_type=1 ";
			if($tab_type == 'holiday_coverage')
				$sql_utilization .=" AND utilization.support_type=2 ";

			if($utilstatus == '3')
				$sql_utilization .=" AND utilization.status IN(3,4) ";
			else if($utilstatus != 'All')
				$sql_utilization .=" AND utilization.status IN('".$utilstatus."') ";

			if($supervisor!='')
			{
				$emp_ids = $this->getEmpForSupervisor($supervisor);
				$sql_utilization .= " AND utilization.employee_id IN ($emp_ids)";
			}

			// echo $sql_utilization;exit;
			$query = $this->db->query($sql_utilization);		
			$result = $query->result_array();

			$client_id_attr = array();
			$utilization_id_attr = array();

			foreach($result as $key => $value)
			{
				$client_id_attr[] = $value['client_id'];
				$utilization_id_attr[] = $value['utilization_id'];
			}
	
			$result1 = implode(',',$client_id_attr);
			$result2 = implode(',',$utilization_id_attr);
			$client_attrib = $this->Client_Attriutes($result1, $result2,'exportcsv');

			//$client_attrib = $this->Client_Attriutes($clientID,'exportcsv');

			foreach($result as $key => $value)
			{
				$AdditionalAttributes = $this->GetAttributesValues($value['client_id'] , $value['utilization_id'],'export');
				/*$AdditionalAttributes = $this->GetAttributesValues($value['client_id'] , $value['utilization_id'],'export');*/
				$result[$key]['AdditionalAttributes'] = array_merge($client_attrib,$AdditionalAttributes);
			}

			foreach($result as $k => $v)
			{
				foreach($v as $k1 => $v1)
				{
					if($k1 == "AdditionalAttributes"){
						foreach($v1 as $k12 => $v12)
						{
							$result[$k][$k12] = $v12;
						}					
					}
				}							
			}
			
			// debug($result);exit;							
			foreach($result as $key => $value)
			{
				unset($result[$key]['AdditionalAttributes'] );
				unset($result[$key]['utilization_id'] );
				unset($result[$key]['fk_client_id'] );
				unset($result[$key]['client_id'] );
			}



			foreach($result as $key => $value)
			{
				foreach($value as $key1 => $value1)
				{
					if($key1=="client_hrs")
					{
						$ret = "Client Hours";
						$return_array[] = ucwords($ret);
					}
					else if($key1=="theorem_hrs")
					{
						$ret = "Theorem Hours";
						$return_array[] = ucwords($ret);
					}
					else
					{
						$ret = str_replace("_"," ",$key1);
						$return_array[] = ucwords($ret);
					}
				}
			}
			$return_array = array_unique($return_array);
			// $return_array = array_splice($return_array, 5);  // additional column
			$final_result = array_merge(array($return_array), $result);
			// debug($final_result);exit;
			return array($final_result);
		}


		function deleteUtlization($data)
		{
			$deldata = array();
			$deldata['utilization_id'] = $data['utilization_id'];
			$retval= $this->delete($deldata, 'utilization');		
			$this->deleteAttributesValues($data['utilization_id']);

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Utilization ID:'.$data['utilization_id'].') has been deleted utilization Successfully.';
			$this->userLogs($logmsg);

			return $retval;
		}

		/* Query to get role codes of a client*/
		function getRoleCode($params)
		{
			$clientId = $params['clientId'];
			
			$wor_id = 1;
			$filter	= json_decode($this->input->get('filter'),true);
			$query	= ($this->input->get('query')!="")?$this->input->get('query'):"";
			$start = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$end = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;	
			$hasNoLimit = $this->input->get('hasNoLimit');	
			$options = array();
			$options['filter']	= $filter;	
			if($options['filter'][0]['value']==1)
			{
				$filterWhere = '1=1';
			}
			else
			{
				if(isset($options['filter']) && $options['filter'] != '') 
				{
					foreach ($options['filter'] as $filterArray) 
					{
						$filterFieldExp = explode(',', $filterArray['property']);
						foreach($filterFieldExp as $filterField) 
						{
							if($filterField != '' && $filterField!="project_code") 
							{
								$filterWhere .= ($filterWhere != '')?' OR ':'';
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}

				}
			}

			$sql = 'SELECT p.`process_id` AS si_no, p.`name` AS project_code, p.`fk_client_id`, `client_name`, model, shift_code as shift_name, `support_type` AS support_coverage,  jr.role_type AS responsibilities
			FROM `process` p
			LEFT JOIN client c ON c.client_id = p.`fk_client_id`
			LEFT JOIN shift s ON s.`shift_id` = p.shift_id
			LEFT JOIN `support_coverage` sc ON sc.`support_id` = p.support_id
			LEFT JOIN `role_types` jr ON jr.`role_type_id` = p.role_type_id';
			$where =' WHERE p.`fk_client_id` ='. $clientId;
			
			if($filterWhere !="")
			{
				$where .= " AND (".$filterWhere.")";    			
			}	
			if($query != "")
			{
				$where .= " AND p.name LIKE '%".$query."%'";    			
			}	
			if($wor_id != "")
			{
				$where .= " AND p.process_id IN(SELECT process_id FROM fte_model WHERE fk_wor_id ='$wor_id' AND anticipated_date <= CURDATE() GROUP BY shift, support_coverage, responsibilities
				HAVING SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte END)>0
				UNION
				SELECT process_id FROM hourly_model WHERE fk_wor_id ='$wor_id' AND anticipated_date <= CURDATE() GROUP BY shift, support_coverage, responsibilities
				HAVING SUM(CASE change_type WHEN 1 THEN min_hours+max_hours  WHEN 2 THEN (min_hours+max_hours) END)>0
				UNION
				SELECT process_id FROM volume_based_model WHERE fk_wor_id ='$wor_id' AND anticipated_date <= CURDATE() GROUP BY shift, support_coverage, responsibilities
				HAVING SUM(CASE change_type WHEN 1 THEN min_volume+max_volume  WHEN 2 THEN -(min_volume+max_volume) END)>0
				UNION
				SELECT process_id FROM project_model WHERE fk_wor_id ='$wor_id')";    			
			}
			$query = $sql.$where;
			
			$totQuery = $this->db->query($query);

			$totCount = $totQuery->num_rows();
			if($hasNoLimit=="")
			{
				$query .= " LIMIT $start, $end";
			}
			$totQuery = $this->db->query($query);
			$data = $totQuery->result_array();

			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);

			return $results;
		}

		function GetModelProcess_ids($clientId,$wor_id)
		{

			$sql = "SELECT  `fte_id`,`hourly_based_id`,`volume_base_id`,`project_model_id` FROM `wor_cor_transition`  WHERE `fk_wor_id` = '$wor_id' AND client_id= '$clientId' AND LEVEL = (SELECT MAX(LEVEL) FROM wor_cor_transition WHERE`fk_wor_id` = '$wor_id')  "; 	

			$query    = $this->db->query($sql);
			$result = $query->result_array();

			foreach($result as $key=>$value)
			{
				if($value['fte_id'] !=''){
					$process_id[$key]['fte_id'] = $this->GetProcess_ids($value['fte_id'],'fte_model','fte_id');
				}
				if($value['hourly_based_id'] !=''){
					$process_id[$key]['hourly_based_id'] = $this->GetProcess_ids($value['hourly_based_id'],'hourly_model','hourly_id');
				}
				if($value['volume_base_id'] !=''){
					$process_id[$key]['volume_base_id'] = $this->GetProcess_ids($value['volume_base_id'],'volume_based_model','vb_id');
				}
				if($value['project_model_id'] !=''){
					$process_id[$key]['project_model_id'] = $this->GetProcess_ids($value['project_model_id'],'project_model','project_id');
				}
			}

			$res= array_values(array_diff($process_id[0],array("","")));

			$output = implode(',', $res);

			if($output != '' || $output != null){
				return $output;
			}else{
				return 'null';
			} 
		}

		function GetProcess_ids($id,$table,$primary_key)
		{
			$sql = "SELECT GROUP_CONCAT(process_id) As process_id FROM $table WHERE $primary_key IN ($id)  "; 	

			$query  = $this->db->query($sql);
			$row = $query->row();
			if (isset($row))
			{		
				return $row->process_id;
			}
			else 
			{
				return  '';
			}

		}

		function get_project_type_id($name)
		{
			$sql = "SELECT project_type_id from project_types_test where project_types like '%".$name."%'";
			$result = $this->db->query($sql)->result_array();
			return $result[0]['project_type_id'];
		}

		function getTaskCode($attr)
		{
			$project_type = $attr['project_type'];
			$project_type_id = $this->get_project_type_id($project_type);
			$filter	= $this->input->get('query')?$this->input->get('query'):"";
			$filter2 = json_decode($this->input->get('filter'),true);

			$options = array();
			if($this->input->get('hasNoLimit') != '1')
			{
				$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
			}

			$where = '';
			if($filter !="")
			{
				$where .= " WHERE task_name LIKE '%".$filter."%' OR task_code LIKE '%".$filter."%'";    			
			}
			else if($filter2 !="")
			{
				$options['filter']	= $filter2;
				foreach ($options['filter'] as $filterArray) 
				{
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) 
					{
						if($filterField != '') 
						{
							//$where .= ($where != '')?' OR ':' WHERE ';
							$where .= $filterField." LIKE '%".$filterArray['value']."%'";
						}
					}
				}
			}
			if($project_type_id != '')
			{
				$where .= ($where != '')?' OR ':' WHERE project_type_id_test = '.$project_type_id.' and status = 1';
			}
			$select = 'SELECT * FROM task_codes';
			$query = $select . $where;
			//echo $query;
			$totQuery = $this->db->query($query);
			$totCount = $totQuery->num_rows();

			if (isset($options['limit']) && isset($options['offset'])) 
			{
				$query .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
			} 
			else if (isset($options['limit'])) 
			{
				$query .= " LIMIT " . $options['limit'];
			}

			$totQuery = $this->db->query($query);
			$data = $totQuery->result_array();

			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);

			return $results;
		}

		function get_task_id($name)
		{
			$sql = "SELECT task_id from task_codes where task_name like '%".$name."%' and status = 1";
			$result = $this->db->query($sql)->result_array();
			return $result[0]['task_id'];
		}

		function getSubTask()
		{
			$filter	= $this->input->get('query')?$this->input->get('query'):"";
			$task_name = $this->input->get('task_name') ? $this->input->get('task_name') : "";

			if($task_name =="")
			{
				return '';
			}
			else
			{
				$task_id = $this->get_task_id($task_name);
			}

			if ($this->input->get('hasNoLimit') != '1') 
			{
				$start = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$end = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
			}

			$where = ' WHERE sub_tasks.status = 1 ';

			if($task_id !="")
			{
				$where .= " AND task_subtask_map.task_id = ".$task_id." AND task_subtask_map.status = 1";    			
			}
			if($filter !="")
			{
				$where .= " AND task_name LIKE '%".$filter."%'";    			
			}

			$select = 'SELECT * from sub_tasks JOIN task_subtask_map ON sub_tasks.sub_task_id = task_subtask_map.subtask_id ';

			$query = $select . $where;
			$query .= " ORDER BY task_name ASC";
			$totQuery = $this->db->query($query);
			$totCount = $totQuery->num_rows();

			if (isset($options['limit']) && isset($options['offset'])) 
			{
				$query .= " limit " . $options['offset'] . " , " . $options['limit'];
			} 
			else if (isset($options['limit'])) 
			{
				$query .= " limit " . $options['limit'];
			}
			//echo $query;
			$totQuery = $this->db->query($query);
			$data = $totQuery->result_array();
			// debug($data);exit;

			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);

			return $results;
		}

		function GetRequestRegion()
		{
			$filter	= $this->input->get('query')?$this->input->get('query'):"";

			$where = '';
			if($filter !="")
			{
				$where .= " WHERE reg_name LIKE '%".$filter."%'";    			
			}

			$select = 'SELECT * from request_region';
			$query = $select . $where;
			$totQuery = $this->db->query($query);
			$totCount = $totQuery->num_rows();

			$totQuery = $this->db->query($query);
			$data = $totQuery->result_array();

			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);

			return $results;
		}

		function GetProjectIdFromCode($project_code, $ClientID='')
		{
			$sql = "SELECT process_id FROM process WHERE name =  '$project_code'";
			if($ClientID)
			{
				$sql .= " AND fk_client_id = '$ClientID'";
			}
			$query  = $this->db->query($sql);
			$row = $query->row();
			if (isset($row))
			{		
				return $row->process_id;
			}	
		}

		function get_pro_detils($project_code)
		{
			$sql = "SELECT pr.name AS project_code, cl.client_name,pr.model,st.shift_code,jr.role_type AS job_responsibility, sc.support_type 
			FROM process AS pr 
			JOIN client `cl` ON pr.fk_client_id = cl.client_id 
			JOIN shift `st` ON pr.shift_id = st.shift_id 
			JOIN role_types AS jr ON jr.role_type_id = pr.role_type_id 
			JOIN `support_coverage` `sc` ON pr.support_id = sc.support_id 
			WHERE pr.name =  '$project_code'  
			LIMIT 1";
			$query = $this->db->query($sql);
			$row = $query->row();

			return $row ;
		}

		function get_task_detils($task_code)
		{
			$query = $this->db->query("SELECT task_code,task_category,task_name,request_type,complexity,priority FROM task_codes WHERE  task_code= '$task_code'  LIMIT 1");
			$row = $query->row();

			return $row ;
		}

		function WeekendSupport($day,$support)
		{
			$sql = "SELECT weekend_support FROM support_coverage WHERE  short_code= '$support' ";
			$query  = $this->db->query($sql);
			$row = $query->row();
			if (isset($row))
			{
				$res = explode(",",$row->weekend_support);
				foreach ($res as $val) 
				{
					if (strpos($day, $val) !== FALSE) { 
						return 1;
					}
				}
				return 0;
			}
		}


		function HolidayCoverage($client_id,$project_id,$date)
		{
			$sql = "SELECT holidays.date FROM `client_holidays` JOIN `holidays` ON holidays.`holiday_id` = `client_holidays`.`holiday_id` WHERE client_id = '$client_id' AND process_id = '$project_id' AND DATE = '$date'";
			$query  = $this->db->query($sql);
			$row = $query->row();
			if (isset($row))
			{		
				return 2;
			}
			else 
			{
				return  0;
			}	

		}


		function getAttributes()
		{
			$params = $this->input->get();
			// debug($params);exit;
			$client_id = $params['client_id'];
			$wor_id = 1;
			$role_id = $params['role_id'];
			
			$sql = "SELECT client_attributes.attributes_id,mandatory,label_name,field_name,field_type,`new_attributes`.values  
			FROM `client_attributes` 
			JOIN `new_attributes` ON new_attributes.attributes_id = client_attributes.attributes_id  
			WHERE client_id = '$client_id' AND wor_id='$wor_id'";
			if($role_id!="")
			{
				$role_attr_sql = "SELECT role_type_id FROM process WHERE process_id='$role_id'";
				$role_attr_qry = $this->db->query($role_attr_sql);
				$role_attr_result = $role_attr_qry->row_array();
				$sql .= " AND fk_role=".$role_attr_result['role_type_id'];
			}

			//echo $sql;exit;
			
			$query = $this->db->query($sql);
			$totCount = $query->num_rows();
			$data = $query->result_array(); 

			$results = array_merge(
				array('totalCount' => $totCount), 
				array('data' => $data)
			);
			
			// debug($results);exit;
			return $results ;
		}

		function getClientAddAttr($params)
		{
			$result = array();
			$loop = 0;
			$wor_id = $this->input->get('wor_id');
			$client_id= $this->input->get('client_id');

			$sql = "SELECT attributes_id,label_name AS name, description, '' AS selected, '' AS mandatory 
			FROM new_attributes WHERE status=1 ORDER BY name ASC";
			$query = $this->db->query($sql);
			$resultAttr = $query->result_array();
			
			foreach($resultAttr as $key => $value)
			{
				$sql = "SELECT mandatory FROM client_attributes WHERE client_id='$client_id' AND wor_id='$wor_id' AND attributes_id='".$value['attributes_id']."'";
				if($this->input->get('project_type_id'))
				{
					$project_type_id = $this->input->get('project_type_id');
					$sql .= " AND fk_project = '$project_type_id'";
				}
				if($this->input->get('role_id'))
				{
					$role_id= $this->input->get('role_id');
					$sql .= " AND fk_role = '$role_id'";
				}
				$query = $this->db->query($sql);
				$resultClntAttr = $query->row_array();
				
				$result[$loop]['attributes_id'] = $value['attributes_id'];
				$result[$loop]['name'] 			= $value['name'];
				$result[$loop]['description'] 	= $value['description'];
				$result[$loop]['selected'] 		= (count($resultClntAttr)>0)?true:false;
				$result[$loop]['mandatory'] 	= ($resultClntAttr['mandatory']>0)?true:false;
				
				$loop++;
			}

			$sorted_array = array();
			foreach ($result as $key => $row)
			{
				$visible_sorted_array[$key] = $row['selected'];
				$name_sorted_array[$key] = $row['name'];
			}
			array_multisort($visible_sorted_array, SORT_DESC, $name_sorted_array, SORT_ASC, $result);
			// debug($result);exit;
			$data['totalCount'] = count($result);
			$data['data'] = $result;

			return $data;
		}

		function getClientAddAttr_old($params)
		{
			error_reporting(E_ALL);
			$wor_id = $this->input->get('wor_id');
			$client_id= $this->input->get('client_id');
			
			$new_attr_sql = "SELECT attributes_id,label_name,description FROM new_attributes";
			$new_attr_qry = $this->db->query($new_attr_sql);
			$new_attr_result = $new_attr_qry->result_array();

			// debug($new_attr_result);exit;

			$client_attr_sql = "SELECT attributes_id,mandatory FROM client_attributes";
			$client_attr_sql .= " WHERE client_id='$client_id' AND wor_id='$wor_id'";

			if($this->input->get('project_type_id'))
			{
				$project_type_id = $this->input->get('project_type_id');
				$client_attr_sql .= " AND fk_project = '$project_type_id'";
			}

			if($this->input->get('role_id'))
			{
				$role_id= $this->input->get('role_id');
				$client_attr_sql .= " AND fk_role = '$role_id'";
			}

			// echo $sql;exit;

			$client_attr_query = $this->db->query($client_attr_sql);
			$client_attr_result = $client_attr_query->result_array();

			// debug($client_attr_result);exit;

			foreach ($new_attr_result as $na_key => $na_value) {
				$na_attribute_id = $na_value['attributes_id'];
				$new_attr_result[$na_key]['checked'] = 0;
				$new_attr_result[$na_key]['mandatory'] = 0;
				foreach ($client_attr_result as $ca_key => $ca_value) {
					if($ca_value['attributes_id'] ==$na_value['attributes_id'])
					{
						$new_attr_result[$na_key]['checked'] = 1;
					}

					if($ca_value['attributes_id'] ==$na_value['attributes_id'])
					{
						$new_attr_result[$na_key]['mandatory'] = $ca_value['mandatory'];
					}
				}
			}

			// debug($new_attr_result);exit;

			$data['totalCount'] = count($new_attr_result);
			$data['rows'] = $new_attr_result;

			// debug($data);exit;
			return $data;
		}

		function ImportTasks($inputValues){

			error_reporting(E_ALL);

			$empData = $this->input->cookie();

			if (!empty($_FILES['UploadXls']['name'])) {

				$uploadPath = $this->config->item('REPOSITORY_PATH').'resources/uploads/';

				$uploadResp = $this->upload_files($_FILES, $uploadPath);
				$uploadData = $this->upload->data();

				$csvObj = new CSVReader();
				$data = $csvObj->parse_file($uploadPath.$uploadData['file_name']);

				$retVal = $support = $project_id = "";
				// debug($data);exit;
				foreach ($data as $key => $val)
				{							
				/*$wor_id = $this->getWorIdByName($val['work_order_id']);
				$employee_id = $this->getEmpIdByCompanyEmployId($val['employee_id']);*/

				if(!isset($val['task_date']) || 
					!isset($val['wor_type_id']) || 
					!isset($val['project_type_id']) || 
					!isset($val['role_code']) || 
					!isset($val['task_code']) || 
					!isset($val['sub_task']) || 
					!isset($val['request_name']) || 
					!isset($val['client_hours']) || 
					!isset($val['theorem_hours']) || 
					!isset($val['remarks']))
				{
					return 0;
				}

				if($val['task_date']=='' || 
					$val['wor_type_id']=='' || 
					$val['project_type_id']=='' || 
					$val['role_code']=='' || 
					$val['task_code']=='' ||
					$val['request_name']=='' || 
					$val['client_hours']=='' || 
					$val['theorem_hours']=='')
				{
					return -1;
				}
				
				$wor_type_id = $this->getWorTypeId_util($val['wor_type_id']);
				$project_type_id = $this->getProjectTypeId_util($val['project_type_id']);
				$task_id = $this->getTaskidByCode($val['task_code']);
				$sub_task = $this->getsubTaskidByCode($val['sub_task']);
				$project_id = $this->getProjectidByCode($val['role_code'],$inputValues['ClientID']);

				// echo "$wor_type_id, $project_type_id, $task_id, $project_id";
				
				if(!$wor_type_id || !$project_type_id || !$task_id || !$project_id)
				{
					return -4;
				}

				$task_date = $this->validateDateFormat($val['task_date']);

				if(!$task_date)
				{
					return -2;
				}

				$client_hours = $this->validateTimeFormat($val['client_hours']);
				$theorem_hours = $this->validateTimeFormat($val['theorem_hours']);

				if(!$client_hours || !$theorem_hours)
				{
					return -3;
				}

				$client_attributes = $this->getClientAttributes($inputValues['ClientID']);

				$timestamp =  strtotime($val['task_date']);
				$day = date('D', $timestamp);
				$weekend_support = $this->WeekendSupport($day,$support);
				if($weekend_support == 0)
				{
					$project_id = $this->GetProjectIdFromCode($val['role_code'],$inputValues['ClientID']);
					$support_type = $this->HolidayCoverage($inputValues['ClientID'], $project_id, date('Y-m-d', strtotime($val['task_date'])));
				}
				else
				{
					$support_type = $weekend_support;
				}
				
				date_default_timezone_set('Asia/Kolkata');

				$final_data = array(
					'employee_id' 			=> $empData['employee_id'],
					'task_date' 			=> date('Y-m-d', strtotime($val['task_date'])),
					'fk_client_id' 			=> $inputValues['ClientID'],
					'fk_wor_id' 			=> $inputValues['Work_id'],
					'fk_type_id' 			=> $wor_type_id,
					'fk_project_id' 		=> $project_type_id,
					'fk_role_id' 			=> $project_id,
					'fk_task_id' 			=> $task_id,
					'fk_sub_task_id' 		=> $sub_task,
					'request_name' 			=> $val['request_name'],
					'client_hrs' 			=> $val['client_hours'],
					'theorem_hrs' 			=> $val['theorem_hours'],
					'support_type' 			=> $support_type,
					'remarks' 				=> $val['remarks'],
					'additional_attributes' => '',
					'created_by' 			=> $empData['employee_id'],
				);

				$retVal = $this->insert($final_data, 'utilization');

				$util_insert_id = $this->db->insert_id();

				$this->insertClientAttributes($client_attributes, $util_insert_id, $val);

			}
			unlink($uploadPath.$uploadData['file_name']);
			
			return $retVal;
		}
	}

	function getWorTypeId_util($wor_type)
	{
		$sql = "SELECT wor_type_id FROM wor_types WHERE wor_type ='$wor_type'";
		$query = $this->db->query($sql);

		if($query->num_rows()>0)
		{
			$row = $query->row();
			return $row->wor_type_id;
		}
		else
		{
			return false;
		}
		
	}

	function getProjectTypeId_util($project_type)
	{
		$sql = "SELECT project_type_id FROM project_types WHERE project_type ='$project_type'";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$row = $query->row();
			return $row->project_type_id;
		}
		else
		{
			return false;
		}
		
	}

	function get_client_id($name)
	{
		$sql = "SELECT client_id from client where client_name like '%".$name."%'";
		$result = $this->db->query($sql)->result_array();
		return $result[0]['client_id'];
	}

	function get_wor_id($id)
	{
		$str = $id;
		$wor = explode(" ",$str);
		$sql = "SELECT wor_id from wor where work_order_id = '".$wor[0]."'";
		$result = $this->db->query($sql)->result_array();
		return $result[0]['wor_id'];
	}

	function project_types($attr)
	{
		/*$id = $this->get_client_id($attr['client_id']);
		$wor_id = $this->get_wor_id($attr['wor_id']);
		//$sql = "SELECT project_types from project_types_test where client_id = '".$id."'";
		$sql = "SELECT distinct(pm.project_name), pm.fk_project_type_id from project_model pm
		inner join employee_client ec on pm.fk_project_type_id = ec.project_type_id
		where ec.client_id = '".$id."' and pm.fk_wor_id = '".$wor_id."'";
		/*$sql = "SELECT distinct(pt.project_type) from rhythm_project_type pt
		inner join employee_client ec on pt.project_type_id = ec.project_type_id
		where ec.client_id = '".$id."' and ec.wor_id = '".$wor_id."'";*/
		/*$result = $this->db->query($sql)->result_array();
		return $result;*/
		$empData = $this->input->cookie();
		$sess_EmpID = $empData['employee_id'];
		$client_id = $this->get_client_id($attr['client_id']);
		$wor_id = $this->get_wor_id($attr['wor_id']);
		/*$fte_sql = "SELECT DISTINCT FM.project_name,PT.project_type_id FROM wor W
		JOIN fte_model FM ON FM.fk_wor_id = W.wor_id
		JOIN rhythm_project_type PT ON PT.project_type_id = FM.fk_project_type_id
		inner join employee_client ec on ec.project_type_id = PT.project_typ_id
		WHERE client='$client_id'" and ec.employee_id = 2707*/

		$fte_sql = "SELECT DISTINCT FM.project_name,FM.fk_project_type_id from fte_model FM
		inner join employee_client ec on FM.fte_id = ec.project_model_id
    	where ec.client_id ='$client_id' and ec.employee_id = '".$empData['employee_id']."' and FM.fk_wor_id = '$wor_id' and ec.project_model_type = 'fte_model'";

		$hourly_sql = "SELECT DISTINCT HM.project_name,HM.fk_project_type_id from hourly_model HM
		inner join employee_client ec on HM.hourly_id = ec.project_model_id
    	where ec.client_id ='$client_id' and ec.employee_id = '".$empData['employee_id']."' and HM.fk_wor_id = '$wor_id' and ec.project_model_type = 'hourly_model'";

		$project_sql = "SELECT DISTINCT PM.project_name,PM.fk_project_type_id from project_model PM
		inner join employee_client ec on PM.project_id = ec.project_model_id
    	where ec.client_id ='$client_id' and ec.employee_id = '".$empData['employee_id']."' and PM.fk_wor_id = '$wor_id' and ec.project_model_type = 'project_model'";

		$volume_based_sql = "SELECT DISTINCT VM.project_name,VM.fk_project_type_id from volume_based_model VM
		inner join employee_client ec on VM.vb_id = ec.project_model_id
    	where ec.client_id ='$client_id' and ec.employee_id = '".$empData['employee_id']."' and VM.fk_wor_id = '$wor_id' and ec.project_model_type = 'vb_model'";

		$sql = "";
		
		/*($wor_id !='')
		{
			$sql = " AND W.wor_id='$wor_id'";x
		}*/

		$fte_query = $this->db->query($fte_sql.$sql);
		$hourly_query = $this->db->query($hourly_sql.$sql);
		$project_query = $this->db->query($project_sql.$sql);
		$volume_based_query = $this->db->query($volume_based_sql.$sql);

		$fte_result = $fte_query->result_array();
		$hourly_result = $hourly_query->result_array();
		$project_result = $project_query->result_array();
		$volume_based_result = $volume_based_query->result_array();

		/*debug($fte_result);
		debug($hourly_result);
		debug($project_result);
		debug($volume_based_result);exit;*/
		$temp_res = array();
		if(!empty($fte_result))
		{
			$temp_res = array_merge($temp_res,$fte_result);
		}
		if(!empty($hourly_result))
		{
			$temp_res = array_merge($temp_res,$hourly_result);
		}
		if(!empty($project_result))
		{
			$temp_res = array_merge($temp_res,$project_result);
		}
		if(!empty($volume_based_result))
		{
			$temp_res = array_merge($temp_res,$volume_based_result);
		}
		$sorted_array = array();
		// array_unique($temp_res, SORT_REGULAR);
		$temp_res = array_map("unserialize", array_unique(array_map("serialize", $temp_res)));
		// debug($temp_res);exit;
		foreach ($temp_res as $key => $row)
		{
			$sorted_array[$key] = $row['project_name'];
		}
		array_multisort($sorted_array, SORT_ASC, $temp_res);
		$result['rows'] = $temp_res;
		return $result;

	}

	function getWorIdByName($work_order_id)
	{
		$query = $this->db->query(" SELECT wor_id FROM wor WHERE work_order_id = '$work_order_id' ");
		$row = $query->row();
		return $row->wor_id;
	}

	function getVolumeModel($attr)
	{
		$wor_id = $this->get_wor_id($attr['wor_id']);
		$project_type_id = $this->get_project_type_id($attr['project_type']);
		$sql = "SELECT * FROM volume_based_model WHERE fk_wor_id='$wor_id' AND project_type_id_test='$project_type_id' ";

		//echo $sql;exit;

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array(); 

		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		
		// debug($results);exit;
		return $results ;
	}

	function getEmpIdByCompanyEmployId($employee_id)
	{
		$query = $this->db->query("SELECT employee_id FROM employees WHERE company_employ_id = ".$employee_id);
		$row = $query->row();
		return $row->employee_id;
	}

	function getProjectidByCode($project_code,$ClientID='')
	{
		$query = $this->db->query("SELECT process_id FROM process WHERE name = '$project_code' AND fk_client_id=$ClientID");
		if($query->num_rows()>0)
		{
			$row = $query->row();
			return $row->process_id;
		}
		else
		{
			return false;
		}
	}

	function getTaskidByCode($task_code)
	{
		$query = $this->db->query("SELECT task_id FROM task_codes WHERE task_code = '$task_code' ");
		if($query->num_rows()>0)
		{
			$row = $query->row();
			return $row->task_id;
		}
		else
		{
			return false;
		}

	}

	function getRegionidByCode($request_region)
	{
		$query = $this->db->query("SELECT reg_id FROM request_region WHERE reg_name = '$request_region' ");
		if($query->num_rows()>0)
		{
			$row = $query->row();
			return $row->reg_id;
		}
		else
		{
			return false;
		}

	}

	function getsubTaskidByCode($sub_task)
	{	
		$query = $this->db->query("SELECT sub_task_id FROM sub_tasks WHERE task_name IN ('".str_replace(",", "','", $sub_task)."')");

		if($query->num_rows()>0)
		{
			$oneDimensionalArray = array_map('current', $query->result_array());

			if (is_array($oneDimensionalArray)) 
			{			
				return implode(",",$oneDimensionalArray);
			}
		}
		else
		{
			return false;
		}
	}

	function getClientAttributes($clientId)
	{
		$sqlattri = "SELECT client_attributes.attributes_id,label_name,field_name,field_type,`new_attributes`.values  
		FROM `client_attributes` 
		JOIN `new_attributes` ON new_attributes.attributes_id = client_attributes.attributes_id  
		WHERE visible=1 AND client_id = ".$clientId;
		$queryatri = $this->db->query($sqlattri);

		if($queryatri->num_rows() > 0)
		{
			return $queryatri->result_array(); 
		}
		else
		{
			return 0;
		}
	}

	function insertClientAttributes($client_attributes, $util_insert_id, $val)
	{
		if ($client_attributes <> 0) 
		{
			
			foreach ($client_attributes as $client_attribute) 
			{						
				$dataset = array(
					'utilization_id'		=> $util_insert_id,
					'attributes_id' 	    => $client_attribute['attributes_id'],
					'attributes_value' 	    => $val[$client_attribute['field_name']],
				);

				$this->insert($dataset, 'utilization_client_attributes'); 
			}		
		}
	}

	function validateDateFormat($date)
	{
		if(strpos($date,'-')!==false OR strpos($date,'/')!==false) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function validateTimeFormat($time)
	{
		// return preg_match("/^(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)/", $time);
		return preg_match('/^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$/', $time);
	}

	function getUtilRoleCode()
	{
		// debug($this->input->get());exit;
		$client_id = $this->input->get('comboClient');

		$sql = "SELECT DISTINCT P.name AS project_code, P.process_id 
		FROM utilization U
		LEFT JOIN process P ON U.fk_role_id = P.process_id
		WHERE U.fk_client_id = '$client_id'";

		if($this->input->get('wor_id'))
		{
			$wor_id = $this->input->get('wor_id');
			$sql .= " AND U.fk_wor_id = '$wor_id'";
		}

		if($this->input->get('wor_type_id'))
		{
			$wor_type_id = $this->input->get('wor_type_id');
			$sql .= " AND U.fk_type_id = '$wor_type_id'";
		}

		if($this->input->get('project_type_id'))
		{
			$project_type_id = $this->input->get('project_type_id');
			$sql .= " AND U.fk_project_id = '$project_type_id'";
		}

		// echo $sql;exit;

		$query = $this->db->query($sql);

		$result = $query->result_array();

		// debug($result);exit;
		$data['data'] = $result;
		$data['totalCount'] = count($result);
		return $data;
	}

	function getWorTypeId($project_type_id)
	{
		// echo $project_type_id;exit;
		$sql = "SELECT DISTINCT wor_type_id 
		FROM wor_project_role_mapping
		WHERE project_type_id='$project_type_id'";
		$qry = $this->db->query($sql);

		$result = $qry->result_array();
		// debug($result);exit;

		$wor_type_id = $result[0]['wor_type_id'];
		// echo $wor_type_id;exit;
		return $wor_type_id;
	}

	private function getEmpForSupervisor($supervisor)
	{
		$sql = "SELECT GROUP_CONCAT(employee_id) AS emp_ids
		FROM employees
		WHERE primary_lead_id = '$supervisor'
		GROUP BY primary_lead_id";
		$result = $this->db->query($sql)->result_array();
		// debug($result);exit;
		return $result[0]['emp_ids'];
	}

}
?>