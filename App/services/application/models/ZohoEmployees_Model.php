<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ZohoEmployees_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_zohoemployees()
	{
		$start_date = "2020-01-01";
		$end_date = "2020-01-15";
		$sql = "SELECT employees.company_employ_id,employees.first_name,employees.last_name,s.shift_code, 
GROUP_CONCAT(v.name) AS vertical_name,employees.email,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `full_name`, CONCAT(m.first_name, ' ', m.last_name) AS lead_name, employees.is_onshore, d.name AS Designation_name FROM employees LEFT JOIN designation d ON d.designation_id = employees.designation_id LEFT JOIN employee_vertical ev ON ev.employee_id = employees.employee_id 
LEFT JOIN verticals v ON v.vertical_id = ev.vertical_id LEFT JOIN qualifications q ON q.qualification_id = employees.qualification_id 
LEFT JOIN location loc ON loc.location_id = employees.location_id LEFT JOIN employees m ON employees.primary_lead_id = m.employee_id 
LEFT JOIN shift s ON s.shift_id = employees.shift_id WHERE 1=1 AND employees.status!=6 AND ((employees.created_date>='2020-01-01' 
AND employees.created_date<='2020-01-15') || (employees.updated_date>='2020-01-01' AND employees.updated_date<='2020-01-15')) GROUP BY employees.employee_id 
ORDER BY first_name ASC";

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge( 
			array('data' => $data)
		);
		//echo $this->db->last_query(); exit;
		echo "<PRE>";print_r(json_encode($results));exit;
		return $results;
	}	

			function addEmployee($data = '')
			{
				$addEmployee = json_decode(trim($data), true);

        		//check vertcal start
				$assigned_vertical = $addEmployee['assigned_vertical'];
				$ops_constant = json_decode(OPS_VERT);
				//check vertical end
				$grade_value = $addEmployee['designation_id'];
				$grades = $this->getGrade($grade_value);
				$grades = $grades[0]['grades'];

				if($addEmployee['mobile']==$addEmployee['alternate_number'])
					return -3;

				if($addEmployee['employee_id']=="")
				{
					if ($this->_isRecordExist("", $addEmployee['company_employ_id'], $addEmployee['email']) == 0) 
					{	
						unset($addEmployee['deligate_to']);
						unset($addEmployee['resign_flag']);
						unset($addEmployee['resigned_notes']);
						unset($addEmployee['resigned_date']);

						$addEmployee['doj']  = date("Y-m-d", strtotime($addEmployee['doj']));
						$addEmployee['dob']  = date("Y-m-d", strtotime($addEmployee['dob']));
						unset($addEmployee['relieve_flag']);
						$addEmployee['relieve_date'] = $addEmployee['relieve_date'] ? $addEmployee['relieve_date'] : '0000-00-00';
						if (!$addEmployee['company_employ_id'])
							unset($addEmployee['company_employ_id']);
						$empData     = $this->input->cookie();
						$this->EmpId = $empData['employee_id'];

						/* Added code for Chatham Employees */
						$chatnamflag = false;
						if($addEmployee['location_id'] == 4 )
						{
							// $addEmployee['assigned_vertical'] = NULL;
							$chatnamflag = true;
							//$addEmployee['primary_lead_id'] = 1297;
						}

						$addEmployee['created_by']   = $this->EmpId;
						$addEmployee['created_on'] = date("Y-m-d H:i:s");
						$addEmployee['status'] = 3;
						$addEmployee['training_needed']  = 1;

						unset($addEmployee['assigned_vertical']);
						// debug($addEmployee);exit;

						$retVal         = $this->insert($addEmployee, 'employees');
						$lastInsertedID = $this->db->insert_id();

						$emp_vert['employee_id'] = $lastInsertedID;
						$emp_vert['vertical_id'] = $assigned_vertical;
						$this->insert($emp_vert,'employee_vertical');

						$addEmployee['assigned_vertical'] = $assigned_vertical;

				// Inserts a record in emp_shift 
						if($addEmployee['location_id'] != 4 )
						{
							$shift_data =  array(
								'employee_id' => $lastInsertedID,
								'from_date' => date("Y-m-d"),
								'shift_id' => $addEmployee['shift_id'],
								'to_date' => '0000-00-00',
								'created_by' => $this->EmpId,
							);

							$this->db->insert('emp_shift', $shift_data);
						}

						// Inserts new record in employee_vertical_history
						if($addEmployee['location_id'] != 4 )
						{
							$vertical_data =  array(
								'employee_id' => $lastInsertedID,
								'vertical_id' =>$addEmployee['assigned_vertical'],
								'start_date' => date("Y-m-d"),
								'created_by' => $this->EmpId,
							);

							$this->db->insert('employee_vertical_history', $vertical_data);
						}

						if(!in_array($assigned_vertical, $ops_constant))
						{
							// Inserts new record in employee_client
							if($addEmployee['location_id'] != 4 )
							{
								$this->db->set('employee_id', $lastInsertedID);
								$this->db->set('primary_lead_id', $addEmployee['primary_lead_id']);
								$this->db->set('client_id', 221);
								$this->db->set('grades', $grades);
								$this->db->set('billable', 0);
								$this->db->set('billable_type', 'Buffer');
								$this->db->set('start_date', $addEmployee['doj']);
								$this->db->where_in('employee_id', $lastInsertedID);
								$resVal = $this->db->insert('employee_client');
							}

							//Inserts new record in employee client history
							if($addEmployee['location_id'] != 4 )
							{
								$this->db->set('employee_id', $lastInsertedID);
								$this->db->set('client_id', 221);
								$this->db->set('start_date', $addEmployee['doj']);
								$this->db->where_in('employee_id', $lastInsertedID);
								$resVal = $this->db->insert('employee_client_history');
							}
						}
						

						return $retVal;
					}
					else
						return -1;
				}
				else
				{
					$setData = json_decode(trim($data), true);
					$idVal = $setData['employee_id'];

					if ($this->_isRecordExist($idVal, $setData['company_employ_id'], $setData['email']) == 0) 
					{
						$getprevious_shift= $this->getpreviousshift($idVal);
						$newData = array(
							'company_employ_id'	=> $setData['company_employ_id'],
							'first_name' 		=> $setData['first_name'],
							'last_name' 		=> $setData['last_name'],
							'email' 			=> $setData['email'],
							'mobile' 			=> $setData['mobile'],
							'location_id' 		=> $setData['location_id'],
							'doj' 				=> date("Y-m-d", strtotime($setData['doj'])),
							'dob' 				=> date("Y-m-d", strtotime($setData['dob'])),
							'gender' 			=> $setData['gender'],
							'qualification_id' 	=> $setData['qualification_id'],
							'personal_email' 	=> $setData['personal_email'],
							'alternate_number' 	=> $setData['alternate_number'],
							'present_address' 	=> $setData['present_address'],
							'present_city' 		=> $setData['present_city'],
							'present_state' 	=> $setData['present_state'],
							'present_zipcode' 	=> $setData['present_zipcode'],
							'same_as' 			=> $setData['same_as'],
							'permanent_address' => $setData['permanent_address'],
							'permanent_city' 	=> $setData['permanent_city'],
							'permanent_state' 	=> $setData['permanent_state'],
							'permanent_zipcode' => $setData['permanent_zipcode'],
							'is_onshore' => $setData['is_onshore'],
						);

						$where             = array();
						$set               = array();
						$empData           = $this->input->cookie();
						$EmpId             = $empData['employee_id'];
						$where['employee_id'] = $idVal;

				//Added for date updation
						$empData     = $this->input->cookie();
						$this->EmpId = $empData['employee_id'];

						$newData['updated_by']   = $this->EmpId;
						$newData['updated_date'] = date("Y-m-d H:i:s");

						/* Added code for Chatnam Employees */
						$chatnamflag = false;
						if($setData['location_id'] == 4 )
						{
							// $newData['assigned_vertical'] = NULL;
							$chatnamflag = true;
							//$newData['primary_lead_id'] = 1297;
						}

						$retVal = $this->update($newData, $where, 'employees');

						if($setData['location_id'] != 4 )
						{			
							if($getprevious_shift != $setData['shift_id'])
							{
								$updateshift_data = array(
									'to_date' => date("Y-m-d"),
								);
								$where['to_date'] = '0000-00-00';

								$update_previous_shift = $this->update($updateshift_data, $where, 'emp_shift');

								$shift_data =  array(
									'employee_id' => $idVal,
									'from_date' => date("Y-m-d"),
									'shift_id' => $setData['shift_id'],
									'to_date' => '0000-00-00'
								);

								$this->db->insert('emp_shift', $shift_data);

							}
						}
						return -2;
					} else
					return -1;
				}
			}


    }
    ?>