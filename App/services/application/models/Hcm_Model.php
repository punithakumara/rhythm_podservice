<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Hcm_Model extends INET_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * 
     * @param $data is an Array contains EmployID,OldValue,NewValue,Type,Date
     * Type can be either Vertical,Shifts
     */
    public function insertHcmData($data)
    {
        $dataset = array(
            'EmployID' => $data['EmployID'],
            'OldValue' => $data['OldValue'],
            'NewValue' => $data['NewValue'],
            'Type' => $data['Type'],
            'Date' => date('Y-m-d')
        );
        $retVal  = $this->insert($dataset, 'hcm_table');
    }
    
}
?>
