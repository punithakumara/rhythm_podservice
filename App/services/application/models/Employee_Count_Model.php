<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_Count_Model extends INET_model
{
	public $empArray = array();
	public $count = false;
	
	/*
	* @description : getting all the employee above grade 2
	* @return : setting into employee 
	*/
	private function _setEmpArray()
	{
		$sql = "SELECT employees.employee_id,employees.first_name,employees.last_name 
		FROM employees 
		JOIN designation ON employees.designation_id = designation.designation_id 
		WHERE designation.grades > 2 and employees.status NOT IN(5,6) ORDER BY employees.employee_id ";
		$result =  $this->db->query($sql);
		$totalRecord = $result->num_rows();
		if($totalRecord > 0)
		{
			$this->empArray = $result->result_array();
		} 
		return false;
	}
	
	/*
	 * @descritpion : check if employid exist in number_of_employee table
	 * @return : 'true' or 'false'
	 */
	private function _isEmpExist($empid)
	{
		$sql= "SELECT employee_id FROM number_of_employee WHERE employee_id = ".$empid;
		$result = $this->db->query($sql);
		$status = ($result->num_rows()==0) ? false : true;
		return $status;
	}
	
	/*
	* @description : 
	* First : Set the employee Array
	* Second : get the Total Count for employee
	* Third : if empid exist updating the query
	* Fourth : if empid not exist inserting 
	* 
	*/
	public function updateCount()
	{
		$this->_setEmpArray();//First
		// print_r($this->empArray); exit;
		if(is_array($this->empArray))
		{
			foreach($this->empArray as $key=>$emp)
			{
				$totalCount  = $this->getTotalCount($emp['employee_id']);
				if($this->_isEmpExist($emp['employee_id']))
				{
				    $sql = "UPDATE number_of_employee SET total_count = ".$totalCount." WHERE employee_id = ".$emp['employee_id'];
				    $result = $this->db->query($sql);
				}
				else
				{
				   $sql = "INSERT INTO number_of_employee (employee_id,total_count) VALUES (".$emp['employee_id'].",".$totalCount.")";
				   $result = $this->db->query($sql);
				}
			}
		}	
	}
	
	
	/*
	 * @description :  getting total count 
	 */
	public function getTotalCount($empid) 
	{
		$resArr = array();
		$sql = "SELECT employee_id FROM employees WHERE primary_lead_id = ".$empid." AND status IN(3,5)";
		$result = $this->db->query($sql);
		$this->count += $result->num_rows();
		if($result->num_rows()!=0)
		{
			$resArr = $result->result_array();
			foreach($resArr as $row)
			{
				$this->getTotalCount($row['employee_id']);
			}
		}
		return $this->count;
	}
}
