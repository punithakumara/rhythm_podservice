<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeAchievement_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_achievements()
	{
		if($this->input->get('employee_id') != null && $this->input->get('employee_id') != '')
		{
			$employee_id = $this->input->get('employee_id');
		}
		else
		{
			$empData     = $this->input->cookie();
			$employee_id = $empData['employee_id'];
		}

		$sql="SELECT * FROM employee_achievements
		WHERE employee_id = '$employee_id' 
		ORDER BY id DESC";

		$query = $this->db->query($sql);

		$results['totalCount'] = $query->num_rows();

		$data = $query->result_array();
		$results['data'] = $data;

		return $results;
	}	

	function addAchievement($data = '')
	{
		$achievementData = $data;
		$empData     = $this->input->cookie();

		$this->db->set('employee_id',$empData['employee_id']);
		$this->db->set('award_name', $achievementData['award_name']);
		$this->db->set('year', $achievementData['year']);
		if(isset($achievementData['expiry_year']) && $achievementData['expiry_year']!='Year') {
		$this->db->set('expiry_year', $achievementData['expiry_year']);
		}
		if(isset($achievementData['expiry_month'])  && $achievementData['expiry_month']!='Month') {
		$this->db->set('expiry_month', $achievementData['expiry_month']);
		}
		$this->db->set('link', $achievementData['link']);
		$this->db->set('created_on', date("Y-m-d H:i:s"));								
		$resVal = $this->db->insert('employee_achievements');

		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been added employee achievement (Achievement ID:'.$this->db->insert_id().').';
			$this->userLogs($logmsg); 

		//echo $this->db->last_query();
		//echo $resVal; exit;
		return $resVal;
	}
	
	function updateAchievement($idVal = '',$data = '')
	{
		$empData     = $this->input->cookie();			
		$setData = $data;		
		$where = array();
		$set = array();
		$where['id'] = $setData['id'];
		if(isset($setData['expiry_month']) && $setData['expiry_month']!='Month') {
			$expMonth = $setData['expiry_month'];
		}
		else {
			$expMonth = "";
		}
		if(isset($setData['expiry_year']) && $setData['expiry_year']!='Year') {
			$expYear = $setData['expiry_year'];
		}
		else {
			$expYear = "";
		}
		$dataset = array(
			'award_name'			=> $setData['award_name'],
			'year'			=> $setData['year'],
			'expiry_month'	=> $expMonth,
			'expiry_year'	=> $expYear,
			'link'	=> $setData['link'],
			'updated_on'	=> date("Y-m-d H:i:s")
		);			
		$retVal = $this->update($dataset, $where, 'employee_achievements');

		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee achievement (Achievement ID:'.$setData['id'].').';
			$this->userLogs($logmsg); 
		//echo $this->db->last_query();
		//echo $retVal; exit;			
		return $retVal;		
	}		
	
	function deleteAchievement( $idVal = '' ) 
	{
		$options = array();
		$options['id'] =  $idVal;
		$retVal = $this->delete($options, 'employee_achievements');	

		$empData     = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been deleted employee achievement (Achievement ID:'.$idVal.').';
			$this->userLogs($logmsg);

		return $retVal;
	}

}
?>