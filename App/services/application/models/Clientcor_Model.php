<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Clientcor_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Clientwor_Model');
	}

	/* building query */
	function build_query()
	{
		$sort = json_decode($this->input->get('sort'),true);
		$empData    = $this->input->cookie();
		$status = ($this->input->get('status')!="")?$this->input->get('status'):'All';
		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;
		$all_cor_ids = ($this->input->get('all_cor_ids')!="")?$this->input->get('all_cor_ids'):0;  	
		$this->db->distinct();
		
		$this->db->select("cor.cor_id,wr.work_order_id,wr.wor_name,wt.wor_type,wt.wor_type,wr.service_id,se.name,wr.start_date as wor_start_date,wr.end_date as wor_end_date,wr.wor_id as wor_id, cor.change_order_id, cor.client, DATE_FORMAT(cor.change_request_date, '%d-%b-%Y') AS change_request_date, cor.status, cor.client_partner, cor.engagement_manager, cor.delivery_manager,cor.team_lead,cor.associate_manager,cor.stake_holder, change_in_model, existing_model, new_model,
			CONCAT(IFNULL(cp.`first_name`, ''),' ',IFNULL(cp.`last_name`, '')) AS cp_name,
			CONCAT(IFNULL(em.`first_name`, ''),' ',IFNULL(em.`last_name`, '')) AS em_name,
			CONCAT(IFNULL(dm.`first_name`, ''),' ',IFNULL(dm.`last_name`, '')) AS dm_name,
			CONCAT(IFNULL(sh.`first_name`, ''),' ',IFNULL(sh.`last_name`, '')) AS sh_name1,
			cor.created_by, c.`client_name`, cor.work_order_id AS previous_work_order_id,
			CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `created_name`, cor.description,
			DATE_FORMAT(cor.created_on, '%d-%b-%Y') AS created_on",false);
		$this->db->from("cor");
		$this->db->join('wor wr', 'wr.`work_order_id` = cor.`work_order_id`','left');
		$this->db->join('client c', 'c.`client_id` = cor.`client`','left');
		$this->db->join('employees cp', 'cp.employee_id = cor.client_partner','left');
		$this->db->join('employees em', 'em.employee_id = cor.engagement_manager','left');
		$this->db->join('employees dm', 'dm.employee_id = cor.delivery_manager','left');
		$this->db->join('employees tl', 'tl.employee_id = cor.team_lead','left');
		$this->db->join('employees am', 'am.employee_id = cor.associate_manager','left');
		$this->db->join('employees sh', 'sh.employee_id = cor.stake_holder','left');
		$this->db->join('employees e', 'e.employee_id = cor.created_by','left');
		$this->db->join('wor_types wt', 'wt.wor_type_id = wr.wor_type_id','left');
        $this->db->join('tbl_rhythm_services se', 'se.service_id = wr.service_id','left');
		
		if(!$all_cor_ids)
		{
			if ($status != "All")
			{
				$this->db->where("cor.status",$status);
			}
			else
			{
				$this->db->where('cor.status <>', 'Delete');
			}
		}
		if($empData['Grade']>= 3 && $empData['Grade'] <=4 )
		{
			$this->db->where("cor.client IN (".$empData['ClientId'].")");
			$this->db->where("(wr.wor_id IN (".$empData['WorId'].") OR cor.created_by =".$empData['employee_id'].")");
		}
		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') {
				$this->db->where('('.$filterWhere.')');
			} 
		}
		
		/* start columns sort dynamically*/
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		if(isset($options['sortBy']))
		{			
			if($options['sortBy'] == 'change_request_date')
			{
				$sortBy = 'cor.change_request_date';			
			}
			else
			{
				$sortBy = $options['sortBy'];
			}						
			$this->db->order_by($sortBy, $options['sortDirection'].' ');
		}
		/* End columns sort dynamically*/	
	}
	
	
	
    /**
	* Get list of Client COR
	*
	* @param 
	* @return array
	*/ 
	function view()
	{
		$options = array();
		
		if($this->input->get('hasNoLimit') != '1'){
			$offset = $options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$limit = $options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		$this->build_query();

		$totQuery = $this->db->get();

		
		if(isset($options['limit']) && isset($options['offset'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		elseif(isset($options['limit'])) {
			$this->db->limit($options['limit']);
		}
		
		$this->build_query();
		$query = $this->db->get();		
		$data = $dataArray = $query->result_array();
		$totCount = $totQuery->num_rows();
		
		foreach($data as $key=>$value)
		{
			foreach($value as $k=>$val)
			{
				if($k=='stake_holder' ) 
				{
					if(empty($val))				
						$data[$key]['sh_name']='';
					else
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS sh_name FROM employees WHERE employee_id in (". $val." )";										
						$query = $this->db->query($sql);
						$res = $query->result_array();											
						if(!empty($res))							
							$data[$key]['sh_name']=$res[0]['sh_name'];															 
					}											
				} 
				else if($k=='team_lead' ) 
				{	
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS tl_name FROM employees WHERE employee_id in (". $val." )";										
						$query = $this->db->query($sql);
						$res = $query->result_array();											
						if(!empty($res))							
							$data[$key]['tl_name']=$res[0]['tl_name'];															 
					}											
				}
				else if($k=='associate_manager' ) 
				{	
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS am_name FROM employees WHERE employee_id in (". $val." )";										
						$query = $this->db->query($sql);
						$res = $query->result_array();											
						if(!empty($res))							
							$data[$key]['am_name']=$res[0]['am_name'];															 
					}											
				}
			}
		}
		
		if(!empty($data))
		{
			foreach ($data as $key=>$value)
			{
				$data[$key]['count_history']= $this->getlevel($value['previous_work_order_id']);
				
				if( strpos($value['existing_model'], ',') !== false )
					$data[$key]['existing_model'] = explode(",",$value['existing_model']); 
				
				if( strpos($value['new_model'], ',') !== false)
					$data[$key]['new_model'] = explode(",",$value['new_model']);
			}
		}	
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('rows' => $data)
		);
		return $results;
	}

	
    /**
     * Get FTE list of COR
     *
     * @param
     * @return array
     */
    function get_fte_list()
    {
    	$cor_id = ($this->input->get('cor_id')!="")?$this->input->get('cor_id'):''; 
    	
    	$sql = "SELECT `fte_id`, `fk_cor_id`, `fk_project_type_id`, `no_of_fte`, `appr_buffer`, `shift`, `experience`, `skills`, anticipated_date, `support_coverage`, `responsibilities` ,change_type,`comments`, pt.project_type FROM fte_model fm";     
    	$sql .= " LEFT JOIN project_types pt ON pt.project_type_id = fm.fk_project_type_id";
    	$sql .= " WHERE fk_cor_id='".$cor_id."'";
//echo $sql;  exit;
    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();
    	
    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;
    }
    
    /**
     * Get HOURLY list of  COR
     *
     * @param
     * @return array
     */
    function get_hourly_list()
    {
    	$cor_id = ($this->input->get('cor_id')!="")?$this->input->get('cor_id'):'';

    	$sql = "SELECT `hourly_id`, `fk_cor_id`, `fk_project_type_id`, `hours_coverage`, `shift`, `min_hours`, `max_hours`, `experience`, `skills`,anticipated_date, `support_coverage`, `responsibilities` ,change_type,`comments`, pt.project_type FROM hourly_model hm";
    	$sql .= " LEFT JOIN project_types pt ON pt.project_type_id = hm.fk_project_type_id";
    	$sql .= " WHERE fk_cor_id='".$cor_id."'";

    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();
    	
    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;
    }
    
    /**
     * Get volume list of  COR
     *
     * @param
     * @return array
     */
    function get_volume_list()
    {
    	$cor_id = ($this->input->get('cor_id')!="")?$this->input->get('cor_id'):'';
    	
    	$sql = "SELECT `vb_id`, `fk_cor_id`, `fk_project_type_id`, `volume_coverage`, `shift`, `min_volume`, `max_volume`, `experience`, `skills`,anticipated_date, `support_coverage`, `responsibilities` ,change_type,`comments`, pt.project_type FROM volume_based_model vbm";
    	$sql .= " LEFT JOIN project_types pt ON pt.project_type_id = vbm.fk_project_type_id";
    	$sql .= " WHERE fk_cor_id='".$cor_id."'";
    	
    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();
    	
    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;
    }
    
    function get_project_list()
    {
    	$cor_id = ($this->input->get('cor_id')!="")?$this->input->get('cor_id'):'';
    	
    	$sql = "SELECT `project_id`, `fk_cor_id`, `fk_project_type_id`, `project_name`, `shift`, start_duration, end_duration, `experience`, `skills`, anticipated_date, `support_coverage`, `responsibilities` ,change_type, pm.project_description,`comments`, pt.project_type FROM project_model pm";
    	$sql .= " LEFT JOIN project_types pt ON pt.project_type_id = pm.fk_project_type_id";
    	$sql .= " WHERE fk_cor_id='".$cor_id."'";
    	
    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();

    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;
    }
    
    function update_status($data = '')
    {
    	$dataset = array(
    		'status'   => $data['status'],
    		'description' => $data['description'],
    	);
    	
    	/* Get FTE records*/
    	$fteCountRecords = $this->getFteCount($data['cor_id']);		
    	
    	/* update client table */
    	$this->update_client_cor_close($fteCountRecords);
    	
    	$this->db->where('cor_id', $data['cor_id']);
    	$retVal = $this->db->update('cor',$dataset);
    	
    	return $retVal;
    }
    
    function getFteCount($cor_id)
    {
    	$sql = "SELECT fte_id,no_of_fte,client,change_type FROM fte_model 
    	LEFT JOIN cor ON cor.cor_id = fte_model.fk_cor_id
    	WHERE fk_cor_id=".$cor_id;
    	$query = $this->db->query($sql);
    	$data = $query->result_array();
    	return $data;
    	
    }
    

    function CheckLineCount($fte_details,$hourly_details,$volume_details,$project_details,$wor_id,$get_prev_ids)
    {
    	$fte_array = array();
    	$hr_array = array();
    	$vol_array = array();
    	
    	if (!empty($fte_details)) 
    	{
    		foreach($fte_details as $fte_key=>$fte_value)
    		{
    			if($fte_value['change_type'] == 2)
    			{
    				$fte_array[$fte_value['shift'].'_'.$fte_value['support_coverage'].'_'.$fte_value['responsibilities']]+= $fte_value['no_of_fte'];
    			}
    		}

    		foreach($fte_array as $f_key=>$f_value)
    		{
    			$DelCount[] = $this->CheckDelCount($f_key,$f_value,$wor_id,'FTE');
    		}
    		if (in_array(0, $DelCount)) 
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    	if(!empty($hourly_details))
    	{
    		foreach($hourly_details as $hr_key=>$hr_value)
    		{
    			if($hr_value['change_type'] == 2)
    			{
    				$hr_array[$hr_value['shift'].'_'.$hr_value['support_coverage'].'_'.$hr_value['responsibilities']]+= $hr_value['max_hours'];
    			}
    		}
    		foreach($hr_array as $h_key=>$h_value)
    		{
    			$hrDelCount[] = $this->CheckDelCount($h_key,$h_value,$wor_id,'Hourly' );
    		}
    		
    		if (in_array(0, $hrDelCount)) 
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		} 
    	}
    	if(!empty($volume_details))
    	{
    		foreach($volume_details as $v_key=>$v_value)
    		{
    			if($v_value['change_type'] == 2){
    				$vol_array[$v_value['shift'].'_'.$v_value['support_coverage'].'_'.$v_value['responsibilities']]+= $v_value['max_volume'];
    			}
    		}
    		foreach($vol_array as $v_key=>$v_value)
    		{
    			$VolDelCount[] = $this->CheckDelCount($v_key,$v_value,$wor_id,'Volume' );
    		} 
    		
    		if (in_array(0, $VolDelCount)) 
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    	if(!empty($project_details))
    	{
    		return 1;
    	}
    }
    
    function CheckDelCount($f_key,$f_value,$wor_id,$model)
    {
    	$pieces = explode("_", $f_key);
    	
    	$shift			  =  $pieces[0]; 
    	$support =  $pieces[1]; 
    	$responsibilities =  $pieces[2]; 
    	if($model == 'FTE'){
    		$sql = "SELECT SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS Total_count FROM fte_model WHERE `fk_wor_id` = '$wor_id' AND shift = '$shift' AND support_coverage = '$support' AND responsibilities = '$responsibilities' AND process_id <> 0 ";
    	}
    	if($model == 'Hourly'){
    		$sql = "SELECT SUM(CASE change_type WHEN 1 THEN max_hours  WHEN 2 THEN -max_hours END) AS Total_count  FROM hourly_model WHERE `fk_wor_id` = '$wor_id' AND shift = '$shift' AND support_coverage = '$support' AND responsibilities = '$responsibilities' AND process_id <> 0 ";
    	}
    	if($model == 'Volume'){
    		$sql = "SELECT SUM(CASE change_type WHEN 1 THEN max_volume  WHEN 2 THEN -max_volume END) AS Total_count  FROM volume_based_model WHERE `fk_wor_id` = '$wor_id' AND shift = '$shift' AND support_coverage = '$support' AND responsibilities = '$responsibilities' AND process_id <> 0 ";
    	}
    	//echo $sql;exit;
    	$query = $this->db->query($sql);
    	
    	$row = $query->row();
    	$ret = array();
    	
    	if (isset($row))
    	{
    		if($row->Total_count < $f_value)
    		{
    			return 0;
    		}
    		else
    		{
    			return 1;
    		}
    	}
    }
    
    
    function add_cor($data = '', $fte_arr_details = '', $hourly_arr_details = '', $volume_list = '', $project_list = '',$client_name)
    {
    	$data 				= json_decode(trim($data), true);
    	$empData    		= $this->input->cookie();
    	$fte_details		= json_decode($fte_arr_details, true);
    	$hourly_details		= json_decode($hourly_arr_details, true);
    	$volume_details		= json_decode($volume_list, true);
    	$project_details	= json_decode($project_list, true);
    	$model_array		= array('FTE','Hourly','Project','Volume');
    	$get_level			= $this->getlevel($data['previous_work_order_id']);
    	$new_level 			= $get_level + 1;

    	
    	$volume_id=array();
    	$project_id=array();
    	$fte_id=array();
    	$hourly_id=array();

    	$final_model = array_diff($model_array, $data['existing_model']);

    	$get_prev_ids= $this->get_current_level_trn_rec($data['previous_work_order_id'],$get_level);
    	
    	$wor_id= $this->getwor_id($data['previous_work_order_id']);

    	$LineCount= $this->CheckLineCount($fte_details,$hourly_details,$volume_details,$project_details,$wor_id,$get_prev_ids);
    	
    	if($LineCount == 0){
    		return 0;
    	}
    	$history_data=array();
    	
    	if($data['cor_id'] == '')
    	{
    		if ($data['status'] == 'In-Progress') $return_value = 1;   		
    		if ($data['status'] == 'Open') $return_value = 2;
    		
    		$dataset = array(
    			'change_order_id' 	          => $data['change_order_id'],
    			'change_request_date' 	      => date("Y-m-d", strtotime($data['change_request_date'])),
    			'client' 		              => $data['client'],
    			'status' 		              => $data['status'],
    			'work_order_id' 	          => $data['previous_work_order_id'],
    			'client_partner'              => $data['client_partner'],
    			'engagement_manager'          => $data['engagement_manager'][1],
    			'delivery_manager'            => $data['delivery_manager'],
    			'stake_holder'                => $data['stake_holder'][1],
    			'created_by' 			      => $empData['employee_id'],
    			'existing_model'              => $existingModel,
    			'new_model'                   => $newModel,
    			'team_lead' 	              => $data['team_lead'][1],
    			'associate_manager' 	      => $data['associate_manager'][1]
    		);
    		
    		$retVal   = $this->insert($dataset, 'cor');  		 
    		$insertid = $this->db->insert_id();
    		
    		if(is_array($fte_details))
    		{
    			foreach($fte_details as $fte_key=>$fte_value)
    			{
    				$fte_start_date = $fte_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{
    					$Process_id= $this->Clientwor_Model->create_process('F',$fte_value['shift'],$fte_value['support_coverage'],$fte_value['responsibilities'],'FTE',$data['client']);					
    				}
    				else{
    					$Process_id = '0';
    				}
    				
    				$dataset = array(
    					'fk_wor_id'			=> $wor_id,
    					'fk_cor_id' 	    => $insertid,
    					'fk_project_type_id'=> $fte_value['fk_project_type_id'],
    					'no_of_fte' 	    => $fte_value['no_of_fte'],
    					'appr_buffer' 	    => $fte_value['appr_buffer'],
    					'shift' 		    => $fte_value['shift'],
    					'experience' 		=> $fte_value['experience'],
    					'skills' 	        => $fte_value['skills'],
    					'anticipated_date' 	=> date("Y-m-d", strtotime($fte_start_date)),
    					'support_coverage'	=> $fte_value['support_coverage'],
    					'responsibilities' 	=> $fte_value['responsibilities'],
    					'change_type' 	    => $fte_value['change_type'],
    					'process_id' 	   	=> $Process_id,
    					'comments' 	   		=> $fte_value['comments'],
    				);
    				
    				$this->insert($dataset, 'fte_model');
    				$fte_id[] = $this->db->insert_id();
    				
    				if ($data['status'] == 'Open')  
    				{					
    					$corStartDate = $data['change_request_date'];					
    					$this->update_clients_table($data['client'], $fte_value['no_of_fte'], $fte_value['appr_buffer'], $fte_start_date, $empData['employee_id'], $fte_value['change_type']);
    					$this->Clientwor_Model->update_fte_details($data,$dataset,$fte_value['change_type']);
    				}
    			}
    		}

    		if(is_array($hourly_details))
    		{
    			foreach($hourly_details as $hourly_key=>$hourly_value)
    			{
    				$hourly_start_date = $hourly_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{
    					$Process_id= $this->Clientwor_Model->create_process('H',$hourly_value['shift'],$hourly_value['support_coverage'],$hourly_value['responsibilities'],'Hourly',$data['client']);					
    				}
    				else{
    					$Process_id = '0';
    				}
    				
    				$dataset = array(
    					'fk_wor_id'			=> $wor_id,
    					'fk_cor_id' 	    => $insertid,
    					'fk_project_type_id'=> $hourly_value['fk_project_type_id'],
    					'hours_coverage' 	=> $hourly_value['hours_coverage'],
    					'shift' 		    => $hourly_value['shift'],
    					'min_hours' 		=> $hourly_value['min_hours'],
    					'max_hours' 	    => $hourly_value['max_hours'],
    					'experience'        => $hourly_value['experience'],
    					'anticipated_date' 	=> date("Y-m-d", strtotime($hourly_start_date)),
    					'skills' 	        => $hourly_value['skills'],
    					'support_coverage' 	=> $hourly_value['support_coverage'],
    					'responsibilities' 	=> $hourly_value['responsibilities'],
    					'change_type' 	    => $hourly_value['change_type'],
    					'process_id' 	   	=> $Process_id,
    					'comments' 	   		=> $hourly_value['comments'],
    				);
    				$this->insert($dataset, 'hourly_model');
    				$hourly_id[] = $this->db->insert_id();
    			}
    		}

    		if(is_array($volume_details))
    		{
    			foreach($volume_details as $volume_key=>$volume_value)
    			{
    				$volume_start_date = $volume_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{
    					$Process_id= $this->Clientwor_Model->create_process('V',$volume_value['shift'],$volume_value['support_coverage'],$volume_value['responsibilities'],'Volume',$data['client']);					
    				}
    				else{
    					$Process_id = '0';
    				}
    				
    				$dataset = array(
    					'fk_cor_id' 	    => $insertid,
    					'fk_wor_id'			=> $wor_id,
    					'fk_project_type_id'=> $volume_value['fk_project_type_id'],
    					'volume_coverage' 	=> $volume_value['volume_coverage'],
    					'shift' 		    => $volume_value['shift'],
    					'min_volume' 		=> $volume_value['min_volume'],
    					'max_volume' 	    => $volume_value['max_volume'],
    					'experience'        => $volume_value['experience'],
    					'anticipated_date' 	=> date("Y-m-d", strtotime($volume_start_date)),
    					'skills' 	        => $volume_value['skills'],
    					'support_coverage' 	=> $volume_value['support_coverage'],
    					'responsibilities' 	=> $volume_value['responsibilities'],
    					'change_type' 	    => $volume_value['change_type'],
    					'process_id' 	   	=> $Process_id,
    					'comments' 	   		=> $volume_value['comments'],
    				);
    				$this->insert($dataset, 'volume_based_model');
    				$volume_id[] = $this->db->insert_id();
    			}
    		}

    		if(is_array($project_details))
    		{
    			foreach($project_details as $project_key=>$project_value)
    			{
    				$project_start_date = $project_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{	
    					$Process_id= $this->Clientwor_Model->create_process('P',$project_value['shift'],$project_value['support_coverage'],$project_value['responsibilities'],'Project',$data['client']);					
    				}
    				else{
    					$Process_id = '0';
    				}
    				
    				$dataset = array(
    					'fk_cor_id' 	        => $insertid,
    					'fk_wor_id'				=> $wor_id,
    					'fk_project_type_id'=> $project_value['fk_project_type_id'],
    					'project_name' 	        => $project_value['project_name'],
    					'shift' 		        => $project_value['shift'],
    					'start_duration' 		=> date("Y-m-d", strtotime($project_value['start_duration'])),
    					'end_duration' 	        => date("Y-m-d", strtotime($project_value['end_duration'])),
    					'skills'                => $project_value['skills'],
    					'experience'            => $project_value['experience'],
    					'anticipated_date' 	    => date("Y-m-d", strtotime($project_start_date)),
    					'support_coverage' 		=> $project_value['support_coverage'],
    					'responsibilities' 		=> $project_value['responsibilities'],
    					'change_type' 	        => $project_value['change_type'],
    					'project_description' 	=> $project_value['project_description'],
    					'process_id' 	  		=> $Process_id,
    					'comments' 	   			=> $project_value['comments'],
    				);
    				$this->insert($dataset, 'project_model');
    				$project_id[] = $this->db->insert_id();
    			}
    		}

    		/* notification code */
    		$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];
    		
    		$wor_notfic          = array(
    			'Title' => 'Client COR',
    			'WOR_ID' => $data['change_order_id'],
    			'Client' => $data['client'],
    			'Flag' => 1
    		);
    		
    		$notification_data   = array(
    			'Title' => json_encode($wor_notfic),
    			'Description' => "New COR Added -- " . $data['change_order_id'],
    			'SenderEmployee' => $empData['employee_id'],
    			'Type' => '1',
    			'ReceiverEmployee' => $receiverEmployee,
    			'URL' => ''
    		);
    		$this->addNotification($notification_data);
    		
    		/* end notification code */
    	}
    	
		// IF ELSE CONDITION
    	else
    	{
    		if ($data['status'] == 'In-Progress') $return_value = 3;   		
    		if ($data['status'] == 'Open')
    		{
    			$return_value = 4;
    			$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];
    			
    			$wor_notfic          = array(
    				'Title' => 'Client COR',
    				'WOR_ID' => $data['change_order_id'],
    				'Client' => $data['client'],
    				'Flag' => 1
    			);
    			
    			$notification_data   = array(
    				'Title' => json_encode($wor_notfic),
    				'Description' => "COR Open -- " . $data['change_order_id'],
    				'SenderEmployee' => $empData['employee_id'],
    				'Type' => '1',
    				'ReceiverEmployee' => $receiverEmployee,
    				'URL' => ''
    			);
    			$this->addNotification($notification_data);
    		}
    		
    		if ($data['status'] == 'Delete')
    		{
    			$return_value = 5;
    			$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];
    			
    			$wor_notfic          = array(
    				'Title' => 'Client COR',
    				'WOR_ID' => $data['change_order_id'],
    				'Client' => $data['client'],
    				'Flag' => 1
    			);
    			
    			$notification_data   = array(
    				'Title' => json_encode($wor_notfic),
    				'Description' => "COR Deleted -- " . $data['change_order_id'],
    				'SenderEmployee' => $empData['employee_id'],
    				'Type' => '1',
    				'ReceiverEmployee' => $receiverEmployee,
    				'URL' => ''
    			);
    			$this->addNotification($notification_data);
    		}
            //var_dump($data);exit;
    		
    		$dataset = array(
    			'change_order_id' 	    => $data['change_order_id'],
    			'change_request_date'   => date("Y-m-d", strtotime($data['change_request_date'])),
    			'client' 		        => $data['client'],
    			'status' 		        => $data['status'],
    			'work_order_id' 	    => $data['previous_work_order_id'],
    			'description' 			=> $data['description'],
    		);

    		$this->db->where('cor_id', $data['cor_id']);
    		$retVal = $this->db->update('cor',$dataset);
    		
    		if(is_array($fte_details))
    		{
				//$fte_id=array();
    			foreach($fte_details as $fte_key=>$fte_value)
    			{
    				$fte_start_date = $fte_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{
    					$Process_id= $this->Clientwor_Model->create_process('F',$fte_value['shift'],$fte_value['support_coverage'],$fte_value['responsibilities'],'FTE',$data['client']);
    				}	
    				else
    				{
    					$Process_id = '';
    				}
    				
    				$dataset = array(
    					'fk_wor_id'			=> $wor_id,
    					'fk_project_type_id'=> $fte_value['fk_project_type_id'],
    					'no_of_fte' 	    => $fte_value['no_of_fte'],
    					'appr_buffer' 	    => $fte_value['appr_buffer'],
    					'shift' 		    => $fte_value['shift'],
    					'experience' 		=> $fte_value['experience'],
    					'skills' 	        => $fte_value['skills'],
    					'anticipated_date' 	=> date("Y-m-d", strtotime($fte_start_date)),
    					'support_coverage' 	=> $fte_value['support_coverage'],
    					'responsibilities' 	=> $fte_value['responsibilities'],
    					'change_type' 	    => $fte_value['change_type'],
    					'process_id' 	    => $Process_id,
    					'comments' 	   		=> $fte_value['comments'],
    				);
    				
    				if($fte_value['fte_id'] != "")
    				{
    					$this->db->where('fte_id', $fte_value['fte_id']);
    					$this->db->update('fte_model',$dataset);
    					$fte_id[] =$fte_value['fte_id'];
    				}
    				else
    				{
    					$dataset['fk_cor_id'] = $data['cor_id'];
    					$this->insert($dataset, 'fte_model');
    					$fte_id[] = $this->db->insert_id();
    				}
    				
    				if ($data['status'] == 'Open')  
    				{					
    					$corStartDate = $data['change_request_date'];					
    					$this->update_clients_table($data['client'], $fte_value['no_of_fte'], $fte_value['appr_buffer'], $fte_start_date, $empData['employee_id'], $fte_value['change_type']);
    				}
    			}
    		}
    		
    		if(is_array($hourly_details))
    		{
    			foreach($hourly_details as $hourly_key=>$hourly_value)
    			{
    				$hourly_start_date = $hourly_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{
    					$Process_id= $this->Clientwor_Model->create_process('H',$hourly_value['shift'],$hourly_value['support_coverage'],$hourly_value['responsibilities'],'Hourly',$data['client']);
    				}
    				else
    				{
    					$Process_id = '';
    				}
    				
    				$dataset = array(
    					'fk_wor_id'			=> $wor_id,
    					'fk_project_type_id'=> $hourly_value['fk_project_type_id'],
    					'hours_coverage' 	=> $hourly_value['hours_coverage'],
    					'shift' 		    => $hourly_value['shift'],
    					'min_hours' 		=> $hourly_value['min_hours'],
    					'max_hours' 	    => $hourly_value['max_hours'],
    					'experience'        => $hourly_value['experience'],
    					'anticipated_date' 	=> date("Y-m-d", strtotime($hourly_start_date)),
    					'skills' 	        => $hourly_value['skills'],
    					'support_coverage' 	=> $hourly_value['support_coverage'],
    					'responsibilities' 	=> $hourly_value['responsibilities'],
    					'change_type' 	    => $hourly_value['change_type'],
    					'process_id' 	    => $Process_id,
    					'comments' 	   		=> $hourly_value['comments'],
    				);
    				
    				if($hourly_value['hourly_id'] != "")
    				{
    					$this->db->where('hourly_id', $hourly_value['hourly_id']);
    					$this->db->update('hourly_model',$dataset);
    					$hourly_id[] = $hourly_value['hourly_id'];
    				}
    				else
    				{
    					$dataset['fk_cor_id'] = $data['cor_id'];
    					$this->insert($dataset, 'hourly_model');
    					$hourly_id[] = $this->db->insert_id();
    				}
    			}
    		}
    		
    		if(is_array($volume_details))
    		{
    			foreach($volume_details as $volume_key=>$volume_value)
    			{
    				$volume_start_date = $volume_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{
    					$Process_id= $this->Clientwor_Model->create_process('V',$volume_value['shift'],$volume_value['support_coverage'],$volume_value['responsibilities'],'Volume',$data['client']);
    				}
    				else
    				{
    					$Process_id = '';
    				}
    				
    				$dataset = array(
    					'fk_wor_id'			=> $wor_id,
    					'fk_project_type_id'=> $volume_value['fk_project_type_id'],
    					'volume_coverage' 	=> $volume_value['volume_coverage'],
    					'shift' 		    => $volume_value['shift'],
    					'min_volume' 		=> $volume_value['min_volume'],
    					'max_volume' 	    => $volume_value['max_volume'],
    					'experience'        => $volume_value['experience'],
    					'anticipated_date' 	=> date("Y-m-d", strtotime($volume_start_date)),
    					'skills' 	        => $volume_value['skills'],
    					'support_coverage' 	=> $volume_value['support_coverage'],
    					'responsibilities' 	=> $volume_value['responsibilities'],
    					'change_type'		=> $volume_value['change_type'],
    					'process_id' 	  	=> $Process_id,
    					'comments' 	   		=> $volume_value['comments'],
    				);
    				
    				if($volume_value['vb_id'] != "")
    				{
    					$this->db->where('vb_id', $volume_value['vb_id']);
    					$this->db->update('volume_based_model',$dataset);
    					$volume_id[] = $volume_value['vb_id'];
    					
    				}
    				else
    				{
    					$dataset['fk_cor_id'] = $data['cor_id'];
    					$this->insert($dataset, 'volume_based_model');
    					$volume_id[] = $this->db->insert_id();
    				}
    				
    			}
    		}
    		
    		if(is_array($project_details))
    		{
    			foreach($project_details as $project_key=>$project_value)
    			{
    				$project_start_date = $project_value['anticipated_date'];
    				
    				if ($data['status'] == 'Open')
    				{	
    					$Process_id= $this->Clientwor_Model->create_process('P',$project_value['shift'],$project_value['support_coverage'],$project_value['responsibilities'],'Project',$data['client']);
    				}
    				else{
    					$Process_id = '';
    				}
    				
    				$dataset = array(
    					'fk_wor_id'			    => $wor_id,
    					'fk_project_type_id'	=> $project_value['fk_project_type_id'],
    					'project_name' 	        => $project_value['project_name'],
    					'shift' 		        => $project_value['shift'],
    					'start_duration' 		=> date("Y-m-d", strtotime($project_value['start_duration'])),
    					'end_duration' 	        => date("Y-m-d", strtotime($project_value['end_duration'])),
    					'skills'                => $project_value['skills'],
    					'experience'            => $project_value['experience'],
    					'anticipated_date' 	    => date("Y-m-d", strtotime($project_start_date)),
    					'support_coverage' 		=> $project_value['support_coverage'],
    					'responsibilities' 		=> $project_value['responsibilities'],
    					'change_type' 	        => $project_value['change_type'],
    					'project_description' 	=> $project_value['project_description'],
    					'process_id' 	  		=> $Process_id,
    					'comments' 	   			=> $fte_value['comments'],
    				);
    				
    				if($project_value['project_id'] != "")
    				{
    					$this->db->where('project_id', $project_value['project_id']);
    					$this->db->update('project_model',$dataset);
    					$project_id[] = $project_value['project_id'];
    				}
    				else
    				{
    					$dataset['fk_cor_id'] = $data['cor_id'];
    					$this->insert($dataset, 'project_model');
    					$project_id[] = $this->db->insert_id();
    				}
    			}
    		}			
    	}

        if($return_value == 1){
            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Change order ID:'.$insertid.') has been updated Client COR Draft Saved Successfully.';
            $this->userLogs($logmsg);
        }
        else if ($return_value == 2)
        {
            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Change order ID:'.$insertid.') has been updated Client COR Submitted Successfully.';
            $this->userLogs($logmsg);
        }
        else if($return_value == 3)
        {
            
            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Change order ID:'.$data['cor_id'].') has been updated Client COR Draft Updated Successfully.';
            $this->userLogs($logmsg);
        }
        else if ($return_value == 4)
        {
            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Change order ID:'.$data['cor_id'].') has been updated Client COR Submitted Successfully.';
            $this->userLogs($logmsg);
        }
        else if ($return_value == 5)
        {
            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Change order ID:'.$data['cor_id'].') has been updated Client COR Deleted Successfully.';
            $this->userLogs($logmsg);
        }
        else if ($return_value == 0)
        {
            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Change order ID:'.$data['cor_id'].') has been updated Deletion Count Mismatch.';
            $this->userLogs($logmsg);
        }

    	return $return_value;
    }
    
    function update_client_when_change_in_model($clientID,$wor_id,$PrevFteIds)
    {
    	$existingFteCount = $this->getFteCountByClientID($clientID);	
    	$chkMultipleWor = $this->chkMultipleWorByClientID($clientID);	
    	
    	if($chkMultipleWor == 1)
    	{
    		$data = array('head_count' => 0);	
    	}
    	else if($chkMultipleWor == 0)
    	{
    		$worFteCount = $this->chkWorCountByWorID($clientID,$wor_id,$PrevFteIds);
    		
    		$res = $existingFteCount - $worFteCount;
    		if($res < 0)
    		{
    			$hc = 0;
    		}
    		else
    		{
    			$hc = $res ;
    		}
    		$data = array('head_count' =>$hc);
    	}
    	
    	$this->db->where('client_id', $clientID);
    	$this->db->update('client',$data);
    }
    
    
    function chkMultipleWorByClientID($clientID)
    {
    	$sql = "SELECT * FROM `wor` WHERE `client` = ".$clientID;
    	$query = $this->db->query($sql);
    	
    	if ($query->num_rows() > 1)
    	{
    		return 0;
    	}

    	return 1;
    }
    
    
    function chkWorCountByWorID($clientID,$wor_id,$PrevFteIds)
    {
    	return $this->db->query("SELECT fk_wor_id, SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS no_of_fte FROM `fte_model` WHERE fte_id IN ($PrevFteIds) ")->row()->no_of_fte;
    	
    }
    
    function update_client_cor_close($fteCountRecords)
    {		    							
    	$data = array(
    		'head_count' => $this->get_final_fte_count_of_wor($fteCountRecords),
    	);	 
    	$this->db->where('client_id', $v['client']);
    	$this->db->update('client',$data);
    }
    
    function get_final_fte_count_of_wor($fteCountRecords)
    {
    	$fte_ids = array();		
    	foreach($fteCountRecords as $k=>$v)
    	{
    		$client_id = $v['client'];			
    		$fte_ids[$k] = $v['fte_id'];					
    	}
    	
    	$fte_implode = implode(",",$fte_ids);			
    	$sql = "SELECT fk_wor_id AS projectId,SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS fte,process_id 
    	FROM  `fte_model`  WHERE fte_id IN ('$fte_implode')";
    	$query = $this->db->query($sql);
    	$rowValue = $query->row();		
    	$existingFteCount = $this->getFteCountByClientID($v['client']);
    	
    	if($rowValue->fte > 0)
    	{
    		$final_count = $existingFteCount - $rowValue->fte;
    	}
    	else
    	{
    		$final_count = 	$existingFteCount;
    	}
    	
    	return $final_count;	
    }
    
	/**
     * Update head_count in clients table.
     *
     * @param
     * @return
     */
	function update_clients_table($clientID, $newFteCount, $newBufferCount, $corStartDate, $createdBy, $forcast_change_type)
	{
		$existingFteCount = $this->getFteCountByClientID($clientID);		
		$existingApprovedBuferCount = $this->getApprovedBufferCountByClientID($clientID);		
		$diffDays = (strtotime($corStartDate) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);

		if ($diffDays <= 0)
		{				
			if($forcast_change_type == 1) {
				$final_count = $existingFteCount+$newFteCount;
				$final_Buffer_count = $existingApprovedBuferCount+$newBufferCount;
			}
			if($forcast_change_type == 2) 
			{	
				$final_count = $existingFteCount - $newFteCount;
				$final_Buffer_count = $existingApprovedBuferCount - $newBufferCount;
				
				if($final_count < 0) { $final_count = 0;}
				if($final_Buffer_count < 0) { $final_Buffer_count = 0;}
			}
			if($forcast_change_type <> 3)
			{
				$dataset = array(
					'head_count'   => $final_count,
					'approved_buffer'   => $final_Buffer_count,
				);
				
				$this->db->where('client_id', $clientID);
				$this->db->update('client',$dataset);
			}
		}
		else
		{												
			$dataset = array(
				'client_id'   => $clientID,
				'start_date'  => date("Y-m-d", strtotime($corStartDate)),
				'forecast_fte' => $newFteCount,
				'forecast_appr_buffer' => $newBufferCount,
				'forecast_type' => 1, //1 -> COR addition
				'forecast_impact' => $forcast_change_type,
				'created_date' => date("Y-m-d"),
				'created_by' => $createdBy
			);
			$this->insert($dataset, 'client_forecast');								
		}					
	}
	
	function getFteCountByClientID($clientID)
	{
		if($clientID)
		{

			$sql = "SELECT `head_count` FROM `client` WHERE `client_id` = ".$clientID;
			$query = $this->db->query($sql);

			if ($query->num_rows() > 0)
			{
				$row = $query->row(); 
				return $row->head_count;
			}
			else
			{
				return '';
			}
		}
		else
		{
			return '';
			
		}

	}
	
	
	function getApprovedBufferCountByClientID($clientID)
	{
		$sql = "SELECT `approved_buffer` FROM `client` WHERE `client_id` = '".$clientID."'";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0)
		{
			$row = $query->row(); 
			return $row->approved_buffer;
		}

		return '';
	}
	
	
	function existingModel($data = '')
	{
		$emArr = array();
		
		$wor_id = $data['work_order_id'];
		$sql1 = "SELECT  * FROM wor_cor_transition WHERE work_order_id = '$wor_id' ORDER BY LEVEL DESC LIMIT 1";
		$query1 = $this->db->query($sql1);
		$data1 = $query1->row_array();
		
		if($data1['fte_id'] != '')
		{
			array_push($emArr, array('name'=>"FTE"));
		}
		if($data1['project_model_id'] != '')
		{
			array_push($emArr, array('name'=>"Project"));
		}
		if($data1['volume_base_id'] != '')
		{
			array_push($emArr, array('name'=>"Volume"));
		}
		if($data1['hourly_based_id'] != '')
		{
			array_push($emArr, array('name'=>"Hourly"));
		}
		
		$results = array_merge(
			array('rows' => $emArr)
		);
		
		return $results;
	}
	
	function newModel($data = '')
	{
		$emArr = array();
		$wor_id = $data['work_order_id'];
		$sql1 = "SELECT  * FROM wor_cor_transition WHERE work_order_id = '$wor_id' ORDER BY LEVEL DESC LIMIT 1";
		$query1 = $this->db->query($sql1);
		$data1 = $query1->row_array();
		
		if($data1['fte_id'] == '')
		{
			array_push($emArr, array('name'=>"FTE"));
		}
		if($data1['project_model_id'] == '')
		{
			array_push($emArr, array('name'=>"Project"));
		}
		if($data1['volume_base_id'] == '')
		{
			array_push($emArr, array('name'=>"Volume"));
		}
		if($data1['hourly_based_id'] == '')
		{
			array_push($emArr, array('name'=>"Hourly"));
		}
		
		$results = array_merge(
			array('rows' => $emArr)
		);
		
		return $results;
	}
	
	function getlevel($wor_id)
	{
		$output = $this->db->query("SELECT MAX(LEVEL) AS LEVEL FROM wor_cor_transition WHERE work_order_id = '$wor_id'")->row()->LEVEL;
		
		return $output;
	}
	
	function getwor_id($wor_id)
	{
		$output = $this->db->query("SELECT wor_id FROM wor WHERE work_order_id = '$wor_id'")->row()->wor_id;
		
		return $output;
	}
	
	/*Renamed function from getfte_id to get_current_level_trn_rec  to use as Global fn. called in Wor Model too*/
	function get_current_level_trn_rec($wor_id,$level)
	{
		$query = $this->db->query("SELECT * FROM wor_cor_transition WHERE work_order_id = '$wor_id' AND level = '$level' ");

		return $query->row();
	}
	
	function Duplicate_exixting_records($table, $primary_key_field, $primary_key_val,$cor_id) 
	{
		if ($table == 'fte_model'){
			$query = $this->db->query("SELECT fk_wor_id, SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS no_of_fte,fk_cor_id,appr_buffer, shift, experience, skills, anticipated_date, support_coverage, responsibilities, change_type, process_id  FROM `fte_model` WHERE fte_id IN ($primary_key_val) GROUP BY shift , support_coverage, responsibilities");
		}
		if($table == 'hourly_model'){
			
			$query = $this->db->query("SELECT fk_wor_id, SUM(CASE change_type WHEN 1 THEN max_hours  WHEN 2 THEN -max_hours END) AS max_hours , SUM(CASE change_type WHEN 1 THEN min_hours WHEN 2 THEN -min_hours END) AS min_hours,  fk_cor_id,hours_coverage, shift, experience, skills, anticipated_date, support_coverage, responsibilities, change_type, process_id  FROM `hourly_model` WHERE hourly_id IN ($primary_key_val) GROUP BY shift , support_coverage, responsibilities");
		}
		if($table == 'volume_based_model'){
			
			$query = $this->db->query("SELECT fk_wor_id, SUM(CASE change_type WHEN 1 THEN max_volume  WHEN 2 THEN -max_volume END) AS max_volume , SUM(CASE change_type WHEN 1 THEN min_volume WHEN 2 THEN -min_volume END) AS min_volume,  fk_cor_id, volume_coverage, shift, experience, skills, anticipated_date, support_coverage, responsibilities, change_type, process_id  FROM `volume_based_model` WHERE vb_id IN ($primary_key_val) GROUP BY shift , support_coverage, responsibilities");
		}
		if($table == 'project_model'){
			
			$query = $this->db->query("SELECT fk_wor_id,start_duration, end_duration,  fk_cor_id,project_name , shift, experience, skills, anticipated_date, support_coverage, responsibilities, change_type, project_description,process_id  FROM `project_model` WHERE project_id IN ($primary_key_val)");
			
		}		 
		foreach ($query->result() as $row){  

			$row->change_type= 2;
			$row->fk_cor_id= $cor_id;
			$row->anticipated_date = date("Y-m-d");

			foreach($row as $key=>$val){        
				if($key != $primary_key_field){ 
					$this->db->set($key, $val);               
				}             
			}
			
			$this->db->insert($table); 
			$ids[] = $this->db->insert_id();
		}
		
		return $ids;
		
		
	}
	
	function update_transition_records($column_id, $model_records, $tr_id) 
	{
		$this->db->set($column_id,implode(",",$model_records));
		$this->db->where('tr_id',$tr_id);
		$this->db->update('wor_cor_transition');
		return true;
	}
	
	/*Func DEF: remove resources when change in model from COR from previous model */
	function remove_resources_when_change_in_model($final_model, $get_prev_ids)
	{		
		//for FTE
		if ($get_prev_ids->fte_id != "")
		{
			$fte_ids = explode(",",$get_prev_ids->fte_id);
			
			foreach($fte_ids as $fte_id)
			{			
				$process_id_fte = $this->getFTEdetails($fte_id); //get process_id by fte_id			
				$this->remove_resources_employee_client($process_id_fte, $get_prev_ids->fk_wor_id, $get_prev_ids->client_id);
			}
		}
		
		//for HOURLY
		if ($get_prev_ids->hourly_based_id != "")
		{
			$hourly_based_ids = explode(",",$get_prev_ids->hourly_based_id);
			
			foreach($hourly_based_ids as $hourly_based_id)
			{
				$hourly_based_id_hourly = $this->getHourlydetails($hourly_based_id); //get process_id by hourly_based_id			
				$this->remove_resources_employee_client($hourly_based_id_hourly, $get_prev_ids->fk_wor_id, $get_prev_ids->client_id);
			}
		}
		
		//for VOLUME
		if ($get_prev_ids->volume_base_id != "")
		{
			$volume_base_ids = explode(",",$get_prev_ids->volume_base_id);			
			foreach($volume_base_ids as $volume_base_id)
			{
				$process_id_vol = $this->getVolumedetails($volume_base_id); //get process_id by volume_base_id			
				$this->remove_resources_employee_client($process_id_vol, $get_prev_ids->fk_wor_id, $get_prev_ids->client_id);
			}
		}
		
		//for PROJECT
		if ($get_prev_ids->project_model_id != "")
		{
			$project_model_ids = explode(",",$get_prev_ids->project_model_id);
			
			foreach($project_model_ids as $project_model_id)
			{
				$process_id_project = $this->getProjectdetails($project_model_id); //get process_id by project_model_id			
				$this->remove_resources_employee_client($process_id_project, $get_prev_ids->fk_wor_id, $get_prev_ids->client_id);
			}
		}
	}

	//get FTE details by fte_id
	function getFTEdetails($fte_id)
	{
		return $this->db->query("SELECT process_id FROM fte_model WHERE fte_id = '$fte_id'")->row()->process_id;
		
	}
	
	//get HOURLY details by hourly_id
	function getHourlydetails($hourly_id)
	{
		return $this->db->query("SELECT process_id FROM hourly_model WHERE hourly_id = '$hourly_id'")->row()->process_id;
		
	}
	
	//get VOLUME details by volume_id
	function getVolumedetails($volume_id)
	{
		return $this->db->query("SELECT process_id FROM volume_based_model WHERE vb_id = '$volume_id'")->row()->process_id;
		
	}
	
	//get PROJECT details by project_id
	function getProjectdetails($project_id)
	{
		return $this->db->query("SELECT process_id FROM project_model WHERE project_id = '$project_id'")->row()->process_id;
		
	}
	
	//remove resources from employee client mapping table
	function remove_resources_employee_client($process_id, $wor_id, $client_id)
	{
		$sql_resources ="DELETE FROM employee_client WHERE client_id = ".$client_id." AND process_id = ".$process_id." AND wor_id = ".$wor_id."";					
		$this->db->query($sql_resources);
	}
	
	function chkLineRecord()
	{
		$wor_id 	= ($this->input->get('wor_id')) ? $this->input->get('wor_id') : "";
		$shift 	= ($this->input->get('shift')) ? $this->input->get('shift') : "";
		$support 	= ($this->input->get('support')) ? $this->input->get('support') : "";
		$responsibilities 	= ($this->input->get('responsibilities')) ? $this->input->get('responsibilities') : "";
		$change_type 	= ($this->input->get('change_type')) ? $this->input->get('change_type') : "";
		$table 	= ($this->input->get('table')) ? $this->input->get('table') : "";
		
		$query = $this->db->query("SELECT * FROM $table WHERE `fk_wor_id` = '$wor_id' AND shift = '$shift' AND support_coverage = '$support' AND responsibilities = '$responsibilities' AND change_type = 1 ");
		
		return  $query->num_rows();
	}
}
?>