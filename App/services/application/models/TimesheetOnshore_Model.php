<?php
ini_set('max_execution_time', 3000);
defined('BASEPATH') OR exit('No direct script access allowed');

class TimesheetOnshore_Model extends INET_Model
{
	
	function __construct() 
	{
		parent::__construct();
		$this->config->load('admin_config');
		$this->load->model('Employees_Model');
	}

	function view($params) 
	{
		// debug($_GET);exit;
		$empData    = $this->input->cookie();
		
		$emp_id = ($this->input->get('teamMember')!='') ? $this->input->get('teamMember') : $empData['employee_id'];
		
		$week_range = (isset($params['week_range'])) ? $params['week_range'] : "";
		
		$output = false;
		
		$output["metaData"]["idProperty"]="timesheetID";
		$output["metaData"]["successProperty"]="success";
		$output["metaData"]["root"]="data";
		
		// you can parse field values via your database schema
		$output["metaData"]["fields"][] = array('name' => 'past', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'ClientName', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Task', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Engagement', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'ServiceType', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Role', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day7', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment7', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Status', 'type' => 'string');
		
		$output["columns"]=$this->get_weekDays($week_range);
		if(isset($params['week_range']))
			$week_range = $params['week_range'];
		
		$monthResult = explode(":",$this->get_totalMonthHours($emp_id, $week_range));
		$hours = $monthResult[0];
		$minutes = $monthResult[1];
		$output['TotalMonthHours'] = "<b>Month Total</b> = ".$hours." Hrs  ".$minutes." Min";
		
		$weekResult = explode(":",$this->get_totalWeekHours($emp_id, $week_range));
		$overall_hours = $weekResult[0];
		$overall_minutes = $weekResult[1];
		$output['TotalWeekHours'] = "<b>Week Total</b> = ".$overall_hours." Hrs  ".$overall_minutes." Min";
		
		$output["success"] = true;
		$output["data"] = array();
		$output["data"]= $this->get_timesheet_data($emp_id,$week_range);
		
		return $output;

	}
	
	function get_totalMonthHours($emp_id,$week_range)
	{
		$sql1 = "SELECT * FROM task_week WHERE week_code_id ='".$week_range."'";
		$query1 = $this->db->query($sql1);
		$result1 = $query1->row_array();
		$dateArr = explode('-',$result1['week_date']);
		$year = $dateArr[0];
		$month = $dateArr[1];
		$startDate = date("$year-$month-01");
		$endDate = date("$year-$month-31");
		
		$sql2 = "SELECT * FROM task_week WHERE week_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$query2 = $this->db->query($sql2);
		$result2 = $query2->result_array();
		
		$dayTotalHrs = 0;
		foreach($result2 AS $val)
		{
			$sql = "SELECT";
			if($val['week_name']=="Mon")
			{
				$sql .= " SUM(TIME_TO_SEC(day1)) AS dayHrs";
			}
			if($val['week_name']=="Tue")
			{
				$sql .= " SUM(TIME_TO_SEC(day2)) AS dayHrs";
			}
			if($val['week_name']=="Wed")
			{
				$sql .= " SUM(TIME_TO_SEC(day3)) AS dayHrs";
			}
			if($val['week_name']=="Thu")
			{
				$sql .= " SUM(TIME_TO_SEC(day4)) AS dayHrs";
			}
			if($val['week_name']=="Fri")
			{
				$sql .= " SUM(TIME_TO_SEC(day5)) AS dayHrs";
			}
			if($val['week_name']=="Sat")
			{
				$sql .= " SUM(TIME_TO_SEC(day6)) AS dayHrs";
			}
			if($val['week_name']=="Sun")
			{
				$sql .= " SUM(TIME_TO_SEC(day7)) AS dayHrs";
			}
			$sql .= " FROM onshore_time_entry WHERE task_week_id='".$val['week_code_id']."' AND emp_id='".$emp_id."'";
			$query = $this->db->query($sql);
			$result = $query->row_array();
			
			$dayTotalHrs += $result['dayHrs'];
		}
		
		$hours = floor($dayTotalHrs / 3600);
		$minutes = floor(($dayTotalHrs / 60) % 60);
		
		return ($dayTotalHrs!="0") ? "$hours:$minutes" : "0:0";
	}
	
	function get_totalWeekHours($emp_id,$week_range)
	{
		$weeklyHrs = 0;
		$sql = "SELECT SUM(TIME_TO_SEC(day1))+ SUM(TIME_TO_SEC(day2))+SUM(TIME_TO_SEC(day3))+SUM(TIME_TO_SEC(day4))+SUM(TIME_TO_SEC(day5))+SUM(TIME_TO_SEC(day6))+SUM(TIME_TO_SEC(day7)) AS weeklyHrs FROM onshore_time_entry WHERE task_week_id='$week_range' AND emp_id=$emp_id";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		$weeklyHrs += $result['weeklyHrs'];
		
		$sqldates = "SELECT week_date FROM task_week WHERE week_code_id='".$week_range."'";
		$querydates = $this->db->query($sqldates);
		$dates =  $querydates->result_array();
		
		$hours = floor($weeklyHrs / 3600);
		$minutes = floor(($weeklyHrs / 60) % 60);
		
		return ($weeklyHrs!="0") ? "$hours:$minutes" : "0:0";
	}
	
	private function getProdutionHoursDetails($employee_id, $startDate, $endDate)
	{
		$sql = "SELECT SUM(TIME_TO_SEC(theorem_hrs)) AS TimeTaken
		FROM `utilization` 
		WHERE utilization.task_date BETWEEN '".$startDate."' AND '".$endDate."' AND employee_id IN( ".$employee_id." ) AND status NOT IN(0,2)  
		-- GROUP BY employee_id";

		$result = $this->db->query($sql);
		$val = $result->row_array();
		
		return isset($val['TimeTaken'])?$val['TimeTaken']:'0';
	}

	function timesheet_week_range()
	{
		$sql="SELECT * FROM timesheet_week_range WHERE status='1'";
		$query = $this->db->query($sql);
		$result= $query->result();	
		$totCount = $query->num_rows();
		
		if($result)
		{
			for($i=0;$i<$totCount;$i++)
			{
				$data[$i]['week_code_id'] =	$result[$i]->timesheet_week_range_id;
				$data[$i]['week_range'] = $result[$i]->week_range."(".$result[$i]->week_code.")";
			}
		}
		// print_r($data);
		return $data;
	}

	function task_codes()
	{
		$empData = $this->input->cookie();
		// debug($empData);exit;
		$VertId = $empData['VertId'];
		$filter = $this->input->get('query');
		$clientID = ($this->input->get('client_id')=="Theorem India Pvt Ltd")?221:0;
		$sql = "SELECT DISTINCT task_name AS task_name 
		FROM timesheet_task_codes,timesheet_vertical_task_subtask_map 
		WHERE timesheet_vertical_task_subtask_map.timesheet_task_code_id = timesheet_task_codes.timesheet_task_code_id AND status='1'";
		if($VertId != '20')
		{
			$sql .= " AND vertical_id IN(".$VertId.")";
		}
		if($clientID>=0)
		{
			$sql .= " AND client = '".$clientID."'";
		}
		if($filter!="")
		{
			$sql .= " AND task_name LIKE '%".$filter."%'";
		}
		$sql .= " ORDER BY task_name ASC";

		$query = $this->db->query($sql);
		$data= $query->result_array();

		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}

	function get_weekDays($weekrange="")
	{
		$sql = "select task_week_id,week_name,week_date from task_week where week_code_id='".$weekrange."'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$totCount = $query->num_rows();

		$i = 1;
		$taskArr = $this->task_codes();
		$subtaskArr = $this->sub_task_codes();
		$clientArr	= $this->clients();
		
		foreach($result as $key=>$value)
		{
			$output[]=array('header'=>$value['week_name']." ".date( 'd', strtotime($value['week_date'])),
				'dataIndex'=> 'day'.$i,'draggable'=>'','menuDisabled' => '1','sortable' =>'',
				'editor'=>array('xtype'=> 'timefield','minValue' => '0:01','maxValue' => '12:00','format' => 'H:i','increment' => 15), 'summaryType'=> 'sum','flex'=> 1)  ;           
			$i++;
		}
		
		return $output;	
	}

	function get_week_range()
	{
		// $sql="SELECT * FROM timesheet_week_range WHERE status='1' AND timesheet_week_range_id<=WEEK(NOW(),1) ORDER BY timesheet_week_range_id DESC LIMIT 5";
		$sql = "SELECT * FROM timesheet_week_range WHERE status='1' AND timesheet_week_range_id IN(SELECT DISTINCT week_code_id AS timesheet_week_range_id FROM task_week WHERE week_date<=DATE_ADD(CURDATE(), INTERVAL 15 DAY) ORDER BY week_code_id DESC) ORDER BY timesheet_week_range_id DESC LIMIT 7";
		
		$query=$this->db->query($sql);
		$result=$query->result_array();
		$totCount = $query->num_rows();
		$output["metaData"]["idProperty"]="weekrangeID";
		$output["metaData"]["successProperty"]="success";
		$output["metaData"]["root"]="data";
		foreach($result as $key=>$value)
		{
			$response[$key]['id']=$value['timesheet_week_range_id'];
			$response[$key]['week']=$value['week_range']." (".$value['week_code'].")";
			
		}
		$output["data"] = $response;
		return $output;

	}

	function get_future_week_range()
	{
		$sql = "SELECT * FROM timesheet_week_range WHERE status='1' AND timesheet_week_range_id IN(SELECT DISTINCT week_code_id AS timesheet_week_range_id FROM task_week WHERE week_date<=DATE_ADD(CURDATE(), INTERVAL 15 DAY) ORDER BY week_code_id DESC) ORDER BY timesheet_week_range_id DESC LIMIT 2";
		
		$query=$this->db->query($sql);
		$result=$query->result_array();
		$totCount = $query->num_rows();
		$output["metaData"]["idProperty"]="weekrangeID";
		$output["metaData"]["successProperty"]="success";
		$output["metaData"]["root"]="data";
		foreach($result as $key=>$value)
		{
			$response[$key]['id']=$value['timesheet_week_range_id'];
			$response[$key]['week']=$value['week_range']." (".$value['week_code'].")";
			
		}
		$output["data"] = $response;
		return $output;

	}

	function get_timesheet_data($emp_id,$week_range_id)
	{
		$sql="SELECT m.task_code_id,t.task_type,c.client_name,st.service_type,e.engagement_type,r.role_type,m.timesheet_task_id,m.task_week_id,IF(m.day1!='00:00', TIME_FORMAT(m.day1, '%H:%i'),'') AS day1,IF(m.day2!='00:00', TIME_FORMAT(m.day2, '%H:%i'),'') AS day2,IF(m.day3!='00:00', TIME_FORMAT(m.day3, '%H:%i'),'') AS day3,
		IF(m.day4!='00:00', TIME_FORMAT(m.day4, '%H:%i'),'') AS day4,IF(m.day5!='00:00', TIME_FORMAT(m.day5, '%H:%i'),'') AS day5,IF(m.day6!='00:00', TIME_FORMAT(m.day6, '%H:%i'),'') AS day6,
		IF(m.day7!='00:00', TIME_FORMAT(m.day7, '%H:%i'),'') AS day7,comment1,comment2,comment3,comment4,comment5,comment6,comment7,m.status 
		FROM onshore_time_entry m
		JOIN onshore_tasks t ON t.task_id=m.task_code_id
		LEFT JOIN onshore_servicetypes st ON st.service_id=m.service_type_id
		LEFT JOIN onshore_engagement e ON e.engagement_id=m.engagement_id
		LEFT JOIN onshore_roles r ON r.role_id=m.role_id
		JOIN client c ON c.client_id=m.client_id
		WHERE emp_id='$emp_id' AND task_week_id='$week_range_id' ORDER BY m.task_code_id DESC";
		//echo $sql; exit;
		$query=$this->db->query($sql);
		$result=$query->result_array();
		
		$data_util = array();

		$i=0;
		foreach($result as $key=>$value)
		{
			$data_util[$i]['timesheetID'] = $value['timesheet_task_id'];
			$data_util[$i]['past'] = $this->checkpastweek($week_range_id);
			$data_util[$i]['ClientName'] = $value['client_name'];
			$data_util[$i]['Task'] = $value['task_type'];
			$data_util[$i]['Engagement'] = $value['engagement_type'];
			$data_util[$i]['ServiceType'] = $value['service_type'];
			$data_util[$i]['Role'] = $value['role_type'];
			//$data_util['SubTask'] = isset($value['sub_task_name'])?$value['sub_task_name']:"";
			$data_util[$i]['day1'] = isset($value['day1'])?$value['day1']:"";
			$data_util[$i]['day2'] = isset($value['day2'])?$value['day2']:"";
			$data_util[$i]['day3'] = isset($value['day3'])?$value['day3']:"";
			$data_util[$i]['day4'] = isset($value['day4'])?$value['day4']:"";
			$data_util[$i]['day5'] = isset($value['day5'])?$value['day5']:"";
			$data_util[$i]['day6'] = isset($value['day6'])?$value['day6']:"";
			$data_util[$i]['day7'] = isset($value['day7'])?$value['day7']:"";
			$data_util[$i]['comment1'] = $value['comment1'];
			$data_util[$i]['comment2'] = $value['comment2'];
			$data_util[$i]['comment3'] = $value['comment3'];
			$data_util[$i]['comment4'] = $value['comment4'];
			$data_util[$i]['comment5'] = $value['comment5'];
			$data_util[$i]['comment6'] = $value['comment6'];
			$data_util[$i]['comment7'] = $value['comment7'];
			$data_util[$i]['Status'] = $value['status'];
			
			$i++;
		}
		// debug($data_util);exit;
		return $data_util;
	}

	function checkpastweek($week_range_id)
	{
		$date_string = date("Y-m-d H:i:s");
		$week_no = date("W", strtotime($date_string));

		$sqlWeek ="SELECT week_code FROM timesheet_week_range WHERE timesheet_week_range_id ='".$week_range_id."'";
		$query = $this->db->query($sqlWeek);
		$result = $query->row_array();

		$week_code = explode("-", $result['week_code']); 
		if($week_code[1] < $week_no )
		{
			return 'show';
		}
		else
		{
			return 'hide';
		}
	}

/*	function get_task_code($task_name)
	{
		$sqlTaskcode ="SELECT timesheet_task_code_id FROM timesheet_task_codes WHERE task_name ='".$task_name."'";
		$query=$this->db->query($sqlTaskcode);
		$result=$query->result_array();
		return $result[0]['timesheet_task_code_id'];

	}*/

	function insert_task_data($timesheetID,$weekcode,$task_code,$servicetype_code,$engagement_code,$role_code,$client_id,$params)
	{
		$empData    = $this->input->cookie();
		$emp_id = ($this->input->post('teamMember')!='') ? $this->input->post('teamMember') : $empData['employee_id'];
		// echo $emp_id;exit;
		$insert = "INSERT INTO onshore_time_entry (task_week_id,client_id,engagement_id,service_type_id,role_id,task_code_id,emp_id,day1,day2,day3,day4,day5,day6,day7,status)
		VALUES('".$weekcode."','".$client_id."','".$engagement_code."','".$servicetype_code."','".$role_code."','".$task_code."','".$emp_id."','".$params['day1']."','".$params['day2']."','".$params['day3']."','".$params['day4']."','".$params['day5']."','".$params['day6']."','".$params['day7']."','1')";
		$query=$this->db->query($insert);
		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Timesheet ID:'.$this->db->insert_id().') has been added onshore timesheet Successfully.';
		$this->userLogs($logmsg);
		return $this->db->insert_id();
	}

	function update_task_data($timesheetID,$weekcode,$task_code,$servicetype_code,$engagement_code,$role_code,$client_id,$params)
	{
		$empData = $this->input->cookie();
		$emp_id = ($this->input->post('teamMember')!='') ? $this->input->post('teamMember') : $empData['employee_id'];			
		// echo $emp_id;exit;
		$update	= "UPDATE onshore_time_entry SET client_id='".$client_id."',engagement_id='".$engagement_code."',service_type_id='".$servicetype_code."',role_id='".$role_code."', task_code_id='".$task_code."',updated_on=NOW(), updated_by = '$emp_id'";
		
		$update.=",day1='".$params['day1']."'";
		$update.=",day2='".$params['day2']."'";
		$update.=",day3='".$params['day3']."'";
		$update.=",day4='".$params['day4']."'";
		$update.=",day5='".$params['day5']."'";
		$update.=",day6='".$params['day6']."'";
		$update.=",day7='".$params['day7']."'";
		$update.=",status='1'";
		$update.= " WHERE timesheet_task_id='".$timesheetID."' AND emp_id='".$emp_id."'";
		
		$this->db->query($update);
		if ($this->db->affected_rows() > 0)
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Timesheet ID:'.$timesheetID.') has been updated onshore timesheet Successfully.';
			$this->userLogs($logmsg);
			return 1;
		// print_r($update);
	}

	function update_task_comments($timesheetID, $params)
	{
		$update	= "UPDATE onshore_time_entry SET updated_on=NOW()";
		
		if(isset($params['comment1']))
		{
			$update.=",comment1='".addslashes($params['comment1'])."'";
		}
		if(isset($params['comment2']))
		{
			$update.=",comment2='".addslashes($params['comment2'])."'";
		}
		if(isset($params['comment3']))
		{
			$update.=",comment3='".addslashes($params['comment3'])."'";
		}
		if(isset($params['comment4']))
		{
			$update.=",comment4='".addslashes($params['comment4'])."'";
		}
		if(isset($params['comment5']))
		{
			$update.=",comment5='".addslashes($params['comment5'])."'";
		}
		if(isset($params['comment6']))
		{
			$update.=",comment6='".addslashes($params['comment6'])."'";
		}
		if(isset($params['comment7']))
		{
			$update.=",comment7='".$params['comment7']."'";
		}
		$update.= " WHERE timesheet_task_id='".$timesheetID."'";
		// echo $update; exit;
		$this->db->query($update);
		if ($this->db->affected_rows() > 0)
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(TimesheetID:'.$timesheetID.') has been updated onshore timesheet comments.';
			$this->userLogs($logmsg);
			return 1;
	}

	function delete_timesheet_task($timesheetID)
	{
		$empData    = $this->input->cookie();	
		$sql="DELETE FROM onshore_time_entry WHERE timesheet_task_id='".$timesheetID."'";
		$this->db->query($sql);
		if ($this->db->affected_rows() > 0)
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Timesheet ID:'.$timesheetID.') has been removed onshore timesheet entry Successfully.';
			$this->userLogs($logmsg);
			return 1;

	}
	function onshore_clients()
	{
		$filter = $this->input->get('query');
		$sql="SELECT client_name FROM client WHERE onshore_client='1'";
		// if($filter!="")
		// {
		// 	$sql .= " AND client_name LIKE '%".$filter."%'";
		// }
		$sql .= " ORDER BY client_name ASC";
		$query = $this->db->query($sql);
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}
	
	function get_task_code($task_name)
	{
		$sqlTaskcode ="SELECT task_id FROM onshore_tasks WHERE task_type ='".$task_name."'";
		$query=$this->db->query($sqlTaskcode);
		$result=$query->result_array();
		return $result[0]['task_id'];
	}
	
	function get_role_code($role_name)
	{ 
		$sqlRolecode ="SELECT role_id FROM onshore_roles WHERE role_type ='".$role_name."'";
		$query=$this->db->query($sqlRolecode);
		$result=$query->result_array();
		if(count($result)>0) {
		return $result[0]['role_id'];
		}
		else {
			return '';
		}
	}
	
	function get_servicetype_code($servicetype_name)
	{
		$sqlServicecode ="SELECT service_id FROM onshore_servicetypes WHERE service_type ='".$servicetype_name."'";
		$query=$this->db->query($sqlServicecode);
		$result=$query->result_array();
		return $result[0]['service_id'];
	}
	
	function get_engagement_code($enagagement_name)
	{
		$sqlTaskcode ="SELECT engagement_id FROM onshore_engagement WHERE engagement_type ='".$enagagement_name."'";
		$query=$this->db->query($sqlTaskcode);
		$result=$query->result_array();
		return $result[0]['engagement_id'];
	}

	function sub_task_codes()
	{
		$empData = $this->input->cookie();
		$VertId = $empData['VertId'];
		$filter = $this->input->get('query');
		$task_name = $this->input->get('task_name');
		$clientID = ($this->input->get('client_id')=="Theorem India Pvt Ltd")?221:0;
		$sql = "SELECT DISTINCT sub_task_name AS sub_task_name 
		FROM timesheet_subtasks
		JOIN timesheet_vertical_task_subtask_map ON timesheet_vertical_task_subtask_map.timesheet_subtask_id = timesheet_subtasks.timesheet_subtask_id 
		JOIN timesheet_task_codes ON timesheet_task_codes.timesheet_task_code_id = timesheet_vertical_task_subtask_map.timesheet_task_code_id 
		WHERE subtask_status='1'";
		if($VertId != '20')
		{
			$sql .= " AND vertical_id IN(".$VertId.")";
		}
		if($clientID>=0)
		{
			$sql .= " AND client = '".$clientID."'";
		}
		if($filter!="")
		{
			$sql .= " AND sub_task_name LIKE '%".$filter."%'";
		}
		if($task_name!="")
		{
			$sql .= " AND task_name LIKE '".$task_name."'";
		}
		$sql .= " ORDER BY sub_task_name ASC";
		$query = $this->db->query($sql);
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}

	function clients()
	{
		$filter = $this->input->get('query');
		$sql="SELECT client_name FROM client WHERE status='1'";
		if($filter!="")
		{
			$sql .= " AND client_name LIKE '%".$filter."%'";
		}
		$sql .= " ORDER BY client_name ASC";
		$query = $this->db->query($sql);
		//print_r($this->db->last_query());exit; 
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}
	function servicetypes()
	{
		$filter = $this->input->get('query');
		$sql="SELECT service_type FROM onshore_servicetypes WHERE status='1'";
		if($filter!="")
		{
			$sql .= " AND service_type LIKE '%".$filter."%'";
		}
		$sql .= " ORDER BY service_type ASC";
		$query = $this->db->query($sql);
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}
	function engagements()
	{
		$filter = $this->input->get('query');
		$sql="SELECT engagement_type FROM onshore_engagement WHERE status='1'";
		if($filter!="")
		{
			$sql .= " AND engagement_type LIKE '%".$filter."%'";
		}
		$sql .= " ORDER BY engagement_type ASC";
		$query = $this->db->query($sql);
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}
	function roles()
	{
		$filter = $this->input->get('query');
		$sql="SELECT role_type FROM onshore_roles WHERE status='1'";
		if($filter!="")
		{
			$sql .= " AND role_type LIKE '%".$filter."%'";
		}
		$sql .= " ORDER BY role_type ASC";
		$query = $this->db->query($sql);
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}
	
	function tasks()
	{
		$filter = $this->input->get('query');
		$sql="SELECT task_type FROM onshore_tasks WHERE status='1'";
		if($filter!="")
		{
			$sql .= " AND task_type LIKE '%".$filter."%'";
		}
		$sql .= " ORDER BY task_type ASC";
		$query = $this->db->query($sql);
		$data= $query->result_array();	
		
		$results = array_merge(
			array('data' => $data)
		);
		return $results;
	}

	function get_client_id($client_name)
	{
		$sql ="SELECT client_id FROM client WHERE client_name ='".$client_name."'";
		$query=$this->db->query($sql);
		$result=$query->result_array();
		return $result[0]['client_id'];

	}

	function get_subtask_code($subtask_name)
	{
		$sql ="SELECT timesheet_subtask_id FROM timesheet_subtasks WHERE sub_task_name ='".$subtask_name."'";
		$query=$this->db->query($sql);
		$result=$query->result_array();
		if(isset($result[0]))
			return $result[0]['timesheet_subtask_id'];

	}

	function get_category_id($task_id,$subtask_id,$catClient_id)
	{
		$empData = $this->input->cookie();
		$VertId = $empData['VertId'];
		$sql = "SELECT timesheet_category_id 
		FROM timesheet_vertical_task_subtask_map 
		WHERE vertical_id='".$VertId."' AND client ='".$catClient_id."' AND timesheet_task_code_id='".$task_id."' AND timesheet_subtask_id='".$subtask_id."'";
		$query=$this->db->query($sql);
		$result=$query->result_array();
		if(isset($result[0]))
			return $result[0]['timesheet_category_id'];

	}

	function get_production_hours($week_range_id,$emp_id='')
	{
		$empData    = $this->input->cookie();
		$emp_id		= ($emp_id=='')?$empData['employee_id']:$emp_id;
		
		$sqldates = "SELECT week_date FROM task_week WHERE week_code_id='".$week_range_id."'";
		$querydates = $this->db->query($sqldates);
		$dates =  $querydates->result_array();
		
		foreach($dates as $key=>$value)
		{
			$dateArr[] = $value['week_date'];
		}
		$datesArr = implode("', '", $dateArr);
		
		$empClient = "SELECT GROUP_CONCAT(DISTINCT fk_client_id SEPARATOR ',') AS client_id FROM utilization WHERE employee_id='".$emp_id."' AND task_date IN('".$datesArr."')";
		$empClient = $this->db->query($empClient);
		$clientId =  $empClient->row_array();
		$client_id = $clientId['client_id'];

		$data_util = array();

		$clientArr = explode(",",$client_id);
		$clientArr = array_unique($clientArr);
		$i=0;
		
		foreach($clientArr as $val)
		{
			$data_util['timesheetID'] = $i.'a';
			$data_util['past'] = 'hide';
			$data_util['ClientName'] = $this->getClientName($val);
			$data_util['Task'] = "Production Work";
			$data_util['SubTask'] = "";
			$data_util['day1'] = $this->getClientHours($val, $dateArr[0], $emp_id);
			$data_util['day2'] = $this->getClientHours($val, $dateArr[1], $emp_id);
			$data_util['day3'] = $this->getClientHours($val, $dateArr[2], $emp_id);
			$data_util['day4'] = $this->getClientHours($val, $dateArr[3], $emp_id);
			$data_util['day5'] = $this->getClientHours($val, $dateArr[4], $emp_id);
			$data_util['day6'] = $this->getClientHours($val, $dateArr[5], $emp_id);
			$data_util['day7'] = $this->getClientHours($val, $dateArr[6], $emp_id);
			$output[]= $data_util;
			$i++;
		}
		
		return $output;
		
	}
	
	private function getClientName($cid='')
	{
		if($cid!='')
		{
			$sql = "SELECT client_name FROM client WHERE client_id=".$cid;
			$query = $this->db->query($sql);
			$list = $query->row_array();
			
			return $list['client_name'];
		}
		else
		{
			return "";
		}
	}
	
	private function getClientHours($cid='', $date='', $emp_id)
	{
		if($cid!='' && $date!='')
		{
			$sql = "SELECT TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') AS hours 
			FROM utilization
			WHERE employee_id='".$emp_id."' AND status NOT IN(0,2) AND task_date in ('".$date."') AND utilization.fk_client_id=".$cid; 
			$sql.=" GROUP BY task_date";
			
			$query = $this->db->query($sql);
			$list = $query->row_array();
			
			return isset($list['hours'])?$list['hours']:'';
		}
		else
		{
			return "";
		}
	}

	function approve($params) 
	{
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		$grade 	 = $empData['Grade'];
		
		$client_id = (isset($params['client_id'])) ? $params['client_id'] : "";
		$week_range = (isset($params['week_range'])) ? $params['week_range'] : "";
		
		$output = false;
		
		$output["metaData"]["idProperty"]="timesheetID";
		$output["metaData"]["successProperty"]="success";
		$output["metaData"]["root"]="data";
		
		// you can parse field values via your database schema
		$output["metaData"]["fields"][] = array('name' => 'emp_id', 'type' => 'int');
		$output["metaData"]["fields"][] = array('name' => 'EmployeeName', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'ClientName', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Task', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Engagement', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'Role', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'ServiceType', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'day7', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment1', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment2', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment3', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment4', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment5', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment6', 'type' => 'string');
		$output["metaData"]["fields"][] = array('name' => 'comment7', 'type' => 'string');
		//$output["metaData"]["fields"][] = array('name' => '', 'type' => 'string');
		
		$output["columns"] = $this->get_approveWeekDays($week_range);
		if(isset($params['week_range']))
			$week_range = $params['week_range'];
		
		$output["success"] = true;
		$output["data"] =  array();

		if($this->input->get('level_members') == 'second')
		{
			$sql = "CALL fs_level_employees($emp_id)";
			$query = $this->db->query($sql);
			mysqli_next_result($this->db->conn_id); // to remove error 'commands out of sync you can't run this command'
			$result = $query->result_array();
			$empId_arr = explode(',',$result[0]['employee_id']);
		}
		else if($this->input->get('level_members') == 'all')
		{
			// all member
			$emp_id = $this->getReportees();	
			$empId_arr = array_map('current', $emp_id);
		}
		else
		{
			$emp_id = $this->getReportees($emp_id);	
			$empId_arr = array_map('current', $emp_id);
		}
		$empId_arr = implode(",", array_filter($empId_arr));
		
		$timeData = $this->get_timesheet_approval($week_range, $client_id, $empId_arr);
		$output["data"] = array_merge($output["data"], $timeData);
		/*if($week_status=="All" || $week_status=="2")
		{
			$prodData = $this->get_production_approval($week_range, $empId_arr);
			$output["data"] = array_merge($output["data"], $prodData);
		}*/

		// echo "<pre>"; print_r($timeData); exit;
		return $output;

	}
	
	function get_timesheet_approval($week_range_id, $client_id, $employee_id)
	{
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		$grade 	 = $empData['Grade'];
		$filter = json_decode($this->input->get('filter'),true);
		
		$sql = "SELECT m.emp_id,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `EmployeeName`,t.task_type AS Task,oe.engagement_type AS Engagement,ro.role_type AS Role,ost.service_type AS ServiceType,c.client_name AS ClientName,m.timesheet_task_id AS timesheetID,IF(m.day1!='00:00', TIME_FORMAT(m.day1, '%H:%i'),'') AS day1,IF(m.day2!='00:00', TIME_FORMAT(m.day2, '%H:%i'),'') AS day2,IF(m.day3!='00:00', TIME_FORMAT(m.day3, '%H:%i'),'') AS day3,
		IF(m.day4!='00:00', TIME_FORMAT(m.day4, '%H:%i'),'') AS day4,IF(m.day5!='00:00', TIME_FORMAT(m.day5, '%H:%i'),'') AS day5,IF(m.day6!='00:00', TIME_FORMAT(m.day6, '%H:%i'),'') AS day6,
		IF(m.day7!='00:00', TIME_FORMAT(m.day7, '%H:%i'),'') AS day7,comment1,comment2,comment3,comment4,comment5,comment6,comment7,
		CASE m.status WHEN '0' THEN 'Rejected' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Approved' END AS `Status` 
		FROM onshore_time_entry m
		LEFT JOIN employees ON employees.employee_id = m.emp_id
		JOIN onshore_tasks t ON t.task_id = m.task_code_id
		LEFT JOIN designation d ON d.designation_id = employees.designation_id
		LEFT JOIN onshore_engagement oe ON oe.engagement_id = m.engagement_id
		LEFT JOIN onshore_roles ro ON ro.role_id = m.role_id
		LEFT JOIN onshore_servicetypes ost ON ost.service_id = m.service_type_id
		JOIN client c ON c.client_id = m.client_id
		WHERE task_week_id = '$week_range_id' AND employees.status != 6 AND employees.is_onshore='1' ";
		if($client_id!="All" && $client_id!="")
		{
			$sql .= " AND m.client_id ='".$client_id."' "; 
		}
		// if($grade!=7)
		// {
		// 	//$sql .= " AND employees.primary_lead_id ='".$emp_id."' OR d.grades='".$grade."' ";
		// }
		
		/* start search functionality */		
		$filterWhere = '';
		if(isset($filter) && $filter != '') 
		{
			foreach ($filter as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) 
					{
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			if($filterWhere != '') 
			{
				$sql .= " AND ";	
				$sql .= '('.$filterWhere.')';
			} 
		}
		/* end search functionality */	
		
		$sql .= " ORDER BY c.client_name,t.task_type ASC";
//echo $sql; exit;
		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;
	}

	function get_production_approval($week_range_id, $employee_id)
	{
		$filter = json_decode($this->input->get('filter'),true);

		$sqldates = "SELECT week_date FROM task_week WHERE week_code_id='".$week_range_id."'";
		$querydates = $this->db->query($sqldates);
		$dates =  $querydates->result_array();
		
		foreach($dates as $key=>$value)
		{
			$dateArr[] = $value['week_date'];
		}
		$datesArr = implode("', '", $dateArr);

		$empClient = "SELECT EmployeeName, ClientName, 'Production Work' AS Task, '' AS SubTask,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day1))), '%H:%i') AS day1,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day2))), '%H:%i') AS day2,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day3))), '%H:%i') AS day3,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day4))), '%H:%i') AS day4,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day5))), '%H:%i') AS day5,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day6))), '%H:%i') AS day6,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day7))), '%H:%i') AS day7,
		'Approved' AS `Status`
		FROM (
		SELECT utilization.employee_id AS emp_id,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `EmployeeName`,
		client_name AS ClientName, 'Production Work' AS Task, '' AS SubTask,
		CASE WHEN task_date='$dateArr[0]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day1,
		CASE WHEN task_date='$dateArr[1]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day2,
		CASE WHEN task_date='$dateArr[2]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day3,
		CASE WHEN task_date='$dateArr[3]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day4,
		CASE WHEN task_date='$dateArr[4]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day5,
		CASE WHEN task_date='$dateArr[5]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day6,
		CASE WHEN task_date='$dateArr[6]' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') END day7,
		task_date,client_id,'Approved' AS `Status`
		FROM utilization 
		JOIN employees ON employees.employee_id = utilization.employee_id
		JOIN `client` ON client.client_id = utilization.fk_client_id
		WHERE utilization.employee_id IN($employee_id) AND task_date BETWEEN '$dateArr[0]' AND '$dateArr[6]'
		GROUP BY utilization.employee_id,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')),
		client_name,client_id,task_date) X ";

		/* start search functionality */		
		$filterWhere = '';
		if(isset($filter) && $filter != '') 
		{
			$sdf = $filter[0]['value'];
			$empClient .= " WHERE employeeName LIKE '%$sdf%'";
		}
		/* end search functionality */

		$empClient .= " GROUP BY employeeName,clientname ";

		$empClient = $this->db->query($empClient);
		$data_util =  $empClient->result_array();
		
		return $data_util;
	}
	
	function get_approveWeekDays($weekrange="")
	{
		$sql = "SELECT task_week_id,week_name,week_date FROM task_week WHERE week_code_id='".$weekrange."'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		$totCount = $query->num_rows();

		$i = 1;
		
		$output[] = array('header'=>'Client Name <span style="color:red">*</span>','dataIndex'=> 'ClientName','draggable'=>'','menuDisabled' => '1','sortable' =>'','flex'=> 2);           
		$output[] = array('header'=>'Service Type','dataIndex'=> 'ServiceType','draggable'=>'','menuDisabled' => '1','sortable' =>'','flex'=> 2.5);
		$output[] = array('header'=>'Engagement','dataIndex'=> 'Engagement','draggable'=>'','menuDisabled' => '1','sortable' =>'','flex'=> 2.5);
		$output[] = array('header'=>'Role','dataIndex'=> 'Role','draggable'=>'','menuDisabled' => '1','sortable' =>'','flex'=> 2.5);		
		$output[] = array('header'=>'Task <span style="color:red">*</span>','dataIndex'=> 'Task','draggable'=>'','menuDisabled' => '1','sortable' =>'','flex'=> 2);
		foreach($result as $key=>$value)
		{
			$output[] = array('header'=>$value['week_name']." ".date( 'd', strtotime($value['week_date'])),
				'dataIndex'=> 'day'.$i,'draggable'=>'','menuDisabled' => '1','sortable' =>'','summaryType'=> 'sum','flex'=> 1)  ;           
			$i++;
		}
		
		return $output;	
	}
	
	private function getReportees($primary_lead_id='')
	{
		$empData = $this->input->cookie();
		$vertical_id = $empData['VertIDs'];
		$sql = "";

		if($primary_lead_id != '')
		{
			$sql .= "SELECT employee_id FROM employees WHERE employees.status != 6 AND employees.primary_lead_id='$primary_lead_id'";
		}
		else
		{
			$sql .= "SELECT employees.employee_id FROM employees 
			JOIN employee_vertical ON employees.employee_id = employee_vertical.employee_id 
			WHERE employees.status != 6";

			if($empData['Grade'] < 7)
				$sql .= " AND employee_vertical.vertical_id IN($vertical_id)";
		}
		
		$result = $this->db->query($sql);
		$resultArr = $result->result_array();

		// debug($resultArr);exit;
		
		return $resultArr;
	}

	function ApproveRecords($input_values)
	{
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		if(isset($input_values['timesheetIDs']))
		{
			$timesheet = json_decode($input_values['timesheetIDs']);
			$timesheet = array_filter($timesheet);
			if(is_array($timesheet))
				$timesheet = implode(", ", $timesheet);
			$logs = $this->insert_log($timesheet,$input_values['value']);
			$sql_timesheet = "UPDATE onshore_time_entry SET status=".$input_values['value'].", approved_by =".$emp_id.", approved_on ='".date('Y-m-d H:i:s')."'   WHERE timesheet_task_id IN (".$timesheet.")";
			$this->db->query($sql_timesheet);
			$affectedRows = $this->db->affected_rows();
			
			

			if($input_values['value']=="0")
			{
				$empData = $this->input->cookie();
				$empIds = json_decode($input_values['empIds']);
				$empIds = implode(",", $empIds);
				
				$notification_data   = array(
					'Title' => "",
					'Description' => "Timesheet -- " . $input_values['reason'],
					'SenderEmployee' => $empData['employee_id'],
					'Type' => '1',
					'ReceiverEmployee' => $empIds,
					'URL' => ''
				);
				$this->addNotification($notification_data);
			}
			
			return  $affectedRows;
		}
	}

	function insert_log($timesheet,$status)
	{
		$timesheet_val = explode(", ", $timesheet);
		//echo "<pre>"; print_r($timesheet_val);exit;
		$empData    = $this->input->cookie();
		$emp_id		= $empData['employee_id'];
		$changes_on = date("Y-m-d H:i:s");
		foreach ($timesheet_val as $result) {
			$insert = "INSERT INTO timesheet_status_log (timesheet_task_mapping_id,change_by,changes_on,status)
			VALUES('".$result."','".$emp_id."','".$changes_on."','".$status."')";
			$query=$this->db->query($insert);
		}
		
	}
	
	
	function task_weekcode($month, $year)
	{
		$sql = "SELECT week_code_id, GROUP_CONCAT(week_name SEPARATOR '+') AS week_name FROM `task_week` WHERE week_date LIKE '%".$year."-".$month."%' GROUP BY week_code_id";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		
		return $result;
	}
	
	
	function monthlyDetailsSelf()
	{
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		
		$monthArr = array(
			array('start_date'=>'2019-09-01','end_date'=>'2019-09-30'),
			array('start_date'=>'2019-08-01','end_date'=>'2019-08-31'),
			array('start_date'=>'2019-07-01','end_date'=>'2019-07-31'),
			array('start_date'=>'2019-06-01','end_date'=>'2019-06-30'),
			array('start_date'=>'2019-05-01','end_date'=>'2019-05-31'),
			array('start_date'=>'2019-04-01','end_date'=>'2019-04-30'),
			array('start_date'=>'2019-03-01','end_date'=>'2019-03-31'),
			array('start_date'=>'2019-02-01','end_date'=>'2019-02-28'),
			array('start_date'=>'2019-01-01','end_date'=>'2019-01-31'),
			array('start_date'=>'2018-12-01','end_date'=>'2018-12-31'),
			array('start_date'=>'2018-11-01','end_date'=>'2018-11-30'),
			array('start_date'=>'2018-10-01','end_date'=>'2018-10-31'),
			array('start_date'=>'2018-09-01','end_date'=>'2018-09-30'),
			array('start_date'=>'2018-08-01','end_date'=>'2018-08-31'),
		);
		
		$output = [];
		foreach($monthArr AS $list)
		{
			$time = strtotime($list['start_date']);
			$month = date("F",$time);
			$monthNo = date("m",$time);
			$year = date("Y",$time);
			
			$prodHrs = $this->getProdutionHoursDetails($emp_id, $list['start_date'], $list['end_date']);
			$task_weeks = $this->task_weekcode($monthNo, $year);
			$timesheetHrs = 0;
			foreach($task_weeks as $result)
			{
				$hours = str_replace('Mon', 'SUM(TIME_TO_SEC(day1))', $result['week_name']);
				$hours = str_replace('Tue', 'SUM(TIME_TO_SEC(day2))', $hours);
				$hours = str_replace('Wed', 'SUM(TIME_TO_SEC(day3))', $hours);
				$hours = str_replace('Thu', 'SUM(TIME_TO_SEC(day4))', $hours);
				$hours = str_replace('Fri', 'SUM(TIME_TO_SEC(day5))', $hours);
				$hours = str_replace('Sat', 'SUM(TIME_TO_SEC(day6))', $hours);
				$hours = str_replace('Sun', 'SUM(TIME_TO_SEC(day7))', $hours);
				
				$sql = "SELECT $hours AS hours 
				FROM `onshore_time_entry` 
				WHERE onshore_time_entry.status = '2' AND task_week_id =".$result['week_code_id']." AND emp_id IN(".$emp_id.")";				
				$query = $this->db->query($sql);
				$result2 = $query->row_array();
				
				$timesheetHrs += isset($result2['hours'])?$result2['hours']:0;
			}
			$totalHrs = $prodHrs + $timesheetHrs;
			
			$resultArr = array('month' => $month, 'timesheetHrs' => $timesheetHrs, 'productionHrs' => $prodHrs, 'totalHrs' => $totalHrs);
			
			array_push($output, $resultArr);
		}
		
		$finalResult = array(
			'rows' => $output,
			'totalCount' => count($output)
		);
		return 	$finalResult;
	}
	
	
	function monthlyDetailsTeam()
	{
		$empData = $this->input->cookie();
		$teamEmpId = ($this->input->get('emp_id')) ? $this->input->get('emp_id') : "";
		$emp_id  = $empData['employee_id'];
		
		$monthArr = array(
			array('start_date'=>'2019-09-01','end_date'=>'2019-09-30'),
			array('start_date'=>'2019-08-01','end_date'=>'2019-08-31'),
			array('start_date'=>'2019-07-01','end_date'=>'2019-07-31'),
			array('start_date'=>'2019-06-01','end_date'=>'2019-06-30'),
			array('start_date'=>'2019-05-01','end_date'=>'2019-05-31'),
			array('start_date'=>'2019-04-01','end_date'=>'2019-04-30'),
			array('start_date'=>'2019-03-01','end_date'=>'2019-03-31'),
			array('start_date'=>'2019-02-01','end_date'=>'2019-02-28'),
			array('start_date'=>'2019-01-01','end_date'=>'2019-01-31'),
			array('start_date'=>'2018-12-01','end_date'=>'2018-12-31'),
			array('start_date'=>'2018-11-01','end_date'=>'2018-11-30'),
			array('start_date'=>'2018-10-01','end_date'=>'2018-10-31'),
			array('start_date'=>'2018-09-01','end_date'=>'2018-09-30'),
			array('start_date'=>'2018-08-01','end_date'=>'2018-08-31'),
		);
		
		$empIds =  array_unique (parent::getEmployeeList($emp_id));
		$empIds = implode(",", $empIds);
		
		$output = [];
		foreach($monthArr AS $list)
		{
			$time = strtotime($list['start_date']);
			$month = date("F",$time);
			$monthNo = date("m",$time);
			$year = date("Y",$time);

			if($teamEmpId!="")
			{
				$empIds = $teamEmpId;
			}
			
			$prodHrs = $this->getProdutionHoursDetails($empIds, $list['start_date'], $list['end_date']);
			
			$task_weeks = $this->task_weekcode($monthNo, $year);
			$timesheetHrs = 0;
			foreach($task_weeks as $result)
			{
				$hours = str_replace('Mon', 'SUM(TIME_TO_SEC(day1))', $result['week_name']);
				$hours = str_replace('Tue', 'SUM(TIME_TO_SEC(day2))', $hours);
				$hours = str_replace('Wed', 'SUM(TIME_TO_SEC(day3))', $hours);
				$hours = str_replace('Thu', 'SUM(TIME_TO_SEC(day4))', $hours);
				$hours = str_replace('Fri', 'SUM(TIME_TO_SEC(day5))', $hours);
				$hours = str_replace('Sat', 'SUM(TIME_TO_SEC(day6))', $hours);
				$hours = str_replace('Sun', 'SUM(TIME_TO_SEC(day7))', $hours);
				
				$sql = "SELECT $hours AS hours 
				FROM `timesheet_task_mapping` 
				WHERE timesheet_task_mapping.status = '2' AND task_week_id =".$result['week_code_id'];
				if($teamEmpId!="")
				{
					$sql .= " AND emp_id IN(".$teamEmpId.") ";
				}
				else if($empIds!="")
				{
					$sql .= " AND emp_id IN(".$empIds.") ";
				}

				$query = $this->db->query($sql);
				$result2 = $query->row_array();
				
				$timesheetHrs += isset($result2['hours'])?$result2['hours']:0;
			}
			$totalHrs = $prodHrs + $timesheetHrs;
			
			$resultArr = array('month' => $month, 'timesheetHrs' => $timesheetHrs, 'productionHrs' => $prodHrs, 'totalHrs' => $totalHrs);
			
			array_push($output, $resultArr);
		}
		
		$finalResult = array(
			'rows' => $output,
			'totalCount' => count($output)
		);
		return 	$finalResult;
	}
	
	
	function taskDetailsSelf()
	{
		$timemonth = ($this->input->get('timemonth')) ? $this->input->get('timemonth') : date('Y-m-01');
		$time = strtotime($timemonth);
		$month = date("m",$time);
		$year = date("Y",$time);
		
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		
		$task_weeks = $this->task_weekcode($month, $year);
		
		$output = [];
		foreach($task_weeks as $result)
		{
			$hours = str_replace('Mon', 'SUM(TIME_TO_SEC(day1))', $result['week_name']);
			$hours = str_replace('Tue', 'SUM(TIME_TO_SEC(day2))', $hours);
			$hours = str_replace('Wed', 'SUM(TIME_TO_SEC(day3))', $hours);
			$hours = str_replace('Thu', 'SUM(TIME_TO_SEC(day4))', $hours);
			$hours = str_replace('Fri', 'SUM(TIME_TO_SEC(day5))', $hours);
			$hours = str_replace('Sat', 'SUM(TIME_TO_SEC(day6))', $hours);
			$hours = str_replace('Sun', 'SUM(TIME_TO_SEC(day7))', $hours);
			
			$sql = "SELECT task_name AS task,sub_task_name AS subtask,$hours AS hours ";
			$sql .= " FROM `onshore_time_entry` 
			JOIN timesheet_task_codes ON timesheet_task_codes.timesheet_task_code_id = timesheet_task_mapping.task_code_id 
			LEFT JOIN timesheet_subtasks ON timesheet_subtasks.timesheet_subtask_id = timesheet_task_mapping.sub_task_id 
			WHERE timesheet_task_mapping.status = '2' AND task_week_id =".$result['week_code_id']." AND emp_id='".$emp_id."'
			GROUP BY timesheet_task_codes.timesheet_task_code_id,timesheet_subtasks.timesheet_subtask_id
			ORDER BY task_name ASC,sub_task_name ASC";

			$query = $this->db->query($sql);
			$result2 = $query->result_array();
			
			if(count($output)>0)
			{
				foreach ($output as $key => $value)
				{
					foreach ($result2 as $key2 => $value2)
					{
						if(($value['task'] == $value2['task']) && ($value['subtask'] == $value2['subtask']))
						{
							$hours = $value['hours']+$value2['hours'];
							$output[$key]['hours'] = $hours;
							unset($result2[$key2]);
						}
					}
				}
			}

			foreach ($result2 as $value)
			{
				if($value['hours']!=0)
				{
					array_push($output, $value);
				}
			}
		}
		
		$keys = array_column($output, 'task');

		array_multisort($keys, SORT_ASC, $output);
		
		$finalResult = array(
			'rows' => $output,
			'totalCount' => count($output)
		);
		return 	$finalResult;
	}
	
	
	function taskDetailsTeam()
	{
		$timemonth = ($this->input->get('timemonth')) ? $this->input->get('timemonth') : date('Y-m-01');
		$teamEmpId = ($this->input->get('emp_id')) ? $this->input->get('emp_id') : "";
		$time = strtotime($timemonth);
		$month = date("m",$time);
		$year = date("Y",$time);
		
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		
		$empIds =  array_unique (parent::getEmployeeList($emp_id));
		$empIds = implode(",", $empIds);
		
		$task_weeks = $this->task_weekcode($month, $year);
		
		$output = [];
		foreach($task_weeks as $result)
		{
			$hours = str_replace('Mon', 'SUM(TIME_TO_SEC(day1))', $result['week_name']);
			$hours = str_replace('Tue', 'SUM(TIME_TO_SEC(day2))', $hours);
			$hours = str_replace('Wed', 'SUM(TIME_TO_SEC(day3))', $hours);
			$hours = str_replace('Thu', 'SUM(TIME_TO_SEC(day4))', $hours);
			$hours = str_replace('Fri', 'SUM(TIME_TO_SEC(day5))', $hours);
			$hours = str_replace('Sat', 'SUM(TIME_TO_SEC(day6))', $hours);
			$hours = str_replace('Sun', 'SUM(TIME_TO_SEC(day7))', $hours);
			
			$sql = "SELECT task_name AS task,sub_task_name AS subtask, $hours AS hours ";
			$sql .= " FROM `onshore_time_entry` 
			JOIN timesheet_task_codes ON timesheet_task_codes.timesheet_task_code_id = onshore_time_entry.task_code_id 
			LEFT JOIN timesheet_subtasks ON timesheet_subtasks.timesheet_subtask_id = onshore_time_entry.sub_task_id 
			WHERE onshore_time_entry.status = '2' AND task_week_id =".$result['week_code_id']; 
			if($teamEmpId!="")
			{
				$sql .= " AND emp_id IN(".$teamEmpId.") ";
			}
			else if($empIds!="")
			{
				$sql .= " AND emp_id IN(".$empIds.") ";
			}
			$sql .= " GROUP BY timesheet_task_codes.timesheet_task_code_id,timesheet_subtasks.timesheet_subtask_id
			ORDER BY task_name ASC,sub_task_name ASC";

			$query = $this->db->query($sql);
			$result2 = $query->result_array();
			
			if(count($output)>0)
			{
				foreach ($output as $key => $value)
				{
					foreach ($result2 as $key2 => $value2)
					{
						if(($value['task'] == $value2['task']) && ($value['subtask'] == $value2['subtask']))
						{
							$hours = $value['hours']+$value2['hours'];
							$output[$key]['hours'] = $hours;
							unset($result2[$key2]);
						}
					}
				}
			}

			foreach ($result2 as $value)
			{
				if($value['hours']!=0)
				{
					array_push($output, $value);
				}
			}
		}

		$keys = array_column($output, 'task');

		array_multisort($keys, SORT_ASC, $output);
		
		$finalResult = array(
			'rows' => $output,
			'totalCount' => count($output)
		);
		return 	$finalResult;
	}
	
	
	function timesheetStatusSelf()
	{
		$timemonth = ($this->input->get('timemonth')) ? $this->input->get('timemonth') : date('Y-m-01');
		$time = strtotime($timemonth);
		$month = date("m",$time);
		$year = date("Y",$time);
		
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		
		$task_weeks = $this->task_weekcode($month, $year);
		
		$output = array();
		foreach($task_weeks as $result)
		{
			$Value1 = str_replace('Mon', 'SUM(TIME_TO_SEC(day1))', $result['week_name']);
			$Value1 = str_replace('Tue', 'SUM(TIME_TO_SEC(day2))', $Value1);
			$Value1 = str_replace('Wed', 'SUM(TIME_TO_SEC(day3))', $Value1);
			$Value1 = str_replace('Thu', 'SUM(TIME_TO_SEC(day4))', $Value1);
			$Value1 = str_replace('Fri', 'SUM(TIME_TO_SEC(day5))', $Value1);
			$Value1 = str_replace('Sat', 'SUM(TIME_TO_SEC(day6))', $Value1);
			$Value1 = str_replace('Sun', 'SUM(TIME_TO_SEC(day7))', $Value1);
			
			$sql = "SELECT IF(status=0,'Rejected', IF(status=1,'Pending','Approved')) AS Name, $Value1 AS Value1
			FROM `timesheet_task_mapping`  
			WHERE task_week_id =".$result['week_code_id']." AND emp_id ='".$emp_id."' 
			GROUP BY timesheet_task_mapping.status";

			$query = $this->db->query($sql);
			$result2 = $query->result_array();
			
			if(count($output)>0)
			{
				foreach ($output as $key => $value)
				{
					foreach ($result2 as $key2 => $value2)
					{
						if($value['Name'] == $value2['Name'])
						{
							$hours = $value['Value1']+$value2['Value1'];
							$output[$key]['Value1'] = $hours;
							unset($result2[$key2]);
						}
					}
				}
			}

			foreach ($result2 as $value)
			{
				if($value['Value1']!=0)
				{
					array_push($output, $value);
				}
			}
		}
		$keys = array_column($output, 'Name');

		array_multisort($keys, SORT_ASC, $output);
		
		$finalResult = array(
			'rows' => $output,
			'totalCount' => count($output)
		);
		return 	$finalResult;
	}
	
	function timesheetStatusTeam()
	{
		$timemonth = ($this->input->get('timemonth')) ? $this->input->get('timemonth') : date('Y-m-01');
		$teamEmpId = ($this->input->get('emp_id')) ? $this->input->get('emp_id') : "";
		$time = strtotime($timemonth);
		$month = date("m",$time);
		$year = date("Y",$time);
		
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		
		$empIds =  array_unique (parent::getEmployeeList($emp_id));
		$empIds = implode(",", $empIds);
		
		$task_weeks = $this->task_weekcode($month, $year);
		
		$output = array();
		foreach($task_weeks as $result)
		{
			$Value1 = str_replace('Mon', 'SUM(TIME_TO_SEC(day1))', $result['week_name']);
			$Value1 = str_replace('Tue', 'SUM(TIME_TO_SEC(day2))', $Value1);
			$Value1 = str_replace('Wed', 'SUM(TIME_TO_SEC(day3))', $Value1);
			$Value1 = str_replace('Thu', 'SUM(TIME_TO_SEC(day4))', $Value1);
			$Value1 = str_replace('Fri', 'SUM(TIME_TO_SEC(day5))', $Value1);
			$Value1 = str_replace('Sat', 'SUM(TIME_TO_SEC(day6))', $Value1);
			$Value1 = str_replace('Sun', 'SUM(TIME_TO_SEC(day7))', $Value1);
			
			$sql = "SELECT IF(status=0,'Rejected', IF(status=1,'Pending','Approved')) AS Name, $Value1 AS Value1
			FROM `onshore_time_entry`  
			WHERE task_week_id =".$result['week_code_id'];
			if($teamEmpId!="")
			{
				$sql .= " AND emp_id IN(".$teamEmpId.") ";
			}
			else if($empIds!="")
			{
				$sql .= " AND emp_id IN(".$empIds.") ";
			}
			
			$sql .= " GROUP BY onshore_time_entry.status";

			$query = $this->db->query($sql);
			$result2 = $query->result_array();
			
			if(count($output)>0)
			{
				foreach ($output as $key => $value)
				{
					foreach ($result2 as $key2 => $value2)
					{
						if($value['Name'] == $value2['Name'])
						{
							$hours = $value['Value1']+$value2['Value1'];
							$output[$key]['Value1'] = $hours;
							unset($result2[$key2]);
						}
					}
				}
			}

			foreach ($result2 as $value)
			{
				if($value['Value1']!=0)
				{
					array_push($output, $value);
				}
			}
		}
		$keys = array_column($output, 'Name');

		array_multisort($keys, SORT_ASC, $output);
		
		$finalResult = array(
			'rows' => $output,
			'totalCount' => count($output)
		);
		return 	$finalResult;
	}
	
	function removeElementWithValue($array, $key, $value)
	{
		foreach($array as $subKey => $subArray)
		{
			if($subArray['SubTask'] == '1-1')
			{
				$array[$subKey]['SubTask'] = "'1-1'";
			}
			
			if($subArray[$key] == $value)
			{
				unset($array[$subKey]);
			}
		}
		return $array;
	}
	
	function downloadCsv($startDate, $endDate) 
	{
		$empData = $this->input->cookie();
		$emp_id  = $empData['employee_id'];
		$grade = $empData['Grade'];
		
		$index =0;
		$data = array();
		
		$sql = "SELECT week_name,week_date FROM `task_week` WHERE week_date BETWEEN '".$startDate."' AND '".$endDate."'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		
		foreach($result as $key => $val)
		{
			$timeData = $this->getTimeDetails($val['week_name'],$val['week_date'],$grade,$emp_id);
			$data = array_merge($data, $timeData);
			$prodData = $this->getProdutionDetails($val['week_date'],$grade,$emp_id);
			$data = array_merge($data, $prodData);
			$index++;
		}

		$data = $this->removeElementWithValue($data, "Hours", "00:00");
		// echo "<pre>"; print_r($data); exit;
		if(empty($data))
			return 0;
		
		$return_array = array();
		foreach($data as $key => $value)
		{
			$return_array = array_keys($value);
			$return_array = str_replace("_"," ",$return_array);	
			$return_array = array_map('ucfirst', $return_array);
		}

		$result1 = array_merge(array(array_unique($return_array)), $data);
		
		// echo "<pre>"; print_r($result1); exit;
		return array($result1);
	}

	private function getTimeDetails($week_name,$date,$grade,$emp_id)
	{
		$loop =0;
		$sql = "SELECT verticals.name AS Vertical,employees.company_employ_id AS EmployID,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name`, 
		designation.hcm_grade AS Grade, designation.name AS Designation, shift_code AS Shift, 
		CONCAT(IFNULL(supervisor.`first_name`, ''),' ',IFNULL(supervisor.`last_name`, '')) AS `Manager L1`, 
		CONCAT(IFNULL(supervisor2.`first_name`, ''),' ',IFNULL(supervisor2.`last_name`, '')) AS `Manager L2`,
		CONCAT(IFNULL(supervisor3.`first_name`, ''),' ',IFNULL(supervisor3.`last_name`, '')) AS `Manager L3`, 
		DATE_FORMAT(week_date,'%d-%b-%Y') AS `Date of Entry`,DATE_FORMAT(week_date,'Week%U') AS `Week of Entry`,
		DATE_FORMAT(week_date,'%M') AS `Month of Entry`,client_name AS Client, task_name AS Task, sub_task_name AS SubTask, category_name AS TaskGroup,
		CASE 
		WHEN timesheet_task_mapping.client_id=221 && task_code_id IN(18) THEN 'Non Productive'
		WHEN timesheet_task_mapping.client_id=221 && task_code_id IN(12,23) THEN 'Personal'
		WHEN timesheet_task_mapping.client_id != 221 THEN 'Client Not-Billed'
		WHEN timesheet_task_mapping.client_id = 221 THEN 'Non Billable'
		END AS Billability,
		CASE 
		WHEN task_code_id IN(18) THEN 'Non Productive'
		WHEN task_code_id IN(12,23) THEN 'Personal'
		WHEN timesheet_task_mapping.client_id != 221 THEN 'Client Productive'
		WHEN timesheet_task_mapping.client_id = 221 THEN 'Theorem Productive'
		END AS TaskType,
		CASE week_name
		WHEN 'Mon' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day1))), '%H:%i')
		WHEN 'Tue' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day2))), '%H:%i')
		WHEN 'Wed' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day3))), '%H:%i')
		WHEN 'Thu' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day4))), '%H:%i')
		WHEN 'Fri' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day5))), '%H:%i')
		WHEN 'Sat' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day6))), '%H:%i')
		WHEN 'Sun' THEN TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day7))), '%H:%i')
		END AS Hours, 
		CASE timesheet_task_mapping.status
		WHEN '0' THEN 'Rejected'
		WHEN '1' THEN 'Pending'
		WHEN '2' THEN 'Approved'
		END AS Status, 
		CASE week_name
		WHEN 'Mon' THEN comment1
		WHEN 'Tue' THEN comment2
		WHEN 'Wed' THEN comment3
		WHEN 'Thu' THEN comment4
		WHEN 'Fri' THEN comment5
		WHEN 'Sat' THEN comment6
		WHEN 'Sun' THEN comment7
		END AS comments
		FROM `timesheet_task_mapping` 
		JOIN task_week ON task_week.week_code_id = timesheet_task_mapping.task_week_id
		JOIN timesheet_task_codes ON timesheet_task_codes.timesheet_task_code_id = timesheet_task_mapping.task_code_id
		LEFT JOIN timesheet_subtasks ON timesheet_subtasks.timesheet_subtask_id = timesheet_task_mapping.sub_task_id
		JOIN client ON client.client_id = timesheet_task_mapping.client_id
		JOIN `employees` ON employees.employee_id = timesheet_task_mapping.emp_id
		JOIN `shift` ON shift.shift_id = employees.shift_id
		JOIN `employee_vertical` ON employee_vertical.employee_id = employees.employee_id
		JOIN `verticals` ON verticals.vertical_id = employee_vertical.vertical_id
		JOIN `designation` ON `designation`.`designation_id` = employees.designation_id
		JOIN `employees` supervisor ON `supervisor`.`employee_id` = employees.primary_lead_id
		JOIN `employees` supervisor2 ON `supervisor2`.`employee_id` = supervisor.primary_lead_id
		JOIN `employees` supervisor3 ON `supervisor3`.`employee_id` = supervisor2.primary_lead_id
		JOIN timesheet_categories ON `timesheet_categories`.`category_id` = timesheet_task_mapping.category_id
		WHERE employees.status!=6 AND week_date = '".$date."' ";
		if($grade>2)
		{
			$empIds =  array_unique (parent::getEmployeeList($emp_id));
			$empIds = implode(",", $empIds);
			if($empIds!="")
			{
				$empIds .= ",".$emp_id;
			}
			else
			{
				$empIds = $emp_id;
			}
			
			$sql .= " AND timesheet_task_mapping.emp_id IN (".$empIds.")";
		}
		else
		{
			$sql .= " AND timesheet_task_mapping.emp_id ='".$emp_id."'";
		}
		
		$sql .= " GROUP BY employees.employee_id,timesheet_task_mapping.client_id,timesheet_task_code_id,sub_task_id";

		$data = array();
		// echo $sql; exit;
		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;
	}
	
	private function getProdutionDetails($date,$grade,$emp_id)
	{
		$loop =0;
		$sql = "SELECT verticals.name AS Vertical,employees.company_employ_id AS EmployID,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name`,
		designation.hcm_grade AS Grade, designation.name AS Designation, shift_code AS Shift, 
		CONCAT(IFNULL(supervisor.`first_name`, ''),' ',IFNULL(supervisor.`last_name`, '')) AS `Manager L1`, 
		CONCAT(IFNULL(supervisor2.`first_name`, ''),' ',IFNULL(supervisor2.`last_name`, '')) AS `Manager L2`,
		CONCAT(IFNULL(supervisor3.`first_name`, ''),' ',IFNULL(supervisor3.`last_name`, '')) AS `Manager L3`,
		DATE_FORMAT(task_date,'%d-%b-%Y') AS `Date of Entry`,DATE_FORMAT(task_date,'Week%U') AS `Week of Entry`,
		DATE_FORMAT(task_date,'%M') AS `Month of Entry`, client_name AS Client,
		'Production Work' AS Task, '' AS SubTask, 'Client Billable' AS Billability, 'Client Mgmt.' AS TaskGroup, 'Client Productive' AS TaskType,
		TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') AS Hours, 'Approved' AS Status, '' AS Comments
		FROM `utilization` 
		JOIN client ON client.client_id = utilization.fk_client_id
		JOIN `employees` ON employees.employee_id = utilization.employee_id
		JOIN `shift` ON shift.shift_id = employees.shift_id
		JOIN `employee_vertical` ON (employee_vertical.`employee_id` = employees.`employee_id`)
		JOIN `verticals` ON (verticals.`vertical_id` = employee_vertical.`vertical_id`)
		JOIN `designation` ON `designation`.`designation_id` = employees.designation_id
		JOIN `employees` supervisor ON `supervisor`.`employee_id` = employees.primary_lead_id
		JOIN `employees` supervisor2 ON `supervisor2`.`employee_id` = supervisor.primary_lead_id
		JOIN `employees` supervisor3 ON `supervisor3`.`employee_id` = supervisor2.primary_lead_id
		WHERE task_date = '".$date."' AND utilization.status NOT IN(0,2)";
		if($grade>2)
		{
			$empIds =  array_unique (parent::getEmployeeList($emp_id));
			$empIds = implode(",", $empIds);
			if($empIds!="")
			{
				$empIds .= ",".$emp_id;
			}
			else
			{
				$empIds = $emp_id;
			}
			
			$sql .= " AND utilization.employee_id IN (".$empIds.")";
		}
		else
		{
			$sql .= " AND utilization.employee_id ='".$emp_id."'";
		}
		$sql .= " GROUP BY employees.employee_id,utilization.fk_client_id";

		$data = array();
		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;
	}

	public function duplicateRow()
	{
		// debug($_POST);exit;
		$row_id = $this->input->post('timesheetID');
		$week_id = $this->input->post('duplicateTo');
		$empData = $this->input->cookie();
		$emp_id  = ($this->input->post('teamMember')!='') ? $this->input->post('teamMember') : $empData['employee_id'];

		$timesheet_task_id = explode(',',$row_id);

		$id_length = count($timesheet_task_id);

		$affectedRows = $recordExists = 0;
		$dulicatedRecords = array();

		foreach ($timesheet_task_id as $key => $tt_id) 
		{

			$row_data_sql = "SELECT * FROM onshore_time_entry WHERE timesheet_task_id = '$tt_id'";
			$row_data = $this->db->query($row_data_sql)->row_array();

			$client_id = $row_data['client_id'];
			$task_code_id = $row_data['task_code_id'];
			$service_id = $row_data['service_type_id'];
			$engagement_id = $row_data['engagement_id'];
			$role_id = $row_data['role_id'];

			$check_sql = "SELECT * FROM onshore_time_entry 
			WHERE task_week_id = '$week_id'
			AND client_id = '$client_id'
			AND task_code_id = '$task_code_id'
			AND service_type_id = '$service_id'
			AND engagement_id = '$engagement_id'
			AND role_id = '$role_id'
			AND emp_id = '$emp_id'";
			if($this->db->query($check_sql)->num_rows()>0)
				$recordExists++;

			if($recordExists == 0)
			{
				$row_data['task_week_id'] = $week_id;
				$row_data['status'] = 1;
				$row_data['created_by'] = $emp_id;
				unset($row_data['timesheet_task_id']);
				unset($row_data['created_on']);
				unset($row_data['updated_on']);
				unset($row_data['updated_by']);
				unset($row_data['approved_on']);
				unset($row_data['approved_by']);

				$dulicatedRecords[] = $row_data;
			}
			else
			{
				return -1;
			}
		}

		if(count($dulicatedRecords)>0)
		{
			foreach ($dulicatedRecords as $key => $row_values) {
				$this->db->insert('onshore_time_entry',$row_values);
				if($this->db->affected_rows()>0)
				{
					$affectedRows++;
				}
			}
		}

		return $affectedRows;
	}
}
?>