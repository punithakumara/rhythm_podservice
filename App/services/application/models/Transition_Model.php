<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transition_Model extends INET_Model 
{
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Audit_Log_Model');
	}
	
	function getTransition()
	{
		$options = array();
		$emp_id_name 		= ($this->input->get('emp_id_name')) ? $this->input->get('emp_id_name') : '';
		$client_id 			= ($this->input->get('client_id')) ? $this->input->get('client_id') : '';
		$assigned_vertical 	= ($this->input->get('assigned_vertical'))? $this->input->get('assigned_vertical') : '';
		$status 			= ($this->input->get('status')) ? $this->input->get('status') : '';
		$TrStatus 			= ($this->input->get('TrStatus')) ? $this->input->get('TrStatus') : '';
		$reportingID 		= ($this->input->get('reportingID')) ? $this->input->get('reportingID') : '';
		$grades 			= ($this->input->get('Grades')) ? $this->input->get('Grades') : '';
		
		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;
		
		$sort = json_decode($this->input->get('sort'),true);
		if($sort[0]['property']=="FullName")
		{
			$options['sortBy'] = "e.first_name";
		}
		else if($sort[0]['property']=="VNameOld") 
		{
			$options['sortBy'] = "v.Name";
		}
		else 
		{
			$options['sortBy'] = $sort[0]['property'];
		}
		$options['sortDirection'] = $sort[0]['direction'];

		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}

		$sql = "SELECT e.employee_id,e.primary_lead_id, CAST(SUBSTRING_INDEX(e.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, e.company_employ_id,e.first_name, e.last_name, e.Email, e2.first_name AS reportingFirstName, e2.last_name AS reportingLastName, e.Mobile, d.grades, d.Name, e.training_status,  IF(e.status = '7', 1, 0) AS training_needed, ev.vertical_id, v.Name AS VName FROM employees e";
		$sql .=" JOIN employee_vertical ev ON ev.employee_id = e.employee_id ";
		$sql .=" JOIN verticals v ON ev.vertical_id = v.vertical_id ";
		if($client_id !="")
		{
			$sql .=" LEFT JOIN employee_client ON e.employee_id = employee_client.employee_id ";
		}
		
		$sql .= " LEFT JOIN employees e2 ON e.primary_lead_id = e2.employee_id ";
		$sql .= " JOIN designation d ON e.designation_id = d.designation_id ";

		if($emp_id_name !="")
		{
			$sql .=' AND (e.first_name LIKE "%'.$emp_id_name.'%" OR e.last_name LIKE "%'.$emp_id_name.'%" OR e.company_employ_id LIKE "%'.$emp_id_name.'%" ) ';
		}
		
		if($client_id !="")
		{
			$sql .=" AND client_id = ".$client_id;
		}
		
		if($reportingID !="")
		{
			$sql .=" AND e.primary_lead_id = ".$reportingID;
		}
		
		if($assigned_vertical !="")
		{
			$sql .=" AND ev.vertical_id = ".$assigned_vertical;
		}
		
		if($TrStatus !="")
		{
			$sql .=" AND e.training_status = ".$TrStatus;
		}
		
		$sql .=" AND d.grades < 5";

		if($status !="")
		{
			$sql .=" AND e.status IN(".$status.")";
		}
		else
		{
			$sql .= ' WHERE e.status IN (3,5,7)';
		}
		
		$sql .=" GROUP BY e.employee_id";
		$sql .= " ORDER BY ".$options['sortBy']." ".$options['sortDirection'];

		$totQuery = $this->db->query($sql);
		// echo $this->db->last_query(); exit;
		$totCount = $totQuery->num_rows();

		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " LIMIT ".$limit;
		}
		else if(isset($options['limit'])) 
		{
			$sql .= " LIMIT ".$options['limit'];
		}
		$query = $this->db->query($sql);
		$data = $query->result_array();

		$results = array_merge(array('totalCount' => $totCount),array('data' => $data));
		
		return $results;
	}
	
	/**
	 * Function for pushing employee to hr pool.
	 * 
	 * @return number
	 */
	function pushHrPool()
	{
		$retVal = -1; $resVal = 1;
		$tarVertId		 	= ($this->input->get('TargetVerticalID')) ? $this->input->get('TargetVerticalID') : -1;
		$transitionComment 	= ($this->input->get('TransitionComment')) ? $this->input->get('TransitionComment') : "";
		$tr_needed 			= ($this->input->get('TrainingNeeded')!="") ? $this->input->get('TrainingNeeded') : -1;
		$transStatus 		= ($this->input->get('hrPoolStatus')) ? $this->input->get('hrPoolStatus') : -1;
		$employID 			= ($this->input->get('EmployID')) ? $this->input->get('EmployID') : '';
		$grade 				= $this->input->get('Grade') ? $this->input->get('Grade') : '';
		$clientID 			= ($this->input->get('ClientID')) ? $this->input->get('ClientID') : -1;
		$AssignLeads 		= ($this->input->get('AssignLeads')) ? $this->input->get('AssignLeads') : -1;
		$release_date        = ($this->input->get('release_date'))? $this->input->get('release_date') : -1;
		
		//echo"<pre>"; print_r($transStatus); exit;

		if($employID != "")
		{
			$resVal = $this->updateHrPoolDetails($employID, $tarVertId, $tr_needed, $grade, $transStatus, $transitionComment, $clientID,$AssignLeads,$release_date);

			if($resVal)
			{
				$retVal = 1;
			}
			
			return $retVal;
		}
	}

	
	/**
	* Function for updating employee details to HR Pool.
	* 
	* @param int $employID
	* @param int $tarVertId
	* @param int $tr_needed
	* @param int $Grade
	* @param int $clientID
	* @param string $transitionComment
	* @return number
	*/
	function updateHrPoolDetails($employID, $tarVertId, $tr_needed, $Grade=-1, $transStatus, $transitionComment = "", $clientID = '', $AssignLeads='',$release_date) 
	{

		$result  = 0;

		$empResult = $this->updateEmployeeData($employID, $tarVertId, $tr_needed, $transStatus, $transitionComment, $release_date);
		
		if($empResult)
			$result  = 1;

		return $result ;
	}
	
	
	/**
	 * Function to update employees table information based on employee id
	 * 
	 * @param string $employIDStr
	 * @param int $tarVertId
	 * @param int $tr_needed
	 * @param int $primaryLeadID
	 * @param int $transStatus
	 * @param string $transitionComment
	 * @return int
	 */
	function updateEmployeeData($employIDStr, $tarVertId = -1, $tr_needed = -1, $transStatus, $transitionComment="", $release_date)
	{
		$empData    = $this->input->cookie();
		$emp_id		= $empData['employee_id'];
		$result = 0;
		$employIDArr = explode(";", $employIDStr);
		//echo "<pre>"; print_r($transStatus); exit;
		foreach($employIDArr AS $list)
		{
			$primary_lead_id = $this->getPrimaryLeadID($list);
			$empRelInfo = $this->empRelInfo($list);
			$clientID = $this->getClientIDforLeadTransition($list);
			$clientID = isset($clientID['client_id'])?$clientID['client_id']:"";
			$primary_lead_id = isset($primary_lead_id['primary_lead_id'])?$primary_lead_id['primary_lead_id']:"";

			if($list != '')
			{									
				if($list != -1) 
				{
					$this->db->set('employee_id', $list);
				}
				if($release_date != -1) 
				{
					$this->db->set('release_date',$release_date);
				}
				if($transitionComment != "")
				{
					$this->db->set('comments', $transitionComment);				
				}
				if($transStatus != -1)
				{
					$this->db->set('type', $transStatus);
				}
				$this->db->set('created_by', $emp_id);
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->insert('employee_rmg_history');
				
				$result = 1;
			}
			
			if($transStatus != -1) 
			{
				$this->db->set('status', $transStatus);
			}
			if($tr_needed > 0)
			{
				$this->db->set('status', 7);
				
				if($tr_needed == 1)
				{
					$this->db->set('training_status', '1');
				}
				else	
				{
					$this->db->set('training_status', '2');
				}
			}
			
			if($transitionComment != "")
			{
				$this->db->set('comments', $transitionComment);				
			}
			$this->db->where_in('employee_id', $employIDArr);
			$resVal = $this->db->update('employees');
			
			$result = 1;

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been released employee to RMG(Employee ID:'.$list.').';
			$this->userLogs($logmsg);	
		}

		return $result;			
	}

	function HRPoolTransition()
	{
		$retVal = -1; $resVal = 1;
		$transitionComment 	= ($this->input->get('TransitionComment')) ? $this->input->get('TransitionComment') : "";
		$transitionPod 	= ($this->input->get('transitionPod')) ? $this->input->get('transitionPod') : "";
		$transitionService 	= ($this->input->get('transitionService')) ? $this->input->get('transitionService') : "";
		$transitionSupervisor 	= ($this->input->get('transitionSupervisor')) ? $this->input->get('transitionSupervisor') : "";
		$transitionShift 	= ($this->input->get('transitionShift')) ? $this->input->get('transitionShift') : "";
		$transDate 		= ($this->input->get('transitionDate')) ? $this->input->get('transitionDate') : "";
		$employID 			= ($this->input->get('EmployID')) ? $this->input->get('EmployID') : '';
		$grade 				= $this->input->get('Grade') ? $this->input->get('Grade') : '';
		$clientID 			= ($this->input->get('ClientID')) ? $this->input->get('ClientID') : -1;
		$AssignLeads 		= ($this->input->get('AssignLeads')) ? $this->input->get('AssignLeads') : -1;
		// $assign_team_lead 	= ($this->input->get('assign_team_lead')) ? $this->input->get('assign_team_lead') : "";

		//echo"<pre>"; print_r($transDate); exit;

		if($employID != "")
		{
			$resVal = $this->updatePullFromHRDetails($employID, $transitionPod, $transitionService, $transitionSupervisor, $transitionShift, $grade, $transDate, $transitionComment, $clientID, $AssignLeads);

			if($resVal)
			{
				$retVal = 1;
			}
			
			return $retVal;
		}
	}

	function updatePullFromHRDetails($employID, $transitionPod, $transitionService, $transitionSupervisor, $transitionShift, $Grade=-1, $transDate, $transitionComment = "", $clientID = '')
	{
		$result  = 0;

		$empResult = $this->updatePullFromHRData($employID,  $transitionPod, $transitionService, $transitionSupervisor, $transitionShift, $transDate, $transitionComment,$Grade);

		if($empResult)
			$result  = 1;

		return $result ;
	}

	function updatePullFromHRData($employIDStr,  $transitionPod, $transitionService, $transitionSupervisor, $transitionShift, $transDate, $transitionComment, $grade)
	{
		$empData    = $this->input->cookie();
		$emp_id		= $empData['employee_id'];
		$result = 0;
		$employIDArr = explode(";", $employIDStr);
		foreach($employIDArr AS $list)
		{
			$from_reporting = $this->getPrimaryLeadID($list);
			$from_pod_id = $this->getPodID($list);
			$from_service_id = $this->getServiceID($list);
			$clientID = $this->getClientIDforLeadTransition($list);
			$clientID = $clientID['client_id'];

			//Assign reportees to Other TL/AM
			if($grade>=3)
			{
				// Update reportees primary_lead_id
				$SQL = "UPDATE employees SET primary_lead_id = '".$from_reporting['primary_lead_id']."' WHERE primary_lead_id = $list";
				$this->db->query($SQL);

				$getEmployeeDetails = $this->getEmployeeDetails($list);

				$notification_data   = array(
					'Title' 			=> '',
					'Description' 		=> "There are members reassigned to you due to the transition of <b>".$getEmployeeDetails['Name']."</b>. Please take necessary actions.",
					'SenderEmployee' 	=> $empData['employee_id'],
					'Type' 				=> '5',
					'ReceiverEmployee' 	=> $from_reporting['primary_lead_id'],
					'URL' 				=> ''
				);

				$this->addNotification($notification_data);
			}

			//Removing client history
			if($list!="")
			{
				$sql_resources ="DELETE FROM employee_client WHERE employee_id = ".$list."";					
				$this->db->query($sql_resources);
			}

			//Update employee_client_history table to track employee_client history
			if($list != '')
			{
				$SQL = "UPDATE employee_client_history SET end_date = '".$transDate."', updated_by= '".$empData['employee_id']."', updated_date = '".date('Y-m-d H:i:s')."' 
				WHERE (end_date = 0000-00-00 OR end_date IS NULL OR end_date > 0)  AND employee_id IN (".$list.") AND client_id IN ('".$clientID."') ";
				
				$this->db->query($SQL);
			}

			//Updating billability history
			if($list != '')
			{
				$SQL = "UPDATE employee_billabality_history SET end_date = '".date('Y-m-d H:i:s')."' 
				WHERE end_date IS NULL AND employee_id IN (".$list.") ";
				
				$this->db->query($SQL);
			}

			//Updating employee details
			if($transitionSupervisor != -1) 
			{
				$this->db->set('primary_lead_id', $transitionSupervisor);
				$this->db->set('shift_id', $transitionShift);
				$this->db->set('status', 3);
				$this->db->set('service_id', $transitionService);
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->update('employees');

				$this->db->set('pod_id', $transitionPod);
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->update('employee_pod');

				$result = 1;
			}

			//Update employee_shift table to track shift history
			if($list != '')
			{
				$SQL = "UPDATE emp_shift SET to_date = '".$transDate."', comments = '".$transitionComment."', updated_by= '".$empData['employee_id']."', updated_on = '".date('Y-m-d H:i:s')."' 
				WHERE (to_date = 0000-00-00 OR to_date IS NULL OR to_date > 0)  AND employee_id IN (".$list.") ";
				
				$this->db->query($SQL);
			}
			

			if($employIDStr != '')
			{
				$data = array(
					'employee_id' => $list ,
					'client_id' => isset($clientID['client_id'])?$clientID['client_id']:0 ,
					'from_date' => $transDate,
					'shift_id' => $transitionShift
				);
				$resVal= $this->db->insert('emp_shift', $data);

				$result = 1;
			}

			//Updates employee_rmg_history
			if($employIDStr != '')
			{
				$data = array(
					'employee_id' => $list ,
					'release_date' => $transDate,
					'type' => 3,
					'comments' => $transitionComment,
					'created_by' => $empData['employee_id'],
				);
				$resVal= $this->db->insert('employee_rmg_history', $data);

				$result = 1;
			}

			//Updates employee_vertical_history
			$query = "SELECT * from employee_pod_history WHERE employee_id = ".$list."";
			$result = $this->db->query($query);
			if ($result->num_rows () > 0) 
			{
				if($list != '')
				{
					$SQL = "UPDATE employee_pod_history SET end_date = '".$transDate."', comments = '".$transitionComment."', updated_by = '".$empData['employee_id']."', updated_on = '".date('Y-m-d H:i:s')."' 
					WHERE (end_date = 0000-00-00 OR end_date IS NULL OR end_date > 0) AND employee_id IN (".$list.") ";
					
					$this->db->query($SQL);
				}
			}

			if($employIDStr != '')
			{
				$data = array(
					'employee_id' => $list ,
					'pod_id' => $transitionPod,
					'start_date' => $transDate,
					'created_by' => $empData['employee_id'],
				);
				$resVal= $this->db->insert('employee_pod_history', $data);

				$result = 1;
			}

			
			if($employIDStr != -1) 
			{
				$this->db->set('employee_id', $list);
			}
			if($from_pod_id != -1) 
			{
				$this->db->set('from_pod_id', $from_pod_id['pod_id']);
			}
			if($clientID != -1) 
			{
				$this->db->set('from_client_id', isset($clientID['client_id'])?$clientID['client_id']:0);
			}
			if($from_reporting != -1) 
			{
				$this->db->set('from_reporting', $from_reporting['primary_lead_id']);
			}
			if($transitionPod != -1) 
			{
				$this->db->set('to_pod_id', $transitionPod);
			}
			if($clientID != -1) 
			{
				$this->db->set('to_client_id', isset($clientID['client_id'])?$clientID['client_id']:0);
			}
			if($transitionSupervisor != "")
			{
				$this->db->set('to_reporting', $transitionSupervisor);				
			}
			if($transDate != -1) 
			{
				$this->db->set('transition_date', $transDate);
			}
			if($transitionComment != -1) 
			{
				$this->db->set('comment', $transitionComment);
			}
			$this->db->set('modified_by', $emp_id);
			$this->db->where_in('employee_id', $list);
			$resVal = $this->db->insert('employee_transition');

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been pulled employee from RMG(Employee ID:'.$list.').';
			$this->userLogs($logmsg);

			$result = 1;
		}
	}
	
	
	function shiftTransition()
	{
		$retVal = -1; $resVal = 1;
		$transitionComment 	= ($this->input->get('TransitionShiftComment')) ? $this->input->get('TransitionShiftComment') : "";
		$transitionShift 	= ($this->input->get('transitionShift')) ? $this->input->get('transitionShift') : "";
		$transDate 		= ($this->input->get('transitionShiftDate')) ? $this->input->get('transitionShiftDate') : "";
		$employID 			= ($this->input->get('EmployID')) ? $this->input->get('EmployID') : '';
		$grade 				= $this->input->get('Grade') ? $this->input->get('Grade') : '';
		$clientID 			= ($this->input->get('ClientID')) ? $this->input->get('ClientID') : -1;
		$AssignLeads 		= ($this->input->get('AssignLeads')) ? $this->input->get('AssignLeads') : -1;
		
		if($employID != "")
		{
			$resVal = $this->updateShiftTransDetails($employID, $transitionShift, $grade, $transDate, $transitionComment, $clientID,$AssignLeads);

			if($resVal)
			{
				$retVal = 1;
			}
			
			return $retVal;
		}
	}
	
	function updateShiftTransDetails($employID, $transitionShift, $Grade=-1, $transDate, $transitionComment = "", $clientID = '', $AssignLeads='') 
	{
		$result  = 0;
		//$employIDStr = is_array($employID)? implode(",", $employID) : $employID ;
		$empResult = $this->updateData($employID, $transitionShift, $transDate, $transitionComment);
		
		if($empResult)
			$result  = 1;

		return $result ;
	}
	
	function updateData($employIDStr, $transitionShift, $transDate, $transitionComment)
	{
		$empData    = $this->input->cookie();
		$result = 0;
		$employIDArr = explode(";", $employIDStr);
		
		foreach($employIDArr AS $list)
		{
			//echo "<pre>"; print_r($list); exit;
			$primary_lead_id = $this->getPrimaryLeadID($list);
			$clientID = $this->getClientIDforLeadTransition($list);

			if($transitionShift != -1) 
			{
				$this->db->set('shift_id', $transitionShift);
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->update('employees');

				$result = 1;
			}

			$query = "SELECT * from emp_shift WHERE employee_id = ".$list."";
			$result = $this->db->query($query);

			if ($result->num_rows () > 0) 
			{
				if($list != '')
				{
					$SQL = "UPDATE emp_shift SET to_date = '".$transDate."', comments = '".$transitionComment."', updated_by= '".$empData['employee_id']."', updated_on = '".date('Y-m-d H:i:s')."' 
					WHERE (to_date IS NULL) AND employee_id IN (".$list.") ";
					
					$this->db->query($SQL);
				}
			}

			if($list != '')
			{									
				if($list != -1) 
				{
					$this->db->set('employee_id', $list);
				}
				if($clientID != -1) 
				{
					$this->db->set('client_id', isset($clientID['client_id'])?$clientID['client_id']:0);
				}
				if($transDate != -1) 
				{
					$this->db->set('from_date', $transDate);
				}
				if($transitionShift != "")
				{
					$this->db->set('shift_id', $transitionShift);				
				}
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->insert('emp_shift');
				
				$result = 1;
			}	
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee(Employee ID:'.$list.') shift(Shift ID:'.$transitionShift.').';
			$this->userLogs($logmsg);

		}

		return $result;			
	}

	/**
	 * Function to get employee information based on employee id
	 * 
	 * @param string $empID
	 * @param string $clientID
	 * @return string
	 */
	function empRelInfo($empID = "", $clientID = "") 
	{
		$sql = "SELECT employee_client.client_id, employee_vertical.vertical_id AS verticalID 
		FROM employees e 
		LEFT JOIN employee_client ON employee_client.employee_id = e.employee_id 
		LEFT JOIN employee_vertical ON employee_vertical.employee_id = e.employee_id
		WHERE e.employee_id = " . $empID;

		$query = $this->db->query($sql);
		
		if ($query->num_rows () > 0) 
		{
			return $query->result_array ();
		}
		
		return "";
	}
	
	/**
	 * Function to update employee client table information based on employee id
	 * 
	 * @param string $employIDStr
	 * @param int $clientID
	 * @param int $primaryLeadID
	 * @param int $grade
	 * @return int
	 */
	function updateEmployeeProcessData($employIDStr, $primaryLeadID = -1)
	{
		$employIDStr = str_replace(";", "','", $employIDStr);
		if($employIDStr != '')
		{
			$SQL = 'UPDATE employee_client SET';
			if($primaryLeadID != -1)
			{
				$SQL .=' primary_lead_id ='.$primaryLeadID;
			}
			$SQL .= " WHERE employee_id IN ('".$employIDStr."')";
			
			$resVal = $this->db->query($SQL);	
			
			$result = 1;
		}
		return $result;
	}
	
	
	/**
	 * Function for adding employee to the process.
	 * 
	 * @param int $employID
	 * @param int $clientID
	 * @param int $grades
	 * @param int $primary_lead_id
	 * @return number
	 */
	function AddEmployProcessData($employID, $clientID, $grades=-1, $primary_lead_id = -1, $transStatus=-1, $WorID, $ProjectID=-1, $transDate) 
	{
		$result = 0;
		$employIds = explode(";", $employID);
		$empData    = $this->input->cookie();

		for($ke=0;$ke < count($employIds);$ke++)
		{
			//Checking whether employee has process already or not, to avoid duplicate entry.
			$isExists = $this->isEmpHasPro($employIds[$ke], $clientID, $WorID, $ProjectID);
			
			$primary_lead_id = $this->getPrimaryLeadID($employIds[$ke]);
			$employee_grade = $this->getEmployeeGrade($employIds[$ke]);		

			if(!$isExists)
			{
				$existing_array['employee_id'] = $employIds[$ke];
				$existing_array['wor_id'] = $WorID;
				if ($clientID != - 1) 
				{
					$existing_array['client_id'] = $clientID;
				}
				if ($ProjectID != - 1) 
				{
					$existing_array['project_type_id'] = $ProjectID;
				}
				
				if($transStatus == 2 || $transStatus == 4)
				{
					$existing_array['approved'] = 1;
				} 
				else 
				{
					$existing_array['approved'] = 0;
				}
				
				if ($grades != - 1) 
				{
					$existing_array['grades'] = $employee_grade['grades'];
				}
				
				if($primary_lead_id != -1)
				{
					$existing_array['primary_lead_id'] = $primary_lead_id['primary_lead_id'];
				}
				// echo "<pre>"; print_r($existing_array); exit;
				$retVal = $this->db->insert ( 'employee_client', $existing_array );				
			}
			
			if(!$isExists)
			{
				$exist_array['employee_id'] = $employIds[$ke];
				
				if ($clientID != - 1) 
				{
					$exist_array['client_id'] = $clientID;
				}
				
				if ($transDate != - 1) 
				{
					$exist_array['start_date'] = $transDate;
				}
				
				$exist_array['wor_id'] = $WorID;
				$exist_array['process_id'] = $ProjectID;
				$exist_array['created_date'] = date("Y-m-d H:i:s");
				$exist_array['created_by'] =   $empData['employee_id'];
				
				
				$retVal = $this->db->insert ( 'employee_client_history', $exist_array );				
			}
			$result = 1;
		}
		return $result;
	}
	
	
	/**
	 * Function to delete employees process data based on employid and clientID
	 * 
	 * @param int $employID
	 * @param int $clientID
	 * $return boolean
	 */
	function DelEmployProcessData($employID, $clientID = -1, $team_lead = -1, $updateTeamMembers = -1) 
	{
		$employIDs = explode(",", $employID);
		$empProcessResult = 1;
		
		if($updateTeamMembers > 2)
		{
			$empProcessResult = $this->updateTeamLeadMembersPrimaryLead($employID, $clientID);
		}
		
		if($clientID != - 1)
		{
			$this->db->where_in('employee_id', $employIDs);
			$this->db->where('client_id', $clientID);
		}
		else
		{
			$this->db->where_in('employee_id', $employIDs); 
		}
		$retVal = $this->db->delete ( 'employee_client' );
		return $empProcessResult;
	}

	
	/**
	 * Funtion to get primary lead of an employee based on employees id
	 * 
	 * @param int $empID
	 * @return string
	 */
	function getPrimaryLeadID($empID = -1) 
	{
		$sql = "SELECT primary_lead_id FROM employees WHERE employee_id = " . $empID;
		
		$query = $this->db->query ( $sql );
		
		if ($query->num_rows () > 0) 
		{
			return $query->row_array ();
		}
		
		return "";
	}

	/**
	 * Funtion to get assigned vertical of an employee based on employees id
	 * 
	 * @param int $empID
	 * @return string
	 */
	function getPodID($empID = -1) 
	{
		$sql = "SELECT pod_id FROM employee_pod WHERE employee_id = " . $empID;
		
		$query = $this->db->query ( $sql );
		
		if ($query->num_rows () > 0) 
		{
			return $query->row_array ();
		}
		
		return "";
	}

	function getServiceID($empID = -1) 
	{
		$sql = "SELECT service_id FROM employees WHERE employee_id = " . $empID;
		
		$query = $this->db->query ( $sql );
		
		if ($query->num_rows () > 0) 
		{
			return $query->row_array ();
		}
		
		return "";
	}

	/**
	 * Funtion to get assigned vertical of an employee based on employees id
	 * 
	 * @param int $empID
	 * @return string
	 */
	function getClientIDforLeadTransition($empID = -1) 
	{
		$sql = 'SELECT GROUP_CONCAT(client_id SEPARATOR "\',\'") AS client_id FROM employee_client WHERE employee_id = "'.$empID.'"';
		//echo $sql;
		$query = $this->db->query ( $sql );

		$result = $query->row_array();
		
		return $result;
	}
	
	function getClientID($empID = -1) 
	{
		$sql = "SELECT client_id FROM emp_shift WHERE employee_id = '". $empID."' AND to_date = '0000-00-00'  ";
		
		$query = $this->db->query ( $sql );
		
		if ($query->num_rows () > 0) 
		{
			return $query->row_array ();
		}
		
		return "";
	}
	
	/**
	 * Funtion to get grade employee based on employees id
	 * 
	 * @param int $empID
	 * @return string
	 */
	function getEmployeeGrade($empID = -1) 
	{
		$sql = "SELECT grades 
		FROM designation d
		LEFT JOIN employees e ON e.designation_id = d.designation_id
		WHERE e.employee_id =" . $empID;
		
		$query = $this->db->query ( $sql );
		
		if ($query->num_rows () > 0) 
		{
			return $query->row_array ();
		}
		
		return "";
	}
	
	
	/**
	* Function to update team lead members primary lead id
	* 
	* @param int $empID
	* @param int $proID
	* @return int
	*/
	function updateTeamLeadMembersPrimaryLead($empID, $clientID)
	{
		$primary_lead = $this->getPrimaryLeadID($empID);
		
		$SQL = "UPDATE employees SET primary_lead_id = ".$primary_lead['primary_lead_id']." WHERE primary_lead_id = ".$empID;
		$resVal = $this->db->query($SQL);
		
		$SQL = "UPDATE employee_client SET primary_lead_id = ".$primary_lead['primary_lead_id']." WHERE primary_lead_id = ".$empID;
		$resVal = $this->db->query($SQL);
		
		return 	$resVal;
	}
	
	
	/**
	* Checking whether employee has process already or not, to avoid duplicate entry
	* 
	* @param int $empID
	* @return string
	*/
	function isEmpHasPro($empID, $clntID = -1, $WorID=-1, $ProjectID=-1) 
	{	
		$sql = "SELECT DISTINCT(employee_id) FROM employee_client WHERE employee_id IN (".$empID.")";
		
		if($clntID != -1)
		{
			$sql .=" AND client_id = ".$clntID;
		}
		if($WorID != -1)
		{
			$sql .=" AND wor_id = ".$clntID;
		}
		if($ProjectID != -1)
		{
			$sql .=" AND project_type_id = ".$ProjectID;
		}
		$query = $this->db->query($sql);

		if($query->num_rows() > 0) 
		{
			return true;
		}
		
		return false;	
	}
	
	
	/**
	 * Function for updating employee billability log.
	 * 
	 * @param array $employIDStr
	 * @return number
	 */
	function updateEmployeeBillabilityData($employIDStr,$clientID)
	{
		$result = 0;
		$empData = $this->input->cookie();
		$modfdId = $empData['employee_id'];
		if($employIDStr != '')
		{
			for($i=0;$i<count($employIDStr);$i++)
			{
				$this->Audit_Log_Model->employBillabilityLog($employIDStr[$i],$clientID,"","","",date('d-m-Y'),$modfdId,date('d-m-Y'),1);
			}			
			$result =1;
		}
		return $result;
	}
	
	/**
	 * Function to get employees process count based on employid
	 * 
	 * @return number
	 */
	function getProcessCount()
	{
		if($this->input->get('EmployID'))
		{
			$sql = "SELECT COUNT(*) FROM `employee_client` WHERE employee_id = '".$this->input->get('EmployID')."' HAVING COUNT(*) > 1";
			$query = $this->db->query($sql);
			
			return $query->num_rows();
		}
		else 
		{
			return 0;
		}
	}

	
	/**
	 * Function for Add Employee to Multiple Process, Add TL to Multiple Process & Add AM to Multiple Process.
	 * 
	 * @return number
	 */
	function addEmpProTrans() 
	{
		$employID   	= ($this->input->get('EmployID')) ? $this->input->get('EmployID') : '';
		$Grade      	= ($this->input->get('Grade')) ? $this->input->get('Grade') : '';
		$clientID   	= ($this->input->get('ClientID')) ? $this->input->get('ClientID') : '';
		$ProjectID   	= ($this->input->get('ProjectID')) ? $this->input->get('ProjectID') : '';
		$WorID   		= ($this->input->get('WorID')) ? $this->input->get('WorID') : '';
		$TransitionDate = ($this->input->get('ClientID')) ? $this->input->get('TransitionDate') : '';
		$res = 0;
		
		/* Audit Log */
		$employIds = explode(";", $employID);
		foreach($employIds as $empID)
		{		
			$primary_lead_id = $this->getPrimaryLeadID($empID);		

			$empData = $this->input->cookie();
			$modfdId = $empData['employee_id'];
			$empRelInfo = $this->empRelInfo($empID);
			$fromVert = $empRelInfo[0]['verticalID'];
			$fromClient = null;
			$fromReporting = isset($primary_lead_id['primary_lead_id'])?$primary_lead_id['primary_lead_id']:'';
			$toVert = $empRelInfo[0]['verticalID'];
			$toClient = $clientID;
			$toReporting = $primary_lead_id['primary_lead_id'];
			$empTrnCnt = 0;
			$transDate = $TransitionDate;
			
			$this->Audit_Log_Model->employTransitionLog($empID, $transDate, $modfdId, $fromVert, $fromClient, $fromReporting, $toVert, $toClient, $toReporting, 1, "Client Added");
		}
		/* End Of Audit Log*/
		
		if($employID!="")
		{
			$res = $this->AddEmployProcessData($employID, $clientID, $Grade, $toReporting, -1, $WorID, $ProjectID, $transDate);
		}

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$WorID.') has been assigned employee(EmpId:'.$employID.')';
		$this->userLogs($logmsg);
		
		return $res;	
	}
	
	
	/**
	 * Function for Remove Employee from Multiple Process.
	 * 
	 * @return number
	 */
	function empRmveTransition() 
	{
		$retVal = -1;
		$res = 0;
		$ClientID = ($this->input->get('ClientID'))?$this->input->get('ClientID'): "";
		$employID = ($this->input->get('EmployID'))?$this->input->get('EmployID'):'';
		$Grade = $this->input->get('Grade')?$this->input->get('Grade'): '';
		$TransitionDate = $this->input->get('TransitionDate')?$this->input->get('TransitionDate'): '';

		if($employID != "")
		{
			$employIds = explode(";", $employID);
			
			foreach($employIds as $empID)
			{
				$primary_lead_id = $this->getPrimaryLeadID($empID);					
				
				// Start Audit Log		
				$empData 		= $this->input->cookie();
				$modfdId 		= $empData['employee_id'];
				$empRelInfo 	= $this->empRelInfo($empID);
				$fromVert 		= $empRelInfo[0]['verticalID'];
				$fromClient 	= $ClientID;
				$fromReporting 	= $primary_lead_id['primary_lead_id'];
				$toVert 		= "";
				$toClient 		= $ClientID;
				$toReporting 	= "";
				$empTrnCnt 		= 0;
				$transDate 		= $TransitionDate;
				
				$this->Audit_Log_Model->employTransitionLog($empID, $transDate, $modfdId, $fromVert, $fromClient, $fromReporting, $toVert,$toClient, $toReporting, 1, "Removed");
				// End Audit Log
			}	

			$retVal = $this->DelEmployProcessData($empID, $ClientID, -1, $Grade);

			//update billable details
			// $empBillResult = $this->updateEmpoyeeBillabilityData($employIds,$ProcessID);
			
			return $retVal;
		}
	}
	
	
	/**
	 * Function to get vertical manager employid based on process id.
	 * 
	 * @param int $processId
	 * @return string
	 */
	function getVerticalManager($verticalId = -1)
	{	
		$sql = "SELECT DISTINCT(e.employee_id) FROM `verticalmanager` ver  JOIN employees e ON e.`employee_id` = ver.`employee_id` WHERE ver.vertical_id = ".$verticalId." AND e.status != 6";
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		return "";
	}

	function leadTransition()
	{
		$retVal = -1; $resVal = 1;
		$transitionComment 	= ($this->input->get('TransitionComment')) ? $this->input->get('TransitionComment') : "";
		$transitionLead 	= ($this->input->get('transitionLead')) ? $this->input->get('transitionLead') : "";
		$transDate 		= ($this->input->get('transitionDate')) ? $this->input->get('transitionDate') : "";
		$employID 			= ($this->input->get('EmployID')) ? $this->input->get('EmployID') : '';
		$grade 				= $this->input->get('Grade') ? $this->input->get('Grade') : '';
		$clientID 			= ($this->input->get('ClientID')) ? $this->input->get('ClientID') : -1;
		$AssignLeads 		= ($this->input->get('AssignLeads')) ? $this->input->get('AssignLeads') : -1;

		if($employID != "")
		{
			$resVal = $this->updateleadTransDetails($employID, $transitionLead, $grade, $transDate, $transitionComment, $clientID,$AssignLeads);

			if($resVal)
			{
				$retVal = 1;
			}
			
			return $retVal;
		}
	}

	function updateleadTransDetails($employID, $transitionLead, $Grade=-1, $transDate, $transitionComment = "", $clientID = '', $AssignLeads='')
	{
		$result  = 0;

		$empResult = $this->updateLeadData($employID,  $transitionLead, $transDate, $transitionComment);

		if($empResult)
			$result  = 1;

		return $result ;
		
	}

	function updateLeadData($employIDStr,  $transitionLead, $transDate, $transitionComment)
	{
		$empData    = $this->input->cookie();
		$emp_id		= $empData['employee_id'];
		$result = 0;
		$employIDArr = explode(";", $employIDStr);
		foreach($employIDArr AS $list)
		{
			$from_reporting = $this->getPrimaryLeadID($list);
			$from_pod_id = $this->getPodID($list);
			$to_pod_id = $this->getPodID($list);
			$clientID = $this->getClientIDforLeadTransition($list);
		// echo "<pre>"; print_r($clientID); exit;

			if($transitionLead != -1) 
			{
				$this->db->set('primary_lead_id', $transitionLead);
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->update('employees');

				$result = 1;
			}

			if($transitionLead != -1) 
			{
				$this->db->set('primary_lead_id', $transitionLead);
				$this->db->where_in('employee_id', $list);
				$resVal = $this->db->update('employee_client');

				$result = 1;
			}

			if($employIDStr != -1) 
			{
				$this->db->set('employee_id', $list);
			}
			if($from_pod_id != -1) 
			{
				$this->db->set('from_pod_id', $from_pod_id['pod_id']);
			}
			if($clientID != -1) 
			{
				$this->db->set('from_client_id', isset($clientID['client_id'])?$clientID['client_id']:0);
			}
			if($from_reporting != -1) 
			{
				$this->db->set('from_reporting', $from_reporting['primary_lead_id']);
			}
			if($to_pod_id != -1) 
			{
				$this->db->set('to_pod_id', $to_pod_id['pod_id']);
			}
			if($clientID != -1) 
			{
				$this->db->set('to_client_id', isset($clientID['client_id'])?$clientID['client_id']:0);
			}
			if($transitionLead != "")
			{
				$this->db->set('to_reporting', $transitionLead);				
			}
			if($transDate != -1) 
			{
				$this->db->set('transition_date', $transDate);
			}
			if($transitionComment != -1) 
			{
				$this->db->set('comment', $transitionComment);
			}
			$this->db->set('modified_by', $emp_id);
			$this->db->where_in('employee_id', $list);
			$resVal = $this->db->insert('employee_transition');

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee(Employee ID:'.$list.') supervisor transition(Supervisor ID: '.$transitionLead.').';
			$this->userLogs($logmsg);
			
			$result = 1;
		}
	}

	private function getEmployeeDetails($empId='')
    {
        if($empId != '')
        {
            $sql = "SELECT CONCAT(IFNULL(`first_name`, ''), ' ', IFNULL(`last_name`, '')) AS Name  FROM `employees` WHERE `employee_id` ='" . $empId . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        }
        
    }
	
}

?>