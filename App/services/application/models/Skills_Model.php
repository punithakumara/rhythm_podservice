<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
	}


	public function getEmployees()
	{
		// debug($_GET);exit;

		$skills[] = ($this->input->get('skills') != '') ? $this->input->get('skills') : '';
		$skills[] = ($this->input->get('tools') != '') ? $this->input->get('tools') : '';
		$skills[] =	($this->input->get('technologies') != '') ? $this->input->get('technologies') : '';

		$shift = ($this->input->get('shift') != '') ? $this->input->get('shift') : '';
		$designation = ($this->input->get('designation') != '') ? $this->input->get('designation') : '';
		$experience = ($this->input->get('experience') != '') ? $this->input->get('experience') : '';
		// $passport =	($this->input->get('passport') != '') ? $this->input->get('passport') : '';
		// $visa =	($this->input->get('visa') != '') ? $this->input->get('visa') : '';
		// $stepup = ($this->input->get('stepup') != '') ? $this->input->get('stepup') : '';
		$languages = ($this->input->get('languages') != '') ? $this->input->get('languages') : '';
		$certifications = ($this->input->get('certifications') != '') ? $this->input->get('certifications') : '';


		$filter				= json_decode($this->input->get('filter'),true);
		// debug($filter);exit;
		$options['filter']	= $filter;
		$reportParam = ($this->input->get('Report')) ? $this->input->get('Report') : "";
		if ($this->input->get('hasNoLimit') != '1') {
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
		} 

		$sort = json_decode($this->input->get('sort'), true);

		$options['sortBy']        = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$sql = "SELECT employees.employee_id, company_employ_id, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `emp_name`,  verticals.name AS emp_vertical, designation.name AS emp_designation, GROUP_CONCAT(DISTINCT skills.skill_name) AS emp_skills, shift_code AS emp_shift 
		FROM employees
		LEFT JOIN employee_vertical ON employee_vertical.employee_id = employees.employee_id
		LEFT JOIN verticals ON verticals.vertical_id = employee_vertical.vertical_id
		LEFT JOIN designation ON designation.designation_id = employees.designation_id
		LEFT JOIN employee_skillset ON employee_skillset.employee_id = employees.employee_id
		LEFT JOIN skills ON skills.skill_id = employee_skillset.skill_id
		LEFT JOIN emp_shift ON emp_shift.employee_id = employees.employee_id
		LEFT JOIN shift ON shift.shift_id = emp_shift.shift_id
		LEFT JOIN employee_language_proficiency ON employee_language_proficiency.employee_id = employees.employee_id";

		if($certifications != '' && $certifications == 1)
			$sql .= " JOIN employee_certifications ON employee_certifications.employee_id = employees.employee_id";

		$sql .= " WHERE employees.status != 6 AND employees.location_id !=4 ";


		$skills = array_filter($skills);
		if(count($skills) > 0)
		{
			// debug($skills);exit;
			$skill_ids = implode(",",$skills);
			// echo $skill_ids;exit;
			$inner_sql = " SELECT employee_id FROM employee_skillset WHERE skill_id IN($skill_ids)
			GROUP BY employee_id HAVING COUNT(*) = ".count($skills);
			$sql .= " AND employee_skillset.employee_id IN ($inner_sql)";
		}

		/*if($skills != '')
			$sql .= " AND employee_skillset.skill_id ='$skills'";

		if($tools != '')
			$sql .= " AND employee_skillset.skill_id ='$tools'";

		if($technologies != '')
			$sql .= " AND employee_skillset.skill_id ='$technologies'";*/

		if($shift != '')
			$sql .= " AND emp_shift.shift_id = '$shift'";

		if($designation != '')
			$sql .= " AND designation.designation_id = '$designation'";

		if($experience != '')
			$sql .= " AND employee_skillset.experience = '$experience'";

/*		if($passport != '' && $passport == 1)
			$sql .= " AND employees.passport_status = 1";

		if($visa != '' && $visa == 1)
			$sql .= " AND employees.visa_status = 1";

		if($stepup != '')
			$sql .= " AND ";
*/
			if($languages != '')
				$sql .= " AND employee_language_proficiency.language_id = '$languages'";


			$filterWhere = '';

			if(isset($options['filter']) && $options['filter'] != '') 
			{
				foreach ($options['filter'] as $filterArray) 
				{
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) 
					{
						if($filterField != '') 
						{
							if(trim($filterField) == "employees.first_name") 
							{
								$filterField = " REPLACE(CONCAT(employees.first_name, ' ',employees.last_name), ' ', ' ') " ;
							}
							$filterWhere .= ($filterWhere != '')?' OR ':'';
							if(isset($filterArray['value']))
							{
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}
				}
			}


			if($filterWhere !="")
			{
				$sql .= " and (".$filterWhere.")";    			
			}

			$sql .= " GROUP BY employees.employee_id";
			// echo $sql;exit;

			$query = $this->db->query($sql);
			$result['totalCount'] = $query->num_rows();

			if ($reportParam != "") 
			{
				$sql .= " ORDER BY first_name ASC";
				$query = $this->db->query($sql);
				
				if($query->num_rows()==0)
					return 0;

				$data = $query->result_array();


				foreach ($data as $key => $value) {
					unset($data[$key]['employee_id']);
				}

				$columns = array(
					array_keys($data[0])
				);

				$results = array_merge($columns, $data);

				return $results;
			}
			else
			{
				if (isset($options['sortBy'])) 
				{
					$sql .= " ORDER BY " . $options['sortBy'] . " " . $options['sortDirection'];
				}

				if (isset($options['limit']) && isset($options['offset'])) {
					$sql .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
				} else if (isset($options['limit'])) {
					$sql .= " LIMIT " . $options['limit'];
				}
			}
			 //echo $sql;exit;
			$query = $this->db->query($sql);
			$data = $query->result_array();

			$result['rows'] = $data;
		// debug($result);exit;
			return $result;
		}

		public function getSkillsets()
		{
			// debug($_GET);exit;
			$filterQuery 		= $this->input->get('query');
			$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";

			$filter				= json_decode($this->input->get('filter'),true);

			$options['filter']	= $filter;

			if ($this->input->get('hasNoLimit') != '1') {
				$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
				$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
			} 

			$sql = "SELECT skill_id, skill_name, skill_description
			FROM skills
			WHERE skill_code = 'SK'";

			$filterWhere = '';
			if (isset($options['filter']) && $options['filter'] != '') {
				foreach ($options['filter'] as $filterArray) {
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach ($filterFieldExp as $filterField) {
						if ($filterField != '') {
							$filterWhere .= ($filterWhere != '') ? ' OR ' : '';
							$filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
						}
					}
				}

				if ($filterWhere != '') 
				{
					$sql .= " AND (" . $filterWhere . ")";
				}
			}

			if($filterName !="")
			{
				$sql .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
			}


			$sql .= " ORDER BY skill_name ASC";

			$query = $this->db->query($sql);
			$result['totalCount'] = $query->num_rows();

			if (isset($options['limit']) && isset($options['offset'])) {
				$sql .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
			} else if (isset($options['limit'])) {
				$sql .= " LIMIT " . $options['limit'];
			}

			// echo $sql;exit;

			$query = $this->db->query($sql);
			$data = $query->result_array();
			$result['rows'] = $data;

			// debug($result);exit;

			return $result;
		}

		function AddUpdateSkill( $data = '' ) 
		{
			// debug($data);exit;

			$skill_id = ($data['skill_id']) != '' ? $data['skill_id'] : '';
			unset($data['skill_id']);


			if($skill_id == '')
			{
				//insert

				$checkDuplicate = $this->checkDuplicate($data['skill_name']);
				if($checkDuplicate)
				{
					return -1;
				}
				$retVal = $this->insert($data, 'skills');

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Skills ID:'.$this->db->insert_id().') has been added skills Successfully.';
				$this->userLogs($logmsg);
			}
			else
			{
				//update

				$checkDuplicate = $this->checkDuplicate($data['skill_name'],$skill_id);
				if($checkDuplicate)
				{
					return -1;
				}

				$where['skill_id'] = $skill_id;

				$this->update($data, $where, 'skills');
				$retVal = $skill_id;

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Skills ID:'.$skill_id.') has been updated skills Successfully.';
				$this->userLogs($logmsg);
			}

			return $retVal;
		}

		public function getTools()
		{
			// debug($_GET);exit;

			$filterQuery 		= $this->input->get('query');
			$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";

			$filter				= json_decode($this->input->get('filter'),true);

			$options['filter']	= $filter;

			if ($this->input->get('hasNoLimit') != '1') {
				$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
				$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
			}

			
			$sql = "SELECT skill_id, skill_name, skill_description
			FROM skills
			WHERE skill_code = 'TO'";

			$filterWhere = '';
			if (isset($options['filter']) && $options['filter'] != '') {
				foreach ($options['filter'] as $filterArray) {
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach ($filterFieldExp as $filterField) {
						if ($filterField != '') {
							$filterWhere .= ($filterWhere != '') ? ' OR ' : '';
							$filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
						}
					}
				}

				if ($filterWhere != '') 
				{
					$sql .= " AND (" . $filterWhere . ")";
				}
			}

			if($filterName !="")
			{
				$sql .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
			}


			$sql .= " ORDER BY skill_name ASC";

			$query = $this->db->query($sql);
			$result['totalCount'] = $query->num_rows();

			if (isset($options['limit']) && isset($options['offset'])) {
				$sql .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
			} else if (isset($options['limit'])) {
				$sql .= " LIMIT " . $options['limit'];
			}

			// echo $sql;exit;

			$query = $this->db->query($sql);
			$data = $query->result_array();
			$result['rows'] = $data;

			// debug($result);exit;

			return $result;
		}

		function AddUpdateTool( $data = '' ) 
		{
			// debug($data);exit;

			$skill_id = ($data['skill_id']) != '' ? $data['skill_id'] : '';
			unset($data['skill_id']);


			if($skill_id == '')
			{
				//insert
				$checkDuplicate = $this->checkDuplicate($data['skill_name']);
				if($checkDuplicate)
				{
					return -1;
				}
				$retVal = $this->insert($data, 'skills');

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Tools ID:'.$this->db->insert_id().') has been added tools Successfully.';
				$this->userLogs($logmsg);
			}
			else
			{
				//update

				$checkDuplicate = $this->checkDuplicate($data['skill_name'],$skill_id);
				if($checkDuplicate)
				{
					return -1;
				}

				$where['skill_id'] = $skill_id;

				$this->update($data, $where, 'skills');

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Skills ID:'.$skill_id.') has been updated tools Successfully.';
				$this->userLogs($logmsg);

				$retVal = $skill_id;
			}

			return $retVal;
		}

		public function getTechnologies()
		{
		// debug($_GET);exit;

			$filterQuery 		= $this->input->get('query');
			$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";

			$filter				= json_decode($this->input->get('filter'),true);

			$options['filter']	= $filter;

			if ($this->input->get('hasNoLimit') != '1') {
				$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
				$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
			}
			
			$sql = "SELECT skill_id, skill_name, skill_description
			FROM skills
			WHERE skill_code = 'TE'";

			$filterWhere = '';
			if (isset($options['filter']) && $options['filter'] != '') {
				foreach ($options['filter'] as $filterArray) {
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach ($filterFieldExp as $filterField) {
						if ($filterField != '') {
							$filterWhere .= ($filterWhere != '') ? ' OR ' : '';
							$filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
						}
					}
				}

				if ($filterWhere != '') 
				{
					$sql .= " AND (" . $filterWhere . ")";
				}
			}

			if($filterName !="")
			{
				$sql .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
			}


			$sql .= " ORDER BY skill_name ASC";

			$query = $this->db->query($sql);
			$result['totalCount'] = $query->num_rows();

			if (isset($options['limit']) && isset($options['offset'])) {
				$sql .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
			} else if (isset($options['limit'])) {
				$sql .= " LIMIT " . $options['limit'];
			}

			// echo $sql;exit;

			$query = $this->db->query($sql);
			$data = $query->result_array();
			$result['rows'] = $data;

			// debug($result);exit;

			return $result;
		}

		function AddUpdateTechnology( $data = '' ) 
		{
			// debug($data);exit;

			$skill_id = ($data['skill_id']) != '' ? $data['skill_id'] : '';
			unset($data['skill_id']);


			if($skill_id == '')
			{
				//insert
				$checkDuplicate = $this->checkDuplicate($data['skill_name']);
				if($checkDuplicate)
				{
					return -1;
				}
				$retVal = $this->insert($data, 'skills');

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Tools ID:'.$this->db->insert_id().') has been added technology Successfully.';
				$this->userLogs($logmsg);
			}
			else
			{
				//update

				$checkDuplicate = $this->checkDuplicate($data['skill_name'],$skill_id);
				if($checkDuplicate)
				{
					return -1;
				}

				$where['skill_id'] = $skill_id;

				$this->update($data, $where, 'skills');

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Tools ID:'.$skill_id.') has been updated technology Successfully.';
				$this->userLogs($logmsg);

				$retVal = $skill_id;
			}

			return $retVal;
		}

		public function getLanguages()
		{
		// debug($_GET);exit;

			$filterQuery 		= $this->input->get('query');
			$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";

			$sql = "SELECT language_id, language
			FROM languages
			WHERE 1=1";

			if($filterName !="")
			{
				$sql .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
			}


			$sql .= " ORDER BY language";
			

			$query = $this->db->query($sql);

			$result['totalCount'] = $query->num_rows($query);

			$data = $query->result_array();
			$result['rows'] = $data;

		// debug($result);exit;

			return $result;
		}

		public function getCertifications()
		{
		// debug($_GET);exit;
			$data = array();
			for($i=0;$i<5;$i++)
			{
				$data[$i]['certification_id'] = ($i+1);
				$data[$i]['certification'] = 'Certification'.($i+1);
			}
		// debug($result);exit;
			$result['totalCount'] = count($data);
			$result['rows'] = $data;
			return $result;
		}

		public function getEmployeeSkillsReport()
		{
			if ($reportParam != "") 
			{
				foreach ($data as $data_key => $data_value) {

				}
			// debug($data);exit;

				$columns = array(
					array_keys($data[0])
				);

				$results = array_merge($columns, $data);

				return $results;
			}
		}


		public function  getSkillToolsTech()
		{
			$filterQuery 		= $this->input->get('query');
			$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";
			
			$sql = "SELECT skill_id,skill_name 
			FROM skills
			WHERE 1=1";

			if($filterName !="")
			{
				$sql .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
			}

			$query = $this->db->query($sql);

			$result['totalCount'] = $query->num_rows($query);

			$data = $query->result_array();
			$result['rows'] = $data;

		// debug($result);exit;

			return $result;
		}

		private function checkDuplicate($skill_name,$skill_id='')
		{
			$sql = "SELECT skill_id 
			FROM skills
			WHERE skill_name = '$skill_name'";

			if($skill_id != '')
				$sql .= " AND skill_id != '$skill_id'";

			$query = $this->db->query($sql);

			if($query->num_rows($query)>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}