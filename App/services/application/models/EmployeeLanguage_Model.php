<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeLanguage_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_languages()
	{  

		if($this->input->get('employee_id') != null && $this->input->get('employee_id') != '')
		{
			$employee_id = $this->input->get('employee_id');
		}
		else
		{
			$empData     = $this->input->cookie();
			$employee_id = $empData['employee_id'];
		}


		$sql="SELECT * FROM employee_language_proficiency 
		INNER JOIN languages on employee_language_proficiency.language_id=languages.language_id
		WHERE employee_id = '$employee_id'
		ORDER BY id DESC";

		
		$query = $this->db->query($sql);

		$results['totalCount'] = $query->num_rows();

		$data = $query->result_array();
		$results['data'] = $data;

		return $results;
	}
		
	function addLanguage($data = '')
	{
		$languageData = $data;
		$empData     = $this->input->cookie();

		$this->db->set('employee_id',$empData['employee_id']);
		$this->db->set('language_id', $languageData['language']);
		$this->db->set('proficiency', $languageData['proficiency']);
		$this->db->set('read', $languageData['read']);
		$this->db->set('write', $languageData['write']);
		$this->db->set('speak', $languageData['speak']);
		$this->db->set('created_on', date("Y-m-d H:i:s"));								
		$resVal = $this->db->insert('employee_language_proficiency');

		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been added employee language (Language ID:'.$this->db->insert_id().').';
			$this->userLogs($logmsg); 

		return $resVal;
	}
	
	function updateLanguage($idVal = '',$data = '')
	{
		$empData     = $this->input->cookie();			
		$setData = $data;		
		$where = array();
		$set = array();
		$where['id'] = $setData['id'];

		$sql="SELECT languages.language_id FROM employee_language_proficiency INNER JOIN languages ON employee_language_proficiency.language_id=languages.language_id WHERE languages.language='".$setData['language']."'";
		$query = $this->db->query($sql);
		$data = $query->result_array();

		if(count($data)>0) {
			$languageId=$data[0]["language_id"];
		}
		else {
			$languageId=$setData["language"];
		}

		$dataset = array(
			'language_id'			=> $languageId,
			'proficiency'			=> $setData['proficiency'],
			'read'	=> $setData['read'],
			'write'	=> $setData['write'],
			'speak'	=> $setData['speak'],
			'updated_on'	=> date("Y-m-d H:i:s")
		);			
		$retVal = $this->update($dataset, $where, 'employee_language_proficiency');	

		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee language (Language ID:'.$setData['id'].').';
			$this->userLogs($logmsg); 

		return $retVal;		
	}		
	
	function deleteLanguage( $idVal = '' ) 
	{
		$options = array();
		$options['id'] =  $idVal;
		$retVal = $this->delete($options, 'employee_language_proficiency');

		$empData     = $this->input->cookie();	
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been deleted employee language (Language ID:'.$idVal.').';
			$this->userLogs($logmsg); 

		return $retVal;
	}


}
?>