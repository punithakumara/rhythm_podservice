<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shift_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function getShiftData(){
		// debug($_GET);exit;

		$filterQuery 		= $this->input->get('query');
		$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";

		$shift_code   = $this->input->get('shift_code') ? str_replace(",", "','",$this->input->get('shift_code')) : "";
		$options = array();
		$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;

		$sort = json_decode($this->input->get('sort'),true);

		// $options['sortBy'] = $sort[0]['property'];
		// $options['sortDirection'] = $sort[0]['direction'];
			//$options['select'] = 'ProcessID';

		//$shiftQry = "SELECT * FROM shift WHERE 1=1";
		$shiftQry = "SELECT * FROM shift WHERE shift_id >= 8";
		if($shift_code)
		{
			$shiftQry .= " AND shift_code NOT IN ('".$shift_code."')";
		}

		if($filterName !="")
		{
			$shiftQry .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
		}


		$shiftQry .= " ORDER BY shift_code ASC";

		//echo $shiftQry;exit;

		$totCount = $this->db->query($shiftQry);
		$results = array_merge(array('totalCount' => $totCount->num_rows()),array('shift' => $totCount->result_array()));

		return $results;
	}
}
?>