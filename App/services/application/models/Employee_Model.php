<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_Model extends INET_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Rhythm_Model','Hierarchycount_Model','Vertical_Model'));
        $this->config->load('admin_config');
    }

    function getEmployeeData($email_Id)
    {
        $sql = "SELECT e.employee_id, e.company_employ_id, e.email,e.user_dashboard,e.gender, e.is_onshore,e.service_id,sv.name AS service_name,ep.pod_id, po.pod_name, GROUP_CONCAT(DISTINCT ev.vertical_id) AS assigned_vertical,v.name, d.grades,d.designation_id,d.Name, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `first_name`,e.dashboard_tools
        from employees e 
        join designation d on d.designation_id = e.designation_id 
        join employee_pod ep on ep.employee_id = e.employee_id 
        join tbl_rhythm_pod po on po.pod_id = ep.pod_id 
        join tbl_rhythm_services sv on sv.service_id = e.service_id 
        join employee_vertical ev on ev.employee_id = e.employee_id
        join verticals v on v.vertical_id = ev.vertical_id
        where e.company_employ_id ='" . $email_Id . "' AND e.status NOT IN(4,6)";
        //echo $sql; exit;
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function getverticalData($VerticalID)
    {
        $sql = "SELECT  employee_id FROM verticalmanager  where VerticalID ='" . $VerticalID . "'";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function getGrade($designation_id)
    {
        $sql = "SELECT  grades FROM designation  where designation_id ='" . $designation_id . "'";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    public function getEmployeegrade($employee_id)
    {
        $sql = "SELECT d.grades,e.employee_id, e.designation_id, e.primary_lead_id,e.training_needed from employees e join designation d on d.designation_id = e.designation_id  where e.employee_id ='" . $employee_id . "'";        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function getAssociateManagers()
    {
        $VerticalID = $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';
        $ManagerID  = $this->input->get('employee_id') ? $this->input->get('employee_id') : '';
        $empData    = $this->input->cookie();
        if($VerticalID)
        {
            if($VerticalID != '')
            {
                $sql = "SELECT DISTINCT(employees.`employee_id`), CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name` from employees
                        JOIN designation ON designation.`designation_id` = employees.`designation_id`
                        JOIN `employprocess` ON employees.employee_id = employprocess.employee_id
                        JOIN PROCESS ON `employprocess`.process_id = process.process_id
                        WHERE employees.relieve_flag=0 AND designation.grades =4 AND process.VerticalID = $VerticalID ORDER BY first_name";       	
                $query = $this->db->query($sql);
                
                $resultData = $query->result_array();
                            
                if(!empty($resultData)) //if Associate Managers are available for Vertical
                { 
                    $results = $query->result_array();            	
                } 
                else //get Vertical Managers Reportees AM'S if Associate Managers are not available for Vertical  
                { 
                    $verMgrId = $this->getVerticalManagerById($VerticalID); //get vertical managers by vertical-id           	
                    $amIdsArray = $this->getAmsbyMid($verMgrId['0']['employee_id']); //get AM's by manager-id           	           				            	 				
                    $amDetailsSql= "SELECT DISTINCT(employees.`employee_id`), CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name` from employees
                                        JOIN designation ON designation.`designation_id` = employees.`designation_id`
                                        JOIN `employprocess` ON employees.employee_id = employprocess.employee_id
                                        JOIN PROCESS ON `employprocess`.process_id = process.process_id
                                        WHERE employees.relieve_flag=0 AND employees.employee_id IN (". implode(",",$amIdsArray ).") ORDER BY first_name";				
                    $amDetailsquery = $this->db->query($amDetailsSql);
                    $results = $amDetailsquery->result_array();
                }
                
                return $results;
            }
        } 
        else if($ManagerID) 
        {
            $sql = "SELECT DISTINCT(employees.`employee_id`), CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name` from employees
                    JOIN designation ON designation.`designation_id` = employees.`designation_id`
                    WHERE employees.relieve_flag=0 AND designation.grades = 4 AND 
                    employees.Primary_Lead_ID = $ManagerID ";
            
            if($empData['Grade'] == 4 || $empData['Grade'] < 5) {
                $sql    .= " AND employees.employee_id = ".$empData['employee_id']."";
            }

            $sql        .= " ORDER BY employees.first_name ";

            $query = $this->db->query($sql);
            return $query->result_array();
            
        } else {
            return '';
        }
    }
        
    function  getVerticalManagerById($VerticalID)
    {
        $sql = "SELECT `employee_id` FROM `verticalmanager` WHERE `VerticalID` ='" . $VerticalID . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function getAmsbyMid($verMgrId)
    {
        $sql = "SELECT d.`grades` FROM `designation` d LEFT JOIN employ e ON e.`designation_id` = d.`designation_id` WHERE e.employee_id = '" . $verMgrId . "' AND relieve_flag = 0";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) 
        {
            $empGradeArray = $query->result_array();   		
            $grade = $empGradeArray[0]['grades'];    		
            if ($grade == 5) 
            {   			
                $amSql = "SELECT employee_id from employees JOIN designation d ON d.designation_id = employees.designation_id WHERE `Primary_Lead_ID` = '" . $verMgrId . "'  AND grades >=4 AND grades <=5 AND relieve_flag = 0";
                $amquery = $this->db->query($amSql);
                if ($amquery->num_rows() > 0) 
                {				
                    return array_map('current', $amquery->result_array());    
                }
            }
            if ($grade == 7) 
            {
                $amSql = "SELECT employee_id from employees JOIN designation d ON d.designation_id = employees.designation_id WHERE grades >=4 AND grades <5 AND relieve_flag = 0";
                $amquery = $this->db->query($amSql);
                if ($amquery->num_rows() > 0) {
                    return array_map('current', $amquery->result_array());
                }
            }
            if ($grade == 6)
            {
                $associateManagerArray = array();
                $managerSql = "SELECT e.employee_id, d.grades   from employees e JOIN designation d ON d.designation_id = e.designation_id WHERE e.`Primary_Lead_ID` = '" . $verMgrId . "'  AND d.grades >= 4 AND d.grades <= 6 AND  e.relieve_flag = 0";  			   		  			
                $managerquery = $this->db->query($managerSql);
                if ($managerquery->num_rows() > 0) {
                    $managers = $managerquery->result_array();			
                    if(!empty($managers))
                    {
                        foreach ($managers as $key=>$value)
                        {
                            if($value['grades'] == '4')
                            {
                                $associateManagerArray[] = $value['employee_id'];
                            }
                            else
                            {
                                $managerArray[] = $value['employee_id'];
                            }	
                        }
                    }
                    
                    if(!empty($managerArray))
                    {
                        $associateManagerSql = "SELECT e.employee_id from employees e JOIN designation d ON d.designation_id = e.designation_id WHERE e.`Primary_Lead_ID` IN (". implode(",",$managerArray ).")  AND d.grades >= 4 AND d.grades < 5 AND  e.relieve_flag = 0";
                        $associateManagerquery = $this->db->query($associateManagerSql);
                        if ($associateManagerquery->num_rows() > 0) 
                        {
                            $associatemanagers = $associateManagerquery->result_array();
                            $associatemanagers = array_map('current', $associatemanagers);
                        
                            return array_merge($associatemanagers,$associateManagerArray);
                        }
                        else
                        {
                            return $associateManagerArray;
                        }
                    }
                    else
                    {
                        return $associateManagerArray;
                    }    				 				
                }
                else { return ''; }
            }  		
        }
    }
        
    function getManagers()
    {
        $VerticalID = $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';
        $listAll = $this->input->get('listAll') ? $this->input->get('listAll') : 0;
        $wor_id         = $this->input->get('wor_id') ? $this->input->get('wor_id') : "";
        $cond       = "";
        $empData    = $this->input->cookie();
        if ($VerticalID) {
            $cond = " AND employee_vertical.vertical_id = $VerticalID";
        }
        $sql_wor = "SELECT GROUP_CONCAT(DISTINCT(delivery_manager) SEPARATOR ',') AS delivery_manager FROM wor";
        
        if($wor_id!="")
        {
            $sql_wor .= " WHERE wor.wor_id=" . $wor_id;
        }
        $query_wor = $this->db->query($sql_wor);
        $result_wor = $query_wor->row_array();
        
        $mgr= $result_wor['delivery_manager'];
        
        $sql = "SELECT DISTINCT employees.`employee_id`, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Name from employees
                    # JOIN designation ON employees.designation_id = designation.designation_id
                    JOIN employee_vertical ON employee_vertical.employee_id = employees.employee_id
                    WHERE employees.status=3 
                    # AND designation.grades > 4 AND designation.grades < 8 
                    AND employees.employee_id IN( " .$mgr.")". $cond . " ";
        
        if ($this->input->get('query') != "") {
            $sql    .= " AND CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) like '%" . $this->input->get('query') . "%'";
        }

        $sql        .= " ORDER BY employees.first_name";
        
        $query       = $this->db->query($sql);
        return $query->result_array();
    }
        
    function getAllManagers()
    {
        $sql = "SELECT employees.`employee_id`, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Name 
                FROM employees
                JOIN designation ON employees.designation_id = designation.designation_id
                WHERE employees.status!=6 AND designation.grades > 4 AND employees.location_id!='4'";
        
        if ($this->input->get('query') != "") {
            $sql    .= " AND CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) like '%" . $this->input->get('query') . "%'";
        }

        $sql        .= " ORDER BY employees.first_name";
        
        $query       = $this->db->query($sql);
        $data        = $query->result_array();
        $results = array(
            'data' => $data
        );
        return $results;
    }

    function build_query()
    {
        $gridEmployeeFlg = $this->input->get('gridEmployees') ? $this->input->get('gridEmployees') : 0;
        $empData         = $this->input->cookie();
        $gridEmployee    = ($empData['Vert'] == HR_VERTICAL_ID) ? 0 : $gridEmployeeFlg;
        $filter          = json_decode($this->input->get('filter'), true);
        
        $filterName = $this->input->get('filterName') ? $this->input->get('filterName') : "";
        
        $profileID      = $this->input->get('profileID') ? $this->input->get('profileID') : "";
        $empTypes       = $this->input->get('empTypes') ? $this->input->get('empTypes') : "";
        $empStatusCombo = $this->input->get('empStatusCombo') ? $this->input->get('empStatusCombo') : 0;
        
        /*added for autofilter*/
        $filterQuery = $this->input->get('query');
        
        $options['filter'] = $filter;
        
        if ($gridEmployee == 1) {
            $this->GetEmployeeHierarchy();
            
        } else {
            if($empStatusCombo == 2){
                $relieve_flag = 0;
            }
            if($empStatusCombo ==1 ){
                $relieve_flag = 1;
            }
            if($empStatusCombo == 0){
                $relieve_flag = 0;
            }
            $this->db->select('employees.*, CAST(SUBSTRING_INDEX(employees.company_employ_id, "-", -1) AS UNSIGNED INTEGER) AS CID, designation.Name AS Designation_Name,designation.grades AS Grade, location.Name AS Location_Name, ShiftCode', false);
            $this->db->from('employ');
            $this->db->join('employprocess', 'employees.employee_id = employprocess.employee_id','left');
            $this->db->join('verticalmanager', 'employees.employee_id = verticalmanager.employee_id','left');
            $this->db->join('shift', 'shift.ShiftID = employprocess.ShiftID OR shift.ShiftID = verticalmanager.ShiftID','left');
            $this->db->join('designation', 'employees.designation_id = designation.designation_id');
            $this->db->join('location', 'employees.LocationID = location.LocationID','left');
            $this->db->where('employees.relieve_flag', $relieve_flag);
            
        if($empStatusCombo == 2)
        {
            $this->db->where('employees.relieve_flag', 0);
            $this->db->where('employees.Resign_Flag', 1);  
        }
        else
        {
            $this->db->where('employees.relieve_flag', $relieve_flag);
            if($empStatusCombo != 1)
            $this->db->where('employees.Resign_Flag', 0);  
        }
        
        
        }
        $filterWhere = '';
        if (isset($options['filter']) && $options['filter'] != '') {
            foreach ($options['filter'] as $filterArray) {
                $filterFieldExp = explode(',', $filterArray['property']);
                foreach ($filterFieldExp as $filterField) {
                    if ($filterField != '') {
                        $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                        $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                    }
                    $filterWhere .= " OR CONCAT_WS('', IFNULL(first_name, '') ,' ', IFNULL(last_name,'')) LIKE '%" . $filterArray['value'] . "%'";               }
            }
            if ($filterWhere != '') {
                $this->db->where('(' . $filterWhere . ')');
            }
        }
        if ($filterName != "") {
            $filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
            $this->db->where($filterWhere);
        }
        if ($profileID != "") {
            $this->db->where('employees.employee_id', $profileID);
        }
        
        if (isset($empTypes) && $empTypes != "") {
            $this->db->where('designation.grades >=', 5);
        }
        
    }

    function GetEmployeeHierarchy()
    {
        
        $comboClient    = $this->input->get('comboClient') ? $this->input->get('comboClient') : 0;
        $comboProcess   = $this->input->get('comboProcess') ? $this->input->get('comboProcess') : 0;
        $empStatusCombo = $this->input->get('empStatusCombo') ? $this->input->get('empStatusCombo') : 0;
        $utilCombo      = $this->input->get('utilCombo') ? $this->input->get('utilCombo') : 0;
        $empData        = $this->input->cookie();
        $this->EmpId    = $empData['employee_id'];
        $filterName     = $this->input->get('filterName') ? $this->input->get('filterName') : "";
        $filterQuery    = $this->input->get('query');
        $filterQueryName     = $this->input->get('filter') ? json_decode($this->input->get('filter'), true) : "";//        $sort          = json_decode($this->input->get('sort'), true);
        $clientPartner = $this->input->get('clientPartner') ? $this->input->get('clientPartner') : 0;
        $engagementManager = $this->input->get('engagementManager') ? $this->input->get('engagementManager') : 0;
        
        if(isset($filterQueryName[0]['property']))
            $filterProperty=$filterQueryName[0]['property'];
        else 
            $filterProperty= ""; 
                
        if(isset($filterQueryName[0]['value']))
            $filterValue=$filterQueryName[0]['value'];
        else 
            $filterValue= "";      
        //get all the employees based on process
        $employArray = $this->getAllEmployee($comboClient, $comboProcess);
        
        $utilArray = "";
        if ($utilCombo != 0) {
            $utilArray = $this->getUtilEmployee($comboClient, $comboProcess);
            
        }
        
        if ($utilArray != "") {
            foreach ($utilArray as $key => $val) {
                if (!in_array($val['employee_id'], $employArray))
                    array_push($employArray, $val['employee_id']);
            }
        }
        
        $query = $this->db->select('DISTINCT(employees.employee_id) AS EmployeeID, employees.*,CAST(SUBSTRING_INDEX(employees.company_employ_id, "-", -1) AS UNSIGNED INTEGER) AS CID,  designation.Name AS Designation_Name, designation.grades AS Grade, location.Name AS Location_Name,ShiftCode', false);
        $this->db->from("employ");
        $this->db->join('employprocess', 'employees.employee_id = employprocess.employee_id','left');
        $this->db->join('verticalmanager', 'employees.employee_id = verticalmanager.employee_id','left');
        $this->db->join('shift', 'shift.ShiftID = employprocess.ShiftID OR shift.ShiftID = verticalmanager.ShiftID','left');
        $this->db->join('designation', 'employees.designation_id = designation.designation_id', 'left');
        $this->db->join('location', 'employees.LocationID = location.LocationID', 'left');
        $this->db->join('employvertical', 'employees.employee_id = employvertical.employee_id', 'left');
                
        if($empStatusCombo == 2)
        {
            $this->db->where('employees.relieve_flag', 0);
            $this->db->where('employees.Resign_Flag', 1);  
        }
        else
        {
            $this->db->where('employees.relieve_flag', $empStatusCombo);
            if($empStatusCombo != 1)
            {
                //condition to display also resigned employeed in Insight Employee dropdown
                if ($utilCombo == 1)
                {
                    $this->db->where_in('employees.Resign_Flag', array('0','1')); //displaying Resigned employees in Insight who are having Insight Records               		
                }
                else
                {
                    $this->db->where('employees.Resign_Flag', 0);
                }               	          	
            }         
        }
        
        
        if ($clientPartner == '1' || $engagementManager == '1')
        {
            if($clientPartner == 1)
            {
                $clientPartnerID = $this->getClientPartnerById($comboClient,'clientPartner');
                $this->db->where_in('employees.employee_id', $clientPartnerID['0']['clientPartner']);
            }
            
            if($engagementManager == '1')
            {
                $accMgrID = $this->getClientPartnerById($comboClient, 'accManager');
                $this->db->where_in('employees.employee_id', $accMgrID['0']['accManager']);
            }
            
        }
        else
        {
            if (is_array($employArray) && count($employArray) != 0)
                $this->db->where_in('employees.employee_id', $employArray);
        }          
        
        if (($filterName != "") && ($filterName == "FullName") && ($filterQuery != '')) {
            $filterWhere = "(first_name LIKE '%" . $filterQuery . "%' OR last_name LIKE '%" . $filterQuery . "%')"; //modified search Like query to display only from Result set
            $this->db->where($filterWhere);
        }
    }

    function getClientPartnerById($clientID, $type)
    {
        if ($type == 'clientPartner')
        {
            $sql = "SELECT `clientPartner` FROM CLIENT WHERE `ClientID` ='" . $clientID . "'";    		
        }
        else
        {
            $sql = "SELECT `accManager` FROM CLIENT WHERE `ClientID` ='" . $clientID . "'";
        }
            
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '000';
    }

    function getUtilEmployee($comboClient, $comboProcess)
    {
        $sql = "SELECT  distinct(employee_id) FROM utilization  where process_id ='" . $comboProcess . "'";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }


    function getEmployeesDetails()
    {
        $options = array();
        if ($this->input->get('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        $comboEmployee = $this->input->get('comboEmployee') ? $this->input->get('comboEmployee') : 0;
        $sort          = json_decode($this->input->get('sort'), true);
        
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        if ($comboEmployee != 1)
            $this->build_query();
        else
            $this->GetEmployeeHierarchy();
        
        $this->db->group_by('`employees`.`employee_id`');
        $totQuery = $this->db->get();
        $totCount = $totQuery->num_rows();
        
        if ($comboEmployee != 1)
            $this->build_query();
        else
            $this->GetEmployeeHierarchy();
        
        if (isset($options['limit']) && isset($options['offset']))
            $this->db->limit($options['limit'], $options['offset']);
        else if (isset($options['limit']))
            $this->db->limit($options['limit']);
        
        if (isset($options['sortBy'])) // sort
            {
            if ($options['sortBy'] == "company_employ_id")
                $this->db->order_by("CID", $options['sortDirection']);
            else
                $this->db->order_by($options['sortBy'], $options['sortDirection']);
        }
        $this->db->group_by('`employees`.`employee_id`');
        $query = $this->db->get();
        $data = $query->result_array();
        //echo $this->db->last_query(); exit;
        foreach($data as $key => $value)
        {
            //fetching billable history
            $billDetails = $this->Rhythm_Model->getBillableHistoryDetails($value['employee_id']);
            if(count($billDetails)>0)
            {
                $data[$key]['billable'] = $billDetails['rows'];
            } else {
                $data[$key]['billable'] = "null";
            }
            
            //fetching shift history
            $shiftDetails = $this->Rhythm_Model->getShiftHistoryDetails($value['employee_id']);
            if(count($shiftDetails)>0)
            {
                $data[$key]['shift'] = $shiftDetails['rows'];
            } else {
                $data[$key]['shift'] = "null";
            }
        }
        // echo "<pre>"; print_r($data); exit;
        $results = array_merge(array(
            'totalCount' => $totCount
        ), array(
            'data' => $data
        ));
        return $results;
    }

    function updateEmployee($idVal = '', $data = '')
    {
        
        $setData = json_decode(trim($data), true);
        
        if ($this->_isRecordExist($idVal, $setData['company_employ_id'], $setData['Email']) == 0) {
            unset($setData['DeligateTo']);        
            $where             = array();
            $set               = array();
            $empData           = $this->input->cookie();
            $EmpId             = $empData['employee_id'];
            $where['employee_id'] = $idVal;
            
            // Code for convertion of date formate 
            $BDay                   = $setData['DateOfBirth'];
            $JDay                   = $setData['DateOfJoin'];
            $RDay                   = $setData['RelieveDate'];
            $setData['DateOfBirth'] = date("Y-m-d", strtotime($BDay));
            $setData['DateOfJoin']  = date("Y-m-d", strtotime($JDay));
            $setData['RelieveDate'] = date("Y-m-d", strtotime($RDay));
            
            //Added for date updation
            $empData     = $this->input->cookie();
            $this->EmpId = $empData['employee_id'];
            $setData['UpdatedBy']   = $this->EmpId;
            $setData['UpdatedDate'] = date("Y-m-d H:i:s");
            
            /* Added for Last Modified by and Date to Track*/
            $setData['Last_ModifiedBy']    = $this->EmpId;
            $setData['Last_Modified_Date'] = date("Y-m-d H:i:s");
            
            /* Added code for Chatnam Employees */
            $chatnamflag = false;
            if($setData['LocationID'] == 4 ){
                $setData['AssignedVertical'] = NULL;
                $chatnamflag = true;
                $setData['Primary_Lead_ID'] = 1297;
            }
            
            //Added for grade4 and above to onboarding
            $mveonboarding = 0;
            if($setData['Grade'] >= 4 && $setData['LocationID'] != 4){
                if($setData['moveonboard'] == 1)
                {
                    $mveonboarding = 1;
                    $setData['status'] = 7;
                }
            }else{
                unset($setData['moveonboard']);
            }
            
            $tepVar                 = $setData;
            unset($tepVar['FullName']);
            unset($tepVar['id']);
            unset($tepVar['UpdatedDate']);
            unset($tepVar['Designation_Name']);
            unset($tepVar['Grade']);
            
            //Added for grade4 and above to onboarding
            if($mveonboarding == 1){
                $tepVar['moveonboard'] = 1;            	
            }
            
            //Inserting data to HCM Table
            $hcm['employee_id'] = $idVal;
            $oldverticleid   = $this->getOldVertical($idVal);
            $hcm['OldValue'] = $oldverticleid;
            
            $newverticleid   = $setData['AssignedVertical'];
            $hcm['NewValue'] = $newverticleid;
            $hcm['Type']     = 'Vertical';
            $this->load->model('Hcm_model');
            
            if ($oldverticleid !== $newverticleid) {
                $this->Hcm_model->insertHcmData($hcm);
            }
            
            //add the action log table.
            $differenceArray = array(
                'employee_id',
                $this->EmpId,
                $tepVar
            );
            $this->log_action('employ', 'update', $tepVar['first_name'], $differenceArray);
            $retVal = $this->update($setData, $where, 'employ');
            
            /* Audit Log */
            $modfdId = $this->EmpId;
            $fromVert = "";
            $fromClient = "";
            $fromProcess = "";
            $fromReporting = "";
            $toVert = "";
            $toClient = "";
            $toProcess = "";
            $toReporting = "";
            $empTrnCnt = 0;
            $transDate = date('d-m-Y');
            $this->load->model('audit_log_model');
            //$this->audit_log_model->employTransitionLog($idVal,$transDate,$modfdId,$fromVert,$fromClient,$fromProcess,$fromReporting,$toVert,$toClient,$toProcess,$toReporting);
            /* End Of Audit Log*/
            
            /* update Grades to employprocess */            
            $queryupdategrades  = "UPDATE employprocess SET Grades =(SELECT grades FROM designation WHERE designation_id='" . $setData['designation_id'] . "') WHERE employee_id='" . $idVal . "'";		
            $this->db->query($queryupdategrades);
            
            /* Grade4 and above to onboarding*/
            if($mveonboarding == 1)
            {
                $this->db->select('grades');
                $this->db->where('designation_id', $setData['designation_id']);
                $this->db->from('designation');
            
                $queryGrade = $this->db->get();
                $grdData    = $queryGrade->row();
                $addonboardingEmployee = array(
                                'process_id' => ONBOARD_PROCESS_ID,
                                'StartDate'=>date('Y-m-d'),
                                'employee_id' => $idVal,
                                'Grades'  =>$grdData->grades,	            					                  		                  
                            );
                $checkinsert = $this->update($addonboardingEmployee, array(
                                    'employee_id' => $idVal                               
                                ), 'employprocess');
                if($checkinsert == 0){
                    $addonboardingEmployee['ShiftID'] = SHIFT_ID;
                    $this->insert($addonboardingEmployee, 'employprocess');
                }
            }	
            return $retVal;
        } else
            return -1;
    }

    function deleteEmployee($idVal = '')
    {
        $options = array();
        
        $options['employee_id'] = $idVal;
        $retVal              = $this->delete($options, 'employ');
        return $retVal;
    }


    function addEmployee123($data = '')
    {
        $addEmployee = json_decode(trim($data), true);
        $retVal      = $this->insert($addEmployee, 'employ');
        return $retVal;
    }


    private function _isRecordExist($id, $companyEmpID, $email)
    {
        $where = " (company_employ_id =  '" . trim($companyEmpID) . "' OR LOWER(email) =  '" . trim(strtolower($email)) . "') ";
        $query = $this->db->select("employee_id")->where($where, null, false);
        if ($id != "")
            $this->db->where("employee_id  != ", trim($id));
        $query = $this->db->get('employ');
        
        return $query->num_rows();
    }

    function addEmployee($data = '')
    {
        $addEmployee = json_decode(trim($data), true);
        if ($this->_isRecordExist("", $addEmployee['company_employ_id'], $addEmployee['Email']) == 0) 
        {
            unset($addEmployee['DeligateTo']);
            unset($addEmployee['Resign_Flag']);
            unset($addEmployee['Resigned_Notes']);
            unset($addEmployee['Resigned_Date']);
            $BDay                       = $addEmployee['DateOfBirth'];
            $JDay                       = $addEmployee['DateOfJoin'];
            $addEmployee['DateOfBirth'] = date("Y-m-d", strtotime($BDay));
            $addEmployee['DateOfJoin']  = date("Y-m-d", strtotime($JDay));
            unset($addEmployee['relieve_flag']);
            $addEmployee['RelieveDate'] = $addEmployee['RelieveDate'] ? $addEmployee['RelieveDate'] : '0000-00-00';
            if (!$addEmployee['company_employ_id'])
                unset($addEmployee['company_employ_id']);
            $empData     = $this->input->cookie();
            $this->EmpId = $empData['employee_id'];
            
            /* Added code for Chatnam Employees */
            $chatnamflag = false;
            if($addEmployee['LocationID'] == 4 ){
                $addEmployee['AssignedVertical'] = NULL;
                $chatnamflag = true;
                $addEmployee['Primary_Lead_ID'] = 1297;
            }
            
            $addEmployee['UpdatedBy']       = $this->EmpId;
            $addEmployee['UpdatedDate']     = date("Y-m-d H:i:s");
            $addEmployee['training_status'] = 0;
            $addEmployee['training_needed']  = 1;
            
            /* Added status 1 for NewJoinees*/
            $addEmployee['status']  = 1;
            
            $this->db->select('grades');
            $this->db->where('designation_id', $addEmployee['designation_id']);
            $this->db->from('designation');
            
            $queryGrade = $this->db->get();
            $grdData    = $queryGrade->row();
            if ($grdData->grades == 4 && $addEmployee['LocationID'] != 4) 
            {
                if(!$chatnamflag){
                    $EmpId                          = $this->employee_model->getverticalData($addEmployee['AssignedVertical']);
                    $Vertivalemployee_id               = $EmpId[0]['employee_id'];
                    $addEmployee['Primary_Lead_ID'] = $Vertivalemployee_id; 
                }
                else 
                {
                    $addEmployee['Primary_Lead_ID'] = 1297;
                }                           
            }
            if ($grdData->grades >= 4 && $addEmployee['LocationID'] != 4) 
            {
                if($addEmployee['moveonboard'] == 1){
                    $addEmployee['status']  = 3;
                }else{
                    $addEmployee['training_status'] = 1;
                    $addEmployee['training_needed']  = 0;
                }
            }
            $retVal         = $this->insert($addEmployee, 'employ');
            $lastInsertedID = $this->db->insert_id();
            
            /* Audit Log */
            $modfdId = $this->EmpId;
            $fromVert = "";
            $fromClient = "";
            $fromProcess = "";
            $fromReporting = "";
            $toVert = "";
            $toClient = "";
            $toProcess = "";
            $toReporting = "";
            $empTrnCnt = 0;
            $transDate = date('d-m-Y');
            $this->load->model('audit_log_model');
            $this->audit_log_model->employTransitionLog($lastInsertedID,$transDate,$modfdId,$fromVert,$fromClient,$fromProcess,$fromReporting,$toVert,$toClient,$toProcess,$toReporting, 1);
            
            /* End Of Audit Log*/

            /*Emp Shift*/

            $employee_id = $lastInsertedID;
            $client_id = 0;
            $from_date = $addEmployee['DateOfJoin'];
            $shift_id = $addEmployee['shift_id'];
            $this->db->insert('emp_shift');

            /*End of Emp Shift*/
                
            // echo $grdData->grades;
            // ------------- start taking all Primary_Lead_ID's to update their count 
            // while adding new Employee who's grade is >= 4 -------
            if ($grdData->grades >= 4 && $addEmployee['LocationID'] != 4) 
            {
                $primary_Lead_array = array();
                $primID = 0;                
                $EmpId  = $this->employee_model->getverticalData($addEmployee['AssignedVertical']);
                $emp    = $EmpId[0]['employee_id'];
                            
                array_push($primary_Lead_array,$emp);
                while($primID != 1296) 
                {                                       
                    $prim_ld = $this->employee_model->getEmployeegrade($emp);
                    $emp = $primID = $prim_ld[0]['Primary_Lead_ID'];                                                        
                        if (!in_array($primID, $primary_Lead_array))
                        array_push($primary_Lead_array,$primID);
                }                 
                $IncCounts = 1;
                $this->Hierarchycount_Model->hierarchyPull($primary_Lead_array,$IncCounts);
            }
            // ------------- end taking all Primary_Lead_ID's to update their count 
            // while adding new Employee who's grade is >= 4 -------
        
            if (!empty($lastInsertedID))
                $this->update(array(
                    'DeleteFlag' => 1
                ), array(
                    'employee_id' => $addEmployee['newjoinee_id']
                ), 'new_joinees');
            
            if ($grdData->grades < 4 && $addEmployee['LocationID'] != 4) 
            {
                $addEmployeeProcess = array(
                    'process_id' => INI_PROCESS_ID,
                    'employee_id' => $lastInsertedID,
                    'ShiftID' => SHIFT_ID,
                    'Grades' => $grdData->grades,
                    'StartDate'=>$addEmployee['DateOfJoin']
                );
                
                $this->insert($addEmployeeProcess, 'employprocess');
            }
            else if ($grdData->grades >= 4 && $addEmployee['LocationID'] != 4 ) 
            {
                if(isset($addEmployee['moveonboard'])){
                    if($addEmployee['moveonboard'] == 1){
                        $addEmployeeProcessLead = array(
                            'process_id' => ONBOARD_PROCESS_ID,
                            'employee_id' => $lastInsertedID,
                            'ShiftID' => SHIFT_ID,
                            'Grades' => $grdData->grades,
                            'StartDate'=>$addEmployee['DateOfJoin']
                        );
                        $this->insert($addEmployeeProcessLead, 'employprocess');
                    }
                }
            }
        
            $this->log_action('employ', 'add', $addEmployee['first_name']);
            return $retVal;
        } 
        else
            return -1;
    }

    function addEmployeeOnboarding($data = '')
    {
        $addEmployeeOnboarding = json_decode(trim($data), true);
        
        $retVal = $this->insert($addEmployeeOnboarding, 'employonboarding');
        return $retVal;
    }

    function ImportEmployee()
    {
        
        if (!empty($_FILES['UploadEmployeeXls']['name'])) 
        {
            $empData     = $this->input->cookie();
            $this->EmpId = $empData['employee_id'];
            
            $uploadPath  = $this->config->item('REPOSITORY_PATH').'resources/uploads/';
            $uploadResp  = $this->upload_files($_FILES, $uploadPath);
            $uploadData  = $this->upload->data();
            $empData     = $this->input->cookie();
            $this->EmpId = $empData['employee_id'];
            
            $this->load->library('CSVReader.php');
            $csvObj = new CSVReader();
            $data   = $csvObj->parse_file($uploadPath . $uploadData['file_name']);
            
            if (count($data) != 0) 
            {
                foreach ($data as $key => $val) 
                {
                    if (!empty($val['EmployeeID']) && !empty($val['OfficialEmail'])) 
                    {
                        $this->db->select('employee_id');
                        $empQry = $this->db->get_where('employ', array(
                            'company_employ_id' => $val['EmployeeID'],
                            'Email' => $val['OfficialEmail']
                        ));
                        $employ = $empQry->result_array();
                        
                        $field['company_employ_id'] = $val['EmployeeID'];
                        $field['first_name']       = $val['first_name'];
                        $field['last_name']        = $val['last_name'];
                        $field['Gender']          = $val['Gender'];
                        
                        $date1 = $val['BirthDate-dd/mm/yyyy'];
                        if (strpos($val['BirthDate-dd/mm/yyyy'], '/') !== false)
                            $date1 = str_replace("/", "-", $val['BirthDate-dd/mm/yyyy']);
                        $field['DateOfBirth'] = date('Y-m-d', strtotime($date1));
                        $date2                = $val['JoiningDate-dd/mm/yyyy'];
                        if (strpos($val['JoiningDate-dd/mm/yyyy'], '/') !== false)
                            $date2 = str_replace("/", "-", $val['JoiningDate-dd/mm/yyyy']);
                        $field['DateOfJoin'] = date('Y-m-d', strtotime($date2));
                        
                        $field['designation_id']    = $this->getDesigination($val['Designation']);
                        $field['Grade']            = $this->getGradeID($val['Designation']);
                        $field['Email']            = $val['OfficialEmail'];
                        $field['Mobile']           = $val['Mobile'];
                        $field['AlternateEmail']   = $val['AlternateEmail'];
                        $field['QualificationID']  = $this->getQualification($val['Qualification']);
                        $field['LocationID']       = $this->getLocation($val['OfficeLocation']);
                        $field['Address1']         = $val['PermanentAddress'];
                        $field['City']             = $val['PermanentCity'];
                        $field['State']            = $val['PermanentState'];
                        $field['Zip']              = $val['PermanentZip'];
                        $field['AddressSame']      = ($val['AddressSameAsPermanent'] == 'yes') ? 1 : 0;
                        $field['Address2']         = $val['PresentAddress'];
                        $field['City1']            = $val['PresentCity'];
                        $field['State1']           = $val['PresentState'];
                        $field['Zip1']             = $val['PresentZip'];
                        $field['UpdatedBy']        = $this->EmpId;
                        $field['UpdatedDate']      = date("Y-m-d H:i:s");
                        $field['assigned_vertical'] = $this->getVeticalID($val['AssignedVetical']);
                        //print_r($field);exit;
                        if ($empQry->num_rows() == 0) {
                            $retVal         = $this->insert($field, 'employ');
                            $lastInsertedID = $this->db->insert_id();
                            if ($field['Grade'] <= 3) {
                                $addEmployeeProcess = array(
                                    'process_id' => INI_PROCESS_ID,
                                    'employee_id' => $lastInsertedID
                                );
                                
                                $this->insert($addEmployeeProcess, 'employprocess');
                            }
                        } else {
                            $retVal = $this->update($field, array(
                                'company_employ_id' => $val['EmployeeID'],
                                'Email' => $val['OfficialEmail']
                            ), 'employ');
                        }
                    }
                }
                unlink($uploadPath . $uploadData['file_name']);
                return $retVal;
            }
            return false;
        }
    }

    function getDesigination($name)
    {
        $this->db->select('designation_id');
        $this->db->where('Name', $name);
        $this->db->from('designation');
        
        $queryGrade = $this->db->get();
        if (count($queryGrade->result_array()) != 0) {
            foreach ($queryGrade->result_array() as $key => $row) {
                if ($row['designation_id'] != "0")
                    return $row['designation_id'];
                else
                    return null;
            }
        } else
            return null;
    }

    function getGradeID($designationName)
    {
        $this->db->select('grades');
        $this->db->where('Name', $designationName);
        $this->db->from('designation');
        
        $queryGrade = $this->db->get();
        if (count($queryGrade->result_array()) != 0) {
            foreach ($queryGrade->result_array() as $key => $row) {
                if ($row['grades'] != "0")
                    return $row['grades'];
                else
                    return null;
            }
        } else
            return null;
    }

    function getVeticalID($verticalName)
    {
        $this->db->select('VerticalID');
        $this->db->where('Name', $verticalName);
        $this->db->from('Vertical');
        
        $queryVertical = $this->db->get();
        $Data          = $queryVertical->row();
        if ($Data->VerticalID != "0")
            return $Data->VerticalID;
        else
            return null;
    }

    function getQualification($name)
    {
        $this->db->select('QualificationID');
        $this->db->where('Name', $name);
        $this->db->from('qualifications');
        
        $queryGrade = $this->db->get();
        $Data       = $queryGrade->row();
        if ($Data->QualificationID != "0")
            return $Data->QualificationID;
        else
            return null;
    }

    function getLocation($name)
    {
        $this->db->select('LocationID');
        $this->db->where('Name', $name);
        $this->db->from('location');
        
        $queryGrade = $this->db->get();
        $Data       = $queryGrade->row();
        if ($Data->LocationID != "0")
            return $Data->LocationID;
        else
            return null;
    }

    function newJoinees_query()
    {
        $filter     = json_decode($this->input->get('filter'), true);
        $filterName = $this->input->get('filterName') ? $this->input->get('filterName') : "";
        
        $filterQuery       = $this->input->get('query');
        $options['filter'] = $filter;
        $filterWhere       = '';
        
        $sql = "SELECT `new_joinees`.*, `designation`.`Name` AS `Designation_Name` FROM `new_joinees` 
                LEFT JOIN `designation` ON `new_joinees`.`designation_id` = `designation`.`designation_id` WHERE `new_joinees`.DeleteFlag=0";
        
        if (isset($options['filter']) && $options['filter'] != '') {
            foreach ($options['filter'] as $filterArray) {
                $filterFieldExp = explode(',', $filterArray['property']);
                foreach ($filterFieldExp as $filterField) {
                    if ($filterField != '') {
                        $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                        $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                    }
                }
            }
            if ($filterWhere != '') {
                $sql .= " WHERE " . $filterWhere;
            }
        }
        if ($filterName != "") {
            $filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
            $sql .= " and " . $filterWhere;
        }
        return $sql;
    }


    function getNewjoineesList()
    {
        
        $options = array();
        
        if ($this->input->get('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        $sort                     = json_decode($this->input->get('sort'), true);
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        $sql      = $this->newJoinees_query();
        $totQuery = $this->db->query($sql);
        $totCount = $totQuery->num_rows();
        $sql      = $this->newJoinees_query();
        
        if (isset($options['sortBy'])) {
            $sql .= " order by " . $options['sortBy'] . " " . $options['sortDirection'];
        }
        if (isset($options['limit']) && isset($options['offset'])) {
            $sql .= " limit " . $options['offset'] . " , " . $options['limit'];
        } else if (isset($options['limit'])) {
            $sql .= " limit " . $options['limit'];
        }
        $query = $this->db->query($sql);
        $data  = $query->result_array();
        
        $results = array_merge(array(
            'totalCount' => $totCount
        ), array(
            'rows' => $data
        ));
        return $results;
    }


    function getEmployeeNotifications($id) 
    {
        if(is_numeric($id)) 
        {
            $res          = array();
            $Valid_Date   = date('Y-m-d', strtotime('-7 day'));
            $sql          = "SELECT NotificationID, NotificationDate, ReadFlag, Type FROM notification 
            WHERE(LOCATE(',".$id.",', CONCAT(',',ReceiverEmployee,','))) AND ReadFlag = '0'";
            $query        = $this->db->query($sql);
            $finalData = array();
            /* START code for resticting future date MOM notifications count */
            $data = $query->result_array();            
            date_default_timezone_set('Asia/Kolkata'); //set timezone                                 
            foreach ($data as $key=>$value) //reconstruct finalData array to remove future date MOM notifications for count and data
            {
                $finalData[$key]['NotificationID'] = $value['NotificationID'];
                $finalData[$key]['ReadFlag'] = $value['ReadFlag'];            		
            }
            
            /* END code for resticting future date MOM notifications count */
            $finalData = array_values($finalData);                      
            $res['count'] = count($finalData);
            $res['rflag'] = $finalData;
            return $res;
        }
    }


    function getNotifications()
    {
        $limitoten = $this->input->get('limitoten');
        $options = array();
        if ($this->input->get('hasNoLimit') != 1) {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 15;
        }
        $sort                     = json_decode($this->input->get('sort'), true);
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        $res        = $this->buildNotify_query();
        $totquery   = $this->db->query($res);
        $totCount   = $totquery->num_rows();
        
        $res        = $this->buildNotify_query();

        //This limit for Notfication pop up display only top ten records
        if($limitoten == 'limitoten' && isset($limitoten)) {
            $Valid_Date = date('Y-m-d', strtotime('-7 day'));
            // $res .= " AND n.NotificationDate >= ". $Valid_Date . " ORDER BY  n.NotificationDate DESC ";
            $res .= " ORDER BY  n.NotificationDate DESC ";
            $options['offset'] = 0;
            $options['limit']  = 10;
        }
        
        if ($this->input->get('sort') != "") {
            $sort                     = json_decode($this->input->get('sort'), true);
            $options['sortBy']        = $sort[0]['property'];
            $options['sortDirection'] = $sort[0]['direction'];
            $sort                     = $options['sortBy'] . ' ' . $options['sortDirection'];
            $res .= " order by " .  $sort;
        }

        if (isset($options['limit']) && isset($options['offset'])) {
            $limit = $options['offset'] . ',' . $options['limit'];
            $res .= " limit " . $limit;
        } elseif (isset($options['limit'])) {
            $res .= " limit " . $options['limit'];
        }
        $query = $this->db->query($res);
        $data  = $query->result_array();
        
        /* START code for resticting future date MOM notifications */
        $finalData = array();       
        date_default_timezone_set('Asia/Kolkata');
        
        //Construct new array containing only current date and below Mom notifications
        foreach ($data as $key=>$value)
        {
            $splitTimeStamp = explode(" ", $value['NotificationDate']);
            $form           = str_replace('-', '/', $splitTimeStamp[0]);
            $nd             = date("d-M-Y", strtotime($form));
            $finalData[$key]['NotificationID']      = $value['NotificationID'];
            $finalData[$key]['Title'] = $value['Title'];
            $finalData[$key]['Description'] = $value['Description'];
            $finalData[$key]['SenderEmployee'] = $value['SenderEmployee'];
            $finalData[$key]['ReceiverEmployee'] = $value['ReceiverEmployee'];
            $finalData[$key]['NotificationDate'] = $nd .' '. $splitTimeStamp[1];;
            $finalData[$key]['ReadFlag'] = $value['ReadFlag'];
            $finalData[$key]['ReadDate'] = $value['ReadDate'];
            $finalData[$key]['Type'] = $value['Type'];
            $finalData[$key]['employee_id'] = $value['employee_id'];
            $finalData[$key]['first_name'] = $value['first_name'];
            $finalData[$key]['last_name'] = $value['last_name'];
        }  
        
        /* END code for resticting future date MOM notifications */
        //ob_start();
        
        $finalData = array_values($finalData);  

        $results = array_merge(array(
            'totalCount' => count($finalData) //$totCount
        ), array(
            'rows' => $finalData
        ));               
        return $results;
    }


    function getApproverDetails()
    {
        $this->db->select('process_id,employee_id');
        $this->db->from('process_approver');
        $where = "employee_id = '" . $this->input->get('employee_id') . "'";
        $this->db->where($where);
        $query   = $this->db->get();
        $count   = $query->num_rows();
        $results = array_merge(array(
            'totalCount' => $count
        ), array(
            'rows' => $query->result_array()
        ));
        if ($count > 0)
            return $results;
        else
            return false;
    }


    function getReporteesDetails()
    {
        $this->db->select('employee_id');
        $this->db->from('employ');
        $where = "Primary_Lead_ID = '" . $this->input->get('Primary_Lead_ID') . "' AND relieve_flag != 1";
        $this->db->where($where);
        $query   = $this->db->get();
        $count   = $query->num_rows();
        $results = array_merge(array(
            'totalCount' => $count
        ), array(
            'rows' => $query->result_array()
        ));
        if ($count > 0)
            return $results;
        else
            return false;
    }

    function updateNotificationStatus($data = '')
    {
        $empid  = $data['empid'];
        
        date_default_timezone_set('Asia/Kolkata');
        $Current_Date = date("Y-m-d h:i:s");
        $retVal = $this->db->query("UPDATE `notification` SET `ReadFlag`= 1, `ReadDate`='" . $Current_Date . "' WHERE (LOCATE(',".$empid.",', CONCAT(',',ReceiverEmployee,',')))");
        
        return $retVal;
    }

    //Below is the function which inserts data into emp_log table
    function updateEmpLog($data = '')
    {
        $retVal = $this->insert($data, 'emp_log');
        return $retVal;
    }

    function updateDeligateTo($idVal, $data)
    {
        $del             = "";
        $setData         = json_decode(trim($data), true);
        $Empdetails      = $this->employee_model->getEmployeegrade($setData['employee_id']);
        $empid           = $Empdetails[0]['employee_id'];
        $grades          = $Empdetails[0]['grades'];
        $Primary_Lead_ID = $Empdetails[0]['Primary_Lead_ID'];
        
        /* added for Last modifiedby and date to track in Employ table*/
            $userData            = $this->input->cookie();
            $Last_ModifiedBy     = $userData["user_data"]['employee_id'];
            $Last_Modified_Date  = date("Y-m-d H:i:s");
            
        if ($setData['DeligateTo'] != "" && $grades > 2) 
        {
            $deligatedetails         = $this->employee_model->getEmployeegrade($setData['DeligateTo']); //print_r($deligatedetails);exit;
            $deligateid              = $deligatedetails[0]['employee_id'];
            $deligategrades          = $deligatedetails[0]['grades'];
            $deligatePrimary_Lead_ID = $deligatedetails[0]['Primary_Lead_ID'];
            $query                   = "UPDATE employ SET DeligateTo='" . $setData['DeligateTo'] . "',Last_ModifiedBy=".$Last_ModifiedBy.",Last_Modified_Date='".$Last_Modified_Date."' WHERE employee_id='" . $empid . "'";
            $retVal                  = $this->db->query($query);
            if ($grades == 2) {
                
                $query  = "DELETE process_approver WHERE employee_id='" . $empid . "'";
                $retVal = $this->db->query($query);
            }
            if ($grades >= 3 && $grades < 4) {
                $ProcessQuery = "select process_id from employeesprocess WHERE employee_id='" . $empid . "'";
                $query        = $this->db->query($ProcessQuery);
                $data         = $query->result_array();
                
                foreach ($data as $key => $entry) {
                    $process_id = $data[$key]['process_id'];
                    if ($deligategrades >= 3 && $deligategrades < 4) {
                        $PQuery = "select process_id from  employprocess WHERE employee_id='" . $setData['DeligateTo'] . "' and process_id='" . $process_id . "'";
                        $PVal   = $this->db->query($PQuery);
                        if ($PVal->num_rows() > 0) {
                            $query  = "Delete from  employprocess WHERE employee_id='" . $empid . "' and process_id='" . $process_id . "'";
                            $retVal = $this->db->query($query);
                            
                        } else {
                            $query  = "update employprocess SET employee_id='" . $setData['DeligateTo'] . "' WHERE employee_id='" . $empid . "' and process_id='" . $process_id . "'";
                            $retVal = $this->db->query($query);
                        }
                    } else {
                        $query  = "Delete from  employprocess WHERE employee_id='" . $empid . "' and process_id='" . $process_id . "'";
                        $retVal = $this->db->query($query);
                    }
                }
                
            }
            if ($grades == 4) {
                if ($deligategrades == 4) {
                    $query  = "UPDATE employprocess SET employee_id='" . $setData['DeligateTo'] . "' WHERE employee_id='" . $empid . "'";
                    $retVal = $this->db->query($query);
                } else {
                    $query  = "Delete from  employprocess WHERE employee_id='" . $empid . "'";
                    $retVal = $this->db->query($query);
                }
            } //Added for date updation
            if ($grades >= 5) {
                $query  = "UPDATE verticalmanager SET employee_id='" . $setData['DeligateTo'] . "' WHERE employee_id='" . $empid . "'";
                $retVal = $this->db->query($query);
            }
            $query  = "UPDATE employ SET Primary_Lead_ID='" . $setData['DeligateTo'] . "' WHERE Primary_Lead_ID='" . $empid . "'";
            $retVal = $this->db->query($query);
            
            $query  = "UPDATE employprocess SET Primary_Lead_ID='" . $setData['DeligateTo'] . "' WHERE Primary_Lead_ID='" . $empid . "'";
            $retVal = $this->db->query($query);
            
            $empData     = $this->input->cookie();
            $this->EmpId = $empData['employee_id'];
            
            $setData['UpdatedBy']   = $this->EmpId;
            $setData['UpdatedDate'] = date("Y-m-d H:i:s");
            $del                    = " ,DeligateTo='" . $setData['DeligateTo'] . "'";
            
        }
        if ($setData['relieve_flag'] == 1) {
            $RelieveDate = date("Y-m-d", strtotime($setData['RelieveDate']));
            $query       = "UPDATE employ SET relieve_flag=1, RelieveDate='" . $RelieveDate . "',RelieveNotes='" . $setData['RelieveNotes'] . "' " . $del . ",Last_ModifiedBy=".$Last_ModifiedBy.",Last_Modified_Date='".$Last_Modified_Date."'  WHERE employee_id='" . $empid . "'";
            $retVal      = $this->db->query($query);
            
        }
        
        if ($grades < 3) 
        {
            // ------------- start taking all Primary_Lead_ID's to update their count-------
                $primary_Lead_array = array();
                $emp = $empid;
                
                $primID = 0;
                while($primID != 1296) 
                {
                    $prim_ld = $this->employee_model->getEmployeegrade($emp);
                    $emp = $primID = $prim_ld[0]['Primary_Lead_ID'];
                        array_push($primary_Lead_array,$primID);
                }
                $primary_Lead_Count_array = array_count_values($primary_Lead_array);
            // ------------- start taking all Primary_Lead_ID's to update their count while moving TLs-------
            
            // ------- Start updating the count of all primary_Lead's -----------
            $this->Hierarchycount_Model->hierarchyPush($primary_Lead_Count_array);
            // ------- End updating the count of all primary_Lead's -----------
        }
        
        
        // save data to action log
        $UpdatedBy           = $userData["user_data"]['employee_id'];
        $logArray            = array(
            'RelieveNotes' => $setData['RelieveNotes'],
            'delegateTo' => $setData['DeligateTo'],
            'relieveDate' => $setData['RelieveDate']
        );
        $logArray_serialized = serialize($logArray);
        $logData             = array(
            'EmpID' => $empid,
            'date' => date('Y-m-d'),
            'empActions' => $logArray_serialized,
            'updatedBy' => $UpdatedBy
        );
        $result              = $this->employee_model->updateEmpLog($logData);
        return $retVal;
        
    }


    function get_DeligateTo()
    {
        $userData = $this->input->cookie();
        $employee_id = $userData["user_data"]['employee_id'];
        $sqls     = "SELECT employee_id,CONCAT(employees.first_name,' ' , employees.last_name) as Name from employees WHERE 1";
        $employeeID=$this->input->get('employee_id');
            $Empdetails       = $this->employee_model->getEmployeegrade($employeeID);
            $empid            = $Empdetails[0]['employee_id'];
            $grades           = $Empdetails[0]['grades'];
            $Primary_Lead_ID           = $Empdetails[0]['Primary_Lead_ID'];
            $AssignedVertical = $this->input->get('AssignedVertical');          
        /* if ($grades == 2) {
            $sqls = "SELECT employees.employee_id as employee_id,CONCAT(employees.first_name,' ' , employees.last_name) AS Name from employees
                    JOIN designation ON designation.designation_id=employees.designation_id
                    WHERE employee_id!='" . $empid . "'  AND relieve_flag=0  AND designation.grades>=2 AND employee_id IN (SELECT employprocess.employee_id from employeesprocess
                    WHERE process_id IN (SELECT employprocess.process_id from employeesprocess JOIN process_approver ON employprocess.employee_id=process_approver.employee_id
                    WHERE process_approver.employee_id='" . $empid . "'))";
            
        }
        if ($grades >= 3 && $grades < 4) {
            
            $sqls = "SELECT employee_id,CONCAT(employees.first_name,' ' , employees.last_name) AS Name from employees 
                WHERE employee_id!='" . $empid . "'  AND relieve_flag=0 AND (employee_id IN (SELECT employee_id from employees JOIN designation ON designation.designation_id=employees.designation_id WHERE Primary_Lead_ID IN (SELECT Primary_Lead_ID from employees WHERE employee_id='" . $empid . "')  AND (grades >=3 AND grades <4)) OR (employee_id IN (SELECT Primary_Lead_ID from employees WHERE employee_id='" . $empid . "')))";
            
        }
        if ($grades == 4) {
    // Loading all the employees with grade 4 and 5 WRT Primary_Lead_ID
            $sqls = "SELECT DISTINCT employee_id,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Name`,designation.grades from employees
                JOIN designation ON designation.designation_id=employees.designation_id
                WHERE employee_id !='" . $empid . "' AND  relieve_flag=0 AND designation.grades IN (4,5)
                AND (Primary_Lead_ID IS NOT NULL) AND Primary_Lead_ID='" . $Primary_Lead_ID . "'
                OR employee_id IN (SELECT employee_id FROM `verticalmanager` WHERE `VerticalID`='".$AssignedVertical."')";
            
        }
        if ($grades >= 5) {
            $sqls = "SELECT employee_id,CONCAT(employees.first_name,' ' , employees.last_name) AS Name,designation.grades from employees
                JOIN designation ON designation.designation_id=employees.designation_id
                WHERE relieve_flag=0 AND employee_id!='" . $empid . "' AND designation.grades>='" . $grades . "' ";
            
        } */
        $query = $this->input->get('query');
        if ($query != '') {
            $sqls .= " AND (first_name like '" . $query . "%' OR last_name='" . $query . "%')";
        }
        
        $options = array();
        if ($this->input->get_post('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        
        $sort = json_decode($this->input->get('sort'), true);
        
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        $totQuery = $this->db->query($sqls);
        $totCount = $totQuery->num_rows();
        
        
        $sql = $sqls;
        if ($this->input->get('query') != "") {
            $sql .= " and (employees.first_name like '" . $this->input->get('query') . "%')";
        }
        if (isset($options['sortBy'])) {
            $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
            $sql .= " order by " . $sort;
        }
        
        $query   = $this->db->query($sql);
        $data    = $query->result_array();
        $results = array_merge(array(
            'totalCount' => $totCount
        ), array(
            'rows' => $data
        ));
        
        
        return $results;
    }
    function getReportToDetails()
    {
        $sql = "SELECT employee_id,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS Name from employees JOIN designation ON designation.designation_id=employees.designation_id WHERE relieve_flag=0  ";
        if ($this->input->get('designationVal')) {
            $Empgrade       = $this->employee_model->getgrade($this->input->get('designationVal'));
            $grades         = $Empgrade[0]['grades'];
            $designationVal = $this->input->get('designationVal');
            $sql .= " AND designation.grades>'" . $grades . "'";
        }
        $sqls    = $sql;
        $options = array();
        
        if ($this->input->get_post('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        
        $sort = json_decode($this->input->get('sort'), true);
        
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        $totQuery = $this->db->query($sql);
        $totCount = $totQuery->num_rows();
        
        $sql = $sqls;
        if ($this->input->get('query') != "") {
            $sql .= " and (employees.first_name like '" . $this->input->get('query') . "%')";
        }
        if (isset($options['sortBy'])) {
            $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
            $sql .= " order by " . $sort;
        }
        
        $query   = $this->db->query($sql);
        $data    = $query->result_array();
        $results = array_merge(array(
            'totalCount' => $totCount
        ), array(
            'rows' => $data
        ));
        
        
        return $results;
    }

    public function getOldVertical($eid)
    {
        if (!empty($eid)) {
            $this->db->select('assigned_vertical');
            $this->db->where('employee_id', $eid);
            $this->db->from('employ');
            $query = $this->db->get();
            $res   = $query->result_array();
            return $res[0]['assigned_vertical'];
        }
    }

    function getSupervisors()
    {
        $userData   = $this->input->cookie();
        $employee_id   = $userData["user_data"]['employee_id'];
        $Empdetails = $this->employee_model->getEmployeegrade($employee_id);
        $grades     = $Empdetails[0]['grades'];
        
        $sql = "SELECT employees.`employee_id`, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Name from employees
                    JOIN designation ON employees.designation_id = designation.designation_id
                    WHERE employees.relieve_flag=0 AND designation.grades >" . $grades . "  ORDER BY first_name";
        
        $query = $this->db->query($sql);
                                
        $results = array_merge(array(
                'data'   => $query->result_array()
        ), array(
                'supervisors' =>  $this->getEmployeeSupervisors($employee_id) //get getEmployeeSupervisors
        ));
        
        return $results;
    }

    /* function to get employee supervisors */
    function getEmployeeSupervisors($employee_id)
    {
        $reporteesID = array();
        $finalData = array();
        $sql = "SELECT  transitionData from employees_transition  where employee_id ='" . $employee_id . "'";        
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) 
        {                        	
            $jsonArray = $query->result_array();        	
            $unserializedData = unserialize($jsonArray[0]['transitionData']);       	       	
            $threeMonthsAgoDate = date("d-m-Y",mktime(0,0,0,date("m")-3,date("d")));         	
            $dataLength = count($unserializedData);        	 
            for($i=0; $i<$dataLength;$i++)
            {       		
                if($unserializedData[$i]['transition_date'] != '')	
                {
                    if(strtotime($unserializedData[$i]['transition_date']) >= strtotime($threeMonthsAgoDate))
                    {
                        if ($unserializedData[$i]['to_reporting'] != '')
                        {
                            $reporteesID[] = $unserializedData[$i]['to_reporting'];
                            $reporteesID[] = $unserializedData[$i]['from_reporting'];
                        }
                    }
                }	        		
            }       	        	       	            	
        }      
        $reporteesID[] = $this->getDirectSupervisor($employee_id);        
        $reporteesID = array_unique($reporteesID);
                    
        if(!empty($reporteesID))
        {
            foreach ($reporteesID as $repID)
            {
                $finalData[] = $this->getSupervisorDetails($repID);
            }
        }                
        return $finalData;       
    }

    //function to get supervisor details by $repID
    public function getSupervisorDetails($repID)
    {
        if($repID != '')
        {
            $sql = "SELECT `company_employ_id` as employee_id, CONCAT(IFNULL(`first_name`, ''), ' ', IFNULL(`last_name`, '')) AS Name  FROM `employ` WHERE `employee_id` ='" . $repID . "'";
            $query = $this->db->query($sql);
            return $query->row_array();
        }
        
    }

    //function to get direct supervisor
    public function getDirectSupervisor($employee_id)
    {
        $sql = "SELECT `Primary_Lead_ID` FROM `employ` WHERE `employee_id` ='" . $employee_id . "'";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        return $result['Primary_Lead_ID'];
    }

    public function buildNotify_query()
    {
        $filter     = json_decode($this->input->get('filter'), true);
        $empData    = $this->input->cookie();
        if(isset($empData['employee_id'])) {
            $sql = 'SELECT `n`.`NotificationID`, 
            `n`.`Title`, `n`.`Description`, `n`.`SenderEmployee`,
            `n`.`ReceiverEmployee`, DATE_FORMAT(`n`.NotificationDate, "%m-%d-%Y %T") AS NotificationDate,
            `n`.`ReadFlag`, `n`.`ReadDate`,`n`.`Type`, `e`.`employee_id`,
            `e`.`first_name`, `e`.`last_name`, n.Type FROM (`notification` n) 
            LEFT JOIN `employ` e ON `e`.`employee_id` = `n`.`SenderEmployee` 
            WHERE (LOCATE(",'.$empData["employee_id"].',", CONCAT(",",n.ReceiverEmployee,",")))';

            return $sql;
        }
        
    }
    public function addresignation($data){
        $userData   = $this->input->cookie();
        $updatedByemployee_id   = $userData["user_data"]['employee_id'];
        $oldNotesSql = $this->db->get_where('employ',array('employee_id' => $data['employee_id']));
        $resultNotes = $oldNotesSql->row_array();              
        $data['Last_ModifiedBy']    = $updatedByemployee_id;
        $data['Last_Modified_Date'] = date("Y-m-d H:i:s");
        $data['Resigned_Date'] = date('Y-m-d',strtotime($data['Resigned_Date']));
        
        //$oldNotes = json_decode($resultNotes['Resigned_Notes']);
        
        if($data['Revoked_Notes'] == ''){
            $data['Resign_Flag'] = 1;
            $data['Resigned_Notes'] = $data['Resigned_Notes'];
            //$notes = 'Revoked_Notes';
        }
        else
        {
            $data['Resign_Flag'] = 0;
            $data['Resigned_Notes'] = $data['Revoked_Notes'];
            //$notes = 'Resigned_Notes';
        }

        $retVal =  $this->update($data, array(
                                'employee_id' => $data['employee_id']                               
                            ), 'employ');
        return $retVal;
    }

    public function buildRelivedEmployee_query($para) {
        $empData            = $this->input->cookie();
        $grade              = $empData['Grade'];     
        $year               = $this->input->get('yearStatus');
        $monthStatus        = $this->input->get('monthStatus');
        $AssignedVertical   = $this->input->get('assignedvert');
        $verticalArray      = $this->getVertical();
        $filter             = json_decode($this->input->get('filter'), true);
        $options['filter']  = $filter;
        $loggedInUserGrade  = $empData['Grade'];
        $pverticalCombo   = $this->input->get('pverticalCombo');
        $verticalId = ($this->input->get('vertical') != '')? $this->input->get('vertical') : '';
        if(isset($para) && $para == 'relivednames' ) {
            $sql = "SELECT company_employ_id AS empid, 
                    Concat(Ifnull(employees.`first_name`, ''), ' ', Ifnull(employees.`last_name`, '') 
                    ) AS 
                    empname, 
                    DATE_FORMAT(dateofjoin, '%d-%m-%Y') AS dateofjoin, 
                    DATE_FORMAT(relievedate, '%d-%m-%Y') AS relievedate 
            FROM   employ 
            WHERE  Year(relievedate)        = $year 
                    AND Month(relievedate)   = $monthStatus
                    AND relieve_flag          = 1
                    AND assigned_vertical  IN (". $AssignedVertical. ") ";
            if($grade >=3 && $grade < 4) {
                $sql .= " AND Primary_Lead_ID IN (". $empData['employee_id'].") ";
            }
            if($grade >= 4 && $grade < 5) {
                $leads = $this->getTeamLeads();
                array_push($leads, $empData['employee_id']);
                if(isset($leads) && count($leads) >= 1) {
                    $sql  .= " AND Primary_Lead_ID IN (". implode(",",$leads ).") ";
                } else {
                    $sql  .= " AND Primary_Lead_ID IN (".$empData['employee_id'].")";
                }
                    
            }
            if($grade >= 5 && $grade < 6) {
                $leads    = $this->getTeamLeads();
                $managers = $this->getTeamManagers();
                $pl       = array_unique(array_merge($leads, $managers));
                array_push($pl, $empData['employee_id']);
                if(isset($leads) && count($leads) >= 1) {
                    $sql  .= " AND Primary_Lead_ID IN (".implode(",",$pl ).")";
                } else {
                    $sql  .= " AND Primary_Lead_ID IN (".$empData['employee_id'].")";
                }
            }
        }
        if(isset($para) && $para == 'relivedcount' ) {
            $sql  = "SELECT Month(relievedate) AS Monthid, assigned_vertical, 
                        vertical.Name AS VerticalName,
                        MONTHNAME(relievedate ) AS Month_Name,
                        Count(relieve_flag) AS RelivedEmployees, 
                    ( Count(relieve_flag) * 100 / (SELECT Count(employee_id) from employees) ) AS Relived_Percentage 
            FROM     employ 
            JOIN     verticals AS vertical ON (employees.assigned_vertical = vertical.VerticalID)
            WHERE    relieve_flag = 1 
                        AND Year(relievedate) = $year";
            if($grade >=3 && $grade < 4) {
                $sql .= " AND Primary_Lead_ID IN (". $empData['employee_id'].") ";
            }
            if($grade >= 4 && $grade < 5) {
                $leads = $this->getTeamLeads();
                array_push($leads, $empData['employee_id']);
                if(isset($leads) && count($leads) >= 1) {
                    $sql  .= " AND Primary_Lead_ID IN (". implode(",",$leads ).") ";
                } else {
                    $sql  .= " AND Primary_Lead_ID IN (".$empData['employee_id'].")";
                }
            }
            if($grade >= 5 && $grade < 6) {
                $leads    = $this->getTeamLeads();
                $managers = $this->getTeamManagers();
                $pl       = array_unique(array_merge($leads, $managers));
                array_push($pl, $empData['employee_id']);
                if(isset($leads) && count($leads) >= 1) {
                    $sql  .= " AND Primary_Lead_ID IN (".implode(",",$pl ).")";
                } else {
                    $sql  .= " AND Primary_Lead_ID IN (".$empData['employee_id'].")";
                }
            }
            $filterWhere = '';
            if (isset($options['filter']) && $options['filter'] != '') {
                foreach ($options['filter'] as $filterArray) {
                $filterFieldExp = explode(',', $filterArray['property']);
                foreach ($filterFieldExp as $filterField) {
                    if ($filterField != '') {
                        if($filterField == 'VerticalName'){
                            $filterField = 'vertical.Name';
                        }
                        $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                        $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                    }
                }
            }
                if ($filterWhere != '') {
                    $sql .= " AND (" . $filterWhere . ")";
                }
            }
            
            if($pverticalCombo != '' || $verticalId != ''){
                if($verticalId != '')
                    $pverticalCombo = $verticalId;
                $Verticals = $this->Vertical_Model->getVerticalsForParentID($pverticalCombo);    
                if($Verticals != '')        
                    $sql .=" AND  vertical.VerticalID IN ($Verticals) ";
            }
            $sql .=" GROUP BY Monthid,vertical.VerticalID ";
        }
        return $sql;
    }

    public function getRelivedEmployee($para) {
        $sort                     = json_decode($this->input->get('sort'), true);
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];

        $sql        = $this->buildRelivedEmployee_query($para);
        $totquery   = $this->db->query($sql);
        $totCount   = $totquery->num_rows();

        if($para != 'relivedcount') {
            if (isset($options['sortBy'])) {
                $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
                if($sort == 'Month_Name ASC') {
                    $sort = ' Monthid ASC ';
                }
                if($sort == 'Month_Name DESC') {
                    $sort = ' Monthid DESC ';
                }
                $sql .= " order by " . $sort;
            }
        }
        
        if (isset($options['limit']) && isset($options['offset'])) {
            $limit = $options['offset'] . ',' . $options['limit'];
            $sql  .= " limit " . $limit;
        } elseif (isset($options['limit'])) {
            $sql .= " limit " . $options['limit'];
        }
        $query   = $this->db->query($sql);
        $data    = $query->result_array();
        $totrelivedpercentage   =   "";
        $totrelivedcounts       =   "";
        foreach ($data as $key => $value) {
            if($para == 'relivedcount') {
                $totrelivedpercentage   += $value['Relived_Percentage'];
                $totrelivedcounts       += $value['RelivedEmployees'];
            }
            if(isset($value['dateofjoin'])) {
                $data[$key]['dateofjoin']   = $this->commonDateSetting($value['dateofjoin']);
            }
            if(isset($value['relievedate'])) {
                $data[$key]['relievedate']  = $this->commonDateSetting($value['relievedate']);
            }
        }
        
        $results = array_merge(array(
            'totalCount'        => $totCount,
            'totrelivedper'     => round($totrelivedpercentage, 2),
            'totrelivedcount'   => $totrelivedcounts
        ), array(
            'data' => $data
        ));
        
        return $results;
    }

    public function getTeamLeads()
    {
        $getProcess = $this->getProcess();
        $sql        = "SELECT employee_id from employeesprocess WHERE process_id IN (". implode(',', $getProcess).") AND Grades > 2 AND Grades < 4 GROUP BY employee_id";
        if(isset($sql)) {
            $result_select = $this->db->query($sql);
            $leadsArray    = array();
            if(isset($result_select) && count($result_select->result_array())!=0) {
                foreach($result_select->result_array() as $key => $val)
                    array_push($leadsArray, $val['employee_id']);
            }
        }
        return $leadsArray;
    }

    public function getTeamManagers()
    {
        $getProcess = $this->getProcess();
        $sql        = "SELECT employee_id from employeesprocess WHERE process_id IN (". implode(',', $getProcess).") AND Grades >= 4 GROUP BY employee_id";
        if(isset($sql)) {
            $result_select   = $this->db->query($sql);
            $managerArray    = array();
            if(isset($result_select) && count($result_select->result_array())!=0) {
                foreach($result_select->result_array() as $key => $val)
                    array_push($managerArray, $val['employee_id']);
            }
        }
        return $managerArray;
    }

    public function getEmployeeVerticals()
    {
        $data         = array();
        $empData      = $this->input->cookie();
        $employee_id     = $empData['employee_id'];
        $grade        = $empData['Grade'];
        $asignedvert  = $empData['Vert'];
        if($asignedvert != 0) {
            $verticals = array($asignedvert);
            if($grade >= 4 && $grade <7) {
                $data = $this->getVertical();
                $verticals= array_unique(array_merge($verticals,$data));
            }
            return $verticals;
        } else {
            return $data;
        }
    }

    public function getTlEmployees(){
        $empData = $this->input->cookie();
        $EmpID  = $empData['employee_id'];
        $this->db->select("employees.employee_id,CONCAT_WS(' ',employees.first_name,employees.last_name) as first_name,employees.Email, employees.designation_id ,designation.Name,designation.grades",FALSE)->from("employ")
        ->join('designation', 'employees.designation_id = designation.designation_id')
        ->where('employees.employee_id NOT IN','(SELECT employee_id from employeesprocess WHERE process_id IN('.INI_PROCESS_ID.','.ONBOARD_PROCESS_ID.'))', false)
        ->where('employees.Primary_Lead_ID =',$EmpID)
        ->where('employees.Primary_Lead_ID !=1')
        ->where('employees.relieve_flag = 0')
        ->order_by('employees.first_name');
        $query = $this->db->get();
        if($query->num_rows() == 0) return FALSE;
        return $query->result_array();      
    }

    public function getTlBillableEmployees($process_id) {
        $empData = $this->input->cookie();
        $EmpID  = $empData['employee_id'];
        $this->db->select("e.employee_id,e.Primary_Lead_Id,CONCAT_WS(' ',e.first_name,e.last_name) as first_name,ep.Billable,eb.billabilityData,e.Comments as comment,e.Email, e.designation_id ,d.Name,d.grades",FALSE)->from("employ e")
        ->join('employprocess ep', 'e.employee_id = ep.employee_id')
        ->join('designation d', 'e.designation_id = d.designation_id')
        ->join('employ_billability eb', 'e.employee_id = eb.employee_id')
        ->where('ep.Billable =',1)
        ->where('ep.Grades <',3)
        ->where('ep.process_id =',$process_id)
        ->where('e.relieve_flag = 0')
        ->order_by('e.first_name');
        $query = $this->db->get();
        if($query->num_rows() == 0) return FALSE;
        return $query->result_array();  
    }

    public function getLeadforClients($clientID){
        $sql = "SELECT DISTINCT Leads.* FROM (
                (SELECT CONCAT(e.first_name,' ',e.last_name) AS lead,email
                from employees e
                JOIN employ_client ON employ_client.EmployID = e.employee_id
                JOIN designation d ON d.designation_id = e.designation_id 
                JOIN CLIENT ON process.clientID = client.clientID
                    WHERE d.grades > 2 AND d.grades < 4   AND e.relieve_flag = 0 AND client.ClientID = $clientID				 
                    ) 
                    )
                
                AS Leads ";
        $result_select   = $this->db->query($sql);
        
        if(isset($result_select) && count($result_select->result_array())!=0) {	                                   
            return $result_select->result_array();
        }else
            return "";    
    }    
    public function getAMforClients($clientID){
        $sql= "SELECT DISTINCT e.employee_id,
                CONCAT(e.first_name, ' ', IFNULL(e.last_name, '')) AS AM,
                e.email            AS amEmail 
                from employeesprocess ep,
                PROCESS p,
                employ e,
                CLIENT c
                WHERE ep.process_id   = p.process_id
                AND ep.employee_id  = e.employee_id
                AND e.relieve_flag  = 0      
                AND p.clientid   = c.clientid
                AND (ep.grades   =4 ) 
                AND c.ClientID = $clientID      
                GROUP BY e.company_employ_id ";
        $result_select   = $this->db->query($sql);
        
        if(isset($result_select) && count($result_select->result_array())!=0) {	                                   
            return $result_select->result_array();
        }else
            return "";
    }
    public function getEmployeeListforRDR(){
            $clientid = ($this->input->get('comboClient'))?$this->input->get('comboClient'):0;
            $process_id = ($this->input->get('comboProcess'))?$this->input->get('comboProcess'):0;
            $employArray = $this->getAllEmployee($clientid, $process_id);
            $this->db->select('employees.*, CAST(SUBSTRING_INDEX(employees.company_employ_id, "-", -1) AS UNSIGNED INTEGER) AS CID, designation.Name AS Designation_Name,designation.grades AS Grade, location.Name AS Location_Name, ShiftCode', false);
            $this->db->from('employ');
            $this->db->join('employprocess', 'employees.employee_id = employprocess.employee_id','left');
            $this->db->join('verticalmanager', 'employees.employee_id = verticalmanager.employee_id','left');
            $this->db->join('shift', 'shift.ShiftID = employprocess.ShiftID OR shift.ShiftID = verticalmanager.ShiftID','left');
            $this->db->join('designation', 'employees.designation_id = designation.designation_id');
            $this->db->join('location', 'employees.LocationID = location.LocationID','left');
            $this->db->where('employees.relieve_flag', 0);
            $this->db->order_by('employees.first_name', 'ASC');
            $this->db->group_by('`employees`.`employee_id`');
            if (is_array($employArray) && count($employArray) != 0)
            $this->db->where_in('employees.employee_id', $employArray);
            $query = $this->db->get();
            $data = $query->result_array();
            $totCount = $query->num_rows();
                $results = array_merge(array(
            'totalCount' => $totCount
            ), array(
                'data' => $data
            ));
            return $results;
    }

    public function getEmployeesData()
    {
        $startDate = $this->input->get('startDate') ? $this->input->get('startDate') : 0;
        $endDate = $this->input->get('endDate') ? $this->input->get('endDate') : 0;    	    	
        
        $options = array();
        if ($this->input->get('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        
        if ($startDate == 0 && $endDate == 0)
        {
            $startDate = date('Y-m-01'); // First day of the month
            $endDate = date('Y-m-t'); // Last day of the month.
        }
        else if ($startDate != 0 && $endDate == 0)
        {
            $startDate = date('Y-m-d', strtotime($startDate));
            $endDate = date('Y-m-t', strtotime($startDate));
        }
        else if ($startDate == 0 && $endDate != 0)
        {
            $endDate = date('Y-m-d', strtotime($endDate));
            $startDate = date('Y-m-01', strtotime($endDate));
        }
        else
        {
            $startDate = date('Y-m-d', strtotime($startDate));
            $endDate = date('Y-m-d', strtotime($endDate));
        }
                    
            $sql = "SELECT e.company_employ_id,e.Primary_Lead_ID,`first_name`,`last_name`,e.Email,`DateOfBirth`,`DateOfJoin`,`Gender`,
                `Mobile`, q.`Name` AS qualification, d.Name AS Designation,l.Name AS location, d.grades as grade, v.`Name` AS vertical, s.ShiftCode AS shift  
                FROM `employ` e 
                LEFT JOIN `location` l ON l.`LocationID` = e.LocationID
                LEFT JOIN `designation` d ON d.`designation_id` = e.designation_id
                LEFT JOIN `qualifications` q ON q.QualificationID = e.QualificationID
                LEFT JOIN `employprocess` ep ON ep.`employee_id` = e.`employee_id`
                LEFT JOIN `process` p ON p.`process_id` = ep.`process_id`
                LEFT JOIN  `verticals` v ON v.`VerticalID` = p.`VerticalID`
                LEFT JOIN `shift` s ON s.`ShiftID` = ep.`ShiftID`
                WHERE `DateOfJoin` BETWEEN '$startDate' AND '$endDate'";
        
        $query = $this->db->query($sql);
        $totCount 	= $query->num_rows();
        
        $queryResults = $this->_addSerialNos($query->result_array(),$options['offset']);
        
        $results = array_merge(array('totalCount' => $totCount),array('rows' => $queryResults));
        
        return $results;
    }


    //add serail no to result set
    private function _addSerialNos($data,$start)
    {
        $l = ($start==0)?1:$start+1;
        $tempArray = $data;
        $i=1;
        foreach($data as $k=>$v)
        {
            $j=$i-1;
            $tempArray[$j]['SlNo']=$l;
            $i++;
            $l++;
        }
        return $tempArray;
    }

        public function getEmployeesNames()
    {
        $sql = "SELECT employees.`employee_id`, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS FullName  from employees
                JOIN designation ON employees.designation_id = designation.designation_id
                WHERE employees.relieve_flag=0 AND designation.grades > 1 ORDER BY first_name";    	
        $query = $this->db->query($sql);    	
        return $query->result_array();
    }

    public function getPegProcess()
    {
        $sql = "SELECT `process_id`,`Name` FROM PROCESS WHERE`VerticalID` = 3";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getEmployByprocess_id()
    {
        $process_id = $this->input->get('process_id') ? $this->input->get('process_id') : 0;
        
        $sql = "SELECT e.`employee_id`, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `Name` FROM `employprocess` ep LEFT JOIN `employ` e ON e.`employee_id` = ep.`employee_id`  WHERE ep.`process_id` = $process_id";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getProcessLatestJoinees()
    {
        $process_id = $this->input->get('process_id') ? $this->input->get('process_id') : 0;
            
        $sql = "SELECT e.`employee_id`, CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `Name` FROM `employprocess` ep 
                LEFT JOIN `employ` e ON e.`employee_id` = ep.`employee_id`  WHERE ep.`process_id` = $process_id 
                AND e.`DateOfJoin` ORDER BY `DateOfJoin` DESC LIMIT 0,10";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public Function getEmployeeProcessList($employee_id){

            $sql = "SELECT employees.employee_id,GROUP_CONCAT(process.process_id) AS process_ids,GROUP_CONCAT(process.Name) AS ProcessList from employees 
                JOIN `employprocess` ON `employees`.`employee_id` = `employprocess`.`employee_id` JOIN `process` ON `process`.`process_id` = `employprocess`.process_id 
                WHERE `employees`.`relieve_flag` =0 AND `employees`.`employee_id` = '$employee_id' GROUP BY `employees`.`employee_id`"; 
        /*  $sql = "SELECT employees.employee_id,GROUP_CONCAT(process.process_id) AS process_ids,GROUP_CONCAT(process.Name) AS ProcessList,GROUP_CONCAT(DISTINCT client.ClientID) AS ClientIDs from employees
                JOIN `employprocess` ON `employees`.`employee_id` = `employprocess`.`employee_id` JOIN `process` ON `process`.`process_id` = `employprocess`.process_id
                JOIN  `client` ON `process`.`ClientID` = client.ClientID 
                WHERE `employees`.`relieve_flag` =0 AND `employees`.`employee_id` = '$employee_id' GROUP BY `employees`.`employee_id`";  */
        $result_select   = $this->db->query($sql);
        if(isset($result_select) && count($result_select->result_array())!=0) {
            return $result_select->result_array();
        }else
            return "";
        
        
        
    }

    function getAllReporteesData()
    {   	
        $sql = "SELECT e.employee_id AS EmployID, e.assigned_vertical AS AssignedVertical,e.company_employ_id AS CompanyEmployID, CONCAT(IFNULL(e.first_name, ''),' ',IFNULL(e.last_name, '')) AS `Name`, e.first_name AS FirstName, e.last_name AS LastName, e.email AS Email, d.name as designation, v.name AS vertical, d.designation_id AS DesignationID, d.grades as Grade 
        FROM employees e 
        JOIN designation d on d.designation_id = e.designation_id 
        JOIN `verticals` v ON v.`vertical_id` = e.`assigned_vertical` 
        WHERE e.status!=6";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) 
        {
            $data = $query->result_array();
            $finalData = array();
            foreach ($data as $key=>$value)
            {
                $finalData[$key]['CompanyEmployID'] = $value['CompanyEmployID'];
                $finalData[$key]['Name'] = $value['Name'];
                $finalData[$key]['FirstName'] = $value['FirstName'];
                $finalData[$key]['LastName'] = $value['LastName'];
                $finalData[$key]['DesignationName'] = $value['designation'];
                $finalData[$key]['VertName'] = $value['vertical'];
                $finalData[$key]['Grade'] = $value['Grade'];
                $finalData[$key]['Email'] = $value['Email'];
                $finalData[$key]['VerticalID'] = $value['AssignedVertical'];
                $finalData[$key]['Reportees'] = $this->getReporteesByID($value['EmployID']);    			
            }

            return $finalData;
        }
        return '';
    }

    function getReporteesByID($employID)
    {
        $sql = "SELECT e.company_employ_id AS CompanyEmployID, CONCAT(IFNULL(e.first_name, ''),' ',IFNULL(e.last_name, '')) AS `Name`, e.email AS Email, d.name AS DesignationName, d.designation_id AS DesignationID, d.grades as Grade, v.name AS vertical 
        FROM employees e 
        JOIN designation d on d.designation_id = e.designation_id 
        JOIN `verticals` v ON v.`vertical_id` = e.`assigned_vertical` 
        WHERE e.status!=6 AND primary_lead_id ='" . $employID . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return '';  	
    }



    /* get All employees data for Asset management system */
    function getAllEmployeesData()
    {
        $sql = "SELECT DISTINCT e.company_employ_id AS 'EmpID', e.first_name AS firstname, e.last_name AS LastName,e.email, rd.name AS 'Designation', GROUP_CONCAT(DISTINCT v.name ORDER BY v.name) AS 'Vertical', '' AS pnames,
                mgr.company_employ_id AS 'SupervisorEmpID',CONCAT(mgr.first_name,' ',IFNULL(mgr.last_name,'')) AS 'ReportingLead',shift_code AS Shift
                FROM 
                employees e
                LEFT JOIN employee_vertical ON e.employee_id = employee_vertical.employee_id 
                LEFT JOIN employees mgr ON e.primary_lead_id = mgr.employee_id 
                LEFT JOIN designation rd ON rd.designation_id = e.designation_id
                LEFT JOIN designation rm ON rm.designation_id = mgr.designation_id
                LEFT JOIN verticals v ON v.vertical_id = employee_vertical.vertical_id
                LEFT JOIN shift s ON s.shift_id = e.shift_id
                WHERE e.status != 6
                --  v.verticalid  = 3
                GROUP BY e.employee_id ORDER BY e.employee_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

}
?>