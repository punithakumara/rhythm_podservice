<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Type_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
	}

	private function roleTypesQuery($forTotal=false)
	{
		$filter_params = $this->input->get();

		$this->db->select('RT.role_type_id,project_type,role_type,role_description');
		$this->db->from('role_types as RT');
		$this->db->join('wor_project_role_mapping as WPRM', 'RT.role_type_id = WPRM.role_type_id', 'LEFT');
		$this->db->join('project_types as PT', 'PT.project_type_id = WPRM.project_type_id', 'LEFT');

		$where = '';		
		if(isset($filter_params['filter']))
		{
			$search_params = json_decode($filter_params['filter'],true);
			$property = explode(',',$search_params[0]['property']);
			if(isset($search_params[0]['value']))
			{
				$search_char = $search_params[0]['value'];
				if($search_char!="")
				{
					foreach ($property as $key => $value) {
						$where .= "(".$value." LIKE '%".$search_char."%') OR ";
					}
					$where = substr($where, 0, -3);
				}
			}
		}

		if($where!='')
		{
			$this->db->where($where);
		}

		if($forTotal)
		{
			$query = $this->db->get();
			return $query;
		}

		if(isset($filter_params['sort']))
		{
			$sort_params = json_decode($filter_params['sort'],true);
			$property = $sort_params[0]['property'];
			$direction = $sort_params[0]['direction'];

			$this->db->order_by($property, $direction); 
		}

		$offset = $filter_params['start'];
		$limit = $filter_params['limit'];

		$this->db->limit($limit, $offset);

		$query = $this->db->get();
		return $query;
	}

	public function get_roleTypes()
	{
		$query = $this->roleTypesQuery(true);
		$result['rows'] = array();
		$result['totalCount'] = $query->num_rows();

		$query = $query = $this->roleTypesQuery();
		if($query->num_rows()>0)
		{
			$result['rows'] = $query->result_array();
		}
		return $result;	
	}

}