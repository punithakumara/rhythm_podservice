<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wor_Type_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
	}


	private function worTypesQuery($forTotal=false)
	{
		$filter_params = $this->input->get();

		$this->db->select('WT.wor_type_id,wor_type,wor_description, COUNT(DISTINCT WPRM.project_type_id) AS total_projects');
		$this->db->from('wor_types as WT');
		$this->db->join('wor_project_role_mapping as WPRM', 'WT.wor_type_id = WPRM.wor_type_id', 'LEFT');
		$this->db->where('WT.status', '1');

		$where = '1=1';
		if(isset($filter_params['filter']))
		{
			$search_params = json_decode($filter_params['filter'],true);
			$property = explode(',',$search_params[0]['property']);
			$search_char = $search_params[0]['value'];
			if($search_char!="")
			{
				$where .= " AND ";
				foreach ($property as $key => $value) 
				{
					$where .= "(".$value." LIKE '%".$search_char."%') OR ";
				}
				$where = substr($where, 0, -3);
			}
		}

		$this->db->where($where);		
		
		if($this->input->get('wor_id') && $this->input->get('wor_id')!='')
		{
			$wor_id = $this->input->get('wor_id');
			$wor_type_sql = "SELECT wor_type_id FROM wor WHERE wor_id='$wor_id'";
			$wor_type_query = $this->db->query($wor_type_sql);
			$wor_type_result = $wor_type_query->result_array();
			$wor_type_id = $wor_type_result[0]['wor_type_id'];

			if($wor_type_id!='')
				$this->db->where('WT.wor_type_id', $wor_type_id);
			
		}

		$this->db->group_by('WT.wor_type_id'); 

		if($forTotal)
		{
			$query = $this->db->get();
			return $query;
		}

		if(isset($filter_params['sort']))
		{
			$sort_params = json_decode($filter_params['sort'],true);
			$property = $sort_params[0]['property'];
			$direction = $sort_params[0]['direction'];

			$this->db->order_by($property, $direction); 
		}

		$offset = $filter_params['start'];
		$limit = $filter_params['limit'];

		$this->db->limit($limit, $offset);

		$query = $this->db->get();
		return $query;

		// echo $this->db->last_query();exit;
	}
	
	//codeigniter query
	public function get_worType_bckup()
	{
		$filter_params = $this->input->get();

		if($this->input->get('query'))
		{
			$filterName = $this->input->get('filterName');
			$like_param = $this->input->get('query');

			$this->db->select('wor_type_id,wor_type');
			$this->db->from('wor_types');
			$this->db->like($filterName, $like_param);
			$query1 = $this->db->get();
			$result1['rows'] = array();
			$result1['totalCount'] = $query1->num_rows();
			if($query1->num_rows()>0)
			{
				$result1['rows'] = $query1->result_array();
			}

			return $result1;
		}

		$query = $this->worTypesQuery(true);
		$result['rows'] = array();
		$result['totalCount'] = $query->num_rows();	

		$query = $this->worTypesQuery();
		if($query->num_rows()>0)
		{
			$result['rows'] = $query->result_array();
		}

		// debug($result);exit;
		
		return $result;	
	}

	public function get_worTypes()
	{
		$filter_params = $this->input->get();

		if($this->input->get('query'))
		{
			$filterName = $this->input->get('filterName');
			$like_param = $this->input->get('query');
			$sql1 = "SELECT wor_type_id,wor_type FROM wor_types WHERE 1=1";
			$sql1 .= " AND ".$filterName." LIKE '%".$like_param."%'";
			$query1 = $this->db->query($sql1);
			$result1['rows'] = array();
			$result1['totalCount'] = $query1->num_rows();
			if($query1->num_rows()>0)
			{
				$result1['rows'] = $query1->result_array();
			}

			return $result1;
		}

		$offset = $filter_params['start'];
		$limit = $filter_params['limit'];
		
		$sql = "SELECT WT.wor_type_id,wor_type,wor_description, COUNT(DISTINCT WPRM.project_type_id) AS total_projects
		FROM wor_types WT
		LEFT JOIN wor_project_role_mapping WPRM ON WT.wor_type_id = WPRM.wor_type_id
		WHERE 1=1";

		if(isset($filter_params['filter']))
		{
			$search_params = json_decode($filter_params['filter'],true);
			$property = explode(',',$search_params[0]['property']);
			$search_char = $search_params[0]['value'];
			if($search_char!="")
			{
				$sql .= " AND ";
				foreach ($property as $key => $value) 
				{
					$sql .= "(".$value." LIKE '%".$search_char."%') OR ";
				}
				$sql = substr($sql, 0, -3);
			}
		}

		$sql .=	" AND WT.status = 1";
		
		if($this->input->get('wor_id') && $this->input->get('wor_id')!='')
		{
			$wor_id = $this->input->get('wor_id');
			$wor_type_sql = "SELECT wor_type_id FROM wor WHERE wor_id='$wor_id'";
			$wor_type_query = $this->db->query($wor_type_sql);
			$wor_type_result = $wor_type_query->result_array();
			$wor_type_id = $wor_type_result[0]['wor_type_id'];

			if($wor_type_id!='')
				$sql .= " AND WT.wor_type_id='$wor_type_id'";
			
		}

		$sql .=	" GROUP BY WT.wor_type_id";

		$query = $this->db->query($sql);
		$result['rows'] = array();
		$result['totalCount'] = $query->num_rows();

		if(isset($filter_params['sort']))
		{
			$sort_params = json_decode($filter_params['sort'],true);
			$property = $sort_params[0]['property'];
			$direction = $sort_params[0]['direction'];

			$sql .= " ORDER BY $property $direction";
		}

		$sql .= " LIMIT $offset,$limit";

		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$result['rows'] = $query->result_array();
		}
		
		return $result;	
	}

	public function getClientWor($client_id)
	{
		$this->db->select('GROUP_CONCAT(DISTINCT wor_type_id) as wor_type_id');
		$this->db->from('wor');
		$this->db->where('client',$client_id);

		if($this->input->get('wor_id') && $this->input->get('wor_id')!='')
		{
			$wor_id = $this->input->get('wor_id');
			$this->db->where('wor_id',$wor_id);
		}
		$query = $this->db->get();
		$wor_type_result = $query->result_array();	
		$wor_type_id = $wor_type_result[0]['wor_type_id'];

		$this->db->select('wor_type_id,wor_type');
		$this->db->from('wor_types');
		$this->db->where_in('wor_type_id', array($wor_type_id));
		$query = $this->db->get();
		$result = $query->result_array();
		$data['rows'] = $result;
		return $data;
	}
}