<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_Model extends INET_Model
{
	public $empData;
	
	function __construct() 
	{
		parent::__construct();
		$this->config->load('admin_config');
		$this->empData = $this->input->cookie();
	}

	function build_query()
	{
		$filter = json_decode($this->input->get('filter'),true);
		$options['filter'] = $filter;
		$documentName = $this->input->get('Access') ? $this->input->get('Access') : '';
		
		if($documentName == 'All')
			$documentName ='("All","All managers")';
		else
			$documentName ='("'.$documentName.'")';
		
		$pro_ver = '';
		if($documentName == '("Service type Access")' && $this->empData['Grade']<5)
		{
			$pro_ver = " AND document.podservice_id IN(".$this->empData['service_id'].")";
		}
		
		// to here
		$sql = "SELECT document.DocumentID,document.Name,document.UploadName,tbl_rhythm_services.name as ServiceName, DATE_FORMAT(document.DocumentDate,'%d-%m-%Y') AS DocumentDate,document.EmployID,document.Access,document.podservice_id,CONCAT(IFNULL(employees.first_name, ''), ' ', IFNULL(employees.last_name, '')) AS FirstName,tbl_rhythm_services.folder AS Folder FROM document JOIN employees ON employees.employee_id = document.EmployID LEFT JOIN tbl_rhythm_services ON tbl_rhythm_services.service_id = document.podservice_id WHERE Access IN ".$documentName."".$pro_ver."";
		
		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if($filterField != '') {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') {					
				$sql .= " AND (".$filterWhere.")";					
			} 
		}
		
		return $sql;
	}

	function view() 
	{
		$options = array();
		$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
		$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
		
		$sort = json_decode($this->input->get('sort'),true);
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$sql = $this->build_query();
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$sql = $this->build_query();
		if(isset($options['sortBy'])) 
		{
			$sortBy=$options['sortBy'];
			if($options['sortBy']=="DocumentDate")
			{
				$sortBy = "document.DocumentDate";
			}
			$sql .= " ORDER BY ".$sortBy." ".$options['sortDirection'];
		}
		
		if(isset($options['limit']) && isset($options['offset'])) {
			$sql .= " LIMIT ".$options['offset'].", ".$options['limit'];
		}
		else if(isset($options['limit'])) {
			$sql .= " LIMIT ".$options['limit'];
		}
		
		$query = $this->db->query($sql);
		$data = $query->result_array();	
		foreach ($data as $key => $value) {
			$data[$key]['DocumentDate']	= $this->commonDateSetting($value['DocumentDate']);
		}
		
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));	
		
		return $results;
	}

	function addDocs( $data = '' ) 
	{
		// Load the configuration
		$this->ci->load->config('documents-config');

		$flag = 0;
		if(isset($_FILES['NewDocument']['name']))
		{
			$verticalid = "";
			if(isset($data['Access']))
			{
				$accessLevel = $data['Access'];
				if(isset($data['VerticalID']))
					$verticalid = $data['VerticalID'];
			}
			
			$exists = $this->checkForOverwrite($_FILES['NewDocument']['name'],$verticalid,$accessLevel);
			
			if($exists == 0){
				$flag = 1;
			}
			else
				$flag = 0;
			
		}
		if($flag == 0 || $flag == 1)
		{
			if(is_dir($this->ci->config->item('company_documents_path') == false)) {
				$newcompDir = mkdir($this->ci->config->item('company_documents_path'), 0777, true);
			}
			
			if(is_dir($this->ci->config->item('vertical_documents_path') == false)) {
				$newvertDir = mkdir($this->ci->config->item('vertical_documents_path'), 0777, true);
			}
			
			// upload file to a directry			
			$file='';
			$verticalID = null; 
			$ClientID = null; 
			
			if(isset($data['VerticalID']))	
				$verticalID = isset($data['VerticalID']) ? $data['VerticalID'] : null;

			$access = isset($data['Access']) ? $data['Access'] : '';
			
			$target_file ='';
			
			if(isset($_FILES['NewDocument']))
			{	
				$file = $_FILES['NewDocument']['name'];			
				
				if($access == "All" || $access == "All managers"){
					$target_file = $this->comDocCreate(basename($_FILES['NewDocument']['name']));			
				}else{
					$target_file = $this->verDocCreate($verticalID,basename($_FILES['NewDocument']['name']));
				}
				move_uploaded_file($_FILES['NewDocument']['tmp_name'], $target_file);						
			//saving in to DB
			}
			
			$id = isset($data['DocumentID']) ? $data['DocumentID'] : '';
			$editRawClient	= isset($data['docEditClient'])?$data['docEditClient']:"";
			$editRawVertical = isset($data['docEditVert'])?$data['docEditVert']:"";
			$editModifiedClient	= isset($data['docModifiedClient'])?$data['docModifiedClient']:"";
			$editModifiedVertical = isset($data['docModifiedVert'])?$data['docModifiedVert']:"";
			
			$rawAccess	= isset($data['rawAccess'])?$data['rawAccess']:"";
			$modifiedAccess = isset($data['modifiedAccess'])?$data['modifiedAccess']:"";
			
			$allMng = array("All managers", "All");
			if($rawAccess !="" && $modifiedAccess !="" && ($rawAccess != $modifiedAccess) && !(in_array($rawAccess,$allMng) && in_array($modifiedAccess,$allMng)))
			{
				
				switch (true) {
					case ($rawAccess =="All managers" || $rawAccess =="All") :
					
					$to_copy = $this->comDocCreate($data['PreviousUploadName']);
					
					if($modifiedAccess == "Vertical Specific"){
						$to_move = $this->verDocCreate($editModifiedVertical,$data['PreviousUploadName']);
					}
					break;
					case ($rawAccess =="Vertical Specific") :
					
					$to_copy = $this->verDocCreate($editRawVertical,$data['PreviousUploadName']);
					
					if($modifiedAccess == "All managers" || $modifiedAccess =="All" ){
						$to_move = $this->comDocCreate($data['PreviousUploadName']);
					}
					break;
					
				}
				
				rename($to_copy,$to_move);
				
			}
			
			$DocDataset = array(
				'UploadName'=>$file,
				'EmployID'=>$this->empData['employee_id'],
				'DocumentDate'=>date('Y-m-d h:i:s'),
				'podservice_id'=>$verticalID,
				'Access'=>$data['Access']
			);


			$Docediaset = array(
				'EmployID'=>$this->empData['employee_id'],
				'DocumentDate'=>date('Y-m-d h:i:s'),
				'podservice_id'=>$verticalID,
				'Access'=>$data['Access']
			);	
			
			if (!empty($id))
			{
				$where['DocumentID'] = $id;
				
				$retVal = $this->update($Docediaset, $where, 'document');
			}
			else
			{	
				$retVal = $this->insert($DocDataset, 'document');					
			}			
			return $retVal;	
		}
	}


	function getFolderName($vertId="")
	{
		$sql = "SELECT folder as Folder FROM verticals WHERE vertical_id =".$vertId;
		$query = $this->db->query($sql);
		
		$data = $query->result_array();	
		
		return $data[0]['Folder'];
	}
	

	function verDocCreate($verticalID,$uploadFile)
	{
		$getFolderName = $this->getFolderName($verticalID);

		if($getFolderName ==""){
			$getFolderName ="temp";
		}
		
		if(is_dir($this->ci->config->item('vertical_documents_path').$getFolderName) == false) {
			$newvertspDir = mkdir($this->ci->config->item('vertical_documents_path').$getFolderName, 0777, true);
		}
		
		$to_send = $this->ci->config->item('vertical_documents_path').$getFolderName."/".$uploadFile;
		
		return $to_send;
		
	}

	
	function comDocCreate($uploadFile){
		
		$to_send = $this->ci->config->item('company_documents_path').$uploadFile;
		
		return $to_send;
		
	}

	public function deleDocs($data)
	{
		$options = array();
		$options['DocumentID'] 	= $data['DocumentID'];
		if($data['doctype'] == "All" || $data['doctype'] == "All managers")
		{
			$retVal = $this->delete($options, 'document');
			$to_remove = $this->ci->config->item('company_documents_path').$data['uploadName'];
			if (is_file($to_remove))
			{
				unlink($to_remove);
			}
		}
		if($data['doctype'] == "Vertical Specific")
		{
			$retVal = $this->delete($options, 'document');
			$to_remove = $this->ci->config->item('vertical_documents_path').$data['getFolder']."/".$data['uploadName'];
			if (is_file($to_remove))
			{
				unlink($to_remove);
				
				if  (count(scandir(dirname ( $to_remove ))) == 2) {
					rmdir(dirname ( $to_remove ))  ;
				}
			}
		}
		return $retVal;
	}
	
	function checkForOverwrite($uploadname,$vid,$access)
	{
		$retVal = 0;
		$getFolder = "";
		$sql = " SELECT DocumentID,UploadName as uploadName,Access as doctype,podservice_id from document 
		WHERE UploadName = '".$uploadname."'";
		
		if($vid != "")
		{
			$sql.=" AND VerticalID = ".$vid;
			$getFolderName = $this->getFolderName($vid);	
			if($getFolderName ==""){
				$getFolder ="temp";
			}
			else
			{
				$getFolder = $getFolderName;
			}
		}	
		if($access != "")
			$sql.=" AND Access = '".$access."'";
		
		$query = $this->db->query($sql);			
		$data = $query->result_array();
		if(count($data)>=1){
			if($data[0]['DocumentID'] != "")
			{
				$delData = array(
					'DocumentID' => $data[0]['DocumentID'],
					'uploadName' => $data[0]['uploadName'],
					'podservice_id' => $data[0]['podservice_id'],
					'doctype' => $data[0]['doctype'],
					'getFolder'=>$getFolder
				);
				$retVal = $this->deleDocs($delData);
			}
		}	
		
		return $retVal;
	}
}
?>