<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monthly_Billabliity_Model extends INET_Model
{
     function __construct()
     {
          parent::__construct();

     }

     function build_query()
     {
          $mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');

          $empData    = $this->input->cookie();

          $time=strtotime($mbrmonth);
          $month=date("m",$time);
          $year=date("Y",$time);

          $sessionEmploy     = $this->input->cookie();
          $loggedInId        = $sessionEmploy['employee_id'];

          $filter            = json_decode($this->input->get('filter'), true);
          $options['filter'] = $filter;
          $sql               = "";
          $filtered_ids = $this->client_max_id($year,$month);

          $sql = "SELECT client_name,billability_report.fk_client_id as client_id,billability_report.fk_roles_id as responsibility_id,role_types.role_type AS responsibilities,billability_report.*, billability_report.fte_total_buffer,billability_report.fte_total_hc as responsibility_hc, case when (fte_ist+fte_emea) < 0 then 0 else (fte_ist+fte_emea) end as india		
          ,case when (fte_pst+fte_cst+fte_est) < 0 then 0 else (fte_pst+fte_cst+fte_est) end as usa  FROM fte_billability_report as billability_report LEFT JOIN client ON client.client_id = billability_report.fk_client_id LEFT JOIN role_types ON role_types.role_type_id = billability_report.fk_roles_id WHERE id IN ($filtered_ids) ";

          if($empData['Grade']>= 3 && $empData['Grade'] <=4 && $empData['employee_id'] !=2654 && $empData['employee_id'] !=2269 )
          {
               $sql .= " And billability_report.fk_client_id IN (".$empData['ClientId'].")";
          }

          $filterWhere = '';
          if (isset($options['filter']) && $options['filter'] != '') {
               foreach ($options['filter'] as $filterArray) {
                    $filterFieldExp = explode(',', $filterArray['property']);
                    foreach ($filterFieldExp as $filterField) {
                         if ($filterField != '') {
                              $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                              $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                         }
                    }
               }

               if ($filterWhere != '') 
               {
                    $sql .= " AND (" . $filterWhere . ")";
               }
          }
          $sql .= " ORDER BY client_name, role_types.role_type ASC";

          return $sql;
     }


     function client_max_id($year,$month)
     {
          $sql = "SELECT MAX(id) FROM fte_billability_report WHERE YEAR(date) =  '$year' AND MONTH(date) = '$month'  GROUP BY fk_wor_id,fk_client_id,fk_roles_id"; 	

          $query    = $this->db->query($sql);
          $result = $query->result_array();
          if (empty($result)) {
               return 'null';
          }
          return implode(', ', array_column($result, 'MAX(id)'));
     }

     function build_report_query()
     {
          $mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');
          $empData    = $this->input->cookie();

          $time=strtotime($mbrmonth);
          $month=date("m",$time);
          $year=date("Y",$time);

          $sessionEmploy     = $this->input->cookie();
          $loggedInId        = $sessionEmploy['employee_id'];

          $filter            = json_decode($this->input->get('filter'), true);
          $options['filter'] = $filter;
          $sql               = "";
          $filtered_ids = $this->client_max_id($year,$month);

          $sql = "SELECT client_name,billability_report.fk_client_id as client_id, billability_report.fk_roles_id as responsibility_id, role_types.role_type AS responsibilities,fte_total_hc AS Approved_HC,fte_total_buffer AS Approved_Buffer,fte_lemea,fte_apac,case when (fte_ist+fte_emea) < 0 then 0 else (fte_ist+fte_emea) end as india,case when (fte_pst+fte_cst+fte_est) < 0 then 0 else (fte_pst+fte_cst+fte_est) end as usa, 0 as hourly_min, 0 as hourly_max, 0 as volume_min, 0 as volume_max,0 as fte_id 
          FROM fte_billability_report as billability_report 
          LEFT JOIN client ON client.client_id = billability_report.fk_client_id 
          LEFT JOIN role_types ON role_types.role_type_id = billability_report.fk_roles_id 
          WHERE id IN ($filtered_ids) ";

          if($empData['Grade']>= 3 && $empData['Grade'] <=4 && $empData['employee_id'] !=2654 && $empData['employee_id'] !=2269 ){
               $sql .= " And billability_report.fk_client_id IN (".$empData['ClientId'].")";

          }

          $filterWhere = '';
          if (isset($options['filter']) && $options['filter'] != '') {
               foreach ($options['filter'] as $filterArray) {
                    $filterFieldExp = explode(',', $filterArray['property']);
                    foreach ($filterFieldExp as $filterField) {
                         if ($filterField != '') {
                              $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                              $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                         }
                    }
               }

               if ($filterWhere != '') {
                    $sql .= " AND (" . $filterWhere . ")";

               }
          }
          $sql .= " ORDER BY client_name, role_types.role_type ASC";

          return $sql;

     }


     function view()
     {

          $options = array();
//$clientParam = ($this->input->get('ClientID')) ? $this->input->get('ClientID') : "";
          $reportParam = ($this->input->get('Report')) ? $this->input->get('Report') : "";
          $mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');

          $time=strtotime($mbrmonth);
          $month=date("m",$time);
          $year=date("Y",$time);

          if ($this->input->get('hasNoLimit') != '1') {
               $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
               $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
          }

          $sort = json_decode($this->input->get('sort'), true);

          $options['sortBy']        = $sort[0]['property'];
          $options['sortDirection'] = $sort[0]['direction'];

          if ($reportParam != "") { 
               $sql      = $this->build_report_query();

               $query    = $this->db->query($sql);
               $totCount = $query->num_rows();
               $result = $query->result_array();

               foreach($result as $key=>$value)
               {
                    if($value['hourly_min'] == 0 && $value['hourly_max'] == 0  && $value['volume_max'] == 0 && $value['volume_min'] == 0 ){
                         unset($result[$key]);
                    }
               } 
               $result = array_merge($result); 

               foreach($result as $key=>$value)
               {
                    $result[$key]['actual_volumes'] = $this->GetCreatives($value['client_id'] , $value['responsibility_id'],$month,$year);
                    $result[$key]['Billable_hrs'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"approved_hrs","export");
                    $result[$key]['approved_Weekend_hrs'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"approved_Weekend_hrs","export");
                    $result[$key]['approved_holiday_coverage'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"approved_holiday_coverage","export");

                    unset($result[$key]['Approved_HC'] );
                    unset($result[$key]['Approved_Buffer'] );
                    unset($result[$key]['fte_lemea'] );
                    unset($result[$key]['fte_apac'] );
                    unset($result[$key]['india'] );
                    unset($result[$key]['usa'] );
                    unset($result[$key]['fte_id'] );
                    unset($result[$key]['client_id'] );
                    unset($result[$key]['responsibility_id'] );
               }

               $columns = array();

               foreach($result as $key=>$value)
               {
                    $columns = array_change_key_case($result[0],CASE_UPPER);
                    $columns = array(str_replace('_',' ',array_keys($columns)));

               }

               $results = array(array_merge($columns, $result));

               return $results;


          }else{
               $sql      = $this->build_query();

               $query    = $this->db->query($sql);
               $data = $query->result_array();

               foreach($data as $key=>$value)
               {
                    if($value['hourly_min'] == 0 && $value['hourly_max'] == 0 && $value['hourly_actual'] == 0 && $value['volume_max'] == 0 && $value['volume_min'] == 0 && $value['volume_actual'] == 0 ){
                         unset($data[$key]);
                    }
               } 

               foreach($data as $key=>$value)
               {
                    $data[$key]['hourly_actual'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"hourly_actual","list");
                    $data[$key]['approved_hrs'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"approved_hrs","list");
                    $data[$key]['approved_Weekend_hrs'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"approved_Weekend_hrs","list");
                    $data[$key]['approved_holiday_coverage'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"approved_holiday_coverage","list");
                    $data[$key]['non_approved_hrs'] = $this->GetActualHours($value['client_id'] , $value['responsibility_id'],$month,$year,"non_approved_hrs","list");
                    $data[$key]['total_creatives'] = $this->GetCreatives($value['client_id'] , $value['responsibility_id'],$month,$year);

               }

               $data = array_merge($data); 
               $totCount = count($data);
               $results = array_merge(array(
                    'totalCount' => $totCount
               ), array(
                    'rows' => $data
               ));

               return $results;

          }
     } 

     /*Fte Model View*/
     function Fteview()
     {
          // debug($_GET);exit;
          $options     = array();

          $reportParam = ($this->input->get('Report')) ? $this->input->get('Report') : "";
          $mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');

          $time=strtotime($mbrmonth);
          $month=date("m",$time);
          $year=date("Y",$time);

          if ($this->input->get('hasNoLimit') != '1') {
               $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
               $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
          }

          $sort = json_decode($this->input->get('sort'), true);

          $options['sortBy']        = $sort[0]['property'];
          $options['sortDirection'] = $sort[0]['direction'];
          $sql      = $this->build_query();
          //echo $sql;exit;
          $query    = $this->db->query($sql);

          $data = $query->result_array();

          foreach($data as $key=>$value)
          {
               $fte_query = $this->db->query("Select fte_id from fte_model where fk_wor_id=".$value['fk_wor_id']);
               $fte_ids = $fte_query->result_array();
               $fte_id = "";
               foreach ($fte_ids as $key1 => $value1) {
                    $fte_id.=$value1['fte_id'].",";
               }

               $data[$key]['fte_id'] = rtrim($fte_id,',');
               // if($value['fte_est'] == 0  && $value['fte_emea'] == 0  && $value['fte_ist'] == 0 && $value['fte_lemea'] == 0 &&  $value['fte_cst'] == 0 &&  $value['fte_pst'] == 0 &&  $value['fte_apac'] == 0 && $value['responsibility_hc'] == 0 ){
               //      unset($data[$key]);
               // }
          } 
          $data = array_merge($data); 
               // debug($data);exit;
          foreach($data as $key=>$value)
          {
               $data[$key]['calculated_fte'] = $this->GetFullMonthFte($value['client_id'] ,$value['fk_wor_id'],$value['fk_type_id'],$value['fk_project_id'], $value['responsibility_id'],$month,$year,$value['responsibility_hc'],$time,$value['fte_id'] );
               $approved_buffer = $this->GetApprovedBuffer($value['fk_wor_id'],$value['fk_project_id'],$value['responsibility_id'],$month,$year);
               $data[$key]['fte_total_buffer'] = $approved_buffer;
          }

          //var_dump($data);exit;

          foreach($data as $k => $v)
          {
               foreach($v as $k1 => $v1)
               {
                    if($k1 == "calculated_fte"){
                         foreach($v1 as $k12 => $v12)
                         {
                              $data[$k][$k12] = $v12;
                         }
                    }
               }	
               $wor_query = $this->db->query("Select wor_name from wor where wor_id =".$data[$k]['fk_wor_id']);
               $data[$k]['wor_name'] = $wor_query->row()->wor_name;
               $type_query = $this->db->query("Select wor_type from wor_types where wor_type_id =".$data[$k]['fk_type_id']);
               $data[$k]['wor_type'] = $type_query->row()->wor_type;
               $project_query = $this->db->query("Select project_type from project_types where project_type_id =".$data[$k]['fk_project_id']);
               $data[$k]['project_type'] = $project_query->row()->project_type;
               $role_type_query = $this->db->query("Select role_type from role_types where role_type_id =".$data[$k]['fk_roles_id']);
               $data[$k]['role_type'] = $role_type_query->row()->role_type;
          }

          $fteData = array();
          $loop = 0;
          foreach ($data as $key => $value) 
          {
               if($value['fte_total_hc'] != 0 || $value['fte_total_buffer'] != 0  || $value['fte_apac'] != 0  || 
               $value['fte_lemea'] != 0  || $value['india'] != 0  || $value['usa'] != 0  || 
               $value['calculated_fte']['prorated_fte'] != 0 )
               {
                    $fteData[$loop]['client_name']         = $value['client_name'];
                    $fteData[$loop]['wor_name']            = $value['wor_name'];
                    $fteData[$loop]['wor_type']            = $value['wor_type'];
                    $fteData[$loop]['project_type']        = $value['project_type'];
                    $fteData[$loop]['role_type']           = $value['role_type'];
                    $fteData[$loop]['fte_total_hc']        = $value['fte_total_hc'];
                    $fteData[$loop]['fte_total_buffer']    = $value['fte_total_buffer'];
                    $fteData[$loop]['fte_apac']            = $value['fte_apac'];
                    $fteData[$loop]['fte_lemea']           = $value['fte_lemea'];
                    $fteData[$loop]['india']               = $value['india'];
                    $fteData[$loop]['usa']                 = $value['usa'];
                    $fteData[$loop]['full_month_fte']      = $value['calculated_fte']['full_month_fte'];
                    $fteData[$loop]['prorated_fte']        = $value['calculated_fte']['prorated_fte'];
                    $loop++;
               }
          }
          $totCount = count($fteData);
          $results = array_merge(array(
               'totalCount' => $totCount
          ), array(
               'rows' => $fteData
          ));
          
          return $results;


     }

     function GetActualHours($client_id,$resp_id,$month,$year,$column,$type)
     {

          if($column == 'approved_hrs'){
               $sql = "SELECT  SUM( TIME_TO_SEC($column) + TIME_TO_SEC(volume_actual) )  AS results  FROM `billability_report` WHERE YEAR(DATE) = $year AND MONTH(`date`) = $month AND client_id= '$client_id' AND responsibility_id = '$resp_id' ";

          }else{
               $sql = "SELECT  SUM( TIME_TO_SEC($column) )  AS results  FROM `billability_report` WHERE YEAR(DATE) = $year AND MONTH(`date`) = $month AND client_id= '$client_id' AND responsibility_id = '$resp_id' ";
          }

          $query  = $this->db->query($sql);

          $row = $query->row();

          if (isset($row))
          {			
               if($type == "export"){
                    $seconds = $row->results;
                    $hours = floor($seconds / 3600);
                    $mins = floor($seconds / 60 % 60);
                    $secs = floor($seconds % 60);

                    $time = $hours.":". $mins;
                    return $time;
               }else{
                    return $row->results;
               }
          }
          else
          {
               return "0";
          }

     }

     function GetCreatives($client_id,$resp_id,$month,$year)
     {

          $sql = "SELECT SUM(total_creatives_volume + total_creatives_hourly  )   AS results  FROM `billability_report` WHERE YEAR(DATE) = $year AND MONTH(`date`) = $month AND client_id= '$client_id' AND responsibility_id = '$resp_id' ";

          $query  = $this->db->query($sql);

          $row = $query->row();

          if (isset($row))
          {			
               return $row->results;
          }
          else
          {
               return "0";
          }

     }

     function GetFteRemarks($mbrmonth,$resp_id,$fte_ids,$type)
     {
          $time=strtotime($mbrmonth);
          $month=date("m",$time);
          $year=date("Y",$time);

          $date = date( "Y-m-d",$time);

          if($fte_ids != ""){
               $sql = "SELECT DATE_FORMAT(anticipated_date,'%d-%b-%Y') as change_date ,no_of_fte as count,change_type,shift.shift_code As shift  FROM `fte_model`  LEFT JOIN shift ON shift.shift_id = fte_model.shift WHERE `fte_id` IN ($fte_ids) AND responsibilities = '$resp_id'  AND anticipated_date BETWEEN  DATE_FORMAT('$date', '%Y-%m-02')  AND LAST_DAY('$date') ";

               $query = $this->db->query($sql);
               $totCount = $query->num_rows();
               $res = $query->result_array();
               if($type == "export"){
                    $rem = "";
                    foreach($res as $key=>$value)
                    {
                         if($value['change_type'] == 1){$change = "Added";}elseif($value['change_type'] == 2){$change = "Reduction";}
                         $rem .= $value['count']." ". $value['shift'] ." Resources  ".$change." On ". $value['change_date']." , ";

                    }

                    $remarks = substr(trim($rem), 0, -1);
                    return $remarks;

               }

               $results = array_merge(array(
                    'totalCount' => $totCount
               ), array(
                    'rows' => $res
               ));

               return $results;
          }		
     }

     function GetProratedFte($fte_ids,$resp_id,$month,$year,$date)
     {
          if($fte_ids != ""){
               $sql = "SELECT SUM(no_of_fte) AS fte  FROM `fte_model` WHERE `fte_id` IN ($fte_ids) AND responsibilities = '$resp_id' AND anticipated_date BETWEEN  DATE_FORMAT('$date', '%Y-%m-02')  AND LAST_DAY('$date') ";

               $query  = $this->db->query($sql);

               $row = $query->row();
               $ret = array();

               if (isset($row))
               {			
                    return  $row->fte;
               }else {return "0";}		
          }
     }

     function GetFullMonthFte($client_id,$wor_id,$type_id,$project_id,$resp_id,$month,$year,$end_resp_hc,$time,$fte_ids)
     {
          $date = date( "Y-m-d",$time);
          $sql = "SELECT MIN(id) ,fte_total_hc 
          FROM fte_billability_report 
          WHERE date = DATE_FORMAT('$date', '%Y-%m-01') AND fk_client_id = '$client_id' AND fk_wor_id = '$wor_id' AND fk_type_id = '$type_id' 
          AND fk_project_id = '$project_id' AND fk_roles_id = '$resp_id'"; 	

          $query  = $this->db->query($sql);

          $row = $query->row();
          $ret = array();

          if (isset($row))
          {			
               $start_hc = $row->fte_total_hc;

               if($start_hc == ""){
                    $ret['full_month_fte'] = 0;
               }

               elseif($start_hc == $end_resp_hc){ 
                    $ret['full_month_fte'] = $end_resp_hc;

               }
               elseif($start_hc < $end_resp_hc){
                    $ret['full_month_fte'] = $start_hc;

               }
               elseif($start_hc > $end_resp_hc){

                    $ret['full_month_fte'] = $end_resp_hc;
               }

               $prorated_fte = $this->GetProratedFte($fte_ids,$resp_id,$month,$year,$date);
               if($prorated_fte == ''){
                    $ret['prorated_fte'] = 0;
               }else{
                    $ret['prorated_fte'] = $prorated_fte;
               }

               return $ret;

          }

     }

     function GetApprovedBuffer($wor_id,$project_id,$resp_id,$month,$year)
     {
          $sql = "SELECT MAX(fte_total_buffer) AS responsibility_ab  
          FROM fte_billability_report 
          WHERE YEAR(date) = '$year' AND MONTH(date) = '$month' AND fk_wor_id = '$wor_id' AND fk_project_id = '$project_id' AND fk_roles_id = '$resp_id'";
          // exit;
          $query  = $this->db->query($sql);

          $row = $query->row();
          $ret = array();
          
          if (isset($row))
          {			
               return  $row->responsibility_ab;
          }
          else 
          {
               return "0";
          }
     }


     function getHourlyVolumeReport()
     {
          // debug($_GET);exit;
          $filter = json_decode($this->input->get('filter'),true);
          if($this->input->get('mbrmonth'))
          {
               $month = date('m', strtotime($this->input->get('mbrmonth')));
               $year = date('Y', strtotime($this->input->get('mbrmonth')));
          }
          else
          {
               $year = date('Y');
               $month = date('m');
          }

          if ($this->input->get('hasNoLimit') != '1') {
               $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
               $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
          }

          $sql = "SELECT wor_id, wor.wor_type_id, client_name, wor_name, wor_type, project_type, role_types.role_type, SUM(CASE hourly_model.change_type WHEN 1 THEN min_hours WHEN 2 THEN -min_hours END) AS min_hours, SUM(CASE hourly_model.change_type WHEN 1 THEN max_hours WHEN 2 THEN -max_hours END) AS max_hours, 
          min_volume, max_volume, '' AS actual_volume, '' AS billable_hrs, '' AS weekend_hrs, '' AS client_holiday_hrs, project_type_id,
          GROUP_CONCAT(process.process_id) AS process_id  
          FROM wor 
          LEFT JOIN `client` ON client.client_id = wor.client
          LEFT JOIN hourly_model ON hourly_model.fk_wor_id = wor.wor_id
          LEFT JOIN volume_based_model ON volume_based_model.fk_wor_id = wor.wor_id
          LEFT JOIN wor_types ON wor_types.wor_type_id = wor.wor_type_id
          LEFT JOIN project_types ON project_types.project_type_id = hourly_model.fk_project_type_id OR project_types.project_type_id = volume_based_model.fk_project_type_id 
          LEFT JOIN `process` ON process.process_id = hourly_model.process_id OR process.process_id = volume_based_model.process_id 
          LEFT JOIN role_types ON role_types.role_type_id = process.role_type_id
          WHERE (process.name LIKE 'H_%' OR process.name LIKE 'V_%') AND wor_id IN(SELECT DISTINCT fk_wor_id AS wor_id FROM utilization WHERE YEAR(task_date) = '$year' AND MONTH(task_date) = '$month')";


          // $sql = "SELECT wor_id, wor_type_id, client_name, wor_name, wor_type, project_type, role_type ,SUM(min_hours) min_hours,SUM(max_hours) max_hours, min_volume, max_volume, actual_volume, billable_hrs, weekend_hrs, client_holiday_hrs, project_type_id, GROUP_CONCAT(process_id) AS process_id 
          // FROM
          // (SELECT wor.wor_id, wor.wor_type_id, client_name, wor_name, wor_type, project_type, role_types.role_type , SUM(CASE hourly_model.change_type WHEN 1 THEN min_hours WHEN 2 THEN -min_hours END) AS min_hours, SUM(CASE hourly_model.change_type WHEN 1 THEN max_hours WHEN 2 THEN -max_hours END) AS max_hours, min_volume, max_volume, '' AS actual_volume, '' AS billable_hrs, '' AS weekend_hrs, '' AS client_holiday_hrs, project_type_id, process.process_id AS process_id,volume_based_model.change_type v_change_type,hourly_model.change_type h_change_type
          // FROM wor 
          // LEFT JOIN `client` ON client.client_id = wor.client 
          // LEFT JOIN hourly_model ON hourly_model.fk_wor_id = wor.wor_id 
          // LEFT JOIN volume_based_model ON volume_based_model.fk_wor_id = wor.wor_id 
          // LEFT JOIN wor_types ON wor_types.wor_type_id = wor.wor_type_id 
          // LEFT JOIN project_types ON project_types.project_type_id = hourly_model.fk_project_type_id OR project_types.project_type_id = volume_based_model.fk_project_type_id
          // LEFT JOIN `process` ON process.process_id = hourly_model.process_id OR process.process_id = volume_based_model.process_id 
          // LEFT JOIN role_types ON role_types.role_type_id = process.role_type_id 
          // LEFT JOIN (SELECT DISTINCT fk_wor_id AS wor_id FROM utilization WHERE YEAR(task_date) = '$year' AND MONTH(task_date) = '$month')util
          // ON util.wor_id=wor.wor_id
          // WHERE (process.name LIKE 'H_%' OR process.name LIKE 'V_%') /*and wor.wor_id in(80,162)*/
          //  GROUP BY wor.wor_id, wor.wor_type_id, client_name, wor_name, wor_type, project_type, role_types.role_type , min_volume, max_volume, project_type_id , process.process_id) a
          // WHERE process_id NOT IN (SELECT process_id FROM (SELECT fk_wor_id,process_id,COUNT(1) FROM hourly_model GROUP BY fk_wor_id,process_id HAVING COUNT(DISTINCT change_type)>1
          // UNION 
          // SELECT fk_wor_id,process_id,COUNT(1) FROM volume_based_model GROUP BY fk_wor_id,process_id HAVING COUNT(DISTINCT change_type)>1)b)";
                    
		//echo $sql; exit;

          /* start search functionality */		
          $filterWhere = '';
          if(isset($filter) && $filter != '') 
          {
               foreach ($filter as $filterArray) 
               {
                    $filterFieldExp = explode(',', $filterArray['property']);
                    foreach($filterFieldExp as $filterField) 
                    {
                         if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) 
                         {
                              $filterWhere .= ($filterWhere != '')?' OR ':'';
                              $filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
                         }
                    }
               }

               if($filterWhere != '') 
               {
                    $sql .= " AND ";	
                    $sql .= '('.$filterWhere.')';
               } 
          }

          //$sql .= " GROUP BY wor_id, role_type  , wor_type_id, client_name, wor_name, wor_type, project_type, min_volume, max_volume, project_type_id ";
          $sql .= " GROUP BY wor.wor_id,project_types.project_type_id, process.role_type_id";
          //echo $sql;exit;

          $result = $this->db->query($sql)->result_array();
          $utilData = array();
          $loop=0;

          foreach($result as $key => $val)
          {
               if($val['wor_id']!="")
               {
                    $type_ids = explode(",", $val['wor_type_id']);
                    foreach($type_ids as $val2)
                    {
                         $approved_hrs = $this->getBillableHours($month, $year, $val['wor_id'], $val2, $val['project_type_id'], $val['process_id']);
                         $actual_volumes = 0;
                         if($val['min_volume']>0 || $val['max_volume']>0)
                         {
                              $actual_volumes = $this->getActualVolumes($month, $year, $val['wor_id'], $val2, $val['project_type_id'], $val['process_id']);
                         }

                         if($approved_hrs["billable_hrs"]!=null && $approved_hrs["weekend_hrs"]!=null && $approved_hrs["client_holiday_hrs"]!=null)
                         {
                              $billable_hrs = $approved_hrs['billable_hrs'];
                              $weekend_hrs = $approved_hrs['weekend_hrs'];
                              $client_holiday_hrs = $approved_hrs['client_holiday_hrs'];
                              if($this->input->get('csvreport')==1)
                              {
                                   $billable_hrs = $this->secToHR($approved_hrs['billable_hrs']);
                                   $weekend_hrs = $this->secToHR($approved_hrs['weekend_hrs']);
                                   $client_holiday_hrs = $this->secToHR($approved_hrs['client_holiday_hrs']);
                              }

                              $utilData[$loop]['client_name'] 		= $val['client_name'];
                              $utilData[$loop]['wor_name'] 			= $val['wor_name'];
                              $utilData[$loop]['wor_type'] 			= $val['wor_type'];
                              $utilData[$loop]['project_type']		= $val['project_type'];
                              $utilData[$loop]['role_type']			= $val['role_type'];
                              $utilData[$loop]['min_hours']           = (isset($val['min_hours']))>0?$val['min_hours']:0;
                              $utilData[$loop]['max_hours']           = (isset($val['max_hours']))>0?$val['max_hours']:0;
                              $utilData[$loop]['min_volume']          = (isset($val['min_volume']))>0?$val['min_volume']:0;
                              $utilData[$loop]['max_volume']          = (isset($val['max_volume']))>0?$val['max_volume']:0;
                              $utilData[$loop]['actual_volume']       = (isset($actual_volumes['actual_volume']))>0?$actual_volumes['actual_volume']:0;
                              $utilData[$loop]['billable_hrs'] 		= $billable_hrs;
                              $utilData[$loop]['weekend_hrs'] 		= $weekend_hrs;
                              $utilData[$loop]['client_holiday_hrs']	= $client_holiday_hrs;

                              $loop++;
                         }
                    }
               }
          }

          // debug($utilData);exit;

          if($this->input->get('csvreport'))
          {
               $columns[0] = array('Client Name',
                    'Work Order Name',
                    'Work Order Type',
                    'Project Type',
                    'Role Type',
                    'Minimum Hours',
                    'Maximum Hours',
                    'Minimum Volume',
                    'Maximum Volume',
                    'Actual Volume',
                    'Billable Hours',
                    'Weekend Coverage',
                    'Holiday Coverage'
               );

               foreach ($utilData as $key => $value) {
                    $billable_hrs = $this->getBillableHrsInDecimal($value['billable_hrs']);
                    $weekend_hrs = $this->getBillableHrsInDecimal($value['weekend_hrs']);
                    $client_holiday_hrs = $this->getBillableHrsInDecimal($value['client_holiday_hrs']);
                    $utilData[$key]['billable_hrs'] = $billable_hrs ;
                    $utilData[$key]['weekend_hrs'] = $weekend_hrs;
                    $utilData[$key]['client_holiday_hrs'] = $client_holiday_hrs;
               }

               $csvReport = array_merge($columns, $utilData);
               // debug($csvReport);exit;
               return $csvReport;
          }
          else
          {
               $data['rows'] = $utilData;
               $data['totalCount'] = count($utilData);
               return $data;
          }
     }

     function getBillableHrsInDecimal($billable_hrs)
     {
          $timeArr = explode(':',$billable_hrs);
          $converted = $timeArr[0] + ($timeArr[1] / 60);
          return sprintf("%0.02f", $converted);
     }

     function secToHR($seconds) {
          $hours = floor($seconds / 3600);
          $minutes = floor(($seconds / 60) % 60);
          $time = "$hours:$minutes";

          return $time;
     }

     private function getBillableHours($month,$year,$wor_id,$wor_type_id,$project_id,$process_id)
     {
          $sql = "SELECT SUM(IF(`status`='1',TIME_TO_SEC(client_hrs),0)) AS billable_hrs,
          SUM(IF(`status`='3',TIME_TO_SEC(client_hrs),0)) AS weekend_hrs,
          SUM(IF(`status`='4',TIME_TO_SEC(client_hrs),0)) AS client_holiday_hrs
          FROM `utilization` 
          WHERE YEAR(task_date) = '$year' AND MONTH(task_date) = '$month' AND fk_wor_id='$wor_id' AND fk_type_id IN($wor_type_id) AND fk_project_id='$project_id' AND fk_role_id IN($process_id)";
          //echo $sql;exit;
          $result = $this->db->query($sql)->row_array();

          return $result;
     }

     private function getActualVolumes($month,$year,$wor_id,$wor_type_id,$project_id,$process_id)
     {
          $sql = "SELECT SUM(attributes_value) AS actual_volume
          FROM `utilization` 
          JOIN utilization_client_attributes ON utilization_client_attributes.utilization_id = utilization.utilization_id
          WHERE YEAR(task_date) = '$year' AND MONTH(task_date) = '$month' AND fk_wor_id='$wor_id' AND fk_type_id='$wor_type_id' AND fk_project_id='$project_id' AND fk_role_id IN($process_id) AND attributes_id = 200";
          $result = $this->db->query($sql)->row_array();
          // debug($result);
          return $result;
     }

     private function getFteWeekendHours($month,$year)
     {
          $sql = "SELECT SUM(IF(`status`='3',TIME_TO_SEC(client_hrs),0)) AS weekend_hrs
          FROM `utilization`
          LEFT JOIN process ON process.process_id = utilization.fk_role_id
          WHERE (process.name LIKE 'F_%') AND YEAR(task_date) = '$year' AND MONTH(task_date) = '$month'";
          $result = $this->db->query($sql)->row_array();

          return $result['weekend_hrs'];
     }

     public function getFteReportcsv()
     {
          // debug($_GET);exit;
          $mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');

          $time=strtotime($mbrmonth);
          $month=date("m",$time);
          $year=date("Y",$time);
          if($this->input->get('csvreport'))
          {
               $sql      = $this->build_query();
               // $sql .= " LIMIT 0,49";
               // echo $sql;exit;
               $query    = $this->db->query($sql);

               $data = $query->result_array();
               foreach($data as $key=>$value)
               {
                    $fte_query = $this->db->query("Select fte_id from fte_model where fk_wor_id=".$value['fk_wor_id']);
                    $fte_ids = $fte_query->result_array();
                    $fte_id = "";
                    foreach ($fte_ids as $key1 => $value1) {
                         $fte_id.=$value1['fte_id'].",";
                    }

                    $data[$key]['fte_id'] = rtrim($fte_id,',');
                    // if($value['fte_est'] == 0  && $value['fte_emea'] == 0  && $value['fte_ist'] == 0 && $value['fte_lemea'] == 0 &&  $value['fte_cst'] == 0 &&  $value['fte_pst'] == 0 &&  $value['fte_apac'] == 0 && $value['responsibility_hc'] == 0 ){
                    //      unset($data[$key]);
                    // }
               } 


               foreach($data as $key=>$value)
               {
                    $data[$key]['calculated_fte'] = $this->GetFullMonthFte($value['client_id'] ,$value['fk_wor_id'],$value['fk_type_id'],$value['fk_project_id'], $value['responsibility_id'],$month,$year,$value['responsibility_hc'],$time,$value['fte_id'] );

                    $approved_buffer = $this->GetApprovedBuffer($value['fk_wor_id'],$value['fk_project_id'],$value['responsibility_id'],$month,$year);
                    $data[$key]['fte_total_buffer'] = $approved_buffer;
                    
                    $wor_query = $this->db->query("Select wor_name from wor where wor_id =".$data[$key]['fk_wor_id']);
                    $data[$key]['wor_name'] = $wor_query->row()->wor_name;
                    
                    $type_query = $this->db->query("Select wor_type from wor_types where wor_type_id =".$data[$key]['fk_type_id']);
                    $data[$key]['wor_type'] = $type_query->row()->wor_type;
                    
                    $project_query = $this->db->query("Select project_type from project_types where project_type_id =".$data[$key]['fk_project_id']);
                    //echo $project_query;exit;
                    $data[$key]['project_type'] = $project_query->row()->project_type;
                    
                    $role_type_query = $this->db->query("Select role_type from role_types where role_type_id =".$data[$key]['fk_roles_id']);
                    $data[$key]['role_type'] = $role_type_query->row()->role_type;
               }

               // echo "<pre>"; print_r($data);exit;
               foreach($data as $k => $v)
               {
                    foreach($v as $k1 => $v1)
                    {
                         if($k1 == "calculated_fte"){
                              foreach($v1 as $k12 => $v12)
                              {
                                   $data[$k][$k12] = $v12;
                              }
                         }
                    }    
                    
                    $data[$k]['remarks'] = $this->GetFteRemarks($mbrmonth, $v['responsibility_id'],$v['fte_id'],"export" );
               }

               $fteReportData = array();
               $loop = 0;
               foreach ($data as $key => $value) {
                    if($value['fte_total_hc'] != 0 || $value['fte_total_buffer'] != 0  || $value['fte_apac'] != 0  || 
                    $value['fte_lemea'] != 0  || $value['india'] != 0  || $value['usa'] != 0  || 
                    $value['calculated_fte']['prorated_fte'] != 0 )
                    {
                         $fteReportData[$loop]['client_name']         = $value['client_name'];
                         $fteReportData[$loop]['work_order_name']     = $value['wor_name'];
                         $fteReportData[$loop]['wor_type']            = $value['wor_type'];
                         $fteReportData[$loop]['project_type']        = $value['project_type'];
                         $fteReportData[$loop]['role_type']           = $value['role_type'];
                         $fteReportData[$loop]['fte_total_hc']        = $value['fte_total_hc'];
                         $fteReportData[$loop]['fte_total_buffer']    = $value['fte_total_buffer'];
                         $fteReportData[$loop]['fte_apac']            = $value['fte_apac'];
                         $fteReportData[$loop]['fte_lemea']           = $value['fte_lemea'];
                         $fteReportData[$loop]['india']               = $value['india'];
                         $fteReportData[$loop]['usa']                 = $value['usa'];
                         $fteReportData[$loop]['full_month_fte']      = $value['calculated_fte']['full_month_fte'];
                         $fteReportData[$loop]['prorated_fte']        = $value['calculated_fte']['prorated_fte'];
                         $fteReportData[$loop]['remarks']             = $value['remarks'];
                         $loop++;
                    }
               }

               // debug($fteReportData);exit;
               $columns[0] = ['Client Name','Work Order Name','Type','Project','Role','Approved FTE','Approved Buffer','APAC','LEMEA','India (IST, EMEA)','USA (PST, CST, EST)','Full Month FTE','Porated FTE','Remarks'];
               $results = array_merge($columns,$fteReportData);

               return $results;

          }
     }

     public function getProjectReport()
     {
          $filter = json_decode($this->input->get('filter'),true);
          if($this->input->get('mbrmonth'))
          {
               $month = date('m', strtotime($this->input->get('mbrmonth')));
               $year = date('Y', strtotime($this->input->get('mbrmonth')));
          }
          else
          {
               $year = date('Y');
               $month = date('m');
          }

          $sql = "SELECT wor_id, wor.wor_type_id, client_name, wor_name, wor_type, project_type, project_name, start_duration, end_duration, '' AS hours_entered, process.process_id AS process_id, project_type_id, role_types.role_type
          FROM wor 
          LEFT JOIN `client` ON client.client_id = wor.client
          LEFT JOIN project_model ON project_model.fk_wor_id = wor.wor_id
          LEFT JOIN wor_types ON wor_types.wor_type_id = wor.wor_type_id
          LEFT JOIN project_types ON project_types.project_type_id = project_model.fk_project_type_id
          LEFT JOIN `process` ON process.process_id = project_model.process_id
          LEFT JOIN role_types ON role_types.role_type_id = process.role_type_id
          WHERE (process.name LIKE 'P%') AND wor_id IN(SELECT DISTINCT fk_wor_id AS wor_id FROM utilization WHERE YEAR(task_date) = '$year' AND MONTH(task_date) = '$month')";


          $filterWhere = '';
          if(isset($filter) && $filter != '') 
          {
               foreach ($filter as $filterArray) 
               {
                    $filterFieldExp = explode(',', $filterArray['property']);
                    foreach($filterFieldExp as $filterField) 
                    {
                         if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) 
                         {
                              $filterWhere .= ($filterWhere != '')?' OR ':'';
                              $filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
                         }
                    }
               }

               if($filterWhere != '') 
               {
                    $sql .= " AND ";    
                    $sql .= '('.$filterWhere.')';
               } 
          }

          // echo $sql;exit;
          $result = $this->db->query($sql)->result_array();
          // debug($result);exit;
          $data['totalCount'] = $this->db->query($sql)->num_rows();

          $utilData = array();
          $loop=0;

          foreach($result as $key => $val)
          {
               if($val['wor_id']!="")
               {
                    $approved_hrs = $this->getBillableHours($month, $year, $val['wor_id'], $val['wor_type_id'],$val['project_type_id'],$val['process_id']);
                    $billable_hrs = $approved_hrs['billable_hrs'];

                    if($this->input->get('csvreport')==1)
                    {
                         $billable_hrs = $this->secToHR($approved_hrs['billable_hrs']);
                    }
                    $utilData[$loop]['client_name']    = $val['client_name'];
                    $utilData[$loop]['wor_name']       = $val['wor_name'];
                    $utilData[$loop]['wor_type']       = $val['wor_type'];
                    $utilData[$loop]['project_type']   = $val['project_type'];
                    $utilData[$loop]['role_type']      = $val['role_type'];
                    $utilData[$loop]['project_name']   = $val['project_name'];
                    $utilData[$loop]['start_duration'] = date('d-M-Y',strtotime($val['start_duration']));
                    $utilData[$loop]['end_duration']   = date('d-M-Y',strtotime($val['end_duration']));
                    $utilData[$loop]['hours_entered']  = $billable_hrs;

                    $loop++;
               }
          }
// debug($utilData);exit;
          if($this->input->get('csvreport'))
          {
               $columns = array(
                    array_keys($utilData[0])
               );

               foreach ($utilData as $key => $value) {
                    $hours_entered = $this->getBillableHrsInDecimal($value['hours_entered']);
                    
                    $utilData[$key]['hours_entered'] = $hours_entered ;

               }

               $csvReport = array_merge($columns, $utilData);

               return $csvReport;
          }
          else
          {
               $data['rows'] = $utilData;
               return $data;
          }
     }
}


?>
