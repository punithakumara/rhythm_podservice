<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Location_Model extends INET_Model
	{
		function __construct()
		{
			parent::__construct();
		}
	
		function getLocationData(){
			
			$options = array();
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
				
			$sort = json_decode($this->input->get('sort'),true);
				
			$options['sortBy'] = $sort[0]['property'];
			$options['sortDirection'] = $sort[0]['direction'];
			
			if($this->input->get('filterName') != '') {
				$options['filter'][0] = array('property' => $this->input->get('filterName'), 'value' => $this->input->get('query'));
			}
			$totCount = $this->getTotalRecordCount($options,'location');
			$results = array_merge(array('totalCount' => $totCount),array('location' => $this->getValues($options,'location')));
									
			return $results;
		}
	}
?>