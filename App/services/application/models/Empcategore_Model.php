	<?php
		defined('BASEPATH') OR exit('No direct script access allowed');
		
		class Empcategore_Model extends INET_Model {
			
			function __construct() {
				parent::__construct();
				$this->load->model(array('Hcm_Model','Shift_Model','Audit_Log_Model'));
				
			}
			
			function build_query() 
			{
				$empData = $this->session->userdata('user_data');
				$wor_id = $empData['WorId'];
				$filter = json_decode($this->input->get('filter'),true);
				$options['filter'] = $filter;

				$sort = json_decode($this->input->get('sort'),true);					
				$options['sortBy'] = $sort[0]['property'];
				$options['sortDirection'] = $sort[0]['direction'];
				
				$clientId = ($this->input->get('ClientID'))?$this->input->get('ClientID'):0;			
				$LeadId = $this->input->get('Lead') ? $this->input->get('Lead'):0;				
				$Grade = $this->input->get('Grade') ? $this->input->get('Grade'):0;
				$manager = $this->input->get('manager') ? $this->input->get('manager'):0;
				$project_type_id = $this->input->get('project_type_id') ? $this->input->get('project_type_id'):0;
				$wor_id = $this->input->get('wor_id') ? $this->input->get('wor_id'):0;
				$searchTxt = $this->input->get('searchTxt') ? $this->input->get('searchTxt'):"";
				$searchsql = $searchinnersql = "";
				$extraSQL = $employeeArray = '';

				if($searchTxt <> "")
				{
					$searchsql = "AND (e.company_employ_id LIKE '%".$searchTxt."%' OR e.first_name LIKE '%".$searchTxt."%' OR e.last_name LIKE '%".$searchTxt."%' OR d.name LIKE '%".$searchTxt."%' OR m.first_name LIKE '%".$searchTxt."%' OR m.last_name LIKE '%".$searchTxt."%' OR p.project_type LIKE '%".$searchTxt."%' )";
				}

				if($manager <> 0)
				{
					$employeesids = $this->getEmployeeList($manager);
					$employids = implode(",",$employeesids);
					$extraSQL = 'AND temp.employee_id IN ('.$employids.')';
				}
				
				$sql = "SELECT id, CAST(e.employee_id AS UNSIGNED INTEGER) as employee_id,e.first_name as first_name,e.last_name as last_name, e.Email, d.Name AS designationName,e.company_employ_id AS company_employ_id,CAST(SUBSTRING_INDEX(e.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID,
						d.grades,client_id,billable,billable_type, CONCAT(m.first_name, ' ', m.last_name) AS supervisor, p.project_type AS project_type, temp.project_type_id,temp.wor_id
						FROM (SELECT client_id, employee_id, billable,id, billable_type, project_type_id, wor_id FROM employee_client WHERE 1=1 $searchinnersql) AS temp 
						JOIN employees e ON e.employee_id = temp.employee_id
						JOIN  employees m  ON e.primary_lead_id = m.employee_id
						JOIN project_types p ON p.project_type_id = temp.project_type_id
						JOIN designation d ON e.designation_id = d.designation_id 
						WHERE e.status != 6 AND temp.wor_id = '$wor_id' AND temp.client_id = '$clientId' $extraSQL $employeeArray $searchsql";// ORDER BY d.grades DESC,CID ASC			
				
				if(($empData['Grade']>2) && ($empData['Grade']<5) && ($wor_id!=""))
				{
					$sql .= " AND wor_id IN ($wor_id) ";
				}

				if($project_type_id>0)
				{
					$sql .= " AND temp.project_type_id IN ($project_type_id) ";
				}
				
				$filterWhere = '';
				if(isset($options['filter']) && $options['filter'] != '') 
				{
					foreach ($options['filter'] as $filterArray) 
					{
						$filterFieldExp = explode(',', $filterArray['property']);
						foreach($filterFieldExp as $filterField) {
							if($filterField != '') {
								$filterWhere .= ($filterWhere != '')?' OR ':'';
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}
					if($filterWhere != '') {
						$filterWhere = ' ( '.$filterWhere.' ) ';
						$sql .= " and (".$filterWhere.")";
					}
				}
				
				if(isset($options['sortBy'])) {
					
					if ($options['sortBy'] == 'FullName') $options['sortBy'] = 'first_name';
					
					$sort = $options['sortBy'].' '.$options['sortDirection'];
					$sql .= " ORDER BY ".$sort;
				}
				
				return $sql;
			}
			
			
			function getReporteesEmployeeIds($manager, $clientId)
			{									
				//LEVEL2			
				$level2 = "SELECT  DISTINCT `employee_id`, grades FROM `employee_client`
												WHERE `primary_lead_id` IN (SELECT `employee_id` FROM `employee_client`
							WHERE `primary_lead_id` = $manager AND `client_id` = $clientId)";

				$query2 = $this->db->query($level2);
							
				$oneDimensionalArray = array_map('current', $query2->result_array());
				if (is_array($oneDimensionalArray) && count($oneDimensionalArray))
				{	
					$teamleads = implode(",",$oneDimensionalArray);	
				}
				else
				{
					$teamleads = 999999;
				}
				//LEVEL3			
					$level3 = "SELECT DISTINCT `employee_id` FROM `employee_client`WHERE `primary_lead_id` = $manager AND `client_id` = $clientId
						UNION ALL
						SELECT  DISTINCT `employee_id`
						FROM `employee_client`
						WHERE `primary_lead_id` IN (SELECT `employee_id` FROM `employee_client`WHERE `primary_lead_id` = $manager AND `client_id` = $clientId) AND `client_id` = $clientId
						UNION ALL
						SELECT  DISTINCT `employee_id`
						FROM `employee_client`
						WHERE `primary_lead_id` IN ($teamleads) AND `client_id` = $clientId
						#UNION ALL (SELECT DISTINCT $manager FROM `employee_client`)";

				$query3 = $this->db->query($level3);
							
				if ($query3->num_rows() > 0) {
					return $query3->result_array();
				}
				else
				{
					return 0;
				}									
			}
			
			
			function build_vertEmp_query() {
				$filter = json_decode($this->input->get('filter'),true);
				$options['filter'] = $filter;
					
							
				$processId = ($this->input->get('processID'))?$this->input->get('processID'):0;
				
				$this->db->select('e.EmployID,e.FirstName,e.LastName,e.CompanyEmployID, e.Email, d.Name AS designationName, emp.Billable');
				$this->db->from('process p');
				$this->db->join('employprocess emp', 'emp.ProcessID = p.ProcessID');
				$this->db->join('employ e', 'e.employid = emp.employid');
				$this->db->join('designation d', 'e.DesignationID = d.DesignationID');
				
				$where = 'e.DesignationID = d.DesignationID AND p.verticalID = (SELECT VerticalID FROM PROCESS WHERE ProcessID = '.$processId.') AND d.grades < 3 AND emp.EmployID = e.EmployID AND emp.ProcessID !='.$processId.' AND e.RelieveFlag != 1 AND emp.EmployID NOT IN (SELECT EmployID FROM employprocess WHERE ProcessID ='.$processId.')';
				
				$this->db->where($where);
				$this->db->group_by('e.EmployID');
				
				$filterWhere = '';
				if(isset($options['filter']) && $options['filter'] != '') {
					foreach ($options['filter'] as $filterArray) {
						$filterFieldExp = explode(',', $filterArray['property']);
						foreach($filterFieldExp as $filterField) {
							if($filterField != '') {
								$filterWhere .= ($filterWhere != '')?' OR ':'';
								$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
							}
						}
					}
					if($filterWhere != '') {
						$filterWhere = ' ( '.$filterWhere.' ) ';
						$this->db->where($filterWhere, NULL, FALSE);
					}
				}
			}
				
			function getVerticalEmp() {
			
				$options = array();
				$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
				
				$sort = json_decode($this->input->get('sort'),true);
							
				$options['sortBy'] = $sort[0]['property'];
				$options['sortDirection'] = $sort[0]['direction'];
				
				$processId = ($this->input->get('processID'))?$this->input->get('processID'):0;	
					
				$resultSet = array();
				if($processId != 0){	
					$this->build_vertEmp_query();
					$totQuery = $this->db->get();
					$totCount = $totQuery->num_rows();
					$this->build_vertEmp_query();
					
					$query = $this->db->get();
					
					$resultSet = $query->result_array();
				}
				
				$results = array_merge(array('totalCount' => $totCount),array('data' => $resultSet));
				
				return $results;
			
			}
			
			function getCategorization() 
			{
				$options = array();	
				$sql = $this->build_query();
				$totQuery = $this->db->query($sql);
				
				$totCount = $totQuery->num_rows();
				
				$query = $this->db->query($sql);
				
				$data = $query->result_array();
				
				$results = array_merge(array('totalCount' => $totCount),array('data' => $data));
				
				return $results;
			}
			
			/* Get the old shift Value to insert to hcm */
			
			function getoldShiftValue($employID,$grade,$processId){
			
				$this->db->select('ShiftID');
				$this->db->from('employprocess');
				
				$where = 'EmployID = '.$employID.' and ProcessID = '.$processId.'';
				$this->db->where($where);
				$getrecord = $this->db->get();
				$shiftRecord = $getrecord->row();
				$oldSiftValue = $shiftRecord->ShiftID;
				return $oldSiftValue;
				
			}
			
			function ShftChangeDetails() {			
				$EmployID = $this->input->get('EmployID');			
				$Grade = $this->input->get('Grade');			
				$Shift = $this->input->get('Shift');			
				$processId = $this->input->get('processID');			
				$clientID = $this->input->get('clientID');
			
				$data = array('ShiftID' => $Shift);
				
				
				/* Audit Log */		
				$this->load->model('newTransition_model');
				$primary_lead_id = $this->newTransition_model->getPrimaryLeadID($EmployID);
				$empData = $this->session->userdata('user_data');
				$modfdId = $empData['EmployID'];
				$empRelInfo = $this->newTransition_model->empRelInfo($EmployID);
				$fromVert = $empRelInfo[0]['verticalID'];
				$fromClient = $clientID;
				$fromProcess = $empRelInfo[0]['processID'];
				$fromReporting = $primary_lead_id[0]['Primary_Lead_ID'];
				$fromShift = $empRelInfo[0]['ShiftID'];
				$toShift = $Shift;
				$toVert = $empRelInfo[0]['verticalID'];
				$toClient = $clientID;
				$toProcess = $processId;
				$toReporting = $primary_lead_id[0]['Primary_Lead_ID'];
				$empTrnCnt = 0;
				$transDate = date('d-m-Y');
				
				$this->Audit_Log_Model->employTransitionLog($EmployID,$transDate,$modfdId,$fromVert,$fromClient,$fromProcess,$fromReporting,$toVert,$toClient,$toProcess,$toReporting,$fromShift,$toShift, 1);
				
				/* End Of Audit Log*/	
					
					
				/* Start of Shift log */
		if($fromShift != $toShift){
				$this->Audit_Log_Model->employShiftLog($EmployID,$toProcess,$toClient,$toVert,date('Y-m-d'),"0000-00-00",$toShift);		
		}
				/* End of Shift log */
				
				
				/* 
				Insert to hcm_table if only Shift has been changed 
				*/					
				$oldSiftId = $this->getoldShiftValue($EmployID,$Grade,$processId);

				$retVal = $this->db->update('employprocess',$data,array('EmployID' => $EmployID,'ProcessID'=>$processId));
				
				/* Shift Model to get Shift Code*/
				$this->load->model('Shift_Model');
					
						$adHcmData = array(
					'EmployID' => $EmployID,
					'OldValue' => $oldSiftId,
					'NewValue' => $Shift,
					'Type'     => 'Shift',
				);
				
				$hcm = $this->Hcm_Model->insertHcmData($adHcmData);				
				return $retVal;		
			}
			
			function removeLead() {
					
				$EmployID = $this->input->get('EmployID');
				
				$processId = $this->input->get('processID');
				
				$isExists = $this->checkProcessLead($EmployID,$processId);
				
					if(!$isExists){
						
						$existing_array = array(
							'ProcessID' => INI_PROCESS_ID,
							'EmployID' => $EmployID
						);
							
						$retVal = $this->db->update('employprocess',$existing_array,array('EmployID' => $EmployID,'ProcessID'=>$processId));
					}else{
						$this->db->where(array('EmployID' => $EmployID,'ProcessID' => $processId));
				
						$retVal = $this->db->delete('employprocess');
					
					}
				return $retVal;	
				
			}
			
			function removeDetails() {
				
				$EmployID = $this->input->get('EmployID');
				
				$processId = $this->input->get('processID');
				
				$clientID = $this->input->get('clientID');
			
				$this->db->where(array('EmployID' => $EmployID,'ProcessID' => $processId));
				
				$retVal = $this->db->delete('employprocess');
				
				// checking whether employee exists in employprocess if not push him into hr pool
				$this->checkEmployProcess($EmployID,$processId);
				
				$this->db->where(array('EmployID' => $EmployID,'ProcessID' => $processId));
				
				$retVal = $this->db->delete('employ_billability');
				
				return $retVal;
			}
			
			/* main function to update employee billable details */
			function getEmpCategorDetails() {
				$clientID = $this->input->get('client_id');					
				$employIds	 = $this->input->get('employee_id');
				$tblIds = $this->input->get('tblIds');			
				$Billable = $this->input->get('billable'); 
				$billable_type = $this->input->get('billable_type');
				$billable_date = $this->input->get('billable_date');
				$projectIds = $this->input->get('projectIds');
				$worIds = $this->input->get('worIds');

																
				$retVal = $this->updateEmployeeDetails($clientID, $employIds, $tblIds, $Billable, $billable_type, $projectIds, $worIds, $billable_date); 

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$worIds.') has been updated billability type for the employees(Employee IDs:'.$employIds.').';
				$this->userLogs($logmsg);
						
				return $retVal;
			
			}
			
			/* defined function to update employee billable details */
			function updateEmployeeDetails($clientID, $employIds, $tblIds, $Billable, $billable_type, $projectIds, $worIds, $billable_date){
			
					$i = 0;
					$tblIdsArray = explode(";",$tblIds);
					$empIdsArray = explode(";",$employIds);
					$projectIdsArray = explode(";",$projectIds);
					$worIdsArray = explode(";",$worIds);
							
					foreach($tblIdsArray as $tblId) //loop records to be updates
					{															
						/* function call to update employee billable history data */
						$this->update_employee_billable_history($tblId, $Billable, $empIdsArray[$i], $clientID, $billable_type, $projectIdsArray[$i], $worIdsArray[$i], $billable_date);					
						
						/* start updating employee billable data */
						$data = array(
							'billable' => $Billable,
							'billable_type' => $billable_type,	
							);
						
						$retVal = $this->db->update('employee_client',$data,array('id' => $tblId));
						$i++;
						/*end updating employee billable data */
					}
			
				return 1;
			}
			
			/* defined function to update employee billbale history data */
			function update_employee_billable_history($tblId, $billable, $empid, $clientID, $billable_type, $projectIds, $worIds, $billable_date)
			{
				
				/* function call to check whether the billable history record is already available in table */
				$check_record_exist = $this->check_billabality_history_record_available($tblId, $billable, $empid, $clientID, $billable_type);
							
				/* if return value : 0 -> record doesnt exist and insert new record in table  */
				/* if return value : 1 and $billable_type == Buffer  -> update the end date of existing table in db  */
				/* if return value : 1 and $billable_type != Buffer  -> update the end date of existing table in db and insert new record in table  */
				if ($check_record_exist['count'] == 0)
				{
					
					if($billable_type <> "Buffer")
					{
						$empData 		= $this->session->userdata('user_data');			
						$billabality_insert_data = array(
							'ec_pk' 	            => $tblId,
							'employee_id' 	   		=> $empid,
							'client_id' 	   		=> $clientID,
							'project_type_id'		=> $projectIds,
							'wor_id'				=> $worIds,
							'start_date' 	   		=> $billable_date,
							'billable_type'         => $billable_type,
							'changed_by'			=> $empData['employee_id'],
							'updated_by'			=> $empData['employee_id'],
						);
						$this->insert($billabality_insert_data,'employee_billabality_history');
						$insertid = $this->db->insert_id();
					}
				}
				else
				{
					if($billable_type == "Buffer")
					{				
						/* function call to update history data of existing record */
						$this->update_billabale_history_data($check_record_exist['id'], $billable_date);
					}
					else
					{
						/* function call to update history data of existing record */
						$this->update_billabale_history_data($check_record_exist['id'], $billable_date);
							
						if($billable_type <> "Buffer")
						{
							$empData 		= $this->session->userdata('user_data');			
							$billabality_insert_data = array(
								'ec_pk' 	            => $tblId,
								'employee_id' 	   		=> $empid,
								'client_id' 	   		=> $clientID,
								'project_type_id'		=> $projectIds,
								'wor_id'				=> $worIds,
								'start_date' 	   		=> $billable_date,
								//'end_date' 	   		=> date('y-m-d'),
								'billable_type'         => $billable_type,
								'changed_by'			=> $empData['employee_id'],
								'updated_by'			=> $empData['employee_id'],
							);
							$this->insert($billabality_insert_data,'employee_billabality_history');
						}	
					}
				}
			}
			
			/* defined function to update history data of existing record */
			function update_billabale_history_data($id, $billable_date)
			{
				$empData = $this->session->userdata('user_data');
				$billabality_update_data = array(									
					'end_date'		=> $billable_date,
					'updated_by'	=> $empData['employee_id'],
				);
		
				$this->db->where('id', $id);
				$this->db->update('employee_billabality_history',$billabality_update_data);
			}
			
			/* defined function to check whether the billable history record is already available in table */
			function check_billabality_history_record_available($tblId, $billable, $empid, $clientID, $billable_type)
			{
				$finalArray = array();
				$sql = "SELECT * FROM employee_billabality_history WHERE ec_pk = ".$tblId." AND client_id = ".$clientID." AND employee_id = ".$empid." AND end_date IS NULL ";
				$query = $this->db->query ( $sql );
				
				if ($query->num_rows () > 0) 
				{
					$data = $query->result_array();
					$finalArray['count'] = $query->num_rows ();
					$finalArray['id'] = $data['0']['id'];
					$finalArray['model'] = $data['0']['billable_type'];
					
					return $finalArray;
				}
				else
				{
					$finalArray['count'] = $query->num_rows ();
					$finalArray['id'] = 0;
				}		
			}
			
			
			function getBillableLog($processId,$employID,$date,$type,$billType,$stdt="",$percntge="",$clientID,$shiftid)
			{	
				$retVal = 1;
				
				$dateStr = implode('-', array_reverse(explode('/', $date)));
				if($date == null || $date == 'null'){
					$dateStr = NULL;
				}
				if($stdt == ""){
					$stdt = NULL;
				}
				if($type =="insert")
				{
					/* Billable Audit Log */	
					$empData = $this->session->userdata('user_data');
					$modfdId = $empData['employee_id'];
					
					$client_id = $clientID;
					$modfdDate = date('d-m-Y');
					
					$this->Audit_Log_Model->employBillabilityLog($employID,$client_id,$processId,$billType,$percntge,$dateStr,"",$modfdId,$modfdDate,0,$shiftid);
					
					/* End Of Billable Audit Log*/
				}
				else
				{
					/* Billable Audit Log */		
					$empData = $this->session->userdata('user_data');
					$modfdId = $empData['employee_id'];
					
					$client_id = $clientID;
					$enDte = implode('-', array_reverse(explode('/', $dateStr)));
					$modfdDate = date('d-m-Y');
					
					$this->Audit_Log_Model->employBillabilityLog($employID,$client_id,$processId,$billType,$percntge,$stdt,$enDte,$modfdId,$modfdDate,0,$shiftid);
					/* End Of Billable Audit Log*/
				}
			
				return $retVal;
			}
			
			
			/**
			* Function to get Leads name by clients
			*/
			function getTLRelCli()
			{
				$employID = $this->input->get('employID') ? $this->input->get('employID'):'';
				$actEmployID = $this->input->get('actEmployID') ? $this->input->get('actEmployID'):'';
				$vertID =  $this->input->get('vert') ? $this->input->get('vert'):'';
				$filter = json_decode($this->input->get('filter'),true);
				$order = json_decode($this->input->get('sort'),true);

				$employArry ="";
				$employIDStr ='';
				$employAMArry ="";
				$employIDAMStr ='';
				
				$sql = "SELECT `cl`.`client_name` AS Name, `cl`.`client_id` FROM (`client` cl) "; 
				
				if($employID != "")
				{
					$sql .=" JOIN ( SELECT `pl`.`client_id` FROM employprocess pl WHERE `pl`.`EmployID` = ".$employID." AND Grades > 2 AND Grades <6 ) AS temp ON temp.client_id = cl.client_id "; 
				}
				
				$sql .=" WHERE `cl`.`status` = 1"; 
				
				$sql .=" GROUP BY `cl`.`client_id` ";
				
				if($this->input->get('filter') && array_key_exists("property",$filter[0]) && array_key_exists("value",$filter[0])){
					$sql .= " HAVING ".$filter[0]['property']." LIKE '%".$filter[0]['value']."%'";
				}
				
				if(!empty($order[0])){
					$sql .= " ORDER BY ".$order[0]['property']." ".$order[0]['direction'];
				}
				// echo $sql;
				$query = $this->db->query($sql);
				
				if($query->num_rows() > 0) {
					return $query->result_array();
				}			
				return '';			
			}
			
			
			function getTLRelPro(){
				
				$employID =  $this->input->get('employID') ? $this->input->get('employID'):'';
				$processID = $this->input->get('processID') ? $this->input->get('processID'):'';
				$clientID =  $this->input->get('clientID') ? $this->input->get('clientID'):'';
				$vertID =  $this->input->get('vert') ? $this->input->get('vert'):'';
				$filterName = $this->input->get('filterName') ? $this->input->get('filterName') : '';
				$filterVal = $this->input->get('query') ? $this->input->get('query') : '';
				
				
				$sql = " select pro.ProcessID,pro.Name from process pro JOIN ( SELECT `pl`.`ProcessID` FROM employprocess pl "; 
				if($employID !=""){
					$sql .=" WHERE `pl`.`EmployID` = ".$employID;
				}
				
				$sql .=" ) AS temp ON temp.ProcessID = pro.ProcessID  WHERE  pro.ProcessStatus = 0 "; 
				
				if($vertID != "")
					$sql .=" AND pro.VerticalID = ".$vertID;
				
				$sql .=" AND pro.ProcessID !=".$processID." and pro.ClientID =".$clientID." GROUP BY pro.ProcessID";
				
				if($filterVal!=""){
					$sql .= " HAVING $filterName LIKE '%".$filterVal."%'";
				}
				
				$query = $this->db->query($sql);
				
				if($query->num_rows() > 0) {
					return $query->result_array();
				}
				
				return '';
				
			}
			
			
			
			
			function getAmRelCli(){
				
				$employID = $this->input->get('employID') ? $this->input->get('employID'):'';
				$actEmployID = $this->input->get('actEmployID') ? $this->input->get('actEmployID'):'';
				$filterName = $this->input->get('filterName') ? $this->input->get('filterName') : '';
				$filterVal = $this->input->get('query') ? $this->input->get('query') : '';
				$order = json_decode($this->input->get('sort'),true);
				
				$employArry ="";
				
				$employIDStr ='';
				
				$employAMArry ="";
				$employIDAMStr ='';
				
				$this->db->distinct();
				$this->db->select('cl.Client_Name as Name,cl.ClientID');
				$this->db->from('client cl');
				$this->db->join('process pro', 'pro.ClientID = cl.ClientID');
				$where = 'cl.status = 1 AND pro.ProcessStatus = 0';
			
				if($actEmployID !="")
					$employArry = $this->getEmployProN($actEmployID);
			
				if($employArry !=""){
					$j = 0;
					foreach($employArry as $empAry){
						
						if($j != 0)
							$employIDStr .= " , ".$empAry['processID'];
						else
							$employIDStr .= $empAry['processID'];
						
						
						$j++;
					}
				
				}
				
				if($employID !="")
					$employAMArry = $this->getLeadAM($employID);
				
					
				if($employAMArry !=""){
					$j = 0;
					foreach($employAMArry as $empAMAry){					
						if($j != 0)
							$employIDAMStr .= " , ".$empAMAry['ProcessID'];
						else
							$employIDAMStr .= $empAMAry['ProcessID'];
						
						
						$j++;
					}
				
				}
				
				
				if($employAMArry !="")
					$where .= ' AND pro.ProcessID IN( '.$employIDAMStr.' ) ';
				
				if($employArry != "")
					$where .= ' AND pro.ProcessID NOT IN( '.$employIDStr.' )';
				
				if($filterVal!=""){
					$where .= " HAVING $filterName LIKE '%".$filterVal."%'";
				}
				$this->db->where($where);
				$this->db->order_by($order[0]['property'], $order[0]['direction']);
				
				$query = $this->db->get();
				
				if($query->num_rows() > 0) {
					return $query->result_array();
				}
				
				return '';
				
			
			
			}
			
			function getAmRelPro(){
				
				$processId = $this->input->get('processID') ? $this->input->get('processID'):'';
				$clientID = $this->input->get('clientID') ? $this->input->get('clientID'):'';
				$employID = $this->input->get('employID') ? $this->input->get('employID'):'';
				$actEmployID = $this->input->get('actEmployID') ? $this->input->get('actEmployID'):'';
				$filterName = $this->input->get('filterName') ? $this->input->get('filterName') : '';
				$filterVal = $this->input->get('query') ? $this->input->get('query') : '';
				$order = json_decode($this->input->get('sort'),true);
			
				$employArry ="";
				
				$employAMArry ="";
				$employIDAMStr ='';
				
				if($actEmployID !="")
					$employArry = $this->getEmployProN($actEmployID);
				
				$employIDStr ='';
				
				
				$this->db->select('p.ProcessID,p.Name');
				$this->db->from('process p');
				$where = 'p.ProcessStatus = 0 ';

				if($processId !="")
				$where .= ' AND p.ProcessID !='.$processId;
				
				if($clientID !="")
				$where .= ' AND p.ClientID ='.$clientID;
				
				if($employID !="")
					$employAMArry = $this->getLeadAM($employID);
				
				if($employArry !=""){
					$j = 0;
					foreach($employArry as $empAry){
						
						if($j != 0)
							$employIDStr .= " , ".$empAry['processID'];
						else
							$employIDStr .= $empAry['processID'];
						
						
						$j++;
					}
				
				}
				
				if($employAMArry !=""){
					$j = 0;
					foreach($employAMArry as $empAMAry){
						
						if($j != 0)
							$employIDAMStr .= " , ".$empAMAry['ProcessID'];
						else
							$employIDAMStr .= $empAMAry['ProcessID'];
						
						
						$j++;
					}
				
				}

				if($employAMArry !="") 
				$where .= ' AND p.ProcessID IN( '.$employIDAMStr.' ) ';
				
				if($employArry !="") 
				$where .=' AND p.ProcessID NOT IN( '.$employIDStr.' )';
				
				if($filterVal!=""){
					$where .= " HAVING $filterName LIKE '%".$filterVal."%'";
				}
				$this->db->where($where);
				$this->db->order_by($order[0]['property'], $order[0]['direction']);			
				
				$query = $this->db->get();
				
				if($query->num_rows() > 0) {
					return $query->result_array();
				}
				
				return '';
			
			}
			
			function getLeadAM($employ ='')
			{
				$this->load->model('employee_model');
				
				$AMgrade = $this->employee_model->getEmployeegrade($employ);
				
				if(floor($AMgrade[0]['grades']) > 4){
					$sql = "SELECT ProcessID  FROM PROCESS p JOIN `verticalmanager`vm ON p.`VerticalID` = vm.`VerticalID` WHERE vm.`EmployID` = ".$employ ;
				}else{
					$sql = "SELECT ProcessID FROM `employprocess` WHERE `EmployID` = ".$employ ;
				}
				
				$query = $this->db->query($sql);

				if($query->num_rows() > 0) {
					return $query->result_array();
				}
					
				return "";
			}

			
			function getEmployProN($employ ='')
			{		
				$sql = "SELECT processID FROM `employprocess` WHERE `EmployID` = ".$employ." AND Grades>=3 AND Grades<4" ;
				$query = $this->db->query($sql);
				
				if($query->num_rows() > 0) {
					return $query->result_array();
				}				
				return ""; 		
			}
			
			
			function getProcessAm(){			
				$clientId = $this->input->get('clientID') ? $this->input->get('clientID') : '';
				$processId = $this->input->get('processID') ? $this->input->get('processID'):'';
				$VerticalID = $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';
				$EmployID = $this->input->get('EmployID') ? $this->input->get('EmployID') : '';
				$filterName = $this->input->get('filterName') ? $this->input->get('filterName') : '';
				$query = $this->input->get('query') ? $this->input->get('query') : '';			
				
				if($EmployID != ""){
					$sql = "SELECT DISTINCT(e.EmployID),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name` FROM employ e JOIN ( SELECT pm.employid,pm.processid,pm.grades FROM  employprocess pm) AS temp ON temp.employid = e.employid JOIN PROCESS pro ON pro.`ProcessID` = temp.`ProcessID` WHERE e.RelieveFlag != 1 AND temp.grades > 3 AND pro.`VerticalID` = ".$VerticalID;
				}else if($VerticalID != ""){
					$sql = "SELECT DISTINCT(e.EmployID),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name` FROM employ e left join employprocess pm ON e.employid = pm.employid JOIN `designation` des ON des.`DesignationID` = e.`DesignationID` JOIN PROCESS pro ON pro.`ProcessID` = pm.`ProcessID` where e.RelieveFlag != 1 and des.grades > 3 and pro.`VerticalID` = ".$VerticalID;
				}else{
					$sql = "SELECT DISTINCT(e.EmployID),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name` FROM employ e left join employprocess pm ON e.employid=pm.employid JOIN `designation` des ON des.`DesignationID` = e.`DesignationID` where ";
					
					if($processId != "")
						$sql .= " ( pm.ProcessID='".$processId."') AND "; 
					
					$sql .= " e.RelieveFlag != 1 and des.grades > 3 ";
				}
				
				if($filterName!="" && $query!=""){
					$sql .= " HAVING $filterName LIKE '%".$query."%'";
				}
				
				$query = $this->db->query($sql);
				
				if($query->num_rows() > 0) {
					return $query->result_array();
				}else{
					$sql = "SELECT DISTINCT(vm.`EmployID`),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name` FROM employ e JOIN `verticalmanager` vm ON e.employid = vm.employid JOIN process p ON p.`VerticalID` = vm.`VerticalID` WHERE e.RelieveFlag != 1";
					if($processId != ""){
					$sql .= " AND p.ProcessID=$processId";
					}
					$query = $this->db->query($sql);
					
					if($query->num_rows() > 0) 
						return $query->result_array();
					else{
						$sql = "SELECT DISTINCT(e.`EmployID`),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name` FROM employ e JOIN `designation` des ON des.`DesignationID` = e.`DesignationID` WHERE des.grades = 7 limit 1";
						
						$query = $this->db->query($sql);
						
						if($query->num_rows() > 0) 
							return $query->result_array();
					}					
				}			
				return '';		
			}
			
			function getProcessManagers($grades){	
				$sql = "SELECT DISTINCT(e.EmployID),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name`,e.DesignationID,d.grades FROM employ e JOIN designation d ON e.DesignationID = d.DesignationID WHERE e.RelieveFlag=0 AND d.grades >".$grades." AND d.grades <=7 ORDER BY e.`FirstName` ASC";
				$query = $this->db->query($sql);
				if($query->num_rows() > 0) 
				return $query->result_array();		
			}
			
			function checkEmployProcess($employID){
			
				$this->db->select('e.ProcessID,e.EmployID');
				$this->db->from('employprocess e');
				$where = 'e.EmployID ='.$employID.' and e.PrimaryProcess = 1';
				
				$this->db->where($where);
				$totQuery = $this->db->get();
				$totCount = $totQuery->num_rows();
			
				if(!($totCount > 0)){
					
					$existing_array = array(
							'ProcessID' => INI_PROCESS_ID,
							'EmployID' => $employID
						);
					
					$retVal = $this->db->insert('employprocess', $existing_array); 
				}
				
			}
			
			function checkProcessLead($employID,$processId){
				
				$this->db->select('e.ProcessID,e.EmployID');
				$this->db->from('employprocess e');
				$where = 'e.EmployID ='.$employID.' AND e.ProcessID !='.$processId.' AND Grades >2 AND Grades <4';
				
				$this->db->where($where);
				$totQuery = $this->db->get();
				$totCount = $totQuery->num_rows();
			
				if($totCount > 0)
					return true;

				return false;
			
			}
			
			
			function checkEmployBillability($employID,$processId,$startdate,$enddate=""){
			
				$this->db->select('e.ProcessID,e.EmployID');
				$this->db->from('employ_billability e');
				$where = 'e.EmployID ='.$employID.' AND e.ProcessID='.$processId;
				
				$this->db->where($where);
				$totQuery = $this->db->get();
				$totCount = $totQuery->num_rows();
			
				if($totCount > 0)
					return true;

				return false;
			}
			
			
			
			function getAM_leads(){
				
				$clientId = $this->input->get('clientID') ? $this->input->get('clientID') : '';
				$processId = $this->input->get('processID') ? $this->input->get('processID'):'';
				$employID = $this->input->get('employID') ? $this->input->get('employID'):'';
			
					$this->db->select('e.EmployID');
					$this->db->from('employ e');
					$where = 'e.Primary_Lead_ID ='.$employID.' AND e.RelieveFlag != 1 AND e.EmployID in (select emp.EmployID from employprocess emp where emp.ProcessID ='.$processId.' and emp.PrimaryProcess = 1)';
					$this->db->where($where);
					$totQuery = $this->db->get();
					$totCount = $totQuery->num_rows();

					if($totCount > 0) {
						return $totCount;
					}		
					
					return '';
			}
			
			function getProcessLead(){
				
				$clientId = $this->input->get('clientID') ? $this->input->get('clientID') : '';
				$processId = $this->input->get('processID') ? $this->input->get('processID'):'';
				
				
				$sql = "SELECT DISTINCT(pm.EmployID),CONCAT(IFNULL(e.`FirstName`, ''),' ',IFNULL(e.`LastName`, '')) AS `Name` FROM employ e JOIN `employprocess` pm ON e.employid=pm.employid AND pm.ProcessID='".$processId."' AND e.RelieveFlag != 1 AND Grades >2 AND Grades <4";
				$query = $this->db->query($sql);
				
				$result_data = array( 0 => array('EmployID' => 0, 'Name' => 'Select All'));
				
				$data = $query->result_array();
				
				foreach ($data	 as $key => $entry) {
						array_push($result_data,$data[$key]);	
				}
				
				return $result_data;
			}
			
			
			/**
			* @description : Getting employee billability log for particular employee
			* @return : billable end date
			*/
			function getBillableLogDetails() 
			{
				$clientId = ($this->input->get('clientId')) ? $this->input->get('clientId') : "";
				$processId = ($this->input->get('processId')) ? $this->input->get('processId') : "";
				$employId = ($this->input->get('employId')) ? $this->input->get('employId') : "";
				
				if($processId != 0)
				{
					$query = $this->db->query("SELECT billabilityData FROM `employ_billability` WHERE employid = $employId");
					$empLog = unserialize($query->row()->billabilityData);
					
					$results = "";
					foreach($empLog as $key => $value)
					{
						if($value['client_id'] == $clientId && $value['process_id'] == $processId)
						{
							$results = array('end_date' => str_replace('-','/',$value['end_date']));
						}
					}
					
					return $results;
				} 
				else 
				{
					return '';
				}
			}
			
			
		function get_fte_summary_list()
			{
			$client_id = ($this->input->get('clientId'));
			$empData = $this->session->userdata('user_data');
			$today = date("Y-m-d");
			$res = array();
			$totCount = array();
			
			$wor_ids = $this->getWorID($client_id,$today);
			$cor_ids = $this->getCorID($client_id,$today);
			if($wor_ids != '')
			{
				$wor_fte_id = $this->getWorModelIds($wor_ids,'fte_id','fte_model',$today);
			}
			else
			{
				$wor_fte_id  = "";
			}
			if($cor_ids != '')
			{
				$cor_fte_id = $this->getCorModelIds($cor_ids,'fte_id','fte_model',$today);
			}
			else
			{
				$cor_fte_id  = "";
			}
			
			$fte_ids = '';
			if($wor_fte_id!='' && $cor_fte_id != '')
			{
				$fte_ids = $wor_fte_id.','.$cor_fte_id;
			}
			else if($wor_fte_id !='' &&  $cor_fte_id == '' )
			{
				$fte_ids = $wor_fte_id;
			}
			else if($wor_fte_id =='' &&  $cor_fte_id != '' )
			{
				$fte_ids = $cor_fte_id;
			}
			
			if($fte_ids != "")
			{
				$sql = "SELECT fte_id,responsibilities,work_order_id,SUM(CASE WHEN shift = '1' THEN no_of_fte ELSE '' END) AS 'est_fte', SUM(CASE WHEN shift = '2' THEN no_of_fte ELSE '' END) AS 'emea_fte', SUM(CASE WHEN shift = '3' THEN no_of_fte ELSE '' END) AS 'ist_fte',  SUM(CASE WHEN shift = '4' THEN no_of_fte ELSE '' END) AS 'lemea_fte', SUM(CASE WHEN shift = '5' THEN no_of_fte ELSE '' END) AS 'cst_fte', SUM(CASE WHEN shift = '6' THEN no_of_fte ELSE '' END) AS 'pst_fte', SUM(CASE WHEN shift = '7' THEN no_of_fte ELSE '' END) AS 'apac_fte'  FROM (SELECT wor_id,fte_id,shift, SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte END) AS no_of_fte, wor.work_order_id AS work_order_id, role_types.role_type AS responsibilities,fk_wor_id FROM fte_model LEFT JOIN role_types ON role_types.role_type_id = fte_model.responsibilities LEFT JOIN wor ON fte_model.fk_wor_id = wor.wor_id  WHERE fte_id IN ($fte_ids) GROUP BY shift,responsibilities,fk_wor_id) AS MYFTE ";
				if($empData['Grade']>2 && $empData['Grade']<5)
				{
					$sql .= "WHERE MYFTE.wor_id IN(".$empData['WorId'].")";
				}
				$sql .= " GROUP BY MYFTE.responsibilities,fk_wor_id";
				$query = $this->db->query($sql);
				$totCount = $query->num_rows();
				$res = $query->result_array();
			
				$results = array_merge(
					array('totalCount' => $totCount),
					array('rows' => $res)
				);
				return $res;
			}
			else
			{
				$results = array_merge(
					array('totalCount' => 0),
					array('rows' => '')
				);
				return $results;
			}
			}
		
		function getWorID($client_id,$today)
		{
			$sql = "SELECT GROUP_CONCAT(wor_id) as wor_id FROM `wor` WHERE CLIENT = '$client_id' AND `status` ='Open' AND `start_date` <='$today' GROUP BY CLIENT";
			$query  = $this->db->query($sql);
			$row = $query->row();

			if (isset($row))
			{
				return $row->wor_id;
			}
			else
			{
				return "";
			}
		}
		
		function getCorID($client_id,$today)
		{		
			$sql = "SELECT GROUP_CONCAT(cor_id) as cor_id FROM `cor` WHERE CLIENT = '$client_id' AND `status` <> 'In-Progress' AND `change_request_date` <='$today' GROUP BY CLIENT";
			$query = $this->db->query($sql);
			$row = $query->row();
			
			if (isset($row))
			{
				return $row->cor_id;
			}
			else
			{
				return "";
			}
		}
		
		function getWorModelIds($wor_id,$primary_key,$table,$today)
		{
			$query = $this->db->query("SELECT GROUP_CONCAT($primary_key) AS Ids FROM $table WHERE fk_wor_id IN ($wor_id) AND fk_cor_id IS NULL AND anticipated_date <= '$today'");
			
			$row = $query->row();
			if (isset($row))
			{
				return $row->Ids;
			}
			else
			{
				return "";
			}
		}
		
		function getCorModelIds($cor_id,$primary_key,$table,$today)
		{
			
			$query = $this->db->query("SELECT GROUP_CONCAT($primary_key) AS Ids FROM $table WHERE fk_cor_id IN ($cor_id) AND anticipated_date <='$today'");
		
			$row = $query->row();
			if (isset($row))
			{
				return $row->Ids;
			}else{
				return "";
			}
			
		}
		
			
	}

	?>