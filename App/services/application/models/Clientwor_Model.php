<?php defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Clientwor_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Audit_Log_Model');
		date_default_timezone_set('Asia/Kolkata');
	}
	
    /**
	* Get list of Client WOR
	*
	* @param 
	* @return array
	*/ 
	function view()
	{
		$sort = json_decode($this->input->get('sort'),true);
		$filter = json_decode($this->input->get('filter'),true); //filter functionality
		$status = ($this->input->get('status')!="")?$this->input->get('status'):'All'; //wor_id		
		$work_order_id = ($this->input->get('work_order_id')!="")?$this->input->get('work_order_id'):"";		
		$all_wor_ids = ($this->input->get('all_wor_ids')!="")?$this->input->get('all_wor_ids'):0;		
		$open_wor = ($this->input->get('open_wor')!="")?$this->input->get('open_wor'):0;
		$empData = $this->input->cookie();
		
		$options = array();
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		/* FilterQuery Added  to Filter Vlues in Dropdown in COR*/
		$filterQuery 		= $this->input->get('query');
		
		$sql = "SELECT wor.wor_id, wor.work_order_id,wor.wor_name,'' AS wor_type,wor.wor_type_id,wor.service_id, wor.client, DATE_FORMAT(wor.start_date, '%d-%b-%Y') AS start_date, 
		DATE_FORMAT(wor.end_date, '%d-%b-%Y') AS end_date, wor.status,wor.client_partner, 
		wor.engagement_manager, wor.delivery_manager,wor.associate_manager,wor.team_lead, wor.stake_holder, wor.time_entry_approver, wor.created_by, c.`client_name`, 
		wor.parent_work_order_id AS previous_work_order_id, wor.updated_by,  
		CONCAT(IFNULL(e.`first_name`, ''),' ',IFNULL(e.`last_name`, '')) AS `created_name`, wor.description,
		CONCAT(IFNULL(cp.`first_name`, ''),' ',IFNULL(cp.`last_name`, '')) AS cp_name, 
		CONCAT(IFNULL(em.`first_name`, ''),' ',IFNULL(em.`last_name`, '')) AS em_name, 
		CONCAT(IFNULL(dm.`first_name`, ''),' ',IFNULL(dm.`last_name`, '')) AS dm_name,
		'' AS sh_name, '' AS tl_name, '' AS am_name, '' AS ta_name,
		CONCAT(IFNULL(ub.`first_name`, ''),' ',IFNULL(ub.`last_name`, '')) AS `updated_name`,
		DATE_FORMAT(wor.created_on, '%d-%b-%Y') AS created_on
		FROM wor 
		JOIN `client` c ON c.`client_id` = wor.`client`
		LEFT JOIN employees cp ON cp.employee_id = wor.client_partner
		LEFT JOIN employees em ON em.employee_id = wor.engagement_manager
		LEFT JOIN employees dm ON dm.employee_id = wor.delivery_manager
		LEFT JOIN employees e ON e.employee_id = wor.created_by
		LEFT JOIN employees ub ON ub.employee_id = wor.updated_by
		LEFT JOIN wor_types wt ON wt.wor_type_id = wor.wor_type_id
		LEFT JOIN tbl_rhythm_services ts ON ts.service_id = wor.service_id 
		WHERE 1 = 1 ";
		
		if(!$all_wor_ids)
		{
			if ($status != "All")
			{
				$sql .= " And wor.status='".$status."'";
			}
			else
			{
				$sql .= " And wor.status <> 'Deleted' ";
			}		
		}
		
		if ($work_order_id != "")
		{
			$sql .= " AND wor.status <> 'Deleted' AND wor.work_order_id = '".$work_order_id."'";
		}
		
		if ($open_wor == "1")
		{
			$sql .= " And wor.status <> 'Deleted' AND wor.status <> 'Closed' AND wor.status <> 'In-Progress' ";
		}
		if ($this->input->get('notStatus') != "")
		{
			$sql .= " AND wor.status NOT IN (".$this->input->get('notStatus').")";
		}
		if($empData['Grade']>= 3 && $empData['Grade'] < 4 ){
			$sql .= " And wor.client IN (".$empData['ClientId'].") AND (wor.wor_id IN (".$empData['WorId'].") OR wor.created_by=".$empData['employee_id'].")";

		}
		/* start search functionality */		
		$filterWhere = '';
		if(isset($filter) && $filter != '') 
		{
			foreach ($filter as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}

			if($filterWhere != '') 
			{
				$sql .= " AND ";	
				$sql .= '('.$filterWhere.')';
			} 
		}
		/* end search functionality */	
		
		/* FilterQuery Added  to Filter Vlues in Dropdown in COR*/
		if($filterQuery !="")
		{
			$sql .=" AND (wor.work_order_id  LIKE '%".$filterQuery."%')";
		}
		
		if(isset($sort[0]['property']))
		{
			if($sort[0]['property']=="start_date" || $sort[0]['property']=="end_date")
			{
				$sql .= " ORDER BY wor.".$sort[0]['property']." ".$sort[0]['direction'];
			}
			else
			{
				$sql .= " ORDER BY ".$sort[0]['property']." ".$sort[0]['direction'];
			}
		}
		else
		{
			$sql .= " ORDER BY wor.wor_id DESC ";
		}

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " LIMIT ".$limit;
		}

		$query = $this->db->query($sql);
		$data = $query->result_array();

		foreach($data as $key=>$value)
		{
			foreach($value as $k=>$val)
			{
				if($k=='time_entry_approver')
				{
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS ta_name FROM employees WHERE employee_id IN (". $val." )";
						$query = $this->db->query($sql);
						$res = $query->result_array();
						foreach($res as $field => $name)
						{
							$data[$key]['ta_name']=$res[0]['ta_name'];
						}
					}
				}
				else if($k=='stake_holder')
				{
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS sh_name FROM employees WHERE employee_id IN (". $val." )";
						$query = $this->db->query($sql);
						$res = $query->result_array();
						foreach($res as $field => $name)
						{
							$data[$key]['sh_name']=$res[0]['sh_name'];
						}
					}
				}
				else if($k=='team_lead')
				{
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS tl_name FROM employees WHERE employee_id IN (". $val." )";
						$query = $this->db->query($sql);
						$res = $query->result_array();
						foreach($res as $field => $name)
						{
							$data[$key]['tl_name']=$res[0]['tl_name'];
						}
					}

				}
				else if($k=='associate_manager')
				{
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, ''))) AS am_name FROM employees WHERE employee_id IN (". $val." )";
						$query = $this->db->query($sql);
						$res = $query->result_array();
						foreach($res as $field => $name)
						{
							$data[$key]['am_name']=$res[0]['am_name'];
						}
					}

				}
				else if($k=='wor_type_id')
				{
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(wor_type) AS wor_type FROM wor_types WHERE wor_type_id IN (". $val." )";
						$query = $this->db->query($sql);
						$res = $query->result_array();
						foreach($res as $field => $name)
						{
							$data[$key]['wor_type']=$res[0]['wor_type'];
						}
					}
				}
				else if($k=='service_id')
				{
					if($val!="")
					{
						$sql="SELECT GROUP_CONCAT(name) AS name FROM tbl_rhythm_services WHERE service_id IN (". $val." )";
						$query = $this->db->query($sql);
						$res = $query->result_array();
						foreach($res as $field => $name)
						{
							$data[$key]['name']=$res[0]['name'];
						}
					}
				}
			}
		}
		
		foreach($data as $key => $value){
			$data[$key]['count_history'] = $this->count_wor_history($value['work_order_id']);
			$data[$key]['corids'] = $this->get_cor_ids($value['work_order_id']);
			$data[$key]['resourceCount'] = $this->get_wor_allocated_resource($value['client'], $value['wor_id']);
		}
		
		$results = array_merge(
			array('totalCount' => $totCount),
			array('rows' => $data)
		);

		return $results;
	}
	
	function projects()
	{
		$this->db->select("project_types.project_type_id, project_types.project_type"); 
		$this->db->from("project_types");
		
		if($this->input->get('wor_type_id')!="")
		{
			$this->db->join("wor_project_role_mapping", "wor_project_role_mapping.project_type_id = project_types.project_type_id");
			$this->db->where("wor_type_id IN ('".str_replace(",","','",$this->input->get('wor_type_id'))."')");
		}
		if($this->input->get('work_order_id')!="")
		{
			$sql = "SELECT GROUP_CONCAT(fk_project_type_id) AS fk_project_type_id FROM (SELECT fk_project_type_id FROM fte_model WHERE fk_wor_id =".$this->input->get('work_order_id')." AND anticipated_date <= CURDATE() GROUP BY shift, support_coverage, responsibilities
			HAVING SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte END)>0
			UNION
			SELECT fk_project_type_id FROM hourly_model WHERE fk_wor_id =".$this->input->get('work_order_id')." AND anticipated_date <= CURDATE() GROUP BY shift, support_coverage, responsibilities
			HAVING SUM(CASE change_type WHEN 1 THEN min_hours+max_hours  WHEN 2 THEN -(min_hours+max_hours) END)>0
			UNION
			SELECT fk_project_type_id FROM volume_based_model WHERE fk_wor_id =".$this->input->get('work_order_id')." AND anticipated_date <= CURDATE() GROUP BY shift, support_coverage, responsibilities
			HAVING SUM(CASE change_type WHEN 1 THEN min_volume+max_volume  WHEN 2 THEN -(min_volume+max_volume) END)>0
			UNION
			SELECT fk_project_type_id FROM project_model WHERE fk_wor_id =".$this->input->get('work_order_id').") temp";
			$query = $this->db->query($sql);
			$res = $query->row_array();
			
			$this->db->join("wor_project_role_mapping", "wor_project_role_mapping.project_type_id = project_types.project_type_id");
			$this->db->where("project_types.project_type_id IN (".$res['fk_project_type_id'].")");
		}
		$this->db->group_by("project_types.project_type_id");
		$this->db->order_by("project_type", "ASC");
		$query = $this->db->get();
		$totCount = $query->num_rows();
		$data = $query->result_array();
		//echo $this->db->last_query(); exit;
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('rows' => $data)
		);
		return $results;
	}
	
	function roles()
	{
		$this->db->select('role_types.role_type_id AS id, role_types.role_type AS name'); 
		$this->db->from('role_types');
		// if($this->input->get('project_type_id')!="")
		// {
		// 	$this->db->join('wor_project_role_mapping', 'wor_project_role_mapping.role_type_id = role_types.role_type_id');
		// 	$this->db->where('project_type_id', $this->input->get('project_type_id'));
			$this->db->where('status= 1');
		//}
		$this->db->order_by('role_type', 'ASC');
		$query = $this->db->get();
		//echo $this->db->last_query();
		//exit;
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('rows' => $data)
		);
		return $results;
	}

	/*Get role types for wor by wor_id and project_id*/
	function get_role_types($wor_id)
	{
		$sql = "SELECT role_type as role_types FROM role_types WHERE role_type_id 
		IN(SELECT DISTINCT role_type_id FROM process WHERE process_id 
		IN (SELECT  DISTINCT project_type_id FROM employee_client WHERE wor_id = ".$wor_id."))";                            
		$query = $this->db->query($sql);
		$data =  $query->result_array();
		$jr_str = '';
		foreach ($data as $key => $value) {
			$jr_str .= $value['role_types'].','; 
		}
		$jr_str = substr($jr_str, 0, -1);
		return $jr_str;
	}
	
	function count_wor_history($id)
	{
		$this->db->select('*'); 
		$this->db->from('wor_cor_transition');
		$this->db->where('work_order_id', $id);
		$query = $this->db->get();
		$result1 = $query->num_rows();
		if ($result1 != 0)
		{
			$result = $result1- 1;
		}
		else{
			$result = $result1;
		}
		return $result;
		
	}
	
	function get_cor_ids($id)
	{

		$output = $this->db->query("SELECT GROUP_CONCAT(change_order_id) AS corids FROM cor WHERE work_order_id="."'".$id."'")->row()->corids;
		$output_exp=explode(",",$output);
		foreach($output_exp as $change_order){
			$cor_id = $this->db->query("SELECT cor_id  FROM cor WHERE change_order_id="."'".$change_order."'")->row()->cor_id;
			if($cor_id !=''){
				$corid[]= "<a href='#' style='text-decoration: none' onClick='AM.app.getController(\"ClientCOR\").onViewCORFromWor(\"$cor_id\")'> $change_order </a>";
			}
		}
		
		return $corid;
		
	}
	
	function add_wor($data = '', $fte_details = '', $hourly_details = '', $volume_list = '', $project_list = '' ,$client_name)
	{
		$data = json_decode(trim($data), true); 
		$empData = $this->input->cookie();    	    	 	  	
		$fte_details	= json_decode($fte_details, true);
		$hourly_details	= json_decode($hourly_details, true);
		$volume_details	= json_decode($volume_list, true);
		$project_details	= json_decode($project_list, true);
		
		$history_data=array();				
		
		if($data['wor_id'] == '')
		{
			if ($data['status'] == 'In-Progress') $return_value = 1;   		
			if ($data['status'] == 'Open') $return_value = 2;
			
			$dataset = array(
				'work_order_id'			=> $data['work_order_id'],
				'wor_name' 	          	=> $data['wor_name'],
				'wor_type_id' 	        => implode(',',$data['wor_type_id']),
				'start_date' 	        => date("Y-m-d", strtotime($data['start_date'])),
				'client' 		        => $data['client'],
				'service_id' 		    => $data['service_id'],
				'status' 		        => $data['status'],
				'parent_work_order_id' 	=> $data['previous_work_order_id'],
				'client_partner'        => $data['client_partner'],
				'engagement_manager'    => $data['engagement_manager'],
				'delivery_manager'      => $data['delivery_manager'],
				'stake_holder'          => (!empty($data) ? implode(',', $data['stake_holder']) : $data['stake_holder']=''),
				'end_date'              => date("Y-m-d", strtotime($data['end_date'])),
				'created_by' 			=> $empData['employee_id'],
				'team_lead' 	        =>(!empty($data) ? implode(',', $data['team_lead']) : $data['team_lead']=''), 
				'associate_manager' 	=> (!empty($data) ? implode(',', $data['associate_manager']) : $data['associate_manager']=''),
			);


			$retVal   = $this->insert($dataset, 'wor');
			
			$insertid = $this->db->insert_id();
			
			$history_data['client_id'] =$data['client'];
			$history_data['client_partner'] = $data['client_partner'];
			$history_data['engagement_manager'] = $data['engagement_manager'];
			$history_data['delivery_manager'] = $data['delivery_manager'];
			$history_data['fk_wor_id'] = $insertid;
			$history_data['work_order_id'] = $data['work_order_id'];
			
			if(is_array($fte_details))
			{
				$fte_id=array();
				foreach($fte_details as $fte_key=>$fte_value)
				{
					$fte_start_date = $fte_value['anticipated_date'];
					$fte_actual_date = $fte_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{
						$Process_id= $this->create_process('F',$fte_value['shift'],$fte_value['support_coverage'],$fte_value['responsibilities'],'FTE',$data['client']);					
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_wor_id' 	    	=> $insertid,
						'fk_project_type_id' 	=> $fte_value['fk_project_type_id'],
						'no_of_fte' 	    	=> $fte_value['no_of_fte'],
						'appr_buffer' 	    	=> $fte_value['appr_buffer'],
						'shift' 		    	=> $fte_value['shift'],
						'experience' 			=> $fte_value['experience'],
						'skills' 	        	=> $fte_value['skills'],
						'anticipated_date' 		=> date("Y-m-d", strtotime($fte_start_date)),
						'support_coverage' 		=> $fte_value['support_coverage'],
						'responsibilities' 		=> $fte_value['responsibilities'],
						'change_type' 	   		=> 1,
						'process_id' 	   		=> $Process_id,
						'comments' 	   			=> $fte_value['comments'],
					);
					$this->insert($dataset, 'fte_model');
					$fte_id[] = $this->db->insert_id();
					if ($data['status'] == 'Open')
					{
						$worStartDate = $data['start_date'];						
						$this->update_clients_table($data['client'], $fte_value['no_of_fte'], $fte_value['appr_buffer'], $fte_start_date, $empData['employee_id']);
						$this->update_fte_details($data,$dataset);
					}
				}
			}
			
			if(is_array($hourly_details))
			{
				$hourly_id=array();
				
				foreach($hourly_details as $hourly_key=>$hourly_value)
				{
					$hourly_start_date = $hourly_value['anticipated_date'];
					$hourly_actual_date = $hourly_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{
						$Process_id= $this->create_process('H',$hourly_value['shift'],$hourly_value['support_coverage'],$hourly_value['responsibilities'],'Hourly',$data['client']);					
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_wor_id' 	    	=> $insertid,
						'fk_project_type_id' 	=> $hourly_value['fk_project_type_id'],
						'hours_coverage' 		=> $hourly_value['hours_coverage'],
						'shift' 		    	=> $hourly_value['shift'],
						'min_hours' 			=> $hourly_value['min_hours'],
						'max_hours' 	    	=> $hourly_value['max_hours'],
						'experience'        	=> $hourly_value['experience'],
						'anticipated_date' 		=> date("Y-m-d", strtotime($hourly_start_date)),
						'skills' 	        	=> $hourly_value['skills'],
						'support_coverage' 		=> $hourly_value['support_coverage'],
						'responsibilities' 		=> $hourly_value['responsibilities'],
						'change_type' 	   		=> 1,
						'process_id' 	   		=> $Process_id,
						'comments' 	   			=> $hourly_value['comments'],
					);
					$this->insert($dataset, 'hourly_model');
					$hourly_id[] = $this->db->insert_id();	   		
				}
			}
			
			if(is_array($volume_details))
			{
				
				$volume_id=array();
				foreach($volume_details as $volume_key=>$volume_value)
				{
					$volume_start_date = $volume_value['anticipated_date'];
					$volume_actual_date = $volume_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{
						$Process_id= $this->create_process('V',$volume_value['shift'],$volume_value['support_coverage'],$volume_value['responsibilities'],'Volume',$data['client']);					
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_wor_id' 	    	=> $insertid,
						'fk_project_type_id'	=> $volume_value['fk_project_type_id'],
						'volume_coverage' 		=> $volume_value['volume_coverage'],
						'shift' 		    	=> $volume_value['shift'],
						'min_volume' 			=> $volume_value['min_volume'],
						'max_volume' 	    	=> $volume_value['max_volume'],
						'experience'        	=> $volume_value['experience'],
						'anticipated_date' 		=> date("Y-m-d", strtotime($volume_start_date)),
						// 'actual_date'		=> date("Y-m-d", strtotime($volume_actual_date)),
						'skills' 	        	=> $volume_value['skills'],
						'support_coverage' 		=> $volume_value['support_coverage'],
						'responsibilities' 		=> $volume_value['responsibilities'],
						'change_type' 	   		=> 1,
						'process_id' 	   		=> $Process_id,
						'comments' 	   			=> $volume_value['comments'],
					);
					$this->insert($dataset, 'volume_based_model');
					$volume_id[] = $this->db->insert_id();
					
				}
			}
			
			if(is_array($project_details))
			{
				$project_id=array();
				foreach($project_details as $project_key=>$project_value)
				{
					$project_start_date = $project_value['anticipated_date'];
					$project_actual_date = $project_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{	
						$Process_id= $this->create_process('P',$project_value['shift'],$project_value['support_coverage'],$project_value['responsibilities'],'Project',$data['client']);					
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_wor_id' 	        => $insertid,
						'fk_project_type_id' 	=> $project_value['fk_project_type_id'],
						'project_name' 	        => $project_value['project_name'],
						'shift' 		        => $project_value['shift'],
						'start_duration' 		=> date("Y-m-d", strtotime($project_value['start_duration'])),
						'end_duration' 	        => date("Y-m-d", strtotime($project_value['end_duration'])),
						'skills'                => $project_value['skills'],
						'experience'            => $project_value['experience'],
						'anticipated_date' 	    => date("Y-m-d", strtotime($project_start_date)),
						// 'actual_date' 	    => date("Y-m-d", strtotime($project_actual_date)),
						'support_coverage' 		=> $project_value['support_coverage'],
						'responsibilities' 		=> $project_value['responsibilities'],
						'project_description' 	=> $project_value['project_description'],
						'change_type' 	   	    => 1,
						'process_id' 	  		=> $Process_id,
						'comments' 	   			=> $project_value['comments'],
					);
					$this->insert($dataset, 'project_model');
					$project_id[] = $this->db->insert_id();
				}
			}
			
			$history_data['client_id'] =$data['client'];
			$history_data['fte_id'] = implode(",",$fte_id);
			$history_data['hourly_based_id'] = implode(",",$hourly_id);
			$history_data['volume_base_id'] = implode(",",$volume_id);
			$history_data['project_model_id'] = implode(",",$project_id);
			$history_data['level'] = '0';
			$this->insert($history_data, 'wor_cor_transition');
			
			$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];
			
			$wor_notfic          = array(
				'Title' 	=> 'Client WOR',
				'WOR_ID' 	=> $data['work_order_id'],
				'Client' 	=> $data['client'],   				
				'Flag' 		=> 1
			);
			$notification_data   = array(
				'Title' 			=> json_encode($wor_notfic),
				'Description' 		=> "New WOR Added -- " . $data['work_order_id'],
				'SenderEmployee' 	=> $empData['employee_id'],
				'Type' 				=> '1',
				'ReceiverEmployee' 	=> $receiverEmployee,
				'URL' 				=> ''
			);
			$this->addNotification($notification_data);
		}
		else
		{    

			if ($data['status'] == 'In-Progress') $return_value = 3;   		 
			if ($data['status'] == 'Open')
			{
				$return_value = 4;  			
				$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];
				
				$wor_notfic          = array(
					'Title' => 'Client WOR',
					'WOR_ID' => $data['work_order_id'],
					'Client' => $data['client'],
					'Flag' => 1
				);
				$notification_data   = array(
					'Title' => json_encode($wor_notfic),
					'Description' => "WOR Open -- " . $data['work_order_id'],
					'SenderEmployee' => $empData['employee_id'],
					'Type' => '1',
					'ReceiverEmployee' => $receiverEmployee,
					'URL' => ''
				);
				$this->addNotification($notification_data);
				
			}
			if ($data['status'] == 'Deleted') 
			{
				$return_value = 5;    			    			
				$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];
				
				$wor_notfic          = array(
					'Title' => 'Client WOR',
					'WOR_ID' => $data['work_order_id'],
					'Client' => $data['client'],
					'Flag' => 1
				);
				$notification_data   = array(
					'Title' => json_encode($wor_notfic),
					'Description' => "WOR Deleted -- " . $data['work_order_id'],
					'SenderEmployee' => $empData['employee_id'],
					'Type' => '1',
					'ReceiverEmployee' => $receiverEmployee,
					'URL' => ''
				);
				$this->addNotification($notification_data);
			}
			
			$dataset = array(
				'work_order_id' 	    => $data['work_order_id'],
				'wor_name' 	          	=> $data['wor_name'],
				'wor_type_id' 	        => implode(',',$data['wor_type_id']),
				'start_date' 	        => date("Y-m-d", strtotime($data['start_date'])),
				'client' 		        => $data['client'],
				'service_id' 		    => $data['service_id'],
				'status' 		        => $data['status'],
				'parent_work_order_id'	=> $data['previous_work_order_id'],
				'client_partner'        => $data['client_partner'],
				'engagement_manager'    => $data['engagement_manager'],
				'delivery_manager'      => $data['delivery_manager'],
				'stake_holder'          => implode(',',$data['stake_holder']),
				'end_date'              => date("Y-m-d", strtotime($data['end_date'])),
				'team_lead' 	        => implode(',',$data['team_lead']),
				'associate_manager' 	=> implode(',',$data['associate_manager']),
				'updated_by' 			=> $empData['employee_id'],
				'description' 			=> $data['description'],
			);
			
			$this->db->where('wor_id', $data['wor_id']);
			$retVal = $this->db->update('wor',$dataset);
			
			if(is_array($fte_details))
			{
				$fte_id=array();
				
				foreach($fte_details as $fte_key=>$fte_value)
				{
					$fte_start_date = $fte_value['anticipated_date'];
					$fte_actual_date = $fte_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{
						$Process_id= $this->create_process('F',$fte_value['shift'],$fte_value['support_coverage'],$fte_value['responsibilities'],'FTE',$data['client']);					
						
					}	
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_project_type_id'	=> $fte_value['fk_project_type_id'],
						'no_of_fte' 	    	=> $fte_value['no_of_fte'],
						'appr_buffer' 	    	=> $fte_value['appr_buffer'],
						'shift' 		    	=> $fte_value['shift'],
						'experience' 			=> $fte_value['experience'],
						'skills' 	        	=> $fte_value['skills'],
						'anticipated_date' 		=> date("Y-m-d", strtotime($fte_start_date)),
						// 'actual_date' 		=> date("Y-m-d", strtotime($fte_actual_date)),
						'support_coverage' 		=> $fte_value['support_coverage'],
						'responsibilities' 		=> $fte_value['responsibilities'],
						'change_type' 	   		=> 1,
						'process_id' 	  		=> $Process_id,
						'comments' 	   			=> $fte_value['comments'],
					);
					
					if($fte_value['fte_id'] != "")
					{
						$this->db->where('fte_id', $fte_value['fte_id']);
						$this->db->update('fte_model',$dataset);
						$fte_id[] =$fte_value['fte_id'];
					}
					else
					{    					
						$dataset['fk_wor_id'] = $data['wor_id'];    					
						$this->insert($dataset, 'fte_model');
						$fte_id[] = $this->db->insert_id();
					}
					if ($data['status'] == 'Open')
					{
						$worStartDate = $data['start_date'];						
						$this->update_clients_table($data['client'], $fte_value['no_of_fte'],$fte_value['appr_buffer'], $fte_start_date, $empData['employee_id']);
						
					}					
				}
			}
			
			if(is_array($hourly_details))
			{
				$hourly_id=array();
				
				foreach($hourly_details as $hourly_key=>$hourly_value)
				{
					$hourly_start_date = $hourly_value['anticipated_date'];
					$hourly_actual_date = $hourly_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{
						$Process_id= $this->create_process('H',$hourly_value['shift'],$hourly_value['support_coverage'],$hourly_value['responsibilities'],'Hourly',$data['client']);
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_project_type_id'	=> $hourly_value['fk_project_type_id'],
						'hours_coverage' 		=> $hourly_value['hours_coverage'],
						'shift' 		    	=> $hourly_value['shift'],
						'min_hours' 			=> $hourly_value['min_hours'],
						'max_hours' 	    	=> $hourly_value['max_hours'],
						'experience'        	=> $hourly_value['experience'],
						'anticipated_date' 		=> date("Y-m-d", strtotime($hourly_start_date)),
						'skills' 	        	=> $hourly_value['skills'],
						'support_coverage' 		=> $hourly_value['support_coverage'],
						'responsibilities' 		=> $hourly_value['responsibilities'],
						'change_type' 	   		=> 1,
						'process_id' 	  		=> $Process_id,
						'comments' 	   			=> $hourly_value['comments'],
					);   			   				
					
					if($hourly_value['hourly_id'] != "")
					{
						$this->db->where('hourly_id', $hourly_value['hourly_id']);
						$this->db->update('hourly_model',$dataset);
						$hourly_id[] = $hourly_value['hourly_id'];
					}
					else
					{
						$dataset['fk_wor_id'] = $data['wor_id'];
						$this->insert($dataset, 'hourly_model');
						$hourly_id[] = $this->db->insert_id();
					}
				}
			}
			
			if(is_array($volume_details))
			{
				$volume_id=array();
				
				foreach($volume_details as $volume_key=>$volume_value)
				{
					$volume_start_date = $volume_value['anticipated_date'];
					$volume_actual_date = $volume_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{
						$Process_id= $this->create_process('V',$volume_value['shift'],$volume_value['support_coverage'],$volume_value['responsibilities'],'Volume',$data['client']);
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_project_type_id'	=> $volume_value['fk_project_type_id'],
						'volume_coverage' 		=> $volume_value['volume_coverage'],
						'shift' 		    	=> $volume_value['shift'],
						'min_volume' 			=> $volume_value['min_volume'],
						'max_volume' 	    	=> $volume_value['max_volume'],
						'experience'        	=> $volume_value['experience'],
						'anticipated_date' 		=> date("Y-m-d", strtotime($volume_start_date)),
						'skills' 	        	=> $volume_value['skills'],
						'support_coverage' 		=> $volume_value['support_coverage'],
						'responsibilities' 		=> $volume_value['responsibilities'],
						'change_type' 	   		=> 1,
						'process_id' 	  		=> $Process_id,
						'comments' 	   			=> $volume_value['comments'],
					);    				   				
					
					if($volume_value['vb_id'] != "")
					{
						$this->db->where('vb_id', $volume_value['vb_id']);
						$this->db->update('volume_based_model',$dataset);
						$volume_id[] = $volume_value['vb_id'];
					}
					else
					{
						$dataset['fk_wor_id'] = $data['wor_id'];
						$this->insert($dataset, 'volume_based_model');
						$volume_id[] = $this->db->insert_id();
					}	
				}
			}
			
			if(is_array($project_details))
			{
				$project_id=array();
				
				foreach($project_details as $project_key=>$project_value)
				{
					$project_start_date = $project_value['anticipated_date'];
					$project_actual_date = $project_value['actual_date'];
					
					if ($data['status'] == 'Open')
					{	
						$Process_id= $this->create_process('P',$project_value['shift'],$project_value['support_coverage'],$project_value['responsibilities'],'Project',$data['client']);
					}
					else{
						$Process_id = '0';
					}
					
					$dataset = array(
						'fk_project_type_id'	=> $project_value['fk_project_type_id'],
						'project_name' 	        => $project_value['project_name'],
						'shift' 		        => $project_value['shift'],
						'start_duration' 		=> date("Y-m-d", strtotime($project_value['start_duration'])),
						'end_duration' 	        => date("Y-m-d", strtotime($project_value['end_duration'])),
						'skills'                => $project_value['skills'],
						'experience'            => $project_value['experience'],
						'anticipated_date' 	    => date("Y-m-d", strtotime($project_start_date)),
						'support_coverage' 		=> $project_value['support_coverage'],
						'responsibilities' 		=> $project_value['responsibilities'],
						'project_description' 	=> $project_value['project_description'],
						'change_type' 	   		=> 1,
						'process_id' 	  		=> $Process_id,
						'comments' 	   			=> $project_value['comments'],
					);

					if($project_value['project_id'] != "")
					{
						$this->db->where('project_id', $project_value['project_id']);
						$this->db->update('project_model',$dataset);
						$project_id[] = $project_value['project_id'];
					}
					else
					{
						$dataset['fk_wor_id'] = $data['wor_id'];
						$this->insert($dataset, 'project_model');
						$project_id[] = $this->db->insert_id();
					}
					
				}
			}
			
			$history_data['client_id'] =$data['client'];
			$history_data['client_partner'] = $data['client_partner'];
			$history_data['engagement_manager'] = $data['engagement_manager'];
			$history_data['delivery_manager'] = $data['delivery_manager'];
			
			$history_data['fte_id'] = implode(",",$fte_id);
			$history_data['hourly_based_id'] = implode(",",$hourly_id);
			$history_data['volume_base_id'] = implode(",",$volume_id);
			$history_data['project_model_id'] = implode(",",$project_id);
			
			$this->db->where('fk_wor_id',  $data['wor_id']);
			$this->db->update('wor_cor_transition',$history_data);
			
		}
		
		if($return_value == 1){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$insertid.') has been updated Client Work Order Draft Saved Successfully.';
			$this->userLogs($logmsg);
		}
		else if ($return_value == 2)
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$insertid.') has been updated Client Work Order Submitted Successfully.';
			$this->userLogs($logmsg);
		}
		else if($return_value == 3)
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has been updated Client Work Order Draft Updated Successfully.';
			$this->userLogs($logmsg);
		}
		else if ($return_value == 4)
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has been updated Client Work Order Submitted Successfully.';
			$this->userLogs($logmsg);
		}
		else if ($return_value == 5)
		{
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has been updated Client Work Order Deleted Successfully.';
			$this->userLogs($logmsg);
		}
		
		
		return $return_value;
	}
	
    /**
     * Update head_count in clients table.
     *
     * @param
     * @return
     */
    function update_clients_table($clientID, $newFteCount,$newBufferCount, $fte_start_date, $createdBy)
    {
    	$existingFteCount = $this->getFteCountByClientID($clientID);						
    	$existingApprovedBuferCount = $this->getApprovedBufferCountByClientID($clientID);						
    	$diffDays = (strtotime($fte_start_date) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
    	
    	if ($diffDays <= 0)
    	{
    		$dataset = array(
    			'head_count'   => $existingFteCount+$newFteCount,
    			'approved_buffer'   => $existingApprovedBuferCount+$newBufferCount,
    		);
    		
    		$this->db->where('client_id', $clientID);
    		$this->db->update('client',$dataset);
    	}
    	else
    	{												
    		$dataset = array(
    			'client_id'   => $clientID,
    			'start_date'  => date("Y-m-d", strtotime($fte_start_date)),
    			'forecast_fte' => $newFteCount,
    			'forecast_appr_buffer' => $newBufferCount,
					'forecast_type' => 0, //0 -> WOR addition
					'forecast_impact' => 1,
					'created_date' => date("Y-m-d"),
					'created_by' => $createdBy
				);
    		$this->insert($dataset, 'client_forecast');								
    	}					
    }

    function update_fte_details($data,$dataset,$change_type=1){
    	if(sizeof($data['wor_type_id'])>0){
    		$type_id=$data['wor_type_id'][0];
    	}
    	else{
    		$type_id=1;
    	}
    	$sql = $this->db->query("select * from fte_billability_report where fk_client_id=".$data['client']." and fk_wor_id=".$dataset['fk_wor_id']." and fk_type_id=".$type_id." and fk_project_id=".$dataset['fk_project_type_id']." and fk_roles_id=".$dataset['responsibilities']." ");
    	$result = $sql->row();
    	
    	$statement = "";

    	if($dataset['shift'] == 1 || $dataset['shift'] == 24 || $dataset['shift'] == 25 || $dataset['shift'] == 26 || $dataset['shift'] == 27){
    		$shift.="fte_est";
    	}
    	if($dataset['shift'] == 2 || $dataset['shift'] == 17 || $dataset['shift'] == 18 || $dataset['shift'] == 19){
    		$shift.="fte_emea";
    	}
    	if($dataset['shift'] == 3 || $dataset['shift'] == 11 || $dataset['shift'] == 12 || $dataset['shift'] == 13 || $dataset['shift'] == 14 || $dataset['shift'] == 15 || $dataset['shift'] == 16){
    		$shift.="fte_ist";
    	}
    	if($dataset['shift'] == 4 || $dataset['shift'] == 20 || $dataset['shift'] == 21 || $dataset['shift'] == 22 || $dataset['shift'] == 23){
    		$shift.="fte_lemea";
    	}
    	if($dataset['shift'] == 5){
    		$shift.="fte_cst";
    	}
    	if($dataset['shift'] == 6){
    		$shift.="fte_pst";
    	}
    	if($dataset['shift'] == 7 || $dataset['shift'] == 8 || $dataset['shift'] == 9 || $dataset['shift'] == 10){
    		$shift.="fte_apac";
    	}

    	if ($sql->num_rows() > 0)
    	{
    		$statement.= " Update fte_billability_report set fte_total_buffer=fte_total_buffer+".$dataset['appr_buffer']."";
    		
    		if($dataset['comments']){
    			$statement.=",comments='".$dataset['comments']."'";
    		}
    		if($change_type==1){
    			$statement.=",".$shift."=".$shift."+".$dataset['no_of_fte']."+".$dataset['appr_buffer'];
    		}
    		else if($result->$shift>=$dataset['no_of_fte']){
    			$statement.=",".$shift."=".$shift."-".$dataset['no_of_fte'];
    		}
    		else{
    			$statement.=",".$shift."=0.00";
    		}
    		
    		$statement.=",updated_on=NOW()";
    		$statement.=" where fk_client_id=".$data['client']." and fk_wor_id=".$dataset['fk_wor_id']." and fk_type_id=".$type_id." and fk_project_id=".$dataset['fk_project_type_id']." and fk_roles_id=".$dataset['responsibilities']."";
    	}
    	else{

    		$statement.=" Insert into fte_billability_report(fk_client_id,fk_wor_id,fk_type_id,fk_project_id,fk_roles_id,fte_total_buffer,".$shift.",fte_total_hc,comments,created_on)values($data[client],$dataset[fk_wor_id],$type_id,$dataset[fk_project_type_id],$dataset[responsibilities],$dataset[appr_buffer],$dataset[no_of_fte],".$dataset['no_of_fte']."+".$dataset['appr_buffer'].",'".$dataset['comments']."','".date('Y-m-d H:i:s')."') ";
    	}
    	
    	$this->db->query($statement);
    	return true;
    }
    
    function getFteCountByClientID($clientID)
    {
    	$sql = "SELECT `head_count` FROM `client` WHERE `client_id` = ".$clientID;
    	$query = $this->db->query($sql);
    	
    	if ($query->num_rows() > 0)
    	{
    		$row = $query->row(); 
    		return $row->head_count;
    	}

    	return '';
    }
    
    function getApprovedBufferCountByClientID($clientID)
    {
    	$sql = "SELECT `approved_buffer` FROM `client` WHERE `client_id` = ".$clientID;
    	$query = $this->db->query($sql);
    	
    	if ($query->num_rows() > 0)
    	{
    		$row = $query->row(); 
    		return $row->approved_buffer;
    	}

    	return '';
    }
    
	/**
     * Get FTE list of  WOR
     *
     * @param
     * @return array
     */
	function get_fte_list()
	{
		$wor_id = ($this->input->get('wor_id')!="")?$this->input->get('wor_id'):''; 
		$count_val = ($this->input->get('level')!="")?$this->input->get('level'):''; 
		
		$sql = "SELECT `fte_id`, `fk_wor_id`, `fk_cor_id`, `fk_project_type_id`, `no_of_fte`, `appr_buffer`, `shift`, `experience`, `skills`, anticipated_date, DATE_FORMAT(actual_date, '%d-%M-%Y') AS actual_date, `support_coverage`, `responsibilities`,`change_type`,`process_id`, comments,pt.project_type FROM fte_model fm";     
		$sql .= " LEFT JOIN project_types pt ON pt.project_type_id = fm.fk_project_type_id";
		$sql .= " LEFT JOIN role_types rt ON rt.role_type_id = fm.responsibilities";
		$sql .= " WHERE fk_wor_id='".$wor_id."'";
		$query = $this->db->query($sql);
		
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount),
			array('rows' => $data)
		);
		
		return $results;
		
		
	}
	
	
    /**
     * Get HOURLY list of  WOR
     *
     * @param
     * @return array
     */
    function get_hourly_list()
    {
    	$wor_id = ($this->input->get('wor_id')!="")?$this->input->get('wor_id'):'';
    	$count_val = ($this->input->get('level')!="")?$this->input->get('level'):''; 


    	$sql = "SELECT `hourly_id`, `fk_cor_id`, `fk_project_type_id`, `hours_coverage`, `shift`, `min_hours`, `max_hours`, `experience`, `skills`, anticipated_date, DATE_FORMAT(actual_date, '%d-%M-%Y') AS actual_date, `support_coverage`, `responsibilities`,`change_type`,`process_id`, comments FROM `hourly_model`";
    	$sql .= " LEFT JOIN role_types rt ON rt.role_type_id = hourly_model.responsibilities";
    	$sql .= " WHERE fk_wor_id='".$wor_id."'";

    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();

    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;

    }
    
    /**
     * Get volume list of  WOR
     *
     * @param
     * @return array
     */
    function get_volume_list()
    {
    	$wor_id = ($this->input->get('wor_id')!="")?$this->input->get('wor_id'):'';
    	$count_val = ($this->input->get('level')!="")?$this->input->get('level'):''; 
    	
    	$sql = "SELECT `vb_id`, `fk_wor_id`, `fk_cor_id`, `fk_project_type_id`, `volume_coverage`, `shift`, `min_volume`, `max_volume`, `experience`, `skills`, anticipated_date, DATE_FORMAT(actual_date, '%d-%M-%Y') AS actual_date, `support_coverage`, `responsibilities`, `change_type`,`process_id`, comments FROM `volume_based_model`";
    	$sql .= " LEFT JOIN role_types rt ON rt.role_type_id = volume_based_model.responsibilities";
    	$sql .= " WHERE fk_wor_id='".$wor_id."'";
    	
    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();
    	
    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;

    }
    
    function get_project_list()
    {
    	$wor_id = ($this->input->get('wor_id')!="")?$this->input->get('wor_id'):'';
    	
    	$count_val = ($this->input->get('level')!="")?$this->input->get('level'):''; 
    	
    	$sql = "SELECT `project_id`, `fk_wor_id`, `fk_cor_id`, `fk_project_type_id`, `project_name`, `shift`, start_duration,end_duration, `experience`, `skills`,anticipated_date, DATE_FORMAT(actual_date, '%d-%M-%Y') AS actual_date, `support_coverage`, `responsibilities`, `change_type`, project_description,`process_id`, comments FROM `project_model`";
    	$sql .= " WHERE fk_wor_id='".$wor_id."'";
    	
    	$query = $this->db->query($sql);
    	$totCount = $query->num_rows();
    	$data = $query->result_array();
    	
    	$results = array_merge(
    		array('totalCount' => $totCount),
    		array('rows' => $data)
    	);
    	
    	return $results;

    }
    
    function update_status($data = '')
    {  	
    	$empData = $this->input->cookie();
		$this->removeAllResources($data['wor_id'], $data['client_id']);	//remove all resources of wor	
		$receiverEmployee = $data['engagement_manager'].','.$data['client_partner'].','.$data['delivery_manager'].','.$empData['employee_id'];

		$dataset = array(
			'status'   => $data['status'],
			'description' => $data['description'],
			'closed_on' => date("Y-m-d H:i:s"),
			'closed_by' => $empData['employee_id'],
		);
		
		$this->db->where('wor_id', $data['wor_id']);
		$retVal = $this->db->update('wor',$dataset);
		
		$this->removeFteCount($data['fte_count'],$data['client_id']);

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$data['wor_id'].') has updated work order status.';
		$this->userLogs($logmsg);
		
		return $retVal;
	}
	
	function update_extension($data = '')
	{
		$empData = $this->input->cookie();
		$wor_dataset = array(
			'end_date'   => date("Y-m-d",strtotime($data['end_date'])),
		);
		
		$this->db->where('wor_id', $data['wor_id']);
		$retVal = $this->db->update('wor',$wor_dataset);
		
		$exten_dataset = array(
			'wor_id'   			=> $data['wor_id'],
			'date_of_entry'   	=> date("Y-m-d",strtotime($data['log_date'])),
			'new_start_date'	=> date("Y-m-d",strtotime($data['start_date'])),
			'new_end_date'   	=> date("Y-m-d",strtotime($data['end_date'])),
			'description'   	=> $data['extenDescription'],
			'created_by'   		=> $empData['employee_id'],
		);
		
		$retVal = $this->db->insert('wor_extension',$exten_dataset);

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$data['wor_id'].') has updated Work order new end date.';
		$this->userLogs($logmsg);
		
		return $retVal;
	}
	
	function removeFteCount($fteCount, $clientID)
	{
		if($fteCount > 0)
		{
			$existingFteCount = $this->getFteCountByClientID($clientID);
			$dataset = array(
				'head_count'   => $existingFteCount-$fteCount,
			);

			$this->db->where('client_id', $clientID);
			$this->db->update('client',$dataset);
		}
	}
	
	/* Remove all resources of WOR */
	function removeAllResources($wor_id, $client_id)
	{		
		$allResources = $this->getAllResources($wor_id, $client_id); /* Get all resources by work order id */
		
		if ($allResources['totalCount'] > 0)
		{
			foreach ($allResources['rows'] as $rowkey=>$rowvalue)
			{
				// Start Audit Log		
				$empData = $this->input->cookie();
				$modfdId 		= $empData['employee_id'];
				$empRelInfo 	= $this->empRelInfo($rowvalue['employee_id']);
				$fromVert 		= $empRelInfo[0]['verticalID'];
				$fromClient 	= $rowvalue['client_id'];
				$fromReporting 	= $rowvalue['primary_lead_id'];
				$toVert 		= null;
				$toClient 		= null;
				$toReporting 	= null;
				$empTrnCnt 		= 0;
				$transDate 		= date('Y-m-d');
				
				$this->Audit_Log_Model->employTransitionLog($rowvalue['employee_id'], $transDate, $modfdId, $fromVert, $fromClient, $fromReporting, $toVert,$toClient, $toReporting, 0,"Removed");
				
				$this->update_billabale_history_data($rowvalue['employee_id'], $client_id, "",$transDate);
			}
		}
		
		$sql_res ="DELETE FROM employee_client WHERE client_id = $client_id AND wor_id = $wor_id";					
		$this->db->query($sql_res);		
	}
	
	/* get all resources by work order id */
	function getAllResources($wor_id, $client_id)
	{
		$sql = 'SELECT * FROM employee_client WHERE client_id = '.$client_id.' AND wor_id = '.$wor_id;
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array();
		$results = array_merge(
			array('totalCount' => $totCount),
			array('rows' => $data)
		);
		
		return $results; 
	}
	
	function get_wor_list($client_id)
	{ 	
		
		$filter = json_decode($this->input->get('filter'),true);
		$empData = $this->input->cookie();
		$options['filter'] = $filter;
		$filterName = 'work_order_id';
		$filterQuery = $this->input->get('query');
		
		$not_wor_id = ($this->input->get('not_wor_id')!="")?$this->input->get('not_wor_id'):'';
		
		$sql = "SELECT `work_order_id` FROM `wor` WHERE (wor.status = 'Closed' OR wor.status = 'Open') AND wor.client =".$client_id;
		
		if ($not_wor_id != '') 
		{
			$sql .= "AND work_order_id <> '".$not_wor_id."' ";   	
			if($filterName !="" && $filterQuery!=""){
				$sql .= "AND ".$filterName." LIKE '%".$filterQuery."%'";
			}   		   	
		}
		else
		{
			if($filterName !="" && $filterQuery!=""){
				$sql .= "AND ".$filterName." LIKE '%".$filterQuery."%'";
			}
		}
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount),
			array('rows' => $data)
		);
		return $results;    
		
	}
	
	function create_process($model_short,$shift_id,$support_coverage_id,$responsibilities_id,$model,$client_id)
	{
		$shift = $this->db->query("SELECT time_zone FROM shift WHERE shift_id = '$shift_id'")->row()->time_zone;
		
		$support = $this->db->query("SELECT short_code FROM support_coverage WHERE support_id = '$support_coverage_id'")->row()->short_code;
		
		$responsibilities = $this->db->query("SELECT short_code FROM role_types WHERE role_type_id = '$responsibilities_id'")->row()->short_code;
		
		$Process_ID = $model_short."_".$shift."_".$responsibilities."_".$support;
		
		$rec = $this->pro_exists($Process_ID,$client_id);
		
		if ($rec == 0)	
		{			
			$processset = array(
				'fk_client_id' 	=> $client_id,
				'name' 	   		=> $Process_ID,
				'model' 	   	=> $model,
				'shift_id' 	   	=> $shift_id,
				'support_id' 	=> $support_coverage_id,
				'role_type_id'	=> $responsibilities_id,
			);
			$this->insert($processset,'process');
			$insertid = $this->db->insert_id();
			return $insertid;
		}
		else
		{
			return $rec;
		}
		
		
	}
	/**
	Check If the Process already exist in Db 
	If Exist return the respective ID
	**/
	
	public function pro_exists($Process_ID,$client_id)
	{	
		$output = $this->db->query("SELECT process_id FROM process WHERE name = '$Process_ID' AND fk_client_id='$client_id'")->row()->process_id;
		
		if($output != '')
		{
			return $output;
		}
		else
		{
			return 0;
		}
	}
	
	/**
     * Get Summary list of  WOR
     *
     * @param
     * @return array
     */
	
	function get_summary_list()
	{
		$wor_id = ($this->input->get('wor_id')!="")?$this->input->get('wor_id'):''; 
		$level = ($this->input->get('level')!="")?$this->input->get('level'):''; 
		$work_order_id = ($this->input->get('work_order_id')!="")?$this->input->get('work_order_id'):''; 
		
		$hourly_summary = array();
		$volume_summary = array(); 
		$fte_summary = array();
		$project_summary = array();
		
		if($wor_id != '')
		{
			$fte_summary = $this->get_fte_summary_list($wor_id); 
		}
		if($wor_id != '')
		{
			$hourly_summary = $this->get_hourly_summary_list($wor_id); 
		}
		if($wor_id != '')
		{
			$volume_summary = $this->get_volume_summary_list($wor_id);  
		}
		if($wor_id != '')
		{
			$project_summary = $this->get_project_summary_list($wor_id); 
		}

		foreach($fte_summary as $key => $csm)
		{
			$fte_summary[$key]['model'] = 'FTE Details';
			$fte_summary[$key]['min'] = '0';
			$fte_summary[$key]['max'] = '0';
			
			if($csm['fte'] <= 0)
			{ 
				unset ($fte_summary[$key]); 
			}
		}
		
		foreach($hourly_summary as $key => $csm)
		{
			$hourly_summary[$key]['model'] = 'Hourly Details';
			$hourly_summary[$key]['fte'] = '0';
			
			if($csm['max'] <= 0)
			{ 
				unset ($hourly_summary[$key]); 
			}
		}
		
		foreach($volume_summary as $key => $csm)
		{
			$volume_summary[$key]['model'] = 'Volume Details';
			$volume_summary[$key]['fte'] = '0';
			
			if($csm['max'] <= 0)
			{ 
				unset ($volume_summary[$key]); 
			}
		}
		
		foreach($project_summary as $key => $csm)
		{
			$project_summary[$key]['model'] = 'Project Details';
			$project_summary[$key]['fte'] = '0';
		}
		
		$result = array_merge($fte_summary, $hourly_summary);
		$result = array_merge($result, $volume_summary);
		$result = array_merge($result, $project_summary);
		$remove_array = array();
		
		foreach($result as $key => $value) 
		{
			if(in_array($result[$key]['model'], $remove_array)) 
			{
				unset($result[$key]);
			}
		}
		
		$res = array_values($result);
		$da1=array();
		
		for($i=0;$i<count($res);$i++)
		{
			array_push($da1,$res[$i]);
		}
		
		$results = array_merge(
			array('totalCount' => count($res)),
			array('rows' => $da1)
		);
		//print_r($results);
		//exit;
		return $results;
	}

	function get_hourly_summary_list($wor_id)
	{
		$sql = "SELECT fk_wor_id As projectId,SUM(CASE change_type WHEN 1 THEN max_hours  WHEN 2 THEN -max_hours END) AS  max, SUM(CASE change_type WHEN 1 THEN min_hours WHEN 2 THEN -min_hours END) AS min,  shift as shift_id, support_coverage, responsibilities As responsibilities_id, change_type, shift.shift_code As shift ,anticipated_date AS due, support_coverage.support_type As support, role_types.role_type AS responsibilities ,process_id,wor.client AS client_id,project_type 
		FROM `hourly_model` 
		LEFT JOIN shift ON shift.shift_id = hourly_model.shift 
		LEFT JOIN support_coverage ON support_coverage.support_id = hourly_model.support_coverage 
		LEFT JOIN role_types ON role_types.role_type_id = hourly_model.responsibilities 
		LEFT JOIN wor ON wor.wor_id = hourly_model.fk_wor_id 
		LEFT JOIN cor ON cor.cor_id = hourly_model.fk_cor_id 
		LEFT JOIN project_types ON project_types.project_type_id = hourly_model.fk_project_type_id 
		WHERE fk_wor_id = '$wor_id' AND process_id !=0 AND anticipated_date <= CURDATE() 
		GROUP BY shift , support_coverage, responsibilities, project_type";
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		return $data;
	}
	
	function get_project_summary_list($wor_id)
	{
		$sql = "SELECT fk_wor_id AS projectId,DATE_FORMAT(start_duration, '%d-%b-%Y') AS min,DATE_FORMAT(end_duration, '%d-%b-%Y')  AS max,shift AS shift_id, support_coverage, responsibilities AS responsibilities_id, change_type, shift.shift_code AS shift , anticipated_date AS due,support_coverage.support_type AS support,role_types.role_type AS responsibilities, process_id,wor.client AS client_id,project_type 
		FROM  `project_model` 
		LEFT JOIN shift ON shift.shift_id = project_model.shift
		LEFT JOIN support_coverage ON support_coverage.support_id = project_model.support_coverage
		LEFT JOIN role_types ON role_types.role_type_id = project_model.responsibilities 
		LEFT JOIN wor ON wor.wor_id = project_model.fk_wor_id 
		LEFT JOIN cor ON cor.cor_id = project_model.fk_cor_id 
		LEFT JOIN project_types ON project_types.project_type_id = project_model.fk_project_type_id 
		WHERE fk_wor_id = '$wor_id' AND process_id !=0 AND anticipated_date <= CURDATE() ";
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		return $data;
	}

	function get_volume_summary_list($wor_id)
	{
		$sql = " SELECT fk_wor_id AS projectId,SUM(CASE change_type WHEN 1 THEN max_volume  WHEN 2 THEN -max_volume END) AS  max,SUM(CASE change_type WHEN 1 THEN min_volume WHEN 2 THEN -min_volume END) AS min,  
		shift AS shift_id, support_coverage, responsibilities AS responsibilities_id, change_type, shift.shift_code AS shift ,anticipated_date AS due, support_coverage.support_type AS support,
		role_types.role_type AS responsibilities,process_id,wor.client AS client_id,project_type 
		FROM `volume_based_model` 
		LEFT JOIN shift ON shift.shift_id = volume_based_model.shift
		LEFT JOIN support_coverage ON support_coverage.support_id = volume_based_model.support_coverage 
		LEFT JOIN role_types ON role_types.role_type_id = volume_based_model.responsibilities 
		LEFT JOIN wor ON wor.wor_id = volume_based_model.fk_wor_id
		LEFT JOIN cor ON cor.cor_id = volume_based_model.fk_cor_id 
		LEFT JOIN project_types ON project_types.project_type_id = volume_based_model.fk_project_type_id 
		WHERE fk_wor_id = '$wor_id' AND process_id !=0 AND anticipated_date <= CURDATE() GROUP BY volume_based_model.shift ,volume_based_model.support_coverage, volume_based_model.responsibilities";
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		return $data;
	}

	function get_fte_summary_list($wor_id)
	{
		$sql = "SELECT fk_wor_id As projectId,SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS fte,process_id, shift as shift_id ,support_coverage, responsibilities As responsibilities_id, change_type,shift.shift_code As shift, anticipated_date AS due, support_coverage.support_type  As support ,role_types.role_type AS responsibilities,wor.CLIENT AS client_id,project_type  
		FROM `fte_model` 
		LEFT JOIN shift ON shift.shift_id = fte_model.shift 
		LEFT JOIN role_types ON role_types.role_type_id = fte_model.responsibilities 
		LEFT JOIN support_coverage ON support_coverage.support_id = fte_model.support_coverage 
		LEFT JOIN wor ON wor.wor_id = fte_model.fk_wor_id 
		LEFT JOIN cor ON cor.cor_id = fte_model.fk_cor_id 
		LEFT JOIN project_types ON project_types.project_type_id = fte_model.fk_project_type_id 
		WHERE fk_wor_id = '$wor_id' AND anticipated_date <= CURDATE() GROUP BY shift , responsibilities";
//echo $sql; exit;
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		$data = $query->result_array();
		
		return $data;
	}
	
	/* Get Allocated Resources of work order line item */
	function get_allocated_resource($client_id, $project_id, $wor_id)
	{		
		$sort	= json_decode($this->input->get('sort'),true);	
		$options = array();
		
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		$sortBy = $sort[0]['property'];
		$sortDirection = $sort[0]['direction'];
		
		$sql = "SELECT id, CAST(e.employee_id AS UNSIGNED INTEGER) AS employee_id,e.first_name AS first_name,e.last_name AS last_name, e.Email, d.Name AS designationName,e.company_employ_id AS company_employ_id,CAST(SUBSTRING_INDEX(e.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID,
		d.grades,client_id,billable,billable_type, CONCAT(m.first_name, ' ', m.last_name) AS supervisor, process_id
		FROM (SELECT client_id,employee_id,billable,id,billable_type, process_id, wor_id   
		FROM employee_client) AS temp 
		JOIN employees e ON e.employee_id = temp.employee_id
		LEFT JOIN  employees m  ON e.primary_lead_id = m.employee_id
		JOIN designation d ON e.designation_id = d.designation_id 
		WHERE e.status != 6 AND temp.client_id = $client_id AND temp.process_id = $project_id AND temp.wor_id = $wor_id 
		GROUP BY e.employee_id";
		
		if ($sortBy == 'FullName') $sortBy = 'first_name';
		
		if($sortBy != 'model')
			$sql .= " ORDER BY ". $sortBy." ".$sortDirection;
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " limit ".$limit;
		}
		
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		return $results;	
	}

	/* Get Allocated Resources of work order line item */
	function get_allocated_res($client_id, $wor_id, $project_id)
	{		
		$sort	= json_decode($this->input->get('sort'),true);	
		$options = array();
		$searchsql = "";
		
		$sortBy = $sort[0]['property'];
		$sortDirection = $sort[0]['direction'];
		
		$searchTxt = $this->input->get('searchTxt') ? $this->input->get('searchTxt'):"";
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		else
		{
			$sortBy = 'first_name';
			$sortDirection = 'ASC';
		}
		

		// debug($_GET);exit;

		if($searchTxt <> "")
		{
			$searchsql = "AND (e.company_employ_id LIKE '%".$searchTxt."%' OR e.first_name LIKE '%".$searchTxt."%' OR e.last_name LIKE '%".$searchTxt."%' OR v.name LIKE '%".$searchTxt."%' OR d.name LIKE '%".$searchTxt."%' OR e.email LIKE '%".$searchTxt."%' OR m.first_name LIKE '%".$searchTxt."%')";
		}

		if($project_id !="")
		{
			$searchsql .=" AND temp.project_type_id = ".$project_id;
		}
		
		$sql = "SELECT temp.id, CAST(e.employee_id AS UNSIGNED INTEGER) AS employee_id,e.first_name AS first_name,e.last_name AS last_name, e.Email, d.Name AS designationName,e.company_employ_id AS company_employ_id,CAST(SUBSTRING_INDEX(e.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID,
		d.grades,client_id,billable,billable_type, CONCAT(m.first_name, ' ', m.last_name) AS supervisor, project_type_id, p.pod_name AS podName, se.name AS serviceName
		FROM (SELECT client_id,employee_id,billable,id,billable_type, project_type_id, wor_id   
		FROM employee_client) AS temp 
		JOIN employees e ON e.employee_id = temp.employee_id
		-- LEFT JOIN employee_vertical ev ON ev.employee_id = temp.employee_id
		LEFT JOIN  employees m  ON e.primary_lead_id = m.employee_id
		-- LEFT JOIN verticals v ON v.vertical_id = ev.vertical_id
		LEFT JOIN employee_pod ep ON ep.employee_id = temp.employee_id
		LEFT JOIN tbl_rhythm_services se ON se.service_id = e.service_id
		LEFT JOIN tbl_rhythm_pod p ON p.pod_id = ep.pod_id
		JOIN designation d ON e.designation_id = d.designation_id 
		WHERE e.status != 6 AND temp.client_id = $client_id AND temp.wor_id = $wor_id $searchsql 
		GROUP BY e.employee_id";
		
		if ($sortBy == 'FullName') $sortBy = 'first_name';
		
		if($sortBy != 'model')
			$sql .= " ORDER BY ". $sortBy." ".$sortDirection;
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " limit ".$limit;
		}
		
		$query = $this->db->query($sql);
		$data = $query->result_array();
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('data' => $data)
		);
		return $results;	
	}

	/*Get project ID for wor by wor_id*/
	function get_project_id($wor_id)
	{
		$sql = "SELECT  DISTINCT project_type_id FROM employee_client WHERE wor_id = '".$wor_id."'";                            
		$query = $this->db->query($sql);
		$data =  $query->result_array();
		$process_id_data = array();
		foreach ($data as $key => $value) {
			$process_id_data[] = $value['process_id'];
		}
		$data = implode(',',$process_id_data);
		return $data;
	}
	
	/* Get Allocated Resources of work order*/
	function get_wor_allocated_resource($client_id, $wor_id)
	{							
		$sql = "SELECT ec.employee_id from employee_client ec JOIN employees e ON ec.employee_id=e.employee_id WHERE ec.client_id = $client_id AND ec.wor_id = $wor_id AND e.status!=6";

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();						
		return $totCount;	
	}
	
	/* Remove resources from WOR line item*/
	function removeResources($input_values)
	{
		if(isset($input_values['resource_ids']))
		{
			$resourcesIds = json_decode($input_values['resource_ids']);
			$TransitionDate = ($input_values['TransitionDate']) ? $input_values['TransitionDate'] : '';

			foreach($resourcesIds as $empVal)
			{
				$sql_resources ="DELETE FROM employee_client WHERE client_id = ".$input_values['client_id']." AND employee_id = ".$empVal." AND wor_id = ".$input_values['wor_id']."";

				if($input_values['process_id'] !="")
				{
					$sql_resources .=" AND project_type_id = ".$input_values['process_id']."";
				}
				$this->db->query($sql_resources);			
				
				$primary_lead_id = $this->getPrimaryLeadID($empVal);					
				// Start Audit Log		
				$empData = $this->input->cookie();
				$modfdId 		= $empData['employee_id'];
				$empRelInfo 	= $this->empRelInfo($empVal);
				$fromVert 		= $empRelInfo[0]['verticalID'];
				$fromClient 	= $input_values['client_id'];
				$fromReporting 	= $primary_lead_id['primary_lead_id'];
				$toVert 		= 0;
				$toClient 		= $input_values['client_id'];
				$toReporting 	= $primary_lead_id['primary_lead_id'];
				$empTrnCnt 		= 0;
				$transDate 		= $TransitionDate;
				
				$this->Audit_Log_Model->employTransitionLog($empVal, $transDate, $modfdId, $fromVert, $fromClient, $fromReporting, $toVert,$toClient, $toReporting, 0, "Client Removed");
				
				$this->update_billabale_history_data($empVal, $fromClient, $input_values['process_id'],$transDate);

				$sql_time_appr = "UPDATE wor
				SET time_entry_approver = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', time_entry_approver, ','),',".$empVal.",',','))
				WHERE FIND_IN_SET('".$empVal."', time_entry_approver) AND wor_id = ".$input_values['wor_id']."";
				$this->db->query($sql_time_appr); 

				$empData    = $this->input->cookie();
				$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$input_values['wor_id'].') has been removed resource(Employee ID:'.$empVal.').';
				$this->userLogs($logmsg);
			}
			
			if($transDate!='')
			{
				$SQL = "UPDATE employee_client_history SET end_date = '".$transDate."',
				updated_by =  '".$empData['employee_id']."' , updated_date = '".date("Y-m-d H:i:s")."'
				WHERE end_date = null  AND employee_id = ".$empVal." AND client_id = ".$toClient." ";

				$this->db->query($SQL);
			}
			
			return  1;
		}
	}
	
	/* defined function to update history data of existing record */
	function update_billabale_history_data($empid, $clientid, $processid, $transDate='')
	{
		$billabality_update_data = array(									
			'end_date'		=> $transDate,
			'updated_by'	=> $empData['employee_id'],
		);

		$this->db->where('employee_id', $empid);
		$this->db->where('client_id', $clientid);
		$this->db->where('end_date IS NULL');
		if($processid!="")
		{
			$this->db->where('project_type_id', $processid);
		}
		$this->db->update('employee_billabality_history',$billabality_update_data);
	}

	
	/**
	 * Funtion to get primary lead of an employee based on employees id
	 * 
	 * @param int $empID
	 * @return string
	 */
	function getPrimaryLeadID($empID = -1) 
	{
		$sql = "SELECT primary_lead_id FROM employees WHERE employee_id = " . $empID;
		
		$query = $this->db->query ( $sql );
		
		if ($query->num_rows () > 0) 
		{
			return $query->row_array ();
		}
		
		return "";
	}
	
	/**
	 * Function to get employee information based on employee id
	 * 
	 * @param string $empID
	 * @param string $clientID
	 * @return string
	 */
	function empRelInfo($empID = "") 
	{
		$sql = "SELECT employee_client.client_id,ev.vertical_id AS verticalID
		FROM employees e
		LEFT JOIN employee_vertical ev ON ev.employee_id = e.employee_id
		LEFT JOIN employee_client ON employee_client.employee_id = e.employee_id 
		WHERE e.employee_id = " . $empID;

		$query = $this->db->query($sql);
		
		if ($query->num_rows () > 0) 
		{
			return $query->result_array ();
		}
		
		return "";
	}
	
	
	/* Deleting client holidays */
	function delClientHolidays($input_values)
	{
		if(isset($input_values['id']))
		{
			$deleteQry = "DELETE FROM client_holidays WHERE client_holiday_id = ".$input_values['id'];
			$this->db->query($deleteQry);
			
			return  1;
		}
	}
	
	
	/* Mapping client holidays */
	function clientHolidays($input_values)
	{
		$empData = $this->input->cookie();
		$added_by = $empData['employee_id'];
		
		if(isset($input_values['holiday_id']))
		{
			// $deleteQry = "DELETE FROM client_holidays WHERE client_id = ".$input_values['holiday_client_id']." AND process_id IN( ".$input_values['holidayProjectCode'].") AND wor_id = ".$input_values['holiday_wor_id']." AND holiday_id IN(SELECT holiday_id FROM holidays WHERE date>CURDATE() AND status=1)";
			// $this->db->query($deleteQry);
			
			$holiProjArr = explode(",", $input_values['holidayProjectCode']);
			$holidayId = explode(",", $input_values['holiday_id']);
			
			foreach($holiProjArr as $codeList)
			{
				foreach($holidayId as $idList)
				{
					$availQry = "SELECT holiday_id FROM client_holidays WHERE client_id = ".$input_values['holiday_client_id']." AND process_id = ".$codeList." AND wor_id = ".$input_values['holiday_wor_id']." AND holiday_id =".$idList;
					$availdata = $this->db->query($availQry);
					
					if($availdata->num_rows()==0)
					{
						$dataset = array(
							'holiday_id'	=> $idList,
							'wor_id'		=> $input_values['holiday_wor_id'],
							'client_id'		=> $input_values['holiday_client_id'],
							'process_id' 	=> $codeList,
							'added_by' 		=> $added_by,
							'added_on'		=> date("Y-m-d H:i:s") 
						);

						$retVal   = $this->insert($dataset, 'client_holidays');
					}
				}
			}

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$input_values['holiday_wor_id'].') has been updated work order client holidays.';
			$this->userLogs($logmsg);
			
			return  1;
		}
	}
	
	/**
     * Get holiday list of clients
     *
     * @param
     * @return array
     */
	
	function get_holidays_list()
	{
		$client_id = ($this->input->get('client_id')!="")?$this->input->get('client_id'):''; 
		$wor_id = ($this->input->get('wor_id')!="")?$this->input->get('wor_id'):''; 
		
		$sql = "SELECT client_holiday_id AS id,process.name AS project_code,holidays.name AS holiday,DATE_FORMAT(holidays.date, '%d-%b-%Y') AS date
		FROM client_holidays 
		LEFT JOIN holidays ON holidays.holiday_id = client_holidays.holiday_id 
		LEFT JOIN process ON process.process_id = client_holidays.process_id 
		WHERE client_holidays.client_id = '$client_id' AND wor_id='$wor_id' AND process.name != ''
		ORDER BY holidays.date ASC";

		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $totCount),
			array('rows' => $data)
		);
		
		return $results;
	}
	
	
	/**
	* Function for updating CP, EM, DM, AM, TL & SH
	*/
	function update_worEmpsHistory($data = '')
	{
		$empData = $this->input->cookie();
		$table_name = "";
		
		if($data['fieldname']=="cpLayout")
		{
			$wor_dataset['client_partner'] = $data['value'];
		}
		else if($data['fieldname']=="emLayout")
		{
			$wor_dataset['engagement_manager'] = $data['value'];
		}
		else if($data['fieldname']=="dmLayout")
		{
			$wor_dataset['delivery_manager'] = $data['value'];
		}
		else if($data['fieldname']=="amLayout")
		{
			$wor_dataset['associate_manager'] = $data['value'];
		}
		else if($data['fieldname']=="tlLayout")
		{
			$wor_dataset['team_lead'] = $data['value'];
		}
		else if($data['fieldname']=="shLayout")
		{
			$wor_dataset['stake_holder'] = $data['value'];
		}
		else if($data['fieldname']=="taLayout")
		{
			$wor_dataset['time_entry_approver'] = $data['value'];
		}
		$this->db->where('wor_id', $data['wor_id']);
		$retVal = $this->db->update('wor',$wor_dataset);
		
		if($data['fieldname']=="cpLayout" || $data['fieldname']=="emLayout" || $data['fieldname']=="dmLayout")
		{
			$history_dataset = array(
				'end_date'   => date("Y-m-d"),
				'updated_by' => $empData['employee_id'],
				'updated_on' => date("Y-m-d H:i:s"),
			);
			
			$this->db->where('wor_id', $data['wor_id']);
			if($data['fieldname']=="cpLayout")
			{
				$this->db->where_not_in('client_partner', $val);
				$table_name = "wor_cp_history";
			}
			else if($data['fieldname']=="emLayout")
			{
				$this->db->where_not_in('engagement_manager', $val);
				$table_name = "wor_em_history";
			}
			else if($data['fieldname']=="dmLayout")
			{
				$this->db->where_not_in('delivery_manager', $val);
				$table_name = "wor_dm_history";
			}
			
			$retVal = $this->db->update($table_name, $history_dataset);
			
			$empIdArr = explode(',',$data['value']);
			foreach($empIdArr as $val)
			{
				if($this->_isHistoryExist($data['wor_id'], $val, $data['fieldname'])<1)
				{
					$history_dataset = array(
						'wor_id'	 => $data['wor_id'],
						'start_date' => date("Y-m-d"),
						'created_by' => $empData['employee_id'],
					);
					if($data['fieldname']=="cpLayout")
					{
						$history_dataset['client_partner'] = $val;
						$table_name = "wor_cp_history";
					}
					else if($data['fieldname']=="emLayout")
					{
						$history_dataset['engagement_manager'] = $val;
						$table_name = "wor_em_history";
					}
					else if($data['fieldname']=="dmLayout")
					{
						$history_dataset['delivery_manager'] = $val;
						$table_name = "wor_dm_history";
					}
					
					$retVal = $this->db->insert($table_name, $history_dataset);
				}
			}
		}
		if($data['fieldname']=="cpLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Client partner.';
			$this->userLogs($logmsg);
		}
		if($data['fieldname']=="emLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Engagement Manager.';
			$this->userLogs($logmsg);
		}
		if($data['fieldname']=="dmLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Delivery Manager.';
			$this->userLogs($logmsg);
		}
		if($data['fieldname']=="amLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Associate Manager.';
			$this->userLogs($logmsg);
		}
		if($data['fieldname']=="tlLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Team Lead.';
			$this->userLogs($logmsg);
		}
		if($data['fieldname']=="shLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Stakeholder Manager.';
			$this->userLogs($logmsg);
		}
		if($data['fieldname']=="taLayout"){
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work order ID:'.$data['wor_id'].') has updated Work Order Time Entry Approver.';
			$this->userLogs($logmsg);
		}
		
		return $retVal;
	}
	
	private function _isHistoryExist($wor_id, $val, $fieldname)
	{
		$totCount = 0;
		$sql = "";
		if($fieldname=="cpLayout")
		{
			$sql = "SELECT * FROM wor_cp_history WHERE wor_id = ". $wor_id." AND client_partner = ".$val;
		}
		else if($fieldname=="emLayout")
		{
			$sql = "SELECT * FROM wor_em_history WHERE wor_id = ". $wor_id." AND engagement_manager = ".$val;
		}
		else if($fieldname=="dmLayout")
		{
			$sql = "SELECT * FROM wor_dm_history WHERE wor_id = ". $wor_id." AND delivery_manager = ".$val;
		}
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();

		return $totCount;
	}

	function getSupervisor()
	{
		$client_id = $this->input->get('comboClient');
		
		$sql = "SELECT DISTINCT L.employee_id AS supervisor_id, CONCAT(L.first_name,' ',L.last_name) AS supervisor_name
		FROM utilization U
		LEFT JOIN employees E on U.employee_id = E.employee_id
		LEFT JOIN employees L on E.primary_lead_id = L.employee_id
		WHERE U.fk_client_id='$client_id'";

		$start_date = ($this->input->get('start_date')?$this->input->get('start_date'):'');
		$end_date = ($this->input->get('end_date')?$this->input->get('end_date'):'');

		if($start_date != '' && $end_date != '')
			$sql .= " AND U.task_date BETWEEN '$start_date' AND '$end_date'";

		if($this->input->get('wor_id'))
		{
			$wor_id = $this->input->get('wor_id');
			$sql .= " AND U.fk_wor_id='$wor_id'";
		}

		if($this->input->get('wor_type_id'))
		{
			$wor_type_id = $this->input->get('wor_type_id');
			$sql .= " AND fk_type_id='$wor_type_id'";
		}


		$query = $this->db->query($sql);

		$result = $query->result_array();

		$results = array('totalCount' => count($result),
			'rows' => $result
		);
		
		return $results;
	}

	function getSupervisor_old()
	{
		$client_id = $this->input->get('comboClient');
		
		$temp_sql = "SELECT associate_manager,team_lead 
		FROM wor
		WHERE client='$client_id'";

		if($this->input->get('wor_id'))
		{
			$wor_id = $this->input->get('wor_id');
			$temp_sql .= " AND wor_id='$wor_id'";
		}

		if($this->input->get('wor_type_id'))
		{
			$wor_type_id = $this->input->get('wor_type_id');
			$temp_sql .= " AND wor_type_id='$wor_type_id'";
		}

		$temp_query = $this->db->query($temp_sql);

		$temp_result = $temp_query->result_array();

		$temp_array = array();
		foreach ($temp_result as $key => $value)
		{
			if(isset($value['associate_manager']))
			{
				$associate_manager = explode(',',$value['associate_manager']);
				$temp_array = array_merge($temp_array,$associate_manager);
			}

			if(isset($value['team_lead']))
			{
				$team_lead = explode(',',$value['team_lead']);
				$temp_array = array_merge($temp_array,$team_lead);
			}
		}

		$temp_array = array_unique($temp_array);
		
		$supervisor_data = array();
		foreach ($temp_array as $key => $value) {
			$sql = "SELECT employee_id AS supervisor_id, CONCAT(first_name, ' ', last_name) as supervisor_name
			FROM employees
			WHERE employee_id = '$value'";
			$query = $this->db->query($sql);

			$result = $query->result_array();
			array_push($supervisor_data,$result[0]);
		}
		
		$results = array('totalCount' => count($supervisor_data),
			'rows' => $supervisor_data
		);
		
		return $results;
	}

	function wor_project_roles()
	{
		error_reporting(E_ALL);
		$project_id = $this->input->get('project_id');
		$wor_id = $this->input->get('wor_id');

		$sql = "SELECT role_type AS role_type, role_type_id FROM role_types 
		WHERE role_type_id IN (SELECT responsibilities FROM fte_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")
		UNION
		SELECT responsibilities FROM hourly_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")
		UNION
		SELECT responsibilities FROM volume_based_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")
		UNION
		SELECT responsibilities FROM project_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")) AND status=1";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		$result['rows'] = $data;
		
		return $result;
	}

	function getModels()
	{
		// debug($_GET);exit;
		$wor_id = $this->input->get('wor_id');
		$default_types = ['Buffer'];

		$fk_project_type_id = $this->input->get('project_type_id');
		$where = "";
		if($fk_project_type_id!="")
		{
			$where = "AND fk_project_type_id='$fk_project_type_id'";
		}
		
		$sql = "SELECT
		(SELECT COUNT(fte_id) FROM fte_model WHERE fk_wor_id='$wor_id' $where) AS fte_count, 
		(SELECT COUNT(hourly_id) FROM hourly_model WHERE fk_wor_id='$wor_id' $where) AS hourly_count,
		(SELECT COUNT(project_id) FROM project_model WHERE fk_wor_id='$wor_id' $where) AS project_count,
		(SELECT COUNT(vb_id) FROM volume_based_model WHERE fk_wor_id='$wor_id' $where) AS volume_count";
		$result = $this->db->query($sql)->result_array();

		if($result[0]['fte_count']>0)
		{
			$default_types[] = 'FTE Contract';
			$default_types[] = 'Approved Buffer';
			$default_types[] = 'FTE';
		}
		if($result[0]['hourly_count']>0)
			$default_types[] = 'Hourly';
		if($result[0]['project_count']>0)
			$default_types[] = 'Project';
		if($result[0]['volume_count']>0)
			$default_types[] = 'Volume';

		$models = array();
		foreach ($default_types as $key => $value) {
			$models[] = array('id'=>$value,'name'=>$value);
		}
		
		$data['totalCount'] = count($models);
		$data['data'] = $models;
		return $data;
	}
}
?>