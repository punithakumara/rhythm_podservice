<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HierarchyCount_Model extends INET_Model{
	
	public $HR_POOL = 223;
	public $Onboarding = 221;
	public $count = 0; 
	
	function __construct() {
		parent::__construct();
	}
	
	/*
	 * @descritpion : check if employid exist in number_of_employee table
	 * @return : 'true' or 'false'
	 */
	private function _isEmpExist($empid)
	{
		$sql= "SELECT EmployID FROM number_of_employee WHERE EmployID = ".$empid;
		$result = $this->db->query($sql);
		$status = ($result->num_rows()==0) ? false : true;
		return $status;
	}
	
	function hierarchyPull($primary_Lead_array,$IncCounts)
	{
		$totalCount  = $IncCounts;
		foreach($primary_Lead_array as $empid)
		{
			if($this->_isEmpExist($empid))
			{
			    $sql = "UPDATE number_of_employee SET TotalCount = TotalCount + ".$totalCount." where EmployID = ".$empid;
			}
			else
			{
			   $sql = "INSERT INTO number_of_employee (EmployID,TotalCount) VALUES (".$empid.",".$totalCount.")";
			}
		    $result = $this->db->query($sql);
		}
	}

	
	function hierarchyPush($primary_Lead_array)
	{
		foreach($primary_Lead_array as $key => $val)
		{
			if($this->_isEmpExist($key))
			{
				if($val == 0)
				$sql = "UPDATE number_of_employee SET TotalCount = ".$val." where EmployID = ".$key;
				else
			    $sql = "UPDATE number_of_employee SET TotalCount = TotalCount - ".$val." where EmployID = ".$key;
			}
			else
			{
			   $sql = "INSERT INTO number_of_employee (EmployID,TotalCount) VALUES (".$key.",".$val.")";
			}
		    $result = $this->db->query($sql);
		}
	}
}