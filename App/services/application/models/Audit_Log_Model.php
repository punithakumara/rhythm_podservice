<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Audit_Log_Model extends INET_Model{
	
	public $HR_POOL = 223;
	public $Onboarding = 221;
	public $count = 0; 
	
	function __construct() {
		parent::__construct();
	}
	
	/*
	 * @descritpion : check if employid exist in number_of_employee table
	 * @return : 'true' or 'false'
	 */
	private function _isTransitionDataExist($empid)
	{
		$sql = "SELECT employId,transitionData FROM employ_transition WHERE employId = ".$empid;
		$result = $this->db->query($sql);
		
		if($result->num_rows() > 0) 
		{
			return $result->result_array();
		}
		
		return ""; 
	}
	
	
	function employTransitionLog($empId ="", $transDate="", $modfdId="", $fromVert="", $fromClient="", $fromReporting="", $toVert="", $toClient="", $toReporting="", $type, $transitionComment="")
	{
		$employ_transition = array(
			'employee_id'		=> $empId,
			'from_vertical_id'	=> $fromVert,
			'from_client_id'	=> $fromClient,
			'from_reporting'	=> $fromReporting,
			'to_vertical_id'	=> $toVert,
			'to_client_id'		=> $toClient,
			'to_reporting'		=> $toReporting,
			'transition_date'	=> $transDate,
			'modified_by'		=> $modfdId,
			'action'            => $type,
			'comment'           => $transitionComment,
		);

		$retVal = $this->insert($employ_transition,'employee_transition');
	}
	
	
	/**
	* @descritpion : check if employid exist in number_of_employee table
	* @return : 'true' or 'false'
	*/
	 
	private function _isBillabiltyDataExist($empid)
	{
		$sql= "SELECT employee_id,billability_data FROM employ_billability WHERE employee_id = ".$empid;
		$result = $this->db->query($sql);
		
		if($result->num_rows() > 0) {
			return $result->result_array();
		}
			
		return ""; 
	}
	
	function employBillabilityLog($empId ="",$clientID ="",$billableType="",$billablePer="",$startDate="",$endDate="",$modfdId="",$modfdDate="",$fromTrans,$shiftid="")
	{
		$stdt ="";$endt="";
		if($startDate != "")
		{
			$startDate = strtr($startDate, '/', '-');
			$stdt = date('d-m-Y',strtotime($startDate));
		}	
		
		if($endDate != "")
		{
			$endDate = strtr($endDate, '/', '-');
			$endt = date('d-m-Y',strtotime($endDate));
		}
		
		$employ_billabilty = array(
			'client_id'=> $clientID,
			'billability_type' => $billableType,
			'billability_percentage' => $billablePer,
			'shift_id' => $shiftid,
			'start_date' =>$stdt,
			'end_date' => $endt,
			'modified_by'=> $modfdId,
			'modified_date'=> $modfdDate
		);
		$ExtBillLogData = $this->_isBillabiltyDataExist($empId);
		$my_array = array();
		if($ExtBillLogData != "")
		{
			$data_attr = unserialize($ExtBillLogData[0]['billability_data']);	 
			
			if(is_array($data_attr))
			{
				if($endDate != "" )
				{
				  	foreach ($data_attr as $key => $val)
					{
						if($fromTrans == 1 && $data_attr[$key]['end_date'] == "" && $clientID=="")
						{
							$data_attr[$key]['modified_by'] = $modfdId;
							$data_attr[$key]['modified_date'] = $modfdDate;
							$data_attr[$key]['end_date'] = $endt;
						}
						else if($fromTrans == 1 && $data_attr[$key]['end_date'] == "" && $val['client_id'] == $clientID)
						{
							$data_attr[$key]['modified_by'] = $modfdId;
							$data_attr[$key]['modified_date'] = $modfdDate;
							$data_attr[$key]['end_date'] = $endt;
						}
						else if($fromTrans != 1 && $val['client_id'] == $clientID)
						{
							$data_attr[$key]['modified_by'] = $modfdId;
							$data_attr[$key]['modified_date'] = $modfdDate;
							$data_attr[$key]['end_date'] = $endt;
						}
					} 
				}
				else
				{
					if($endDate == "" )
						array_push($data_attr,$employ_billabilty);
				}
			}
			$data_employ = array('employee_id'=>$empId,'billability_data' =>serialize($data_attr));
			
			if($fromTrans == 1)
			{
				$retVal = $this->update($data_employ,array('employee_id'=>$empId), 'employ_billability');
			}
			else if($fromTrans != 1)
			{
				$retVal = $this->update($data_employ,array('employee_id'=>$empId), 'employ_billability');
			}		
		}
		else if($fromTrans != 1)
		{
			array_push($my_array,$employ_billabilty);
			
			$data_employ = array('employee_id'=>$empId,'billability_data' =>serialize($my_array));
			$retVal = $this->insert($data_employ,'employ_billability');
		}
	}
	
	function employBillabilityLogNew($empId ="",$clientID ="",$processID="",$billableType="",$billablePer="",$startDate="",$endDate="",$modfdId="",$modfdDate="",$fromTrans,$shiftid="")
	{
		$stdt ="";$endt="";
		if($startDate != "")
			$stdt = date('d-m-Y',strtotime($startDate));
	
		if($endDate != "")
			$endt = date('d-m-Y',strtotime($endDate));
	
		$employ_billabilty = array(
				'client_id'=> $clientID,
				'process_id'=>$processID,
				'billability_type' => $billableType,
				'billability_percentage' => $billablePer,
				'shift_id' => $shiftid,
				'start_date' =>$stdt,
				'end_date' => $endt,
				'modified_by'=> $modfdId,
				'modified_date'=> $modfdDate
		);
			
	
		$ExtBillLogData = $this->_isBillabiltyDataExist($empId);
	
		$my_array = array();
		if($ExtBillLogData != ""){
				
			$data_attr = unserialize($ExtBillLogData[0]['billability_data']);
			if(is_array($data_attr))
			{
	
				if($endDate != ""){
					//$my_array = end($data_attr);
					foreach ($data_attr as $key => $val)
					{
						if($fromTrans == 1 && $data_attr[$key]['end_date'] == ""){
							$data_attr[$key]['modified_by'] = $modfdId;
							$data_attr[$key]['modified_date'] = $modfdDate;
							$data_attr[$key]['end_date'] = $endt;
						}else if($fromTrans != 1 && $val['process_id'] == $processID){
							$data_attr[$key]['modified_by'] = $modfdId;
							$data_attr[$key]['modified_date'] = $modfdDate;
							$data_attr[$key]['end_date'] = $endt;
						}
					}
						
						
					//array_pop($data_attr);
						
					//array_push($data_attr,$my_array);
						
				}else{
					array_push($data_attr,$employ_billabilty);
				}
			}
				
			$data_employ = array('employId'=>$empId,'billability_data' =>serialize($data_attr));
				
			if($fromTrans == 1 && $data_attr[$key]['end_date'] != ""){
				$retVal = $this->update($data_employ,array('employId'=>$empId), 'employ_billability');
			}else if($fromTrans != 1){
				$retVal = $this->update($data_employ,array('employId'=>$empId), 'employ_billability');
			}
		}else if($fromTrans != 1){
			array_push($my_array,$employ_billabilty);
				
			// print_r($my_array);die;
				
			$data_employ = array('employId'=>$empId,'billability_data' =>serialize($my_array));
			$retVal = $this->insert($data_employ,'employ_billability');
		}
	}
	
	function employShiftLog($employId ="",$processId="",$clientId="",$verticalId="",$fromDate="",$toDate="",$shiftId="")
	{
		$sql= "SELECT * FROM emp_shift WHERE employId = '".$employId."' AND processId='".$processId."' AND shiftId='".$shiftId."' and ToDate ='0000-00-00'";
		$result = $this->db->query($sql);
		//$fromDate=date('Y-m-d',strtotime(str_replace('/', '-', $fromDate)));
		//$toDate=date('Y-m-d',strtotime(str_replace('/', '-', $toDate)));
		//echo $toDate;//str_replace('/', '-', $fromDate)
		
		if($result->num_rows() > 0){
			if($toDate != '0000-00-00'){
				$data_employ = array('ToDate'=>$toDate,'shiftId'=>$shiftId);
				//$retVal = $this->update($data_employ,array('employId'=>$employId,'processId'=>$processId,'ToDate'=>'0000-00-00'), 'emp_shift');
			}	
		}else{
			$data_employ = array('ToDate'=>date('Y-m-d'));
			$retVals = $this->update($data_employ,array('employId'=>$employId,'ToDate'=>'0000-00-00'), 'emp_shift');

			$shiftLog = array(
				'EmployID'=> $employId,
				'ProcessID'=> $processId,
				'ClientID'=> $clientId,
				'VerticalID'=>$verticalId,
				'FromDate'=>$fromDate,
				'ToDate'=> $toDate,
				'ShiftID'=> $shiftId
			);
				
			$retVal = $this->insert($shiftLog,'emp_shift');		
			
		}
	}	
}