<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pod_Model extends INET_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Rhythm_Model');
    }
    
    function build_query()
    {
		$empData = $this->input->cookie();
		$grade  = $empData['Grade'];
		$VertIDs = $empData['PodId'];
		
        $filter             = json_decode($this->input->get('filter'), true);
        $filterName         = $this->input->get('filterName') ? $this->input->get('filterName') : "";
        $fromClientCont     = $this->input->get('fromClientCont') ? $this->input->get('fromClientCont') : "";
        $comboPod      = $this->input->get('comboPod') ? $this->input->get('comboPod') : '';
        $podStatusCombo  = $this->input->get('podStatusCombo');
        $ComboNoPodID  = $this->input->get('ComboNoPodID') ? $this->input->get('ComboNoPodID') : '';
        $fromdashboard      = $this->input->get('dashboard') ? $this->input->get('dashboard') : '';
        $filterQuery        = $this->input->get('query');
        $options['filter']  = $filter;
        $ppod = $this->input->get('ppod') ? $this->input->get('ppod') : '';
        isset($podStatusCombo) ? $podStatusCombo : '';
        $PodFromParent = '';
        $sql = "SELECT temp.pod_id AS pod_id ,temp.parent_pod_id ,temp.pod_name,temp.pod_name AS name,temp.status, GROUP_CONCAT(temp.employee_id) AS MG,GROUP_CONCAT(CONCAT(IFNULL(emp.first_name, ''),' ',IFNULL(emp.last_name, ''))) AS MGName,GROUP_CONCAT(emp.company_employ_id) AS ComEmpIDS,temp.description,temp.folder,sft.shift_id,sft.shift_code AS ShiftCode FROM(SELECT tbl_rhythm_pod.*, podmanager.pod_id AS podID, podmanager.employee_id,podmanager.shift_id FROM `tbl_rhythm_pod` LEFT JOIN `podmanager` ON tbl_rhythm_pod.pod_id=podmanager.pod_id ) AS temp LEFT JOIN shift sft ON sft.shift_id = temp.shift_id LEFT JOIN employees emp ON temp.employee_id = emp.employee_id  where 1=1 ";

        $filterWhere = '';
        if (isset($options['filter']) && $options['filter'] != '') {
            foreach ($options['filter'] as $filterArray) {
                $filterFieldExp = explode(',', $filterArray['property']);
                foreach ($filterFieldExp as $filterField) {
                    if ($filterField != '') {
                        $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                        $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                    }
                }
            }
            
            
            if ($filterWhere != '') {
                $sql .= " and (" . $filterWhere . ")";
            }
        }

        if($podStatusCombo !="" && $empData['is_onshore']!=1)
        {
            $sql .= " and temp.`status` = ".$podStatusCombo;
        }
        
        if ($filterName != "") {
            if($filterName == 'PodName') {
                $filterName = 'temp.pod_name';
            }
            $filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
            $sql .= " and " . $filterWhere;
        }
        
        $empData     = $this->input->cookie();
        $this->EmpId = $empData['employee_id'];
        
        if ($grade < 5 && $PodIDs!=10 && $empData['is_onshore']!=1) 
		{
            $sql .= " AND temp.pod_id IN(" . $PodIDs . ")";
        }
		if($empData['is_onshore']==1)
		{
			 $sql .= " AND temp.pod_id IN(28,29)";
		}
		
		if ($ComboNoPodID != "") 
		{
            $sql .= " AND temp.pod_id NOT IN(" . $ComboNoPodID . ")";
        }
		
        if($ppod != '')
		{
        	$this->db->select("Group_concat(DISTINCT tbl_rhythm_pod.pod_id) as PodIDs ", FALSE)->from("tbl_rhythm_pod")
	         ->where('tbl_rhythm_pod.parent_pod_id =', $ppod);
	        /*if($loggedInVerticals != ''){
	        	$this->db->where_in('verticals.vertical_id ',$loggedInVerticals);
	        }*/	        
	        $this->db->order_by('tbl_rhythm_pod.pod_name');	        
	        $query = $this->db->get();
	        if ($query->num_rows() >0)
	        { 
	        	$PodFromParent = $query->row()->PodIDs;	        	
	        	$sql .=  " AND temp.pod_id IN(" . $PodFromParent . ")";
	        }
        }
        $sql .= " GROUP BY temp.pod_id";       
        // echo $sql; exit;
        return $sql;
    }
    
    function view()
    {
        $options = array();
        
        if ($this->input->get('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        
        $sort = json_decode($this->input->get('sort'), true);
        
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        $sql      = $this->build_query();	
        $totQuery = $this->db->query($sql);
        $totCount = $totQuery->num_rows();
        
        if (isset($options['sortBy'])) {
            $sql .= " order by " . $options['sortBy'] . " " . $options['sortDirection'];
        }
        
        if (isset($options['limit']) && isset($options['offset'])) {
            $sql .= " limit " . $options['offset'] . " , " . $options['limit'];
        } else if (isset($options['limit'])) {
            $sql .= " limit " . $options['limit'];
        }
		//echo $sql; exit;
        $query = $this->db->query($sql);
        $data = $query->result_array();
        
        foreach ($data as $key => $entry) {
            $temp             = explode(",", $data[$key]['MG']);
            $data[$key]['MG'] = $temp;
        }
        
        $results = array_merge(array(
            'totalCount' => $totCount
        ), array(
            'rows' => $data
        ));
        return $results;
    }
    
    
    private function _isRecordExists($id = "", $name)
    {
        
        $query = $this->db->select("pod_id")->where("pod_name", trim(strtolower($name)));
        if ($id != "") {
            $this->db->where("pod_id  != ", trim($id));
        }
        $query = $this->db->get('tbl_rhythm_pod');
        return $query->num_rows();
    }
    
    function updatePod($idVal = '', $data = '')
    {
        $setData = json_decode(trim($data), true);
		
        if ($this->_isRecordExists($idVal, $setData['pod_name']) == 0) {
            $empData = $this->input->cookie();
            $where               = array();
            $set                 = array();
            $setData['updated_by'] = $empData['employee_id'];
            $setData['updated_on'] = date('Y-m-d H:i:s');
            $shift = $this->shift($setData['MG']);
            $setData['shift_id'] = $shift[0]['shift_id']; 
            $where['pod_id'] = $idVal;
            $assRetVal           = $this->updatePodManager($idVal, $setData);
            $tempArr             = $setData;
            unset($tempArr['MG']);
            unset($tempArr['MGName']);
            unset($tempArr['shift_code']);
            unset($tempArr['shift_id']);
           
			$retVal = $this->update($setData, $where, 'tbl_rhythm_pod');

            // $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Pod ID: '.$idVal.') has been updated POD.';
            $this->userLogs($logmsg);
			
			/* The Below Code added to change the Vertical name in the Utilization Table Once record is being Updated */
		//	$vertical_name = array('vertical_name' => $setData['name'] );
			
			//$change_util_verticalname  = $this->update($vertical_name, $where, 'utilization');
			
            if (($assRetVal == '1') || ($retVal == '1')) {
                return '1';
            } else if (($assRetVal != '1') && ($retVal != '1')) {
                return '0';
            }
        } else {
            return "-1";
        }
    }
    
    function getPodId($employID, $AssignedPod)
    {
        
        if ($AssignedPod != "" && $AssignedPod != 0) 
		{
        	if(!is_array($AssignedPod))
			{
            	$sql = "SELECT ver.pod_id,ver.Pod_Name,pro.process_id 
				FROM PROCESS pro, tbl_rhythm_pod ver WHERE ver.pod_id = " . $AssignedPod;
            }
			else
            {
            	$AssignedPods = implode(",",$AssignedPod);
                if(count($AssignedPod) == 1)
				{
					$sql = "SELECT ver.pod_id,ver.Pod_Name,pro.process_id 
					FROM PROCESS pro, tbl_rhythm_pod ver WHERE ver.pod_id = " . $AssignedPods;
                }
                else
				{
					$sql = "SELECT ver.pod_id,ver.Pod_Name,pro.process_id,
					(SELECT GROUP_CONCAT(DISTINCT mver.Pod_Name ORDER BY FIELD(mver.pod_id, " . $AssignedPods. ")) FROM tbl_rhythm_pod mver WHERE mver.pod_id IN ( " . $AssignedPods. ") ) AS MultiPodName 
					FROM PROCESS pro, tbl_rhythm_pod ver
					WHERE ver.pod_id IN ( " . $AssignedPods. ") ORDER BY FIELD(ver.pod_id, " . $AssignedPods. ") ";
				}
            }
           
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            if ($query->num_rows() > 0) {
                $processID    = $result[0]['ProcessID'];
                $podID   = $result[0]['PodID'];
                $podName = $result[0]['Name'];
                if(is_array($AssignedPod) && count($AssignedPod) > 1)
				{
                    $podName = $result[0]['MultiPodName'];
                }
                else
				{
              	 	$podName = $result[0]['Name'];
                }
                
                foreach ($result as $key => $val) 
				{
                    if ($val['ProcessID'] == HR_PROCESS_ID && $val['PodID'] == HR_POD_ID) 
					{
                        $processID    = $val['ProcessID'];
                        $verticalID   = $val['PodID'];
                        $verticalName = $val['Name'];
                    }
                }
            }
            return array(
                "PodID" => $podID,
                "ProcessID" => $processID,
                "Name" => $podName
            );
        } else {
            return '';
        }
    }
    
    
    function deletePod($idVal = '')
    {
        $options = array();
        
        $options['pod_id'] = $idVal;
        $retVal                = $this->delete($options, 'tbl_rhythm_pod');
        return $retVal;
    }
    
    function deletePodManager($idVal = '')
    {
        $options = array();
        
        $options['pod_id'] = $idVal;
        $retVal = $this->delete($options, 'podmanager');
        return $retVal;
    }
    
    
    function updatePodManager($idVal = '', $data = '')
    {
        
        $arrayEntities = explode(",", $data['MG'][0]);
        
        $retDelVal = $this->deletePodManager($idVal);
        
        $retVal = $this->addPodManager($idVal, $data['MG'], $data['shift_id']);
        
        return $retVal;
    }
    
    function addPodManager($idVal = '', $data = '', $shift = '')
    {
        
        $array_data = array();
        $empData    = $this->input->cookie();
        
        array_push($array_data, array(
            'pod_id' => $idVal,
            'employee_id' => $data,
            'shift_id' => $shift,
            'created_by' => $empData['employee_id']
        ));
        
        $retVal = $this->db->insert_batch('podmanager', $array_data);
        
        return $retVal;
    }

    function shift($employee_id)
    {
        $sql = "SELECT shift_id FROM emp_shift WHERE employee_id=".$employee_id;
        $query = $this->db->query($sql);
        $data = $query->result_array();

        return $data;
    }
    
    
    function addPod($data = '')
    {
        $addPod = json_decode(trim($data), true);
		
        // cheking if verticals value already exist or not
        if ($this->_isRecordExists("", $addPod['pod_name']) == 0) {
            $empData = $this->input->cookie();
        	$addPod['short_name'] = substr($addPod['pod_name'],0,9);
            $addPod['created_by'] = $empData['employee_id'];
        	
        	$retVal = $this->insert($addPod, 'tbl_rhythm_pod');       	
        	$addPod['pod_id'] = $this->getInsertId();
            $shift = $this->shift($addPod['MG']);
            $addPod['shift_id'] = $shift[0]['shift_id'];        	
        	$assRetVal = $this->addPodManager($addPod['pod_id'], $addPod['MG'], $addPod['shift_id']);
        	
            if ($assRetVal) {
                $empData    = $this->input->cookie();
                $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(POD ID: '.$addPod['pod_id'].') has been added POD.';
                $this->userLogs($logmsg);
                return $assRetVal;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
        
    }
    function getpodHead($podId)
	{
    	$this->db->select("CONCAT_WS(' ',employees.first_name,employees.last_name) AS FirstName,employees.employee_id",false)->from('employees')->join('podmanager', 'podmanager.Employee_id = employees.employee_id')->where('podmanager.Pod_id =', $podId);
		
    	$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			    $row = $query->row(); 
			    return $row->FirstName."==".$row->employee_id;
			}
		
	    return '';
    }
    
	function getParentPod(){
		$sql = "SELECT c.parent_vertical_name AS `groupBy` ,c2.parent_vertical_name AS name, c2.parent_verticalID AS id
				FROM parent_vertical c
				JOIN parent_vertical c2 ON c.parent_verticalID = c2.super_parent_verticalID";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$data = array_merge(array(
            'totalCount' => $query->num_rows()
        ), array(
            'rows' => $data
        ));
		return $data;
	}
// Group verticals Combobox
	function GroupPodComboValues()
	{
		$query  = "SELECT pod_id AS PodID,tbl_rhythm_pod.Pod_Name AS name FROM tbl_rhythm_pod  WHERE parent_pod_id = 0";
		$query  = $this->db->query($query);
		$data = $query->result_array();
		$i=0;
		$temp_array=array();
		foreach($data as $key => $value){
			//$data[$key]['VerticalName'] = $value['Name'];
			$temp_array[$i]['PodID']=$value['PodID'];
			$temp_array[$i]['Name']		 =$value['Name'];
			$i++;
			$FirstLevelquery  = "SELECT pod_id AS PodID,tbl_rhythm_pod.Pod_Name AS Name FROM tbl_rhythm_pod  WHERE parent_pod_id = ".$value['PodID'];
			$FirstLevelresult  = $this->db->query($FirstLevelquery);
			$FirstLeveldata = $FirstLevelresult->result_array();
				foreach($FirstLeveldata as $FirstLevelkey => $FirstLevelvalue){
					$temp_array[$i]['PodID']=$FirstLevelvalue['PodID'];
					$temp_array[$i]['Name']		 ="	&emsp;&#9830; ".$FirstLevelvalue['Name'];
					$i++;
					$secondLevelquery  = "SELECT pod_id AS PodID,tbl_rhythm_pod.Pod_Name AS Name FROM tbl_rhythm_pod WHERE parent_pod_id = ".$FirstLevelvalue['PodID'];
					$secondLevelresult  = $this->db->query($secondLevelquery);
					$secondLeveldata = $secondLevelresult->result_array();
					foreach ($secondLeveldata as $secondLevelkey => $secondLevelvalue){
						$temp_array[$i]['PodID']=$secondLevelvalue['PodID'];
						$temp_array[$i]['Name']		 ="&emsp;"."	&#8594; ".$secondLevelvalue['Name'];
						$i++;	
					}
				}
			}
		//print_r($temp_array);exit;
		return $temp_array;
	}	

	
	// Group verticals Combobox
	function RevisedGroupPodComboValues()
	{
		$empData = $$this->input->cookie();
		$loggedInUserId = $empData['employee_id'];
		$loggedInUserGrade = $empData['Grade'];
		if($loggedInUserGrade >= 6)
		{
			$query  = "SELECT Root.pod_id AS rootid,Department.pod_id AS deptid, PodName.pod_id as podid,  Root.Pod_Name  AS Root, Department.Pod_Name AS Department, PodName.Pod_Name AS PodName
			FROM tbl_rhythm_pod AS Root
			LEFT OUTER JOIN tbl_rhythm_pod AS Department ON Department.parent_pod_id = root.pod_id
			LEFT OUTER JOIN tbl_rhythm_pod AS PodName ON PodName.parent_pod_id = Department.pod_id
			WHERE Root.parent_pod_id = 0 AND PodName.pod_id IN(SELECT DISTINCT contact_vertical AS vertical_id FROM client_contacts)
			ORDER BY Root, Department, PodName";
		}
		else
		{
			$loggedInUserPod  = $empData['PodId'];
			
			$query  = "SELECT Root.pod_id AS rootid,Department.pod_id AS deptid, PodName.pod_id as podid,  Root.Pod_Name  AS Root, Department.Pod_Name AS Department, PodName.Pod_Name AS PodName
			FROM tbl_rhythm_pod AS Root
			LEFT OUTER JOIN tbl_rhythm_pod AS Department ON Department.parent_pod_id = root.pod_id
			LEFT OUTER JOIN tbl_rhythm_pod AS PodName ON PodName.parent_pod_id = Department.pod_id
			WHERE PodName.pod_id in (".$loggedInUserPod.")
			ORDER BY FIELD(VerticalName.vertical_id, " . $loggedInUserPod. "), Root, Department, VerticalName";		
		}
		// echo $query; exit;
		$query  = $this->db->query($query);
		$data = $query->result_array();
		$i=0;
		$temp_array=array();
		$tempRootID=0;
		$tempDeptID=0;
		$tempPodID=0;
		
		foreach($data as $key => $value)
		{
			if($loggedInUserGrade >= 6)
			{
				if($data[$key]['rootid'] != $tempRootID)
				{
					$tempRootID=$value['rootid'];
					$temp_array[$i]['pod_id']=$value['rootid'];
					$temp_array[$i]['Pod_Name']		 =$value['Root'];
					$i++;
				}			
				if($data[$key]['deptid'] != $tempDeptID)
				{
					$tempDeptID=$value['deptid'];
					$temp_array[$i]['pod_id']=$value['deptid'];
					$temp_array[$i]['Pod_Name']		 ="	&emsp;&#9830; ".$value['Department'];
					$i++;
				}
			}
			if($data[$key]['podid'] != $tempVerticalID)
			{
				$tempVerticalID=$value['podid'];
				$temp_array[$i]['pod_id']=$value['podid'];
				$temp_array[$i]['Pod_Name']		 ="&emsp;"."	&#8594; ".$value['PodName'];
				$i++;
			}
			
		}
		//print_r($temp_array);exit;
		return $temp_array;
	}	
	
		
	/* Get Top Level verticals */
	private function __getTopLevelPods()
	{
		$query  = "SELECT `pv`.`pod_id` AS PodID,`pv`.`Pod_Name` AS Name FROM `tbl_rhythm_pod` pv  WHERE pv.`parent_pod_id` = 0 GROUP BY pv.`pod_id` ORDER BY pv.parent_pod_id DESC LIMIT 1";
		$query  = $this->db->query($query);
		$result = $query->result_array();
		return $result;
	}
	    
    
	/*
     * @description : getting verticals name  detaild for Tree View
     */
    public function getVerticalsNames($verticalId,$loggedInVerticals = '')
    {
        $this->db->select("verticals.vertical_id,verticals.Name ", FALSE)->from("verticals")
				->join('client_vertical', 'client_vertical.vertical_id = verticals.vertical_id')
				->where('verticals.parent_vertical_id =', $verticalId);
        if($loggedInVerticals != ''){
        	$this->db->where_in('verticals.vertical_id ',$loggedInVerticals);
        }
        $this->db->group_by('verticals.vertical_id');
        $this->db->order_by('verticals.Name');
        $query = $this->db->get();
        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array();
    }
/*
     * @description : getttin verticals based on root verticals
     * 
     */
    public function getsubverticalsNames($verticalId)
    {
        
       	$this->db->select("tbl_rhythm_pod.pod_id as PodID,tbl_rhythm_pod.Pod_Name as Name ", FALSE)
       	->from("tbl_rhythm_pod")
       	->where('parent_pod_id =', $podId)
       	->order_by('Pod_Name');
		//echo $this->db->last_query();
       	$query = $this->db->get();
        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array();
        
    }
    
	
	public function repoteesverticalHierarchyGrid($grade = '')
    {
        $data = array();
        $data = $this->__getTopLevelVerticals();
        // $hrpool = $this->Rhythm_Model->getHRPoolNonBillableEmployees();
       // print_r($data);exit;
        foreach ($data as $key => $value) 
		{
			$sql = $this->Rhythm_Model->getBillableCountQuery();       
			// echo "<pre>"; print_r($sql); exit;
			$query = $this->db->query($sql);
			$billabledata = count($query->result_array());
			
			$sqlnonbill = $this->Rhythm_Model->getNonBillableCountQuery(); 
            // echo "<pre>"; print_r($sqlnonbill); exit;      
			$querynonbill = $this->db->query($sqlnonbill);
			$nonbillabledata = count($querynonbill->result_array());
			
			//$HRpoolData = $this->Rhythm_Model->getProcessEmpNames(INI_PROCESS_ID);				
			$data[$key]['VerticalName'] = $value['Name'];
			$data[$key]['ParentVerticalID'] = $value['VerticalID'];
			$data[$key]['Billable'] = $billabledata;
			$data[$key]['NonBillable'] = $nonbillabledata;
			$data[$key]['Leadership'] = $this->Rhythm_Model->getLeadverticalcount($Verticals = '',$processIds = '',$subVertical = '') ;
			//$data[$key]['HRPool'] = count($HRpoolData);
			//$data[$key]['Onboarding'] = count($this->Rhythm_Model->getProcessEmpNames(ONBOARD_PROCESS_ID)); //displaying onboarding count in verticals heirarchy					
        }
        return $data;
    }
	
    public function getsubTreeTruture($vid){
    	$data   = array_shift($this->getsubverticalsNames($vid));
    	foreach ($data as $key => $value) {
         			$data[$key]['VerticalName'] = $value['Name'];
		            $data[$key]['VerticalID'] = $value['VerticalID'];

        }
        return $data;
    }

	
    public function getPodsForParentID($pVid) 
	{
		if(!is_numeric($pVid) && !strpos($pVid,',')) 
		{
            $pVid   = $this->getPodIdFrmName($pVid);
        }          
        $this->db->select("GROUP_CONCAT(DISTINCT tbl_rhythm_pod.pod_id) as PodIDs ", FALSE);
		$this->db->from("tbl_rhythm_pod");
		// $this->db->join('process', 'process.vertical_id = verticals.vertical_id');
		$this->db->where("tbl_rhythm_pod.parent_pod_id IN (".$pVid.") OR tbl_rhythm_pod.pod_id IN (".$pVid.")" );
        $this->db->order_by('tbl_rhythm_pod.pod_name');
        $query = $this->db->get();
		
        if ($query->num_rows() >0) 
		{
            return $PodFromParent = $query->row()->PodIDs;
        }
        return '';
    }

    public function getPodIdFrmName($podName) {
        $res = "";
        if($podName) {
            $sql_podID         = "SELECT pod_id, pod_name FROM tbl_rhythm_pod WHERE pod_name = '".$podName."'";
            $sql_podID_query   = $this->db->query($sql_podID);
            $sql_pod_data      = $sql_podID_query->result_array();
            if(!empty($sql_pod_data)) {
                $res    = array_shift($sql_pod_data);
                return $res['pod_id'];
            } else {
                return $res;
            }
            
        }
       
    }
      
    // get all verticals irrespective of login
    function AllGroupVerticalComboValues()
    {    
    		$query  = "SELECT Root.vertical_id AS rootid,Department.vertical_id AS deptid, VerticalName.vertical_id as verticalid,  Root.name  AS Root, Department.name AS Department, VerticalName.name AS VerticalName
						     FROM verticals AS Root
						     LEFT OUTER JOIN verticals AS Department ON Department.parent_vertical_id = root.vertical_id
						     LEFT OUTER JOIN verticals AS VerticalName ON VerticalName.parent_vertical_id = Department.vertical_id
						     JOIN PROCESS ON process.vertical_id = VerticalName.vertical_id
						     WHERE Root.parent_vertical_id = 0 AND VerticalName.vertical_id IN(SELECT DISTINCT PROCESS.vertical_id FROM PROCESS)
						     ORDER BY Root, Department, VerticalName";
    			   	
    	//echo $query;
    	$query  = $this->db->query($query);
    	$data = $query->result_array();
    	$i=0;
    	$temp_array=array();
    	$tempRootID=0;
    	$tempDeptID=0;
    	$tempVerticalID=0;
    
    	foreach($data as $key => $value){
    		if(1){
    			if($data[$key]['rootid'] != $tempRootID){
    				$tempRootID=$value['rootid'];
    				$temp_array[$i]['VerticalID']=$value['rootid'];
    				$temp_array[$i]['Name']		 =$value['Root'];
    				$i++;
    			}
    			if($data[$key]['deptid'] != $tempDeptID){
    				$tempDeptID=$value['deptid'];
    				$temp_array[$i]['VerticalID']=$value['deptid'];
    				$temp_array[$i]['Name']		 ="	&emsp;&#9830; ".$value['Department'];
    				$i++;
    			}
    		}
    		if($data[$key]['verticalid'] != $tempVerticalID){
    			$tempVerticalID=$value['verticalid'];
    			$temp_array[$i]['VerticalID']=$value['verticalid'];
    			$temp_array[$i]['Name']		 ="&emsp;"."	&#8594; ".$value['VerticalName'];
    			$i++;
    		}
    			
    	}
    	//print_r($temp_array);exit;
    	return $temp_array;
    }
    
    
    
}
?>