<?php
class Rhythm_Model extends INET_Model
{
    public $EmpId = NULL;
    public $super_admin = array();    

    public function __construct()
    {
        parent::__construct();
        $resultArray = array();
        $empData     = $this->input->cookie();
        $this->config->load('admin_config');
        $this->super_admin = $this->config->item('super_admin');
        $this->load->model(array('Clients_Model','Clientwor_Model'));

        if (isset($empData['employee_id'])) 
        {
            $this->EmpId = $empData['employee_id'];
        }
    }

/*
* @description :Dashboard Billable graph - getting count of Billable employee for particular lead
* @return : Billable and count
*/
public function getBillablityCount($PodId = "")
{
    $empData = $this->input->cookie();
    $grade   = $empData['Grade'];

    $qwe = 0;
    $result = array();
    $sql_fte = $this->getBillableCount($PodId,'FTE');
    $query_fte = $this->db->query($sql_fte);

    if($query_fte->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Billable FTE';
        $result[$qwe]['Value1'] = $query_fte->num_rows();
        $qwe++;
    }

    $sql_hour = $this->getBillableCount($PodId,'Hourly');
    $query_hour = $this->db->query($sql_hour);

    if($query_hour->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Billable Hourly';
        $result[$qwe]['Value1'] = $query_hour->num_rows();
        $qwe++;
    }

    $sql_vol = $this->getBillableCount($PodId,'Volume');
    $query_vol = $this->db->query($sql_vol);

    if($query_vol->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Billable Volume';
        $result[$qwe]['Value1'] = $query_vol->num_rows();
        $qwe++;
    }

    $sql_proj = $this->getBillableCount($PodId,'Project');
    $query_proj = $this->db->query($sql_proj);

    if($query_proj->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Billable Project';
        $result[$qwe]['Value1'] = $query_proj->num_rows();
        $qwe++;
    }


    /* non India billable HC */
    $sql_nonhc   = $this->getBillableNonIndianCount($PodId);
    $query_nonhc = $this->db->query($sql_nonhc);

    if($query_nonhc->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Non-India Billable HC';
        $result[$qwe]['Value1'] = $query_nonhc->num_rows();
        $qwe++;
    }


    /* non billable */
    $sql   = $this->getNonBillableCount($PodId);
    $query = $this->db->query($sql, false);

    if($query->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Non Billable';
        $result[$qwe]['Value1'] = $query->num_rows();
        $qwe++;
    }

    $sql   = $this->__leaderShip($PodId,'Billable Leadership');            
    $query = $this->db->query($sql, false);

    if($query->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Billable Leadership';
        $result[$qwe]['Value1'] = $query->num_rows();
        $qwe++;
    }

    $sql   = $this->__leaderShip($PodId, 'Non-Billable Leadership');            
    $query = $this->db->query($sql, false);

    if($query->num_rows()>0)
    {
        $result[$qwe]['Name']   = 'Non-Billable Leadership';
        $result[$qwe]['Value1'] = $query->num_rows();
        $qwe++;
    }	

    $unassigned  = $this->getNotAssignEmpNames();

    $result[$qwe]['Name']   = 'Unassigned';
    $result[$qwe]['Value1'] = count($unassigned);
    $qwe++;


    return $result;
}

/*
* @description (Dashboard Table) : Getting Non India Billable Count
* @return : Total Count
*/
function getBillableNonIndianCount($PodId = '')
{

    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];


    $sql = "SELECT DISTINCT employees.company_employ_id, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, designation.name AS DesignationName,CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,employee_client.billable_type, GROUP_CONCAT(DISTINCT client.client_name) AS Client_Name 
    FROM employees
    JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    JOIN `designation` ON `designation`.designation_id = `employees`.designation_id
    join `location` ON location.location_id = `employees`.location_id 
    JOIN `client` ON `employee_client`.client_id = `client`.client_id 
    WHERE (employee_client.billable = 1 AND employees.`status` NOT IN(6) AND employees.`location_id` NOT IN(1,2))";

    $sql .= " GROUP BY employees.employee_id ";

    return $sql;
}


/*
* @description (Dashboard Table) : Getting Billable Count
* @return : Total Count
*/
public function getBillableCount($PodId = '',$billabletype='')
{

    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];
    $join = $billtype ="";


    $sql = "SELECT DISTINCT employees.company_employ_id, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, designation.name AS DesignationName,CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,employee_client.billable_type, GROUP_CONCAT(DISTINCT client.client_name) AS Client_Name 
    FROM employees
    JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
    JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
    JOIN `client` ON `employee_client`.client_id = `client`.client_id $join";

    if($billabletype!='')
    {
        $billtype= " AND employee_client.billable_type LIKE '%".$billabletype."%'";
    }
    $sql.= " WHERE (employee_client.billable = 1 AND designation.grades<3 $billtype AND employees.`status` NOT IN(6)) ";

    if (is_array($this->getEmployeeList($empData['employee_id'])))
        $employeeArray = implode(',', $this->getEmployeeList($empData['employee_id']));

    if($grade>2 && ($grade<5))
    {
        $sql .= ' AND employees.employee_id IN ('.$employeeArray.')';
    }
    if ($PodId != "")
    {
        $PodId = str_replace("'", "", $PodId);
        $sql .= " AND employee_pod.pod_id IN ($PodId)";
    }

    $sql .= " GROUP BY employees.employee_id ";

    return $sql;
}


/*
* @description (Dashboard Table) : Getting Non Billable Count
* @return : Total Count
*/
public function getNonBillableCount($PodId = '')
{
    $empData        = $this->input->cookie();
    $grade          = $empData['Grade'];

    $sql = "SELECT DISTINCT employees.company_employ_id, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, designation.name as DesignationName,CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName, GROUP_CONCAT( DISTINCT client.client_name) AS Client_Name 
    FROM employees
    LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    LEFT JOIN employee_pod ON employee_pod.employee_id = employees.employee_id 
    LEFT JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id 
    WHERE employees.status !=6 AND designation.grades < 3 AND location_id!=4 AND employees.employee_id NOT IN(SELECT DISTINCT employee_id FROM employee_client WHERE billable=1 AND employee_client.grades < 3 )";
    if (is_array($this->getEmployeeList($empData['employee_id'])))
        $employeeArray = implode(',', $this->getEmployeeList($empData['employee_id']));

    if($grade>2 && ($grade<5))
    {
        $sql .= ' AND employees.employee_id IN ('.$employeeArray.')';
    }

    if($PodId!='')
    {
        $sql .= " AND employee_pod.pod_id IN (".$PodId.")";
    }


    $sql .= " GROUP BY employees.employee_id";
    return $sql;
}

function arrToStr($data)
{

    $str = "";
    foreach ($data as $val) 
    {

        if (is_array($val)) 
        {
            $str .= $this->arrToStr($val);
            $str .= ",";
        } 
        else 
        {

            $str .= $val;

        }
    }

    $str = rtrim($str,",");
    $str = implode(',',array_unique(explode(',', $str)));

    return $str;
}

/*
* @description (Dashboard Table) : Getting Leadership Count which are in Non-Billable
* @return : Total Count
*/
private function __leaderShip($PodId = '', $billability)
{
    $empData = $this->input->cookie();
    $grade   = $empData['Grade'];
    $sessClnt   = $empData['pod_id'];
    $filClnt = $worid = "";


    $sql = "SELECT DISTINCT e.`employee_id`, CAST(SUBSTRING_INDEX(e.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, e.company_employ_id, d1.name as DesignationName,CONCAT_WS(' ',e.first_name,e.last_name) as FirstName, client.client_name as Client_Name 
    FROM employees e 
    JOIN `designation` d1 ON e.`designation_id` = d1.`designation_id`
    LEFT JOIN employee_client ON e.employee_id = employee_client.employee_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id 
    LEFT JOIN employee_pod ON employee_pod.employee_id = e.employee_id
    LEFT JOIN tbl_rhythm_pod ON tbl_rhythm_pod.pod_id = employee_pod.pod_id
    WHERE e.`status` IN(1,2,3,4,5,7) AND e.location_id!=4 AND e.`employee_id`!=1296 AND d1.`grades` > 2";

    if (is_array($this->getEmployeeList($empData['employee_id'])))
    {
        $employeeArray = implode(',', $this->getEmployeeList($empData['employee_id']));
    }
    $employeeArray = ($employeeArray!="") ? ($employeeArray.",".$empData['employee_id']) : $employeeArray;


    if($grade>2 && $grade<5 && $employeeArray!="")
    {
        $sql .= ' AND e.employee_id IN ('.$employeeArray.')';
    }

    if($billability=="Non-Billable Leadership")
    {
        if($grade > 4)
        {
            $sql .= " AND e.employee_id NOT IN (SELECT employee_id FROM employee_client 
            WHERE billable = 1 AND employee_client.grades > 2)"; 
        }
        else
        {
            $sql .= " AND e.employee_id NOT IN (SELECT employee_id FROM employee_client 
            WHERE billable = 1 AND employee_client.grades> 2 AND employee_client.grades < 4)"; 
        }
    }
    if($billability=="Billable Leadership")
    {
        if($grade > 4)
        {
            $sql .= " AND e.employee_id IN (SELECT employee_id FROM employee_client 
            WHERE billable = 1 AND employee_client.grades > 2)"; 
        }
        else
        {
            $sql .= " AND e.employee_id IN (SELECT employee_id FROM employee_client 
            WHERE billable = 1 AND employee_client.grades> 2 AND employee_client.grades < 4)"; 
        }
    }

    if ($PodId != 0) 
    {
        $PodId = str_replace("'", "", $PodId);
        $sql .= " AND tbl_rhythm_pod.pod_id IN ($PodId)";
    }

    return $sql;
}

public function getNrrCount($empId, $clientId = "", $year = "",$status="")
{

    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];
    $worid        = $empData['WorId'];

    $vert = "";
    if(isset($user_data['VertIDs']) && count($empData['VertIDs']) > 1){
        $vert = $empData['Vert'];
    }
    $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : $vert;

    $ClientID = ($this->input->get('ClientID') != '')? implode("','",json_decode($this->input->get('ClientID'))) : '';

    $ClientID = str_replace("'", "", $ClientID);


    $sql = "SELECT count(wor_id) AS NrrCount, DATE_FORMAT( wor.start_date,'%b') AS MONTH FROM wor ";


    $sql .= " WHERE 1=1 AND YEAR(wor.start_date) = YEAR(NOW()) ";
    if($status!=''){
        if($status!='FTE')
            $sql .=  " AND status='".$status."'";

    }  
    if($status=='FTE'){
        $sql="SELECT SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS NrrCount, DATE_FORMAT( wor.start_date,'%b') AS MONTH
        FROM wor
        JOIN fte_model ON wor.wor_id=fte_model.fk_wor_id 
        WHERE 1=1 AND YEAR(wor.start_date) = YEAR(NOW()) AND wor.status='Open'
        AND fte_model.process_id!=0 ";

    }  

    if ($ClientID != "")
    {		
        $sql .= " AND  wor.client IN('$ClientID')";
    }else{

        if($empData['Grade']>2 && ($empData['Grade']<5)){
            $sql .= " AND wor.client IN ('".$empData['ClientId']."')";
        }
    }
    if($worid!='')
    { 
        $sql .=  " AND wor.wor_id IN ('".$worid."')";
    }    


    $sql .= " GROUP BY MONTH( wor.start_date)";

    $query = $this->db->query($sql);

    if ($query->num_rows() == 0)
        return FALSE;
    return $query->result_array();
}
public function getCorCount($empId, $clientId = "", $year = "",$status="")
{

    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];

    $vert = "";
    if(isset($user_data['VertIDs']) && count($empData['VertIDs']) > 1){
        $vert = $empData['Vert'];
    }
    $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : $vert;

    $ClientID = ($this->input->get('ClientID') != '')? implode("','",json_decode($this->input->get('ClientID'))) : '';

    $ClientID = str_replace("'", "", $ClientID);


    if($status=='Open'){
        $sql = "SELECT count(cor_id) AS CorCount, DATE_FORMAT( cor.change_request_date,'%b') AS MONTH FROM cor 
        WHERE 1=1 AND YEAR(cor.change_request_date) = YEAR(NOW()) AND status='Open'
        ";
    }

    if($status!="Open"){
        $sql="SELECT SUM(no_of_fte) AS CorCount, DATE_FORMAT( fte_model.anticipated_date,'%b') AS MONTH 
        FROM cor 
        JOIN fte_model ON cor.cor_id=fte_model.fk_cor_id 
        WHERE 1=1 AND YEAR(fte_model.anticipated_date) = YEAR(NOW()) AND cor.STATUS='Open' 
        ";
    }
    if($status=="Added"){
        $sql .="AND fte_model.change_type=1";
    }else if($status=="Deleted"){
        $sql .="AND fte_model.change_type=2";
    }

    if($empData['Grade']>4){
        if ($ClientID != "")
        {   
            $sql .= " AND cor.client IN($ClientID)";
        }
    }
    if($empData['Grade']>2 && ($empData['Grade']<5)){
        $sql .= " AND cor.client IN (".$empData['ClientId'].")";
    }


    if($status=='Open'){
        $sql .= " GROUP BY MONTH( cor.change_request_date) ";
    }else{
        $sql .=" GROUP BY MONTH( fte_model.anticipated_date)";
    }



    $query = $this->db->query($sql);

    if ($query->num_rows() == 0)
        return FALSE;
    return $query->result_array();
}

/*
* @description (Bar Chart) -  Getting RDR count
* @return -  Count
*/
public function getRdrCount($empId, $clientId = "", $year = "")
{
    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];

    $vert = "";
    if(isset($user_data['VertIDs']) && count($empData['VertIDs']) > 1){
        $vert = $empData['Vert'];
    }
    $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : $vert;

    $ClientID = ($this->input->get('ClientID') != '')? implode("','",json_decode($this->input->get('ClientID'))) : '';

    $ClientID = str_replace("'", "", $ClientID);


    $sql = "SELECT count(cor_id) AS RdrCount, DATE_FORMAT( cor.change_request_date,'%b') AS MONTH FROM cor 
    "; 

    $sql .= " WHERE 1=1 AND YEAR(cor.change_request_date) = YEAR(NOW()) ";

    if($empData['Grade']>4){
        if ($ClientID != "")
        {
            $sql .= " AND cor.client IN($ClientID)";
        }
    }
    if($empData['Grade']>2 && ($empData['Grade']<5)){
        $sql .= " AND cor.client IN (".$empData['ClientId'].")";
    }     


    $sql .= " GROUP BY MONTH(cor.change_request_date) ";

    $query = $this->db->query($sql);
    if ($query->num_rows() == 0)
        return FALSE;
    return $query->result_array();
}
/**
* @description : Function to get employee details.
* @return : if true array else false
*/
public function getEmployDetails()
{
    $empData   = $this->input->cookie();
    $searchKey = $this->input->get('searchKey');
    $sort      = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    $sql = "SELECT `employee_id`, company_employ_id, CAST(SUBSTRING_INDEX(company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, GROUP_CONCAT(CONCAT(IFNULL(first_name, '') ,' ', IFNULL(last_name,''))) AS Employ_Name, email FROM employees WHERE `employees`.`status` IN (1,2,3,4,5,7)";

    if (!$searchKey) 
    {
        $sql .= " AND `employees`.`employee_id`='" . $empData['employee_id'] . "' ";
    } 
    else 
    {
        $sql .= " AND (`company_employ_id` LIKE '%" . $searchKey . "%' OR CONCAT_WS('', IFNULL(first_name, '') ,' ', IFNULL(last_name,'')) LIKE '%" . $searchKey . "%')";
    }
    $sql .= " GROUP BY `employee_id`";

    if (isset($options['sortBy'])) 
    {
        $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
        if ($options['sortBy'] == "company_employ_id") 
        {
            $sort = 'CID ' . $options['sortDirection'];
        }
        $sql .= " ORDER BY " . $sort;
    }


    $query = $this->db->query($sql);

    if ($query->num_rows() > 0) 
    {
        $data = $query->result_array();
        foreach($data as $key => $value)
        {

            $billDetails = $this->getBillableHistoryDetails($value['employee_id']);
            if(count($billDetails)>0)
            {
                $data[$key]['billable'] = $billDetails['rows'];
            } 
            else 
            {
                $data[$key]['billable'] = "null";
            }


            $shiftDetails = $this->getShiftHistoryDetails($value['employee_id']);
            if(count($shiftDetails)>0)
            {
                $data[$key]['shift'] = $shiftDetails['rows'];
            } 
            else 
            {
                $data[$key]['shift'] = "null";
            }
        }

        return array('rows' => $data);
    } 
    else 
    {
        return false;
    }
    return false;
}

/**
* Get Transition History Details Based on EmployID
*/
public function getEmpHistoryDetails()
{
    $EmpID = $this->input->get('EmployID');
    $sql = "SELECT `employees`.`company_employ_id` AS company_employ_id, CONCAT_WS(' ', `employees`.`first_name`, `employees`.`last_name`) AS Employ_Name,
    fv.pod_name AS From_Pod, tv.pod_name AS To_Pod, fc.Client_Name AS From_client, tc.client_name AS To_client,
    CONCAT_WS(' ', fr.`first_name`, fr.`last_name`) AS from_reporting, CONCAT_WS(' ', tr.`first_name`, tr.`last_name`) AS to_reporting,
    employee_transition.comment AS comment, DATE_FORMAT(transition_date, '%d-%b-%Y') AS transition_date 
    FROM `employees`
    LEFT JOIN `employee_transition` ON (`employees`.`employee_id` = `employee_transition`.`employee_id`)
    LEFT JOIN tbl_rhythm_pod fv ON fv.pod_id = employee_transition.from_pod_id
    LEFT JOIN tbl_rhythm_pod tv ON tv.pod_id = employee_transition.to_pod_id
    LEFT JOIN client fc ON fc.client_id = employee_transition.from_client_id
    LEFT JOIN client tc ON tc.client_id = employee_transition.to_client_id
    LEFT JOIN employees fr ON fr.employee_id = employee_transition.from_reporting
    LEFT JOIN employees tr ON tr.employee_id = employee_transition.to_reporting
    WHERE `employees`.`employee_id` = $EmpID
    ORDER BY employee_transition.transition_date DESC,log_id DESC";

    $query	= $this->db->query($sql);
    $result = $query->result_array();

    $results = array('rows' => $result);

    return $results;
}


/**
* Get Billable History Details Based on EmployID
*/
public function getBillableHistoryDetails($empId='')
{
    $EmpID = $this->input->get('EmployID') ? $this->input->get('EmployID') : $empId;

    $sql = "SELECT `project_types`.`project_type` AS name, `wor`.`work_order_id`,`employees`.`employee_id` , `employees`.`company_employ_id`, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Employ_Name, employee_billabality_history.*, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID FROM `employees`
    LEFT JOIN  `employee_billabality_history` ON (`employees`.`employee_id` = `employee_billabality_history`.`employee_id`)
    LEFT JOIN  `project_types` ON (`project_types`.`project_type_id` = `employee_billabality_history`.`project_type_id`)
    LEFT JOIN  `wor` ON (`wor`.`wor_id` = `employee_billabality_history`.`wor_id`)
    WHERE `employees`.`employee_id` IN($EmpID)";

    $query = $this->db->query($sql);
    $data  = $query->result_array();

    $historyData=$data;
    if (!$data) 
        return $results = array('rows' => $data);


    $result = array();     
    foreach ($historyData as $key => $val) 
    {  

        if(isset($val['client_id']))
        {         

            $this->db->select('client_name')->from('client')->where('client_id', $val['client_id']);
            $query  = $this->db->get();
            $Client = $query->result_array();

            $result[] = array(
                'company_employ_id' 		=> $data[0]['company_employ_id'],
                'wor_id'                    => $val['work_order_id'],
                'Employ_Name' 				=> $data[0]['Employ_Name'],
                'Client_Name'				=> (isset($Client[0]['client_name']))?$Client[0]['client_name']:"",
                'billability_type' 			=> $val['billable_type'],
                'project_code'              =>$val['name'],
                'start_date' 				=> $val['start_date'] ? $this->commonDateSetting($val['start_date']): ' -- ',
                'end_date' 					=> $val['end_date'] ? $this->commonDateSetting($val['end_date']): ' -- '
            );
        }
    }

    $results = array('rows' => $result);
    return $results;
}


/**
* Get Shift History Details in History Based on EmployID
*/
public function getShiftHistDetails($empId='')
{
    $EmpID = $this->input->get('EmployID');
    $sql = "SELECT `employees`.`company_employ_id` AS company_employ_id, CONCAT_WS(' ', `employees`.`first_name`, `employees`.`last_name`) AS Employ_Name,
    emp_shift.client_id, emp_shift.shift_id, DATE_FORMAT(from_date, '%d-%b-%Y') AS from_date, DATE_FORMAT(to_date, '%d-%b-%Y') AS to_date,emp_shift.comments AS comment, s.shift_code AS Shift_Name, fc.client_name AS Client_Name 
    FROM `employees`
    LEFT JOIN `emp_shift` ON (`employees`.`employee_id` = `emp_shift`.`employee_id`)			
    LEFT JOIN client fc ON fc.client_id = emp_shift.client_id
    LEFT JOIN shift s ON s.shift_id = emp_shift.shift_id
    WHERE `employees`.`employee_id` = $EmpID
    ORDER BY emp_shift.emp_shift_id DESC";


    $query	= $this->db->query($sql);
    $data = $query->result_array();

    $shiftData=$data;

    $result = array(); 
    foreach ($shiftData as $key => $val) 
    {
        $this->db->select('client_name')->from('client')->where('client_id', $val['client_id']);
        $query  = $this->db->get();
        $Client = $query->result_array();

        $this->db->select('shift_code')->from('shift')->where('shift_id', $val['shift_id']);
        $query  = $this->db->get();
        $Shift = $query->result_array();


        $result[] = array(
            'company_employ_id' 		=> $data[0]['company_employ_id'],                    
            'Employ_Name' 				=> $data[0]['Employ_Name'],
            'Client_Name'				=> (isset($Client[0]['client_name']))?$Client[0]['client_name']:"",
            'Shift_Name'                => (isset($Shift[0]['shift_code']))?$Shift[0]['shift_code']:"",					
            'start_date' 				=> $val['from_date'],
            'end_date' 					=> $val['to_date'], 
            'comment'					=> $val['comment']
        );
    }
    $results = array('rows' => $result);

    return $results;
}

/**
* Get Vertical History Details in History Based on EmployID
*/
public function getVerticalHistDetails($empId='')
{
    $EmpID = $this->input->get('EmployID');
    $sql = "SELECT `employees`.`company_employ_id` AS company_employ_id, CONCAT_WS(' ', `employees`.`first_name`, `employees`.`last_name`) AS Employ_Name,
    fp.pod_name AS Pod, DATE_FORMAT(start_date, '%d-%b-%Y') AS start_date, DATE_FORMAT(end_date, '%d-%b-%Y') AS end_date,employee_pod_history.comments AS comment 
    FROM `employees`
    LEFT JOIN `employee_pod_history` ON `employees`.`employee_id` = `employee_pod_history`.`employee_id`
    LEFT JOIN tbl_rhythm_pod fp ON fp.pod_id = employee_pod_history.pod_id
    WHERE `employees`.`employee_id` = $EmpID
    ORDER BY employee_pod_history.pod_history_id DESC";


    $query  = $this->db->query($sql);
    $data = $query->result_array();


    $podData=$data;

    $result = array(); 
    foreach ($podData as $key => $val) 
    {
        $result[] = array(
            'company_employ_id'     => $val['company_employ_id'],                    
            'Employ_Name'         => $val['Employ_Name'],
            'Pod'                => $val['Pod'],         
            'start_date'        => $val['start_date'],
            'end_date'          => $val['end_date'], 
            'comment'         => $val['comment']
        );
    }
    $results = array('rows' => $result);

    return $results;
}

/**
* Get Shift History Details in History Based on EmployID
*/
public function getClientHistDetails($empId='')
{
    $EmpID = $this->input->get('EmployID');
    $sql = "SELECT `employees`.`company_employ_id` AS company_employ_id, CONCAT_WS(' ', `employees`.`first_name`, `employees`.`last_name`) AS Employ_Name,
    employee_client_history.client_id AS client_id,  DATE_FORMAT(employee_client_history.start_date, '%d-%b-%Y') AS start_date, DATE_FORMAT(employee_client_history.end_date, '%d-%b-%Y') AS end_date, employee_client_history.wor_id AS wor_id, employee_client_history.process_id AS process_id, 
    fc.client_name AS Client_Name, pc.name AS name, wr.work_order_id AS work_order_id				
    FROM `employees`
    LEFT JOIN employee_client_history ON employees.employee_id = employee_client_history.employee_id
    LEFT JOIN client fc ON fc.client_id = employee_client_history.client_id
    LEFT JOIN process pc ON pc.process_id = employee_client_history.process_id
    LEFT JOIN wor wr ON wr.wor_id = employee_client_history.wor_id
    WHERE `employees`.`employee_id` = $EmpID
    ORDER BY employee_client_history.id DESC";

    $query	= $this->db->query($sql);
    $data = $query->result_array();


    $clientData=$data;

    $result = array(); 
    foreach ($clientData as $key => $val) 
    {
        $this->db->select('client_name')->from('client')->where('client_id', $val['client_id']);
        $query  = $this->db->get();
        $Client = $query->result_array();

        $this->db->select('work_order_id')->from('wor')->where('wor_id', $val['wor_id']);
        $query  = $this->db->get();
        $Work = $query->result_array();

        $this->db->select('name')->from('process')->where('process_id', $val['process_id']);
        $query  = $this->db->get();
        $Process = $query->result_array();


        $result[] = array(
            'company_employ_id' 		=> $data[0]['company_employ_id'],                    
            'Employ_Name' 				=> $data[0]['Employ_Name'],
            'Client_Name'				=> (isset($Client[0]['client_name']))?$Client[0]['client_name']:"",
            'wor_id'					=> (isset($Work[0]['work_order_id']))?$Work[0]['work_order_id']:"",
            'process_id'				=> (isset($Process[0]['name']))?$Process[0]['name']:"",
            'start_date' 				=> $val['start_date'],
            'end_date' 					=> $val['end_date'] 

        );
    }
    $results = array('rows' => $result);

    return $results;
}

/**
* Get Shift History Details Based on EmployID
*/
public function getShiftHistoryDetails($empId='')
{
    $sql = "SELECT `employees`.`employee_id` , `employees`.`company_employ_id`, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Employ_Name, client_name, emp_shift.from_date, emp_shift.to_date, shift_code 
    FROM `employees`
    LEFT JOIN `emp_shift` ON `employees`.`employee_id` = `emp_shift`.`employee_id`
    LEFT JOIN `client` ON `client`.`client_id` = `emp_shift`.`client_id`
    LEFT JOIN `shift` ON `shift`.`shift_id` = `emp_shift`.`shift_id`
    WHERE `employees`.`employee_id` = $empId";

    $query = $this->db->query($sql);
    $data = $query->result_array();

    $results = array('rows' => $data);
    return $results;
}


public function getEmployDetailsList()
{
    $empid = $this->input->get('EmployID');
    $empArray = array();

    $sql = "SELECT employees.primary_lead_id, employees.company_employ_id, CONCAT(IFNULL(employees.first_name, '') ,' ', IFNULL(employees.last_name,'')) AS Name, employees.email AS Email, `designation`.`name` AS Designation,
    GROUP_CONCAT(DISTINCT tbl_rhythm_pod.pod_name) AS Pod, GROUP_CONCAT(DISTINCT client.`client_name`) AS Client, GROUP_CONCAT(DISTINCT shift.shift_code) AS Shift,
    GROUP_CONCAT(CASE WHEN employee_client.billable = 1 THEN 'Yes' ELSE 'No' END) AS Billable
    FROM `employees`
    JOIN `designation` ON (`designation`.`designation_id` = employees.designation_id)
    LEFT OUTER JOIN `employee_client` ON (`employee_client`.employee_id = employees.employee_id)
    LEFT OUTER JOIN `verticalmanager` ON (`verticalmanager`.employee_id = employees.employee_id AND `designation`.`grades`=5)
    LEFT JOIN `shift` ON (`shift`.shift_id = `employees`.shift_id)
    LEFT JOIN `client` ON (`employee_client`.client_id = `client`.client_id)
    LEFT JOIN `employee_pod` ON (`employee_pod`.employee_id = `employees`.employee_id)
    LEFT JOIN `tbl_rhythm_pod` ON (`tbl_rhythm_pod`.pod_id = `employee_pod`.pod_id)
    WHERE employees.status IN(1,2,3,4,5,7) AND employees.employee_id = $empid AND client.status = 1 
    GROUP BY employees.employee_id";
// echo $sql; exit;
    $query = $this->db->query($sql);
    if($query->num_rows() == 0)
    {
        $sql = "SELECT employees.primary_lead_id, employees.company_employ_id, CONCAT(IFNULL(employees.first_name, '') ,' ', IFNULL(employees.last_name,'')) AS Name, employees.email AS Email, `designation`.`name` AS Designation,
        GROUP_CONCAT(DISTINCT tbl_rhythm_pod.pod_name) AS Pod,
        CASE WHEN 'Client' IS NULL THEN 0 ELSE '--' END AS Client,
        CASE WHEN 'Billable' IS NULL THEN 0 ELSE '--' END AS Billable,
        GROUP_CONCAT(DISTINCT shift.shift_code) AS Shift
        FROM `employees`
        LEFT JOIN `designation` ON (`designation`.`designation_id` = employees.designation_id)
        LEFT JOIN `shift` ON (`shift`.shift_id = `employees`.shift_id)
        LEFT JOIN `employee_pod` ON (`employee_pod`.employee_id = `employees`.employee_id)
    LEFT JOIN `tbl_rhythm_pod` ON (`tbl_rhythm_pod`.pod_id = `employee_pod`.pod_id)
        WHERE employees.status IN(1,2,3,4,5,7) AND employees.employee_id = $empid";
        $query = $this->db->query($sql);
    }
    $data = $query->result_array();

    if($data[0]['Client'] && $data[0]['Client'] != '')
    {
        $Billable = explode(',', $data[0]['Billable']);
        $Client = explode(',', $data[0]['Client']);

        foreach($Billable as $k => $v)
        {
            if($v == 'Yes')
                $Billable[$k] = $v." ($Client[$k])";
            else
                $Billable[$k] = $v;
        }
        $Billable = array_unique($Billable, SORT_REGULAR);
        $data[0]['Billable'] = implode(',', $Billable);
    }

    if($data[0]['primary_lead_id'])
    {
        $priLead = $this->db->query("SELECT CONCAT_WS(' ', first_name, ' ', last_name) AS PrimaryLead FROM employees WHERE employees.employee_id=".$data[0]['primary_lead_id']);
        $priLead = $priLead->result_array();
    }

    foreach($data[0] as $key => $val)
    {
        $empArray["rows"][]= array("Name"=>$key, "Value"=>$val);
    }
    $empArray['rows'][1]['Name'] = 'Employee ID';
    $empArray['rows'][] = array('Name' => 'Reports To', "Value"=>$data[0]['primary_lead_id'] ? $priLead[0]['PrimaryLead'] : '--');
    array_shift ($empArray['rows']);

    $totalCount = $query->num_rows();
    if($query->num_rows() > 0)
        return $empArray;
    else
        return false;
}

/* Query to get Billable employees globally*/
function getBillableCountQuery($Verticals = '',$processIds = '',$subVertical = '')
{
    $empData     = $this->input->cookie();
    $excludingProcess = 10; 
    $processArray = '';

    switch ($empData['Grade']) 
    {
        case 3:
        case '3.1':
        {
            $processArray = $this->getProcess();


            if (count($processArray) == 0)
                $status = false;
            else
                $processArray = " AND c.client_id IN(" . implode(",", $processArray) . ") ";
        }
    }
    $sqlBillableQuery = "SELECT DISTINCT temp.employee_id AS empid,c.client_name AS Client_Name FROM client c 
    JOIN ( SELECT employee_id,client_id FROM employee_client WHERE billable = 1 ) AS temp ON temp.client_id = c.client_id
    JOIN employees ON employees.employee_id = temp.employee_id";

    if($empData['Grade'] == 4 )
        $sqlBillableQuery.=" AND (employees.primary_lead_id IN (SELECT employee_id FROM employees WHERE primary_lead_id = '".$empData['employee_id']."') || employees.primary_lead_id = '".$empData['employee_id']."')";

    $sqlBillableQuery .= " JOIN `employee_vertical` ON employee_vertical.`employee_id` = employees.`employee_id`  ";
    $sqlBillableQuery .= " JOIN `verticals` ON verticals.`vertical_id` = employee_vertical.`vertical_id` ";
    $sqlBillableQuery .= " WHERE employees.`status` IN (1,2,3,4,5,7) AND c.status = 1 ";  

    if($Verticals!= '')
    {
        $sqlBillableQuery.= " AND verticals.vertical_id IN ( $Verticals )";
    }
    if($subVertical != '')
    {
        $sqlBillableQuery.= " AND verticals.parent_vertical_id IN ( $subVertical )";
    }
    if(isset($empData['VertIDs']))
    {
        if($empData['Grade'] <6)
        {
            $VerticalIds = implode(',',$empData['VertIDs']);
            $sqlBillableQuery.= " AND verticals.vertical_id IN ( $VerticalIds )";
        }
    } 

    return $sqlBillableQuery;

}

/* Query to get NonBillable employees globally*/
function getNonBillableCountQuery($Verticals = '',$processIds = '',$subVertical = '',$chartName = '')
{
    $empData     = $this->input->cookie();
    $sqlBillable = $this->getBillableCountQuery($Verticals, $processIds, $subVertical);

    $billNonAry       = "";
    $vertmanager = '';
    $excludingProcess = 10; 
    if (isset($sqlBillable))
        $result_select = $this->db->query($sqlBillable);

    $empArray = array();
    if (isset($result_select) && count($result_select->result_array()) != 0) 
    {
        foreach ($result_select->result_array() as $key => $val)
            array_push($empArray, $val['empid']);
        $billNonAry = " AND employees.employee_id NOT IN (" . implode(",", $empArray) . ") ";
    }
    $BillableCount = count($empArray);
    $processArray = '';

    switch ($empData['Grade']) 
    {
        case 3:
        case '3.1':
        case 4:
        case 5: {
            $processArray = $this->getProcess();
            if (count($processArray) == 0){
                $status = false;
                $processArray = '';
            }
            else 
            {
                $processArray = " AND c.client_id IN(" . implode(",", $processArray) . ") AND c.status=1 ";
            }
        }
    }
    $sqlNonBillableQuery = "SELECT  DISTINCT temp.employee_id as empid,c.client_name AS Client_Name FROM client c 
    JOIN ( SELECT employee_id,client_id FROM employee_client WHERE billable = 0  AND employee_client.grades<3 ) AS temp ON temp.client_id = c.client_id/*AND grades <3*/ 
    JOIN employees ON employees.employee_id = temp.employee_id";

    $sqlNonBillableQuery .= " JOIN `employee_vertical` ON employee_vertical.`employee_id` = employees.`employee_id`  ";
    $sqlNonBillableQuery .= " JOIN `verticals` ON verticals.`vertical_id` = employee_vertical.`vertical_id` ";

    if($chartName == "")
        $sqlNonBillableQuery .= "WHERE employees.`status` IN (1,2,3,4,5,7)   AND c.status = 1 $billNonAry";  
    else 
        $sqlNonBillableQuery .= "WHERE employees.`status` IN (1,2,3,4,5,7) AND c.status = 1 $billNonAry";  

    if($Verticals!= '')
    {
        $sqlNonBillableQuery.= " AND verticals.vertical_id IN ( $Verticals ) ";
        $vertmanager = $Verticals;
    }
    if($subVertical != '')
    {
        $sqlNonBillableQuery.= " AND verticals.parent_vertical_id IN ( $subVertical )";
        $vertmanager = '';
    }


    if(isset($empData['VertIDs']))
    {
        if($empData['Grade'] <6)
        {
            $VerticalIds = implode(',',$empData['VertIDs']);
            $sqlNonBillableQuery.= " AND verticals.vertical_id IN ( $VerticalIds )";
            $vertmanager = $VerticalIds;
        }
    }    

    return $sqlNonBillableQuery;

}

function getLeadverticalcount($Verticals = '',$processIds = '',$subVertical = '')
{
    $lcount = 0;
    $empData     = $this->input->cookie();
    $loggedInVerticals = '';
    if(isset($empData['VertIDs']))
    {
        if($empData['Grade'] <6)
        {
            $loggedInVerticals = implode(',',$empData['VertIDs']);
            $lgVert = $empData['VertIDs'];
        }
    }
    $vert = array();
    $vertIds = '';
    $processArray = '';
    $subvertCount = false;
    if($subVertical!= '')
    {
        $this->db->select("verticals.vertical_id ", FALSE)->from("verticals")
        ->join('client_vertical', 'client_vertical.vertical_id = verticals.vertical_id')
        ->where('verticals.parent_vertical_id =', $subVertical);
        if($loggedInVerticals != '')
        {
            $this->db->where_in('verticals.vertical_id ',$lgVert);
        }
        $this->db->group_by('verticals.vertical_id');
        $this->db->order_by('verticals.Name');
        $query = $this->db->get();

        if (count($query->result_array()) > 0)
        {          
            foreach($query->result_array() as $key=>$val)
            {
                $vert[] = $val['vertical_id'];
            }
            $vertIds = implode(",",$vert);        	
        }
        else 
            $subvertCount  = true;
    }
    switch ($empData['Grade']) 
    {
        case 3:
        case '3.1':
        case 4:
        case 5: {
            $processArray = $this->getProcess();
            if (count($processArray) == 0){
                $processArray = '';
                $status = false;
            }
            else
                $processArray = " AND Leadership.ProcessID IN(" . implode(",", $processArray) . ")  AND Leadership.`ProcessStatus` = 0";
        }
    }

    $sql = "SELECT COUNT(temp1.employee_id) AS Value1,temp1.vertical_id 
    FROM (SELECT DISTINCT Leadership.`employee_id`,Leadership.`vertical_id`,verticals.`Name` 
    FROM ((SELECT v.`employee_id`,client_vertical.`vertical_id`,client_vertical.`client_id`
    FROM `verticalmanager` v
    JOIN client_vertical ON (v.vertical_id = client_vertical.vertical_id) ) 
    UNION
    (SELECT t.`employee_id`,client_vertical.`vertical_id`,client_vertical.`client_id`
    FROM `employee_client` t  
    JOIN client ON (t.client_id = client.client_id) 
    JOIN client_vertical ON (client.client_id = client_vertical.client_id) 
    WHERE t.`billable` = 0 AND grades > 2 
    AND t.employee_id NOT IN (SELECT employee_id FROM employee_client WHERE billable = 1 AND grades > 2 AND grades < 4)  )) AS Leadership 
    JOIN employees e1 ON Leadership.`employee_id` = e1.`employee_id` 
    JOIN `designation` d1 ON e1.`designation_id` = d1.`designation_id` AND d1.grades < 7  
    JOIN `verticals` ON Leadership.`vertical_id`=`verticals`.`vertical_id` AND e1.`status` IN(3,5) AND e1.`employee_id`!=1296 
    GROUP BY Leadership.`employee_id`) temp1";
    if($Verticals != '' || $vertIds != '' || $loggedInVerticals!= '' || $subVertical!= '')
    {
        $sql.=" WHERE ";
    }

    if($loggedInVerticals != '')
    {    			

        if($vertIds!= '')
        {
            $sql.="   temp1.vertical_id IN ( ".$vertIds.")";
        }
        else
        {
            if($Verticals != '')
            {
                $sql.="  temp1.vertical_id = ".$Verticals;
            }
            else
                $sql.="  temp1.vertical_id IN ( ".$loggedInVerticals.")";
        }
    }
    else
    {
        if($Verticals != '')
        {
            $sql.=" temp1.vertical_id = ".$Verticals;
        }

        if($vertIds!= '' && $subVertical != '')
        {
            $sql.="  temp1.vertical_id IN ( ".$vertIds.")";
        }
        if($vertIds!= '' && $Verticals != '')
        {
            $sql.="  AND temp1.vertical_id IN ( ".$vertIds.")";
        }
    }

    $sql.=" GROUP BY temp1.vertical_id";

    $result_select = $this->db->query($sql);
    if($Verticals == '')
    {   	  	
        if($subvertCount) 
            return 0;

        foreach ($result_select->result_array() as $key => $val){
            $lcount += $val['Value1'];
        }
        return $lcount;
    }
    else
    {
        if(count($result_select->row()) >0)
            return $result_select->row()->Value1;
        else
            return 0;
    }
}

//added to get verticalwise grade empoyee count
public function getGradeVerticalCount($empId, $PodId = 0, $year = "")
{
    $PodId = str_replace("'", "", $PodId);

    $employeeArray  = "";
    $verthiearchy   = "";
    $empData        = $this->input->cookie();
    $grade          = $empData['Grade'];
    $vert = "";
    if(isset($user_data['PodId']) && count($empData['PodId']) > 1)
    {
        $vert = $empData['pod_id'];
    }

    $sql = '';
    switch ($grade) 
    {
        case 3:
        case '3.1':
        case 4:
        if (is_array($this->getEmployeeList($empId)))
            $employeeArray = implode(',', $this->getEmployeeList($empId));

        if ($employeeArray != '') 
        {
            $sql .= "SELECT COUNT(DISTINCT(`employees`.`employee_id`)) AS Value1, `designation`.`grades` AS Name 
            FROM `employees`
            LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id
            LEFT JOIN designation ON `employees`.`designation_id` = designation.`designation_id`
            LEFT JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
            LEFT JOIN tbl_rhythm_pod ON tbl_rhythm_pod.pod_id = employee_pod.pod_id
            WHERE `designation`.`grades` != 0 AND `employees`.`status` IN (1,2,3,4,5,7) AND `employees`.location_id!=4";
            $sql .= " AND `employees`.`employee_id` IN($employeeArray)";
        }
        break;
        default:
        $sql .= "SELECT COUNT(DISTINCT(`employees`.`employee_id`)) AS Value1, `designation`.`grades` AS Name 
        FROM `employees`
        LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
        LEFT JOIN designation ON `employees`.`designation_id` = designation.`designation_id` 
        LEFT JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
        LEFT JOIN tbl_rhythm_pod ON tbl_rhythm_pod.pod_id = employee_pod.pod_id
        WHERE `designation`.`grades` != 0 AND `employees`.`status` IN (1,2,3,4,5,7) AND `employees`.location_id!=4 $vert";
        break;
    }

    if ($PodId != 0) 
    {
        $PodId = str_replace("'", "", $PodId);
        $sql .= " AND tbl_rhythm_pod.pod_id IN ($PodId)";
    }
    if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['pod_id']!="")){
        $sql .= " AND tbl_rhythm_pod.pod_id IN (".$empData['pod_id'].")";
    }

    if ($sql != '') 
    {
        $sql .= " GROUP BY `designation`.grades ORDER BY `designation`.grades ";

        $query = $this->db->query($sql);

        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array();
    } else
    return false;
}

/*
* @Desctiption: ( Bar chart ) - For getting employee count based on grade
* Returns array of grade and count pairs
*
*/
public function getGradeCount($empId, $clientId = "", $year = "")
{

    $clientId = str_replace("'", "", $clientId);

    $employeeArray  = "";
    $verthiearchy   = "";
    $empData        = $this->input->cookie();
    $grade          = $empData['Grade'];
    $vert = "";
    if(isset($user_data['VertId']) && count($empData['VertId']) > 1)
    {
        $vert = $empData['VertId'];
    }

    $sql = '';
    switch ($grade) 
    {
        case 3:
        case '3.1':
        case 4:
        case 5:
        if (is_array($this->getEmployeeList($empId)))
            $employeeArray = implode(',', $this->getEmployeeList($empId));

        if ($employeeArray != '') 
        {
            $sql .= "SELECT COUNT(DISTINCT(`employees`.`employee_id`)) AS Value1, `designation`.`grades` AS Name 
            FROM `employees`
            LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id
            LEFT JOIN client ON employee_client.client_id = client.client_id
            LEFT JOIN designation ON `employees`.`designation_id` = designation.`designation_id`
            LEFT JOIN `employee_vertical` ON (employee_vertical.`employee_id` = employees.`employee_id`)
            LEFT JOIN `verticals` ON (verticals.`vertical_id` = employee_vertical.`vertical_id`)
            WHERE `designation`.`grades` != 0 AND `employees`.`status` IN (1,2,3,4,5,7) ";
            $sql .= " AND `employees`.`employee_id` IN($employeeArray) ";
        }
        break;
        default:
        $sql .= "SELECT COUNT(DISTINCT(`employees`.`employee_id`)) AS Value1, `designation`.`grades` AS Name 
        FROM `employees`
        LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
        LEFT JOIN client ON employee_client.client_id = client.client_id
        LEFT JOIN designation ON `employees`.`designation_id` = designation.`designation_id` 
        LEFT JOIN `employee_vertical` ON (employee_vertical.`employee_id` = employees.`employee_id`)
        LEFT JOIN `verticals` ON (verticals.`vertical_id` = employee_vertical.`vertical_id`)
        WHERE `designation`.`grades` != 0 AND `employees`.`status` IN (1,2,3,4,5,7)";
        break;
    }

    if ($clientId != "") 
    {
        $sql .= "AND client.client_id IN ($clientId)";
    }
    if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['ClientId']!="")){
        $sql .= " AND client.client_id IN (".$empData['ClientId'].")";
    }

    if ($sql != '') 
    {
        $sql .= " GROUP BY `designation`.grades ORDER BY `designation`.grades ";

        $query = $this->db->query($sql);

        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array();
    } else
    return false;
}


/*
* @description : Dashboard Location graph - getting count of employee location for particular lead
* @return : Location Name and count
*/
public function getLocationCount($VertId = 0)
{
    $VertId = str_replace("'", "", $VertId);
    $sql = $this->__locationCount_query($VertId);
    if($sql != '')
    {
        $sql .= "GROUP BY employees.`location_id` ";
        $query = $this->db->query($sql);

        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array(); 
    }  	
    else
        return '';
}


private function __locationCount_query($PodId, $location = "", $clientID ="")
{
    $empData       = $this->input->cookie();
    $grade         = $empData['Grade'];
    $employeeArray = "";
    $pods=$pod = "";
    if(isset($user_data['PodId']) && count($empData['PodId']) > 1){
        $vert = $empData['pod_name'];
    }

    $status        = true;       
    switch ($grade) {
        case 3:
        case '3.1':
        case 4:{
            $employeeArray = $this->getEmployeeList($empData['employee_id']);
            if (count($employeeArray) == 0)
                $status = false;
            else
                $employeeArray = " AND employees.employee_id IN(" . implode(",", $employeeArray) . ") ";
        }
        break;
    }

    if ($location != 0)
        $sql = "SELECT DISTINCT employees.`employee_id`, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, employees.company_employ_id, CONCAT_WS(' ',employees.first_name,employees.last_name) AS FirstName,designation.name AS DesignationName,GROUP_CONCAT(DISTINCT client.client_name) AS Client_Name,GROUP_CONCAT(DISTINCT tbl_rhythm_pod.pod_name) AS Pod_Name ";
    else
        $sql = "SELECT COUNT(DISTINCT employees.`employee_id`) AS Value1, `location`.`Name`, location.`location_id` ";

    if ($status) 
    {
        $sql .= "FROM employees
        LEFT JOIN `employee_client` ON (employee_client.`employee_id` = employees.`employee_id`) 
        LEFT JOIN `client` ON (client.`client_id` = employee_client.`client_id`)  
        LEFT JOIN `employee_pod` ON (employee_pod.`employee_id` = employees.`employee_id`)
        LEFT JOIN `tbl_rhythm_pod` ON (tbl_rhythm_pod.`pod_id` = employee_pod.`pod_id`)            		
        JOIN `location` ON (employees.`location_id` = location.`location_id`)
        JOIN `designation` ON (employees.`designation_id` = designation.`designation_id`)";


        $sql .=	" WHERE employees.`status` IN (1,2,3,4,5,7) AND designation.grades<=7 $employeeArray $pods ";
        if ($PodId != 0) 
        {
            $PodId = str_replace("'", "", $PodId);
            $sql.= "AND employee_pod.pod_id IN($PodId) ";
        }

        $sql .=	" AND employees.location_id != 4 ";
    }
    else
    {
        $sql = '';
    }

    return $sql;
}



/*
* @description : Dashboard Shift graph - getting count of employee shifts for particular lead
* @return : Shift Name and count
*/


public function getShiftCount($PodId = 0)
{
    $sql = $this->__getShiftCount_query('', $PodId);
    if ($sql) {
        $sql .= "JOIN `designation` ON `designation`.designation_id = `ShiftwiseTable`.designation_id ";

        $sql .= "GROUP BY ShiftwiseTable.shift_code ";
        $sql .= "ORDER BY ShiftwiseTable.start_time ";

        $query = $this->db->query($sql);
        return $query->result_array();
    } else
    return false;
}


private function __getShiftCount_query($shift = '', $PodId = '')
{

    $empData = $this->input->cookie();
    $grade              = $empData['Grade'];
    $employeeArray       = "";
    $status             = true;

    switch ($grade) 
    {
        case 3:
        case '3.1':
        case 4:{
            $employeeArray = $this->getEmployeeList($empData['employee_id']);
            if (count($employeeArray) == 0)
                $status = false;
            else
                $employeeArray = " AND employees.employee_id IN(" . implode(",", $employeeArray) . ") ";
        }
        break;  
    }

    if ($shift)
        $sql = "SELECT DISTINCT ShiftwiseTable.`employee_id`, ShiftwiseTable.company_employ_id, CAST(SUBSTRING_INDEX(ShiftwiseTable.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, CONCAT_WS(' ',ShiftwiseTable.first_name,ShiftwiseTable.last_name) AS FirstName,designation.name AS DesignationName, GROUP_CONCAT(DISTINCT ShiftwiseTable.client_name) AS Client_Name  ";
    else
        $sql = "SELECT COUNT(DISTINCT ShiftwiseTable.employee_id) AS Value1, ShiftwiseTable.`shift_code` AS Name, ShiftwiseTable.`shift_id` ";

    $podQry = "";
    if ($PodId != 0) 
    {
        $PodId = str_replace("'", "", $PodId);
        $podQry = "AND employee_pod.pod_id IN($PodId) ";
    }
    if ($status) 
    {
        $sql .= "FROM (SELECT DISTINCT employees.employee_id, shift.shift_code, shift.shift_id, employees.first_name, employees.last_name, employees.company_employ_id,employees.designation_id, shift.start_time, client.client_name, tbl_rhythm_pod.pod_name FROM employees
        JOIN shift ON (employees.`shift_id` = shift.shift_id)
        LEFT JOIN employee_client ON (employees.employee_id = employee_client.employee_id)
        LEFT JOIN `client` ON (employee_client.client_id = client.client_id)  
        LEFT JOIN `employee_pod` ON (employee_pod.`employee_id` = employees.`employee_id`)
        LEFT JOIN `tbl_rhythm_pod` ON (tbl_rhythm_pod.`pod_id` = employee_pod.`pod_id`)
        WHERE employees.`status` IN (1,2,3,4,5,7) AND location_id !=4 $podQry $employeeArray ";

        $sql .= ") AS ShiftwiseTable ";
        return $sql;
    } 
    else
        return false;
}

/*
* @description : getting user name and designation detaild for Tree View
*/
public function getUserName($empId)
{
    $this->db->select("CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName, employees.email, employees.designation_id ,designation.name,designation.grades", FALSE)->from("employees")->join('designation', 'employees.designation_id = designation.designation_id')->where('employees.employee_id =', $empId)->order_by('employees.first_name');
    $query = $this->db->get();
    if ($query->num_rows() == 0)
        return FALSE;
    return $query->result_array();
}


/*
* @description : getttin empName based on primary lead id
* get the employees expect from hr pool and onboarding process
*/
public function getEmpNames($empId)
{        
    $this->db->select("employees.employee_id,CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,employees.comments as comment,employees.email, employees.designation_id ,designation.name,designation.grades", FALSE)
    ->from("employees")->join('designation', 'employees.designation_id = designation.designation_id')
    ->where('employees.employee_id NOT IN', '(SELECT employees.employee_id FROM employees JOIN employee_client ON employee_client.employee_id = employees.employee_id WHERE status IN("1,2,4") AND grades < 3 )', false)->where('employees.primary_lead_id =', $empId)
    ->where('employees.primary_lead_id !=1')->where('employees.status NOT IN (6)')
    ->order_by('employees.first_name');
    $query = $this->db->get();

    if ($query->num_rows() == 0)
        return FALSE;
    return $query->result_array();

}


public function getEmpCountFromView($empId)
{
    $employeeArray = $this->getEmployeeList($empId);
    if (count($employeeArray) == 0)
        return FALSE;
    return count($employeeArray);

}


/*
* @description : getting all emp_process data for Tree View
* @return : Emplyee data
*/
public function getEmpProcessData($empIdVal = '')
{
    $toGetEmpData = $this->db->query("SELECT * FROM employees WHERE primary_lead_id ='" . $empIdVal . "'");
    if ($toGetEmpData->num_rows() == 0)
        return FALSE;
    return $toGetEmpData->result_array();
}


/* To get HR Pool Employess @paramaters ProcessID(NewJoinees,Transition,TrainingNeeded)*/
public function getHRProcessEmpNames($processID)
{
    $Status = "";
    $hr = array();
    $hrOpsLeadSql = "SELECT employee_id FROM employee_client WHERE Grades > 2 AND Grades < 4";
    $queryHrLead = $this->db->query($hrOpsLeadSql);
    if($queryHrLead->num_rows() != 0)
    {
        foreach($queryHrLead->result_array() as $key => $val)
            array_push($hr, $val['employee_id']);
    }

    $HrLead = implode(",", $hr);

    $this->db->select("employees.employee_id,CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,employees.comments as comment,designation.name AS Name,designation.grades",FALSE)->from("employees")
    ->join('designation', 'employees.designation_id = designation.designation_id');
    if($processID == "NewJoinees-process")
    {
        $this->db->where('employees.status = 1');
    }
    if($processID == "Training Needed-process")
    {
        $this->db->where('employees.status = 7');
    }
    if($processID == "Transition-process")
    {
        $this->db->where('employees.status = 2');
    }
    if($processID == "Long Leave-process")
    {
        $this->db->where('employees.status = 4');
    }
    $this->db->where('employees.location_id != 4 ');

    $query  = $this->db->get();
    $result = $query->result_array();
    if($query->num_rows() == 0)
    {
        return  $result;
    }
    else
    {
        if(is_array($result))
        {
            return $result;
        }
    }
}


public function getNotAssignEmpNames()
{
    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];
    $employeeArray = "";
    $searchKey    = $this->input->get('searchKey');
    switch ($grade) {
        case 3:
        case '3.1':
        case 4:{
            $employeeArray = $this->getEmployeeList($empData['employee_id']);
        }
    }

    $opsPod = implode("','", json_decode(OPS_VERT));
    $sql1="SELECT DISTINCT company_employ_id AS EmployeeID, CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName, employees.employee_id, d.grades
    FROM employees
    JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
    LEFT JOIN designation d ON d.designation_id = employees.designation_id
    WHERE employees.status NOT IN(6,4) AND d.grades<3 AND employee_pod.pod_id IN ('".$opsPod."') AND employees.employee_id NOT IN(SELECT DISTINCT employee_id FROM employee_client) GROUP BY employees.employee_id";

    $sql1 .= " ORDER BY first_name ASC";
    $query1 = $this->db->query($sql1);

    $memeber_nondata = $query1->result_array();

    $mergedData = array_merge($memeber_nondata);
    foreach ($mergedData as $key => $row) {
        $FirstName[$key]  = $row[FirstName];
    }
    array_multisort($FirstName, SORT_ASC, $mergedData);
    return $mergedData;
}


private function __getEmployBillableCnt()
{        
    $sql = "SELECT DISTINCT temp.employee_id as empid 
    FROM client c 
    JOIN ( SELECT employee_id,client_id FROM employee_client WHERE billable = 1 AND grades < 4 ) AS temp ON temp.client_id = c.client_id  ";

    if (isset($sql))
        $result_select = $this->db->query($sql);

    $empArray = array();
    if (isset($result_select) && count($result_select->result_array()) != 0) {
        foreach ($result_select->result_array() as $key => $val)
            array_push($empArray, $val['empid']);
    }

    return $empArray;

}

/**
@Function for Get Resources With Respect To Clients
*/
function clientBillableResource()
{
    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];
    $employeeArray = "";
    $searchKey    = $this->input->get('searchKey');
    switch ($grade) {
        case 3:
        case '3.1':
        case 4:{
            $employeeArray = $this->getEmployeeList($empData['employee_id']);
        }
    }

    $sort                     = json_decode($this->input->get('sort'), true);
    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    if (isset($options['sortBy'])) 
    {
        $this->db->order_by($options['sortBy'], $options['sortDirection']);
    } 
    else 
    {
        $this->db->order_by('client_name');
    }

    $this->db->select("c.client_name, c.client_id, GROUP_CONCAT(DISTINCT NULLIF( `wor`.`wor_id`, '')) AS wor_id, SUM(forecast_wor) AS ForcastWOR, SUM(forecast_cor) AS ForcastCOR ,GROUP_CONCAT(NULLIF(fte_id,'')) AS fte_id,GROUP_CONCAT(NULLIF(hourly_based_id,'')) AS hourly_id,GROUP_CONCAT(NULLIF(volume_base_id,'')) AS volume_id,GROUP_CONCAT(NULLIF(project_model_id,'')) AS project_id,'' AS model")->from('client c');
    $this->db->join('wor', 'wor.client = c.client_id', 'left');
    $this->db->join('wor_cor_transition wt', 'wt.client_id = c.client_id', 'left');
    $this->db->where('c.status = 1');
    if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['ClientId']!=""))
    {
        $this->db->where("c.client_id IN (".$empData['ClientId'].")");
    }
    if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['WorId']!=""))
    {
        $this->db->where("wor.wor_id IN (".$empData['WorId'].")");
    }
    $this->db->where("wor.status IN ('Open')");

    if (isset($searchKey) && $searchKey != 'empty') 
    {
        $search = array(
            'c.client_name' => $searchKey
        );
        $this->db->like($search);
    }
    $this->db->group_by('c.client_id');

    $query  = $this->db->get();
    $result = $query->result_array();
    // echo $this->db->last_query();exit;

    if ($query->num_rows() > 0) 
    {
        foreach ($result as $key => $val) 
        {
            $modelArray = [];
            $cid = $val['client_id'];
            $BillableResources = 0;

            $Bilquery = "SELECT COUNT(ec.employee_id) AS EmployID FROM employee_client ec ";

            $Bilquery .= " JOIN employees e ON e.employee_id = ec.employee_id WHERE ec.client_id IN (" . $cid . ") AND ec.billable = 1 AND e.status != 6";
            if (count($employeeArray) != 0 && $employeeArray!="")
            {
                $Bilquery .= " AND ec.employee_id IN (".implode(",", $employeeArray).")";
            }
            $BillableQuery = $this->db->query($Bilquery);
            $BillableEmps  = $BillableQuery->result_array();


            $nonBilquery = "SELECT COUNT(ec.employee_id) AS EmployID FROM employee_client ec ";

            $nonBilquery .= " JOIN employees e ON e.employee_id = ec.employee_id WHERE ec.client_id IN (" . $cid . ") AND ec.billable = 0 AND e.status != 6";
            if (count($employeeArray) != 0 && $employeeArray!="")
            {
                $nonBilquery .= " AND ec.employee_id IN (".implode(",", $employeeArray).")";
            }
            $nonBillableQuery = $this->db->query($nonBilquery);

            $nonBillableEmps  = $nonBillableQuery->result_array();

            $result[$key]['Billable']    =  ($BillableEmps[0]['EmployID']); 
            $result[$key]['NonBillable'] = $nonBillableEmps[0]['EmployID']; 

            if($result[$key]['fte_id']!="" && $result[$key]['fte_id']!='null')
            {
                $sql = "SELECT SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS fte
                FROM  `fte_model` 
                WHERE fk_wor_id IN (".$result[$key]['wor_id'].") AND process_id !=0 AND anticipated_date <= CURDATE()";

                $query = $this->db->query($sql);
                $totCount = $query->row_array();
                // echo($sql);exit;
                if($totCount['fte']>0)
                {
                    $modelArray[] = $totCount['fte'];
                }
            }
            if($result[$key]['hourly_id']!="" && $result[$key]['hourly_id']!='null')
            {
                $sql1 = "SELECT SUM(CASE change_type WHEN 1 THEN max_hours  WHEN 2 THEN -max_hours END) AS  max, SUM(CASE change_type WHEN 1 THEN min_hours WHEN 2 THEN -min_hours END) AS min
                FROM  `hourly_model` 
                WHERE fk_wor_id IN (".$result[$key]['wor_id'].") AND process_id !=0 AND anticipated_date <= CURDATE()";

                $query1 = $this->db->query($sql1);
                $totCount1 = $query1->row_array();
                if($totCount1['max']>0 || $totCount1['min']>0)
                {
                    $modelArray[] = "Hourly";
                }
            }
            if($result[$key]['volume_id']!="" && $result[$key]['volume_id']!='null')
            {
                $sql2 = "SELECT SUM(CASE change_type WHEN 1 THEN max_volume  WHEN 2 THEN -max_volume END) AS  max,SUM(CASE change_type WHEN 1 THEN min_volume WHEN 2 THEN -min_volume END) AS min
                FROM  `volume_based_model` 
                WHERE fk_wor_id IN (".$result[$key]['wor_id'].") AND process_id !=0 AND anticipated_date <= CURDATE()";

                $query2 = $this->db->query($sql2);
                $totCount2 = $query2->row_array();
                if($totCount2['max']>0 || $totCount2['min']>0)
                {
                    $modelArray[] = "Volume";
                }
            }
            if($result[$key]['project_id']!="" && $result[$key]['project_id']!='null')
            {
                $sql3 = "SELECT DATE_FORMAT(start_duration, '%d-%b-%Y') AS min,DATE_FORMAT(end_duration, '%d-%b-%Y')  AS max
                FROM  `project_model` 
                WHERE fk_wor_id IN (".$result[$key]['wor_id'].") AND process_id !=0 AND anticipated_date <= CURDATE()";

                $query3 = $this->db->query($sql3);
                $totCount3 = $query3->row_array();
                if($totCount3['max']>0 || $totCount3['min']>0)
                {
                    $modelArray[] = "Project";
                }
            }
            $result[$key]['model'] = implode(",", array_unique($modelArray));
           
        }
    }
    if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['WorId']==""))
    {
        $result = array(
            'rows' => array(),
            'totalCount' => 0
        );
    }
    else
    {
        $result = array(
            'rows' => $result,
            'totalCount' => $query->num_rows()
        );
    }

    return $result;
}


public function locationwiseClient($empID)
{
    $result = $this->__locationWiseClient_query();
    if($result)
    {
        return $result;
    } 
    else
        return false;
}


public function getlocationClientDetails($location, $loggedInempId)
{
// print_r($location);exit;
    $options = array();
    if ($this->input->get('hasNoLimit') != '1') {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }

    $sql = $this->__locationWiseClient_query($location);
// $sql .= "JOIN client ON (client.client_id = Clientwise.client_id) ";
// $sql .= "AND Clientwise.location_id=$location ";
    $totQuery = $this->db->query($sql);
    $totCount = $totQuery->num_rows();

    $sort = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    if (isset($options['sortBy'])) {
        $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
        $sql .= " ORDER BY " . $sort;
    } else
    $sql .= " ORDER BY client.Client_Name ";

    if (isset($options['limit']) && isset($options['offset'])) {
        $limit = $options['offset'] . ',' . $options['limit'];
        $sql .= " LIMIT " . $limit;
    } else if (isset($options['limit']))
    $sql .= " LIMIT " . $options['limit'];
// echo $sql;exit;
    $query = $this->db->query($sql);
    if ($query->num_rows() == 0)
        return FALSE;
    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}
private function __locationWiseClient_query($location = '')
{
    $empData	= $this->input->cookie();
    $grade      = $empData['Grade'];
    $vert = "";
    if(isset($user_data['VertId']) && count($empData['VertId']) > 1)
    {
        $vert = $empData['Vert'];
    }
//  $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : $vert;
    $sqlvert = "";
    $grupvertcals = "";
    $status       = true;

//  echo $location;exit;
    if($location){
        $sql = "SELECT DISTINCT(client.client_name) AS Client_Name, client.Web, client.Country FROM client where client.status=1 AND client.country LIKE'%".$location."%'";
        if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['ClientId']!="")){
            $sql .= " AND client.client_id IN (".$empData['ClientId'].")";
        }
//echo $sql;exit;
//  $sql .= "GROUP BY client.country";

        return $sql;
    }else{

        $sql_country="SELECT `c`.`country_name` as Name FROM `country` `c` ORDER BY `country_name` ASC ";
        $query = $this->db->query($sql_country);
        $result=$query->result_array();
// print_r($result);exit;

        if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['ClientId']=="")){
            return false;
        }

        if ($query->num_rows() > 0) 
        {
            foreach ($result as $key => $val) 
            {
                $cname               = $val['Name'];
                $sql_err="SELECT COUNT(client_id) AS clientname FROM client WHERE  client.status=1 AND client.country LIKE '%".$cname."%'";
                if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['ClientId']!="")){
                    $sql_err .= " AND client.client_id IN (".$empData['ClientId'].")";
                }
// $sql_err="SELECT count(e.id) as err_cnt FROM external_errors e where  e.status=0 and e.client_id=".$cid;
                $query_err = $this->db->query($sql_err);
                $result_err=$query_err->result_array();
//print_r($result_err);exit;
                $result[$key]['Value1']    =  $result_err[0]['clientname']; 
                if($result[$key]['Value1']==0){
                    unset($result[$key]);
                }
            }
        }
    }

    $result = array_values($result);


    return $result;

}

public function shiftwiseClient($shift = '')
{
    $empData      = $this->input->cookie();
    $grade        = $empData['Grade'];
    $searchKey    = $this->input->get('searchKey');
    $vert = "";
    $cnd="";
    if(isset($user_data['VertId']) && count($empData['VertId']) > 1)
    {
        $vert = $empData['Vert'];
    }
    $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : $vert;

    $status       = true;

    if($empData['Grade']>2 && ($empData['Grade']<5)){

        $cnd= " AND c.client_id IN (".$empData['ClientId'].")";
    }
    if (isset($searchKey) && $searchKey != 'empty') 
    {
        $cnd.=  " AND `client_name` LIKE '%" . $searchKey . "%'";
    }
    $sql_client="SELECT `c`.`client_name` as Name, `c`.`client_id` FROM `client` `c` WHERE `c`.`status` = 1  $cnd GROUP BY `c`.`client_id` ORDER BY `client_name` ASC  ";
//echo $sql_client;exit;
    $query = $this->db->query($sql_client);
    $result=$query->result_array();
// echo'<pre>';print_r($result);exit;
    foreach ($result as $key => $val) 
    {
        $cid               = $val['client_id'];

        $sql = "SELECT  ShiftwiseTable.`shift_code` AS `Name`, sum(ShiftwiseTable.shift_id) as shift_id ";
        if ($status) 
        {
            if($grade < 3)
            {
                $sql .= "FROM (SELECT DISTINCT client.client_id, shift.shift_code, shift.shift_id,client_contacts.contact_vertical 
                FROM client 
                JOIN client_contacts ON (client.client_id = client_contacts.client_id)
                JOIN employee_client ON (client_contacts.client_id = employee_client.client_id)
                JOIN employees ON (employee_client.employee_id = employees.employee_id)
                JOIN shift ON (employees.`shift_id` = shift.shift_id)
                WHERE grades < 3)";
            } 
            else 
            {
//$wherecnd
                $sql .= "FROM (SELECT DISTINCT client.client_id, shift.shift_code, shift.shift_id,client_contacts.contact_vertical 
                FROM client 
                JOIN client_contacts ON (client.client_id = client_contacts.client_id)
                JOIN employee_client ON (client_contacts.client_id = employee_client.client_id)
                JOIN employees ON (employee_client.employee_id = employees.employee_id)
                JOIN shift ON (employees.`shift_id` = shift.shift_id) where client.client_id=".$cid;

                if($grade>2 && $grade <5){
                    $sql .= " AND client.client_id IN (".$empData['ClientId'].")";
                }
                $sql_shift1=$sql." AND shift.shift_id=1";
                $sql_shift1 .=")";
                $sql_shift2=$sql." AND shift.shift_id=2";
                $sql_shift2 .=")";
                $sql_shift3=$sql." AND shift.shift_id=3";
                $sql_shift3 .=")";
                $sql_shift4=$sql." AND shift.shift_id=4";
                $sql_shift4 .=")";
                $sql_shift5=$sql." AND shift.shift_id=5";
                $sql_shift5 .=")";
                $sql_shift6=$sql." AND shift.shift_id=6";
                $sql_shift6 .=")";
                $sql_shift7=$sql." AND shift.shift_id=7";
                $sql_shift7 .=")";
            }

            $sql_shift="";
            $sql_shift .= " AS ShiftwiseTable ";
            $sql_shift1.= $sql_shift;
            $sql_shift2.= $sql_shift;
            $sql_shift3.= $sql_shift;
            $sql_shift4.= $sql_shift;
            $sql_shift5.= $sql_shift;
            $sql_shift6.= $sql_shift;
            $sql_shift7.= $sql_shift;

            $query_shift1 = $this->db->query($sql_shift1);

            $result_shift1=$query_shift1->result_array();

            $query_shift2 = $this->db->query($sql_shift2);
            $result_shift2=$query_shift2->result_array();

            $query_shift3 = $this->db->query($sql_shift3);
            $result_shift3=$query_shift3->result_array();

            $query_shift4 = $this->db->query($sql_shift4);
            $result_shift4=$query_shift4->result_array();

            $query_shift5 = $this->db->query($sql_shift5);
            $result_shift5=$query_shift5->result_array();

            $query_shift6 = $this->db->query($sql_shift6);
            $result_shift6=$query_shift6->result_array();

            $query_shift7 = $this->db->query($sql_shift7);
            $result_shift7=$query_shift7->result_array();

            $result[$key]['Value1']   =  $result_shift1 ? $result_shift1[0]['shift_id'] : 0 ;  
            $result[$key]['Value2']   =  $result_shift2 ? $result_shift2[0]['shift_id'] : 0 ; 
            $result[$key]['Value3']   =  $result_shift3 ? $result_shift3[0]['shift_id'] : 0 ; 
            $result[$key]['Value4']   =  $result_shift4? $result_shift4[0]['shift_id'] : 0 ; 
            $result[$key]['Value5']   =  $result_shift5 ? $result_shift5[0]['shift_id'] : 0 ; 
            $result[$key]['Value6']   =  $result_shift6 ? $result_shift6[0]['shift_id'] : 0 ;  
            $result[$key]['Value7']   =  $result_shift7 ? $result_shift7[0]['shift_id'] : 0 ; 
//echo '<pre>';print_r($result);
        } 
    }
//echo '<pre>';print_r($result);exit;
    $result = array(
        'rows' => $result,
        'totalCount' => $query->num_rows()
    );

    return $result;

}

function getShiftClientDetails($shift, $loggedInempId)
{
    $options = array();
    if ($this->input->get('hasNoLimit') != '1') {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }

    $sql = $this->__shiftWiseClient_query($shift);
    $sql .= "JOIN `client` ON (ShiftwiseTable.client_id = `client`.client_id) ";
    $sql .= "AND ShiftwiseTable.shift_id=$shift ";
    $totQuery = $this->db->query($sql);
    $totCount = $totQuery->num_rows();

    $sort = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    if (isset($options['sortBy'])) {
        $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
        $sql .= " ORDER BY " . $sort;
    } else
    $sql .= " ORDER BY client.Client_Name ";

    if (isset($options['limit']) && isset($options['offset'])) {
        $limit = $options['offset'] . ',' . $options['limit'];
        $sql .= " LIMIT " . $limit;
    } else if (isset($options['limit']))
    $sql .= " LIMIT " . $options['limit'];

    $query = $this->db->query($sql);
    if ($query->num_rows() == 0)
        return FALSE;
    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}

public function ExeaccuracyDetails($ClientID)
{
    $ClientID = str_replace("'", "", $ClientID);

    $monday     = array();
    $temp_array = array();
    $monday[]   = date('Y-m-d', strtotime('last monday', strtotime('tomorrow')));
    $monday[]   = date('Y-m-d', strtotime('last monday', strtotime($monday[0])));
    $monday[]   = date('Y-m-d', strtotime('last monday', strtotime($monday[1])));
    $cond       = 0;
    for ($i = 0; $i < 3; $i++) 
    {
        $sql = "SELECT DISTINCT accuracy as Value1, accuracy_week AS Name FROM accuracy WHERE accuracy_week='" . $monday[$i] . "' ";
        if ($ClientID != "")
        {
            $sql = " AND client_id IN($ClientID)";
        }

        $query     = $this->db->query($sql);
        $res_array = $query->result_array();
        if ($query->num_rows() == 0) 
        {
            $res = 0;
            $cond++;
        } 
        else 
        {
            $res = $res_array[0]['Value1'];
        }
        $temp_array[$i] = array(
            "Name" => date('d-m-Y', strtotime($monday[$i])),
            "Value1" => $res
        );
    }
    if ($cond == 3)
        return FALSE;
    return array_reverse($temp_array);
}


public function accuracyDetails($ClientID = '',$groupvertical='')
{
    $clientArray2 =array();
    if($ClientID !='')
    {
        $select_query = "SELECT DISTINCT(client_id) FROM client_contacts WHERE vertical_id IN (".$groupvertical.") ";
        $result_select = $this->db->query($select_query);
        if (count($result_select->result_array())!=0)
        {
            $processList = array();
            foreach($result_select->result_array() as $key => $val)
            {
                array_push($processList, $val['ProcessID']);
            }
        }
    }
    $this->db->select("DISTINCT client_id, AVG(accuracy) AS Accuracy, DATE_FORMAT(accuracy_week,'%b') AS MONTH", false);
    $this->db->from("accuracy");

    $dateRange = $this->dateRange('11');
    $StartDate = $dateRange['StartDate'];
    $EndDate   = date('Y-m-d');
    $this->db->where('accuracy_week >=', $StartDate);
    $this->db->where('accuracy_week <=', $EndDate);
    /**** @End *********/

    if ($ClientID != "")
    {
        $this->db->where_in("client_id", $ClientID);
    }

    /*** Changed for Dashboard Hierachy Report Graph***/
    if(is_array($clientArray2) && count($clientArray2) != 0)
        $this->db->where_in("client_id", $clientArray2);
    else
    {
        $clientArray = $this->getClients();
        if (is_array($clientArray) && count($clientArray) != 0)
            $this->db->where_in("client_id", $clientArray);
    }
    $this->db->group_by("MONTH(accuracy_week)");
    $query = $this->db->get();
// $this->db->last_query();exit;
    if ($query->num_rows() == 0)
        return FALSE;

    return $query->result_array();
}

//added to get verticalwise employee details
function getGradePodEmployeeDetails($Empgrade, $loggedInempId, $PodId = 0)
{
    $options        = array();
    $employeeArray  = "";
    $empData        = $this->input->cookie();
    $grade          = $empData['Grade'];
// print_r($Empgrade);
    $pod="";
    if(isset($user_data['PodId']) && count($empData['PodId']) > 1)
    {
        $pod = $empData['pod_id'];
    }
    $grouppod  = $this->input->get('grouppod') ? $this->input->get('grouppod') : $pod;
    $PodId = ($this->input->get('PodId') != '')? implode("','",json_decode($this->input->get('PodId'))) : '';
    $PodId = str_replace("'", "", $PodId);
//echo "<pre>"; print_r($VertId);
    $podhiearchy   = "";
    $cond           = "";

    if ($Empgrade == '3.1')
        $cond = " AND `designation`.`grades` BETWEEN 3 AND 3.1 AND `designation`.grades != 3 ";
    else
        $cond = "  AND `designation`.`grades` = $Empgrade ";

    if ($this->input->get('hasNoLimit') != '1') 
    {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }
    if($grouppod != '') 
    {
        $this->load->model('Pod_Model');
        $Pods = $this->Pod_Model->getPodsForParentID($grouppod);
        if($Pods   != '')
        {
            $podhiearchy = " AND tbl_rhythm_pod.pod_id IN ($Pods)";
        }
    }

    $sort                     = json_decode($this->input->get('sort'), true);
    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    $sql = "SELECT DISTINCT(`employees`.`employee_id`), company_employ_id, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName, 
    `designation`.`Name` AS DesignationName, GROUP_CONCAT(DISTINCT client.client_name) AS Client_Name , GROUP_CONCAT(DISTINCT tbl_rhythm_pod.pod_name) AS Pod_Name, `designation`.`hcm_grade` as Grade
    FROM `employees`
    LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id
    LEFT JOIN designation ON `employees`.`designation_id` = designation.`designation_id` 
    LEFT JOIN employee_pod ON employee_pod.employee_id = employees.employee_id 
    LEFT JOIN tbl_rhythm_pod ON tbl_rhythm_pod.pod_id = employee_pod.pod_id 
    WHERE `designation`.`grades` != 0 AND `employees`.location_id!=4 AND `employees`.status IN(1,2,3,4,5,7)  $cond";
    if($empData['Grade']>2 && ($empData['Grade']<5))
    {
        if (is_array($this->getEmployeeList($empData['employee_id'])))
            $employeeArray = implode(',', $this->getEmployeeList($empData['employee_id']));
        if ($employeeArray != '') 
        {
            $sql .= " AND `employees`.`employee_id` IN($employeeArray)";
        }
    }

    if($grouppod!='') 
    {
        $this->load->model('Pod_Model');
        $Pods = $this->Pod_Model->getPodsForParentID($grouppod); 
// if($Verticals!='')
// $sql .= " AND verticals.vertical_id IN ($Verticals)";
    }
    if ($VertId != "") 
    {
        $sql .= " AND tbl_rhythm_pod.pod_id IN($PodId)";
    }
    else if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['PodId']!="")){
        $sql .= " AND tbl_rhythm_pod.pod_id IN (".$empData['PodId'].")";
    }

    if (isset($options['sortBy'])) 
    {
        $orderBy = "";
//$sort = 'LPAD('.$options['sortBy'] . ', 20, "0") ' . $options['sortDirection'];
        if ($options['sortBy'] == "company_employ_id")
            $orderBy .= ' ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ' . $options['sortDirection'];
        if ($options['sortBy'] == "FirstName")
            $orderBy .= ' ORDER BY FirstName ' . $options['sortDirection'];
        if ($options['sortBy'] == "DesignationName")
            $orderBy .= ' ORDER BY DesignationName ' . $options['sortDirection'];
        if ($options['sortBy'] == "Client_Name")
            $orderBy .= ' ORDER BY Client_Name ' . $options['sortDirection'];
        if ($options['sortBy'] == "Grade")
            $orderBy .= ' ORDER BY Grade ' . $options['sortDirection'];
        $sql .=" GROUP BY `employees`.`employee_id` " . $orderBy;
    } 

    else
        $sql .= " GROUP BY `employees`.`employee_id` ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ASC ";
    $query    = $this->db->query($sql, false);
    $totCount = $query->num_rows();

    if (isset($options['limit']) && isset($options['offset'])) 
    {
        $limit = $options['offset'] . ',' . $options['limit'];
        $sql .= " LIMIT " . $limit;
    } 
    else if (isset($options['limit'])) 
    {
        $sql .= " LIMIT " . $options['limit'];
    }
// echo $sql; exit;
    $query = $this->db->query($sql);

    if ($query->num_rows() == 0)
        return FALSE;
    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}

function getGradeEmployeeDetails($Empgrade, $loggedInempId)
{
    $options        = array();
    $employeeArray  = "";
    $empData        = $this->input->cookie();
    $grade          = $empData['Grade'];
// print_r($Empgrade);
    $vert="";
    if(isset($user_data['VertId']) && count($empData['VertId']) > 1)
    {
        $vert = $empData['Vert'];
    }
    $groupvertical  = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : $vert;
    $ClientId = ($this->input->get('clientID') != '')? implode("','",json_decode($this->input->get('clientID'))) : '';
    $ClientId = str_replace("'", "", $ClientId);


    $verthiearchy   = "";
    $cond           = "";

    if ($Empgrade == '3.1')
        $cond = " AND `designation`.`grades` BETWEEN 3 AND 3.1 AND `designation`.grades != 3 ";
    else
        $cond = "  AND `designation`.`grades` = $Empgrade ";

    if ($this->input->get('hasNoLimit') != '1') 
    {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }
    if($groupvertical != '') 
    {
        $this->load->model('Vertical_Model');
        $Verticals = $this->Vertical_Model->getVerticalsForParentID($groupvertical);
        if($Verticals   != '')
        {
            $verthiearchy = " AND verticals.vertical_id IN ($Verticals)";
        }
    }

    $sort                     = json_decode($this->input->get('sort'), true);
    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    $sql = "SELECT DISTINCT(`employees`.`employee_id`), company_employ_id, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName, 
    `designation`.`Name` AS DesignationName, GROUP_CONCAT(DISTINCT client.client_name) AS Client_Name ,`designation`.`hcm_grade` as Grade
    FROM `employees`
    LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id
    LEFT JOIN designation ON `employees`.`designation_id` = designation.`designation_id` 
    LEFT JOIN `employee_vertical` ON employee_vertical.`employee_id` = employees.`employee_id`
    LEFT JOIN `verticals` ON verticals.`vertical_id` = employee_vertical.`vertical_id`
    WHERE `designation`.`grades` != 0 AND `employees`.status IN(1,2,3,4,5,7)  $cond";
    if (is_array($this->getEmployeeList($empData['employee_id'])))
        $employeeArray = implode(',', $this->getEmployeeList($empData['employee_id']));
    if ($employeeArray != '') 
    {
        $sql .= " AND `employees`.`employee_id` IN($employeeArray)";
    }

    if($groupvertical!='') 
    {
        $this->load->model('Vertical_Model');
        $Verticals = $this->Vertical_Model->getVerticalsForParentID($groupvertical); 
// if($Verticals!='')
// $sql .= " AND verticals.vertical_id IN ($Verticals)";
    }
    if ($ClientId != "") 
    {
        $sql .= "AND client.client_id IN($ClientId)";
    }
    if($empData['Grade']>2 && ($empData['Grade']<5) && ($empData['ClientId']!="")){
        $sql .= " AND client.client_id IN (".$empData['ClientId'].")";
    }

    if (isset($options['sortBy'])) 
    {
        $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
        if ($options['sortBy'] == "CompanyEmployID")
            $sort = 'CID ' . $options['sortDirection'];
        $sql .= " GROUP BY `employees`.`employee_id` ORDER BY " . $sort;
    } 
    else
        $sql .= " GROUP BY `employees`.`employee_id` ORDER BY `designation`.grades ";
    $query    = $this->db->query($sql, false);
    $totCount = $query->num_rows();

    if (isset($options['limit']) && isset($options['offset'])) 
    {
        $limit = $options['offset'] . ',' . $options['limit'];
        $sql .= " LIMIT " . $limit;
    } 
    else if (isset($options['limit'])) 
    {
        $sql .= " LIMIT " . $options['limit'];
    }
// echo $sql; exit;
    $query = $this->db->query($sql);

    if ($query->num_rows() == 0)
        return FALSE;
    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}


function getEmpLocationDetails($location, $loggedInempId, $PodId = 0, $clientID = 0)
{
//$VertId = ($VertId != '')? implode("','",json_decode($VertId)) : '0';
//$clientID = ($clientID != '')? implode("','",json_decode($clientID)) : '0';
//$VertId = ($this->input->get('VertId') != '')? implode("','",json_decode($this->input->get('VertId'))) : '';
//$VertId = str_replace("'", "", $VertId);

    $options = array();
    if ($this->input->get('hasNoLimit') != '1') 
    {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }

    $sql = $this->__locationCount_query($PodId, $location, $clientID);
    $sql .= "AND location.`location_id`=$location ";

    $sort                     = json_decode($this->input->get('sort'), true);
    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    if (isset($options['sortBy'])) 
    {
        $orderBy = "";
//$sort = 'LPAD('.$options['sortBy'] . ', 20, "0") ' . $options['sortDirection'];
        if ($options['sortBy'] == "company_employ_id")
            $orderBy .= ' ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ' . $options['sortDirection'];
        if ($options['sortBy'] == "FirstName")
            $orderBy .= ' ORDER BY FirstName ' . $options['sortDirection'];
        if ($options['sortBy'] == "DesignationName")
            $orderBy .= ' ORDER BY DesignationName ' . $options['sortDirection'];
        if ($options['sortBy'] == "Client_Name")
            $orderBy .= ' ORDER BY Client_Name ' . $options['sortDirection'];
        $sql .= "GROUP BY employees.`employee_id` $orderBy ";
    }

    else
        $sql .= "GROUP BY employees.`employee_id` ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ASC ";

//echo $sql; exit;
    $totQuery = $this->db->query($sql);
    $totCount = $totQuery->num_rows();


    if (isset($options['limit']) && isset($options['offset']))
        $sql .= 'LIMIT ' . $options['offset'] . ',' . $options['limit'];
    else if (isset($options['limit']))
        $sql .= " LIMIT " . $options['limit'];

    $query = $this->db->query($sql);
    if ($query->num_rows() == 0)
        return FALSE;
    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}


function getShiftEmployeeDetails($shift, $loggedInempId, $clientID = '', $PodId = 0)
{
    $options = array();

// $clientID = ($clientID != '')? implode("','",json_decode($clientID)) : '';

    if ($this->input->get('hasNoLimit') != '1') {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }

    $sql = $this->__getShiftCount_query($shift, $PodId, $clientID);
    $sql .= "JOIN `designation` ON `designation`.designation_id = `ShiftwiseTable`.designation_id ";
    $sql .= "AND ShiftwiseTable.shift_id = $shift GROUP BY ShiftwiseTable.`employee_id`";

    $totQuery = $this->db->query($sql);
    $totCount = $totQuery->num_rows();

    $sort = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];


    if (isset($options['sortBy'])) 
    {
        $orderBy = "";
//$sort = 'LPAD('.$options['sortBy'] . ', 20, "0") ' . $options['sortDirection'];
        if ($options['sortBy'] == "company_employ_id")
            $orderBy .= ' ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ' . $options['sortDirection'];
        if ($options['sortBy'] == "FirstName")
            $orderBy .= ' ORDER BY FirstName ' . $options['sortDirection'];
        if ($options['sortBy'] == "DesignationName")
            $orderBy .= ' ORDER BY DesignationName ' . $options['sortDirection'];
        if ($options['sortBy'] == "Client_Name")
            $orderBy .= ' ORDER BY Client_Name ' . $options['sortDirection'];
        $sql .= $orderBy;
    } 
    else
        $sql .= " ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ASC ";

    if (isset($options['limit']) && isset($options['offset'])) 
    {
        $limit = $options['offset'] . ',' . $options['limit'];
        $sql .= " LIMIT " . $limit;
    } 
    else if (isset($options['limit']))
        $sql .= " LIMIT " . $options['limit'];
// echo $sql;exit;
    $query = $this->db->query($sql);

    if ($query->num_rows() == 0)
        return FALSE;
    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}


/* My reportee Billable */
public function getMyRepBillable()
{
    $Range                    = ($this->input->get('Range')) ? $this->input->get('Range') : 0;
    $sort                     = json_decode($this->input->get('sort'), true);
    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];
    $empData                  = $this->input->cookie();
    $grade                    = $empData['Grade'];
    $EmployID                 = $empData['employee_id'];
    $cond                     = "";

    if ($grade >= 5) 
    {
        $cond = " AND designation.grades >= 4 AND designation.grades < 7";
    }
    if ($grade == 4) 
    {
$cond = " "; //" AND designation.grades = 3 ";
}
if ($grade == 3) 
{
    $cond = "AND designation.grades IN(1,2) ";
}
if ($Range != 0) 
{
    return $BillableResult = $this->getPreviousBillablityData($grade, $EmployID, $Range, $billable = 1);
} 
else 
{
    if($grade >= 7)
    {
        $EmployID = '1296';
    }

    $sql = "SELECT DISTINCT(employees.employee_id) AS EmployID,GROUP_CONCAT(DISTINCT(client.client_id)) AS ClientID,
    designation.grades,CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Name 
    FROM employees
    JOIN designation ON employees.designation_id = designation.designation_id 
    JOIN employee_client 
    JOIN client ON employee_client.client_id = client.client_id 
    WHERE employees.status IN (3,5) AND employees.primary_lead_id = $EmployID $cond 
    GROUP BY employees.employee_id
    ORDER BY first_name";
    $result         = $this->db->query($sql);
    $BillableResult = $result->result_array();
    if ($result->num_rows() >= 1) 
    {
        foreach ($BillableResult as $key => $value) 
        {
            $reporteesbill = $this->getMyReporteeBillablePerc($value['processID'], $value['grades'], $value['EmployID']);

            if (!empty($reporteesbill)) 
            {
                $BillableResult[$key]['Billable'] = $reporteesbill['Billable'];

                $BillableResult[$key]['Value1']       = $reporteesbill['Percentage']." (".$reporteesbill['Billable'].")";
                $BillableResult[$key]['Value3']       = $reporteesbill['BufferPercentage']." (".$reporteesbill['Buffer'].")";
                $BillableResult[$key]['Value4']       = $reporteesbill['Utilization'];
                $BillableResult[$key]['Value5']       = $reporteesbill['BufferNewValue'];
                $BillableResult[$key]['ToolTipValue'] = '';
                if ($BillableResult[$key]['Value1'] == '' || $BillableResult[$key]['Value1'] == 0) 
                {
                    $BillableResult[$key]['Value1']       = 0;
                    $BillableResult[$key]['ToolTipValue'] = '@@@';
                }
            } 
            else 
            {
                $BillableResult[$key]['Billable'] = 0;
                $BillableResult[$key]['Value1'] = 0;
                $BillableResult[$key]['Value2'] = 0;
                $BillableResult[$key]['Value3'] = 0;
                $BillableResult[$key]['Value4'] = 0;
                $BillableResult[$key]['Value5'] = 0;
            }
        }
        return $BillableResult;
    } 
    else
        return '';
}
exit;
}


/**
* @description : dashboard - carousel function
*/
function dashboardTablesGridOne()
{
    $user_data = $this->input->cookie();
    $grade     = $user_data['Grade'];
    $empId     = $user_data['employee_id'];
    $count     = '';

    $NoOfEmployees = $NoOfErrorsLastWeek = $NoOfEscalationLastWeek = $NoOfAppreciationsLastWeek = $Billable = $NonIndiaBillable = $Non_Billable = $LeaderShip = $rmgsize= 0;

//Count No of employees
    $NoOfEmployees = $this->getNoOfEmployees($empId, $grade);

//billable employees count
    $Billable = $this->get_billable($empId, $grade);

//non india billable employees count
    $sql = $this->getBillableNonIndianCount();
    $totQuery = $this->db->query($sql);
    $NonIndiaBillable = $totQuery->num_rows();

//non billable employees count
    $Non_Billable = $this->getNon_billable($empId, $grade);

//RMG count
    $rmgsize = $this->get_hrpoolsize();

//Appreciations
    $NoOfAppreciationsLastWeek = $this->GetApprLastWeekCount();

//Escalation
    $NoOfEscalationLastWeek = $this->GetEscLastWeekCount();

//Errors
    $NoOfErrorsLastWeek =$this->GetErrLastWeekCount();

//Leadership
    $sql        = $this->__leaderShip($clientId = "", '');
    $LeaderShip = $this->db->query($sql, false);
    $LeaderShip = $LeaderShip->num_rows();

    $data  = array(
        array(
            'name1' 	=> 'Employees',
            'value1' 	=> 'Billable-India',
            'name5' 	=> 'Billable-Non India',
            'name2' 	=> 'Non-Billable',
            'value2' 	=> 'Leadership',
            'name3'		=> 'RMG',
            'value3' 	=> 'Appreciations',
            'name4' 	=> 'Escalations',
            'value4' 	=> 'Error'
        ),
        array(
            'name1' 	=> $NoOfEmployees,
            'value1' 	=> $Billable,
            'name5' 	=> $NonIndiaBillable,
            'name2' 	=> $Non_Billable,
            'value2' 	=> $LeaderShip,
            'name3'		=> $rmgsize,     
            'value3' 	=> $NoOfAppreciationsLastWeek,
            'name4' 	=> $NoOfEscalationLastWeek,
            'value4' 	=> $NoOfErrorsLastWeek
        )
    );
    $count = count($data);

    $total = $count;
// you can pre-define the required property parameters
    $output["metaData"]["idProperty"]      = "id";
    $output["metaData"]["totalProperty"]   = "total";
    $output["metaData"]["successProperty"] = "success";
    $output["metaData"]["root"]            = "data";

// you can parse field values via your database schema
    $output["metaData"]["fields"][] = array(
        "name" => "id",
        "type" => "int"
    );

    for ($i = 1; $i <= 5; $i++) 
    {
        $output["metaData"]["fields"][] = "name$i";
        $output["metaData"]["fields"][] = "value$i";
    }

// you can parse column values via your database schema
    $output["columns"][] = array(
        "dataIndex" => "name1",
        "header" => "Title",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );
    $output["columns"][] = array(
        "dataIndex" => "value1",
        "header" => "Value",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );
    $output["columns"][] = array(
        "dataIndex" => "name5",
        "header" => "Value",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );
    $output["columns"][] = array(
        "dataIndex" => "name2",
        "header" => "Title",
        "tdCls" => "wrap",
        "align" => "center"
    );
    $output["columns"][] = array(
        "dataIndex" => "value2",
        "header" => "Value",
        "tdCls" => "wrap",
        "align" => "center"
    );
    $output["columns"][] = array(
        "dataIndex" => "name3",
        "header" => "Title",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );
    $output["columns"][] = array(
        "dataIndex" => "value3",
        "header" => "Value",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );
    $output["columns"][] = array(
        "dataIndex" => "name4",
        "header" => "Value",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );
    $output["columns"][] = array(
        "dataIndex" => "value4",
        "header" => "Value",
        "tdCls" => "wrap",
        "align" => "center",
        "width" => "30px"
    );

// the misc properties
    $output["total"]     = $total;
    $output["success"]   = true;
    $output["message"]   = "success";

// parse pages
    $start = $_GET['start'] + 1;
    $max   = $_GET['start'] + $_GET['limit'];

// make sample data
    for ($i = $start; $i <= $count; $i++) 
    {
        $output["data"][] = array(
            "id" => $i,
            "name1" => $data[$i - 1]["name1"],
            "value1" => $data[$i - 1]["value1"],
            "name5" => $data[$i - 1]["name5"],
            "name2" => $data[$i - 1]["name2"],
            "value2" => $data[$i - 1]["value2"],
            "name3" => $data[$i - 1]["name3"],
            "value3" => $data[$i - 1]["value3"],
            "name4" => $data[$i - 1]["name4"],
            "value4" => $data[$i - 1]["value4"]
        );
    }

    return $output;
}


/**
* @description: Get total Count of Employees
* @return : int
*/
public function getNoOfEmployees($empId, $grade)
{
    $NoOfEmployees = 0;
    $empData = $this->input->cookie();
    $sql = "SELECT COUNT(DISTINCT(employees.employee_id)) AS NoOfEmployees FROM employees 
    JOIN designation ON employees.designation_id = designation.designation_id 
    WHERE employees.status !=6";
    if (is_array($this->getEmployeeList($empId)))
    {
        $employeeArray = implode(',', $this->getEmployeeList($empId));
    }
    $employeeArray = ($employeeArray!="") ? ($employeeArray.",".$empData['employee_id']) : $employeeArray;  

    if($grade>2 && $grade<5 && $employeeArray!="")
    {
        $sql .= ' AND employees.employee_id IN ('.$employeeArray.')';
    }
    else if($grade<5)
    {
        $sql .= ' AND employees.primary_lead_id IN ('.$empData['employee_id'].')';
    }

    $query = $this->db->query($sql);
    $result = $query->row_array();

    if (!empty($result['NoOfEmployees'])) 
    {
        $NoOfEmployees = $result['NoOfEmployees'];
    }

    return $NoOfEmployees;
}


/**
* @description: Get total no of billable Employees
* @return : int
*/
function get_billable($empId, $grade)
{
    $empData    = $this->input->cookie();

    $sql = "SELECT DISTINCT employees.company_employ_id, designation.name AS DesignationName,CONCAT_WS(' ',employees.first_name,employees.last_name) AS FirstName, GROUP_CONCAT( DISTINCT client.client_name) AS Client_Name 
    FROM employees 
    LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    LEFT JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id 
    WHERE employee_client.billable = 1 AND employees.status != 6 AND designation.grades < 5 ";

    if (is_array($this->getEmployeeList($empId)))
        $employeeArray = implode(',', $this->getEmployeeList($empId));

    if($grade>2 && $grade<5 && $employeeArray!="")
    {
        $sql .= ' AND employees.employee_id IN ('.$employeeArray.')';
    }
    else if($grade<5)
    {
        $sql .= ' AND employees.primary_lead_id IN ('.$empData['employee_id'].')';
    }
    $sql .= " GROUP BY employees.employee_id";

    $query = $this->db->query($sql);
    $result = $query->num_rows();

    return $result;
}

/**
* @description: Get total no of non india billable Employees
* @return : int
*/
function get_nonIndiaBillable($empId)
{
    $empData    = $this->input->cookie();

    $sql = "SELECT DISTINCT employees.company_employ_id, designation.name AS DesignationName,CONCAT_WS(' ',employees.first_name,employees.last_name) AS FirstName, GROUP_CONCAT( DISTINCT client.client_name) AS Client_Name 
    FROM employees 
    LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    LEFT JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id 
    WHERE employee_client.billable = 1 AND employees.status != 6 AND location_id NOT IN(1,2)";

    $sql .= " GROUP BY employees.employee_id";

    $query = $this->db->query($sql);
    $result = $query->num_rows();

    return $result;
}


/**
* @description: Get total no of non-billable Employees
* @return : int
*/
function getNon_billable($empId, $grade)
{
    $empData    = $this->input->cookie();

    $sql = "SELECT DISTINCT employees.company_employ_id, designation.name AS DesignationName,CONCAT_WS(' ',employees.first_name,employees.last_name) AS FirstName, GROUP_CONCAT( DISTINCT client.client_name) AS Client_Name 
    FROM employees 
    LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
    LEFT JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
    LEFT JOIN `client` ON `employee_client`.client_id = `client`.client_id 
    WHERE employees.status !=6 AND designation.grades < 3 AND employees.employee_id NOT IN(SELECT DISTINCT employee_id FROM employee_client WHERE billable=1 AND employee_client.grades < 3 ) ";

    if (is_array($this->getEmployeeList($empId)))
        $employeeArray = implode(',', $this->getEmployeeList($empId));

    if($grade>2 && $grade<5 && $employeeArray!="")
    {
        $sql .= '  AND employees.employee_id IN ('.$employeeArray.')';
    }
    else if($grade<5)
    {
        $sql .= ' AND employees.primary_lead_id IN ('.$empData['employee_id'].')';
    }
    $sql .= " GROUP BY employees.employee_id";

    $query = $this->db->query($sql);
    $result = $query->num_rows();

    return $result;
}


/**
* @description: Get total no of RMG Employees
* @return : int
*/
public function get_hrpoolsize()
{
    $sql = "SELECT * FROM employees WHERE STATUS IN (2,4)";
    $query = $this->db->query($sql);
    $result = $query->num_rows();

    return $result;
}


/**
* @description: Get total no of leadership Employees
* @return : int
*/
public function get_Leadership()
{
    $empData    = $this->input->cookie();

    $sql = "SELECT DISTINCT Leadership.`employee_id`, CAST(SUBSTRING_INDEX(e1.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, 
    e1.company_employ_id,d1.name as DesignationName,CONCAT_WS(' ',e1.first_name,e1.last_name) as FirstName 
    FROM ( (SELECT e.`employee_id` FROM employees e JOIN `designation` d ON e.`designation_id` = d.`designation_id` 
    WHERE d.`grades` >= 5 AND d.`grades` < 7 ) 
    UNION (SELECT t.`employee_id` FROM `employee_client` t WHERE t.`billable` = 0 AND t.grades > 2 AND t.employee_id NOT IN (SELECT employee_id FROM employee_client WHERE billable = 1 AND employee_client.grades> 2 )) ) AS Leadership 
    JOIN employees e1 ON Leadership.`employee_id` = e1.`employee_id` 
    JOIN `designation` d1 ON e1.`designation_id` = d1.`designation_id` and d1.grades < 7 and e1.`status` NOT IN(6) and e1.`employee_id`!=1296";
    $query = $this->db->query($sql);
    $result = $query->num_rows();

    return $result;
}


/**
* @description: Get total no of appreciation from current year to till date
* @return : int
*/
public function GetApprLastWeekCount()
{

    $TotalCount = 0;
    $sql = "SELECT COUNT(*) AS No_of_LogType FROM `incidentlog_appreciations` WHERE  YEAR(appr_date) = YEAR( NOW() ) AND status = 0";

//echo $sql;exit;
    $query = $this->db->query($sql);

    $result = $query->row_array();

    if (!empty($result['No_of_LogType'])) 
    {
        $TotalCount = $result['No_of_LogType'];
    }

    return $TotalCount;
}


/**
* @description: Get total no of escalation from current year to till date
* @return : int
*/
public function GetEscLastWeekCount()
{
    $TotalCount = 0;
    $sql = "SELECT COUNT(*) AS No_of_LogType FROM `incidentlog_escalation` WHERE YEAR(escalation_date) = YEAR( NOW() ) AND status = 0";
    $query = $this->db->query($sql);
    $result = $query->row_array();

    if (!empty($result['No_of_LogType'])) 
    {
        $TotalCount = $result['No_of_LogType'];
    }

    return $TotalCount;
}


/**
* @description: Get total no of external error from current year to till date
* @return : int
*/
public function GetErrLastWeekCount()
{
    $TotalCount = 0;
    $sql = "SELECT COUNT(*) AS No_of_LogType FROM `external_errors` WHERE YEAR(error_date) = YEAR( NOW() ) AND status = 0";
    $query = $this->db->query($sql);
    $result = $query->row_array();

    if (!empty($result['No_of_LogType'])) 
    {
        $TotalCount = $result['No_of_LogType'];
    }

    return $TotalCount;
}


/******** Get Employees With VerticalID ***********/
public function getEmployees($PodID, $coulmnName)
{
    $options    = $data = array();
    $empData    = $this->input->cookie();
    $EmployID   = $empData['EmployID'];
    $grade      = $empData['Grade'];
    $flag       = false;
    $sql        = '';
    $totalCount = 0;
    $ProcessID  = $this->input->get('ProcessID');

    $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
    $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;

    $sort = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    if ($coulmnName == "Non-Billable") 
    {
        if (ONBOARD_PROCESS_ID == $ProcessID)
        {
            $sql = $this->getOnboardingLeaderShipEmployees($ProcessID, $coulmnName);
            $query = $this->db->query($sql);
            $totalCount = $query->num_rows();
            $data  = $query->result_array();
        }
        else 
        {
            $sql  = $this->__verticalWiseNonBillables_query($grade, $PodID, $ProcessID);
            $flag = true;
        }
    }
    if ($coulmnName == "Leadership") 
    {
        if (empty($ProcessID)) 
        {
            $sql = $this->__getLeaderShip($coulmnName, true);
            $sql .= "JOIN `client` c1 ON c1.ClientID = `Leadership`.ClientID ";
            $sql .= "AND Leadership.ProcessStatus = '0'";
            $sql .= "AND Leadership.verticalid =$PodID ";
            $sql .= "GROUP BY Leadership.`EmployID`";

// $sql .= "ORDER BY CID ASC ";
        } 
        else 
        {
            if (INI_PROCESS_ID == $ProcessID)
                $sql = $this->getHRPoolLeads($ProcessID);
            if (VVPROCESS_PROCESS_ID == $ProcessID)
                $sql = $this->getVVPRocessLeads($ProcessID);
if (ONBOARD_PROCESS_ID == $ProcessID) //Condition to display onBoarding Leadership employees in vertical wise
$sql = $this->getOnboardingLeaderShipEmployees($ProcessID, $coulmnName);
}
if ($sql) 
{
    $query      = $this->db->query($sql);
    $totalCount = $query->num_rows();

    if (isset($options['sortBy'])) {
        $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
        if ($options['sortBy'] == "CompanyEmployID")
            $sort = 'CID ' . $options['sortDirection'];
        $sql .= " ORDER BY " . $sort;
    } else
    $sql .= " ORDER BY CID ASC ";

    if (isset($options['limit']) && isset($options['offset']))
        $sql .= ' LIMIT ' . $options['offset'] . ',' . $options['limit'];

    $query = $this->db->query($sql);
    $data  = $query->result_array();
}

}
if ($coulmnName == "Billable") 
{
    $sql  = $this->__verticalWiseBillables_query($grade, $VerticalID, $ProcessID);
    $flag = true;
}

if ($flag) 
{
    $sql .= "JOIN `client` ON `client`.ClientID = `BillableTable`.ClientID ";
    $sql .= "JOIN `designation` ON `designation`.DesignationID = `BillableTable`.DesignationID ";
    $sql .= "JOIN `tbl_rhythm_pod` ON BillableTable.`VerticalID`=`vertical`.`VerticalID` ";
    $sql .= "AND BillableTable.verticalid =$VerticalID ";
}

if ($flag) 
{
    $query      = $this->db->query($sql);
    $totalCount = $query->num_rows();
}

if (isset($options['sortBy'])) 
{
    $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
    if ($options['sortBy'] == "CompanyEmployID")
        $sort = 'CID ' . $options['sortDirection'];
    $sql .= " ORDER BY " . $sort;
} 
else
    $sql .= " ORDER BY CID ASC ";

if ($flag) 
{
    if (isset($options['limit']) && isset($options['offset']))
        $sql .= ' LIMIT ' . $options['offset'] . ',' . $options['limit'];
    $query = $this->db->query($sql);
    $data  = $query->result_array();
}

$results = array_merge(array(
    'totalCount' => $totalCount
), array(
    'rows' => $data
));
return $results;
}

//VerticalHierarchy employee popup

public function getEmployeesForVhierarchy($VerticalID,$coulmnName,$chartname)
{
//print_r($VerticalID);exit;
    $options    = $data = array();
    $empData    = $this->input->cookie();
    $EmployID   = $empData['employee_id'];
    $grade      = $empData['Grade'];


    $flag       = false;
    $sql        = '';
    $totalCount = 0;
    $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
    $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;

    $sort = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];
    $flag = false;

    if($coulmnName == 'Billable' || ($coulmnName == 'Non-Billable'))
    {
        $sql  = $this->__getEmployeesForVerticalHierarchy($coulmnName,$VerticalID,$grade);        		       
    }



    if($coulmnName == 'Leadership')
    {
        $sql = $this->__getEmployeesForVerticalHierarchy($coulmnName,$VerticalID);

    }

    if($flag)
    {
        $sql .= "JOIN `client` ON `client`.ClientID = `BillableTable`.ClientID ";
        $sql .= "JOIN `designation` ON `designation`.DesignationID = `BillableTable`.DesignationID ";
        $sql .= "JOIN `vertical` ON BillableTable.`VerticalID`=`vertical`.`VerticalID` ";
        $sql .= "AND BillableTable.verticalid = $VerticalID ";
    }   
//echo $sql;
    $query      = $this->db->query($sql);
    $totalCount = $query->num_rows();    	
    if (isset($options['sortBy'])) 
    {
        $sort = $options['sortBy'] . ' ' . $options['sortDirection'];
        if ($options['sortBy'] == "CompanyEmployID")
        {            	
            $sort = 'CID ' . $options['sortDirection'];
        }
        $sql .= " ORDER BY " . $sort;
    } 
    else
    {
        $sql .= " ORDER BY CID ASC ";
    }

    if ($sql) 
    {
        if (isset($options['limit']) && isset($options['offset']))
            $sql .= ' LIMIT ' . $options['offset'] . ',' . $options['limit'];
        $query = $this->db->query($sql);
        $data  = $query->result_array();
    }

    $results = array_merge(array(
        'totalCount' => $totalCount
    ), array(
        'rows' => $data
    ));
    return $results;
}


//Query to return for pop up employees for Leadership,NonBillable,Billable
function __getEmployeesForVerticalHierarchy($columnname ,$verticalID,$grade = '')
{

    $empData     = $this->input->cookie();
    $processcon  = "";
    $hrcon = "";
    $topvertical = false;
    $vertical_id=$verticalID;
    switch ($empData['Grade']) {
        case 3:
        case '3.1':
        case 4:
        case 5: {
            $processArray = $this->getProcess();
            if (count($processArray) == 0){
                $processArray = '';
            }
            else
                $processArray = " AND Leadership.ProcessID IN(" . implode(",", $processArray) . ")  AND Leadership.`ProcessStatus` = 0";
        }
    }
    if(!empty($processArray)){
        $processcon = $processArray;
    }
    $loggedInVerticals = '';
    $lgVert = "";
    if(isset($empData['VertId']))
    {
        if($empData['Grade'] <6)
        {
            $loggedInVerticals = implode(',',$empData['VertId']);
            $lgVert = $empData['VertId'];
        }
    }
    $this->load->model('Vertical_Model');
//print_r($verticalID);exit;
    $vertids = $this->Vertical_Model->getVerticalsForParentID($verticalID);

    $con = "";
    $sqlhrpoolonborading = "";
    $chartpop = "";

    if($vertids != "")
    {
        $verticalID =  $vertids;
        $con .= " WHERE  X.verticalid IN( $verticalID ) ";
        $transition = " AND e1.TransitionStatus != 3 ";
    }
    else
    {
        $transition = "";
        $topvertical = true;	
    }
    if($loggedInVerticals != "")
    {
        if($topvertical)
            $verticalID = $loggedInVerticals;
        $con = " WHERE  X.verticalid IN( $verticalID ) ";
    }
    if($vertids != "")
    {
        $hrcon = " AND Leadership.ProcessID NOT IN(223)";
    }
    if($columnname == "Leadership")
    {
        $vertic="";
        $parent_verticals=array("20","21","22");
        if (in_array($vertical_id, $parent_verticals)){
            if($verticalID==20){
                $vertic= " ";
            }else{
                $vertic=" AND Leadership.vertical_id IN($vertids)";
            }
        }else{
            $vertic= " AND Leadership.vertical_id IN($vertical_id)";
        }

        $sql = " SELECT COUNT(temp1.employee_id) AS Value1,temp1.vertical_id,FirstName,CID ,Name,Client_Name,company_employ_id FROM 
        (SELECT DISTINCT Leadership.`employee_id`,Leadership.`vertical_id`,verticals.`Name` as vname, Client_Name , CONCAT_WS(' ',e1.first_name,e1.last_name) AS FirstName,
        CAST(SUBSTRING_INDEX(e1.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID,e1.company_employ_id as company_employ_id ,d1.name AS Name FROM 
        ((SELECT v.`employee_id`,client_vertical.`vertical_id`,client_vertical.`client_id`,NULL AS Client_Name   FROM 
        `verticalmanager` v JOIN client_vertical ON (v.vertical_id = client_vertical.vertical_id) ) 
        UNION (SELECT t.`employee_id`,client_vertical.`vertical_id`,client_vertical.`client_id`,client.client_name AS Client_Name 
        FROM `employee_client` t JOIN client ON (t.client_id = client.client_id) JOIN client_vertical 
        ON (client.client_id = client_vertical.client_id) WHERE t.`billable` = 0 AND grades > 2 
        AND t.employee_id NOT IN (SELECT employee_id FROM employee_client WHERE billable = 1 
        AND grades > 2 AND grades < 4) )) AS Leadership JOIN employees e1 ON Leadership.`employee_id` = e1.`employee_id` 
        JOIN `designation` d1 ON e1.`designation_id` = d1.`designation_id` AND d1.grades < 7 JOIN `verticals` 
        ON Leadership.`vertical_id`=`verticals`.`vertical_id` AND e1.`status` IN(3,5) AND e1.`employee_id`!=1296 $vertic
        GROUP BY Leadership.`employee_id`) temp1 GROUP BY temp1.vertical_id";
    }

    if($columnname == "Non-Billable")
    {
        $sqlBillable = $this->getBillableCountQuery($vertical_id);

        $billNonAry       = "";
        $vertmanager = '';
        $excludingProcess = 10; 
        if (isset($sqlBillable))
            $result_select = $this->db->query($sqlBillable);

        $empArray = array();
        if (isset($result_select) && count($result_select->result_array()) != 0) 
        {
            foreach ($result_select->result_array() as $key => $val)
                array_push($empArray, $val['empid']);
            $billNonAry = " AND employees.employee_id NOT IN (" . implode(",", $empArray) . ") ";
        }

        $sql="SELECT  DISTINCT employees.company_employ_id,temp.employee_id AS empid, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,c.client_name as Client_Name,designation.name as Name FROM client c 
        JOIN ( SELECT employee_id,client_id FROM employee_client WHERE billable = 0 AND employee_client.grades<3 ) AS temp ON temp.client_id = c.client_id
        JOIN employees ON employees.employee_id = temp.employee_id JOIN client_vertical cv ON c.client_id = cv.client_id
        JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
        JOIN `employee_vertical` ON employee_vertical.`employee_id` = employees.`employee_id`
        JOIN `verticals` ON verticals.`vertical_id` = employee_vertical.`vertical_id`   
        WHERE employees.`status` IN (1,2,3,4,5,7) AND c.status = 1 $billNonAry 

        ";


        $parent_verticals=array("20","21","22");
        if (in_array($vertical_id, $parent_verticals)){
            if($verticalID==20){
                $sql.= " AND verticals.parent_vertical_id IN($vertids)";
            }else{
                $sql.=" AND verticals.vertical_id IN($vertids)";
            }
        }else{
            $sql.= " AND verticals.vertical_id IN($vertical_id)";
        }

//   echo $sql;exit;

    }
    if($columnname == "Billable")
    {

        $sql="SELECT  DISTINCT employees.company_employ_id,temp.employee_id AS empid, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName,c.client_name as Client_Name,designation.name as Name FROM client c 
        JOIN ( SELECT employee_id,client_id FROM employee_client WHERE billable = 1 ) AS temp ON temp.client_id = c.client_id
        JOIN employees ON employees.employee_id = temp.employee_id JOIN client_vertical cv ON c.client_id = cv.client_id
        JOIN `designation` ON `designation`.designation_id = `employees`.designation_id 
        JOIN `employee_vertical` ON employee_vertical.`employee_id` = employees.`employee_id`
        JOIN `verticals` ON verticals.`vertical_id` = employee_vertical.`vertical_id`
        WHERE employees.`status` IN (1,2,3,4,5,7) AND c.status = 1 

        ";


        $parent_verticals=array("20","21","22");
        if (in_array($vertical_id, $parent_verticals)){
            if($verticalID==20){
                $sql.= " AND verticals.parent_vertical_id IN($vertids)";
            }else{
                $sql.=" AND verticals.vertical_id IN($vertids)";
            }
        }else{
            $sql.= " AND verticals.vertical_id IN($vertical_id)";
        }

//  echo $sql;exit;

    }
    return $sql;
}


private function __verticalWiseBillables_query($grade, $PodID = '', $pid = '')
{
    $processArray = "";
    $status       = true;
    if (empty($pid))
        $processID = " NOT IN(" . implode(',', array(
            ONBOARD_PROCESS_ID,
            INI_PROCESS_ID
        )) . ")";
    else
        $processID = " =$pid";


    switch ($grade) {
        case 3:
        case '3.1':
        case 4:
        case 5: {
            $processArray = $this->getProcess();

            if (count($processArray) == 0)
                $status = false;
            else
                $processArray = " AND Process.ProcessID IN(" . implode(",", $processArray) . ") AND process.ProcessStatus=0 ";
        }
    }

    if (empty($PodID)) 
    {
        $sql = "SELECT `tbl_rhythm_pod`.`pod_name` AS `Name`, COUNT(BillableTable.employid) AS Value1, BillableTable.PodID ";
    } 
    else 
    {
        $sql = "SELECT BillableTable.CompanyEmployID, CAST(SUBSTRING_INDEX(BillableTable.CompanyEmployID, '-', -1) AS UNSIGNED INTEGER) AS CID, BillableTable.FirstName, client.Client_Name, BillableTable.Process_Name, designation.Name ";
    }
    if ($status) 
    {
        if($grade < 4)
        {
            $sql .= "FROM (
            (SELECT employ.employid, process.verticalid, employ.CompanyEmployID, employ.FirstName, process.ClientID, `process`.`Name` AS Process_Name, employ.DesignationID FROM employ 
            JOIN employprocess ON (employ.employid = employprocess.employid)
            JOIN PROCESS ON (employprocess.processid = process.processid)
            WHERE employprocess.billable = 1 AND employprocess.`PrimaryProcess`=1 AND employprocess.`ProcessID` $processID AND employ.`RelieveFlag`=0 AND employprocess.Grades < 4 $processArray ) ";
        }
        else if($grade >= 4)
        {
            $sql .= "FROM (
            (SELECT employ.employid, process.verticalid, employ.CompanyEmployID, employ.FirstName, process.ClientID, `process`.`Name` AS Process_Name, employ.DesignationID FROM employ 
            JOIN employprocess ON (employ.employid = employprocess.employid)
            JOIN PROCESS ON (employprocess.processid = process.processid)
            WHERE employprocess.billable = 1 AND employprocess.`PrimaryProcess`=1 AND employprocess.`ProcessID` $processID AND employ.`RelieveFlag`=0 $processArray ) ";
        }
        $sql .= ") AS BillableTable ";

        return $sql;

    }	
    else
        return false;
}

function getEmpBillableDetails($billability, $PodId = '', $grouppod = '')
{

    $PodId = ($PodId != '')? implode(",",json_decode($PodId)) : '';
    $options = array();
    $status  = false;

    if ($this->input->get('hasNoLimit') != '1') {
        $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
        $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
    }
    if ($billability == 'Non-Billable Leadership')
    {
        $sql = $this->__leaderShip($PodId,$billability);
    }
    else if($billability == 'Billable Leadership')
    {
        $sql = $this->__leaderShip($PodId,$billability);
    }
    else 
    {
        if ($billability == 'Billable FTE')
            $sql = $this->getBillableCount($PodId, 'FTE');
        else if($billability == 'Billable Hourly')
            $sql = $this->getBillableCount($PodId, 'Hourly');
        else if($billability == 'Billable Volume')
            $sql = $this->getBillableCount($PodId, 'Volume');
        else if($billability == 'Billable Project')
            $sql = $this->getBillableCount($PodId, 'Project');
        else if($billability == 'Non-India Billable HC')
            $sql = $this->getBillableNonIndianCount($PodId);
        else if($billability == 'Unassigned')
            $sql = $this->getUnassignedEmps($PodId);
        else
            $sql = $this->getNonBillableCount($PodId, $grouppod);
    }
    $totQuery = $this->db->query($sql, false);
    $totCount = $totQuery->num_rows();

    $sort = json_decode($this->input->get('sort'), true);

    $options['sortBy']        = $sort[0]['property'];
    $options['sortDirection'] = $sort[0]['direction'];

    if (isset($options['sortBy'])) 
    {
        $orderBy = "";
//$sort = 'LPAD('.$options['sortBy'] . ', 20, "0") ' . $options['sortDirection'];
        if ($options['sortBy'] == "company_employ_id")
            $orderBy .= ' ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ' . $options['sortDirection'];;
        if ($options['sortBy'] == "FirstName")
            $orderBy .= ' ORDER BY FirstName ' . $options['sortDirection'];
        if ($options['sortBy'] == "DesignationName")
            $orderBy .= ' ORDER BY DesignationName ' . $options['sortDirection'];
        if ($options['sortBy'] == "Client_Name")
            $orderBy .= ' ORDER BY Client_Name ' . $options['sortDirection'];
        $sql .= $orderBy;
    } else {
        if ($billability == 'Non-Billable Leadership' || $billability == 'Billable Leadership')
            $sql .= " ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ASC  ";
        else if($billability == 'Unassigned')
            $sql .= ' ORDER BY FirstName ASC';
        else
            $sql .= " ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ASC ";
    }

    if (isset($options['limit']) && isset($options['offset'])) {
        $limit = $options['offset'] . ',' . $options['limit'];
        $sql .= " LIMIT " . $limit;
    } else if (isset($options['limit']))
    $sql .= " LIMIT " . $options['limit'];

    $query = $this->db->query($sql);

    // debug($query->result_array());exit;

    return $results = array_merge(array(
        'totalCount' => $totCount
    ), array(
        'rows' => $query->result_array()
    ));
}

function getUnassignedEmps($PodId = '')
{
    $opsPod = implode("','", json_decode(OPS_VERT));
    $sql="SELECT DISTINCT company_employ_id, CAST(SUBSTRING_INDEX(employees.company_employ_id, '-', -1) AS UNSIGNED INTEGER) AS CID, d.name AS DesignationName, CONCAT_WS(' ',employees.first_name,employees.last_name) as FirstName, 'Unassigned' as billable_type, 'Unassigned' as Client_Name
    FROM employees
    JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
    LEFT JOIN designation d ON d.designation_id = employees.designation_id
    WHERE employees.status NOT IN(6,4) AND d.grades<3 AND employee_pod.pod_id IN ('".$opsPod."') AND employees.employee_id NOT IN(SELECT DISTINCT employee_id FROM employee_client) GROUP BY employees.employee_id";

    return $sql;

}

}

?>