<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Technology_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
		$this->load->model('Rhythm_Model');
	}

	function build_query() 
	{
		$filter = json_decode($this->input->get('filter'),true);
		// $processArray = $this->getProcess();
		$filterQuery = $this->input->get('query');
		$options['filter'] = $filter;
		$filterName = "tech.Name";//$this->input->get('filterName')?$this->input->get('filterName'):"";
		$tooltechReport = $this->input->get('tooltechReport')?$this->input->get('tooltechReport'):'';	
		$ClientID = $this->input->get('ClientID')? $this->input->get('ClientID') : '';
		$ProcessID = $this->input->get('ProcessID')?$this->input->get('ProcessID'):'';
		$AmID = $this->input->get('AMID')?$this->input->get('AMID'):'';
		$managerID = $this->input->get('managerID')?$this->input->get('managerID'):'';
		$TLID = $this->input->get('TLId')?$this->input->get('TLId'):'';		
			/* non  codeigniter format */
		$sessionEmploy     = $this->input->cookie();
		$loggedInUserGrade = $sessionEmploy['Grade'];
		
		$VerticalID = $this->input->get('VerticalID') ? $this->input->get('VerticalID') : '';

		/* codeigniter format */
		if($tooltechReport == "")
		{
			$this->db->select('tech.technology_id,tech.name AS name,tech.vertical_id,tech.description AS description,vert.name AS vName,tech.activities ',false);
			$this->db->from("technology tech");
			$this->db->join('verticals vert', 'tech.vertical_id = vert.vertical_id','left');
		}
		else
		{			
			$this->db->select("tech.technology_id,GROUP_CONCAT(IF (tech.name ='', NULL, tech.name) SEPARATOR ', ') AS name, GROUP_CONCAT(IF (tech.activities ='', NULL, tech.activities) SEPARATOR ', ') AS activities,cl.ClientID,cl.Client_Name,GROUP_CONCAT(DISTINCT cl.industryType) AS industryType,pro.process_id ",false);
			$this->db->from("technology tech");
			$this->db->join('processtechnology processTech', 'processTech.technology_id = tech.technology_id','inner');
			$this->db->join('process pro', 'pro.process_id = processTech.processID','inner');
			$this->db->join('verticals vert', 'tech.vertical_id = vert.vertical_id','inner');
			$this->db->join('client cl', 'cl.ClientID = pro.ClientID','inner');
			if($VerticalID == "" && ($loggedInUserGrade == 3  || $loggedInUserGrade == 4 || $loggedInUserGrade == 5))
			{
				$processData = $processArray;
				$this->db->WHERE('pro.process_id IN('.implode(',', $processData).')');
			}
			if($ClientID!="")
				$this->db->where("pro.ClientID",$ClientID);
			if($ProcessID!="" && $AmID == "")
				$this->db->where("pro.process_id",$ProcessID);
			if($managerID !="")
			{
				$processManager = $this->getprocessAMLeads($managerID,"VL");
				if($ProcessID !="")
					array_push($processManager, $ProcessID);
				if(count($processManager)!= 0)
					$this->db->where_in("pro.process_id",$processManager);
				else
					$this->db->where("pro.process_id","0");
			}
			if($AmID !="")
			{						
				$this->db->join('employprocess pm', '  pro.process_id = pm.processID AND ( pm.Grades >=4 AND pm.Grades < 5) AND   pm.EmployID ='.$AmID);
				$processAm = $this->getprocessAMLeads($AmID,"AM");
				if($ProcessID !="")
					array_push($processAm, $ProcessID);
				if(count($processAm)!= 0)
					$this->db->where_in("pro.process_id",$processAm);
			}
			if($TLID !="")
			{						
				$this->db->join('employprocess pl', ' pro.process_id = pl.processID AND  (pl.Grades >=3 AND pl.Grades < 4) AND pl.EmployID ='.$TLID);
				$processTl = $this->getprocessAMLeads($TLID,"TL");
				if($ProcessID !="")
					array_push($processTl, $ProcessID);
				if(count($processTl)!= 0)
					$this->db->where_in("pro.process_id",$processTl);
			}
			
			$this->db->where("pro.ProcessStatus",0);	
			$this->db->group_by("cl.ClientID");	
		}
		
		if($VerticalID){
			$this->db->where("tech.vertical_id",$VerticalID);
		}

		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') {
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= ($filterField == 'name')?'tech.name':$filterField;
						
						$filterWhere .=" LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') {
				$this->db->where('('.$filterWhere.')');
			} 
		}
		
		if($filterName !="" && $filterQuery!="" ){
			$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			$this->db->where($filterWhere);
		}			
	}

	function view() 
	{
		$options = array();
		$tooltechReport = $this->input->get('tooltechReport')?$this->input->get('tooltechReport'):'';
		$Reportparam = $this->input->get('Report')?$this->input->get('Report'):'';	
		if($this->input->get('hasNoLimit') != '1' && $Reportparam == ""){
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		$sort = json_decode($this->input->get('sort'),true);
			
		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$this->build_query();
		$totQuery = $this->db->get();
		// echo $this->db->last_query();
		$totCount = $totQuery->num_rows();
			
		$this->build_query();
		if(isset($options['sortBy'])) {
			$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		}
		
		if(isset($options['limit']) && isset($options['offset'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		elseif(isset($options['limit'])) {
			$this->db->limit($options['limit']);
		}
		
		$query = $this->db->get();
		//print $this->db->last_query();exit;//$query->num_rows();//
		if($tooltechReport == "")
			$results = array_merge(array('totalCount' => $totCount),array('rows' => $query->result_array()));
		else
		{
			$resultsq = array_merge(array('totalCount' => $totCount),array('rows' => $query->result_array()));
			$results = $this->_addSerialNos($resultsq);
			
			if($Reportparam!="")
			{
				$dtRept = $query->result_array();
				foreach($dtRept as $leadMngr => $val)
				{
					unset($dtRept[$leadMngr]['technology_id']);
					unset($dtRept[$leadMngr]['processID']);
					unset($dtRept[$leadMngr]['ClientID']);
					$dtRept[$leadMngr]["Client"] = $dtRept[$leadMngr]['Client_Name'];
					unset($dtRept[$leadMngr]['Client_Name']);   
					$dtRept[$leadMngr]["Client Type"] = $dtRept[$leadMngr]['industryType'];
					unset($dtRept[$leadMngr]['industryType']);
					$dtRept[$leadMngr]["Tools/Platforms"] = $dtRept[$leadMngr]['Name'];
					unset($dtRept[$leadMngr]['Name']);
					$dtRept[$leadMngr]["activities "] = $dtRept[$leadMngr]['activities'];
					unset($dtRept[$leadMngr]['activities']);                  		               
				}       
				$columns = array(
					array_keys($dtRept[0])
				);
				$results = array_merge($columns, $dtRept);			   
			}		
		}
		return $results;
	}

	function updateTechnology( $idVal = '', $data = '' ) 
	{
		$setData = $data;
		
		if(!empty($setData) && ($this->_isRecordExists($setData['technology_id'],$setData['vertical_id'],$setData['name'])== 0))
		{
			$where = array();
			$set = array();
			$where['technology_id'] = $setData['technology_id'];
			
			$dataset = array(
				'name'			=> $setData['name'],
				'vertical_id'	=> $setData['vertical_id'],
				'description'	=> $setData['description'],
				'activities'	=> $setData['activities']
			);
			
			$retVal = $this->update($dataset, $where, 'technology');
			
			return $retVal;
		}
		else
			return '-1';
	}

	function deleteTechnology( $idVal = '' ) 
	{
		$options = array();
		$options['technology_id'] =  $idVal;
		$retVal = $this->delete($options, 'technology');
		
		return $retVal;
	}
	
	private function _isRecordExists($TechnologyID="",$VerticalID,$name) 
	{
		$query = $this->db->select("technology_id")->where("name",trim(strtolower($name)))->where("vertical_id",$VerticalID);
		if($TechnologyID!="")
			$this->db->where("technology_id  != ",trim($TechnologyID));
		$query = $this->db->get('technology');
		
		return $query->num_rows();
	}

	function addTechnology( $data = '' ) 
	{
		$addVertical = $data;
		
		$VerticalID = is_array($addVertical['vertical_id'])?$addVertical['vertical_id'][0]:$addVertical['vertical_id'];
		$this->getTotalRecordCount(array("name"=>trim($addVertical['name'])),'technology');
		if($this->_isRecordExists("",$VerticalID,$addVertical['name'])== 0)
		{
			$dataset = array(
				'name'			=> $addVertical['name'],
				'description'	=> $addVertical['description'],
				'vertical_id'	=> $VerticalID,
				'activities'	=> $addVertical['activities']
			);
			
			$retVal = $this->insert($dataset, 'technology');
			
			return $retVal;
		}
		else 
			return -1;
	}
	
	private function _addSerialNos($data)
    {
        $i = 1;
        foreach ($data['rows'] as $k => $v) {
            $data['rows'][$k]['SlNo'] = $i;
            $i++;
        }   
		
        return $data;
    }
	
    public function getprocessAMLeads($Amid,$param)
	{
    	if($param == "AM")
    		$sql = "SELECT ProcessID FROM employprocess WHERE (Grades >=4 AND Grades < 5 ) AND EmployID =".$Amid;
    	else if($param == "TL")
    		$sql = "SELECT ProcessID FROM employprocess WHERE (Grades >=3 AND Grades < 4 ) WHERE EmployID =".$Amid;
    	else if($param == "VL")
    		$sql = "SELECT ProcessID FROM PROCESS
					JOIN `verticalmanager` ON `PROCESS`.`vertical_id`=verticalmanager.`vertical_id`
					WHERE verticalmanager.`EmployID`='".$Amid."' AND PROCESS.`ProcessStatus`=0"; 

    	$result_select = $this->db->query($sql);
    	$processArray = array();
		if(isset($result_select) && count($result_select->result_array())!=0)
		{
			foreach($result_select->result_array() as $key => $val)
			array_push($processArray, $val['ProcessID']);		
		}
		return $processArray;
    }
    
}
	
?>