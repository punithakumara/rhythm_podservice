<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Client_Contacts_Model extends INET_Model
	{
		function __construct() {
			parent::__construct();
		}
		
		//**** List View Query ****//
		function build_query($client_id) 
		{
			$filter = json_decode($this->input->get('filter'),true);
			$options['filter'] = $filter;
			$statusflag = $this->input->get('status');
			$sql = "SELECT client_contacts.*, GROUP_CONCAT(client_contacts.contact_id) AS ContactIDs, 
					tbl_rhythm_services.name AS ServiceName, tbl_rhythm_services.service_id AS ServiceID
					FROM client_contacts 
					LEFT JOIN tbl_rhythm_services ON (client_contacts.service_id = tbl_rhythm_services.service_id)
					WHERE 1 = 1";			
			if(isset($client_id) && $client_id!="")
				$sql .=" AND client_id = ".$client_id;
			if(isset($statusflag) && $statusflag == 'deleted') {
				$sql .=" AND client_contacts.status = 1 ";
			} else {
				$sql .=" AND client_contacts.status = 0 ";
			}
		
			$filterWhere = '';
			if(isset($options['filter']) && $options['filter'] != '') {
				foreach ($options['filter'] as $filterArray) {
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) {
						if($filterField != '') {
							$filterWhere .= ($filterWhere != '')?' OR ':'';
							$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
						}
					}
				}
				if($filterWhere != '') {
					$sql .= " AND (".$filterWhere.")";
				}
			}
			$sql.=" GROUP BY client_contacts.contact_email ";				
			return $sql;
		}
	
		function view($client_id) 
		{
			$options = array();
			
			if($this->input->get('hasNoLimit') != '1'){
				$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
			}
			
			$sort = json_decode($this->input->get('sort'),true);
			//print_r($sort);	
			$options['sortBy'] = $sort[0]['property'];
			$options['sortDirection'] = $sort[0]['direction'];
			if(isset($client_id) && $client_id != "") 
			{
				$sql = $this->build_query($client_id);
				
				$query = $this->db->query($sql);			
				$totCount = $query->num_rows();
				
				if(isset($options['sortBy'])) 
				{
					$sort = $options['sortBy'].' '.$options['sortDirection'];
					$sql .= " order by ".$sort;
				}			
				if(isset($options['limit']) && isset($options['offset'])) 
				{
					$limit = $options['offset'].','.$options['limit'];
					$sql .= " limit ".$limit;
				}	
				elseif(isset($options['limit'])) 
				{
					$sql .= " limit ".$options['limit'];
				}
				// echo $sql;
				$query = $this->db->query($sql);
				
				$data = $query->result_array();
				$data = $this->_addSerialNos($data,$options['offset']);			
				
				$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));
				return $results;
			}
		}
		
		
		private function _addSerialNos($data,$start)
		{
			$l = ($start==0)?1:$start+1;
			$tempArray = $data;
			$i=1;
			foreach($data as $k=>$v)
			{				
				$j=$i-1;
				$tempArray[$j]['SlNo']=$l;
				$i++;
				$l++;
			}			
			return $tempArray;
		}
		
		
		/**
		* Inserting And Updating
		*/
		function addClient_Contact( $data = '' ) 
		{
			$clientID 				= $data['client_id'];
			$data 					= json_decode(trim($data['Client_Contacts']), true);
			
			//loading model
			$this->load->model(array('Services_Model'));
			$getServIdFrmName		= $data['ServiceName'];
			$retVal = 0;
			$id						= isset($data['ContactIDs']) ? $data['ContactIDs'] : '';
			
			
			if (!empty($id)) 
			{
				$id = explode(",", $id);
				$this->db->where_in('contact_id', $id);
				$this->db->delete('client_contacts');
			}
			
				$client_contact_Dataset = array(
					'client_id'			=>	$clientID,
					'contact_name'		=>	$data['contact_name'],
					'contact_title'		=>	$data['contact_title'],
					'contact_email'		=>	$data['contact_email'],
					'contact_mobile'	=>  $data['contact_mobile'],
					'contact_phone'		=>	$data['contact_phone'],				
					'service_id'	=>	(is_numeric($getServIdFrmName))?trim($getServIdFrmName): $this->Services_Model->getServIdFrmName(trim($getServIdFrmName)),
					'role'				=>	$data['role'],
					'status'			=>	0
				);
				
				//Inserting
				if($data['contact_email'] !="") {
					$retVal 			= $this->insert($client_contact_Dataset, 'client_contacts');	
					
					$Client_contact_ID	= $this->getInsertId();

					$empData    = $this->input->cookie();
			        $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Client Contact ID: '.$Client_contact_ID.')(Client ID: '.$clientID.') has been added client contact successfully.';
			        $this->userLogs($logmsg);
					// $this->log_action('client_contact','add',$Client_contact_ID);
				} else {
					$retVal =1;	
				}
			// }
			return $retVal;
		}
		
		
		/**
		* Delete CLient Contact Details
		*/
		function Remove_clientContact($clientContactId, $clientContactEmail, $status, $role) 
		{
			$statusval = "";
			if(isset($status) && $status == 'delete') {
				$statusval	= 1;
			} else {
				$statusval	= 0;
			}
			$sql	= " UPDATE client_contacts SET status = $statusval 
						WHERE contact_id = '".$clientContactId."' AND contact_email = '".$clientContactEmail."' AND role='".$role."'";
			$query	= $this->db->query($sql);

			$empData    = $this->input->cookie();
	        $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Client Contact ID: '.$clientContactId.') has been removed client contact successfully.';
	        $this->userLogs($logmsg);

			return  $query;			
		}	
	}
?>