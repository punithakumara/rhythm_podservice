<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vertical_Model extends INET_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Rhythm_Model');
    }
    
    function build_query()
    {
		$empData = $this->input->cookie();
		$grade  = $empData['Grade'];
		$VertIDs = $empData['VertId'];
		
        $filter             = json_decode($this->input->get('filter'), true);
        $filterName         = $this->input->get('filterName') ? $this->input->get('filterName') : "";
        $fromClientCont     = $this->input->get('fromClientCont') ? $this->input->get('fromClientCont') : "";
        $comboVertical      = $this->input->get('comboVertical') ? $this->input->get('comboVertical') : '';
        $ComboNoVerticalID  = $this->input->get('ComboNoVerticalID') ? $this->input->get('ComboNoVerticalID') : '';
        $fromdashboard      = $this->input->get('dashboard') ? $this->input->get('dashboard') : '';
        $filterQuery        = $this->input->get('query');
        $options['filter']  = $filter;
        $pvertical = $this->input->get('pvertical') ? $this->input->get('pvertical') : '';
        $VerticalFromParent = '';
        $sql = "SELECT temp.vertical_id AS vertical_id ,temp.parent_vertical_id ,temp.Name,temp.Name AS name, GROUP_CONCAT(temp.employee_id) AS MG,GROUP_CONCAT(CONCAT(IFNULL(emp.first_name, ''),' ',IFNULL(emp.last_name, ''))) AS MGName,GROUP_CONCAT(emp.company_employ_id) AS ComEmpIDS,temp.description,temp.folder,sft.shift_id,sft.shift_code AS ShiftCode FROM(SELECT verticals.*, verticalmanager.vertical_id AS vertID, verticalmanager.employee_id,verticalmanager.shift_id FROM `verticals` LEFT JOIN `verticalmanager` ON verticals.vertical_id=verticalmanager.vertical_id ) AS temp LEFT JOIN shift sft ON sft.shift_id = temp.shift_id LEFT JOIN employees emp ON temp.employee_id = emp.employee_id  where 1=1 ";

        $filterWhere = '';
        if (isset($options['filter']) && $options['filter'] != '') {
            foreach ($options['filter'] as $filterArray) {
                $filterFieldExp = explode(',', $filterArray['property']);
                foreach ($filterFieldExp as $filterField) {
                    if ($filterField != '') {
                        $filterWhere .= ($filterWhere != '') ? ' OR ' : '';
                        $filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
                    }
                }
            }
            
            
            if ($filterWhere != '') {
                $sql .= " and (" . $filterWhere . ")";
            }
        }
        
        if ($filterName != "") {
            if($filterName == 'VerticalName') {
                $filterName = 'temp.Name';
            }
            $filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
            $sql .= " and " . $filterWhere;
        }
        
        $empData     = $this->input->cookie();
        $this->EmpId = $empData['employee_id'];
        
        if ($grade < 5 && $VertIDs!=10 && $empData['is_onshore']!=1) 
		{
            $sql .= " AND temp.vertical_id IN(" . $VertIDs . ")";
        }
		if($empData['is_onshore']==1)
		{
			 $sql .= " AND temp.vertical_id IN(28,29)";
		}
		
		if ($ComboNoVerticalID != "") 
		{
            $sql .= " AND temp.vertical_id NOT IN(" . $ComboNoVerticalID . ")";
        }
		
        if($pvertical != '')
		{
        	$this->db->select("Group_concat(DISTINCT verticals.vertical_id) as VerticalIDs ", FALSE)->from("verticals")
	         ->where('verticals.parent_vertical_id =', $pvertical);
	        /*if($loggedInVerticals != ''){
	        	$this->db->where_in('verticals.vertical_id ',$loggedInVerticals);
	        }*/	        
	        $this->db->order_by('verticals.Name');	        
	        $query = $this->db->get();
	        if ($query->num_rows() >0)
	        { 
	        	$VerticalFromParent = $query->row()->VerticalIDs;	        	
	        	$sql .=  " AND temp.vertical_id IN(" . $VerticalFromParent . ")";
	        }
        }
        $sql .= " GROUP BY temp.vertical_id";       
        // echo $sql; exit;
        return $sql;
    }
    
    function view()
    {
        $options = array();
        
        if ($this->input->get('hasNoLimit') != '1') {
            $options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
            $options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
        }
        
        $sort = json_decode($this->input->get('sort'), true);
        
        $options['sortBy']        = $sort[0]['property'];
        $options['sortDirection'] = $sort[0]['direction'];
        
        $sql      = $this->build_query();	
        $totQuery = $this->db->query($sql);
        $totCount = $totQuery->num_rows();
        
        if (isset($options['sortBy'])) {
            $sql .= " order by " . $options['sortBy'] . " " . $options['sortDirection'];
        }
        
        if (isset($options['limit']) && isset($options['offset'])) {
            $sql .= " limit " . $options['offset'] . " , " . $options['limit'];
        } else if (isset($options['limit'])) {
            $sql .= " limit " . $options['limit'];
        }
		//echo $sql; exit;
        $query = $this->db->query($sql);
        $data = $query->result_array();
        
        foreach ($data as $key => $entry) {
            $temp             = explode(",", $data[$key]['MG']);
            $data[$key]['MG'] = $temp;
        }
        
        $results = array_merge(array(
            'totalCount' => $totCount
        ), array(
            'rows' => $data
        ));
        return $results;
    }
    
    
    private function _isRecordExists($id = "", $name)
    {
        
        $query = $this->db->select("vertical_id")->where("Name", trim(strtolower($name)));
        if ($id != "") {
            $this->db->where("vertical_id  != ", trim($id));
        }
        $query = $this->db->get('verticals');
        return $query->num_rows();
    }
    
    function updateVertical($idVal = '', $data = '')
    {
        $setData = json_decode(trim($data), true);
		
        if ($this->_isRecordExists($idVal, $setData['name']) == 0) {
            $where               = array();
            $set                 = array();
            $where['vertical_id'] = $idVal;
            $assRetVal           = $this->updateVerticalManager($idVal, $setData);
            $tempArr             = $setData;
            unset($tempArr['MG']);
            unset($tempArr['MGName']);
            unset($tempArr['shift_code']);
            unset($tempArr['shift_id']);
           
			$retVal = $this->update($setData, $where, 'verticals');

            $empData    = $this->input->cookie();
            $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Vertical ID: '.$idVal.') has been updated vertical.';
            $this->userLogs($logmsg);
			
			/* The Below Code added to change the Vertical name in the Utilization Table Once record is being Updated */
		//	$vertical_name = array('vertical_name' => $setData['name'] );
			
			//$change_util_verticalname  = $this->update($vertical_name, $where, 'utilization');
			
            if (($assRetVal == '1') || ($retVal == '1')) {
                return '1';
            } else if (($assRetVal != '1') && ($retVal != '1')) {
                return '0';
            }
        } else {
            return "-1";
        }
    }
    
    function getVerticalId($employID, $AssignedVertical)
    {
        
        if ($AssignedVertical != "" && $AssignedVertical != 0) 
		{
        	if(!is_array($AssignedVertical))
			{
            	$sql = "SELECT ver.vertical_id,ver.Name,pro.process_id 
				FROM PROCESS pro, verticals ver WHERE ver.vertical_id = " . $AssignedVertical;
            }
			else
            {
            	$AssignedVerticals = implode(",",$AssignedVertical);
                if(count($AssignedVertical) == 1)
				{
					$sql = "SELECT ver.vertical_id,ver.Name,pro.process_id 
					FROM PROCESS pro, verticals ver WHERE ver.vertical_id = " . $AssignedVerticals;
                }
                else
				{
					$sql = "SELECT ver.vertical_id,ver.Name,pro.process_id,
					(SELECT GROUP_CONCAT(DISTINCT mver.Name ORDER BY FIELD(mver.vertical_id, " . $AssignedVerticals. ")) FROM verticals mver WHERE mver.vertical_id IN ( " . $AssignedVerticals. ") ) AS MultiVerName 
					FROM PROCESS pro, verticals ver
					WHERE ver.vertical_id IN ( " . $AssignedVerticals. ") ORDER BY FIELD(ver.vertical_id, " . $AssignedVerticals. ") ";
				}
            }
           
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            if ($query->num_rows() > 0) {
                $processID    = $result[0]['ProcessID'];
                $verticalID   = $result[0]['VerticalID'];
                $verticalName = $result[0]['Name'];
                if(is_array($AssignedVertical) && count($AssignedVertical) > 1)
				{
                    $verticalName = $result[0]['MultiVerName'];
                }
                else
				{
              	 	$verticalName = $result[0]['Name'];
                }
                
                foreach ($result as $key => $val) 
				{
                    if ($val['ProcessID'] == HR_PROCESS_ID && $val['VerticalID'] == HR_VERTICAL_ID) 
					{
                        $processID    = $val['ProcessID'];
                        $verticalID   = $val['VerticalID'];
                        $verticalName = $val['Name'];
                    }
                }
            }
            return array(
                "VerticalID" => $verticalID,
                "ProcessID" => $processID,
                "Name" => $verticalName
            );
        } else {
            return '';
        }
    }
    
    
    function deleteVertical($idVal = '')
    {
        $options = array();
        
        $options['vertical_id'] = $idVal;
        $retVal                = $this->delete($options, 'verticals');
        return $retVal;
    }
    
    function deleteVerticalManager($idVal = '')
    {
        $options = array();
        
        $options['vertical_id'] = $idVal;
        $retVal                = $this->delete($options, 'verticalmanager');
        return $retVal;
    }
    
    
    function updateVerticalManager($idVal = '', $data = '')
    {
        
        $arrayEntities = explode(",", $data['MG'][0]);
        
        $retDelVal = $this->deleteVerticalManager($idVal);
        
        $retVal = $this->addVerticalManager($idVal, $data['MG'], $data['shift_id']);
        
        return $retVal;
    }
    
    function addVerticalManager($idVal = '', $data = '', $shift = '')
    {
        
        $array_data = array();
        
        array_push($array_data, array(
            'vertical_id' => $idVal,
            'employee_id' => $data,
            'shift_id' => $shift
        ));
        
        $retVal = $this->db->insert_batch('verticalmanager', $array_data);
        
        return $retVal;
    }
    
    
    function addVertical($data = '')
    {
        $addVertical = json_decode(trim($data), true);
		
        // cheking if verticals value already exist or not
        if ($this->_isRecordExists("", $addVertical['name']) == 0) {
        	$addVertical['short_name'] = substr($addVertical['name'],0,9);
        	
        	$retVal = $this->insert($addVertical, 'verticals');       	
        	$addVertical['vertical_id'] = $this->getInsertId();
            $addVertical['shift_id'] = null;        	
        	$assRetVal = $this->addVerticalManager($addVertical['vertical_id'], $addVertical['MG'], $addVertical['shift_id']);
        	
            if ($assRetVal) {
                $empData    = $this->input->cookie();
                $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Vertical ID: '.$addVertical['vertical_id'].') has been added vertical.';
                $this->userLogs($logmsg);
                return $assRetVal;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
        
    }
    function getverticalHead($verticalId)
	{
    	$this->db->select("CONCAT_WS(' ',employees.first_name,employees.last_name) AS FirstName,employees.employee_id",false)->from('employees')->join('verticalmanager', 'verticalmanager.employee_id = employees.employee_id')->where('verticalmanager.vertical_id =', $verticalId);
		
    	$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
			    $row = $query->row(); 
			    return $row->FirstName."==".$row->employee_id;
			}
		
	    return '';
    }
    
	function getParentVertical(){
		$sql = "SELECT c.parent_vertical_name AS `groupBy` ,c2.parent_vertical_name AS name, c2.parent_verticalID AS id
				FROM parent_vertical c
				JOIN parent_vertical c2 ON c.parent_verticalID = c2.super_parent_verticalID";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$data = array_merge(array(
            'totalCount' => $query->num_rows()
        ), array(
            'rows' => $data
        ));
		return $data;
	}
// Group verticals Combobox
	function GroupVerticalComboValues()
	{
		$query  = "SELECT vertical_id AS VerticalID,verticals.name AS name FROM verticals  WHERE parent_vertical_id = 0";
		$query  = $this->db->query($query);
		$data = $query->result_array();
		$i=0;
		$temp_array=array();
		foreach($data as $key => $value){
			//$data[$key]['VerticalName'] = $value['Name'];
			$temp_array[$i]['VerticalID']=$value['VerticalID'];
			$temp_array[$i]['Name']		 =$value['Name'];
			$i++;
			$FirstLevelquery  = "SELECT vertical_id AS VerticalID,verticals.Name AS Name FROM verticals  WHERE parent_vertical_id = ".$value['VerticalID'];
			$FirstLevelresult  = $this->db->query($FirstLevelquery);
			$FirstLeveldata = $FirstLevelresult->result_array();
				foreach($FirstLeveldata as $FirstLevelkey => $FirstLevelvalue){
					$temp_array[$i]['VerticalID']=$FirstLevelvalue['VerticalID'];
					$temp_array[$i]['Name']		 ="	&emsp;&#9830; ".$FirstLevelvalue['Name'];
					$i++;
					$secondLevelquery  = "SELECT vertical_id AS VerticalID,verticals.Name AS Name FROM verticals WHERE parent_vertical_id = ".$FirstLevelvalue['VerticalID'];
					$secondLevelresult  = $this->db->query($secondLevelquery);
					$secondLeveldata = $secondLevelresult->result_array();
					foreach ($secondLeveldata as $secondLevelkey => $secondLevelvalue){
						$temp_array[$i]['VerticalID']=$secondLevelvalue['VerticalID'];
						$temp_array[$i]['Name']		 ="&emsp;"."	&#8594; ".$secondLevelvalue['Name'];
						$i++;	
					}
				}
			}
		//print_r($temp_array);exit;
		return $temp_array;
	}	

	
	// Group verticals Combobox
	function RevisedGroupVerticalComboValues()
	{
		$empData = $$this->input->cookie();
		$loggedInUserId = $empData['employee_id'];
		$loggedInUserGrade = $empData['Grade'];
		if($loggedInUserGrade >= 6)
		{
			$query  = "SELECT Root.vertical_id AS rootid,Department.vertical_id AS deptid, VerticalName.vertical_id as verticalid,  Root.name  AS Root, Department.name AS Department, VerticalName.name AS VerticalName
			FROM verticals AS Root
			LEFT OUTER JOIN verticals AS Department ON Department.parent_vertical_id = root.vertical_id
			LEFT OUTER JOIN verticals AS VerticalName ON VerticalName.parent_vertical_id = Department.vertical_id
			WHERE Root.parent_vertical_id = 0 AND VerticalName.vertical_id IN(SELECT DISTINCT contact_vertical AS vertical_id FROM client_contacts)
			ORDER BY Root, Department, VerticalName";
		}
		else
		{
			$loggedInUserVert  = $empData['VertId'];
			
			$query  = "SELECT Root.vertical_id AS rootid,Department.vertical_id AS deptid, VerticalName.vertical_id as verticalid,  Root.name  AS Root, Department.name AS Department, VerticalName.name AS VerticalName
			FROM verticals AS Root
			LEFT OUTER JOIN verticals AS Department ON Department.parent_vertical_id = root.vertical_id
			LEFT OUTER JOIN verticals AS VerticalName ON VerticalName.parent_vertical_id = Department.vertical_id
			WHERE VerticalName.vertical_id in (".$loggedInUserVert.")
			ORDER BY FIELD(VerticalName.vertical_id, " . $loggedInUserVert. "), Root, Department, VerticalName";		
		}
		// echo $query; exit;
		$query  = $this->db->query($query);
		$data = $query->result_array();
		$i=0;
		$temp_array=array();
		$tempRootID=0;
		$tempDeptID=0;
		$tempVerticalID=0;
		
		foreach($data as $key => $value)
		{
			if($loggedInUserGrade >= 6)
			{
				if($data[$key]['rootid'] != $tempRootID)
				{
					$tempRootID=$value['rootid'];
					$temp_array[$i]['vertical_id']=$value['rootid'];
					$temp_array[$i]['name']		 =$value['Root'];
					$i++;
				}			
				if($data[$key]['deptid'] != $tempDeptID)
				{
					$tempDeptID=$value['deptid'];
					$temp_array[$i]['vertical_id']=$value['deptid'];
					$temp_array[$i]['name']		 ="	&emsp;&#9830; ".$value['Department'];
					$i++;
				}
			}
			if($data[$key]['verticalid'] != $tempVerticalID)
			{
				$tempVerticalID=$value['verticalid'];
				$temp_array[$i]['vertical_id']=$value['verticalid'];
				$temp_array[$i]['name']		 ="&emsp;"."	&#8594; ".$value['VerticalName'];
				$i++;
			}
			
		}
		//print_r($temp_array);exit;
		return $temp_array;
	}	
	
		
	/* Get Top Level verticals */
	private function __getTopLevelVerticals()
	{
		$query  = "SELECT `pv`.`vertical_id` AS VerticalID,`pv`.`Name` AS Name FROM `verticals` pv  WHERE pv.`parent_vertical_id` = 0 GROUP BY pv.`vertical_id` ORDER BY pv.parent_vertical_id DESC LIMIT 1";
		$query  = $this->db->query($query);
		$result = $query->result_array();
		return $result;
	}
	    
    
	/*
     * @description : getting verticals name  detaild for Tree View
     */
    public function getVerticalsNames($verticalId,$loggedInVerticals = '')
    {
        $this->db->select("verticals.vertical_id,verticals.Name ", FALSE)->from("verticals")
				->join('client_vertical', 'client_vertical.vertical_id = verticals.vertical_id')
				->where('verticals.parent_vertical_id =', $verticalId);
        if($loggedInVerticals != ''){
        	$this->db->where_in('verticals.vertical_id ',$loggedInVerticals);
        }
        $this->db->group_by('verticals.vertical_id');
        $this->db->order_by('verticals.Name');
        $query = $this->db->get();
        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array();
    }
/*
     * @description : getttin verticals based on root verticals
     * 
     */
    public function getsubverticalsNames($verticalId)
    {
        
       	$this->db->select("verticals.vertical_id as VerticalID,verticals.Name as Name ", FALSE)
       	->from("verticals")
       	->where('parent_vertical_id =', $verticalId)
       	->order_by('name');
		//echo $this->db->last_query();
       	$query = $this->db->get();
        if ($query->num_rows() == 0)
            return FALSE;
        return $query->result_array();
        
    }
    
	
	public function repoteesverticalHierarchyGrid($grade = '')
    {
        $data = array();
        $data = $this->__getTopLevelVerticals();
        // $hrpool = $this->Rhythm_Model->getHRPoolNonBillableEmployees();
       // print_r($data);exit;
        foreach ($data as $key => $value) 
		{
			$sql = $this->Rhythm_Model->getBillableCountQuery();       
			// echo "<pre>"; print_r($sql); exit;
			$query = $this->db->query($sql);
			$billabledata = count($query->result_array());
			
			$sqlnonbill = $this->Rhythm_Model->getNonBillableCountQuery(); 
            // echo "<pre>"; print_r($sqlnonbill); exit;      
			$querynonbill = $this->db->query($sqlnonbill);
			$nonbillabledata = count($querynonbill->result_array());
			
			//$HRpoolData = $this->Rhythm_Model->getProcessEmpNames(INI_PROCESS_ID);				
			$data[$key]['VerticalName'] = $value['Name'];
			$data[$key]['ParentVerticalID'] = $value['VerticalID'];
			$data[$key]['Billable'] = $billabledata;
			$data[$key]['NonBillable'] = $nonbillabledata;
			$data[$key]['Leadership'] = $this->Rhythm_Model->getLeadverticalcount($Verticals = '',$processIds = '',$subVertical = '') ;
			//$data[$key]['HRPool'] = count($HRpoolData);
			//$data[$key]['Onboarding'] = count($this->Rhythm_Model->getProcessEmpNames(ONBOARD_PROCESS_ID)); //displaying onboarding count in verticals heirarchy					
        }
        return $data;
    }
	
    public function getsubTreeTruture($vid){
    	$data   = array_shift($this->getsubverticalsNames($vid));
    	foreach ($data as $key => $value) {
         			$data[$key]['VerticalName'] = $value['Name'];
		            $data[$key]['VerticalID'] = $value['VerticalID'];

        }
        return $data;
    }

	
    public function getVerticalsForParentID($pVid) 
	{
		if(!is_numeric($pVid) && !strpos($pVid,',')) 
		{
            $pVid   = $this->getVertIdFrmName($pVid);
        }          
        $this->db->select("GROUP_CONCAT(DISTINCT verticals.vertical_id) as VerticalIDs ", FALSE);
		$this->db->from("verticals");
		// $this->db->join('process', 'process.vertical_id = verticals.vertical_id');
		$this->db->where("verticals.parent_vertical_id IN (".$pVid.") OR verticals.vertical_id IN (".$pVid.")" );
        $this->db->order_by('verticals.Name');
        $query = $this->db->get();
		
        if ($query->num_rows() >0) 
		{
            return $VerticalFromParent = $query->row()->VerticalIDs;
        }
        return '';
    }

    public function getVertIdFrmName($verticalName) {
        $res = "";
        if($verticalName) {
            $sql_vertID         = "SELECT vertical_id, Name FROM verticals WHERE Name = '".$verticalName."'";
            $sql_vertID_query   = $this->db->query($sql_vertID);
            $sql_vert_data      = $sql_vertID_query->result_array();
            if(!empty($sql_vert_data)) {
                $res    = array_shift($sql_vert_data);
                return $res['vertical_id'];
            } else {
                return $res;
            }
            
        }
       
    }
      
    // get all verticals irrespective of login
    function AllGroupVerticalComboValues()
    {    
    		$query  = "SELECT Root.vertical_id AS rootid,Department.vertical_id AS deptid, VerticalName.vertical_id as verticalid,  Root.name  AS Root, Department.name AS Department, VerticalName.name AS VerticalName
						     FROM verticals AS Root
						     LEFT OUTER JOIN verticals AS Department ON Department.parent_vertical_id = root.vertical_id
						     LEFT OUTER JOIN verticals AS VerticalName ON VerticalName.parent_vertical_id = Department.vertical_id
						     JOIN PROCESS ON process.vertical_id = VerticalName.vertical_id
						     WHERE Root.parent_vertical_id = 0 AND VerticalName.vertical_id IN(SELECT DISTINCT PROCESS.vertical_id FROM PROCESS)
						     ORDER BY Root, Department, VerticalName";
    			   	
    	//echo $query;
    	$query  = $this->db->query($query);
    	$data = $query->result_array();
    	$i=0;
    	$temp_array=array();
    	$tempRootID=0;
    	$tempDeptID=0;
    	$tempVerticalID=0;
    
    	foreach($data as $key => $value){
    		if(1){
    			if($data[$key]['rootid'] != $tempRootID){
    				$tempRootID=$value['rootid'];
    				$temp_array[$i]['VerticalID']=$value['rootid'];
    				$temp_array[$i]['Name']		 =$value['Root'];
    				$i++;
    			}
    			if($data[$key]['deptid'] != $tempDeptID){
    				$tempDeptID=$value['deptid'];
    				$temp_array[$i]['VerticalID']=$value['deptid'];
    				$temp_array[$i]['Name']		 ="	&emsp;&#9830; ".$value['Department'];
    				$i++;
    			}
    		}
    		if($data[$key]['verticalid'] != $tempVerticalID){
    			$tempVerticalID=$value['verticalid'];
    			$temp_array[$i]['VerticalID']=$value['verticalid'];
    			$temp_array[$i]['Name']		 ="&emsp;"."	&#8594; ".$value['VerticalName'];
    			$i++;
    		}
    			
    	}
    	//print_r($temp_array);exit;
    	return $temp_array;
    }
    
    
    
}
?>