<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function build_query_OpsDashboard()
	{     
		$filter        = json_decode($this->input->get('filter'), true);
		$filterName    = $this->input->get('filterName') ? $this->input->get('filterName') : "";
		$filterQuery   = $this->input->get('query');
		$empData = $this->input->cookie(); 
		$loggedInUserGrade = $empData['Grade'];
		$options['filter'] = $filter;	
		$sql = "SELECT DISTINCT(employees.company_employ_id) AS EmployeeID, CONCAT(employees.first_name,' ',IFNULL(employees.last_name,'')) AS EmployeeFullName,
		location.name AS Location, shift.shift_code AS WorkShift, DATE_FORMAT(employees.doj, '%d-%b-%Y') AS JoiningDate, tbl_rhythm_pod.pod_name AS Pod, tbl_rhythm_services.name AS Service, GROUP_CONCAT( DISTINCT client.client_name) AS ClientName, 
		designation.hcm_grade AS Grade, designation.name AS Designation,employees.primary_lead_id AS Supervisor, 
		employee_pod.pod_id AS PodManager,
		IF((SUM(emp.billable) = '0' OR SUM(emp.billable) IS NULL), 'Non-Billable', 'Billable') AS Billable, 
		IF(employees.status = '5', 'Yes', 'No') AS NoticePeriod,DATE_FORMAT(employees.resigned_date, '%d-%b-%Y') AS ResignationDate
		FROM employees 
		JOIN designation ON employees.designation_id = designation.designation_id 
		LEFT JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
		LEFT JOIN tbl_rhythm_pod ON tbl_rhythm_pod.pod_id = employee_pod.pod_id
		LEFT JOIN tbl_rhythm_services ON tbl_rhythm_services.service_id = employees.service_id
		LEFT JOIN location ON location.location_id = employees.location_id
		LEFT JOIN employee_client emp ON emp.employee_id = employees.employee_id
		LEFT JOIN shift ON shift.shift_id = employees.shift_id
		LEFT JOIN client ON client.client_id = emp.client_id
		WHERE employees.status != 6 AND employees.location_id !=4 ";

		if ( $loggedInUserGrade >2 &&  $loggedInUserGrade < 5 && $empData['pod_id']!=10) 
		{
			$sql .= " AND tbl_rhythm_pod.pod_id IN(" .$empData['pod_id'] . ")";
		}

		$filterWhere = '';
		if (isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach ($filterFieldExp as $filterField) 
				{
					if ($filterField != '') {
						$filterWhere .= ($filterWhere != '') ? ' OR ' : '';                       
						$filterWhere .= $filterField . " LIKE '%" . $filterArray['value'] . "%'";
					}
				}
			}            

			if ($filterWhere != '') {
				$sql .= " and (" . $filterWhere . ")";
			}
		}

		if ($filterName != "") 
		{            
			$filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
			$sql .= " AND " . $filterWhere;            
		}
		$sql .= " GROUP BY employees.company_employ_id";        
		// echo $sql; exit;
		return $sql;
	}
	
	
	function getOpsDashboard()
	{
		$options = array();
		$reportParam = ($this->input->get('Report')) ? $this->input->get('Report') : "";

		if ($this->input->get('hasNoLimit') != '1') {
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
		}        

		$sort = json_decode($this->input->get('sort'), true);

		$options['sortBy'] = isset($sort[0]['property']) ? $sort[0]['property'] : 0;
		$options['sortDirection'] = isset($sort[0]['direction']) ? $sort[0]['direction'] : 0;

		$sql      = $this->build_query_OpsDashboard();
		$totQuery = $this->db->query($sql);
		$totCount = $totQuery->num_rows();

		if (isset($options['sortBy'])) 
		{
			if($options['sortBy']=="EmployeeID")
			{
				$sql .= " ORDER BY CAST(company_employ_id AS SIGNED INTEGER) ";
			}
			else
			{
				$sql .= " ORDER BY " . $options['sortBy'] . " " . $options['sortDirection'];
			}
		}

		if ($reportParam == "") {
			if (isset($options['limit']) && isset($options['offset'])) {
				$sql .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
			} else if (isset($options['limit'])) {
				$sql .= " LIMIT " . $options['limit'];
			}
		}
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		foreach($data as $leadMngr => $val)
		{
			$ReportingManager = "";
			if($val['Supervisor']!="")
			{
				$sqlLead = "SELECT primary_lead_id AS ReportingManager, CONCAT(employees.first_name,' ',IFNULL(employees.last_name,''))  AS Supervisor
				FROM `employees` WHERE employee_id IN (".$val['Supervisor'].")"; 
				$queryLead = $this->db->query($sqlLead);

				$datalead = $queryLead->result_array(); 
				$ReportingManager = isset($datalead[0]['ReportingManager'])?$datalead[0]['ReportingManager']:"";
				$data[$leadMngr]['Supervisor'] = isset($datalead[0]['Supervisor'])?$datalead[0]['Supervisor']:"";
			} else {
				$data[$leadMngr]['Supervisor'] = "";
			}
			
			if($ReportingManager!="" && $ReportingManager!="(NULL)" && $data[$leadMngr]['Grade'] < 5)
			{
				if($data[$leadMngr]['Grade'] > 2 && $data[$leadMngr]['Grade'] < 4)
				{
					$data[$leadMngr]['ReportingManager'] = $data[$leadMngr]['Supervisor'];
				}
				else if($data[$leadMngr]['Grade'] == 4)
				{
					$data[$leadMngr]['ReportingManager'] = $data[$leadMngr]['Supervisor'];
				}
				else
				{
					$sqlLead = "SELECT CONCAT(employees.first_name,' ',IFNULL(employees.last_name,''))  AS ReportingManager
					FROM `employees` WHERE employee_id IN (".$ReportingManager.")"; 
					$queryLead = $this->db->query($sqlLead);
					$datalead = $queryLead->result_array(); 
					$data[$leadMngr]['ReportingManager'] = isset($datalead[0]['ReportingManager'])?$datalead[0]['ReportingManager']:"";
				}
			} else {
				$data[$leadMngr]['ReportingManager'] = "";
			}
			
			if($val['PodManager']!="0" && $val['PodManager']!="NULL" && $val['PodManager']!="" && $data[$leadMngr]['Grade'] < 5)
			{
				if($data[$leadMngr]['Grade'] == 4)
				{
					$data[$leadMngr]['PodManager'] = $data[$leadMngr]['Supervisor'];
				}
				else
				{
					$sqlVerMgr = "SELECT CONCAT(employees.first_name,' ',IFNULL(employees.last_name,'')) AS PodManager
					FROM `employees` 
					JOIN podmanager ON podmanager.employee_id = employees.employee_id
					WHERE pod_id = ".$val['PodManager']; 
					$queryVerMgr = $this->db->query($sqlVerMgr);

					$dataVerMgr= $queryVerMgr->result_array(); 
					$data[$leadMngr]['PodManager'] = isset($dataVerMgr[0]['PodManager'])?$dataVerMgr[0]['PodManager']:'';
				}
			} else {
				$data[$leadMngr]['PodManager'] = "";
			}

			if ($reportParam != "") {
				unset($data[$leadMngr]['FirstName']);
			}
		}

		if ($reportParam != "") {
           // $data = $this->sort_col($data, "vertical_Name");
			$columns = array(
				array_keys($data[0])
			);

			$results = array_merge($columns, $data);

			return $results;
		} else {
			$results = array_merge(array(
				'totalCount' => $totCount
			), array(
				'rows' => $data
			));
			//echo $this->db->last_query();
	       // echo"<pre>";print_r($results); exit;
			return $results;
		}

	}

	function getResourceMappingReport()
	{
		// debug($this->input->get());exit;
		$options = array();
		$reportParam = ($this->input->get('Report')) ? $this->input->get('Report') : "";
		$mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');
		$time 	= strtotime($mbrmonth);
		$month 	= date("m",$time);
		$year 	= date("Y",$time);
		
		if ($this->input->get('hasNoLimit') != '1') {
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
		}        
		if($this->input->get('sort'))
		{	
			$sort = json_decode($this->input->get('sort'), true);
			$options['sortBy']        = $sort[0]['property'];
			$options['sortDirection'] = $sort[0]['direction'];
		}
		else
		{
			$options['sortBy'] = 'EmployeeName';
			$options['sortDirection'] = 'ASC';
		}

		$query = "SELECT company_employ_id AS EmployeeID, employees.employee_id, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `EmployeeName`, IFNULL(`client_name`, 'Unassigned') AS ClientName, tbl_rhythm_pod.pod_name AS Pod,tbl_rhythm_services.name AS Service,d.grades, GROUP_CONCAT(CASE WHEN billable = 1 THEN 'Yes' ELSE 'Unassigned' END) AS Billable, IFNULL(`billable_type`, 'Unassigned') AS BillableType, location.name as location
		FROM employees
		LEFT JOIN employee_client ON employees.employee_id =  employee_client.employee_id
		LEFT JOIN employee_pod ON employee_pod.employee_id = employees.employee_id
		LEFT JOIN tbl_rhythm_pod ON tbl_rhythm_pod.pod_id = employee_pod.pod_id
		LEFT JOIN tbl_rhythm_services ON tbl_rhythm_services.service_id = employees.service_id
		LEFT JOIN `client` ON employee_client.client_id =  client.client_id
		LEFT JOIN `location` ON location.location_id =  employees.location_id
		LEFT JOIN designation d ON d.designation_id = employees.designation_id
		WHERE employees.status != 6 AND employees.location_id !=4 ";

		//Search filter starts		
		$filter_options = json_decode($this->input->get('filter'),true);

		$filter_property = array('company_employ_id','first_name','client_name','tbl_rhythm_pod.pod_name','billable','billable_type','location.name');
		$filter_value = isset($filter_options[0]['value']) ? $filter_options[0]['value']:0; 
		// debug($filter_property);exit;

		if($filter_value!='')
		{
			$search_filter = 'AND (';
			foreach ($filter_property as $key => $value) {
				$search_filter .= $value .' LIKE "%'.$filter_value.'%" OR ';		
			}
			$search_filter = substr($search_filter, 0, -4);
			$search_filter .= ')';
			$query = $query.$search_filter;
		}

		$query .= " GROUP BY employees.employee_id, employee_client.client_id"; 
		//echo $query;exit;
		$result = $this->db->query($query);

		$data = $result->result_array();

		 //debug($data);exit;

		$data = $this->highGradeReport($data);

		$totCount = count($data); 
		//debug($data);exit;

		$sorted_data = array();
		foreach ($data as $data_key => $data_value)
		{
			$sorted_data[$data_key] = $data_value[$options['sortBy']];
		}
		
		if ($options['sortDirection'] == 'ASC') 
		{		
			array_multisort($sorted_data, SORT_ASC , $data);	
		}
		else
		{
			array_multisort($sorted_data, SORT_DESC , $data);
		}
		

		// debug($data);exit;

		if ($reportParam != "") 
		{
			foreach ($data as $data_key => $data_value) {
				unset($data[$data_key]['employee_id']);
				unset($data[$data_key]['grades']);
			}
			// debug($data);exit;

			$columns = array(
				array_keys($data[0])
			);

			$results = array_merge($columns, $data);

			return $results;
		}
		else
		{
			if (isset($options['limit']) && isset($options['offset'])) 
			{
				$data = array_slice($data,$options['offset'],$options['limit'],false);
			} 

			$results = array_merge(array(
				'totalCount' => $totCount
			), array(
				'rows' => $data
			));
		}
		// debug($results);exit;
		return $results;
	}


	function highGradeReport($data)
	{
		// debug($data);exit;
		$employees_data = $data;
		$highGradeEmpData = array();

		foreach ($employees_data as $e_key => $e_value) 
		{
			// debug($e_value);exit;

			if($e_value['ClientName']=='Unassigned')
			{
				$employees_data[$e_key]['Billable'] = 'Unassigned';
				$employees_data[$e_key]['BillableType'] = 'Unassigned';
			}
			else
			{
				if($e_value['BillableType'] == 'Buffer')
				{
					$employees_data[$e_key]['Billable'] = 'No';
				}
				else
				{
					$employees_data[$e_key]['Billable'] = 'Yes';
					// $employees_data[$e_key]['BillableType'] = $e_value['BillableType'];
				}
			}

			if( $e_value['grades']>2 && $e_value['grades']<7)
			{
				$e_value['e_key'] = $e_key;
				$highGradeEmpData[] = $e_value;
			}
		}

		// return $employees_data;
		// debug($highGradeEmpData);exit;
		// debug($employees_data);exit;

		$wor_sql = "SELECT client,client_name,team_lead,delivery_manager,associate_manager 
		FROM wor w
		lEFT JOIN client c ON c.client_id = w.client
		WHERE w.status = 'open'";

		$wor_query = $this->db->query($wor_sql);

		$wor_result = $wor_query->result_array();
		$wor_result = array_unique($wor_result, SORT_REGULAR);

		// debug($wor_result);exit;

		$highGradeWorData = array();
		
		foreach ($highGradeEmpData as $hg_key => $hg_value) 
		{
			$company_employ_id = $hg_value['EmployeeID'];
			$employee_id = $hg_value['employee_id'];
			$employee_name = $hg_value['EmployeeName'];
			$pod = $hg_value['Pod'];
			$service = $hg_value['Service'];
			$grade = $hg_value['grades'];
			$location = $hg_value['location'];
			$Billable = $hg_value['Billable'];
			$BillableType = $hg_value['BillableType'];
			if($Billable == 'Unassigned' && $BillableType == 'Unassigned')
			{
				$Billable = 'No';
				$BillableType = 'Buffer';
			}

			if($grade > 2 && $grade < 4)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$team_lead = explode(',',$wor_res_value['team_lead']);
					if(in_array($employee_id, $team_lead))
					{
						$grades_wor_data['EmployeeID']		= $company_employ_id;
						$grades_wor_data['employee_id']		= $employee_id;
						$grades_wor_data['EmployeeName']	= $employee_name;
						$grades_wor_data['ClientName']		= $wor_res_value['client_name'];
						$grades_wor_data['Pod']		= $pod;
						$grades_wor_data['Service']		= $service;
						$grades_wor_data['grades']			= $grade;
						$grades_wor_data['Billable']		= $Billable;
						$grades_wor_data['BillableType']	= $BillableType;
						$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
						$highGradeWorData[] = $grades_wor_data;
						unset($employees_data[$hg_value['e_key']]);
					}
				}
			}
			elseif($grade >= 4 && $grade < 5)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$associate_manager = explode(',',$wor_res_value['associate_manager']);
					if(in_array($employee_id, $associate_manager))
					{
						$grades_wor_data['EmployeeID']		= $company_employ_id;
						$grades_wor_data['employee_id']		= $employee_id;
						$grades_wor_data['EmployeeName']	= $employee_name;
						$grades_wor_data['ClientName']		= $wor_res_value['client_name'];
						$grades_wor_data['Pod']		= $pod;
						$grades_wor_data['Service']		= $service;
						$grades_wor_data['grades']			= $grade;
						$grades_wor_data['Billable']		= 'No';
						$grades_wor_data['BillableType']	= 'Buffer';
						$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
						$highGradeWorData[] = $grades_wor_data;
						unset($employees_data[$hg_value['e_key']]);
					}
				}
			}
			elseif($grade >= 5 && $grade < 6)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$delivery_manager = explode(',',$wor_res_value['delivery_manager']);
					if(in_array($employee_id, $delivery_manager))
					{
						$grades_wor_data['EmployeeID']		= $company_employ_id;
						$grades_wor_data['employee_id']		= $employee_id;
						$grades_wor_data['EmployeeName']	= $employee_name;
						// $grades_wor_data['ClientName']		= $wor_res_value['client_name'];
						$grades_wor_data['ClientName']		= "Theorem India Pvt Ltd";
						$grades_wor_data['Pod']		= $pod;
						$grades_wor_data['Service']		= $service;
						$grades_wor_data['grades']			= $grade;
						$grades_wor_data['Billable']		= 'No';
						$grades_wor_data['BillableType']	= 'Buffer';
						$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
						$highGradeWorData[] = $grades_wor_data;
						unset($employees_data[$hg_value['e_key']]);
					}
				}
			}
			elseif($grade >= 6 && $grade < 7)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$grades_wor_data['EmployeeID']		= $company_employ_id;
					$grades_wor_data['employee_id']		= $employee_id;
					$grades_wor_data['EmployeeName']	= $employee_name;
					$grades_wor_data['ClientName']		= "Theorem India Pvt Ltd";
					$grades_wor_data['Pod']		= $pod;
					$grades_wor_data['Service']		= $service;
					$grades_wor_data['grades']			= $grade;
					$grades_wor_data['Billable']		= 'No';
					$grades_wor_data['BillableType']	= 'Buffer';
					$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
					$highGradeWorData[] = $grades_wor_data;
					unset($employees_data[$hg_value['e_key']]);
				}
			}
		}
		// debug($highGradeWorData);exit;
		$mergedData = array_merge($employees_data,$highGradeWorData);
		$mergedData = array_unique($mergedData, SORT_REGULAR);
		$mergedData = array_values($mergedData);
		return $mergedData;
	}

	function mbrReportCreate($uploadFile){
		$upload_path = 'C:/wamp64/www/Rhythm/App/services/download/MBR_Report/';
		$to_send = $upload_path."/".$uploadFile;	
		
		return $to_send;
		
	}
}


?>
