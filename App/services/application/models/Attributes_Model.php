<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attributes_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
		$this->load->model('Rhythm_Model');
	}
	
	function build_query() {
		$filter = json_decode($this->input->get('filter'),true);
		$filterQuery = $this->input->get('query');
			$filterName = "attributes.Name";//$this->input->get('filterName')?$this->input->get('filterName'):"";
			$options['filter'] = $filter;
			
			$this->db->simple_query('SET SESSION group_concat_max_len=150000');

			$AttributesID = $this->input->get('AttributesID') ? $this->input->get('AttributesID') : '';
		/*	$this->db->select("attributes.attributes_id, attributes.name, attributes.type, GROUP_CONCAT(attributes_list.value SEPARATOR '~' ) AS ListValues, GROUP_CONCAT(attributes_list.status SEPARATOR '~' ) AS ValuesStatus",false);
			$this->db->from("attributes");
			$this->db->join('attributes_list', 'attributes.attributes_id = attributes_list.attributes_id');*/

			$this->db->select("attributes_id, label_name as name, field_type as type, description");
			$this->db->from("new_attributes");
			//$this->db->order_by("secquence", "asc");
			
			if($AttributesID){
				$this->db->where("new_attributes.attributes_id",$AttributesID);
			}

			$filterWhere = '';
			if(isset($options['filter']) && $options['filter'] != '') {
				foreach ($options['filter'] as $filterArray) {
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) {
						if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) {
							$filterWhere .= ($filterWhere != '')?' OR ':'';
							$filterWhere .= $filterField;
							$filterWhere .=" LIKE '%".$filterArray['value']."%'";
						}
					}
				}
				
				if($filterWhere != '') {
					$this->db->where('('.$filterWhere.')');
				}
				
			}
			if($filterName !="" && $filterQuery!="" ){
				$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
				$this->db->where($filterWhere);
			}				
			$this->db->group_by("new_attributes.attributes_id"); 
		}

		function view()
		{
			// debug($this->input->get());exit;
			$options = array();
			if($this->input->get('hasNoLimit') != '1')
			{
				$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
				$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 15;
			}

			$filter = json_decode($this->input->get('filter'),true);
			$options['filter'] = $filter;

			$sort = json_decode($this->input->get('sort'),true);					
			$options['sortBy'] = $sort[0]['property'];
			$options['sortDirection'] = $sort[0]['direction'];

			$sql = "SELECT label_name as name,attributes_id, description, new_attributes.values, field_type AS type
			FROM new_attributes 
			WHERE 1 = 1 ";	

			$filterWhere = '';
			if(isset($options['filter']) && $options['filter'] != '') 
			{
				foreach ($options['filter'] as $filterArray) 
				{
					$filterFieldExp = explode(',', $filterArray['property']);
					foreach($filterFieldExp as $filterField) 
					{
						if($filterField != '') {
							$filterWhere .= ($filterWhere != '')?' OR ':'';
							$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
						}
					}
				}
				
				if($filterWhere != '') 
				{
					$filterWhere = ' ( '.$filterWhere.' ) ';
					$sql .= " and (".$filterWhere.")";
				}
				
			}
			if($filterName !="" && $filterQuery!="" ){
				$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
				$this->db->where($filterWhere);
			}

			if(isset($options['sortBy'])) 
			{
				if ($options['sortBy'] == 'Field Name') $options['sortBy'] = 'label_name';
				if ($options['sortBy'] == 'Field Description') $options['sortBy'] = 'description';

				$sort = $options['sortBy'].' '.$options['sortDirection'];
				$sql .= " ORDER BY ".$sort;
			}

			$query = $this->db->query($sql);
			$totCount = $query->num_rows();

			if (isset($options['limit']) && isset($options['offset'])) 
			{
				$limit = $options['offset'] . ',' . $options['limit'];
				$sql .= " LIMIT " . $limit;
			} 
			else if (isset($options['limit'])) 
			{
				$sql .= " LIMIT " . $options['limit'];
			}

			$query = $this->db->query($sql);
			$data = $query->result_array();
			// debug($data);exit;

			$results = array_merge(array('totalCount' => $totCount),array('rows' => $data));
			return $results;

		}

		public function getClientAttributes()
		{
			// print_r($this->input->get());exit;
			$wor_id = $this->input->get('wor_id');
			$client_id = $this->input->get('client_id');
			$project_type_id = $this->input->get('project_type_id');
			$role_id = $this->input->get('role_id');
			if($this->input->get('searchChar'))
			{
				$searchChar = $this->input->get('searchChar');
				$sql_searchChar = " AND (NA.label_name LIKE '%$searchChar%' OR NA.description LIKE '%$searchChar%')";
			}

			$sql1 = "SELECT label_name AS name,attributes_id,description,field_type AS type,true AS selected, true AS mandatory 
			FROM new_attributes NA
			WHERE 1=1 $sql_searchChar
			ORDER BY name ASC";
			// echo $sql1;exit;
			$result1 = $this->db->query($sql1)->result_array();
			// debug($result1);exit;

			$sql2 = "SELECT NA.label_name AS name,NA.attributes_id,NA.description,NA.field_type AS type, CA.mandatory,IF(CA.attributes_id > 0, 'checked', 'checked') AS selected_checkbox
			FROM client_attributes CA
			LEFT JOIN new_attributes NA ON CA.attributes_id = NA.attributes_id
			WHERE CA.wor_id = '$wor_id' AND CA.client_id = '$client_id' AND CA.fk_project = '$project_type_id' AND CA.fk_role = '$role_id' $sql_searchChar";
			$result2 = $this->db->query($sql2)->result_array();
			// debug($result2);exit;

			$keyed1 = $keyed2 = array();
			foreach ($result1 as $row) {
				$keyed1["a{$row['attributes_id']}"] = $row;
			}
			foreach ($result2 as $row) {
				$keyed2["a{$row['attributes_id']}"] = $row;
			}
			$result = array_values(array_replace_recursive($keyed1, $keyed2));
			$sorted_array = array();
			foreach ($result as $key => $row)
			{
				$sorted_array[$key] = $row['selected_checkbox'];
			}
			array_multisort($sorted_array, SORT_DESC, $result);
			foreach ($result as $key => $value) {

				if($value['mandatory']==0)
					unset($result[$key]['mandatory']);

				if(!$value['selected_checkbox'])
					$result[$key]['selected_checkbox'] = 'unchecked';
			}
			$data['totalCount'] = count($result);
			$data['rows'] = $result;
			// debug($data);exit;
			return $data;
		}

		
		private function _isRecordExists($id="",$name) {
			
			$query = $this->db->select("attributes_id")->where("name",trim(strtolower($name)));
			if($id!="")
				$this->db->where("attributes_id  != ",trim($id));
			$query = $this->db->get('attributes');	
			return $query->num_rows();
		}
		

		function updateAttributes( $idVal = '', $data = '' ) {
			
			
			$setData = json_decode(trim($data), true);	

			if(is_array($setData['ListValues']))
			{
				$listValuesStatus = array_combine($setData['ListValues'], $setData['ValuesStatus']);
			}
			else 
			{
				$listValuesStatus = '';
			}

			if($this->_isRecordExists($idVal,$setData['name'])== 0)
			{
				$where = array();
				$set = array();
				$where['attributes_id'] = $idVal;

				$dataset = array(
					'attributes_id'=> $setData['attributes_id'],
					'name'=> $setData['name'],
					'type'=>$setData['type']
				);
				
				$retVal = $this->update($dataset, $where, 'attributes');
				$this->delete(array('attributes_id'=>$idVal), 'attributes_list');			

				if(is_array($listValuesStatus))
				{
					
					foreach ($listValuesStatus as $key=>$value)
					{

						if($key != '')
						{		
							
							$dataInfo = array(
								"attributes_id"=>$setData['attributes_id'],
								"value" => $key,
								"status" => $value
							);						

							$retVal = $this->insert($dataInfo, 'attributes_list');
							
						}

					}
				}
				else
				{
					$dataInfo = array(
						"attributes_id"=>$setData['attributes_id'],
						"value" => $setData['ListValues']
					);
					$retVal = $this->insert($dataInfo, 'attributes_list');
				}
				return $retVal;
			}
			else
				return -1;
		}

		
		function deleteAttributes( $idVal = '' ) {
			$options = array();
			$options['attributes_id'] =  $idVal;
			$retVal = $this->delete($options, 'attributes');
			return $retVal;
		}

		function addAttributes( $data = '' ) {
			
			$addAttributes = json_decode(trim($data), true);
			
			if($this->_isRecordExists("",$addAttributes['name'])== 0)
			{

				$dataset = array(
					'name'=> $addAttributes['name'],
					'type'=>$addAttributes['type']
				);
				$retVal = $this->insert($dataset, 'attributes');
				$newInsertID = $this->insertId;
				if(is_array($addAttributes['ListValues']))
				{
					$addAttributes['ListValues'] = array_values(array_filter($addAttributes['ListValues']));
					foreach ($addAttributes['ListValues'] as $val)
					{
						$dataInfo = array(
							"attributes_id"=>$newInsertID,
							"value" => $val
						);
						$retVal = $this->insert($dataInfo, 'attributes_list');
					}
				}
				else
				{
					$dataInfo = array(
						"attributes_id"=>$newInsertID,
						"value" => $addAttributes['ListValues']
					);
					$retVal = $this->insert($dataInfo, 'attributes_list');
				}
				//add the action log table.
				// $this->log_action('attributes','add',$dataset['Name']);
				return $retVal;
			}
			else
				return -1;
		}

		function saveclientattributes() 
		{
			$empData    	= $this->session->userdata('user_data');
			$emp_id			= $empData['employee_id'];
			$project_type_id = ($this->input->get('project_type_id')) ? $this->input->get('project_type_id') : '';
			$role_type_id 	= ($this->input->get('role_type_id')) ? $this->input->get('role_type_id') : '';
			$clientID   	= ($this->input->get('ClientID')) ? $this->input->get('ClientID') : '';
			$WorID   		= ($this->input->get('WorID')) ? $this->input->get('WorID') : '';
			
			$row_data = ($this->input->get('RowData')) ? $this->input->get('RowData') : '';
			$row_data = json_decode($row_data,true);

			$where = array(
				'wor_id'=> $WorID,
				'client_id'=> $clientID,
				'fk_project'=> $project_type_id,
				'fk_role'=> $role_type_id,
			);

			$query = $this->db->delete('client_attributes',$where);

			foreach ($row_data as $key => $value) 
			{
				$attribute_id = $value['attribute_id'];
				$mandatory = ($value['mandatory']==true)?1:0;
				$visible = ($value['visible']==true)?1:0;

				$this->db->set('wor_id', $WorID);
				$this->db->set('client_id', $clientID);
				$this->db->set('attributes_id', $attribute_id);
				$this->db->set('fk_project', $project_type_id);
				$this->db->set('fk_role', $role_type_id);
				$this->db->set('mandatory', $mandatory);
				$this->db->set('visible', $visible);
				$this->db->insert('client_attributes');

			}
			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Work Order ID:'.$WorID.') has been updated configure time entry fields.';
			$this->userLogs($logmsg);
			
			return 1;
		}
	}
	?>