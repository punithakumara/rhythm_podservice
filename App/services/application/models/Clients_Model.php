<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Clients_Model extends INET_Model
{
	public $uploadPath = "";
	
	function __construct()
	{
		parent::__construct();
		$this->config->load('admin_config');
		$this->uploadPath = $this->config->item('REPOSITORY_PATH').'resources/uploads/client/';
		
	}

    /**
	* Get list of Client WOR
	*
	* @param 
	* @return array
	*/ 
	function list_clients()
	{
		error_reporting(E_ALL);
		
		$filter				= json_decode($this->input->get('filter'),true);
		$filterQuery 		= $this->input->get('query');
		$filterName			= $this->input->get('filterName')?$this->input->get('filterName'):"";
    	//$WorID			= $this->input->get('WorID')?$this->input->get('WorID'):"";
		$comboAMRes			= $this->input->get('comboAMRes')?$this->input->get('comboAMRes'):"";
		$filterQuery 		= $this->input->get('query');
		$sort				= json_decode($this->input->get('sort'),true);
		$clientStatusCombo 	= $this->input->get('clientStatusCombo');

		$categorization 	= $this->input->get('categorization');

		isset($clientStatusCombo) ? $clientStatusCombo : '';
		
		$empData    = $this->input->cookie();

		$options = array();
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		$options['filter']	= $filter;
		
		$sortBy = $sort[0]['property'];
		$sortDirection = $sort[0]['direction'];
		
		$people = array(360, 451, 230, 333, 2161, 2691, 490,230,308,662,1442,2324,2754); //Domain Expert

		$sql = "SELECT client.`client_id`,parent_company,`head_count`,`client_name`,DATE_FORMAT(client.created_on, '%m-%d-%Y')  AS `created_on`, `web`, `city`, `state`, `country`, `logo`, client.`status`, `created_date`, GROUP_CONCAT(DISTINCT (verticals.name)) AS vertical_name, GROUP_CONCAT(DISTINCT (verticals.vertical_id)) AS vertical_id, `created_date`,GROUP_CONCAT(DISTINCT(client_attributes.attributes_id)) AS Attribute_Names, `industry_type`,`acc_manager`,client.client_partner,`client_tier`,`client_description` , `inactive_date`,`reason`
		FROM `client`
		LEFT JOIN client_attributes ON client_attributes.client_id = client.client_id
		LEFT JOIN client_vertical ON client_vertical.client_id = client.client_id
		LEFT JOIN verticals ON client_vertical.vertical_id = verticals.vertical_id ";
		
		$sql .=	" LEFT JOIN employee_client ON employee_client.client_id = client.client_id";
		$sql .=	" LEFT JOIN wor ON wor.client = client.client_id";
		
		$filterWhere = '';
		if($clientStatusCombo !="" && $empData['is_onshore']!=1)
		{
			$sql .= " WHERE client.`status` = ".$clientStatusCombo;
		}
		else if($empData['is_onshore']!=1)
		{
			$sql .= " WHERE client.`status` = 1";
		}
		else if($empData['is_onshore']==1)
		{
			$sql .= " WHERE client.`onshore_client` = '1'";
		}

		if($empData['Grade']>2 && $empData['employee_id'] != 41 && $empData['employee_id'] != 2269 && $empData['employee_id'] != 117 && $empData['Grade']<4 && $empData['is_onshore']!=1)
		{
			$sql .= " AND (employee_client.employee_id=".$empData['employee_id'];
			$sql .= " OR wor.team_lead REGEXP '[[:<:]]".$empData['employee_id']."[[:>:]]')";
		}
		else if($empData['Grade']==4 && $empData['employee_id'] != 41 && $empData['employee_id'] != 2269 && $empData['employee_id'] != 117 && $empData['is_onshore']!=1)
		{
			$sql .= " AND (employee_client.employee_id=".$empData['employee_id'];
			$sql .=" OR wor.associate_manager REGEXP '[[:<:]]".$empData['employee_id']."[[:>:]]')";
		}
		else if($empData['Grade']<3 && $empData['employee_id'] != 41 && $empData['employee_id'] != 2269 && $empData['employee_id'] != 117 && $empData['is_onshore']!=1)
		{
			$sql .=" AND employee_client.employee_id=".$empData['employee_id'];
		}

		if($empData['Grade']<5 && $empData['employee_id'] != 41 && $empData['employee_id'] != 2269 && $empData['employee_id'] != 117 && $empData['is_onshore']!=1)
			$sql .= " AND wor.status = 'open'";
		
		if($comboAMRes !="")
		{
			$sql .=" AND client.`client_id` IN ( SELECT `client_id` FROM employee_client WHERE employee_id = ".$comboAMRes.")";
		}
		
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if($filterField != '') 
					{
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}
		}

		if($filterName !="")
		{
			$sql .=" AND ". $filterName." LIKE '%".$filterQuery."%'";    			
		}

		if($filterWhere !="")
		{
			$sql .= " AND (".$filterWhere.")";    			
		}

		$sql .= " GROUP BY client_id";
		
		if($sortBy!="" && $sortDirection!="")
		{
			$sql .= " ORDER BY ". $sortBy." ".$sortDirection;
		}
		else
		{
			$sql .= " ORDER BY client_name ASC";
		}
		//echo $sql; exit;
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " LIMIT ".$limit;
		}
		
		$query = $this->db->query($sql);
		// if($empData['is_onshore']==1){	
		// 	$data[0] = "";
		// 	$data = array_merge($data, $query->result_array());
	    // } else {
	    	$data = $query->result_array();
	    //}

		if (isset($value['logo']) && (!is_null($value['logo']))) 
		{
			if (file_exists($this->uploadPath.$value['logo'])) 
			{
				$data[$key]['imgLogo'] = $value['logo'];
			}
		}
		
		
		$results = array_merge(
			array('totalCount' => $totCount), 
			array('rows' => $data)
		);
		//var_dump($results);
		
		return $results;
	}
	
	function GroupWorComboValues()
	{
		$ClientID = ($this->input->get('ClientID') != '')? implode(",",json_decode($this->input->get('ClientID'))) : '';
		$filter = json_decode($this->input->get('filter'),true); //filter functionality
		$status = ($this->input->get('status')!="")?$this->input->get('status'):'All'; //wor_id		
		$work_order_id = ($this->input->get('work_order_id')!="")?$this->input->get('work_order_id'):"";		
		$all_wor_ids = ($this->input->get('all_wor_ids')!="")?$this->input->get('all_wor_ids'):0;		
		$open_wor = ($this->input->get('open_wor')!="")?$this->input->get('open_wor'):0;
		$empData    = $this->input->cookie();
		
		$options = array();
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		/* FilterQuery Added  to Filter Vlues in Dropdown in COR*/
		$filterQuery 		= $this->input->get('query');
		
		$sql = "SELECT wor.wor_id, wor.work_order_id
		FROM wor JOIN `client` c ON c.`client_id` = wor.`client`
		LEFT JOIN employees cp ON cp.employee_id = wor.client_partner
		LEFT JOIN employees em ON em.employee_id = wor.engagement_manager
		LEFT JOIN employees dm ON dm.employee_id = wor.delivery_manager
		LEFT JOIN employees sh ON sh.employee_id = wor.stake_holder
		LEFT JOIN employees tl ON tl.employee_id = wor.team_lead
		LEFT JOIN employees am ON am.employee_id = wor.associate_manager
		LEFT JOIN employees e ON e.employee_id = wor.created_by
		WHERE 1 = 1";
		
		if(!$all_wor_ids)
		{
			if ($status != "All")
			{
				$sql .= " And wor.status='".$status."'";
			}
			else
			{
				$sql .= " And wor.status <> 'Delete' ";
			}		
		}
		
		if ($work_order_id != "")
		{
			$sql .= " AND wor.status <> 'Delete' AND wor.work_order_id = '".$work_order_id."'";
		}
		
		if ($open_wor == "1")
		{
			$sql .= " And wor.status <> 'Delete' AND wor.status <> 'Closed' AND wor.status <> 'In-Progress' ";
		}
		if($empData['Grade']>= 3 && $empData['Grade'] <=4 ){
			$sql .= " And wor.client IN ('".$empData['ClientId']."') AND wor.wor_id IN ('".$empData['WorId']."')";

		}
		if($ClientID !=""){
			$sql .=" AND wor.client IN ('".$ClientID."')";	
		}
		/* start search functionality */		
		$filterWhere = '';
		if(isset($filter) && $filter != '') {

			foreach ($filter as $filterArray) {
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) {
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) {
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= $filterField." LIKE '%".$filterArray['value']."%'";
					}
				}
			}

			if($filterWhere != '') {
				$sql .= " AND ";	
				$sql .= '('.$filterWhere.')';
			} 
		}
		/* end search functionality */	
		
		/* FilterQuery Added  to Filter Vlues in Dropdown in COR*/
		if($filterQuery !="")
		{
			$sql .=" AND (wor.work_order_id  LIKE '%".$filterQuery."%')";
		}
		
		$sql .= " ORDER BY wor.created_on DESC ";
		
		$query = $this->db->query($sql);
		$totCount = $query->num_rows();
		
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$limit = $options['offset'].','.$options['limit'];
			$sql .= " LIMIT ".$limit;
		}
		
		$query = $this->db->query($sql);
		$data = $query->result_array();

		$results = array_merge(
			array('totalCount' => $totCount),
			array('rows' => $data)
		);

		return $results;
	}	
	
	
	function getClientWorkModels($data)
	{
		foreach ($data as $key => $value) 
		{
			$sqlWorkModel = 'SELECT * FROM `wor_cor_transition` WHERE `client_id` ='.$value['client_id']; 
			$query = $this->db->query($sqlWorkModel);
			$workModeldata = $query->result_array();
			$finalworkmodel = array();	
			if(!empty($workModeldata))
			{
				foreach ($workModeldata as $k => $v)
				{
					$sqlee = 'SELECT * FROM `wor_cor_transition` WHERE `work_order_id` ="'.$v['work_order_id'].'" AND `client_id` ="'.$value['client_id'].'" ';
					$query1 = $this->db->query($sqlee);
					$worsqldata = $query1->result_array();
					
					if(!empty($worsqldata))
					{
						$worsqldata = end($worsqldata);												
						if($worsqldata['fte_id'] != "") $finalworkmodel[$key][$k]['f'] = 'FTE';
						if($worsqldata['hourly_based_id'] != "") $finalworkmodel[$key][$k]['h'] = 'Hourly';
						if($worsqldata['volume_base_id'] != "") $finalworkmodel[$key][$k]['v'] = 'Volume';	
						if($worsqldata['project_model_id'] != "") $finalworkmodel[$key][$k]['p'] = 'Project';
					}
				}
				$oneDimensionalArray = array_map('current', $finalworkmodel);
			}	
			
			$finalArray = array();

			if(!empty($finalworkmodel))
			{
				foreach ($finalworkmodel as $ka => $va)
				{
					foreach ( $finalworkmodel[$ka] as $kri => $vri)
					{						
						foreach ( $vri as $vri_key => $vri_val)
						{	
							$finalArray[$vri_key] = $vri_val;
						}
					}
				} 

				$data[$key]['work_model'] = implode(", ",$finalArray);
				$data[$key]['work_model_array'] = array_values($finalArray);	
			}
			else
			{
				$data[$key]['work_model'] = '';
				$data[$key]['work_model_array'] = '';
			}
		}	
		return $data;	
	}
	
	function getLeads() 
	{
		$ClientID 	= $this->input->get('ClientID') ? $this->input->get('ClientID') : '';
		$filterName = $this->input->get('filterName') ? $this->input->get('filterName') : '';
		$filterVal 	= $this->input->get('query') ? $this->input->get('query') : '';
		$grades 	= ($this->input->get('Grades')) ? $this->input->get('Grades') : '';
		$EmployID 	= ($this->input->get('EmployID')) ? $this->input->get('EmployID') : '';

		$sql = " SELECT DISTINCT( employees.`employee_id` ), CONCAT(IFNULL(employees.`first_name`, ''), ' ',IFNULL(employees.`last_name`, '')) AS Name, designation.grades as Grade FROM employees 
		LEFT JOIN designation ON designation.designation_id = employees.designation_id 
		LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
		WHERE employees.status IN(3)";
		
		if($grades=='Mgr')
		{
			$sql .=" AND designation.grades >= 5 AND designation.grades < 8";
		}
		else if($grades=='AM')
		{
			$sql .=" AND employee_client.grades = 4";
		}
		else  if($grades=='TL')
		{
			$sql .=" AND employee_client.grades BETWEEN 3 AND 3.1";
		}
		
		if($ClientID!="")
		{
			$sql .= " AND employee_client.client_id = ".$ClientID;	 
		}
		
		if($EmployID!="")
		{
			$sql .= " AND employees.employee_id != ".$EmployID;	 
		}

		if($filterVal!="") 
		{
			if($filterName == 'Name') 
			{
				$sql .= " AND CONCAT(IFNULL(employees.`first_name`, ''), ' ', IFNULL(employees.`last_name`, '')) LIKE '%".$filterVal."%'";
			} 
			else 
			{
				$sql .= " AND $filterName LIKE '%".$filterVal."%'";
			}
		}
		
		$sql .= " ORDER BY Name ASC";
		$query = $this->db->query($sql);
		
		return $query->result_array();
	}

	function getCountries() 
	{
		$sql = "SELECT * FROM country ORDER BY country_name ASC";
		$query = $this->db->query($sql);

		return $query->result_array();
	}	
	
	private function _isRecordExists($id="",$name)
	{
		$query = $this->db->select("client_id")->where("client_name",trim(strtolower($name)));
		if($id!="")
			$this->db->where("client_id  != ",trim($id));
		$query = $this->db->get('client');

		return $query->num_rows();
	}
	
	function saveClient( $saveData = '' ) 
	{
		if($saveData['country_array']!=''){
			$saveData['country']= $saveData['country_array'];
		}
		$empData    = $this->input->cookie();
		$saveData['created_by'] = $empData['employee_id'];
		$saveData['inactive_date'] = null;
		$exp_web = explode("/",$saveData['web']);
		if(isset($exp_web[2]) && $exp_web[2] != ''){
			$saveData['web'] = $exp_web[2];
		}else{
			$saveData['web'] = $exp_web[0];
		}
		if($saveData['acc_manager'] == 'Select Account Manager' || $saveData['acc_manager'] == "") {
			$saveData['acc_manager']  = 0;
		}
		if($saveData['client_partner'] == 'Select Client Partner' || $saveData['client_partner'] == "") {
			$saveData['client_partner']	= 0;
		}
		if($saveData['client_tier'] == 'Select Tier' || $saveData['client_tier'] == "") {
			$saveData['client_tier']	= 0;
		}
		if($saveData['client_description'] == "undefined") {
			$saveData['client_description']	= '';
		}
		
		$uploadResp = '';
		if($_FILES['NewLogo']['name'] != '')
		{
			if($saveData['logo'] != '') 
			{
				if(file_exists($this->uploadPath.$saveData['logo'])) 
				{
					unlink($this->uploadPath.$saveData['logo']);
				}
			}
			$uploadResp = $this->upload_files($_FILES, $this->uploadPath);
			$uploadData = $this->upload->data();
			
			$saveData['logo'] = $uploadData['file_name'];
			chmod($this->uploadPath.$uploadData['file_name'], 0775);
		}
		/* Mapping Client & vertical*/ 
		$service_array = array();	
		if($saveData['service_array']!= "")
		{
			$service_array = explode(",",$saveData['service_array']);		
		}
		else
		{
			$service_array = "";
		}

		if(($saveData['client_id'] == '') || empty($saveData['client_id'])) 
		{
			if($this->_isRecordExists("",$saveData['client_name'])==0)
			{
				
				$saveData['created_on'] = date('Y-m-d', strtotime($saveData['created_on']));
				if($saveData['industry_type'] == 'Client Type'){
					$saveData['industry_type'] ='';
				}
				
				//create client short code by taking first three and last three characters from client name.
				if($saveData['client_name'])
				{
					$cn_sr =  str_replace (' ', '', $saveData['client_name']);					
					$first_three = substr($cn_sr, 0, 3);
					$cn_reverse = strrev($cn_sr);
					$last_three = substr($cn_sr, -2);

					$saveData['short_code'] = strtoupper($first_three.$last_three);
				}
				
				$saveResp = $this->insert($saveData, 'client');
				$lastInsertID = $this->getInsertId();
				
				if(is_array(json_decode($saveData['Attribute_Names'])))
				{	
					$attrNames = json_decode($saveData['Attribute_Names']);
					/* START Code to get mulitple process attributes */
					$attrArray = array();					
					foreach ($attrNames as $attrName=>$attrValues)
					{						
						if(is_array($attrValues))
						{
							foreach($attrValues as $attrValue)
							{
								$attrArray[] =  $attrValue;
							}
						}
						else
						{
							$attrArray[] = $attrValues;
						}
					}	

					$query = $this->db->query('SELECT attributes_id FROM new_attributes WHERE label_name IN ("' . implode('", "', $attrArray) . '") GROUP BY label_name ORDER BY FIELD(label_name, "' . implode('", "', $attrArray) . '" )');				
					$attrNames = $query->result_array();
					$attrIDs = $this->arrayFilter($attrNames, 'attributes_id');
					$this->__addProcessChilds($lastInsertID, $attrIDs, 'client_attributes', 'attributes_id');
				}	
			}
			else
			{
				$saveResp = -1;
				$lastInsertID = "";
			}

			$empData    = $this->input->cookie();
	        $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Client ID: '.$lastInsertID.') has been added client successfully.';
	        $this->userLogs($logmsg);
		}
		else 
		{
			// debug($saveData);exit;
			$where = array();
			$set = array();

			$where['client_id'] = $saveData['client_id'];
			if($saveData['industry_type'] == 'Client Type'){
				$saveData['industry_type'] ='';
			}
			$saveData['updated_by'] = $empData['employee_id'];
			//capturing date while inactiving
			if($saveData['status'] == 0){
				$saveData['inactive_date'] = date('Y-m-d H:i:s'); 
			}				
			if(($saveData['created_on'] == "Client Since" || empty($saveData['created_on']) ))
			{ 
				unset($saveData['created_on']);
			}
			else
			{	
				$saveData['created_on']= date("Y-m-d H:i:s", strtotime($saveData['created_on']));
			}
			if($this->_isRecordExists($saveData['client_id'],$saveData['client_name'])==0) 
			{
				$saveData['updated_on'] = date('Y-m-d H:i:s');

				$saveResp = $this->update($saveData, $where, 'client');
				$lastInsertID = $saveData['client_id'];
				if ($saveData['status'] == 1) 
				{ 
					$this->updateInactiveDate($saveData['client_id']); 
				}	
				
				if(is_array(json_decode($saveData['Attribute_Names'])))
				{	
					$attrNames = json_decode($saveData['Attribute_Names']);

					/*delete Previous Records */
					if (!empty($saveData['client_id'])) 
					{
						/*Deleting The Attributes*/
						$this->db->where_in('client_id', $saveData['client_id']);
						$this->db->delete('client_attributes');
					}

					/* START Code to get mulitple process attributes */
					$attrArray = array();					
					foreach ($attrNames as $attrName=>$attrValues)
					{						
						if(is_array($attrValues))
						{
							foreach($attrValues as $attrValue)
							{
								$attrArray[] =  $attrValue;
							}
						}
						else
						{
							$attrArray[] = $attrValues;
						}
					}	

					$query = $this->db->query('SELECT attributes_id FROM new_attributes WHERE label_name IN ("' . implode('", "', $attrArray) . '") GROUP BY label_name ORDER BY FIELD(label_name, "' . implode('", "', $attrArray) . '" )');				
					$attrNames = $query->result_array();
					$attrIDs = $this->arrayFilter($attrNames, 'attributes_id');
					$this->__addProcessChilds($lastInsertID, $attrIDs, 'client_attributes', 'attributes_id');
				}

				$empData    = $this->input->cookie();
		        $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Client ID: '.$saveData['client_id'].') has been updated client successfully.';
		        $this->userLogs($logmsg);

			}
			else
			{
				$saveResp = -1;
				$lastInsertID = "";
			}
		}		

		return array('updateResp' => $saveResp, 'uploadResp' => $uploadResp, 'lastInsertID' => $lastInsertID);
	}
	
	function updateInactiveDate($ClientID)
	{
		$sq ="UPDATE client SET inactive_date = NULL WHERE client_id = ".$ClientID;
		$this->db->query($sq);
	}

	
	function __addProcessChilds( $idVal = '', $data = '', $table, $columnName) 
	{
		$array_data =array();
		if(is_array($data) && count($data)!=0)
		{	
			/* added to test array of columns in  cleints Attributes*/
			$counterattribues = 1;		
			$columns = array();	
			/* added to test array of columns in  cleints Attributes*/
			foreach($data as $asctMngID)
			{
				/* added to test array of columns in  cleints Attributes*/
				if(is_array($columnName))
				{
					$columns['client_id'] = $idVal;
					$columns[$columnName[0]] = $asctMngID;
					$columns[$columnName[1]] = $counterattribues;
					$counterattribues++;
					array_push($array_data,$columns);
				}
				/* added to test array of columns in  cleints Attributes*/					
				else
					array_push($array_data,array('client_id' =>$idVal, $columnName => $asctMngID));
			}
			$retVal = $this->db->insert_batch($table, $array_data);
			return $retVal;
		}
	}
	
	/* get Wor list by clientID */
	function getWorList($client_id)
	{
		$empData = $this->input->cookie();

		$sql = "SELECT `wor_id`, `work_order_id` AS wor_name, CONCAT(work_order_id, ' (', wor_name, ')') AS work_order_name, start_date, end_date 
		FROM `wor` 
		WHERE status='Open' AND client =".$client_id;
		if($empData['Grade']<5 && $empData['employee_id'] != 41 && $empData['employee_id'] != 2269 && $empData['employee_id'] != 117)
		{
			$sql .= " AND wor_id IN(".$empData['WorId'].")";
		}
		
		$query = $this->db->query($sql);

		$totCount = $query->num_rows();
		$data = $query->result_array();
		$results = array_merge(
			array('totalCount' => $totCount),
			array('data' => $data)
		);
		return $results;
	}


	function getUtilClientWorList($clientID)
	{
		$sql = "SELECT DISTINCT wor_id,work_order_id AS wor_name, CONCAT(work_order_id, ' (', wor_name, ')') AS work_order_name, start_date, end_date  
		FROM utilization
		LEFT JOIN wor ON utilization.fk_wor_id = wor.wor_id
		WHERE utilization.fk_client_id = '$clientID'";
		$query = $this->db->query($sql);

		$totCount = $query->num_rows();
		$data = $query->result_array();
		$results = array_merge(
			array('totalCount' => $totCount),
			array('data' => $data)
		);
		return $results;
	}
	/* get Headcount For Billability based on Grades list by clientID */
	function getCategoriseHeadcount($Wor_id,$client_id)
	{
		
		$sql = "SELECT GROUP_CONCAT(IF (fte_id ='', NULL, fte_id)) AS fte_id FROM `wor_cor_transition` JOIN wor ON wor.wor_id = wor_cor_transition.`fk_wor_id`  WHERE  client_id =".$client_id . " AND wor.status = 'Open'";
		if(isset($Wor_id) && ($Wor_id!=null)){
			$sql .=" AND fk_wor_id IN ($Wor_id)";
		}

		$query = $this->db->query($sql);
		
		$row = $query->row();
		
		if(!empty($row->fte_id))
		{
			
			$fte_id = $row->fte_id;
			
			if($fte_id != ""){
				
				$res = "  SELECT SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS headcount FROM `fte_model` WHERE  `fte_id` IN ($fte_id) AND process_id !=0  AND `anticipated_date` <= CURDATE()";
				//echo $res;exit;
				$out = $this->db->query($res);

				$result = $out->row();
				if (!empty($result->headcount))
				{
					$headcount = $result->headcount;
					if($headcount > 0){
						return $headcount;
					}else{
						return 0;
					}
				} else{
					return 0;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}	

		
	}
	/* get  Forecast Nrr & Rdr  For Billability based on Grades list by clientID */
	function GetforecatCategorisedCount($Wor_id,$client_id,$impact)
	{
		
		$sql = "SELECT GROUP_CONCAT(IF (fte_id ='', NULL, fte_id)) AS fte_id FROM `wor_cor_transition` WHERE  client_id =".$client_id;
		if(isset($Wor_id) && ($Wor_id!=null)){
			$sql .=" AND fk_wor_id IN ($Wor_id)";
		}
		$query = $this->db->query($sql);

		$row = $query->row();
		
		if(!empty($row->fte_id))
		{
			
			$fte_id = $row->fte_id;
			if($fte_id != ""){
				$res = "  SELECT SUM(CASE change_type WHEN ".$impact ." THEN no_of_fte  END) AS forecast_count FROM `fte_model` WHERE  `fte_id` IN ($fte_id) AND process_id !=0 AND `anticipated_date` > CURDATE()";
				$out = $this->db->query($res);
				

				$result = $out->row();
				if (!empty($result->forecast_count))
				{
					$headcount = $result->forecast_count;
					if($headcount > 0){
						return $headcount;
					}else{
						return 0;
					}
				} else{
					return 0;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}	

		
	}
	
	/* get Forecast Nrr & Rdr For Billability based on clientID */
	function GetforecatCount($Wor_id,$client_id,$impact)
	{
		$sql = "SELECT  SUM(CASE forecast_impact WHEN ".$impact." THEN forecast_fte END) AS forecast_count FROM `client_forecast` WHERE client_id = '$client_id' AND  start_date > CURDATE() ";
		// echo $sql;echo"<br>";
		$query  = $this->db->query($sql);

		$row = $query->row();
		if (!empty($row->forecast_count))
		{
			$headcount = $row->forecast_count;
			if($headcount > 0){
				return $headcount;
			}else{
				return 0;
			}
		} else{
			return 0;
		}
	}

	function worClientsList()
	{
		$filter = '';
		if($this->input->get('query')!='')
		{
			$search_param = $this->input->get('query');
			$filter = " AND client_name LIKE '%$search_param%'";
		}

		$sql = "SELECT client_name,client_id FROM client 
		WHERE 1=1 AND status = 1 $filter 
		ORDER BY client_name ASC";


		// echo $sql;exit;
		$query  = $this->db->query($sql);

		$data['totalCount'] = $query->num_rows();
		$data['rows'] = $query->result_array();

		return $data;
	}
}
?>