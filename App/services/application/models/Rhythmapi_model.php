<?php
header('Access-Control-Allow-Origin: *');

class Rhythmapi_model extends INET_Model
{
    function __construct()
    {
        parent::__construct();
        //$this->load->model(array('Employee_Model','Client_Model'));
        $this->config->load('admin_config');
    }
    
    function get_employees()
    {		
        $sql = "SELECT employee_id AS `EmployID`, company_employ_id AS `CompanyEmployID`, first_name AS `FirstName`, last_name AS `LastName`, email AS `Email`, designation_id AS `DesignationID`, assigned_Vertical AS `AssignedVertical`, location_id AS `LocationID`, mobile AS `Mobile`, doj AS `DateOfJoin`, '' AS `Picture` FROM employees WHERE status!=6";	
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();$query->result_array();
        }
        return '';
    }

    function get_clients()
    {		
        $sql = "SELECT client_id AS `ClientID`, client_name AS `Client_Name` FROM `client` WHERE status=1";	
        $query = $this->db->query($sql);
        // print_r($query->result_array());exit; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

	// function get_process()
 //    {		
 //        $sql = "SELECT * FROM `process`WHERE ProcessStatus=0";	
	// 	$query = $this->db->query($sql);
 //        if ($query->num_rows() > 0) {
 //            return $query->result_array();
 //        }
 //        return '';
 //    }

    function get_vertical_manager()
    {		
        $sql = "SELECT vertical_id AS `VerticalID`, employee_id AS `EmployID`, shift_id AS `ShiftID` FROM `verticalmanager`";	
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function get_designation_list()
    {		
        $sql = "SELECT designation_id AS `DesignationID`, name AS `Name`, grades AS `grades`, hcm_grade AS `HcmGrade` FROM `designation` WHERE STATUS=1";	
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function get_vertical_list()
    {		
        $sql = "SELECT vertical_id AS VerticalID, name as Name FROM `verticals`";	
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function get_employee_process()
    {		
        $sql = "SELECT process_id AS `ProcessID`, employee_id AS `EmployID`, primary_lead_id AS `Primary_Lead_ID`, grades AS `Grades`, '' AS `ShiftID`,'' AS `PrimaryProcess`, '' AS `BillablePercentage`, billable AS `Billable`, billable_type AS `BillableType`, start_date AS `StartDate`, end_date AS `EndDate`, created_on AS `createdOn`, approved AS `Approved` FROM `employee_client`";	
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function get_location_details()
    {		
        $sql = "SELECT location_id AS `LocationID`, name AS `Name`, address1 AS `Address1`, city AS `City`, state AS `State`, country AS `Country`,email AS `Email`,url AS `URL`,phone as `Phone` FROM location";	
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return '';
    }

    function get_client_utilization_details()
    {
        ini_set('memory_limit', '-1');
        $client_id = 182; //Pandora
        $params = $this->input->get();
        // debug($params);exit;
        $start_date = $end_date = '';
        if(isset($params['start_date']))
            $start_date = date('Y-m-d', strtotime($params['start_date']));
        if(isset($params['end_date']))
            $end_date = date('Y-m-d', strtotime($params['end_date']));

        $sql_utilization = "SELECT DATE_FORMAT(utilization.task_date, '%d-%b-%Y') AS task_date ,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Employee Name`,wor.work_order_id, task_codes.task_code AS task_id  ,utilization.fk_sub_task_id AS sub_task , utilization.utilization_id, utilization.fk_client_id AS client_id ,client.client_name, process.name AS role_code, utilization.request_name ,  utilization.client_hrs ,utilization.theorem_hrs ,utilization.remarks ,  utilization.status,task_codes.task_code AS task_code_value 
        FROM  `utilization`
        LEFT JOIN `employees` ON utilization.employee_id = `employees`.`employee_id`
        LEFT JOIN `wor` ON utilization.fk_wor_id = `wor`.`wor_id` 
        LEFT JOIN task_codes  ON task_codes.task_id = utilization.fk_task_id
        LEFT JOIN `client` ON utilization.fk_client_id = `client`.`client_id`
        LEFT JOIN project_types ON project_types.project_type_id = utilization.fk_project_id
        LEFT JOIN process ON process.process_id = utilization.fk_role_id
        WHERE utilization.fk_client_id='$client_id'";

        if($start_date != '' && $end_date != '')
            $sql_utilization .= " AND utilization.task_date BETWEEN '$start_date' AND '$end_date'";
        
        $sql_utilization .= " ORDER BY utilization.utilization_id DESC";

        // $sql_utilization .= " LIMIT 11332";
        // $sql_utilization .= " LIMIT 0,10000";

        // echo $sql_utilization;exit;

        $client_attrib = $this->client_attributes($client_id);

        $query = $this->db->query($sql_utilization);
        $result = $query->result_array();

        if ($query->num_rows() == 0) {
            return '';
        } 
        else
        {
            // debug($result);exit;
            foreach($result as $key => $value)
            {
                unset($result[$key]['task_code_value']);

                if($value['status']==0)
                    $result[$key]['status'] = "Pending";
                if($value['status']==1)
                    $result[$key]['status'] = "Approved";
                if($value['status']==2)
                    $result[$key]['status'] = "Rejected";
                if($value['status']==3)
                    $result[$key]['status'] = "Weekend Hours";
                if($value['status']==4)
                    $result[$key]['status'] = "Client Holiday Hours";

                $AdditionalAttributes = $this->GetAttributesValues($value['client_id'] , $value['utilization_id']);
                $result[$key]['AdditionalAttributes'] = array_merge($client_attrib,$AdditionalAttributes);

                $regex="/^[0-9,]+$/";
                if(!preg_match($regex,$value['sub_task']))
                {
                    $sub_task = "";
                }
                else
                {
                    $sub_task = $value['sub_task'];
                }

                $result[$key]['sub_task']   = $this->getSubtaskNames($sub_task);
            }
            // debug($result);exit;
            return $result;
        }      



    }

    private function client_attributes($clientID)
    {
        $sqlattri = "SELECT label_name,field_name FROM client_attributes  LEFT JOIN new_attributes ON new_attributes.attributes_id = client_attributes.attributes_id WHERE client_id = ".$clientID;
        $attrires = $this->db->query($sqlattri);
        // debug($attrires->result_array());exit;
        $attribuites = array();
        foreach($attrires->result_array() as $attr => $attri)
        {
            $attribuites[$attri['field_name']] = '';

        }
        // debug($attribuites);exit;
        return $attribuites;
    }

    private function GetAttributesValues($client_id,$uitlization_id) 
    {
        $sqlattri = "SELECT utilization_client_attributes.attributes_id, label_name, field_name, attributes_value 
        FROM utilization_client_attributes 
        JOIN new_attributes ON new_attributes.attributes_id = utilization_client_attributes.attributes_id  
        WHERE utilization_id  =".$uitlization_id;

        $attrires = $this->db->query($sqlattri);
        $attribuites = array();
        foreach($attrires->result_array() as $attr => $attri)
        {

            $attribuites[$attri['field_name']] = $attri['attributes_value'];

        }
        // debug($attribuites);exit;
        return $attribuites;
    }

    private function getSubtaskNames($SubArr)
    {
        $SubtaskNameArr = str_replace(",", "','", $SubArr);
        $SubArr = rtrim($SubArr,',');
        if($SubArr != '')
        {
            $sql = "SELECT GROUP_CONCAT(sub_tasks.`task_name`) AS sub_tasks 
            FROM sub_tasks  
            WHERE sub_task_id IN ($SubArr)";
            $empRst = $this->db->query($sql);
            $SubtaskNameArr = $empRst->row_array();
                // echo"<pre>";print_r($SubtaskNameArr);exit;
            return $SubtaskNameArr['sub_tasks'];
        }
        else
        {
            return '';
        }
    }

}
?>