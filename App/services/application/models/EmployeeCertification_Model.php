<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeCertification_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_certificates()
	{
		if($this->input->get('employee_id') != null && $this->input->get('employee_id') != '')
		{
			$employee_id = $this->input->get('employee_id');
		}
		else
		{
			$empData     = $this->input->cookie();
			$employee_id = $empData['employee_id'];
		}

		$sql="SELECT * FROM employee_certifications
		WHERE employee_id = '$employee_id' 
		ORDER BY id DESC";

		$query = $this->db->query($sql);

		$results['totalCount'] = $query->num_rows();

		$data = $query->result_array();
		$results['data'] = $data;

		return $results;
	}	

	function addCertificate($data = '')
	{
		$certificateData = $data;
		$empData     = $this->input->cookie();

		$this->db->set('employee_id',$empData['employee_id']);
		$this->db->set('certificate_name', $certificateData['certificate_name']);
		$this->db->set('content', $certificateData['content']);
		$this->db->set('year', $certificateData['year']);
		$this->db->set('created_on', date("Y-m-d H:i:s"));								
		$resVal = $this->db->insert('employee_certifications');

		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been added employee certificate (Certificate ID:'.$this->db->insert_id().')(Certificate Name:'.$certificateData['certificate_name'].').';
			$this->userLogs($logmsg); 

		return $resVal;
	}
	
	function updateCertificate($idVal = '',$data = '')
	{
		$empData     = $this->input->cookie();			
		$setData = $data;		
		$where = array();
		$set = array();
		$where['id'] = $setData['id'];

		$dataset = array(
			'certificate_name'			=> $setData['certificate_name'],
			'content'	=> $setData['content'],
			'year'	=> $setData['year'],
			'updated_on'	=> date("Y-m-d H:i:s")
		);			
		$retVal = $this->update($dataset, $where, 'employee_certifications');

		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee certificate (Certificate ID:'.$setData['id'].')(Certificate Name:'.$setData['certificate_name'].').';
			$this->userLogs($logmsg); 			
		return $retVal;		
	}		
	
	function deleteCertificate( $idVal = '' ) 
	{
		$options = array();
		$options['id'] =  $idVal;
		$retVal = $this->delete($options, 'employee_certifications');	

		$empData     = $this->input->cookie();	
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been deleted employee certificate (Certificate ID:'.$idVal.').';
			$this->userLogs($logmsg); 

		return $retVal;
	}
	
}
?>