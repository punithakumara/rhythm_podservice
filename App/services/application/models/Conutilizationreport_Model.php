<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conutilizationreport_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
		$this->load->helper('datediff_helper');
		$this->load->model('Vertical_Model');
	}
	
	/**
	* Function for get the Utilization reports
	* $GroupBy - Daily(0), Weekly(1), Monthly(2)
	*/
	function getReports( $start="", $end="", $GroupBy="", $ClientID="", $VerticalID="",$ShiftID="",$managerID="",$AMID="",$TLId="",$EmployID="",$Weekendsupport="",$ClientHoliday="")
	{
		
		if(!empty($VerticalID)) 
		{
			$VerticalID=json_decode($VerticalID);
		}
		
		$pverticalCombo = $this->input->get('pverticalCombo');
		
		//paging code//
		$options = array();
		if ($this->input->get('hasNoLimit') != '1') {
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 1;
		}
		$sort = json_decode($this->input->get('sort'), true);
		$options['sortBy']        = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];
		
		$empData = $this->input->cookie();
		$grade  = $empData['Grade'];
		$VertIDs='';
		if($grade <= 5) {
			$VertIDs =$empData['VertIDs'];
		}

		if(($start=="") && $end=="")
		{
			$start 	=date('Y-m-01');
			$end 	=date('Y-m-t'); 
		}
		else if($start!="" && $end=="")
		{
			$start = $start;
			$end   = date('Y-m-d');
		}
		
		$sql='';			
		if($GroupBy=='2'){
			$sql ="SELECT client_id, utilization_id,vertical_name,vertical_id,
			ROUND(AVG(Employee_Utilization),2) AS Employee_Utilization,GROUP_CONCAT(employee_id) AS employee_id , task_date,Month ,Year ,
			SUM(No_Hours) AS No_Hours ,SUM(No_Days) AS No_Days ,  SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(Total_Hours_Day) )))  AS Total_Hours_Day ,
			DATE_FORMAT(task_date,'%b') AS GroupBy,SUM(client_exp_hours) AS client_exp_hours,SUM(WorkedBillable) AS WorkedBillable
			FROM (";
		}
		
		// WITH CLIENT HEAD COUNT

		$sql.="SELECT support,DATE_FORMAT(task_date,'%b-%d') AS GroupBy,GROUP_CONCAT(DISTINCT (vertical_name)) AS vertical_name,task_date,COUNT(DISTINCT task_date) AS No_Days,utilization_id,GROUP_CONCAT(DISTINCT (vertical_id)) AS vertical_id,
		GROUP_CONCAT(DISTINCT(client_id)) AS client_id,
		COUNT(employee_id) AS WorkedBillable,ROUND(SUM(utilization),2) AS Employee_Utilization,
		GROUP_CONCAT(employee_id) AS employee_id ,SUM(TIME_TO_SEC(num_hours)) AS No_Hours,
		MONTH(task_date) AS Month, YEAR(task_date) AS Year,
		SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(num_hours) ))) AS Total_Hours_Day ,client_exp_hours
		FROM utilization_employeewise
		WHERE  1=1  ";
		if(!empty($VerticalID))
		{
			if(is_array($VerticalID))
				$sql .=" AND vertical_id IN(".implode(",",$VerticalID).")";
			else
				$sql .=" AND vertical_id = ".$VerticalID;
		}
		else if($VertIDs!="")
		{
			if($grade <= 5)
				$sql .=" AND vertical_id IN(".implode(",",$VertIDs).")";
		}

		if($pverticalCombo != '')
		{
			$Verticals = $this->Vertical_Model->getVerticalsForParentID($pverticalCombo);;            
			$sql .=" AND  vertical_id IN ($Verticals) ";
		}

		if($ClientID!="" &&  count($ClientID) >0)
		{
			if(is_array($ClientID))
				$sql .=" AND client_id IN(".implode(",",$ClientID).")";
			else
				$sql .=" AND client_id = ".$ClientID;
		}


		if($ShiftID!="")
		{
			if(is_array($ShiftID))
				$sql .=" AND shift_id IN(".implode(",",$ShiftID).")";
			else
				$sql .=" AND shift_id = ".$ShiftID;
		}

		if($EmployID!="")
		{
			if(is_array($EmployID)){
				$sql .=" AND employee_id IN(".implode(",",$EmployID).")";
			}
			else
			{
				if($EmployID!="")
					$sql .=" AND employee_id = ".$EmployID;
			}			
		}
		else if(($AMID!="") || $TLId!="")
		{
			if($TLId!="")
				$AMID = $TLId;
			$empIDs =  array_unique (parent::getEmployeeList($AMID) );
			
			if(count($empIDs) != '0')
			{	
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
		}
		else if($managerID)
		{
			$empIDs = array_unique (parent::getEmployeeList($managerID) );
			if(count($empIDs) != '0'){
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
			else{
				$sql.=" AND employee_id =0";
			}
		}
		if($Weekendsupport=='true' && $ClientHoliday=='false'){
			$sql .=" AND support LIKE '%Weekend Support%' ";
			
		}elseif($Weekendsupport=='false' && $ClientHoliday=='true'){
			$sql .=" AND support LIKE '%Client Holiday%' ";
		}elseif($Weekendsupport=='true' && $ClientHoliday=='true'){
			$sql .="  AND support LIKE '%Weekend Support%' OR support LIKE '%Client Holiday%' ";
		}
		
		if($start!="" && $end!=""){
			$sql.=" AND task_date BETWEEN '".$start."' AND '".$end."'";
		}
		else 
		{
			if($GroupBy=='2')
				$sql .=" AND DATE_FORMAT(task_date,'%m-%d ') = MONTH('".$today."')";
			else	
				$sql .=" AND task_date ='".$today."' ";
		}
		if($GroupBy=='2')
		{
			$sql.=" GROUP BY task_date ) AS monthly_table GROUP BY MONTH(task_date) ,YEAR(task_date)";
		} else {
			$sql.=" GROUP BY task_date";
		}
		if (isset($options['sortBy'])) {
			$sql .= " ORDER BY " . $options['sortBy'] . " " . $options['sortDirection'];
		}
		
		$query 		= $this->db->query($sql);
		$totCount 	= $query->num_rows();
		
		if (isset($options['limit']) && isset($options['offset'])) 
		{
			$sql .= " LIMIT " . $options['offset'] . " , " . $options['limit'];
		} 
		else if (isset($options['limit'])) 
		{
			$sql .= " LIMIT " . $options['limit'];
		}
		$query 		= $this->db->query($sql);
		$data 		= $this->db->query($sql);
		
		$array = $data->result_array();
		foreach ($array as $key => $value)
		{			 
			$array[$key] ['Approved_FTE']= $this->GetClientHeadCount($GroupBy ,$value['task_date'],$start,$end,$value['client_id']);
			
			$array[$key] ['client_name']= $this->GetClientName($value['client_id']);
			
		} 	
		foreach ($array as $key => $results)
		{
			$array[$key]['expectedHours']	= $results['Approved_FTE']*8;
			
			if(!empty($results['No_Hours']))
			{
				$init                            = $results['No_Hours'];
				$hours                           = floor($init / 3600);
				$minutes                         = floor(($init / 60) % 60);
				$FinalHours                      = $hours . '.' . $minutes;
				if($results['Approved_FTE']!=0)
				{
					$array[$key]['Utilization']	= round(($FinalHours/($results['Approved_FTE']*8)) * 100, 2);
				} else {
					$array[$key]['Utilization']	= 0;
				}
			}
		}

		$results = array_merge(array('totalCount' => $totCount),array('rows' => $array));

		return $results;
	}

	function conutilizationreport_weekly_model( $start="", $end="", $monthRange="", $ClientID="", $VerticalID="",$ShiftID="",$managerID="",$AMID="",$TLId="",$EmployID="",$j,$Weekendsupport="",$ClientHoliday="")
	{
		if(!empty($VerticalID)){
			$VerticalID=json_decode($VerticalID);
		}
		$pverticalCombo   = $this->input->get('pverticalCombo');
		
		$empData = $this->input->cookie();
		$grade  = $empData['Grade'];
		$VertIDs='';
		if($grade <= 5)
			$VertIDs =$empData['VertIDs'];
		
		$sql = "SELECT client_id, utilization_id,vertical_name,vertical_id, 
		ROUND(AVG(Employee_Utilization), 2) AS Employee_Utilization,employee_id, task_date,Month ,Year, SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(Total_Hours_Day) )))  AS Total_Hours_Day , SUM(No_Hours) AS No_Hours ,SUM(No_Days) AS No_Days ,CONCAT_WS(' ',GroupBy,WEEK) AS GroupBy ,SUM(client_exp_hours) AS client_exp_hours,
		SUM(WorkedBillable) AS WorkedBillable FROM (
		SELECT utilization_id,GROUP_CONCAT(DISTINCT (vertical_name)) AS vertical_name,GROUP_CONCAT(DISTINCT (vertical_id)) AS vertical_id,
		SUM(Utilization) AS Employee_Utilization,employee_id, task_date, GROUP_CONCAT(DISTINCT(client_id)) AS client_id, MONTH(task_date) AS Month, SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(num_hours) ))) AS Total_Hours_Day, YEAR(task_date) AS Year, COUNT(employee_id) AS WorkedBillable, SUM(TIME_TO_SEC(num_hours)) AS No_Hours, COUNT(DISTINCT task_date) AS No_Days,
		WEEK ('".$start."') AS weeksout,FLOOR((DAYOFMONTH(task_date)-1)/7)+1 AS WEEK, 'Week' AS GroupBy, SUM(client_exp_hours) AS client_exp_hours
		FROM utilization_employeewise
		WHERE 1=1 ";

		if(($AMID!="") || $TLId!=""){
			if($TLId!="")
				$AMID = $TLId;
			$empIDs = parent::getEmployeeList($AMID);
			$empIDs = implode(",", $empIDs);
			if(count($empIDs) != '0')
				$sql.=" AND employee_id IN($empIDs)";
		}
		if(!empty($VerticalID)){
			if(is_array($VerticalID))
				$sql .=" AND vertical_id IN(".implode(",",$VerticalID).")";
			else
				$sql .=" AND vertical_id = ".$VerticalID;
		}
		if($pverticalCombo != ''){
			$Verticals = $this->Vertical_Model->getVerticalsForParentID($pverticalCombo);;            
			$sql .=" AND  vertical_id IN ($Verticals) ";
		}
		if($ClientID!="" &&  count($ClientID) >0){
			if(is_array($ClientID))
				$sql .=" AND client_id IN(".implode(",",$ClientID).")";
			else
				$sql .=" AND client_id = ".$ClientID;
		}

		if($ShiftID!=""){
			if(is_array($ShiftID))
				$sql .=" AND shift_id IN(".implode(",",$ShiftID).")";
			else
				$sql .=" AND shift_id = ".$ShiftID;
		}
		if($EmployID!=""){
			if(is_array($EmployID))
			{
				$sql .=" AND employee_id IN(".implode(",",$EmployID).")";
			}
			else
			{
				if($EmployID!="")
					$sql .=" AND employee_id = ".$EmployID;
			}
		}
		else if(($AMID!="") || $TLId!=""){
			if($TLId!="")
				$AMID = $TLId;
			$empIDs = array_unique (parent::getEmployeeList($AMID) );

			if(count($empIDs) != '0')
			{
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
		}
		else if($managerID){
			$empIDs = array_unique (parent::getEmployeeList($managerID) );
			if(count($empIDs) != '0'){
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
			else{
				$sql.=" AND employee_id =0";
			}	
		}
		if($Weekendsupport=='true' && $ClientHoliday=='false'){
			$sql .=" AND support LIKE '%Weekend Support%' ";

		}elseif($Weekendsupport=='false' && $ClientHoliday=='true'){
			$sql .=" AND support LIKE '%Client Holiday%' ";
		}elseif($Weekendsupport=='true' && $ClientHoliday=='true'){
			$sql .="  AND support LIKE '%Weekend Support%' OR support LIKE '%Client Holiday%' ";
		}

		if($start!="" && $end!=""){
			$sql.=" AND task_date BETWEEN '".$start."' AND '".$end."'";
		}
		else
		{
			$sql .=" AND task_date ='".$today."' ";
		}
		
		$sql.=" GROUP BY task_date ) AS weekly_table GROUP BY weeksout ";
		
		$data 		= $this->db->query($sql);
		$results 	=  $data->result_array();
		
		$array = $data->result_array();
		
		foreach ($array as $key => $value)
		{			 
			$array[$key] ['Approved_FTE']= $this->GetClientHeadCount($monthRange ,$value['task_date'],$start,$end,$value['client_id']);
			
			$array[$key] ['client_name']= $this->GetClientName($value['client_id']);	
		} 	
		
		foreach ($array as $key => $results)
		{
			$array[$key]['expectedHours']	= $results['Approved_FTE']*8;
			
			if(!empty($results['No_Hours']))
			{
				$init                            = $results['No_Hours'];
				$hours                           = floor($init / 3600);
				$minutes                         = floor(($init / 60) % 60);
				$FinalHours                      = $hours . '.' . $minutes;
				if($results['Approved_FTE']!=0)
				{
					$array[$key]['Utilization']	= round(($FinalHours/($results['Approved_FTE']*8)) * 100, 2);
				} else {
					$array[$key]['Utilization']	= 0;
				}
			}
		}	

		if(isset($array[0])) 
			$array = $array[0];
		if (empty($array))
		{
			return false;
		}

		return $array;
	}
	
	
	/**
	* Function to get the Utilization data on weekly basis
	*/
	function weekly_report($start="", $end="", $monthRange="", $ClientID="", $VerticalID="",$ShiftID="",$managerID="",$AMID="",$TLId="",$EmployID="",$Weekendsupport="",$ClientHoliday="")
	{
		$signupweek	='' ;
		$year 			="";
		$currentweek 	="";
		$options = array();
		if ($this->input->get('hasNoLimit') != '1') {
			$options['offset'] = ($this->input->get('start') != '') ? $this->input->get('start') : 0;
			$options['limit']  = ($this->input->get('limit') != '') ? $this->input->get('limit') : 20;
		}
		$sort = json_decode($this->input->get('sort'), true);
		$options['sortBy']        = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		if($start!="" && $end==""){
			$signupdate = $start;
			$enddate   = date('Y-m-d');
		}
		else if(($start=="") && $end==""){
			$signupdate 	=date('Y-m-01');
			$enddate 		=date('Y-m-t');
		}
		else{
			$signupdate		= $start;
			$enddate 		= $end;
		}

		$signupweek		= date("W",strtotime($signupdate));
		$year			= date("Y",strtotime($signupdate));
		$currentweek 	= date("W",strtotime($enddate));
		$j=1;

 		// For JAN 53rd 2016 week issue
		if($signupweek > $currentweek){
			$signupdate2		= date($signupweek,date(strtotime("+1 week")));
			$signupweek			= date("W",strtotime($signupdate2));
		}

		for($i=$signupweek;$i<=$currentweek;$i++) {
			$result	= $this->getWeek($i,$year);
			// echo "<pre>"; print_r($result); 
			$result_all[] = $this->conutilizationreport_weekly_model($result['start'], $result['end'], $monthRange, $ClientID,$VerticalID,$ShiftID ,$managerID ,$AMID ,$TLId ,$EmployID,$j,$Weekendsupport);
			$j++; 
		}

		$result_all = array_values(array_filter($result_all));
		$new_array = array_slice($result_all,$options['offset'],$options['limit'] );
		$results = array_merge(array('totalCount' => count($result_all)),array('rows' => $new_array));
		
		return $results;
	}
	
	function getWeek($week, $year) 
	{
		
		$dto = new DateTime();
		$result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
		$result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
		
		return $result;
	}
	
	
	
	/**
	* Function to get the Utilization data for excel on daily & monthly basis.
	*/
	function getCSVReports()
	{
		// $ProcessID 	= $this->input->get('ProcessID');
		$ClientID 	= $this->input->get('ClientID');
		$VerticalID = $this->input->get('VerticalID');
		$ShiftID 	= $this->input->get('ShiftID');
		$managerID	= $this->input->get('managerID');
		$AMID		= $this->input->get('AMID');
		$TLId 		= $this->input->get('TLId');
		$EmployID	= $this->input->get('EmployID');
		$start 		= $this->input->get('startDate');
		$end 		= $this->input->get('endDate');
		$GroupBy 	= $this->input->get('ConUtilGroupBy');
		$Weekendsupport	=$this->input->get('WeekendSupport');
		$ClientHoliday	=$this->input->get('ClientHoliday');
		$pverticalCombo   = $this->input->get('pverticalCombo');
		$empData = $this->input->cookie();
		$grade  = $empData['Grade'];
		$VertIDs='';
		if($grade <= 5)
			$VertIDs =$empData['VertIDs'];

		if(($start=="") && $end=="")
		{
			$start 	= date('Y-m-01');
			$end 	= date('Y-m-t'); 		
		}
		else if($start!="" && $end=="")
		{
			$start = $start;
			$end   = date('Y-m-d');			
		}
		
		$sql='';			
		if($GroupBy=='2'){
			$sql = "SELECT client_id, utilization_id,vertical_name,
			ROUND(AVG(Employee_Utilization),2) AS Employee_Utilization,GROUP_CONCAT(employee_id) AS employee_id , task_date,Month ,Year ,
			SUM(No_Hours) AS No_Hours ,SUM(No_Days) AS No_Days ,  SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(Total_Hours_Day) )))  AS Total_Hours_Day ,
			DATE_FORMAT(task_date,'%b') AS GroupBy,SUM(WorkedBillable) AS WorkedBillable
			FROM (";
		}

		$sql.="SELECT support,DATE_FORMAT(task_date,'%b-%d') AS GroupBy,GROUP_CONCAT(DISTINCT (vertical_name)) AS vertical_name,task_date,COUNT(DISTINCT task_date) AS No_Days,utilization_id,
		GROUP_CONCAT(DISTINCT(client_id)) AS client_id,
		COUNT(employee_id) AS WorkedBillable,ROUND(SUM(utilization),2) AS Employee_Utilization,
		GROUP_CONCAT(employee_id) AS employee_id ,SUM(TIME_TO_SEC(num_hours)) AS No_Hours,
		MONTH(task_date) AS Month, YEAR(task_date) AS Year,
		SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(num_hours) ))) AS Total_Hours_Day
		FROM utilization_employeewise
		WHERE  1=1  ";
		if($VerticalID!=""){
			if(is_array($VerticalID))
				$sql .=" AND vertical_id IN(".implode(",",$VerticalID).")";
			else
				$sql .=" AND vertical_id = ".$VerticalID;
		}
		else if($VertIDs!="")
		{
			if($grade <= 5)
				$sql .=" AND vertical_id IN(".implode(",",$VertIDs).")";
		}
		if($pverticalCombo != ''){
			$Verticals = $this->Vertical_Model->getVerticalsForParentID($pverticalCombo);;            
			$sql .=" AND vertical_id IN ($Verticals) ";
		}
		if($ClientID!="" &&  count($ClientID) >0){
			if(is_array($ClientID))
				$sql .=" AND client_id IN(".implode(",",$ClientID).")";
			else
				$sql .=" AND client_id = ".$ClientID;
		}
		if($ShiftID!=""){
			if(is_array($ShiftID))
				$sql .=" AND shift_id IN(".implode(",",$ShiftID).")";
			else
				$sql .=" AND shift_id = ".$ShiftID;
		}

		if($EmployID!=""){
			if(is_array($EmployID)){
				$sql .=" AND employee_id IN(".implode(",",$EmployID).")";
			}
			else
			{
				if($EmployID!="")
					$sql .=" AND employee_id = ".$EmployID;
			}

		}
		else if(($AMID!="") || $TLId!=""){
			if($TLId!="")
				$AMID = $TLId;
			$empIDs = array_unique (parent::getEmployeeList($AMID) );

			if(count($empIDs) != '0')
			{	
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
		}
		else if($managerID){
			$empIDs = array_unique (parent::getEmployeeList($managerID) );
			if(count($empIDs) != '0'){
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
			else{
				$sql.=" AND employee_id =0";
			}
		}

		if($start!="" && $end!=""){
			$sql.=" AND task_date BETWEEN '".$start."' AND '".$end."'";
		}
		else 
		{
			if($GroupBy=='2')
				$sql .=" AND DATE_FORMAT(task_date,'%m-%d ') = MONTH('".$today."')";
			else	
				$sql .=" AND task_date ='".$today."' ";
		}
		if($GroupBy=='2')
		{
			$sql.=" GROUP BY task_date ) AS monthly_table GROUP BY MONTH(task_date)";
		}else{
			$sql.=" GROUP BY task_date";
		}


		if (isset($options['sortBy'])) {
			$sql .= " ORDER BY " . $options['sortBy'] . " " . $options['sortDirection'];
		}
		$query 		= $this->db->query($sql);
		
		$array = $query->result_array();
		
		foreach ($array as $key => $value)
		{			 
			$array[$key] ['Approved_FTE']= $this->GetClientHeadCount($GroupBy ,$value['task_date'],$start,$end,$value['client_id']);
			
			$array[$key] ['client_name']= $this->GetClientName($value['client_id']);
			
		} 	
		
		foreach ($array as $key => $results)
		{
			$array[$key]['expectedHours']	= $results['Approved_FTE']*8;
			
			if(!empty($results['No_Hours']))
			{
				$init                            = $results['No_Hours'];
				$hours                           = floor($init / 3600);
				$minutes                         = floor(($init / 60) % 60);
				$FinalHours                      = $hours . '.' . $minutes;

				if($results['Approved_FTE']!=0)
				{
					$array[$key]['Utilization']	= round(($FinalHours/($results['Approved_FTE']*8)) * 100, 2);
				} else {
					$array[$key]['Utilization']	= 0;

				}
			}

		}
		
		foreach ($array as &$value) 
		{	
			$value['Group By']				= 	$value['GroupBy'];
			$value['Vertical']				=	$value['vertical_name'];
			$value['Client']				=	$value['client_name'];
			$value['Utilization %']			= 	$value['Utilization'];
			$value['No_Hour']			    =	$value['No_Hours'];
			$value['ExpHours']				=   $value['expectedHours'];
			$value['No.of.ResWorked']		=   $value['WorkedBillable']."(".$value['Approved_FTE'].")";
			$value['Billable Resources']	=	$value['Approved_FTE'];
			$value['No.of Days']			=	$value['No_Days'];
			$value['Months']				=	$value['Month'];
			$value['Years']					=	$value['Year'];
			
			unset($value['GroupBy']);
			unset($value['client_id']);
			unset($value['Approved_FTE']);
			unset($value['Total_Hours_Day']);
			unset($value['utilization_id']);
			unset($value['Employee_Utilization']);
			unset($value['employee_id']);
			unset($value['vertical_name']);
			unset($value['support']);
			unset($value['client_name']);
			unset($value['Utilization']);
			
			unset($value['No_Hours']);
			unset($value['expectedHours']);
			unset($value['No_Days']);

			unset($value['task_date']);
			unset($value['WorkedBillable']);
			unset($value['Month']);
			unset($value['Year']);
			
		}
		
		if(count($array))
		{
			$columns = array(array_keys($array[0]));
			$results = array_merge($columns, $array);
			$totalarray	= count($results);

			return $results;  
		}
		else{
			return "";
		}	
	}
	
	
	/**
	* Function to get the Utilization data for excel on weekly basis.
	*/
	function conutilizationreport_weekly_export( $start="", $end="", $monthRange="", $ClientID="", $VerticalID="",$ShiftID="",$managerID="",$AMID="",$TLId="",$EmployID="",$j,$Weekendsupport='')
	{	 
		$empData = $this->input->cookie();
		$grade  = $empData['Grade'];
		$pverticalCombo   = $this->input->get('pverticalCombo');
		$VertIDs='';
		if($grade <= 5)
			$VertIDs =$empData['VertIDs'];

		
		$sql = "SELECT client_id, utilization_id,vertical_name,vertical_id, 
		ROUND(AVG(Employee_Utilization), 2) AS Employee_Utilization,employee_id, task_date,Month ,Year, SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(Total_Hours_Day) )))  AS Total_Hours_Day , SUM(No_Hours) AS No_Hours ,SUM(No_Days) AS No_Days ,CONCAT_WS(' ',GroupBy,WEEK) AS GroupBy ,SUM(client_exp_hours) AS client_exp_hours,
		SUM(WorkedBillable) AS WorkedBillable FROM (
		SELECT utilization_id,GROUP_CONCAT(DISTINCT (vertical_name)) AS vertical_name,GROUP_CONCAT(DISTINCT (vertical_id)) AS vertical_id,
		SUM(Utilization) AS Employee_Utilization,employee_id, task_date, GROUP_CONCAT(DISTINCT(client_id)) AS client_id, MONTH(task_date) AS Month, SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(num_hours) ))) AS Total_Hours_Day, YEAR(task_date) AS Year, COUNT(employee_id) AS WorkedBillable, SUM(TIME_TO_SEC(num_hours)) AS No_Hours, COUNT(DISTINCT task_date) AS No_Days,
		WEEK ('".$start."') AS weeksout,FLOOR((DAYOFMONTH(task_date)-1)/7)+1 AS WEEK, 'Week' AS GroupBy, SUM(client_exp_hours) AS client_exp_hours
		FROM utilization_employeewise
		WHERE 1=1  ";


		//DATE_FORMAT(TaskDate,	'%a') as GroupBy
		if(($AMID!="") || $TLId!=""){
			if($TLId!="")
				$AMID = $TLId;
			$empIDs = parent::getEmployeeList($AMID);
			$empIDs = implode(",", $empIDs);
			if(count($empIDs) != '0')
				$sql.=" AND employee_id IN($empIDs)";
		}
		if($VerticalID!=""){
			if(is_array($VerticalID))
				$sql .=" AND vertical_id IN(".implode(",",$VerticalID).")";
			else
				$sql .=" AND vertical_id = ".$VerticalID;
		}
		if($pverticalCombo != ''){
			$Verticals = $this->Vertical_Model->getVerticalsForParentID($pverticalCombo);;            
			$sql .=" AND  vertical_id IN ($Verticals) ";
		}
		if($ClientID!="" &&  count($ClientID) >0){
			if(is_array($ClientID))
				$sql .=" AND client_id IN(".implode(",",$ClientID).")";
			else
				$sql .=" AND client_id = ".$ClientID;
		}
		
		if($ShiftID!="")
		{
			if(is_array($ShiftID))
				$sql .=" AND shift_id IN(".implode(",",$ShiftID).")";
			else
				$sql .=" AND shift_id = ".$ShiftID;
		}
		if($EmployID!="")
		{
			if(is_array($EmployID)){
				$sql .=" AND employee_id IN(".implode(",",$EmployID).")";
			}
			else
			{
				if($EmployID!="")
					$sql .=" AND employee_id = ".$EmployID;
			}			
		}
		else if(($AMID!="") || $TLId!="")
		{
			if($TLId!="")
				$AMID = $TLId;
			$empIDs = array_unique (parent::getEmployeeList($AMID) );

			if(count($empIDs) != '0')
			{	
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
		}
		else if($managerID)
		{
			$empIDs = array_unique (parent::getEmployeeList($managerID) );
			if(count($empIDs) != '0'){
				$empIDs = implode(",", $empIDs);
				$sql.=" AND employee_id IN($empIDs)";
			}
			else{
				$sql.=" AND employee_id =0";
			}		 
		}
		
		if($start!="" && $end!="")
		{
			$sql.=" AND task_date BETWEEN '".$start."' AND '".$end."'";
		}
		else
		{
			$sql .=" AND task_date ='".$today."' ";
		}

		$sql.=" GROUP BY task_date ) AS weekly_table GROUP BY weeksout ";
		$data 		=  $this->db->query($sql);
		
		$array = $data->result_array();
		
		foreach ($array as $key => $value)
		{			 
			$array[$key] ['Approved_FTE']= $this->GetClientHeadCount($monthRange ,$value['task_date'],$start,$end,$value['client_id']);
			
			$array[$key] ['client_name']= $this->GetClientName($value['client_id']);
			
		} 	
		
		foreach ($array as $key => $results)
		{
			$array[$key]['expectedHours']	= $results['Approved_FTE']*8;
			
			if(!empty($results['No_Hours']))
			{
				$init                            = $results['No_Hours'];
				$hours                           = floor($init / 3600);
				$minutes                         = floor(($init / 60) % 60);
				$FinalHours                      = $hours . '.' . $minutes;

				if($results['Approved_FTE']!=0)
				{
					$array[$key]['Utilization']	= round(($FinalHours/($results['Approved_FTE']*8)) * 100, 2);
				} else {
					$array[$key]['Utilization']	= 0;

				}
			}

		}

		foreach ($array as &$value) {
			$value['Group By']				= 	$value['GroupBy'];
			$value['Vertical']				=	$value['vertical_name'];
			$value['Client']				=	$value['client_name'];
			$value['Utilization %']			=	$value['Utilization'];
			$value['No.of Hours']			=	$value['No_Hours'];
			$value['ExpHours']				=	$value['expectedHours'];
			$value['No.of ResWorked']		=	$value['WorkedBillable'];
			$value['Billable_Resources']	=	$value['Approved_FTE'];
			$value['No.of Days']			=	$value['No_Days'];
			$value['Months']				=	$value['Month'];
			$value['Years']					=	$value['Year'];

			unset($value['GroupBy']);
			unset($value['client_id']);
			unset($value['vertical_id']);
			unset($value['Employee_Utilization']);
			unset($value['employee_id']);
			unset($value['Total_Hours_Day']);
			unset($value['vertical_name']);
			unset($value['client_name']);
			unset($value['Utilization']);
			unset($value['No_Hours']);
			unset($value['expectedHours']);
			unset($value['No_Days']);
			unset($value['WorkedBillable']);
			unset($value['Month']);
			unset($value['Year']);
			unset($value['client_exp_hours']);

			unset($value['Approved_FTE']);
			unset($value['utilization_id']);
			unset($value['support']);
			unset($value['task_date']);


		}

		if(isset($array[0])){
			$results = $array[0];
			$FinalHours='';
			$utl ='';

			if(isset($results['No.of Hours']))
			{
				if(!empty($results['No.of Hours']))
				{
					$init                            = $results['No.of Hours'];
					$hours                           = floor($init / 3600);
					$minutes                         = floor(($init / 60) % 60);
					$FinalHours                      = $hours . '.' . $minutes;
				}
			}

			$results['No.of Hours']			= $FinalHours;
		}
		if (empty($results))
		{
			return false;
		}

		return $results;
	}

	function weekly_reportCSV()
	{		
		$ClientID 	= $this->input->get('ClientID');
		$VerticalID = $this->input->get('VerticalID');
		$ShiftID 	= $this->input->get('ShiftID');
		$managerID	= $this->input->get('managerID');
		$AMID		= $this->input->get('AMID');
		$TLId 		= $this->input->get('TLId');
		$EmployID	= $this->input->get('EmployID');
		$start 		= $this->input->get('startDate');
		$end 		= $this->input->get('endDate');
		$monthRange = $this->input->get('ConUtilGroupBy');
		$Weekendsupport	=$this->input->get('WeekendSupport');
		$ClientHoliday	=$this->input->get('ClientHoliday');

		if($start!="" && $end==""){
			$signupdate = $start;
			$enddate   	= date('Y-m-d');
		}
		else if(($start=="") && $end==""){
			$signupdate =	date('Y-m-01');
			$enddate 	=	date('Y-m-t');
		}
		else{
			$signupdate		= $start;
			$enddate 		= $end;
		}

		$signupweek		= date("W",strtotime($signupdate));
		$year			= date("Y",strtotime($signupdate));
		$currentweek 	= date("W",strtotime($enddate));
		$j=1;
		for($i=$signupweek;$i<=$currentweek;$i++) 
		{
			$result	=	$this->getWeek($i,$year);
			$result_all[] = $this->conutilizationreport_weekly_export($result['start'], $result['end'], $monthRange, $ClientID, $VerticalID,$ShiftID ,$managerID ,$AMID ,$TLId ,$EmployID,$j,$Weekendsupport);
			$j++;
		}
		$result_all = array_values(array_filter($result_all));
		$data = $result_all;
		$columns = array(
			array_keys($data[0])
		);

		$results = array_merge($columns, $data);

		return $results;
	}

	function GetClientHeadCount($GroupBy='' ,$date='',$startdate='',$endDate='',$clientID='')
	{
		$clientID 	= rtrim($clientID, ',');
		$array2  	= explode(',', $clientID);
		$cli=	join("','",$array2);

		if ($GroupBy >= 1)
		{	
			$output = $this->db->query("SELECT SUM(`head_count`) AS tothead FROM `client_billable_history_cron` WHERE DATE BETWEEN '$startdate' AND '$endDate' AND client_id IN ('$cli')")->row()->tothead;
		}
		else{

			$output = $this->db->query("SELECT SUM(`head_count`) AS tothead FROM `client_billable_history_cron` WHERE DATE =  '$date' AND client_id IN ('$cli')")->row()->tothead;
		}

		return $output; 

	}
	function GetClientName($clientID='')
	{
		$clientID 	= rtrim($clientID, ',');
		$array2  	= explode(',', $clientID);
		$cli=	join("','",$array2);
		
		$output = $this->db->query("SELECT GROUP_CONCAT(client_name) AS client_name  FROM `client` WHERE  client_id IN ('$cli')")->row()->client_name;

		return $output; 

	}

	function GetBillableData($GroupBy='',$date='',$processID,$startdate='',$endDate='',$clientID='',$managerID ='',$AMID ='',$TaskDateList="")
	{
		/**
		* Report types:
		* $GroupBy - 0(Daily), 1(Weekly), 2(Monthly)
		*/
		$Billable_Resources =array();
		$processID 	= rtrim($processID, ',');
		$array  	= explode(',', $processID);
		$count  	= count($array);
		$array2  	= explode(',', $clientID);
		$clientTot  = count($array2);
		$client		= $clientID;
		if(!empty($client)){
			$client = $clientID;
		}
		if($clientTot!=1  && $count!=1){
			if($managerID!=""){
				$clientTot = 1;
			}
			elseif($AMID!=""){
				$clientTot = 1;
			}
		}
		if($GroupBy=="2")
		{
			$timestamp = strtotime($date);
			$date= trim(date("Y-m", $timestamp));
			if($startdate !=''&& $endDate !='')
			{
				if($count==1)
				{
					if(!empty($processID))
						$sql="SELECT SUM(Billable) as Resources  FROM `process_billablenonbillable_cron` WHERE DATE   Like '%".trim($date)."%'  AND ProcessID IN($processID)";
				}
				else
				{
					if($clientTot==1  && $count!=1)
						$sql="SELECT SUM(Billable) as Resources  FROM `process_billablenonbillable_cron` WHERE  DATE  Like '%".trim($date)."%'  AND ProcessID IN($processID)";
					else if(!empty($processID))
						$sql="SELECT SUM(DISTINCT(Billable)) as Resources  FROM `process_billablenonbillable_cron` WHERE  DATE Like '%".trim($date)."%' AND   ProcessID IN($processID)";
				}
			}
			else
			{
				echo "Start Date End date error";
			}
		}
		if($GroupBy=="0")
		{
			if($count==1)
			{
				if(!empty($processID))
					$sql="SELECT (Billable) as Resources FROM `process_billablenonbillable_cron` WHERE DATE = '$date' AND ProcessID IN($processID)";
			}
			else
				{	if($clientTot==1 ){
					$sql="SELECT SUM(Resources) AS Resources FROM (SELECT  SUM(DISTINCT (Billable)) AS Resources  FROM `process_billablenonbillable_cron` WHERE DATE = '$date' AND ProcessID IN($processID)) AS test";
				}
				else{
					if(!empty($processID))
						$sql="SELECT SUM(Billable) AS Resources FROM `process_billablenonbillable_cron` WHERE   DATE = '$date' AND ProcessID IN($processID)";
				}
			}
		}
		if($GroupBy== "2" || $GroupBy== "1" )
		{		
			if($startdate !=''&& $endDate !='')
			{
				$myArray 	= 	explode(',', $processID);
				$processID2	=	array_unique($myArray);
				$processID2	=	array_values($processID2);
				$total= count($processID2);
				$myArray_TaskDateList 	= 	explode('(', $TaskDateList);
				
				unset($myArray_TaskDateList[0]);
				
				foreach($myArray_TaskDateList as $k => $v)
				{
					list($k, $v) = explode('->',$v);
					$result_array[ $k ] = $v;
				} 
				
				$i=0;
				foreach($result_array as $key =>$value)
				{
					$value= rtrim($value,',');
					$sql2 ="SELECT SUM(Billable) as Resources  FROM `process_billablenonbillable_cron` WHERE  DATE= '".$key."' AND  ProcessID IN(".$value.")  GROUP BY MONTH(DATE)";
					$data2 		=  $this->db->query($sql2);
					$results2 	=  $data2->result_array();
					if($data2->num_rows()>0){
						$Billable_Resources[] = $results2[0]['Resources'];
					}
					$i++;
				}
				if($GroupBy== "1"){
					if($client != ""){
						$conclient = " AND processID IN( $processID ) AND clientID = $client ";
					}else{
						$conclient = "";
					}
					if($clientTot==1 )
						$sql="SELECT Billable as Resources  FROM `process_billablenonbillable_cron` WHERE DATE IN (SELECT DISTINCT TaskDate FROM employwise_utilization_process WHERE TaskDate BETWEEN '$startdate' AND '$endDate' AND ProcessID IN($processID)) $conclient ";
					else
						$sql="SELECT SUM(DISTINCT(Billable)) as Resources  FROM `process_billablenonbillable_cron` WHERE  DATE BETWEEN '$startdate' AND '$endDate'  AND ProcessID IN($processID)";
				}
			}
			
			if(count($Billable_Resources)> 0)
				$Billable_Resource['Billable_Resource']=array_sum($Billable_Resources);
			
		} 
		$result = "";
		$data 		= $this->db->query($sql);
		$results 	=  $data->result_array();
		$EmpUtIDS = array('TaskDate'=>$date);
		array_push($results, $EmpUtIDS);

		if(!empty($Billable_Resource))
			array_push($results, $Billable_Resource);

		$result = array();
		foreach($results as $inner) 
		{
			$result[key($inner)] = current($inner);
		}

		return $result;
		
	}
	
}
?>