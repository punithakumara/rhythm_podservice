<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeProfile_Model extends INET_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function list_months()
	{
		$query = ($this->input->get('query')!="") ? $this->input->get('query') : '';
		
		$sql = "SELECT * FROM months";
		if($query) {
			$sql .= " WHERE month_name LIKE '%".$query."%'";
		}  
		$sql .= " ORDER BY month_id ASC";

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $query->num_rows()), 
			array('data' => $data)
		);
		
		return $results;
	}

	function list_years()
	{
		$query = ($this->input->get('query')!="") ? $this->input->get('query') : '';
		
      	$sql = "SELECT * FROM years";
		if($query) {
			$sql .= " WHERE year_name LIKE '%".$query."%'";
		}  
		$sql .= " ORDER BY year_name DESC";

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$results = array_merge(
			array('totalCount' => $query->num_rows()), 
			array('data' => $data)
		);
		
		return $results;
	}

    /**
	* Get list of employees
	*
	* @param 
	* @return array
	*/ 
	function list_employees()
	{
		if($this->input->get('employee_id') != null && $this->input->get('employee_id') != '')
		{
			$employee_id = $this->input->get('employee_id');
		}
		else
		{
			$empData     = $this->input->cookie();
			$employee_id = $empData['employee_id'];
		}
		
      	$sql = "SELECT * FROM employee_experience 
      	WHERE employee_id = '$employee_id' 
      	ORDER BY id DESC";

		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		foreach($data as $key => $value) 
		{
			$data[$key]['work_period'] = $this->calculateExp($value['start_month'], $value['start_year'], $value['end_month'], $value['end_year']);
		}
		
		$results = array_merge(
			array('totalCount' => $query->num_rows()), 
			array('data' => $data)
		);
		
		return $results;
	}

	function calculateExp($startMonth, $startYear, $endMonth, $endYear)
	{
		$sdate = date("$startYear-$startMonth-01");
		$edate = date("$endYear-$endMonth-31");
		if($endMonth==null || $endYear==null || $endMonth=="" || $endYear=="")
		{
			$edate = date("Y-m-t");
		}
		
		$date_diff = abs(strtotime($edate) - strtotime($sdate));

		$years = floor($date_diff / (365*60*60*24));
		$months = floor(($date_diff - $years * 365*60*60*24) / (30*60*60*24));

		$yrData = "$years yr";
		if($years>1)
		{
			$yrData = "$years yrs";
		}

		$mnData = " $months mo";
		if($mnData>1)
		{
			$mnData = " $months mos";
		}
		$data = $yrData . $mnData;

		return $data;
	}

	function deleteExperience($data)
	{
		$deldata = array();
		$deldata['id'] = $data['experience_id'];
		$retval= $this->delete($deldata, 'employee_experience');

		$empData     = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been deleted employee experience (Experience ID:'.$deldata['id'].').';
			$this->userLogs($logmsg); 

		return $retval;
	}

	function addExperience($expData = '')
	{
		if($expData['id']=="")
		{
			$empData     = $this->input->cookie();
			$this->EmpId = $empData['employee_id'];
			
			$expData['employee_id']  = $this->EmpId;
			$expData['created_on']	= date("Y-m-d H:i:s");
			
			$retVal         = $this->insert($expData, 'employee_experience');
			$lastInsertedID = $this->db->insert_id();

			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been added employee experience (Experience ID:'.$lastInsertedID.').';
			$this->userLogs($logmsg); 

			return $retVal;
		}
		else
		{
			if(!isset($expData['cuurentWork']))
			{
				$newData = array(
					'org_name'		=> $expData['org_name'],
					'start_month' 	=> $expData['start_month'],
					'designation' 	=> $expData['designation'],
					'job_profile' 	=> $expData['job_profile'],
					'start_month' 	=> $expData['start_month'],
					'start_year' 	=> $expData['start_year'],
					'end_month' 	=> $expData['end_month'],
					'end_year' 		=> $expData['end_year'],
				);
			}
			else
			{
				$newData = array(
					'org_name'		=> $expData['org_name'],
					'start_month' 	=> $expData['start_month'],
					'designation' 	=> $expData['designation'],
					'job_profile' 	=> $expData['job_profile'],
					'start_month' 	=> $expData['start_month'],
					'start_year' 	=> $expData['start_year'],
					'end_month' 	=> '',
					'end_year' 		=> '',
				);
			}
			

			$where          = array();
			$empData        = $this->input->cookie();
			$EmpId          = $empData['employee_id'];
			$where['id'] 	= $expData['id'];

			$newData['updated_on'] = date("Y-m-d H:i:s");

			$retVal = $this->update($newData, $where, 'employee_experience');

			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') has been updated employee experience (Experience ID:'.$expData['id'].').';
			$this->userLogs($logmsg); 

			return -2;
		}
	}
}
?>