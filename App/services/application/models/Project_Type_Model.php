<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_Type_Model extends INET_Model
{
	function __construct() {
		parent::__construct();
	}

	public function get_projectTypes()
	{
		$filter_params = $this->input->get();

		// debug($filter_params);exit;

		$offset = $filter_params['start'];
		$limit = $filter_params['limit'];

		$wor_id = $this->input->get('wor_id') ? $this->input->get('wor_id'):"";
		$wor_type_id = $this->input->get('wor_type_id') ? $this->input->get('wor_type_id'):"";

		$sql = "SELECT wor_type,PT.project_type_id, project_type, project_description, COUNT('RT.role_type_id') AS total_roles
		FROM project_types PT
		LEFT JOIN wor_project_role_mapping WPRM ON PT.project_type_id = WPRM.project_type_id
		LEFT JOIN role_types RT ON RT.role_type_id = WPRM.role_type_id
		LEFT JOIN wor_types WT ON WT.wor_type_id = WPRM.wor_type_id
		WHERE 1=1 ";
		if($wor_id!="")
		{
			$project_type_id = $this->getProjectTypeId($wor_id);
			$sql .= " AND PT.project_type_id IN($project_type_id)";
		}

		if($wor_type_id!="")
		{
			$sql .= " AND WPRM.wor_type_id = '$wor_type_id'";
		}

		if(isset($filter_params['filter']))
		{
			$search_params = json_decode($filter_params['filter'],true);
			$property = explode(',',$search_params[0]['property']);
			if(isset($search_params[0]['value']))
			{
				$search_char = $search_params[0]['value'];
				if($search_char!="")
				{
					$sql .= " AND(";
					foreach ($property as $key => $value) {
						$sql .= "(".$value." LIKE '%".$search_char."%') OR ";
					}
					$sql = substr($sql, 0, -3);
					$sql .= ") ";
				}
			}
		}

		$sql .= " GROUP BY PT.project_type_id";

			// echo $sql;exit;

		$query = $this->db->query($sql);
		$result['rows'] = array();
		$result['totalCount'] = $query->num_rows();

		if(isset($filter_params['sort']))
		{
			$sort_params = json_decode($filter_params['sort'],true);
			$property = $sort_params[0]['property'];
			$direction = $sort_params[0]['direction'];

			$sql .= " ORDER BY $property $direction";
				// debug($sort_params);exit;
		}

		if($this->input->get('hasNoLimit') != '1')
		{
			$sql .= " LIMIT $offset,$limit";
		}
//echo $sql; exit;
		$query = $this->db->query($sql);
		$result['rows'] = array();

		if($query->num_rows()>0)
		{
			$result['rows'] = $query->result_array();
		}
		// debug($result);exit;
		return $result;	
	}

	private function getProjectTypeId($wor_id)
	{
		$project_type_id = array();

		$ftsql = "SELECT GROUP_CONCAT(DISTINCT fk_project_type_id SEPARATOR ',') AS fk_project_type_id
		FROM wor WR 
		LEFT JOIN fte_model FT ON FT.fk_wor_id = WR.wor_id 
		WHERE WR.wor_id = ".$wor_id."";
//echo $ftsql;
//exit;
		$query = $this->db->query($ftsql);
		$ft_data = $query->row_array();
		if($ft_data['fk_project_type_id']!="")
		{
			array_push($project_type_id, $ft_data['fk_project_type_id']);
		}


		$htsql = "SELECT GROUP_CONCAT(DISTINCT fk_project_type_id SEPARATOR ',') AS fk_project_type_id
		FROM wor WR 
		LEFT JOIN hourly_model HT ON HT.fk_wor_id = WR.wor_id 
		WHERE WR.wor_id = ".$wor_id."";

		$query = $this->db->query($htsql);
		$ht_data = $query->row_array();
		if($ht_data['fk_project_type_id']!="")
		{
			array_push($project_type_id, $ht_data['fk_project_type_id']);
		}

		$ptsql = "SELECT GROUP_CONCAT(DISTINCT fk_project_type_id SEPARATOR ',') AS fk_project_type_id
		FROM wor WR 
		LEFT JOIN project_model PT ON PT.fk_wor_id = WR.wor_id 
		WHERE WR.wor_id = ".$wor_id."";

		$query = $this->db->query($ptsql);
		$pt_data = $query->row_array();
		if($pt_data['fk_project_type_id']!="")
		{
			array_push($project_type_id, $pt_data['fk_project_type_id']);
		}

		$vtsql = "SELECT GROUP_CONCAT(DISTINCT fk_project_type_id SEPARATOR ',') AS fk_project_type_id
		FROM wor WR 
		LEFT JOIN volume_based_model VT ON VT.fk_wor_id = WR.wor_id 
		WHERE WR.wor_id = ".$wor_id."";

		$query = $this->db->query($vtsql);
		$vt_data = $query->row_array();
		if($vt_data['fk_project_type_id']!="")
		{
			array_push($project_type_id, $vt_data['fk_project_type_id']);
		}

		$results = implode(',',$project_type_id);

		return $results;
	}

	public function getClientWorProjectTypes($client_id)
	{
		$fte_sql = "SELECT DISTINCT PT.project_type,PT.project_type_id FROM wor W
		JOIN fte_model FM ON FM.fk_wor_id = W.wor_id
		JOIN project_types PT ON PT.project_type_id = FM.fk_project_type_id
		WHERE client='$client_id'";

		$hourly_sql = "SELECT DISTINCT PT.project_type,PT.project_type_id FROM wor W
		JOIN hourly_model HM ON HM.fk_wor_id = W.wor_id
		JOIN project_types PT ON PT.project_type_id = HM.fk_project_type_id
		WHERE client='$client_id'";

		$project_sql = "SELECT DISTINCT PT.project_type,PT.project_type_id FROM wor W
		JOIN project_model PM ON PM.fk_wor_id = W.wor_id
		JOIN project_types PT ON PT.project_type_id = PM.fk_project_type_id
		WHERE client='$client_id'";

		$volume_based_sql = "SELECT DISTINCT PT.project_type,PT.project_type_id FROM wor W
		JOIN volume_based_model VBM ON VBM.fk_wor_id = W.wor_id
		JOIN project_types PT ON PT.project_type_id = VBM.fk_project_type_id
		WHERE client='$client_id'";

		$sql = "";
		
		if($this->input->get('wor_id') && $this->input->get('wor_id')!='')
		{
			$wor_id = $this->input->get('wor_id');
			$sql = " AND W.wor_id='$wor_id'";
		}

		$fte_query = $this->db->query($fte_sql.$sql);
		$hourly_query = $this->db->query($hourly_sql.$sql);
		$project_query = $this->db->query($project_sql.$sql);
		$volume_based_query = $this->db->query($volume_based_sql.$sql);

		$fte_result = $fte_query->result_array();
		$hourly_result = $hourly_query->result_array();
		$project_result = $project_query->result_array();
		$volume_based_result = $volume_based_query->result_array();

		/*debug($fte_result);
		debug($hourly_result);
		debug($project_result);
		debug($volume_based_result);exit;*/
		$temp_res = array();
		if(!empty($fte_result))
		{
			$temp_res = array_merge($temp_res,$fte_result);
		}
		if(!empty($hourly_result))
		{
			$temp_res = array_merge($temp_res,$hourly_result);
		}
		if(!empty($project_result))
		{
			$temp_res = array_merge($temp_res,$project_result);
		}
		if(!empty($volume_based_result))
		{
			$temp_res = array_merge($temp_res,$volume_based_result);
		}
		$sorted_array = array();
		// array_unique($temp_res, SORT_REGULAR);
		$temp_res = array_map("unserialize", array_unique(array_map("serialize", $temp_res)));
		// debug($temp_res);exit;
		foreach ($temp_res as $key => $row)
		{
			$sorted_array[$key] = $row['project_type'];
		}
		array_multisort($sorted_array, SORT_ASC, $temp_res);
		$result['rows'] = $temp_res;
		return $result;
	}
}