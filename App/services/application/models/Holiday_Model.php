<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Holiday_Model extends INET_Model
{
	function __construct() 
	{
		parent::__construct();
	}

	function build_query() 
	{
		$filter = json_decode($this->input->get('filter'),true);
		
		$filterQuery = $this->input->get('query');
		$fromWOR = ($this->input->get('fromWOR')!="")?$this->input->get('fromWOR'):"";
		$options['filter'] = $filter;
		
		/* codeigniter format */
		$this->db->select("holiday_id, name, DATE_FORMAT(holidays.date, '%d-%b-%Y') AS date, CONCAT(name, ' - ', DATE_FORMAT(holidays.date, '%b %d, %Y')) AS name_date, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS added_by",false);
		$this->db->join('employees', 'employees.employee_id = holidays.added_by','inner');
		$this->db->from("holidays");
		$this->db->where('holidays.status', '1');
		if($fromWOR!="")
		{
			$this->db->where('holidays.date >=', date('Y-m-d'));
		}
		$this->db->order_by('holidays.date', 'ASC');

		$filterWhere = '';
		if(isset($options['filter']) && $options['filter'] != '') 
		{
			foreach ($options['filter'] as $filterArray) 
			{
				$filterFieldExp = explode(',', $filterArray['property']);
				foreach($filterFieldExp as $filterField) 
				{
					if(($filterField != '') && isset($filterArray['value']) && (trim($filterArray['value']) != '')) 
					{
						$filterWhere .= ($filterWhere != '')?' OR ':'';
						$filterWhere .= ($filterField == 'name')?'name':$filterField;
						
						$filterWhere .=" LIKE '%".$filterArray['value']."%'";
					}
				}
			}
			
			if($filterWhere != '') 
			{
				$this->db->where('('.$filterWhere.')');
			} 
		}
		
		if($filterQuery!="" )
		{
			$filterWhere = $filterName." LIKE '%".$filterQuery."%'";
			$this->db->where($filterWhere);
		}			
	}

	
	function view() 
	{
		$options = array();
		if($this->input->get('hasNoLimit') != '1')
		{
			$options['offset'] = ($this->input->get('start') != '')? $this->input->get('start') : 0;
			$options['limit'] = ($this->input->get('limit') != '')? $this->input->get('limit') : 20;
		}
		
		$sort = json_decode($this->input->get('sort'),true);

		$options['sortBy'] = $sort[0]['property'];
		$options['sortDirection'] = $sort[0]['direction'];

		$this->build_query();
		$totQuery = $this->db->get();
		$totCount = $totQuery->num_rows();

		$this->build_query();
		if(isset($options['sortBy'])) 
		{
			$this->db->order_by($options['sortBy'], $options['sortDirection'].' ');
		}
		
		if(isset($options['limit']) && isset($options['offset'])) 
		{
			$this->db->limit($options['limit'], $options['offset']);
		}
		else if(isset($options['limit'])) 
		{
			$this->db->limit($options['limit']);
		}
		
		$query = $this->db->get();
		// echo $this->db->last_query(); exit;
		$results = array_merge(array('totalCount' => $totCount),array('rows' => $query->result_array()));
		
		return $results;
	}

	function addHoliday( $data = '' ) 
	{
		$setData = $data;
		
		$empData = $this->input->cookie();
		$added_by = $empData['employee_id'];
		
		if(!empty($setData))
		{
			$dataset = array(
				'name'		=> $setData['name'],
				'date'		=> date("Y-m-d", strtotime($setData['date'])),
				'added_by'	=> $added_by,
				'added_on'	=> date("Y-m-d H:i:s")
			);
			
			$retVal = $this->insert($dataset, 'holidays');

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Holiday ID:'.$this->db->insert_id().') has been added holiday Successfully.';
			$this->userLogs($logmsg);
			
			return $retVal;
		}
		else 
			return -1;
	}

	function updateHoliday( $data = '' ) 
	{
		$setData = $data;
		
		$empData = $this->input->cookie();
		$updated_by = $empData['employee_id'];
		
		if(!empty($setData))
		{
			$where = array();
			$set = array();
			$where['holiday_id'] = $setData['holiday_id'];
			
			$dataset = array(
				'name'			=> $setData['name'],
				'date'			=> date("Y-m-d", strtotime($setData['date'])),
				'updated_by'	=> $updated_by,
				'updated_on'	=> date("Y-m-d H:i:s")
			);
			
			$retVal = $this->update($dataset, $where, 'holidays');

			$empData    = $this->input->cookie();
			$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Holiday ID:'.$setData['holiday_id'].') has been updated holiday Successfully.';
			$this->userLogs($logmsg);
			
			return $retVal;
		}
		else
			return '-1';
	}

	function deleteHoliday( $idVal = '' ) 
	{
		$empData = $this->input->cookie();
		$updated_by = $empData['employee_id'];
		
		$where = array();
		$dataset = array();
		$where['holiday_id'] = $idVal;
		
		$dataset = array(
			'status'		=> 0,
			'updated_by'	=> $updated_by,
			'updated_on'	=> date("Y-m-d H:i:s")
		);
		
		$retVal = $this->update($dataset, $where, 'holidays');

		$empData    = $this->input->cookie();
		$logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].')(Holiday ID:'.$idVal.') has been removed holiday Successfully.';
		$this->userLogs($logmsg);
		
		return $retVal;
	}
    
}
	
?>