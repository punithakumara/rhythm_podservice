<?php
// Insert the path where you unpacked log4php
require_once('log4php/Logger.php');

// Tell log4php to use our configuration file.
Logger::configure('config.xml');

class TestLog{
	/** Holds the Logger. */
    	private $logger;

    	/** Logger is instantiated in the constructor. */
    	public function __construct()
    	{
    	    // The __CLASS__ constant holds the class name, in our case "Foo".
    	    // Therefore this creates a logger named "Foo" (which we configured in the config file)
    	    $this->logger = Logger::getLogger(__CLASS__);
    	}

	protected function fatherName(){
		$this->logger->info("Inside TestLog --> fatherName method");
		$this->logger->debug("Echoing the name of the Father");
		echo "Father: Mr. Ashok Bansal<br>";
	}

	protected function motherName(){
		$this->logger->info("Inside TestLog --> motherName method");
		$this->logger->debug("Echoing the name of the Mother");
		echo "Mother: Mrs. kiran Bansal<br>";
	}

	public function profession(){
		$this->logger->info("Inside TestLog --> profession method");
		$this->logger->debug("Echoing the profession");
		echo "Business in retail field<br>";
		try{
			$result = 15/0;
		}
		catch(Exception $e){
			$this->logger->error("Exception caught inside TestLog --> profession" . $e->getMessage());
		}
	}
}
?>
