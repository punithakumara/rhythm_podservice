<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transition extends INET_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model("Transition_Model");
	}
	
	
	/**
	 * Function to populate the employee details to the transition grid
	 */
	public function transition_view_get() 
	{
		$data = $this->Transition_Model->getTransition();
			
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		} 
		else 
		{
			$httpCode = 200;
		}
		$this->response($data,$httpCode);
	}

	
	/**
	* Function for Add G1, G2, G3 & G4 to Multiple Process
	*/
	public function empSaveEmpProTransition_get() 
	{
		$retVal = $this->Transition_Model->addEmpProTrans();

		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		} 
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't do Transition Since Process AMs are different.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}
	
	
	
	/**
	 * Function for Remove G1, G2, G3, G3.1 from Multiple Process
	 */
	public function empRemoveEmpTransition_get() 
	{
		$retVal = $this->Transition_Model->empRmveTransition();
			
		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		}
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't do Transition Since Process AMs are different.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}
	
	
	
	/**
	 * Get process count by employee wise
	 */
	public function processCount_get()
	{
		$retVal = $this->Transition_Model->getProcessCount();
	
		if($retVal > 0)
		{
			$data = array("success" => "true");
		} 
		else 
		{
			$data = array("success" => "false");
		}
		$this->response($data);
	}
	

	/**
	 * Function for release employees to HR Pool.
	 */
	public function empSaveTransition_get() 
	{
		$retVal = $this->Transition_Model->pushHrPool();
			
		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		} 
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't do Transition Since Process AMs are different.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}

	
	public function empSaveShiftTransition_get() 
	{
		$retVal = $this->Transition_Model->shiftTransition();
			
		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		} 
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't do Transition Since Process AMs are different.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}


	/**
	 * Function for supervisior transition.
	 */
	public function empSaveLeadTransition_get() 
	{
		$retVal = $this->Transition_Model->leadTransition();
			
		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		} 
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't do Transition Since Process AMs are different.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}

	/**
	 * Function for pull from HR transition.
	 */
	public function SaveFromHRPoolTransition_get() 
	{
		$retVal = $this->Transition_Model->HRPoolTransition();
			
		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		} 
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't do Transition Since Process AMs are different.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}
}
	