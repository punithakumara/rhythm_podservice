<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class EmployeeSkillset extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model		
		$this->load->model('EmployeeSkillset_Model');
	}
	
	
	/**
	* Fetch skillset list
	*
	* @param 
	* @return JSON
	*/
	function skillset_list_get()
	{
		$data = $this->EmployeeSkillset_Model->list_skillsets();
		$httpCode = 200;
		$this->response($data,$httpCode);
	}
	
	function skillset_add_post()
    {
        $data   = $this->post();
		// echo "<PRE>";print_r($data);exit;
		
        $retVal = $this->EmployeeSkillset_Model->addSkillset($data);
		//echo $retVal; exit;
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Skill Deatail added successfully.",
                "success" => "true"
            );      
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding Skill data.',
                "success" => "false"
            );
        $this->response($data);
    }

	function skillset_edit_post( $idVal = '',$data='')
    {
         $data   = $this->post();
		
		// print_r($data);
		// exit;
        $retVal = $this->EmployeeSkillset_Model->updateSkillset( $idVal,$data);
        
        if ($retVal > 0)
            $data = array(
                "title" => "Updated",
                "msg" => "Skill Detail updated successfully.",
                "success" => "true"
            );       
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while updating skill data',
                "success" => "false"
            );
        $this->response($data);
    }
	// for deleting
	
	function skillset_dele_post($idVal = '')
	{
		
		$data = $this->post();;
		//$delData = json_decode(trim($data), true);
		
		$retVal = $this->EmployeeSkillset_Model->deleteSkillset($data['id']);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Skill Detail deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}	
	function skillset_erase_delete($idVal = '')
	{
		echo "here";
	}
	
}

?>