<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class EmployeeAchievement extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model		
		$this->load->model('EmployeeAchievement_Model');
	}	
	/**
	* Fetch Achievement list
	*
	* @param 
	* @return JSON
	*/
	function achievement_list_get()
	{
		$data = $this->EmployeeAchievement_Model->list_achievements();
		$httpCode = 200;
		$this->response($data,$httpCode);
	}
	
	function achievement_add_post()
    {
        $data   = $this->post();
	   // echo "<PRE>";print_r($data);exit;
		
        $retVal = $this->EmployeeAchievement_Model->addAchievement($data);
		//echo $retVal; exit;
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Achievement added successfully.",
                "success" => "true"
            );      
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding achievement data.',
                "success" => "false"
            );
        $this->response($data);
    }

	function achievement_edit_post( $idVal = '',$data='')
    {
         $data   = $this->post();
		
		// print_r($data);
		// exit;
        $retVal = $this->EmployeeAchievement_Model->updateAchievement( $idVal,$data);
        
        if ($retVal > 0)
            $data = array(
                "title" => "Updated",
                "msg" => "Employee achievement updated successfully.",
                "success" => "true"
            );       
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while updating achievement data',
                "success" => "false"
            );
        $this->response($data);
    }
	// for deleting
	
	function achievement_del_delete($idVal = '')
	{
		$data = $this->delete('achievement');
		$delData = json_decode(trim($data), true);		
		$retVal = $this->EmployeeAchievement_Model->deleteAchievement($delData['id']);		
		if($retVal > 0) 
		{
			$data = array("msg" => "Employee achievement detail deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}		
}

?>