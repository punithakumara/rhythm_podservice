<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilization extends INET_Controller
{
	public $uploadPath = ""; 
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Utilization_Model');
		$this->load->library('CSVReader.php');
		$this->config->load('admin_config');
		$this->uploadPath = $this->config->item('REPOSITORY_PATH').'resources/uploads/opsdata/';
	}
	
	function utilization_view_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->view($params);
		if(!$data)
			$data = array("msg" => "Something went wrong.", "success" => false);
		
		$this->response($data);
	}	
	
	function getTotalHours_post()
	{
		$data = $_REQUEST;

		$resData = $this->Utilization_Model->getTotalHours( $data );

		$this->response($resData);
	}
	
	
	function utilization_create_post()
	{
		$data = $_REQUEST;

		$retVal = $this->Utilization_Model->saveUtilization( $data );

		if($retVal > 0) 
		{
			if($data['utilization_id'] != '')
			{
				$msg = "Record Updated Successfully.";
			}
			else
			{
				$msg = "Record Saved Successfully.";
			}
			if(isset($data['duplicate'])){
				$msg = "Record Duplicated Successfully.";
			}
			$data = array("msg" => $msg, "success" => "true" ,"exceed" => "0");
			$this->response($data);
		}
		else 
		{
			if($retVal == -1){
				$data = array("msg" => 'Utilization Hours can\'t exceed 24 hours', "success" => "false","exceed" => "1");
			}else{
				$data = array("msg" => 'Error occured while updating Record data.', "success" => "false","exceed" => "0");
			}
			$this->response($data);
		}
	}
	
	
	function utilization_update_put() 
	{
		$retVal = $this->Utilization_Model->saveUtilization( $data );
		$this->response($retVal);
		$params = $this->put();
		
		$data['client_id'] = $params['client_id'];
		$data['utilization_id'] = $params['utilization_id'];
		if(isset($params['WeekendSupport']))
			$data['WeekendSupport'] = $params['WeekendSupport'];
		if(isset($params['ClientHoliday']))
			$data['ClientHoliday'] = $params['ClientHoliday'];
		if(isset($params['Date']))
			$data['TaskDate'] = $params['Date'];
		$data['utilization'] = $params['utilization'];
		$dashboard = isset($params['dashboard']) ? $params['dashboard'] : "";
		if($dashboard=="") 
			$data['employee_id'] = $params['employee_id'];
		
		if($dashboard==1)
		{
			$retVal = $this->Utilization_Model->saveDashboardUtilization($data);
		}
		else 
			$retVal = $this->Utilization_Model->saveUtilization($data);
		
		if($retVal > 0) 
		{
			if($params['utilization_id'] != '')
			{
				$msg = "Updated successfully.";
			}
			else
			{
				$msg = "Saved successfully.";
			}
			$data = array("msg" => $msg, "success" => "true");
			$this->response($data);
		}
		else if($retVal == 0) 
		{
			$data = array("msg" => 'No Changes made to save.', "success" => "nochange");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
			$this->response($data);
		}
	}
	
	
	function utilizationDashboard_upload_post()
	{
		$inputValues = $this->post();
		$retVal = $this->Utilization_Model->ImportUtilization($inputValues);
		
		if($retVal > 0)
			$data = array("msg" => "Data Imported Successfully.", "success" => true);
		else
			$data = array("msg" => "Data Imported Error.", "success" => true);
		$this->response($data);
	}

	function utilization_future_range_get()
	{
		$data = $this->Utilization_Model->get_future_week_range();
		if(!$data)
			$data = array("msg" => "Something went wrong.", "success" => false);
		
		$this->response($data);
	}

	function utilizationSaveComments_post()
	{
		$params = $this->post();
		
		$UtilizationID = (isset($params['UtilizationID'])) ? $params['UtilizationID'] : "";
		
		$res = $this->Utilization_Model->update_task_comments($UtilizationID, $params);
		
		if(isset($res))
		{
			$response["message"] = "Comments updated successfully";
			$response["success"] = 1;
		}
		// $empData    = $this->input->cookie();
		// $logmsg = $empData['username'].'(EmpID:'.$empData['employee_id'].') timesheet comment updated.';
		// $this->INET_Controller->userLogs($logmsg);
		$this->response($response);
	}
	
	
	function downloadCsvTemplate_post()
	{
		ini_set('memory_limit', '-1');
		$inputValues = $this->post();
		// echo "<pre>"; print_r($inputValues); exit;
		$path = FCPATH."download/";
		$Export = (isset($inputValues['Export'])) ? $inputValues['Export'] : "";
		if($Export=='insighExport')
		{
			$filename = "Utilization_insight.csv";
			$retVal = $this->Utilization_Model->utilization_insightCSV($inputValues);
			
			if(empty($retVal['0']['0']))
			{					
				$this->response(array("success" => true, "data" => 0));
			}
		}
		else
		{	
			if($Export=='Template')
			{
				$filename = "UtilizationTemplate.csv";
			}
			else
			{
				$filename = "Utilization.csv";
			}
			$retVal = $this->Utilization_Model->downloadCsv($inputValues);

			//echo '<pre>'; print_r($retVal); exit;
		}
		// $retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");			
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;        
		foreach ($retVal[0] as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true, "data" => 1));
	}
	
	function utilization_dashboard_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->insightview($params);
		if(!$data)
			$data = array("msg" => "You are not approver to this process.", "success" => false);
		$this->response($data);
	}

	function volume_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->getVolumeModel($params);
		if(!$data)
			$data = array("msg" => "You are not approver to this process.", "success" => false);
		$this->response($data);
	}

	function week_days_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->get_weekDays($params['week_range'],$params['project_type']);
		if(!$data)
			$data = array("msg" => "You are not approver to this process.", "success" => false);
		$this->response($data);
	}
	
	
	function utilization_dashboardedit_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->getUtilAttributes($params);
		if(count($data)!=0)
			$return_data = array("data"=>$data, "success"=>true);
		else 
			$return_data = array("success"=>false);
		$this->response($return_data);
	}
	
	
	function utilization_approve_post()
	{
		$inputValues = $this->post();			
		$utilizationArraycount = 0;
		$retVal = $this->Utilization_Model->ApproveUtilization($inputValues);
		$utilizationArraycount = count(json_decode($inputValues['UtilizationArray']));
		$title = "Approved";
		$msg = "Approved";
		$record = 'Record';
		if($utilizationArraycount > 1)
		{
			$record = 'Records';
		}
		if($inputValues['value']==2)
		{
			$title = "Reject";
			$msg = "Rejected";
		}
		if($inputValues['value']==1)
		{
			$title = "Approve";
			$msg = "Approved";
		}			
		if($retVal==0)
			$data = array("title"=>$title, "msg" => "$record Already $msg.", "success" => true);
		else if($retVal > 0)
			$data = array("title"=>$title, "msg" => "Record $msg Successfully.", "success" => true);
		else
			$data = array("msg" => "Data Imported Error.", "success" => false);
		$this->response($data);
	}

	function project_types_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->project_types($params);
		if(!$data)
			$data = array("msg" => "No project types available", "success" => false);
		$this->response($data);
	}
	
	function insight_billble_approve_post()
	{
		$inputValues = $this->post();			
		$utilizationArraycount = 0;
		$retVal = $this->Utilization_Model->insightBillableApprove($inputValues);
		$utilizationArraycount = count(json_decode($inputValues['UtilizationArray']));
		
		if($inputValues['tab_type'] == 'weekend_list')
		{
			$title = "Weekend Coverage";

			if($inputValues['value']==3)
			{
				$data = array("title"=>$title, "msg" => "Billable Hours Approved.", "success" => true);
			}			
			else if($inputValues['value']==1)
			{
				$data = array("title"=>$title, "msg" => "Billable Hours Excluded.", "success" => true);					
			}
			else
			{
				$data = array("msg" => "Data Imported Error.", "success" => false);
			}	
		}
		
		if($inputValues['tab_type'] == 'holiday_list')
		{
			$title = "Holiday Coverage";
			if($inputValues['value']==4)
			{
				$data = array("title"=>$title, "msg" => "Billable Hours Approved.", "success" => true);
			}			
			else if($inputValues['value']==1)
			{
				$data = array("title"=>$title, "msg" => "Billable Hours Excluded.", "success" => true);					
			}
			else
			{
				$data = array("msg" => "Data Imported Error.", "success" => false);
			}	
		}

		$this->response($data);
	}

	function utilization_insightCSV()
	{
		$retVal = $this->Utilization_Model->utilization_insightCSV();
	}
	
	
	function utilization_dele_post()
	{
		$data =  $this->post();
	
		$retVal = $this->Utilization_Model->deleteUtlization($data);
		if($retVal > 0) 
		{
			$data = array("success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("success" => "false");
			$this->response($data);
		}
	}
	
	
	function role_code_list_get()
	{
		$params = $this->get();
		if($params['filter_rolecode']==1)
		{
			$data = $this->Utilization_Model->getUtilRoleCode();
		}
		else
		{
			$data = $this->Utilization_Model->getRoleCode($params);
			if(!$data)
				$data = array("msg" => "No Process available for this Client.", "success" => false);	
		}
		$this->response($data);
	}
	
	
	function requestRegion_list_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->GetRequestRegion($params);
		if(!$data)
			$data = array("msg" => "No Regions Available.", "success" => false);
		$this->response($data);
	}
	
	
	function task_code_list_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->getTaskCode($params);
		if(!$data)
			$data = array("msg" => "No task available", "success" => false);
		$this->response($data);
	}
	
	
	function SubTaskList_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->getSubTask();
		if(!$data)
			$data = array("msg" => "No Sub tasks available", "success" => false);
		$this->response($data);
	}
	
	
	function ClientAttributes_get()
	{
		$data = $this->Utilization_Model->getAttributes();
		$this->response($data);
	}

	function ClientAddAttr_get()
	{
		$params = $this->get();
		$data = $this->Utilization_Model->getClientAddAttr($params);
		$this->response($data);
	}

	function utilization_upload_post(){
		
		$inputValues = $this->post();
		// debug($inputValues); exit;
		$retVal = $this->Utilization_Model->ImportTasks($inputValues);
		
		if($retVal > 0)
		{
			$data = array("msg" => "Data Imported Successfully.", "success" => true);
		}
		elseif($retVal == 0)
		{
			$data = array("msg" => "Error in Importing Data. Kindly Upload a Valid Template.", "success" => false);
		}
		elseif($retVal == -1)
		{
			$data = array("msg" => "Error in Importing Data. Few columns have empty value(s).", "success" => false);
		}
		elseif($retVal == -2)
		{
			$data = array("msg" => "Error in Importing Data. Invalid Date Format.", "success" => false);
		}
		elseif($retVal == -3)
		{
			$data = array("msg" => "Error in Importing Data. Invalid Time Format.", "success" => false);
		}
		elseif($retVal == -4)
		{
			$data = array("msg" => "Error in Importing Data. Incorrect data.", "success" => false);
		}

		// echo"<pre>";print_r($data);exit;	
		$this->response($data);
	}

	public function duplicateRow_post()
	{			
		$retVal = $this->Utilization_Model->duplicateRow();
		if($retVal==-1)
			$data = array("msg" => "Record already exists.", "success" => false);
		else if($retVal > 0)
			$data = array("msg" => "Record duplicated Successfully.", "success" => true);
		else
			$data = array("msg" => "Error Occured.", "success" => false);
		$this->response($data);
	}
}
?>