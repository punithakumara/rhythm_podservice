<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Employees extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model
		$this->load->model(array('Employees_Model','Employee_Model'));
		$this->load->model('Employee_Model');
	}
	
	
	/**
	* Fetch Active Employees list
	*
	* @param 
	* @return JSON
	*/
	function employees_details_get()
	{			
		$data = $this->Employees_Model->list_employees();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	function util_employees_details_get()
	{
		$data = $this->Employees_Model->getUtilEmployees();
		$this->response($data);
	}
	
	function employee_add_post()
    {
        $data   = $this->post('employ');
	 //echo "<PRE>";print_r($data);exit;
		
        $retVal = $this->Employees_Model->addEmployee($data);
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Employee details added successfully.",
                "success" => "true"
            );
        else if ($retVal == -1)
            $data = array(
                "title" => "Error",
                "msg" => "Employee ID or Employee Email already exists.",
                "success" => "true"
            );
        else if ($retVal == -2)
            $data = array(
                "title" => "Success",
                "msg" => "Employee details updated successfully.",
                "success" => "true"
            );
         else if ($retVal == -3)
            $data = array(
                "title" => "Error",
                "msg" => "Primary and Alternate numbers are same",
                "success" => "true"
            );
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding Employee data.',
                "success" => "false"
            );
        $this->response($data);
    }
	
	public function notificationCount_get()
	{
		$empData        = $this->input->cookie();		
		$res            = $this->Employees_Model->getEmployeeNotifications($empData['employee_id']);
		if($res['count'] == 0) {
			$httpCode   = 200;
		} else {
			$httpCode   = 200;
		}
		$this->response($res, $httpCode);
	}
	
	function getNotifications_get()
	{
		$data = $this->Employees_Model->getNotifications();
		if ($data['totalCount'] == 0) {
			$httpCode = 204;
		} else {
			$httpCode = 200;
		}
	
		$this->response($data, $httpCode);
	}


	public function Managers_get() 
    {
        $data = $this->Employee_Model->getManagers();
        $this->response($data, 200);
    }


	public function AllManagers_get() 
    {
        $data = $this->Employee_Model->getAllManagers();
        $this->response($data, 200);
    }
	

	public function ReportTo_get()
    {
        $data = $this->Employees_Model->getReportToDetails();
        
        if ($data['totalCount'] == 0) {
            $httpCode = 204;
        } else {
            $httpCode = 200;
        }
        
        $this->response($data, $httpCode);
    }
	
	
	public function AllReportees_get()
    {
        $data = $this->Employees_Model->getAllReportDetails();
        
        if ($data['totalCount'] == 0) {
            $httpCode = 204;
        } else {
            $httpCode = 200;
        }
        
        $this->response($data, $httpCode);
    }

    public function AllReportees1_get()
    {
        $data = $this->Employees_Model->getAllReportDetails_1();
        
        if ($data['totalCount'] == 0) {
            $httpCode = 204;
        } else {
            $httpCode = 200;
        }
        
        $this->response($data, $httpCode);
    }

    public function PullFromHR_get()
    {
        $data = $this->Employees_Model->getPullFromHRDetails();
        
        if ($data['totalCount'] == 0) {
            $httpCode = 204;
        } else {
            $httpCode = 200;
        }
        
        $this->response($data, $httpCode);
    }
	

	function employee_update_put($idVal = '')
    {
        $data   = $this->put('employ');
        $retVal = $this->Employees_Model->updateEmployee($idVal, $data);
        
        if ($retVal > 0)
            $data = array(
                "title" => "Added",
                "msg" => "Employee added successfully.",
                "success" => "true"
            );
        else if ($retVal == -1)
            $data = array(
                "title" => "Existing",
                "msg" => "Employee ID or Employee Email already exists.",
                "success" => "true"
            );
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while updating employee data',
                "success" => "false"
            );
        $this->response($data);
    }
	
	
	function loggedinUserDashboard_get()
    {
        $empData = $this->input->cookie();
        $query   = $this->db->query("SELECT user_dashboard FROM `employees` WHERE employee_id=" . $empData['employee_id']);
        if ($query->num_rows() > 0) 
		{
            $userDash = $query->row_array();
            $this->response(json_decode($userDash['user_dashboard']), 200);
        }
        $this->response('', 204);
    }
	
	
	function loggedinUserDataIcons_get()
    {
        $empData = $this->input->cookie();
        $query   = $this->db->query("SELECT dashboard_tools FROM `employees` WHERE employee_id=" . $empData['employee_id']);
		
		if ($query->num_rows() > 0) 
		{
            $userDash = $query->row_array();
            $this->response(json_decode($userDash['dashboard_tools']), 200);
        }
        $this->response('', 204);
	}
	
	
	public function TlBillableEmployees_get()
    {
	    $utilData = $this->input->get();
		$client_id = $utilData['client_id'];
		$data = $this->Employees_Model->getTlBillableEmployees($client_id);
		// echo"<PRE>";print_r($data);exit;
		$billableData = array();
		if($data) 
		{
			foreach($data as $key=>$value)
			{
				
					$billableData[$key]['employee_id'] = $value['employee_id'];
					$billableData[$key]['first_name'] = $value['first_name'];
					$billableData[$key]['comment'] = $value['comment'];
					$billableData[$key]['email'] = $value['email'];
					$billableData[$key]['designation_id'] = $value['designation_id'];
					$billableData[$key]['name'] = $value['name'];
					$billableData[$key]['grades'] = $value['grades'];
						
			}
			$this->response(array_values($billableData), 200);
		}		
	}
	
	
	function userDashboard_post()
    {
        $empData = $this->input->cookie();
        $data    = $this->post('userDashboard');  
        $icons = $this->post('icons') ? $this->post('icons') : '';
        if($icons == "")     
        	$this->db->query("UPDATE `employees` SET user_dashboard='" . $data . "' WHERE employee_id='" . $empData['employee_id'] . "'");		
    	else 
    		$this->db->query("UPDATE `employees` SET dashboard_tools='" . $data . "' WHERE employee_id='" . $empData['employee_id'] . "'");
    }
	
	 public function associateManagers_get()
    {
        $data = $this->Employees_Model->getAssociateManagers();
        $this->response($data, 200);
    }
	public function TlEmployees_get()
    {
    	$data = $this->Employees_Model->getTlEmployees();
		//print_r($data); exit;
     	$this->response($data, 200);
    }
	
	/**
	* Fetch Stake Holders list
	*
	* @param 
	* @return JSON
	*/
	function stake_holders_get()
	{			
		$data = $this->Employees_Model->list_stakeholders();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Fetch Time entry approver list
	*
	* @param 
	* @return JSON
	*/
	function time_entry_approvers_get()
	{			
		$data = $this->Employees_Model->list_timeEntryApprover();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Fetch Delivery Managers list
	*
	* @param 
	* @return JSON
	*/
	function delivery_managers_get()
	{			
		$data = $this->Employees_Model->list_delivery_managers();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Fetch Engagement Managers list
	*
	* @param 
	* @return JSON
	*/
	function engagement_managers_get()
	{			
		$data = $this->Employees_Model->list_engagament_managers();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Fetch All Client Partner
	*
	* @param 
	* @return JSON
	*/
	function client_partner_get()
	{			
		$data = $this->Employees_Model->list_client_partner();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
		/**
	* Fetch Active Team Lead and Associate Manager details for seperate stores
	*
	* @param 
	* @return JSON
	*/
	function tl_am_details_get()
	{			
		$data = $this->Employees_Model->list_tl_am_details();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}

	function am_lead_details_get()
	{			
		$data = $this->Employees_Model->list_am_lead_details();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	function tl_am_details_incident_get()
	{			
		$data = $this->Employees_Model->list_tl_am_details_incident();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	function list_supportcoverage_get()
	{			
		$data = $this->Employees_Model->list_supportcoverage();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	 * Save promotion of employee
	 *
	 * @param
	 * @return JSON
	 */
	function savePromotion_post()
	{
		$data = $this->post();
	
		$return_value = $this->Employees_Model->update_promotion($data);
	
		if($return_value!="")
		{
			$data = array("msg" => "Details updated successfully.", "success" => "true", "title" => "Success");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Update resignation of employee
	 *
	 * @param
	 * @return JSON
	 */
	function updateResignation_post()
	{
		$data = $this->post();
	
		$return_value = $this->Employees_Model->update_resignation($data);
	
		if($return_value!="")
		{
			$data = array("msg" => "Details updated successfully.", "success" => "true", "title" => "Success");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Update relieve of employee
	 *
	 * @param
	 * @return JSON
	 */
	function updateRelieve_post()
	{
		$data = $this->post();
	
		$return_value = $this->Employees_Model->update_relieve($data);
	
		if($return_value!="")
		{
			$data = array("msg" => "Details updated successfully.", "success" => "true", "title" => "Success");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}

	public function upDateNotification_post()
    {
        $data = $this->post();
        $this->Employee_Model->updateNotificationStatus($data);
    }
	
}

?>