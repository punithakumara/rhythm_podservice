<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class ZohoEmployees extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Employees_Model');
	}
	
	function employee_add_get()
    {
		//$zohoDump=file_get_contents("https://people.zoho.com/people/api/forms/P_EmployeeView/records?authtoken=ee4bd52f6081979e3bbe21f76f793ba8");
		//$zohoData=file_get_contents("https://people.zoho.com/people/api/forms/employee/getRecords?authtoken=ee4bd52f6081979e3bbe21f76f793ba8&searchParams={searchField: 'EmailID', searchOperator: 'Like', searchText: '1234@theoreminc.net'}");
		//$zohoShift_Qual=file_get_contents("https://people.zoho.com/people/api/forms/Internal_Job_Position/getRecords?authtoken=ee4bd52f6081979e3bbe21f76f793ba8&searchParams=%7BsearchField:'EmailID',searchOperator:'Like',searchText:'dharmavathy.ponnappa@theoreminc.net'%7D");
		//$zohoData=file_get_contents("https://people.zoho.com/people/api/forms/employee/getRecords?authtoken=ee4bd52f6081979e3bbe21f76f793ba8&searchParams=%7BsearchField: 'EmailID', searchOperator: 'Like', searchText: '1234@theoreminc.net'%7D");
		//echo "<PRE>";print_r(json_decode($zohoData));
		//echo "<PRE>";print_r(json_decode($zohoShift_Qual));
		//exit;
		echo "<pre>";
		$zohoDump=file_get_contents("https://people.zoho.com/people/api/forms/employee/getRecords?authtoken=ee4bd52f6081979e3bbe21f76f793ba8&searchParams=%7BsearchField:'ModifiedTime',searchOperator:'Yesterday',searchText:'Active'%7D%7BsearchField:'AddedTime',searchOperator:'Yesterday',searchText:'Active'%7D");
		//$zohoDump=file_get_contents("https://people.zoho.com/people/api/forms/employee/getRecords?authtoken=ee4bd52f6081979e3bbe21f76f793ba8&searchParams=%7BsearchField:'EmailID',searchOperator:'Like',searchText:'kurucheti.rajasrikanth@theoreminc.net'%7D");
		$zohoData=json_decode($zohoDump);
		print_r($zohoData); exit;	
		$zdata=$zohoData->response->result; 
		//echo count($zdata);
		//exit;
		for($i=0;$i<count($zdata); $i++)
		{			
		foreach($zdata[$i] as $key => $value) {
		$val=$value[0];
		$data['employee_id']='';
		$data['company_employ_id']=$val->EmployeeID;
		$data['status']=3;
		$reportingMailId=explode(" ",$val->Reporting_To);
		$cnt=count($reportingMailId)-1;
		$leadId = $this->Employees_Model->get_lead_id($reportingMailId[$cnt]);		
		$data['primary_lead_id']=$leadId;
		$data['first_name']=$val->FirstName;
		$data['full_name']='';
		$data['last_name']=$val->LastName;
		$data['Designation_name']='';
		$DesignationId = $this->Employees_Model->get_designation_id($val->Designation); 
		$data['designation_id']=$DesignationId;
		$data['email']=$val->EmailID;
		echo $val->EmailID;
		$data['mobile']=$val->Mobile;
		$LocationId = $this->Employees_Model->get_location_id($val->LocationName);
		$data['location_id']=$LocationId;
		$doj_date = str_replace('/', '-',$val->Dateofjoining);
		$dob_date = str_replace('/', '-',$val->Date_of_birth);
		$data['doj']=date('Y-m-d', strtotime($doj_date));
		$data['dob']=date('Y-m-d', strtotime($dob_date));
		$data['gender']=$val->Gender;
		$data['shift_id']='';
		$data['shift_code']='';
		$data['qualification_id']='';
		$data['Grade']=$val->Grade;
		$verticalId = $this->Employees_Model->get_vertical_id($val->Vertical);
		$data['VerticalID']=$verticalId;
		$data['assigned_vertical']=$verticalId;
		$data['transition_status']='';
		$data['user_dashboard']='';
		$data['dashboard_tools']='';
		$data['grade_level']='';
		$data['vertical_name']=$verticalId;
		$data['resigned_notes']='';
		$data['resign_flag']='';
		$data['relieve_flag']='';
		$data['relieve_date']='';
		$data['relieve_type']='';
		$data['lead_name']='';
		$data['stake_holder']='';
		$data['comments']='';
		$data['personal_email']=$val->EmailID;
		$data['alternate_number']='';
		$data['permanent_address']=$val->Address;
		$data['permanent_city']='';
		$data['permanent_state']='';
		$data['permanent_zipcode']='';
		$data['same_as']='';
		$data['present_address']=$val->Address;
		$data['present_city']='';
		$data['present_state']='';
		$data['present_zipcode']='';
		$date = date('Y-m-d H:i:s');
		$data['created_date']=$date;
		$data['is_onshore']=0;			 		 
		//$i++;
		}
		}			
        $retVal = $this->Employees_Model->addEmployee(json_encode($data));
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Employee details added successfully.",
                "success" => "true"
            );
        else if ($retVal == -1)
            $data = array(
                "title" => "Error",
                "msg" => "Employee ID or Employee Email already exists.",
                "success" => "true"
            );
        else if ($retVal == -2)
            $data = array(
                "title" => "Success",
                "msg" => "Employee details updated successfully.",
                "success" => "true"
            );
         else if ($retVal == -3)
            $data = array(
                "title" => "Error",
                "msg" => "Primary and Alternate numbers are same",
                "success" => "true"
            );
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding Employee data.',
                "success" => "false"
            );
        $this->response($data);
    }		
}

?>