<?php defined('BASEPATH') OR exit('No direct script access allowed');

class NewIncidentLog extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('NewIncidentLog_Model');
		$this->load->model('Email_Trigger_Model');
	}
	
	/**
	* Function to get Error list
	*/
	function log_viewError_get()
	{
		$data = $this->NewIncidentLog_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Function to add new error log
	*/
	function log_addError_post() 
	{
		$data = $this->post('log');
		$retVal = $this->NewIncidentLog_Model->saveErrorLog( $data );
		if($retVal =='big size') {
			$data = array("msg" => "file size exceeded", "failure" => "true");
			$this->response($data);
		}
		if($retVal > 0) 
		{
			$data = array("msg" => "Error Log Added Successfully.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error Occured While Adding Technology Data.', "success" => "false");
			$this->response($data);
		}
	}
	

	/**
	* Function to update error log based on id
	*/
	function log_updateError_put( $idVal = '' ) 
	{
		$data = $this->put('log');
		$retVal = $this->NewIncidentLog_Model->saveErrorLog($data, $idVal);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Error Log Updated Successfully.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error Occured While Updating Technology Data.', "success" => "false");
			$this->response($data);
		}
	}


	/**
	* Function to delete error based on id
	*/
	function log_delError_delete($idVal = '')
	{
		$retVal = $this->NewIncidentLog_Model->deleteErrorLog($idVal);
		
		if($retVal > 0) {
			$data = array("msg" => "Error log Deleted Successfully.", "success" => "true");
			$this->response($data,200);
		}
		else {
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
	function log_viewAppr_get()
	{
		$data = $this->NewIncidentLog_Model->view_appr();
		//print_r($data);exit;
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Function to add new error log
	*/
	function log_addAppr_post() 
	{

		$data = $this->post('log');
		$retVal = $this->NewIncidentLog_Model->saveApprLog( $data );
		
		if($retVal =='big size') {
			$data = array("msg" => "file size exceeded", "failure" => "true");
			$this->response($data);
		}
		if($retVal > 0) 
		{
			$data = array("msg" => "Error Log Added Successfully.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error Occured While Adding Technology Data.', "success" => "false");
			$this->response($data);
		}
	}
	

	/**
	* Function to update error log based on id
	*/
	function log_updateAppr_put( $idVal = '' ) 
	{
		$data = $this->put('log');
		$retVal = $this->NewIncidentLog_Model->saveApprLog($data, $idVal);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Error Log Updated Successfully.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error Occured While Updating Technology Data.', "success" => "false");
			$this->response($data);
		}
	}


	/**
	* Function to delete error based on id
	*/
	function log_delAppr_delete($idVal = '')
	{
		$retVal = $this->NewIncidentLog_Model->deleteAppr($idVal);
		
		if($retVal > 0) {
			$data = array("msg" => "Error log Deleted Successfully.", "success" => "true");
			$this->response($data,200);
		}
		else {
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
	
	/**
	* Function to get Escalation  list
	*/
	function log_viewEscalation_get()
	{
		$data = $this->NewIncidentLog_Model->view_esacalation();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Function to add new Escalation log
	*/
	function log_addEsc_post() 
	{
		$data = $this->post('log');
		
		$retVal = $this->NewIncidentLog_Model->saveEscLog( $data );
		if($retVal =='big size') {
			$data = array("msg" => "file size exceeded", "failure" => "true");
			$this->response($data);
		}
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Escalation Log Added Successfully.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error Occured While Adding Escalation Data.', "success" => "false");
			$this->response($data);
		}
	}
	/**
	* Function to delete Escalation based on id
	*/
	function log_delEsc_delete($idVal = '')
	{
		$retVal = $this->NewIncidentLog_Model->deleteEscLog($idVal);
		
		if($retVal > 0) {
			$data = array("msg" => "Escalation log Deleted Successfully.", "success" => "true");
			$this->response($data,200);
		}
		else {
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
	
	/**
	* Function to review/approve error, and send mail when approved
	*/
	function reviewApproveError_post()
	{
		$data = $this->post();	
		$return_value = $this->NewIncidentLog_Model->reviewApproveError($data);
		
		if(($return_value!="") && ($data['type']=="review"))
		{			 
			$data = array("msg" => "Error Reviewed successfully.", "success" => "true", "title" => "Review");
			$httpCode = 200;
		}
		else if(($return_value!="") && ($data['type']=="approve"))
		{			 
			$this->Email_Trigger_Model->initiateEmailTrigger($data['client_id'],$data['id'],'Error');
			$data = array("msg" => "Error Approved successfully.", "success" => "true", "title" => "Approve");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}	
		$this->response($data,$httpCode);
	}	
	
	/**
	* Function to review/approve Escalation & appericiation, and send mail when approved
	*/
	function reviewApprove_post()
	{
		$data = $this->post();	
		
		$return_value = $this->NewIncidentLog_Model->reviewApprove($data);
		
		if(($return_value!="") && ($data['type']=="review"))
		{			 
			$data = array("msg" => "Escalation Reviewed successfully.", "success" => "true", "title" => "Review");
			$httpCode = 200;
		}
		else if(($return_value!="") && ($data['type']=="approve"))
		{			 
			$this->Email_Trigger_Model->initiateEmailTrigger($data['client_id'],$data['id'],'Escalation');
			$data = array("msg" => "Escalation Approved successfully.", "success" => "true", "title" => "Approve");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}	
		$this->response($data,$httpCode);
	}	
	
	function deletefile_post()
	{
		$data = $this->post();	
		
		$return_value = $this->NewIncidentLog_Model->deletefile($data);
		
		if(($return_value!=0))
		{			 
			
			$data = array("msg" => "File deleted successfully.", "success" => "true", "title" => "Delete File");
			$httpCode = 200;
		}
		
		else
		{
			$httpCode = 204;
		}	
		$this->response($data,$httpCode);
	}
	/**
	* Get Root Cause
	*/
	function rootcause_get()
	{		
		$data = $this->NewIncidentLog_Model->get_root_cause();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	
	/**
	* Function to get Error list
	*/
	function viewResponsiblePerson_get()
	{
		$data = $this->NewIncidentLog_Model->viewResponsiblePerson();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}

	function downloadExternalErrorCsv_get()
	{
		$path = FCPATH."download/";
		$filename = "ExternalError.csv";
		$retVal = $this->NewIncidentLog_Model->downloadExternalErrorCsv();
		$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0; 
		
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
		
	}

	function downloadEscalationCsv_get()
	{
		$path = FCPATH."download/";
		$filename = "Escalation.csv";
		$retVal = $this->NewIncidentLog_Model->downloadEscalationCsv();
		$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0; 
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
		
	}
}
?>