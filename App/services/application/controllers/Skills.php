<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Skills_Model');
	}

	// list view
	function getEmployees_get()
	{
		$data = $this->Skills_Model->getEmployees();

		echo json_encode($data);
	}

	function getSkillsets_get()
	{
		$data = $this->Skills_Model->getSkillsets();
		echo json_encode($data);
	}

	function skill_add_update_post() 
	{
		$data = $this->post();
		$retVal = $this->Skills_Model->AddUpdateSkill( $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Added","msg" => "Skill saved successfully.", "success" => "1");
		elseif($retVal == -1)
			$data = array("title"=>"Warning", "msg" => "This Skill already exist", "success" => "0");
		else 
			$data = array("title"=>"Warning","msg" => 'Error occured while adding Skill data.', "success" => false);
		
		$this->response($data);
	}


	function getTools_get()
	{
		$data = $this->Skills_Model->getTools();
		echo json_encode($data);
	}

	function tool_add_update_post() 
	{
		$data = $this->post();
		$retVal = $this->Skills_Model->AddUpdateTool( $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Added","msg" => "Tool saved successfully.", "success" => "1");
		elseif($retVal == -1)
			$data = array("title"=>"Warning","msg" => "This Tool already exist", "success" => "0");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while adding Tool data.', "success" => false);
		
		$this->response($data);
	}

	function getTechnologies_get()
	{
		$data = $this->Skills_Model->getTechnologies();
		return $this->response($data);
	}

	function technology_add_update_post() 
	{
		$data = $this->post();
		$retVal = $this->Skills_Model->AddUpdateTechnology( $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Added","msg" => "Technology saved successfully.", "success" => "1");
		elseif($retVal == -1)
			$data = array("title"=>"Warning","msg" => "This Technology already exist", "success" => "0");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while adding Technology data.', "success" => false);
		
		$this->response($data);
	}

	function getLanguages_get()
	{
		$data = $this->Skills_Model->getLanguages();
		return $this->response($data);
	}

	function getCertifications_get()
	{
		$data = $this->Skills_Model->getCertifications();
		return $this->response($data);
	}

	function downloadEmployeeSkillsCsv_get()
	{
		$path = FCPATH."download/";
		$filename = "EmployeeSkills.csv";
		$retVal = $this->Skills_Model->getEmployees();

		if($retVal ==0)
			$this->response(array("success" => true, "data" => 0));

		$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0; 

		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true, "data" => 1));
	}

	function getSkillToolsTech_get()
	{
		$data = $this->Skills_Model->getSkillToolsTech();
		echo json_encode($data);
	}

}
?>

