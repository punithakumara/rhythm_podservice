<?php
ini_set('upload_max_filesize', '20M'); 
//ini_set('error_reporting', '0');
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 0);
ini_set('html_errors', 0);

class Documents extends INET_Controller {
	function __construct() {
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Document_Model');
		$this->ci =& get_instance();
	}
	
	function Document_view_get()
	{
		$data = $this->Document_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	
	private function _addSerialNos($data,$start)
	{
		$l = ($start==0)?1:$start+1;
		$tempArray = $data;
		$i=1;
		foreach($data as $k=>$v)
		{				
			$j=$i-1;
			$tempArray[$j]['SlNo']=$l;
			$i++;
			$l++;
		}			
		//echo "<pre>".print_r($tempArray);			
		return $tempArray;
	}
	
	
	// for saving
	function Documents_save_post() 
	{
		$data = $this->post();
		//echo "<pre>"; print_r($_FILES); exit;
		
		$filesize = floor($_FILES["NewDocument"]["size"]);
		$access = isset($data['Access']) ? $data['Access'] : '';
		
		$target_file ='';
		//print_r($filesize); exit;
		if(isset($_FILES['NewDocument']))
		{
			if($filesize<=FILE_SIZE_LIMIT) 
			{
				$file = $_FILES['NewDocument']['name'];						
				if($access == "All" || $access == "All managers")
				{
					$target_file = $this->Document_Model->comDocCreate(basename($_FILES['NewDocument']['name']));			
				}
				else
				{				
					$target_file = $this->Document_Model->verDocCreate($data['VerticalID'],basename($_FILES['NewDocument']['name']));
				}
			}					
		}
		if($filesize<=FILE_SIZE_LIMIT) 
		{			
			$retVal = $this->Document_Model->addDocs( $data );
		}
		else 
		{
			$retVal = 1;
		}			
		if($retVal > 0) 
		{

			if($filesize>FILE_SIZE_LIMIT) 
			{
				$data = array("msg" => "Document Size must be less than or equals to 20 MB.", "success" => "true", "filesize" => "max");
				$this->response($data);
			}
			else 
			{
				$data = array("msg" => "Document uploaded successfully.", "success" => "true");
				$this->response($data);
			}
		}
		else {
			$data = array("msg" => 'Error occured while Processing data.', "success" => "false");
			$this->response($data);
		}
	}
	
	// for deleting
	function Documents_dele_post() 
	{
		$data = $this->post();	
		
		$retVal = $this->Document_Model->deleDocs( $data );
	
		if($retVal > 0) {
			$data = array("msg" => "Documents Deleted successfully.", "success" => "true");
			$this->response($data);
		}
		else {
			$data = array("msg" => 'Error occured while Processing data.', "success" => "false");
			$this->response($data);
		}
	}
	
	
	function document_access_get() 
	{
		if($this->input->cookie() != null)
		{
			// Load the configuration
			$this->ci->load->config('documents-config');
			
			/* $ipaddress = file_get_contents('http://checkip.amazonaws.com/');
			if($this->ci->config->item('internal_ip') == $ipaddress)
			{ */
				$attachment_location = "";
				if($_GET['type']=="company")
				{
					$attachment_location .= $this->ci->config->item('company_documents_path');
				}
				else
				{
					$attachment_location .= $this->ci->config->item('vertical_documents_path');
					$attachment_location .= "/".$_GET["folder"];
				}
				
				if($_GET['filename']!="")
				{
					$attachment_location .= "/".$_GET['filename'];
				}
				
				if (file_exists($attachment_location)) 
				{
					$ext = pathinfo($_GET['filename'], PATHINFO_EXTENSION);
					$contentType = $this->getMimeType($ext);
					
					header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
					header("Cache-Control: public"); // needed for internet explorer
					header("Content-Type: $contentType");
					header("Content-Length:".filesize($attachment_location));
					header("Content-Disposition: inline; filename=".$_GET['filename']);
					readfile($attachment_location);
					die();        
				} 
				else 
				{
					die("Error: File not found.");
				}
			/* }
			else
			{
				die("Error: Access denied from outside");
			} */
		}
		else
		{
			die("Access denied");
		}
	}
	
	
	function document_download_get() 
	{
		if($this->input->cookie() != null)
		{
			// Load the configuration
			$this->ci->load->config('documents-config');
			
			/* $ipaddress = file_get_contents('http://checkip.amazonaws.com/');
			if($this->ci->config->item('internal_ip') == $ipaddress)
			{ */
				$attachment_location = "";
				if($_GET['type']=="company")
				{
					$attachment_location .= $this->ci->config->item('company_templates_path');
				}
				else
				{
					$attachment_location .= $this->ci->config->item('vertical_templates_path');
				}
				
				if($_GET['filename']!="")
				{
					$attachment_location .= $_GET['filename'];
				}
				
				if (file_exists($attachment_location)) 
				{
					$ext = pathinfo($_GET['filename'], PATHINFO_EXTENSION);
					$contentType = $this->getMimeType($ext);
					ob_end_clean();
					header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
					header("Cache-Control: public"); // needed for internet explorer
					header("Content-Type: $contentType");
					header("Content-Length:".filesize($attachment_location));					
					header("Content-Disposition: attachments; filename=".$_GET['filename']);
					//$cnt = ob_get_length();					
					ob_end_clean();
					readfile($attachment_location);					
					die();        
				} 
				else 
				{
					die("Error: File not found.");
				}
			/* }
			else
			{
				die("Error: Access denied from outside");
			} */
		}
		else
		{
			die("Access denied");
		}
	}
	
	private function getMimeType($extension)
	{
		$mimeType = "";
		switch ($extension) 
		{
			case 'pdf':
				$mimeType = "application/pdf";
				break;
			case 'doc':
				$mimeType = "application/msword";
				break;
			case 'docx':
				$mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
				break;
			case 'gif':
				$mimeType = "image/gif";
				break;
			case 'jpeg':
				$mimeType = "image/jpeg";
				break;
			case 'jpg':
				$mimeType = "image/jpg";
				break;
			case 'png':
				$mimeType = "image/png";
				break;
			case 'ppt':
				$mimeType = "application/vnd.ms-powerpoint";
				break;
			case 'pptx':
				$mimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
				break;
			case 'xls':
				$mimeType = "application/vnd.ms-excel";
				break;
			case 'xlsx':
				$mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				break;
			case 'xml':
				$mimeType = "application/xml";
				break;
			case 'csv':
				$mimeType = "text/csv";
				break;
		}
		
		return $mimeType;
	}
	
}
?>