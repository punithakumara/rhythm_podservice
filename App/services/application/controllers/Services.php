<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class Services extends INET_Controller
{
	public $EmpId = NULL;
	public $verticals = NULL;
	public $Grade = NULL;
	var $tempArray = array();
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model(array('Services_Model','Rhythm_Model'));	
		
		$empData = $this->input->cookie();
		
		if (isset($empData['EmployID']) != "")
		{
			$this->EmpId = $empData['EmployID'];
			$this->Grade = $empData['Grade'];
			if(isset($empData['VertIDs']))
			{
				if($empData['Grade'] <6)
				{
					$this->verticals = $empData['VertIDs'];
				}
			}
		}

		$this->config->load('admin_config');
		$this->super_admin = $this->config->item('super_admin');
	}
	
	function Services_view_get()
	{
		$data = $this->Services_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	function GroupVerticalCombo_get()
	{
		$data = $this->Vertical_Model->RevisedGroupVerticalComboValues();
		$this->response($data);
	}		
	
	function vertical_my_get()
	{
		$data = array(array("key"=>"Karthik","name"=>"Karthik"),array("key"=>"Jana","name"=>"Jana"));			
		
		$this->response($data);
	}
	
	function services_del_delete($idVal = '')
	{
		$retVal = $this->Services_Model->deleteServices($idVal);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Services deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
	
	function Services_update_put( $idVal = '' ) 
	{
		$data = $this->put('services');
		$retVal = $this->Services_Model->updateServices( $idVal, $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Updated","msg" => "Services updated successfully.", "success" => "true");
		else if($retVal == -1)
			$data = array("title"=>"Existing","msg" => "Services Name already exists.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while updating Services data.', "success" => "false");
		$this->response($data);
	}
	
	
	function services_add_post() 
	{
		$data = $this->post('services');
		
		$retVal = $this->Services_Model->addServices( $data );
			
		if($retVal > 0) 
		{
			$data = array("title"=>"Added","msg" => "Services added successfully.", "success" => "true");
			$this->response($data);
		}
		else if($retVal == -1)
		{
			$data = array("title"=>"Existing","msg" => "Services Name already exists.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("title"=>"Error","msg" => 'Error occured while adding Services data.', "success" => "false");
			$this->response($data);
		}
	}
	
	function parentVertical_get() 
	{
		$retVal = $this->Vertical_Model->getParentVertical();
			
		if($retVal['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		$this->response($retVal,$httpCode);
	}
	
	function getParentVertical_get()
	{
		$sql = 	"SELECT v.vertical_id as Id,v.Name AS pVerticalName FROM verticals v WHERE v.parent_vertical_id = 0 ORDER BY v.Name ASC";
		$query = $this->db->query($sql);        
		$data = $query->result_array();
		$data = array_merge(array(
			'totalCount' => $query->num_rows()
		), array(
			'rows' => $data
		));
	   
		$this->response( $data);
		
	}

	private function _hierarchverticaalCount($vID)
	{
		$query = $this->db->select("count(vertical_id) as TotalCount")->from("vertical")->where("parent_vertical_id", $vID);
		$query = $this->db->get();
		if ($query->num_rows() == 0)
			$empCount = 0;
		else
			$empCount = $query->row('TotalCount');
			
		return ($empCount != 0) ? " (" . $empCount . ")" : '';
	}


	function getVerticalTreeStructureData_get()
	{
		$node = $this->input->get();
		$root = $this->input->get('node');   
		$res  = $this->Vertical_Model->repoteesverticalHierarchyGrid($this->Grade);
		if(isset($res)) 
		{
			foreach($res as $key => $val) 
			{
				$res[$key]['icon'] = '';//"resources/images/tree/DashboardSubVertical.png";
				$res[$key]['leaf'] = 'false';
				$res[$key]['id'] = $res[$key]['ParentVerticalID'].'-'.'vertRoot';
			}
		}

		if($root == 'root') 
		{
			echo json_encode($res);
		} 
		else 
		{
			$verticalDet = explode('-', $root);          		         	
			// $hrpool = $this->Rhythm_Model->getHRPoolNonBillableEmployees();  
			
			if($verticalDet[1] != 'subVert')
			{
				$res  = $this->Vertical_Model->getsubverticalsNames($verticalDet[0]);
				$res[2]['VerticalID'] = 5;
				$res[2]['Name'] = 'Onboarding';
				
				$res[3]['VerticalID'] = 10;
				$res[3]['Name'] = 'HR Pool';
				
				foreach($res as $key => $val) 
				{
					if($res[$key]['Name'] == 'Operations')
						$icon = "resources/images/tree/operations.png";
					else 
						$icon = "resources/images/tree/support.png";
					
					
					if(($res[$key]['Name'] == 'Operations') || ($res[$key]['Name'] == 'Support'))
					{
						$sql = $this->Rhythm_Model->getBillableCountQuery($Verticals = '',$processIds = '',$subVertical = $res[$key]['VerticalID']);
						$query = $this->db->query($sql);
						$billabledata = count($query->result_array());
						$sqlnonbill = $this->Rhythm_Model->getNonBillableCountQuery($Verticals = '',$processIds = '',$subVertical = $res[$key]['VerticalID']);
						$querynonbill = $this->db->query($sqlnonbill);
						$nonbillabledata = count($querynonbill->result_array());
						$res[$key]['Billable'] = $billabledata;
						$res[$key]['NonBillable'] = $nonbillabledata;
						$res[$key]['Leadership'] = $this->Rhythm_Model->getLeadverticalcount($Verticals = '',$processIds = '',$subVertical= $res[$key]['VerticalID']);
						$res[$key]['iconCls'] = '';//$icon;
						$res[$key]['leaf'] = 'false';
						$res[$key]['id'] = $res[$key]['VerticalID'].'-'.'subVert';
					}
					
					if(($res[$key]['Name'] == 'HR Pool'))
					{
						$res[$key]['Billable'] = $hrpool[0]['Value1'];
						$res[$key]['NonBillable'] = $hrpool[0]['Value2'];
						$res[$key]['Leadership'] = $hrpool[0]['Value3'];
						$res[$key]['iconCls'] = '';
						$res[$key]['leaf'] = 'false';
						$res[$key]['id'] = $res[$key]['VerticalID'].'-'.'subVert';
					}
					
					if(($res[$key]['Name'] == 'Onboarding'))
					{
						$res[$key]['Billable'] = $hrpool[1]['Value1'];
						$res[$key]['NonBillable'] = $hrpool[1]['Value2'];
						$res[$key]['Leadership'] = $hrpool[1]['Value3'];
						$res[$key]['iconCls'] = '';
						$res[$key]['leaf'] = 'false';
						$res[$key]['id'] = $res[$key]['VerticalID'].'-'.'subVert';
					}
				}
				echo json_encode($res);
			}
			else
			{
				$res  = $this->Vertical_Model->getVerticalsNames($verticalDet[0],$this->verticals);
				foreach($res as $key => $val) 
				{
					$sql = $this->Rhythm_Model->getBillableCountQuery($Verticals = $res[$key]['vertical_id'],$processIds = '',$subVertical = '');       
					$query = $this->db->query($sql);
					$billabledata = count($query->result_array());
					
					$sqlnonbill = $this->Rhythm_Model->getNonBillableCountQuery($Verticals = $res[$key]['vertical_id'],$processIds = '',$subVertical = '');       
					$querynonbill = $this->db->query($sqlnonbill);
					$nonbillabledata = count($querynonbill->result_array());
					
					$getverticalHead = $this->Vertical_Model->getverticalHead($res[$key]['vertical_id']);
					$employID = explode("==",$getverticalHead);												
					$sqlleadership = $this->Rhythm_Model->getLeadverticalcount($Verticals = $res[$key]['vertical_id'],$processIds = '',$subVertical = '');					
					
					$res[$key]['Billable'] = $billabledata;
					$res[$key]['NonBillable'] = $nonbillabledata;
					$res[$key]['Leadership'] = $sqlleadership;//$leadership;
					$res[$key]['iconCls'] = '';//"resources/images/tree/DashboardVertical.png";
					$res[$key]['leaf'] = 'true';
					$res[$key]['id'] = $res[$key]['vertical_id'].'-'.'verTicals';
					$res[$key]['Employid'] = $employID[1];
					$res[$key]['tool_tip'] = $employID[0];
					$res[$key]['VertID'] = $res[$key]['vertical_id'];
				}
				$i = 0;
				
				echo json_encode($res);
			}
		}
	}
	
	function getemployVerticals($sqlleadership, $employArray = array())
	{
		$querylead = $this->db->query($sqlleadership);
		$employList = array();
		if(count($querylead->result_array()) > 0)
		{
			foreach($querylead->result_array() as $key=> $value){
				array_push($employList, $value['empid']);
			}
				$employArray = array_merge($employArray, $employList);
		}
		return $employArray;
	}
	
	//get all verticals irrespective of user login
	function getAllVerticals_get()
	{
		$data = $this->Vertical_Model->AllGroupVerticalComboValues();
		$this->response($data);
	}
	
	/**
	* Get Client Contact Vertical
	*/	
	public function getClientContactServices_get() 
	{
		$filterQuery	= $this->input->get('query');
		$filterName     = $this->input->get('filterName') ? $this->input->get('filterName') : "";
		$sql			= "SELECT service_id, name AS ServiceName FROM tbl_rhythm_services WHERE 1 = 1";
		if ($filterName != "") 
		{
			if($filterName == 'ServiceName') 
			{
				$filterName = 'Name';
			}
			$filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
			$sql .= " AND " . $filterWhere;
		}	
		
		$res = $this->db->query($sql);

		if(count($res->result_array()) > 0) 
		{
			$results = array_merge(array('totalCount' => count($res->result_array())), array('rows'=> $res->result_array()));
			$this->response($results);
		}
	}
}
?>