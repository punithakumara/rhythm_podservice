<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Clientwor extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model
		$this->load->model('Clientwor_Model');
	}
	
	
	/**
	* Fetch Client WOR
	*
	* @param 
	* @return JSON
	*/
	function list_get()
	{			
		$data = $this->Clientwor_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Fetch roles list
	*
	* @param 
	* @return JSON
	*/
	function role_list_get()
	{			
		$data = $this->Clientwor_Model->roles();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Fetch project list
	*
	* @param 
	* @return JSON
	*/
	function worProject_list_get()
	{			
		$data = $this->Clientwor_Model->projects();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}

	/**
	* Fetch job responsibilities list
	*
	* @param 
	* @return JSON
	*/
	function job_role_list_get()
	{
		$wor_id = $this->input->get('wor_id');		
		$data = $this->Clientwor_Model->get_role_types($wor_id);
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch Client WOR
	 *
	 * @param
	 * @return JSON
	 */
	function wor_add_post()
	{
		$data = $this->post('clientwor');
				
		$fte_details = $this->post('fte_details');
		$hourly_details = $this->post('hourly_details');
		$volume_list = $this->post('volume_list');
		$project_list = $this->post('project_list');
		$client_name = $this->post('client_name');
		
		$return_value = $this->Clientwor_Model->add_wor($data, $fte_details, $hourly_details, $volume_list, $project_list,$client_name);
	
		if($return_value == 1)
		{					
			$data = array("msg" => "Client Work Order Draft Saved Successfully.", "success" => "true", "title" => "Added");
			$httpCode = 200;
		}
		else if ($return_value == 2)
		{
			$data = array("msg" => "Client Work Order Submitted Successfully.", "success" => "true", "title" => "Submit");
			$httpCode = 200;
		}
		else if($return_value == 3)
		{
			
			$data = array("msg" => "Client Work Order Draft Updated Successfully.", "success" => "true", "title" => "Updated");			
			$httpCode = 200;
		}
		else if ($return_value == 4)
		{
			$data = array("msg" => "Client Work Order Submitted Successfully.", "success" => "true", "title" => "Submit");
			$httpCode = 200;
		}
		else if ($return_value == 5)
		{
			$data = array("msg" => "Client Work Order Deleted Successfully.", "success" => "true", "title" => "Delete");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch Client WOR
	 *
	 * @param
	 * @return JSON
	 */
	function wor_add_put()
	{
		$data = $this->post('clientwor');
		$return_value = 3;
							
		if($return_value == 1)
		{					
			$data = array("msg" => "Client Work Order Draft Saved Successfully.", "success" => "true", "title" => "Added");
			$httpCode = 200;
		}
		else if ($return_value == 2)
		{
			$data = array("msg" => "Client Work Order Submitted Successfully.", "success" => "true", "title" => "Submit");
			$httpCode = 200;
		}
		else if($return_value == 3)
		{
			
			$data = array("msg" => "Client Work Order Updated Successfully.", "success" => "true", "title" => "Updated");			
			$httpCode = 200;
		}
		else if ($return_value == 4)
		{
			$data = array("msg" => "Client Work Order Submitted Successfully.", "success" => "true", "title" => "Submit");
			$httpCode = 200;
		}
		else if ($return_value == 5)
		{
			$data = array("msg" => "Client Work Order Deleted Successfully.", "success" => "true", "title" => "Delete");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	
	
	/**
	 * Fetch FTE list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function fte_list_get()
	{
		$data = $this->Clientwor_Model->get_fte_list();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch Hourly list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function hourly_list_get()
	{
		$data = $this->Clientwor_Model->get_hourly_list();
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
	
		$this->response($data,$httpCode);
	}
	
	
	/**
	 * Fetch volume list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function volume_list_get()
	{
		$data = $this->Clientwor_Model->get_volume_list();
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch project list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function project_list_get()
	{
		$data = $this->Clientwor_Model->get_project_list();
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Save status of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function saveStatus_post()
	{
		$data = $this->post();
	
		$return_value = $this->Clientwor_Model->update_status($data);
	
		if($return_value!="")
		{
			$data = array("msg" => "Client WOR closed successfully.", "success" => "true", "title" => "closed");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Save extension of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function saveExtension_post()
	{
		$data = $this->post();
	
		$return_value = $this->Clientwor_Model->update_extension($data);
	
		if($return_value!="")
		{
			$data = array("msg" => "Client WOR Extended Successfully.", "success" => "true", "title" => "Success");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * get WOR list to display in combo
	 *
	 * @param
	 * @return JSON
	 */
	function wor_list_get()
	{
		$client_id = $this->get('comboClient');
		$data = $this->Clientwor_Model->get_wor_list($client_id);
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	 * get Summary list to display in Tabpanel
	 *
	 * @param
	 * @return JSON
	 */
	
	function summary_list_get()
	{
		$data = $this->Clientwor_Model->get_summary_list();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	* Get Allocated Resources of work order line item
	*
	* @param 
	* @return JSON
	*/
	function allocated_resources_get()
	{			
		$client_id = $this->input->get('client_id');
		$project_id = $this->input->get('project_id');
		$wor_id = $this->input->get('wor_id');
		$data = $this->Clientwor_Model->get_allocated_resource($client_id, $project_id, $wor_id);
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}		
		$this->response($data,$httpCode);
	}

	/**
	* Get Allocated Resources of work order line item
	*
	* @param 
	* @return JSON
	*/
	function allocated_res_get()
	{			
		$client_id = $this->input->get('client_id');
		$wor_id = $this->input->get('wor_id');
		$project_id = $this->input->get('project_type_id');
		$data = $this->Clientwor_Model->get_allocated_res($client_id, $wor_id,$project_id);
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}		
		$this->response($data,$httpCode);
	}
	
	/**
	* Remove resources from work order line item
	*
	* @param 
	* @return JSON
	*/
	function remove_resources_post()
	{			
		$inputValues = $this->post();		
		$retVal = $this->Clientwor_Model->removeResources($inputValues);
		
		if($retVal==1)
		{
			$data = array("title"=>'Remove Resources', "msg" => "Resources Removed Successfully", "success" => true);		
		}
		else
		{
			$data = array("msg" => "Data Imported Error.", "success" => false);
		}
		$this->response($data);
	}
	
	/**
	* Remove client holidays from work order line item
	*
	* @param 
	* @return JSON
	*/
	function remove_client_holidays_post()
	{			
		$inputValues = $this->post();			
		$retVal = $this->Clientwor_Model->delClientHolidays($inputValues);
		
		if($retVal==1)
		{
			$data = array("title"=>'Remove Holidays', "msg" => "Holiday Removed Successfully", "success" => true);		
		}
		else
		{
			$data = array("msg" => "Data Imported Error.", "success" => false);
		}
		$this->response($data);
	}
	
	/**
	* Mapping project code with holidays
	*
	* @param 
	* @return JSON
	*/
	function client_holidays_post()
	{			
		$inputValues = $this->post();			
		$retVal = $this->Clientwor_Model->clientHolidays($inputValues);
		
		if($retVal==1)
		{
			$data = array("msg" => "Holidays added Successfully", "success" => true);		
		}
		else
		{
			$data = array("msg" => "Data Imported Error.", "success" => false);
		}
		$this->response($data);
	}
	
	/**
	 * Get holidays list based on clients
	 *
	 * @param
	 * @return JSON
	 */
	
	function client_holidays_get()
	{
		$data = $this->Clientwor_Model->get_holidays_list();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	
	/**
	* Function for updating CP, EM, DM, AM, TL & SH
	*/
	function worEmpsHistory_post()
	{
		$data = $this->post();
	
		$return_value = $this->Clientwor_Model->update_worEmpsHistory($data);
	
		if($return_value!="")
		{
			$data = array("msg" => "Reocrd(s) Updated Successfully.", "success" => "true", "title" => "Success");
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	* Function for get Roles based on work order and project
	*/
	function wor_project_roles_post()
	{
		$project_id = $this->input->post('project_id');
		$wor_id = $this->input->post('wor_id');
	
		$sql = "SELECT GROUP_CONCAT(DISTINCT role_type) AS role_type FROM role_types 
				WHERE role_type_id IN (SELECT responsibilities FROM fte_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")
				UNION
				SELECT responsibilities FROM hourly_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")
				UNION
				SELECT responsibilities FROM volume_based_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")
				UNION
				SELECT responsibilities FROM project_model WHERE fk_wor_id ='".$wor_id."' AND fk_project_type_id IN(".$project_id.")) AND status=1";
		$query = $this->db->query($sql);
		$data = array_merge(
			array('totalCount' => $query->num_rows()),
			array('rows' => $query->row_array())
		);
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}

	function wor_project_roles_get()
	{
		$data = $this->Clientwor_Model->wor_project_roles();
		
		$this->response($data);
	}

	function supervisor_get()
	{
		$data = $this->Clientwor_Model->getSupervisor();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}

	function models_get()
	{
		$data = $this->Clientwor_Model->getModels();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		$this->response($data,$httpCode);
	}
	
}

?>