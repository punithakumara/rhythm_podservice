<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WorTypes extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Wor_Type_Model');
	}

	// list view
	function wor_types_get()
	{
		if($this->input->get('comboClient'))
		{
			$client_id = $this->input->get('comboClient');
			$data = $this->Wor_Type_Model->getClientWor($client_id);
		}
		else
		{
			$data = $this->Wor_Type_Model->get_worTypes();	
		}
		return $this->response($data);
	}

}
?>

