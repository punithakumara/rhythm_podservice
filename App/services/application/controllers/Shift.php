<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shift extends INET_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Shift_Model');
	}

	function shifts_list_get(){
		
	    $data = $this->Shift_Model->getShiftData();
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
			
		$this->response($data,$httpCode);
	}
}

