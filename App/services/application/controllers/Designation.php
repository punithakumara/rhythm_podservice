<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designation extends INET_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Designation_Model');
	}

	function grades_list_get(){
		
		$data = $this->Designation_Model->getGradeData();
			
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
			
		$this->response($data,$httpCode);
	}

	function designations_list_get(){
		
		$data = $this->Designation_Model->getDesignationData();
			
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
			
		$this->response($data,$httpCode);
	}
	
	function designations_add_post() {
		
		$data = $this->post('data');
		
		$retVal = $this->Designation_Model->addDesignation( $data );
			
		if($retVal == 1) {
			$data = array("msg" => "Designation added successfully.", "success" => "true", "status" => "1");
			$this->response($data);
		}
		else if($retVal == 3) { //condition for duplicates			
			$data = array("msg" => "Designation already Exists.", "success" => "true", "status" => "3");
			$this->response($data);
		}
		else {
			$data = array("msg" => 'Error occured while adding Designation data.', "success" => "false", "status" => "0");
			$this->response($data);
		}
	}
	
	function designations_update_put( $idVal = '' ) {
		
		$data = $this->put('data');
		
		$retVal = $this->Designation_Model->updateDesignation( $idVal, $data );
		
		if($retVal == 1) {
			$data = array("msg" => "Records updated successfully.", "success" => "true", "status" => "1");
			$this->response($data);
		}
		else if($retVal == 3) {	//condition for duplicates			
			$data = array("msg" => "Designation already Exists.", "success" => "true", "status" => "3");
			$this->response($data);
		}
		else {
			$data = array("msg" => 'Error occured while updating Designation data.', "success" => "false", "status" => "0");
			$this->response($data);
		}
	}
	
	function designations_del_delete($idVal = '') {
		
		$retVal = $this->Designation_Model->deleteDesignation($idVal);
		
		if($retVal > 0) {
			$data = array("msg" => "Designation deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else {
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
}

