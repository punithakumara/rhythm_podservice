<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonthlyBillabilityReport extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Monthly_Billabliity_Model');
	}

	function MonthlyBillabilityReport_view_get()
	{
		$data = $this->Monthly_Billabliity_Model->getHourlyVolumeReport();

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}

		$this->response($data,$httpCode);

	}

	function DownloadMonthlyBillabilityReportCsv_get()
	{
		$path = FCPATH."download/";
		$model = $this->input->get('model');
		if($model == 'hourly_volume')
		{
			$filename = "MonthlyBillabilityHourlyVolumeReport.csv";
			$retVal = $this->Monthly_Billabliity_Model->getHourlyVolumeReport();	
		}
		elseif($model == 'fte')
		{
			$filename = "MonthlyBillabilityFteReport.csv";
			// $retVal = $this->Monthly_Billabliity_Model->Fteview();
			$retVal = $this->Monthly_Billabliity_Model->getFteReportcsv();
		}
		elseif ($model == 'project') 
		{
			$filename = "MonthlyBillabilityProjectReport.csv";
			$retVal = $this->Monthly_Billabliity_Model->getProjectReport();
		}

		// debug($retVal);exit;
		$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0; 

		// debug($retVal);exit;

		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
	}

	function MonthlyBillabilityFteReport_view_get(){

		$data = $this->Monthly_Billabliity_Model->Fteview();
		// $data = $this->Monthly_Billabliity_Model->getFteReport();

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}

		$this->response($data,$httpCode);

	}

	function MonthlyBillabilityFteRemarks_view_get(){

		$mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');

		$resp_id		= $this->input->get('responsibility_id')?$this->input->get('responsibility_id'):"";
		$fte_ids		= $this->input->get('fte_id')?$this->input->get('fte_id'):"";
		$type = "list";
		$data = $this->Monthly_Billabliity_Model->GetFteRemarks($mbrmonth,$resp_id,$fte_ids,$type);

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}

		$this->response($data,$httpCode);

	}
	function MonthlyBillabilityReport_save_post(){
		$data = $this->post('MonthlyBillabilityReport');
		$retVal = $this->Monthly_Billabliity_Model->addmonthly_billability( $data );
		
		if($retVal > 0) {
			$data = array("msg" => "Billable For The Month Addedd Successfully.", "success" => "true");
			$this->response($data);
		}
		else {
			$data = array("msg" => 'Error occured while Processing data.', "success" => "false");
			$this->response($data);
		}
	}
	function MonthlyChangeReport_view_get(){

		$data = $this->Monthly_Billabliity_Model->preMonthlyRepview();

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}

		$this->response($data,$httpCode);

	}

	function MonthlyBillabilityProjectReport_view_get()
	{
		$data = $this->Monthly_Billabliity_Model->getProjectReport();

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}

		$this->response($data,$httpCode);
	}

	function DownloadReportCsv_get(){

		$path = FCPATH."download/";

		$filename = "MonthlyBillReports.csv";

		$mbrmonth = ($this->input->get('mbrmonth')) ? $this->input->get('mbrmonth') : date('Y-m-01');$time=strtotime($mbrmonth);
		$model = ($this->input->get('model'));
		$time=strtotime($mbrmonth);
		$month=date("M",$time);
		$year=date("Y",$time);	

		/* Last 3 months date*/
	    	/*$last3Months = date('Y-m-d', strtotime('-3 month'));
	        $st_date = date('Y-m-01', strtotime($last3Months));
	        $st_date = date('jS M Y', strtotime($st_date));
	        $end_date = date('jS M Y', strtotime($endParam));*/
	        /* Last 3 months date*/

	        if($model == "fte"){
	        	$retValsplit = $this->Monthly_Billabliity_Model->Fteview();
	        }
	        else{
	        	$retValsplit = $this->Monthly_Billabliity_Model->view();
	        }

	        $retVal		 = $retValsplit[0];		

	        $Header = 'Monthly Billability Report For the '.$month." - ".$year.'';
	        array_unshift($retVal, array('','','',$Header));
	        ob_start();
	        $f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
	        $n = 0;    
			// $fields = array('Total', '', '');  


	        foreach ($retVal as $line)
	        {			
	        	$n++;
	        	if ( ! fputcsv($f, $line))
	        	{
	        		show_error("Can't write line $n: $line");
	        	}
	        }
	        fputcsv($f,array('', '', '','','', '', '','', ''));
			// fputcsv($f, $fields);
	        fputcsv($f,array('', '', '','','', '', '','', ''));
	        fputcsv($f,  array('', '', '', "Theorem Inc. Confidential(Not To Be Shared)"));	
	        fclose($f) or show_error("Can't close php://output");
	        $str = ob_get_contents();
	        ob_end_clean();

			/* Send Mail From Cron Every Month End/*
			$sendmail = ($this->input->get('SendMail')) ? $this->input->get('SendMail'):"";
			if($sendmail != ""){
				//$this->sendReport($path,$filename,$st_date,$end_date,$retValsplit);
			}
			/* Send Mail From Cron Every Month End*/			
			$this->response(array("success" => true));
		}
		
		
		function sendReport($path,$filename,$st_date,$end_date,$data){
			require_once('../onboarding/phpmailer/class.phpmailer.php');
			$filenamepath = $path.$filename;
			$fp = fopen($filenamepath, "rb");
			$upload_size=filesize($filenamepath);
			$file = fread($fp, $upload_size);		
			$encoded_content = chunk_split( base64_encode($file));
		   // $to = "bhaskar.kalale@theoreminc.net,sbalaji@theoreminc.net";		    
			$subject 	= 'Monthly Billability Report Between '.$st_date." AND  ".$end_date.'';		 
			$mail = new PHPMailer(true);
			$mail->IsSMTP();                       // telling the class to use SMTP
			$mail->SMTPDebug = 0;                  

			// 0 = no output, 1 = errors and messages, 2 = messages only.
			$mail->SMTPDebug = 1; 
			$mail->SMTPAuth = true;                // enable SMTP authentication 
			$mail->SMTPSecure = "tls";              // sets the prefix to the servier
			$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
			$mail->Port = 587;                    // set the SMTP port for the GMAIL 
			$mail->Username = "rhythmsupport";  // Gmail username
			$mail->Password = "rhy_th499";      // Gmail password
			$mail->ContentType = 'text/html'; 
			$mail->IsHTML(true);
			$mail->CharSet = 'windows-1250';
			$mail->From = 'rhythmsupport@theoreminc.net';
			$mail->FromName = 'Rhythmsupport@theoreminc.net';
			$mail->addReplyTo('Rhythmsupport@theoreminc.net');

			$body = "<table width='600' border='0' cellspacing='0' cellpadding='0' align='left'>

			<tr>
			<td align='left' valign='top' style='padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; height:auto'>
			<span style='font-size:14px; font-weight:normal;'>Hi All,</span>
			<br />
			<br />
			As on Rhythm, Monthly Billability Report is attached for  ".date('M Y')."

			<br />
			<br />
			Below is Monthly Billability Report abstract for the same.
			<br />
			<br />
			<table  style='position:absolute;top:37px;border:1px solid #333333'   cellspacing= '0' cellpadding= '0' class= 'Grid'>
			<thead>
			<tr style='background-color: #7FBF3F;'>

			<th style ='font-weight: normal;border-bottom:1px solid #333333; border-right:1px solid #333333; padding:5px; color:black;'>Total Approved FTE <br/>(Beginning of the month)</th>
			<th style ='font-weight: normal;border-bottom:1px solid #333333; border-right:1px solid #333333; padding:5px; color:black;'>Addition</th>
			<th style ='font-weight: normal;border-bottom:1px solid #333333; border-right:1px solid #333333; padding:5px; color:black;'>Deletion</th>
			<th style ='font-weight: normal;border-bottom:1px solid #333333; border-right:1px solid #333333; padding:5px; color:black;'>Total Approved FTE <br/>(End of the month)</th>
			<th style ='font-weight: normal;border-bottom:1px solid #333333;  padding:5px; color:black;'>Total Billable Count <br/></th>										
			</tr>
			</thead>
			<tbody  id='tbodyid'>
			<tr><td align= 'center' style= 'font-weight: bold; border-right:1px solid #333333;padding:5px; color:#333333'>$data[1]</td>
			<td align= 'center' style= 'font-weight: bold;border-right:1px solid #333333;  padding:5px; color:#333333'>$data[3]</td>
			<td align= 'center' style= 'font-weight: bold;border-right:1px solid #333333;  padding:5px; color:#333333'>$data[4]</td>
			<td align= 'center' style= 'font-weight: bold;border-right:1px solid #333333;  padding:5px; color:#333333'>$data[2]</td>
			<td align= 'center' style= 'font-weight: bold;  padding:5px; color:#333333'>$data[5]</td>
			</tr>

			</tbody>
			</table>
			<br /><br />
			Regards<br />
			Rhythm Support Team</td>
			</tr>
			<tr>
			<td align='left' valign='top'>&nbsp;</td>
			</tr>
			</table>";
			$mail->Subject = $subject;
			$mail->Body = $body;			
			$mail->addStringAttachment($file, $filename, 'base64', 'text/csv');
			$addresses = explode(',',$to);
			foreach ( $addresses as $address ){
				$mail->AddAddress($address);
			}
			$mail->Send();  
			//mail($to,$subject,$body,implode("\r\n", $headers));			
		}			
	}
	?>