<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Holidays extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Holiday_Model');
	}
	
	
	function holiday_view_get()
	{
		$data = $this->Holiday_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}	
	
	
	function holiday_update_post( $idVal = '' ) 
	{
		$data = $this->post();
		$retVal = $this->Holiday_Model->updateHoliday( $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Updated","msg" => "Holiday updated successfully.", "success" => "true");
		else if($retVal == -1)	
			$data = array("title"=>"Existing","msg" => "Holiday Name already exists in this Vertical.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while updating Holiday data.', "success" => "false");
		$this->response($data);

	}
	
	
	function holiday_add_post() 
	{
		$data = $this->post();
		$retVal = $this->Holiday_Model->addHoliday( $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Added","msg" => "Holiday added successfully.", "success" => "true");
		else if($retVal == -1)	
			$data = array("title"=>"Existing","msg" => "Holiday Name already exists.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while adding holiday data.', "success" => "false");
		
		$this->response($data);
	}
	
	
	function holiday_del_delete($idVal = '')
	{
		$data = $this->delete('holiday');
		$delData = json_decode(trim($data), true);
		
		$retVal = $this->Holiday_Model->deleteHoliday($delData['holiday_id']);
		
		if($retVal > 0)
		{
			$data = array("msg" => "Holiday deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => "Holiday cannot be deleted.", "success" => "false");
			$this->response($data,400);
		}
	}
	
}
?>