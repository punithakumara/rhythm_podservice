<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProjectTypes extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Project_Type_Model');
	}

	// list view
	function project_types_get()
	{
		if($this->input->get('comboClient'))
		{
			$client_id = $this->input->get('comboClient');
			$data = $this->Project_Type_Model->getClientWorProjectTypes($client_id);
		}
		else
		{	
			$data = $this->Project_Type_Model->get_projectTypes();	
		}
		
		return $this->response($data);
	}

}
?>