<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TimesheetOnshore extends INET_Controller
{	
	function __construct()
	{
		parent::__construct();
		$this->load->model('TimesheetOnshore_Model');
		$this->config->load('admin_config');
	}
	
	function timesheet_view_get()
	{
		$params = $this->get();
		$data = $this->TimesheetOnshore_Model->view($params);
		if(!$data)
			$data = array("msg" => "Something went wrong.", "success" => false);
		
		$this->response($data);
	}
	
	function timesheet_approve_get()
	{
		$params = $this->get();
		//print_r($params);
		//exit;
		$data = $this->TimesheetOnshore_Model->approve($params);
		if(!$data)
			$data = array("msg" => "Something went wrong.", "success" => false);
		
		$this->response($data);
	}
	
	function timesheet_range_get()
	{
		$data = $this->TimesheetOnshore_Model->get_week_range();
		if(!$data)
			$data = array("msg" => "Something went wrong.", "success" => false);
		
		$this->response($data);
	}
	
	function timesheet_future_range_get()
	{
		$data = $this->TimesheetOnshore_Model->get_future_week_range();
		if(!$data)
			$data = array("msg" => "Something went wrong.", "success" => false);
		
		$this->response($data);
	}

	function timesheetSave_post()
	{
		$params = $this->post();

		$timesheetID = (isset($params['timesheetID'])) ? $params['timesheetID'] : "";
		$weekcode = $params['weekcode'];
		$task_code=$this->TimesheetOnshore_Model->get_task_code($params['TaskCode']);	
		$servicetype_code=$this->TimesheetOnshore_Model->get_servicetype_code($params['ServiceType']);
		$engagement_code=$this->TimesheetOnshore_Model->get_engagement_code($params['Engagement']);
		$role_code=$this->TimesheetOnshore_Model->get_role_code($params['Role']);
		
		$client_id=$this->TimesheetOnshore_Model->get_client_id($params['ClientName']);
		
		$catClient_id = ($params['ClientName']=='Theorem India Pvt Ltd')?221:0;
		//$categoryId = $this->TimesheetOnshore_Model->get_category_id($task_code,$subtask_id,$catClient_id);
		
		if($timesheetID!=null || $timesheetID!="")
		{
			$res=$this->TimesheetOnshore_Model->update_task_data($timesheetID,$weekcode,$task_code,$servicetype_code,$engagement_code,$role_code,$client_id,$params);
		}
		else
		{
			$res=$this->TimesheetOnshore_Model->insert_task_data($timesheetID,$weekcode,$task_code,$servicetype_code,$engagement_code,$role_code,$client_id,$params);
		}
		if(isset($res))
		{
			$response["message"]="Task details Updated";
			$response["success"]=1;
		}
		$this->response($response);
	}

	function timesheetSaveComments_post()
	{
		$params = $this->post();
		
		$timesheetID = (isset($params['timesheetID'])) ? $params['timesheetID'] : "";
		
		$res = $this->TimesheetOnshore_Model->update_task_comments($timesheetID, $params);
		
		if(isset($res))
		{
			$response["message"] = "Comments updated successfully";
			$response["success"] = 1;
		}
		$this->response($response);
	}

	function timesheetDelete_post()
	{
		$params = $this->post();
		$timesheetID = (isset($params['timesheetID'])) ? $params['timesheetID'] : "";
		$res=$this->TimesheetOnshore_Model->delete_timesheet_task($timesheetID);
		if(isset($res))
		{
			$response["message"]="Task details deleted";
			$response["success"]=1;
		}
		$this->response($response);
	}
	
	function validateDate_post()
	{
		$params = $this->post();
		$weekno = (isset($params['weekno'])) ? $params['weekno'] : "";
		$day = (isset($params['day'])) ? $params['day'] : "";
		
		$sql = "SELECT week_date FROM task_week WHERE week_code_id='".$weekno."'";
		if($day == "day1")
			$sql .= " LIMIT 0,1";
		if($day == "day2")
			$sql .= " LIMIT 1,1";
		if($day == "day3")
			$sql .= " LIMIT 2,1";
		if($day == "day4")
			$sql .= " LIMIT 3,1";
		if($day == "day5")
			$sql .= " LIMIT 4,1";
		if($day == "day6")
			$sql .= " LIMIT 5,1";
		if($day == "day7")
			$sql .= " LIMIT 6,1";
		
		$query = $this->db->query($sql);
		$result = $query->row_array();
		
		if(strtotime($result['week_date'])<=strtotime(date('Y-m-d')))
		{
			$response["success"] = 1;
		}
		else
		{
			$response["success"] = -1;
		}
		
		$this->response($response);
	}

	function test_service_get()
	{
		$response=$this->TimesheetOnshore_Model->get_production_hours();
		$this->response($response);
	}
	
	function task_code_list_get()
	{
		$data = $this->TimesheetOnshore_Model->task_codes();
		if(!$data)
			$data = array("msg" => "No task available", "success" => false);
		$this->response($data);
	}
	
	function sub_task_list_get()
	{
		$data = $this->TimesheetOnshore_Model->sub_task_codes();
		if(!$data)
			$data = array("msg" => "No Subtask available", "success" => false);
		$this->response($data);
	}
	
	function clients_list_get()
	{
		$data = $this->TimesheetOnshore_Model->clients();
		if(!$data)
			$data = array("msg" => "No clients available", "success" => false);
		$this->response($data);
	}
	
	function servicetype_list_get()
	{
		$data = $this->TimesheetOnshore_Model->servicetypes();
		if(!$data)
			$data = array("msg" => "No clients available", "success" => false);
		$this->response($data);
	}
	function engagement_list_get()
	{
		$data = $this->TimesheetOnshore_Model->engagements();
		if(!$data)
			$data = array("msg" => "No clients available", "success" => false);
		$this->response($data);
	}
	function task_list_get()
	{
		$data = $this->TimesheetOnshore_Model->tasks();
		if(!$data)
			$data = array("msg" => "No clients available", "success" => false);
		$this->response($data);
	}
	function role_list_get()
	{
		$data = $this->TimesheetOnshore_Model->roles();
		if(!$data)
			$data = array("msg" => "No clients available", "success" => false);
		$this->response($data);
	}	
	
	function approve_records_post()
	{
		$inputValues = $this->post();			
		$timesheetIDcount = 0;
		$retVal = $this->TimesheetOnshore_Model->ApproveRecords($inputValues);
		$timesheetIDcount = count(json_decode($inputValues['timesheetIDs']));
		$title = "Approved";
		$msg = "Approved";
		$record = 'Record';
		if($timesheetIDcount > 1)
		{
			$record = 'Records';
		}
		if($inputValues['value']==0)
		{
			$title = "Reject";
			$msg = "Rejected";
		}
		if($inputValues['value']==2)
		{
			$title = "Approve";
			$msg = "Approved";
		}			
		if($retVal==0)
			$data = array("title"=>$title, "msg" => "$record Already $msg.", "success" => true);
		else if($retVal > 0)
			$data = array("title"=>$title, "msg" => "Record $msg Successfully.", "success" => true);
		else
			$data = array("msg" => "Data Imported Error.", "success" => false);
		$this->response($data);
	}
	
	
	public function getMonthlyDetailsSelf_get()
	{
		$data = $this->TimesheetOnshore_Model->monthlyDetailsSelf();
		
		if ($data['totalCount'] >=0) 
		{
			$this->response($data);
		} 
		else
			$this->response($this->nodata);
	}
	
	
	public function getMonthlyDetailsTeam_get()
	{
		$data = $this->TimesheetOnshore_Model->monthlyDetailsTeam();
		
		if ($data['totalCount'] >=0) 
		{
			$this->response($data);
		} 
		else
			$this->response($this->nodata);
	}
	
	
	public function getTimesheetStatusSelf_get()
	{
		$data = $this->TimesheetOnshore_Model->timesheetStatusSelf();
		
		if ($data['totalCount'] >=0) 
		{
			$this->response($data);
		} 
		else
			$this->response($this->nodata);
	}
	
	
	public function getTimesheetStatusTeam_get()
	{
		$data = $this->TimesheetOnshore_Model->timesheetStatusTeam();
		
		if ($data['totalCount'] >=0) 
		{
			$this->response($data);
		} 
		else
			$this->response($this->nodata);
	}
	
	
	public function getTaskDetailsSelf_get()
	{
		$data = $this->TimesheetOnshore_Model->taskDetailsSelf();
		
		if ($data['totalCount'] >=0) 
		{
			$this->response($data);
		} 
		else
			$this->response($this->nodata);
	}
	
	
	public function getTaskDetailsTeam_get()
	{
		$data = $this->TimesheetOnshore_Model->taskDetailsTeam();
		
		if ($data['totalCount'] >=0) 
		{
			$this->response($data);
		} 
		else
			$this->response($this->nodata);
	}

	public function downloadCsvTemplate_post()
	{
		$inputValues = $this->post();
		
		$path = FCPATH."download/";
		$filename = "Timesheet.csv";

		$retVal = $this->TimesheetOnshore_Model->downloadCsv($inputValues['start'], $inputValues['end']);

		if($retVal == 0)
			$this->response(array("msg" => "No Records Found", "success" => false));
		
		$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");			
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;        
		foreach ($retVal[0] as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true, "data" => 1));
	}

	public function duplicateRow_post()
	{			
		$retVal = $this->TimesheetOnshore_Model->duplicateRow();
		if($retVal==-1)
			$data = array("msg" => "Record already exists.", "success" => false);
		else if($retVal > 0)
			$data = array("msg" => "Record duplicated Successfully.", "success" => true);
		else
			$data = array("msg" => "Error Occured.", "success" => false);
		$this->response($data);
	}
}
?>