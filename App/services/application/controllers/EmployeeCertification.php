<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class EmployeeCertification extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model		
		$this->load->model('EmployeeCertification_Model');
	}
	
	
	/**
	* Fetch certificate list
	*
	* @param 
	* @return JSON
	*/
	function certificate_list_get()
	{
		$data = $this->EmployeeCertification_Model->list_certificates();
		$httpCode = 200;
		//print_r($data);
		$this->response($data,$httpCode);
	}

	function certificate_add_post()
    {
        $data   = $this->post();
		// echo "<PRE>";print_r($data);exit;
		
        $retVal = $this->EmployeeCertification_Model->addCertificate($data);
		//echo $retVal; exit;
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Certificate details added successfully.",
                "success" => "true"
            );      
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding Employee data.',
                "success" => "false"
            );
        $this->response($data);
    }

	function certificate_edit_post( $idVal = '',$data='')
    {
         $data   = $this->post();
		
		// print_r($data);
		// exit;
        $retVal = $this->EmployeeCertification_Model->updateCertificate( $idVal,$data);
        
        if ($retVal > 0)
            $data = array(
                "title" => "Updated",
                "msg" => "Employee Certificate updated successfully.",
                "success" => "true"
            );       
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while updating certificate data',
                "success" => "false"
            );
        $this->response($data);
    }
	// for deleting
	
	function certificate_del_delete($idVal = '')
	{
		$data = $this->delete('certificate');
		$delData = json_decode(trim($data), true);
		
		$retVal = $this->EmployeeCertification_Model->deleteCertificate($delData['id']);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Certificate deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
}

?>