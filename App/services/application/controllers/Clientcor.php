<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Clientcor extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model
		$this->load->model('Clientcor_Model');
	}
	
	
	/**
	* Fetch Client WOR
	*
	* @param 
	* @return JSON
	*/
	function list_get()
	{			
		$data = $this->Clientcor_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	
	/**
	 * Fetch Client COR
	 *
	 * @param
	 * @return JSON
	 */
	function cor_add_post()
	{
		// debug($this->post());exit;
		$data = $this->post('clientcor');			
		$fte_details = $this->post('fte_details');
		$hourly_details = $this->post('hourly_details');
		$volume_list = $this->post('volume_list');
		$project_list = $this->post('project_list');
		$client_name = $this->post('client_name');
	
		$return_value = $this->Clientcor_Model->add_cor($data, $fte_details, $hourly_details, $volume_list, $project_list,$client_name);
	
		if($return_value == 1)
		{
			$data = array("msg" => "Client COR Draft Saved Successfully.", "success" => "true", "title" => "Added");
			$httpCode = 200;
		}
		else if ($return_value == 2)
		{
			$data = array("msg" => "Client COR Submitted Successfully.", "success" => "true", "title" => "Submit");
			$httpCode = 200;
		}
		else if($return_value == 3)
		{
			$data = array("msg" => "Client COR Draft Updated Successfully.", "success" => "true", "title" => "Updated");
			$httpCode = 200;
		}
		else if ($return_value == 4)
		{
			$data = array("msg" => "Client COR Submitted Successfully.", "success" => "true", "title" => "Submit");
			$httpCode = 200;
		}
		else if ($return_value == 5)
		{
			$data = array("msg" => "Client WOR Deleted Successfully.", "success" => "true", "title" => "Delete");
			$httpCode = 200;
		}
		else if($return_value == 0 ){
			$data = array("msg" => "Deletion Count Mismatch", "success" => "true", "title" => "Error","exceed" => 1);
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch FTE list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function fte_list_get()
	{
		$data = $this->Clientcor_Model->get_fte_list();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch hourly list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function hourly_list_get()
	{
		$data = $this->Clientcor_Model->get_hourly_list();
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
	
		$this->response($data,$httpCode);
	}
	
	
	/**
	 * Fetch volume list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function volume_list_get()
	{
		$data = $this->Clientcor_Model->get_volume_list();
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Fetch project list of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function project_list_get()
	{
		$data = $this->Clientcor_Model->get_project_list();
	
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Save status of WOR
	 *
	 * @param
	 * @return JSON
	 */
	function saveStatus_post()
	{
		$data = $this->post();
	
		$return_value = $this->Clientcor_Model->update_status($data);
	
		if($return_value!="")
		{
			if($data['status']=="Closed")
			{
				$data = array("msg" => "Client COR Closed successfully.", "success" => "true", "title" => "closed");
			}
			else
			{
				$data = array("msg" => "Client COR Deleted successfully.", "success" => "true", "title" => "Deleted");
			}
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Listing of existing Model
	 *
	 * @param
	 * @return JSON
	 */
	function existingModel_get()
	{
		$data = $this->get();
	
		$data = $this->Clientcor_Model->existingModel($data);
	
		if($data!="")
		{
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	/**
	 * Listing of new Model
	 *
	 * @param
	 * @return JSON
	 */
	function newModel_get()
	{
		$data = $this->get();
	
		$data = $this->Clientcor_Model->newModel($data);
	
		if($data!="")
		{
			$httpCode = 200;
		}
		else
		{
			$httpCode = 204;
		}
	
		$this->response($data,$httpCode);
	}
	
	function chkLineRecords_get()
	{
		$retVal = $this->Clientcor_Model->chkLineRecord();
		if($retVal > 0) 
		{
			$data = array("msg" => "Record can be added", "success" => "true","val" => 1 );
		} 
		if($retVal <= 0) 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false" ,"val" => 0 );
		} 
		
		$this->response($data);
		return ;
	}
	
}

?>