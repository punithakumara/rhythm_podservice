<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attributes extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Attributes_Model');
	}

	function attributes_view_get()
	{
		if($this->input->get('allClientAttributes'))
		{
			$data = $this->Attributes_Model->getClientAttributes();
		}
		else
		{
			$data = $this->Attributes_Model->view();
		}

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}

		$this->response($data,$httpCode);
	}


	function attributes_del_delete($idVal = '')
	{
		$retVal = $this->Attributes_Model->deleteAttributes($idVal);

		if($retVal > 0) {
			$data = array("msg" => "Attribute Deleted Successfully.", "success" => "true");
			$this->response($data,200);
		}
		else {
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}

	function attributes_update_put( $idVal = '' ) {
		$data = $this->put('Attributes');
		$retVal = $this->Attributes_Model->updateAttributes( $idVal, $data );

		if($retVal > 0) 
			$data = array("title"=>"Updated","msg" => "Attributes Updated Successfully.", "success" => "true");
		else if($retVal == -1)	
			$data = array("title"=>"Existing","msg" => "Attributes Name Already Exists.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error Occured While Updating Attributes Data.', "success" => "false");

		$this->response($data);

	}

	function attributes_add_post() {
		$data = $this->post('Attributes');
		$retVal = $this->Attributes_Model->addAttributes( $data );

		if($retVal > 0) 
			$data = array("title"=>"Added","msg" => "Attributes Added Successfully.", "success" => "true");
		else if($retVal == -1)
			$data = array("title"=>"Existing","msg" => "Attributes Name Already Exists.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error Occured While Adding Attributes Data.', "success" => "false");
		$this->response($data);
	}

	public function SaveClientAttributes_get() 
	{
		$retVal = $this->Attributes_Model->saveclientattributes();

		if($retVal > 0) 
		{
			$data = array("msg" => "Record updated successfully.", "success" => "true");
		} 
		else if($retVal == -1) 
		{
			$data = array("msg" => "We can't set additional attributes.", "success" => "false");
		} 
		else 
		{
			$data = array("msg" => 'Error occured while updating Record data.', "success" => "false");
		}
		$this->response($data);
	}
}
?>