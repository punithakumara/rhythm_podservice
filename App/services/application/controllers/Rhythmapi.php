<?php header('Access-Control-Allow-Origin: *');

class Rhythmapi extends INET_Controller{
	
	var $acces_email = 'sankarshana.Venugopalachar@theoreminc.net';
	
	public function __construct()
	{		
		parent::__construct();	
		/*if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}*/			
		$this->load->library('AuthLDAP');
		$this->authldap->connect();
		$this->load->model(array('Rhythmapi_model'));		
		$this->config->load('admin_config');
		$this->ci =& get_instance();		
	}
	
	/*get active employee details */
	function get_employees_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);

		 //$data = $this->Rhythmapi_model->get_employees();
		 //$this->response($data);

		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_employees();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}	
	}

	/*get active client details */
	function get_clients_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);

		 //$data = $this->Rhythmapi_model->get_clients();
		 //$this->response($data);

		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_clients();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}
	}
	
	
	/*get active process details 
	function get_process_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);

		$data = $this->Rhythmapi_model->get_process();
		$this->response($data);

		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_process();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}	
	}*/
	
	/*get active vertical manager details */
	function get_vertical_manager_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);

		 //$data = $this->Rhythmapi_model->get_vertical_manager();
		 //$this->response($data);		
		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_vertical_manager();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}	
	}
	
	/*get active designation details */
	function get_designation_list_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);

		 //$data = $this->Rhythmapi_model->get_designation_list();
		 //$this->response($data);

		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_designation_list();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}
	}
	
	/*get vertical details */
	function get_vertical_list_get()
	{			
		/*$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);		
		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_vertical_list();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}*/

		$data = $this->Rhythmapi_model->get_vertical_list();				
		if(empty($data))
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}				
		$this->response($data,$httpCode);		
	}
	
	/*get employee process details */
	function get_employee_process_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);
// var_dump($user_info); exit;

		 //$data = $this->Rhythmapi_model->get_employee_process();
		 //$this->response($data);

		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_employee_process();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}	
	}
	
	/*get location details */
	function get_location_details_get()
	{				
		$username 	= $this->security->xss_clean(trim($this->input->get('username')));
		$password 	= $this->security->xss_clean(trim($this->input->get('password')));				
		$user_info = $this->authldap->authenticateUser($username, $password);

		 //$data = $this->Rhythmapi_model->get_location_details();
		 //$this->response($data);

		if(!empty($user_info))
		{
			if($user_info['0']['Email'] == $this->acces_email)
			{				
				$data = $this->Rhythmapi_model->get_location_details();				
				if(empty($data))
				{
					$httpCode = 204;
				}
				else 
				{
					$httpCode = 200;
				}				
				$this->response($data,$httpCode);				
			}
			else
			{
				echo 'invalid credentials (or) no access'; exit;
			}
		}
		else
		{
			echo 'invalid credentials (or) no access'; exit;			
		}	
	}

	function get_client_details_get()
	{
		if($_GET['authKey']=="@Pandora!")
		{
		$data = $this->Rhythmapi_model->get_client_utilization_details();
		if(empty($data))
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}				
		$this->response($data,$httpCode);
		}
		else{
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
	}

}

?>