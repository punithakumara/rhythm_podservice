<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ConsolidatedUtilizationReport extends INET_Controller {

	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->library('CSVReader.php');
		$this->load->model('Conutilizationreport_Model');
	}
	
	/**
	 * $reportOption is array => Utilization, Accuracy...
	 * $reportType is string => Month or Week
	 * $start => date
	 * $end =>date
	 * $ProcessID is Integer if user select a process and is array when user select multipal process.
	 * 
	 */
	function view_get()
	{
			
		//$reportOption = array();
		
		$ClientID 	= $this->input->get('ClientID');
		$VerticalID = $this->input->get('VerticalID');
		$ShiftID 	= $this->input->get('ShiftID');
		$managerID	= $this->input->get('managerID');
		$AMID		= $this->input->get('AMID');
		$TLId 		= $this->input->get('TLId');
		$EmployID	= $this->input->get('EmployID');
		$start 		= $this->input->get('startDate');
		$end 		= $this->input->get('endDate');
		$GroupBy = ($this->input->get('ConUtilGroupBy')!="")?$this->input->get('ConUtilGroupBy'):0;
		$Weekendsupport = $this->input->get('WeekendSupport');
		$ClientHoliday  = $this->input->get('ClientHoliday');
		
		// echo "THIS IS IN COntroller";
		
		// echo $ClientID;echo"<BR>";
		// echo $VerticalID;echo"<BR>";
		// echo $ShiftID;echo"<BR>";
		// echo $managerID;echo"<BR>";
		// echo $AMID;echo"<BR>";
		// echo $TLId;echo"<BR>";
		// echo $EmployID;echo"<BR>";
		// echo $start;echo"<BR>";
		// echo $end;echo"<BR>";
		// echo $GroupBy;echo"<BR>";
		// echo $Weekendsupport;echo"<BR>";
		// echo $ClientHoliday;echo"<BR>";
		if($GroupBy=='1')
		{
			$reports = $this->Conutilizationreport_Model->weekly_report(  $start, $end, $GroupBy, $ClientID, $VerticalID,$ShiftID,$managerID,$AMID,$TLId,$EmployID,$Weekendsupport,$ClientHoliday);
		}
		else
		{
			$reports = $this->Conutilizationreport_Model->getReports(  $start, $end, $GroupBy, $ClientID, $VerticalID,$ShiftID,$managerID,$AMID,$TLId,$EmployID,$Weekendsupport,$ClientHoliday);
		}
	
		if($reports['totalCount'] == 0)
			$httpCode = 204;
		else 
			$httpCode = 200;
		
		$this->response($reports,$httpCode);
	}


 
}