<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends INET_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Location_Model');
	}

	function locations_list_get(){
		
		$data = $this->Location_Model->getLocationData();
			
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
			
		$this->response($data,$httpCode);
	}
}

