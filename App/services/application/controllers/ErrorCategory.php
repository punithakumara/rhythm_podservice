<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class ErrorCategory extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model
		$this->load->model(array('ErrorCategory_Model'));
		
	}
	
	
	
	/**
	* Fetch All error category
	*
	* @param 
	* @return JSON
	*/
	function list_get()
	{			
		$data = $this->ErrorCategory_Model->list_error_category();
		//print_r($data);exit;
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}

}

?>