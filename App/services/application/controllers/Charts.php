<?php
class Charts extends INET_Controller
{
    public $EmpId = NULL;
    public $Year = NULL;
    public $super_admin = array();
    public $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
    public $nodata = array("success" => false);
    public $treeEmpArray = array();
    public $childProcess = array("NewJoinees-process", "Transition-process", "Long Leave-process");

    public function __construct()
    {
        parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
        $this->load->model(array('Rhythm_Model'));
        $this->load->model(array('NewIncidentLog_Model'));
        $empData = $this->input->cookie();
        if (isset($empData['employee_id']) != "")
            $this->EmpId = $empData['employee_id'];

        $this->Year = "";
        $this->config->load('admin_config');
        $this->super_admin = $this->config->item('super_admin');
    }


/*
* @description : Getting Billable and Non Billable Data ( Bar Graph )
* @return : json encode data
*/
public function getBillableDetails_get($PodId = "", $Year = "")
{
    $PodId = ($this->input->get('PodID') != '')? implode(",",json_decode($this->input->get('PodID'))) : '';
    $billabelRslt = $this->Rhythm_Model->getBillablityCount($PodId);

    if (is_array($billabelRslt))
        $this->response($billabelRslt);
    else
        $this->response($this->nodata);
}

public function getNrrRdrDetails_get($ClientID = "", $Year = "")
{
    $NrrRslt_open        = $this->Rhythm_Model->getNrrCount($this->EmpId, $ClientID, $Year,'Open');
    $NrrRslt_closed        = $this->Rhythm_Model->getNrrCount($this->EmpId, $ClientID, $Year,'Closed');
    $NrrRslt_fte       = $this->Rhythm_Model->getNrrCount($this->EmpId, $ClientID, $Year,'FTE');
    $NrrCount_open       = 0;
    $NrrCount_closed       = 0;
    $NrrCount_fte       =0;
    $NrrRdrBarGraph = array();
    $status         = 0;

    foreach ($this->months as $k => $month) {
        if (is_array($NrrRslt_open)) {
            foreach ($NrrRslt_open as $k => $nrrList_open)
                $NrrCount_open += $month ==  $nrrList_open['MONTH'] ?  $nrrList_open['NrrCount'] : 0;
        }
        if (is_array($NrrRslt_closed)) {
            foreach ($NrrRslt_closed as $k => $nrrList_closed)
                $NrrCount_closed += $month ==  $nrrList_closed['MONTH'] ?  $nrrList_closed['NrrCount'] : 0;
        }
        if (is_array($NrrRslt_fte)) {
            foreach ($NrrRslt_fte as $k => $nrrList_fte)
                $NrrCount_fte += $month ==  $nrrList_fte['MONTH'] ?  $nrrList_fte['NrrCount'] : 0;
        }

        $NrrRdrBarGraph[] = array(
            $month,
            $NrrCount_open,
            $NrrCount_closed,
            $NrrCount_fte  
        );

        if ($NrrCount_open != 0 || $NrrCount_closed != 0 ||  $NrrCount_fte != 0 && $status == 0)
            $status = 1;

        $NrrCount_open = 0;
        $NrrCount_closed = 0;
        $NrrCount_fte = 0;

    }


    if ($status == 1) {
// formig an data into graph struncture
        $tempBillableArray = array();
        foreach ($NrrRdrBarGraph as $key => $val) {
            $temp['Name']   = $val['0'];
            $temp['Value1'] = $val['1'];
            $temp['Value2'] = $val['2'];
            $temp['Value3'] = $val['3'];
            array_push($tempBillableArray, $temp);
        }
        $tempBillableArray = array(
            "success" => true,
            "data" => $tempBillableArray
        );
        $this->response($tempBillableArray);
    } else
    $this->response($this->nodata);
}    
public function getCorDetails_get($ClientID = "", $Year = "")
{
    $CorRslt_open        = $this->Rhythm_Model->getCorCount($this->EmpId, $ClientID, $Year,'Open');
    $CorRslt_closed        = $this->Rhythm_Model->getCorCount($this->EmpId, $ClientID, $Year,'Added');
    $CorRslt_fte       = $this->Rhythm_Model->getCorCount($this->EmpId, $ClientID, $Year,'Deleted');
    $CorCount_open       = 0;
    $CorCount_closed       = 0;
    $CorCount_fte       =0;
    $CorRdrBarGraph = array();
    $status         = 0;

    foreach ($this->months as $k => $month) {
        if (is_array($CorRslt_open)) {
            foreach ($CorRslt_open as $k => $corList_open)
                $CorCount_open += $month ==  $corList_open['MONTH'] ?  $corList_open['CorCount'] : 0;
        }
        if (is_array($CorRslt_closed)) {
            foreach ($CorRslt_closed as $k => $corList_closed)
                $CorCount_closed += $month ==  $corList_closed['MONTH'] ?  $corList_closed['CorCount'] : 0;
        }
        if (is_array($CorRslt_fte)) {
            foreach ($CorRslt_fte as $k => $corList_fte)
                $CorCount_fte += $month ==  $corList_fte['MONTH'] ?  $corList_fte['CorCount'] : 0;
        }

        $NrrRdrBarGraph[] = array(
            $month,
            $CorCount_open,
            $CorCount_closed,
            $CorCount_fte
        );

        if ($CorCount_open != 0 || $CorCount_closed != 0 ||  $CorCount_fte != 0 && $status == 0)
            $status = 1;

        $CorCount_open = 0;
        $CorCount_closed = 0;
        $CorCount_fte = 0;

    }


    if ($status == 1) {
// formig an data into graph struncture
        $tempBillableArray = array();
        foreach ($NrrRdrBarGraph as $key => $val) {
            $temp['Name']   = $val['0'];
            $temp['Value1'] = $val['1'];
            $temp['Value2'] = $val['2'];
            $temp['Value3'] = $val['3'];
            array_push($tempBillableArray, $temp);
        }
        $tempBillableArray = array(
            "success" => true,
            "data" => $tempBillableArray
        );
        $this->response($tempBillableArray);
    } else
    $this->response($this->nodata);
}    

/*
* @description : Getting data for Employee details panel
* @return : json encode data
*/
public function employDetails_get()
{
    $data = $this->Rhythm_Model->getEmployDetails();
    $this->response($data);
}


public function employDetailsPopup_get()
{
    $data = $this->Rhythm_Model->getEmployDetailsList();
    $this->response($data);
}

/**
Get Transition History Details Based on EmployID
**/
public function EmpHistoryDetails_get()
{
    $data = $this->Rhythm_Model->getEmpHistoryDetails();
    $this->response($data);
}

/**
Get Billable History Details Based on EmployID
**/
public function BillableHistoryDetails_get()
{
    $data = $this->Rhythm_Model->getBillableHistoryDetails();
    $this->response($data);
}
/**
Get Shift History Details Based on EmployID
**/
public function EmpShiftDetails_get()
{
    $data = $this->Rhythm_Model->getShiftHistDetails();
    $this->response($data);
}

/**
Get Client History Details Based on EmployID
**/
public function EmpClientDetails_get()
{
    $data = $this->Rhythm_Model->getClientHistDetails();
    $this->response($data);
}

/**
Get Vertical History Details Based on EmployID
**/
public function EmpVerticalDetails_get()
{
    $data = $this->Rhythm_Model->getVerticalHistDetails();
    $this->response($data);
}

/*
* @description : Getting Grade Data ( Pie Chart )
* @return : json encode data
*/
public function getGradeDetails_get($ClientID = "", $Year = "")
{
    $ClientID = ($this->input->get('ClientID') != '')? implode("','",json_decode($this->input->get('ClientID'))) : '';

    $GradeBasedArray  = $this->Rhythm_Model->getGradeCount($this->EmpId, $ClientID, $Year);
    $gradeResultArray = array();
    if (is_array($GradeBasedArray)) 
    {
        foreach ($GradeBasedArray as $k => $grade) 
        {
            if ($grade['Name'] == "3.1")
                $grade['Name'] = "3.1";
            if($grade['Name'] != "8" && $grade['Name'] != "9") 
            {
                $gradeResultArray[] = array(
                    "Name" => $grade['Name'],
                    'Value1' => $grade['Value1']
                );
            }
        }
    }
    $tempArray['data']    = $gradeResultArray;
    $tempArray['success'] = true;

    if ($tempArray > 0)
        $this->response($tempArray);
    else
        $this->response($this->nodata);
}

/*
* @description : Getting Grade Data ( Pie Chart )
* @return : json encode data
*/
public function getGradeVerticalDetails_get($PodId = "", $Year = "")
{
    $PodId = ($this->input->get('PodID') != '')? implode("','",json_decode($this->input->get('PodID'))) : '';
    $GradeBasedArray  = $this->Rhythm_Model->getGradeVerticalCount($this->EmpId, $PodId, $Year);
    $gradeResultArray = array();
    if (is_array($GradeBasedArray)) 
    {
        foreach ($GradeBasedArray as $k => $grade) 
        {
            if($grade['Name'] != "8" && $grade['Name'] != "9") 
            {
                $gradeResultArray[] = array(
                    "Name" => $grade['Name'],
                    'Value1' => $grade['Value1']
                );
            }
        }
    }
    $tempArray['data']    = $gradeResultArray;
    $tempArray['success'] = true;

    if ($tempArray > 0)
        $this->response($tempArray);
    else
        $this->response($this->nodata);
}


/*
* @description : Getting Location Data ( Pie Chart )
* @return : json encode data
*/
public function getLocationData_get($ClientID = "", $Year = "",$PodId="")
{       
    $PodId = ($this->input->get('PodId') != '')? implode("','",json_decode($this->input->get('PodId'))) : '0';

    $locationArray = $this->Rhythm_Model->getLocationCount($PodId);

    if (is_array($locationArray))
        $this->response($locationArray);
    else
        $this->response($this->nodata);
}

/*
* @description : Getting Shift Data ( Pie Chart)
* @return : json encode data
*/
public function getShiftData_get($ClientID = "", $Year = "",$PodId="")
{        
    $PodId = ($this->input->get('PodId') != '')? implode("','",json_decode($this->input->get('PodId'))) : '0';

    $ShiftRsltArray = $this->Rhythm_Model->getShiftCount($PodId);
    if (count($ShiftRsltArray) > 0)
        $this->response($ShiftRsltArray);
    else
        $this->response($this->nodata);
}


/*
* @description : Getting TreeStructuresEmployeeDetails data ( TreeView Fromat)
* @return : json data
*/
public function getTreeStructuresEmployeeData_get()
{
    $empId      = "";
    $empDetails = array();
    $employeeArray = array();

    if ($this->get('node') == 'root') 
    {
        $empDetails = $this->_getEmployeeName($this->EmpId);
        if (in_array($empDetails[0]['email'], $this->super_admin)) 
        {
            $empId      = $this->__getTopLevelEmpId();
            $empDetails = $this->_getEmployeeName($empId);
        } 
        else
            $empId = $this->EmpId;
    } 
    else if ($this->get('node') != 'root' && $this->get('node') != $empId && $this->get('node') != "ua-process") 
    {
        $empId = $this->get('node');
        if (!preg_match('/process/', $empId))
        {
            $empDetails = $this->Rhythm_Model->getEmpNames($empId);               
        }
    }

    if (is_array($empDetails)) 
    {
        $formatedTreeArray = $this->_formatTreeStructure($empId, $empDetails);
        $empData           = $this->input->cookie();
        $grade             = $empData['Grade'];
        switch ($grade) {
            case 3:
            case '3.1':
            case 4:{
                $this->load->model('INET_Model');
                $employeeArray = $this->INET_Model->getEmployeeList($empData['employee_id']);
            }
        }
        if ($grade >= 3) 
        {
            $formatedHR        = $this->_getTreeStructuresHRData();
            $formatedTraneed   = $this->_getTreeStructuresTraneedData();
            if(in_array($empData['VertId'], json_decode(OPS_VERT)) && (count($employeeArray)>0 || $grade>4))
            {
                $formatedNotassign = $this->_getTreeNotAssigned();
                $result            = array_merge($formatedTreeArray, $formatedHR, $formatedTraneed, $formatedNotassign);
            }
            else
            {
                $result            = array_merge($formatedTreeArray, $formatedHR, $formatedTraneed);
            }
        } 
        else
            $result = $formatedTreeArray;


        if (is_array($result))
            $this->response(array("data" => $result));
        else
            return false;
    } else
    return false;
}




/*
* @description : getting employee name for Tree View
* @return : imaged appended firstName
*/
private function _getEmployeeName($empid)
{
    $empArray = $this->Rhythm_Model->getUserName($empid);
    if (is_array($empArray)) 
    {
        return $empArray;
    }
    return false;
}

private function __getTopLevelEmpId()
{
    $query = "SELECT `employees`.`employee_id`, MAX(designation.`grades`) AS grade 
    FROM `designation` 
    JOIN `employees` WHERE designation.`designation_id` = employees.`designation_id` AND designation.grades=7 AND `employees`.`status` != 6 AND `employees`.`is_onshore` != '1' AND `employees`.`email` != 'test1@theoreminc.net' 
    GROUP BY designation.`grades` 
    ORDER BY grades DESC LIMIT 1";
    //echo $query;exit;
    $query  = $this->db->query($query);
    $result = $query->result_array();

    return $result[0]['employee_id'];
}

/*
* @description: formating the array based on tree struncture
* @return : Treeformated Array
*/
private function _formatTreeStructure($empId, $finalResultSet)
{
    $tempID      = "";
    $formatArray = array();
    $temp        = array();
    if ($this->get('node') != 'root') 
    {
        foreach ($finalResultSet as $key => $empDetails) 
        {
            $this->cnt        = false;
            $temp['id']       = $empDetails['employee_id'];
            $temp['text']     = $empDetails['FirstName'] . $this->_hierarchEmpCount($empDetails['employee_id']);
            $temp['icon']     = $this->_getEmployeeTreeImage($empDetails);
            $temp['leaf']     = !$this->_hasChildTreeNode($empDetails['employee_id']);
            $temp['tool_tip'] = $empDetails['name'];
            array_push($formatArray, $temp);
        }
    } 
    else 
    {
        array_push($formatArray, array(
            "id" => $empId,
            "text" => $finalResultSet[0]['FirstName'] . $this->_hierarchEmpCount($empId),
            "icon" => $this->_getEmployeeTreeImage($finalResultSet),
            "leaf" => !$this->_hasChildTreeNode($empId),
            "tool_tip" => $finalResultSet[0]['name']
        ));
    }

    return $formatArray;
}

/*
* @description : Getting TreeStructuresHRDetails data ( TreeView Fromat)
* @return : json data
*/
private function _getTreeStructuresHRData()
{
    $empDetails        = array();
    $formatArray       = array();
    $formatedTreeArray = array();

    if ($this->get('node') == 'root') 
    {
        $empDetails = array("Name"=>"RMG");
    } 
    else if ($this->get('node') != 'root' && ($this->get('node') == INI_PROCESS_ID . "-process" || in_array($this->get('node'), $this->childProcess))) 
    {
        $empDetails = $this->Rhythm_Model->getHRProcessEmpNames($this->get('node'));
    }

    if (is_array($empDetails) && count($empDetails) != 0) 
    {
        if (in_array($this->get('node'), $this->childProcess))
            $formatedTreeArray = $this->_formatHrpoolTreeStructure($empDetails);
        else
            $formatedTreeArray = $this->_getChildHrNodes(INI_PROCESS_ID, $empDetails);
        if (is_array($formatedTreeArray))
            return $formatedTreeArray;
        else
            return $empDetails;
    } else
    return $empDetails;
}

/* To Get HrPool Nodes*/
private function _getChildHrNodes($ProcessID, $empDetails)
{
    $formatArray     = array();
    $formatTreeArray = array();
    if ($this->get('node') != 'root') 
    {
        $getchildNodes = $this->_getProcessNameChilds();
        foreach ($getchildNodes as $key => $val) 
        {
            array_push($formatArray, $val);
        }
    } 
    else 
    {
        $ProcessCount = " (" . $this->_getHrpoolEmpCount() . ")";
        array_push($formatArray, array(
            "id" => $ProcessID . "-process",
            "text" => $empDetails['Name'] . $ProcessCount,
            "icon" => "resources/images/tree/hrpool.png",
            "leaf" => false,
            "tool_tip" => $empDetails['Name']
        ));
    }

    return $formatArray;
}

/* To get HrPool Count*/
private function _getHrpoolEmpCount()
{
    $query = "SELECT COUNT(employee_id) as TotalCount FROM `employees` JOIN `designation` 
    WHERE designation.`designation_id` = employees.`designation_id` 
    AND employees.status IN(1) AND employees.location_id != 4 ";

    $query                = $this->db->query($query);
    $result               = $query->result_array();
    $resultTransition     = $this->Rhythm_Model->getHRProcessEmpNames("Transition-process");
    $resultLongLeave     =  $this->Rhythm_Model->getHRProcessEmpNames("Long Leave-process");;
    if (is_array($resultTransition)) {
        $TransitionCount = count($resultTransition);
    } else {
        $TransitionCount = 0;
    }
    if (is_array($resultLongLeave)) {
        $LeaveCount = count($resultLongLeave);
    } else {
        $LeaveCount = 0;
    }
    return ($query->num_rows() != 0) ? ($result[0]['TotalCount']  + $TransitionCount + $LeaveCount) : "";
}


private function _formatHrpoolTreeStructure($empDetails)
{
    $tempID      = "";
    $formatArray = array();
    $temp        = array();
    $toolTip     = "";
    if ($this->get('node') != 'root') 
    {
        foreach ($empDetails as $key => $empDetails)
        {
            if ($this->get('node') == "Transition-process"  || $this->get('node') == "Long Leave-process") 
            {
                $toolTip = $empDetails['comment'];
            }
            else 
            {
                $toolTip = $empDetails['Name'];
            }

            $this->cnt        = false;
            $temp['id']       = $empDetails['employee_id'] . $this->get('node');
            $temp['text']     = $empDetails['FirstName'];
            $temp['icon']     = $this->_getEmployeeTreeImage($empDetails);
            $temp['leaf']     = true;
            $temp['tool_tip'] = $toolTip;
            array_push($formatArray, $temp);
        }
    }

    return $formatArray;
}

/* To get NewJoinee,Transition,Training Needed Nodes*/
private function _getProcessNameChilds()
{
    if (is_array($this->Rhythm_Model->getHRProcessEmpNames("NewJoinees" . "-process")))
        $NewJoineeCount = " (" . count($this->Rhythm_Model->getHRProcessEmpNames("NewJoinees" . "-process")) . ")";
    else
        $NewJoineeCount = "";


    if (is_array($this->Rhythm_Model->getHRProcessEmpNames("Transition" . "-process")))
        $TrainsitionCount = " (" . count($this->Rhythm_Model->getHRProcessEmpNames("Transition" . "-process")) . ")";
    else
        $TrainsitionCount = "";

    if (is_array($this->Rhythm_Model->getHRProcessEmpNames("Long Leave" . "-process")))
        $LongLeaveCount = " (" . count($this->Rhythm_Model->getHRProcessEmpNames("Long Leave" . "-process")) . ")";
    else
        $LongLeaveCount = "";

    $childHrProcess = array(
        array(
            "id" => "Transition" . "-process",
            "text" => "Available For Transition" . $TrainsitionCount,
            "icon" => "resources/images/menu/peplTransition.png",
            "leaf" => !$this->Rhythm_Model->getHRProcessEmpNames("Transition" . "-process"),
            "tool_tip" => "Transition"
        ),
        array(
            "id" => "Long Leave" . "-process",
            "text" => "Long Leave" . $LongLeaveCount,
            "icon" => "resources/images/tree/leave_New.jpg",
            "leaf" => !$this->Rhythm_Model->getHRProcessEmpNames("Long Leave" . "-process"),
            "tool_tip" => "Long Leave"
        )
    );

    return $childHrProcess;
}

/*
* @description : Getting TreeStructuresOnboardDetails data ( TreeView Fromat)
* @return : json data
*/


private function _getTreeStructuresTraneedData()
{
    $TrainingNeededCount = array();
    if ($this->get('node') == 'root') 
    {
        $TrainingNeededCount = array("FirstName"=>"Trainees");
    } 
    else if ($this->get('node') != 'root' && $this->get('node') == "tn-process") 
    {
        $TrainingNeededCount = $this->Rhythm_Model->getHRProcessEmpNames("Training Needed" . "-process");
    }
    if (is_array($TrainingNeededCount) && count($TrainingNeededCount) != 0)
    {
        $formatedTreeArray = $this->_formattranProcessTreeStructure("tn", $TrainingNeededCount);
        if (is_array($formatedTreeArray))
            return $formatedTreeArray;
        else
            return $TrainingNeededCount;
    } 
    else
        return $TrainingNeededCount;
}




/**
* Not assigned count
*/ 

private function _getTreeNotAssigned()
{
    $NotAssign = array();
    if ($this->get('node') == 'root') 
    {
        $NotAssign = array("FirstName"=>"Unassigned");
    } 
    else if ($this->get('node') != 'root' && $this->get('node') == "ua-process") 
    {
        $NotAssign = $this->Rhythm_Model->getNotAssignEmpNames("Unassigned" . "-process");
    }
    if (is_array($NotAssign))
    {
        $formatedTreeArray = $this->_formatProcessTreeStructure("ua", $NotAssign);
        if (is_array($formatedTreeArray))
            return $formatedTreeArray;
        else
            return $NotAssign;
    } 
    else
        return $NotAssign;
}




/*
* @description: formating the array based on tree struncture
* @return : Treeformated Array
*/
private function _formatProcessTreeStructure($ProcessId, $finalResultSet)
{
    $tempID      = "";
    $formatArray = array();
    $temp        = array();
    if ($this->get('node') != 'root') 
    {
        foreach ($finalResultSet as $key => $empDetails) 
        {
            $this->cnt        = false;
            $temp['id']       = $empDetails['employee_id'];
            $temp['text']     =  $empDetails['FirstName'];
            $temp['icon']     = $this->_getEmployeeTreeImage($empDetails);
            $temp['leaf']     = true;
            $temp['tool_tip'] = $empDetails['FirstName'];
            array_push($formatArray, $temp);
        }
    } 
    else 
    {
        $ProcessCount = " (" . count($this->Rhythm_Model->getNotAssignEmpNames()) . ")";
        array_push($formatArray, array(
            "id" => $ProcessId . "-process",
            "text" => "Unassigned". $ProcessCount,
            "icon" => "resources/images/tree/notAssign.png",
            "leaf" => false,
            "tool_tip" => "Unassigned"
        ));
    }

    return $formatArray;
}

private function _formattranProcessTreeStructure($ProcessId, $finalResultSet)
{
    $tempID      = "";
    $formatArray = array();
    $temp        = array();
    if ($this->get('node') != 'root') 
    {
        foreach ($finalResultSet as $key => $TrainingNeededCount) 
        {
            $this->cnt        = false;
            $temp['id']       = $TrainingNeededCount['employee_id'];
            $temp['text']     = $TrainingNeededCount['FirstName'] ;
            $temp['icon']     = $this->_getEmployeeTreeImage($TrainingNeededCount);
            $temp['leaf']     = true;
            $temp['tool_tip'] = $TrainingNeededCount['FirstName'];
            array_push($formatArray, $temp);
        }
    } 
    else 
    {
        $ProcessCount = " (" . count($this->Rhythm_Model->getHRProcessEmpNames("Training Needed" . "-process")) . ")";
        array_push($formatArray, array(
            "id" => $ProcessId . "-process",
            "text" => $finalResultSet['FirstName'] . $ProcessCount,
            "icon" => "resources/images/tree/training.png",
            "leaf" => false,
            "tool_tip" => $finalResultSet['FirstName']
        ));
    }

    return $formatArray;
}


private function _hierarchEmpCount($empID)
{
    $empCount = $this->Rhythm_Model->getEmpCountFromView($empID);

    return ($empCount != 0) ? " (" . $empCount . ")" : '';
}


/*
* @description : checking tree has child node or not
* @return : imaged appended firstName
*/
private function _hasChildTreeNode($empid)
{
    $empArray = $this->Rhythm_Model->getEmpProcessData($empid);
    if (is_array($empArray)) {
        return true;
    }
    return false;
}

/*
* @description : getting employee images for Tree View
* @return : imaged appended firstName
*/
private function _getEmployeeTreeImage($empArray)
{
    $grade = array_key_exists(0, $empArray) ? $empArray[0]['grades'] : $empArray['grades'];
    if (is_array($empArray)) 
    {
        switch ($grade) 
        {
            case 6:
            $imgName = "resources/images/tree/CEO.png";
            break;
            case 5:
            $imgName = "resources/images/tree/VM.png";
            break;
            case 4:
            $imgName = "resources/images/tree/AM.png";
            break;
            case 3:
            case '3.1':
            $imgName = "resources/images/tree/PL.png";
            break;
            default:
            $imgName = "resources/images/tree/ENG.png";
        }
        return $imgName;
    }
    return false;
}


private function _getProcessName($ProcessID)
{
    $query = "SELECT name FROM `process` WHERE process_id=" . $ProcessID;
    $query = $this->db->query($query);
    return $query->result_array();
}

/**
@Function for Get Resources With Respect To Clients
*/
public function getClientBillableResource_get()
{
    $data = $this->Rhythm_Model->clientBillableResource();
//total Count >= 0 @Refrence RHYT-844 issue 
    if ($data['totalCount'] >=0) 
    {
        $this->response($data);
    } 
    else
        $this->response($this->nodata);
}

/*
* @description : Getting LocationWiseClient data ( Stacked Bar Graph )
* @return : json data
*/
public function getlocationWiseClientData_get()
{
    $locationClientArray = $this->Rhythm_Model->locationwiseClient($this->EmpId);
// echo '<pre>';print_r($locationClientArray);exit;
    if (is_array($locationClientArray))
        $this->response($locationClientArray);
    else
        $this->response($this->nodata);
}

/*
* @description : Getting ShiftWiseClient data ( Stacked Bar Graph )
* @return : json data
*/
public function getShiftWiseClientData_get()
{
    $shiftClientArray = $this->Rhythm_Model->shiftwiseClient($this->EmpId);

    if (is_array($shiftClientArray))
        $this->response($shiftClientArray);
    else
        $this->response($this->nodata);
}
public function shiftClientDetails_get()
{
    $queryValue = $this->get();
    $empData    = $this->Rhythm_Model->getShiftClientDetails($queryValue['shift'], $queryValue['empId']);
    if (is_array($empData)) {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}
public function locationClientDetails_get()
{
    $queryValue = $this->get();
    $empData    = $this->Rhythm_Model->getlocationClientDetails($queryValue['location'], $queryValue['empId']);
    if (is_array($empData)) {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}

/*
* @description : Getting Accuracy Data (Bar Chart)
* @return : json encode data
*/
public function getAccuracyData_get()
{
    $tempArray     = array();
    $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : "";
    $IsExecutive   = $this->input->get('IsExecutive') ? $this->input->get('IsExecutive') : "";
    $sessionclient = $this->input->cookie();

    $ClientID = ($this->input->get('ClientID') != '')? json_decode($this->input->get('ClientID')) : '';

//Customer Dashboard
    if (isset($sessionclient['ClientID']))
        $ClientID = $sessionclient['ClientID'];

    if ($IsExecutive == 1)
    {
        $accuracyArray = $this->Rhythm_Model->ExeaccuracyDetails($ClientID);
        $parsedArray   = $this->_getExeParsedArray($accuracyArray, 'Accuracy');
    } 
    else 
    {
        $accuracyArray = $this->Rhythm_Model->accuracyDetails($ClientID,$groupvertical);
        $parsedArray   = $this->_getParsedArray($accuracyArray, 'Accuracy');
    }
    if (is_array($parsedArray)) 
    {
        $parsedArray = array_map('array_filter', $parsedArray);
        $parsedArray = array_filter($parsedArray);
        $parsedArray = array_map(array(
            $this,
            '__filterAccuracyData'
        ), $parsedArray);
        $this->response($parsedArray);
    }
    else
    {
        $this->response($this->nodata);
    }
}

public function DashboardGraph_get()
{
    $LogType   = $this->input->get('LogType') ? $this->input->get('LogType') : 'Error';
    $year      = $this->input->get('Year') ? $this->input->get('Year') : date('Y');
    $month     = $this->input->get('Month') ? $this->input->get('Month') : '';
    $ClientID = ($this->input->get('ClientID') != '')? implode("','",json_decode($this->input->get('ClientID'))) : '';
    $ProcessID = ($this->input->get('processID') != '')? implode("','",json_decode($this->input->get('processID'))) : '';
    $groupvertical = $this->input->get('groupvertical') ? $this->input->get('groupvertical') : '';
    $data      = $this->NewIncidentLog_Model->DashboardData($LogType, $year, $month, $ClientID, $ProcessID, $groupvertical);

    $this->response($data);
/*if($data) {
$this->response($data);
}
else {
$this->response($this->nodata);
}*/

}

/*
* @description : Common function for getting count and making formated array based on chartjs
* @param : array and Array field name
* @return : formated array or false
* */
private function _getParsedArray($dataArray = array(), $fieldName)
{
    $total     = 0;
    $rsltArray = array();
    $tempArray = array();
    $status    = false;
    foreach ($this->months as $k => $month) 
    {
        $i = 0;
        if (is_array($dataArray)) 
        {
            foreach ($dataArray as $key => $list) 
            {
                if ($month == $list['MONTH']) 
                {
                    $total += $list[$fieldName];
                    $i++;
                    $status = true;
                }
            }
        }
        $total       = $i == 0 ? 0 : $total / $i;
        $rsltArray[] = array(
            $month,
            $total
        );
        $total       = 0;
    }

// forming array based on graph js
    if ($status) 
    {
        foreach ($rsltArray as $key => $val) 
        {
            $temp['Name']   = $val['0'];
            $temp['Value1'] = $val['1'];
            array_push($tempArray, $temp);
        }
    }
    if (count($tempArray) >= 1) 
    {
$currMonths = array_splice($tempArray, 0, date('m')); // Escalating the Months from Current Year
$tempArray  = array_merge($tempArray, $currMonths);
return $tempArray;
} else
return false;
}

private function _getExeParsedArray($dataArray = array(), $fieldName)
{
    $monday1 = date('d-m-Y', strtotime('last monday', strtotime('tomorrow')));
    $monday2 = date('d-m-Y', strtotime('last monday', strtotime($monday1)));
    $monday3 = date('d-m-Y', strtotime('last monday', strtotime($monday2)));
    if (count($dataArray) >= 1)
        return $dataArray;
    else
        return false;
}


/* Filter Accuracy data */    
public function __filterAccuracyData($value)
{
    if (isset($value['Value1'])) 
    {
        if ((int) $value['Value1'] < 50)
            $value['Value1'] = 50;
    }
    return $value;
}

/* @description: Grade employee chart popup */
public function gradeEmployeeDetails_get()
{
    $queryValue  = $this->get();
    $billability = isset($queryValue['billability']) ? $queryValue['billability'] : '';
    $empData     = $this->Rhythm_Model->getGradeEmployeeDetails($queryValue['grade'], $queryValue['empId']);
    if (is_array($empData)) 
    {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}

/* @description: Grade  verticalwise employee chart popup */
public function gradePodEmployeeDetails_get()
{
    $queryValue  = $this->get();
    $billability = isset($queryValue['billability']) ? $queryValue['billability'] : '';
    $PodId = ($this->input->get('PodId') != '') ? implode(",",json_decode($queryValue['PodId'])) : '';
    $empData     = $this->Rhythm_Model->getGradePodEmployeeDetails($queryValue['grade'], $PodId, $queryValue['empId']);
    if (is_array($empData)) 
    {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}

public function locationEmployeeDetails_get()
{
    $queryValue = $this->get();
    $PodId = ($this->input->get('PodId') != '') ? implode(",",json_decode($queryValue['PodId'])) : '';
    $empData    = $this->Rhythm_Model->getEmpLocationDetails($queryValue['location'], $queryValue['empId'], $PodId, $queryValue['clientID']);
    if (is_array($empData)) 
    {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}

/* @description: shift employee chart popup */
public function shiftEmployeeDetails_get()
{
    $queryValue = $this->get();
    $PodId = ($this->input->get('PodId') != '')  ? implode(",",json_decode($queryValue['PodId'])) : '';
    $empData    = $this->Rhythm_Model->getShiftEmployeeDetails($queryValue['shift'], $queryValue['empId'], $queryValue['clientID'], $PodId);
    if (is_array($empData)) 
    {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}

private function _addSerialNos($data, $start)
{
    $l         = ($start == 0) ? 1 : $start + 1;
    $tempArray = $data;
    $i         = 1;
    foreach ($data['rows'] as $k => $v) 
    {
        $j                             = $i - 1;
        $tempArray['rows'][$j]['SlNo'] = $l;
        $i++;
        $l++;
    }

    return $tempArray;
}


/*
* @description : My reportee Billable
* @return : json encode data
*/
public function getMyReporteesBillable_get() 
{
    $billabelRslt = $this->Rhythm_Model->getMyRepBillable();
    if (is_array($billabelRslt))
        $this->response($billabelRslt);
    else
        $this->response($this->nodata);
}

/**
* @description : dashboard first carousel function
*/
function dashboardTablesGridOneView_get()
{
    $result = $this->Rhythm_Model->dashboardTablesGridOne();
    if (!empty($result)) {
        echo json_encode($result);
    } else {
        $result = $result['columns'] = array();
        echo json_encode($result);
    }
}


/******** Get Employees With PodID ***********/

public function getPodEmployees_get()
{
    $queryValue = $this->get();
    $PodID = $queryValue['PodID'];
    $coulmnName = $queryValue['Type'];
    $chartVhierarchy = isset($queryValue['chart'])?$queryValue['chart']:"";  
    if($chartVhierarchy == "")     
        $empData    = $this->Rhythm_Model->getEmployees($PodID, $coulmnName);
    else
        $empData    = $this->Rhythm_Model->getEmployeesForVhierarchy($PodID, $coulmnName, $chartVhierarchy);
    if (count($empData['rows']) > 0) {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    } else
    $this->response($this->nodata);
}

public function billableEmployeeDetails_get()
{
    $queryValue = $this->get();
    $empData    = $this->Rhythm_Model->getEmpBillableDetails($queryValue['billability'], $queryValue['PodId']);
    if (is_array($empData)) {
        $data = $this->_addSerialNos($empData, $queryValue['start']);
        $this->response($data);
    }
}


}
?>