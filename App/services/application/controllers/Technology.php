<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class Technology extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Technology_Model');
	}
	
	function technology_view_get()
	{
		$data = $this->Technology_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	function technology_del_delete($idVal = '')
	{
		$data = $this->delete('technology');
		$delData = json_decode(trim($data), true);
		
		$retVal = $this->Technology_Model->deleteTechnology($delData['technology_id']);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Technology deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
	
	function technology_update_post( $idVal = '' ) 
	{
		$data = $this->post();
		$retVal = $this->Technology_Model->updateTechnology( $idVal, $data );
		
		if($retVal > -1) 
			$data = array("title"=>"Updated","msg" => "Technology updated successfully.", "success" => "true");
		else if($retVal == -1)	
			$data = array("title"=>"Existing","msg" => "Technology Name already exists in this Vertical.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while updating Technology data.', "success" => "false");
		$this->response($data);

	}	
	
	function technology_add_post() 
	{
		$data = $this->post();
		$retVal = $this->Technology_Model->addTechnology( $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Added","msg" => "Technology added successfully.", "success" => "true");
		else if($retVal == -1)	
			$data = array("title"=>"Existing","msg" => "Technology Name already exists.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while adding Technology data.', "success" => "false");
		
		$this->response($data);
	}
	
	function DownloadtooltechCsv_get()
	{
		$path		= FCPATH."download\\";
		$filename	= "ToolTechnology.csv";
		$retVal 	= $this->Technology_Model->view();
		$retVal		= $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;        
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
	}
}
?>