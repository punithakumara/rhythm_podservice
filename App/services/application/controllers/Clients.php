<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends INET_Controller
{
	public $downloadPath = "";
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model
		$this->load->model('Clients_Model');
		$this->load->model('Utilization_Model');
		$this->load->model('Client_Contacts_Model','',True);
		$this->config->load('admin_config');
		// $this->downloadPath = $this->config->item('REPOSITORY_PATH').'resources/uploads/clientCDD/';
	}
	
	
	/**
	* Fetch Active Clients list
	*
	* @param 
	* @return JSON
	*/
	function client_view_get()
	{
		// debug($_GET);exit;
		if($this->input->get('worClientsList'))
		{
			$data = $this->Clients_Model->worClientsList();
			// debug($data);exit;
		}	
		else
		{

			$data = $this->Clients_Model->list_clients();
		}	
		
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	function GroupWorCombo_get()
	{
		
		$data = $this->Clients_Model->GroupWorComboValues();
		$this->response($data);
	}		
	
	/**
	* Fetch to get leads based on Clients
	*
	* @param 
	* @return JSON
	*/
	public function getLeads_get()
	{
		$data = $this->Clients_Model->getLeads();
		
		$this->response(array('leads' => $data),200);
	}

	/**
	* Fetch Countries list
	
	*/
	public function countries_list_get()
	{
		$data=$this->Clients_Model->getCountries();
		$this->response($data,200);
		//print_r($data);exit;
		//$this->load->view('employee_view');
	}
	
	public function clientuser_view_get()
	{
		$queryString = $this->get('ClientID');
		if($queryString !=""){
			$ClientUserList = $this->client_users_model->getClientUserList($queryString);
			$this->response($ClientUserList);
		}
	}
	
	function client_save_post() 
	{
		$data = $this->post();
		//var_dump($data['when_add_contact']);exit;
		
		if($data['country']=='Select Country'){
			$data['country'] = '';
		}
		$retVal = $this->Clients_Model->saveClient( $data );
		
		if($retVal['updateResp'] == -1){
			$data = array("title"=>"Existing", "msg" => "Client Name already exists.", "success" => true, "lastIsertedID"=>$retVal['lastInsertID']);
			$this->response($data);
		}
		else
		{
			if(is_array($retVal['uploadResp']) && ($retVal['uploadResp']['upload_success'] === false))
			{
				$data = array("title"=>"","msg" => $retVal['uploadResp']['upload_error'], "success" => true, "lastIsertedID"=>$retVal['lastInsertID']);
				$this->response($data);
			}
			elseif(($retVal['updateResp'] > 0) || (is_array($retVal['uploadResp']) && ($retVal['uploadResp']['upload_success'] === true))) {
				$data = array("title"=>"", "success" => true, "lastIsertedID"=>$retVal['lastInsertID']);
				$this->response($data);
			}
			else {
				$data = array("title"=>"","success" => true, "lastIsertedID"=>$retVal['lastInsertID']);
				$this->response($data);
			}
		}
	}

		// claim account
	public function Remove_clientContact_post() 
	{
		$inputValues		= $this->post();
		// echo "<PRE>";print_r($inputValues);
		$status				= $inputValues['status'];
		$role				= $inputValues['role'];
		$clientContactEmail	= $inputValues['clientContactEmail'];
		$clientContactId	= $inputValues['clientContactId'];
		$retVal				= $this->Client_Contacts_Model->Remove_clientContact($clientContactId, $clientContactEmail, $status, $role);
		if($status	== 'delete') 
		{
			$title	= 'Removed Client Contact';
			$msg	= 'Removed Client Contact.';
		} 
		else 
		{
			$title	= 'Client contact has been  rolled back';
			$msg	= 'Client contact has been  rolled back.';
		}
		if ($retVal > 0) 
		{
			$data = array(
				"title" 	=> $title,
				"msg" 		=> $msg,
				"success" 	=> "true"
			);
		} 
		else 
		{
			$data = array(
				"title" 	=> "Error",
				"msg" 		=> 'Error occured while updating data',
				"success" 	=> "false"
			);
		}
		$this->response($data);		
	}
	
	/* Get Wor List by Client*/
	public function get_wors_get()
	{
		$params = $this->get();
		if (array_key_exists("client_id",$params))
		{
		  	$client_id = $this->Utilization_Model->get_client_id($params['client_id']);
		}
		else {
			$client_id = $this->get('comboClient');
		}
		if($client_id !="")
		{
			if($this->get('utilClientWor'))
			{				
				$WorList = $this->Clients_Model->getUtilClientWorList($client_id);
			}
			else
			{
				$WorList = $this->Clients_Model->getWorList($client_id);
			}
		}

		if($WorList['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($WorList,$httpCode);	
	}
}

?>