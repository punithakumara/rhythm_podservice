<?php
header('Access-Control-Allow-Origin: *');

class Authenticate extends INET_Controller
{
	protected $data = array();
	public $super_admin =  array();
	public $login_email =  array();
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('AuthLDAP');
		$this->authldap->connect();
		$this->load->model(array('Employee_Model','Vertical_Model','Employees_Model'));
		$this->config->load('admin_config');
		$this->ci =& get_instance();
		$this->super_admin = $this->ci->config->item('super_admin');
		$this->login_email = $this->ci->config->item('login_email');
	}
	
	
 	/*
	* Getting system username and fetching username, loginid, email from ldap
	* @return : username else FALSE
	*/
	/* public function index_post()
	{
		$EmpID = $this->input->post('EmpID')?$this->input->post('EmpID'):"";
		if($EmpID !="")
		{
			$userData = $this->session->userdata;
			
			if($userData["user_data"]['employee_id'] == $EmpID)
			{
				setcookie("employee_id", $EmpID, time() + 60 * 15, '/');
				header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
				$user_Data = array("msg_info" => true);
			}
			else
			{
				$user_Data = array("msg_info" => false);
			}
		}
		else
		{
			$user_Data = array("msg_info" => false);
		}
		
		$this->response($user_Data,200);
	} */
	
	/** 
	* Setting session For Chrome Browser 
	*/
	function authentication_details_get()
	{
		$username = $this->input->get('username')?$this->input->get('username'):"";
		//var_dump($username);exit;	
		// if($userInfo = $this->_check_system_login($username))
		// {	
			if($username!="")
			{			
				$loginEmail = $username;
				//echo $loginEmail;
				
				if($this->login_email['name'] !="" && $this->login_email['value'])
				{
					$loginEmail = $this->login_email['name'];
				}
				
				$data = $this->Employee_Model->getEmployeeData($loginEmail);
				
				if(is_array($data))
				{
					if($username && $data[0]['employee_id'] != '')
					{
						$notifications =  $this->Employees_Model->getEmployeeNotifications($data[0]['employee_id']);
					    if(in_array($data[0]['email'], $this->super_admin)) 
						{
							$empGrade = '7';
						} 
						else 
						{
							$empGrade = $data[0]['grades'];
						}
						$people = array(360, 451, 2661, 333, 2161, 2691, 490,230,308,662,1442,2324,2754); // Domain Expert
						if($empGrade<3 || ($data[0]['designation_id']==128) || in_array($data[0]['employee_id'], $people))
						{
							$sql_client="SELECT  GROUP_CONCAT(DISTINCT ec.client_id) as client,GROUP_CONCAT(DISTINCT wor_id) AS wor_id FROM employee_client ec  WHERE ec.employee_id=".$data[0]['employee_id'];
							$query_client = $this->db->query($sql_client);
							$data_client = $query_client->result_array();
						}
						else if($empGrade>2 && $empGrade<4)
						{
							$sql_client="SELECT GROUP_CONCAT(res.client SEPARATOR ', ') AS client,GROUP_CONCAT(res.wor_id SEPARATOR ', ') AS wor_id
							FROM (SELECT  GROUP_CONCAT(wor.client) AS client,GROUP_CONCAT(wor_id) AS wor_id 
							FROM wor WHERE wor.team_lead REGEXP '[[:<:]]".$data[0]['employee_id']."[[:>:]]'
							UNION
							SELECT GROUP_CONCAT(DISTINCT ec.client_id) AS client,GROUP_CONCAT(DISTINCT wor_id) AS wor_id 
							FROM employee_client ec  
							WHERE ec.employee_id='".$data[0]['employee_id']."') AS res";
							
							$query_client = $this->db->query($sql_client);
							$data_client = $query_client->result_array();
						}
						else if($empGrade==4)
						{
							$sql_client="SELECT GROUP_CONCAT(res.client SEPARATOR ', ') AS client,GROUP_CONCAT(res.wor_id SEPARATOR ', ') AS wor_id
							FROM (SELECT  GROUP_CONCAT(wor.client) AS client,GROUP_CONCAT(wor_id) AS wor_id 
							FROM wor WHERE wor.associate_manager REGEXP '[[:<:]]".$data[0]['employee_id']."[[:>:]]'
							UNION
							SELECT GROUP_CONCAT(DISTINCT ec.client_id) AS client,GROUP_CONCAT(DISTINCT wor_id) AS wor_id 
							FROM employee_client ec  
							WHERE ec.employee_id='".$data[0]['employee_id']."') AS res";
							
							$query_client = $this->db->query($sql_client);
							$data_client = $query_client->result_array();
						}

						$appr_client = "SELECT * FROM wor WHERE wor.time_entry_approver REGEXP '[[:<:]]".$data[0]['employee_id']."[[:>:]]'";
						$query_client = $this->db->query($appr_client);
						$dataApprover = $query_client->num_rows();

						//Check login ip is within network
						$ip = $_SERVER['REMOTE_ADDR'];
						$allowed_ip = json_decode(ALLOWED_RANGE);
						$docAllow = (in_array($ip, $allowed_ip))?'yes':'no';

						// echo $dataApprover; exit;
						$userData = array(
							'employee_id'	=>	$data[0]['employee_id'],
							'gender'		=>	$data[0]['gender'],
							'Grade'			=>	$empGrade,
							'UserName'	 	=> 	$data[0]['first_name'],
							'LoginId' 		=> 	$userInfo[0]['LoginID'],
							'Email'			=>	$userInfo[0]['Email'],
							'Designation'	=>	$data[0]['designation_id'],
							'service_id'	=>	$data[0]['service_id'],
							'service_name'	=>	$data[0]['service_name'],
							'pod_id'        =>  $data[0]['pod_id'],
							'pod_name'        =>  $data[0]['pod_name'],
							'DesgnName'		=>	$data[0]['Name'],
							'UserDashboard'	=>	$data[0]['user_dashboard'],
							'DashboardTools'=>	$data[0]['dashboard_tools'],
							'VertId'        =>  $data[0]['assigned_vertical'],
							'VertName'      =>  $data[0]['name'],
							'ClientId'      =>  $data_client[0]['client'],
							'WorId'     	=>  $data_client[0]['wor_id'],
							'approver'     	=> 	($dataApprover!=0)?true:false,
							'success'		=>	true,
							'docAllow' 		=> $docAllow,
							'is_onshore' 		=> $data[0]['is_onshore']
						);

						$user_data = array_merge(array('Notifications' => $notifications['count']),$userData);

						$this->session->set_userdata('user_data', $user_data);

						setcookie("employee_id", $data[0]['employee_id'], COOKIE_EXPIRE,'/');
						setcookie("service_id", $data[0]['service_id'], COOKIE_EXPIRE,'/');
						setcookie("pod_id", $data[0]['pod_id'], COOKIE_EXPIRE,'/');
						setcookie("pod_name", $data[0]['pod_name'], COOKIE_EXPIRE,'/');
						setcookie("service_name", $data[0]['service_name'], COOKIE_EXPIRE,'/');
						setcookie("username", $data[0]['first_name'], COOKIE_EXPIRE,'/');
						setcookie("grade", $empGrade, COOKIE_EXPIRE,'/');
						setcookie("DsnName", $data[0]['Name'], COOKIE_EXPIRE,'/');
						setcookie("VertName", $data[0]['name'], COOKIE_EXPIRE,'/');
						setcookie("gender", $data[0]['gender'], COOKIE_EXPIRE,'/');
						setcookie("VertId", $data[0]['assigned_vertical'], COOKIE_EXPIRE,'/');
						setcookie("VertIDs", $data[0]['assigned_vertical'], COOKIE_EXPIRE,'/');
						setcookie("Uemail", $data[0]['Email'], COOKIE_EXPIRE,'/');
						setcookie("LoginId", $userInfo[0]['LoginID'], COOKIE_EXPIRE,'/');
						header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

						$logmsg = $data[0]['first_name'].'(EmpID:'.$data[0]['employee_id'].') was logged in. LoginId:'.$userInfo[0]['LoginID'];
						$this->userLogs($logmsg);
					}
					else
					{
						if($loginEmail=="")
							$loginEmail="Not Found";
						$user_data = array("msg" => "<span style='text-align:center; display:block;'>Your employee data is inactive. Kindly contact your vertical manager to activate the Rhythm profile.</span>", "success" => false);
					}
				}
				else
					$user_data = array("msg" => "<span style='text-align:center; display:block;'>Your employee data is not found. Kindly contact your HRBP to populate the details.</span>", "success" => false);
			}
			else
				$user_data = array("msg" => "Email doesn't exist in LDAP", "success" => false);
			$this->response($user_data,200);
		// }
	}

		/** 
		* Setting session For Firefox Browser 
	*/
		function authentication_details_post() 
		{
			$username 	= $this->security->xss_clean(trim($this->input->post('username')));
			$password 	= $this->security->xss_clean(base64_decode(trim($this->input->post('password'))));

			$user_data	= array();

			if(!empty($password)) 
			{
				$user_info = $this->authldap->authenticateUser($username, $password);
				if($user_info!="" && is_array($user_info)) 
				{
					if($user_info[0]['Email'] != "") 
					{
						$loginEmail = $user_info[0]['Email'];						

						if($this->login_email['name'] !="" && $this->login_email['value']) 
						{
							$loginEmail	= $this->login_email['name'];
						}

						$data = $this->Employee_Model->getEmployeeData($loginEmail);

						if(is_array($user_info) && isset($data[0]['employee_id']) && $data[0]['employee_id'] != '') 
						{

							if(in_array($data[0]['email'], $this->super_admin)) 
							{
								$empGrade = '7';
							} 
							else 
							{
								$empGrade = $data[0]['grades'];
							}

							$people = array(360, 451, 2661, 333, 2161, 2691, 490,230,308,662,1442,2324,2754); // Domain expert
							if($empGrade<3 || ($data[0]['designation_id']==128) || in_array($data[0]['employee_id'], $people))
							{
								$sql_client="SELECT  GROUP_CONCAT(DISTINCT ec.client_id) as client,GROUP_CONCAT(DISTINCT wor_id) AS wor_id FROM employee_client ec  WHERE ec.employee_id=".$data[0]['employee_id'];
								$query_client = $this->db->query($sql_client);
								$data_client = $query_client->result_array();
							}
							else if($empGrade>2 && $empGrade<4)
							{
								$sql_client="SELECT GROUP_CONCAT(res.client SEPARATOR ', ') AS client,GROUP_CONCAT(res.wor_id SEPARATOR ', ') AS wor_id
								FROM (SELECT  GROUP_CONCAT(wor.client) AS client,GROUP_CONCAT(wor_id) AS wor_id 
								FROM wor WHERE wor.team_lead REGEXP '[[:<:]]".$data[0]['employee_id']."[[:>:]]'
								UNION
								SELECT GROUP_CONCAT(DISTINCT ec.client_id) AS client,GROUP_CONCAT(DISTINCT wor_id) AS wor_id 
								FROM employee_client ec  
								WHERE ec.employee_id='".$data[0]['employee_id']."') AS res";
								$query_client = $this->db->query($sql_client);
								$data_client = $query_client->result_array();
							}
							else if($empGrade==4)
							{
								$sql_client="SELECT GROUP_CONCAT(res.client SEPARATOR ', ') AS client,GROUP_CONCAT(res.wor_id SEPARATOR ', ') AS wor_id
								FROM (SELECT  GROUP_CONCAT(wor.client) AS client,GROUP_CONCAT(wor_id) AS wor_id 
								FROM wor WHERE wor.associate_manager REGEXP '[[:<:]]".$data[0]['employee_id']."[[:>:]]'
								UNION
								SELECT GROUP_CONCAT(DISTINCT ec.client_id) AS client,GROUP_CONCAT(DISTINCT wor_id) AS wor_id 
								FROM employee_client ec  
								WHERE ec.employee_id='".$data[0]['employee_id']."') AS res";
								$query_client = $this->db->query($sql_client);
								$data_client = $query_client->result_array();
							}

							$appr_client = "SELECT * FROM wor WHERE wor.time_entry_approver REGEXP '[[:<:]]".$data[0]['employee_id']."[[:>:]]'";
							$query_client = $this->db->query($appr_client);
							$dataApprover = $query_client->num_rows();

							//Check login ip is within network
							$ip = $_SERVER['REMOTE_ADDR'];
							$allowed_ip = json_decode(ALLOWED_RANGE);
							$docAllow = (in_array($ip, $allowed_ip))?'yes':'no';

							//For getting Num of Notifications
							$notifications	=  $this->Employees_Model->getEmployeeNotifications($data[0]['employee_id']);
							
							$userData = array(
								'employee_id'	=>	$data[0]['employee_id'],
								'gender'		=>	$data[0]['gender'],
								'Grade'			=>	$empGrade,
								'UserName'	 	=> 	$data[0]['first_name'],
								'LoginId' 		=> 	$user_info[0]['LoginID'],
								'Email'			=>	$user_info[0]['Email'],
								'Designation'	=>	$data[0]['designation_id'],
								'service_id'	=>	$data[0]['service_id'],
								'service_name'	=>	$data[0]['service_name'],
								'pod_id'        =>  $data[0]['pod_id'],
								'pod_name'        =>  $data[0]['pod_name'],
								'DesgnName'		=>	$data[0]['Name'],
								'UserDashboard'	=>	$data[0]['user_dashboard'],
								'DashboardTools'=>	$data[0]['dashboard_tools'],
								'VertId'        =>  $data[0]['assigned_vertical'],
								'VertName'      =>  $data[0]['name'],
								'ClientId'      =>  $data_client[0]['client'],
								'WorId'      	=>  $data_client[0]['wor_id'],
								'approver'     	=> 	($dataApprover!=0)?true:false,
								'success'		=>	true,
								'docAllow'		=>  $docAllow,
								'isOnshore'		=>  $data[0]['is_onshore']
							);

							$user_data	= array_merge(array('Notifications'	=>	$notifications['count']), $userData); //For getting Num of Notifications

							$this->session->set_userdata('user_data', $user_data);	

							setcookie("employee_id", $data[0]['employee_id'], COOKIE_EXPIRE,'/');
							setcookie("service_id", $data[0]['service_id'], COOKIE_EXPIRE,'/');
							setcookie("service_name", $data[0]['service_name'], COOKIE_EXPIRE,'/');
							setcookie("pod_id", $data[0]['pod_id'], COOKIE_EXPIRE,'/');
							setcookie("pod_name", $data[0]['pod_name'], COOKIE_EXPIRE,'/');
							setcookie("username", $data[0]['first_name'], COOKIE_EXPIRE,'/');
							setcookie("grade", $empGrade, COOKIE_EXPIRE,'/');
							setcookie("DsnName", $data[0]['Name'], COOKIE_EXPIRE,'/');
							setcookie("VertName", $data[0]['name'], COOKIE_EXPIRE,'/');
							setcookie("gender", $data[0]['gender'], COOKIE_EXPIRE,'/');
							setcookie("VertId", $data[0]['assigned_vertical'], COOKIE_EXPIRE,'/');
							setcookie("VertIDs", $data[0]['assigned_vertical'], COOKIE_EXPIRE,'/');
							setcookie("Uemail", $data[0]['Email'], COOKIE_EXPIRE,'/');
							setcookie("LoginId", $userInfo[0]['LoginID'], COOKIE_EXPIRE,'/');
							setcookie("isOnshore", $data[0]['is_onshore'], COOKIE_EXPIRE,'/');
							header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
						} 
						else 
						{
							if($loginEmail == "") 
							{
								$loginEmail = "Not Found";
							}
							$user_data		= array(
								"msg" 		=> "<span style='text-align:center; display:block;'>Email mismatch from LDAP.</span> Your LDAP Email: ".$loginEmail, 
								"success"	=> false
							);
						}
					} 
					else 
					{
						$user_data = array("msg" => "Email doesn't exist in LDAP", "success" => false);
					}
				} 
				else 
				{
					$user_data = array("msg" => " You have entered wrong Username/Password, Please enter valid credentials", "success" => false);
				}
			} 
			else 
			{
				$user_data = array("msg" => " You have entered wrong Username/Password, Please enter valid credentials", "success" => false, "username" => $username, "password" => $password);
			}

			$this->response($user_data,200);
		}


		function _check_system_login($userName="")
		{
			$headers = apache_request_headers();
			if (!isset($headers['Authorization']))
			{
				header('HTTP/1.1 401 Unauthorized');
				header('WWW-Authenticate: NTLM');
				return FALSE;
			//exit;
			}
			$auth = $headers['Authorization'];
			if (substr($auth,0,5) == 'NTLM ') 
			{
				$msg = base64_decode(substr($auth, 5));
				if (substr($msg, 0, 8) != "NTLMSSP\x00")
				{
					return FALSE;
				//die('error header not recognised');
				}

				if ($msg[8] == "\x01") 
				{
					$msg2 = "NTLMSSP\x00\x02"."\x00\x00\x00\x00". // target name len/alloc
					"\x00\x00\x00\x00". // target name offset
					"\x01\x02\x81\x01". // flags
					"\x00\x00\x00\x00\x00\x00\x00\x00". // challenge
					"\x00\x00\x00\x00\x00\x00\x00\x00". // context
					"\x00\x00\x00\x00\x30\x00\x00\x00"; // target info len/alloc/offset

					header('HTTP/1.1 401 Unauthorized');
					header('WWW-Authenticate: NTLM '.trim(base64_encode($msg2)));
					exit;
				}
				else if ($msg[8] == "\x03") 
				{

					$user = $this->get_msg_str($msg, 36);
					$domain = $this->get_msg_str($msg, 28);
					$workstation = $this->get_msg_str($msg, 44);
				
				// echo $this->authldap->checkLoginID($user);die;
					if($userName!="")
					{
						$user_info = $this->authldap->checkLoginID($userName);
					}
					else
					{
						$user_info = $this->authldap->checkLoginID($user);
					}
//print_r($user_info); exit;
					if(count($user_info)==1)
						return $user_info;
					else	
						return FALSE;
				//print "You are $user from $workstation.$domain";
				}
			}
			else
				return FALSE;
		}

		/* @description : NTLR substring the msg */
		public function get_msg_str($msg, $start, $unicode = true) 
		{
			$len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
			$off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
			if ($unicode)
				return str_replace("\0", '', substr($msg, $off, $len));
			else
				return substr($msg, $off, $len);
		}


		/**
		*	Setting user credentioal (username, loginid, useremail) in session before redirecting 
	*/
		function _set_section($user_info=array(),$data=array(),$type="")
		{
			if(isset($data[0]['employee_id']))
			{
				$sess_array = array(
					'employee_id'		=> $data[0]['employee_id'],
					'gender'			=>	$data[0]['gender'],
					'UserName' 			=> $data[0]['UserName'],
					'Grade'				=> $data[0]['Grade'],
					'Designation'		=> $data[0]['Designation'],
					'DesgnName'			=> $data[0]['DesgnName'],
					'LoginId' 			=> $data[0]['LoginId'],
					'Email'				=> $data[0]['Email'],
					'VertId'        	=> $data[0]['VertId'],
					'VertName'      	=> $data[0]['VertName'],
					'ClientId'      	=> $data[0]['ClientId'],
					'UserDashboard'		=> $data[0]['UserDashboard'],
					'DashboardTools'	=> $data[0]['DashboardTools'],
				);

				$this->session->set_userdata('logged_in', TRUE);
				$this->session->set_userdata('user_data', $sess_array);	
			}
		}

		function human_filesize($bytes, $decimals = 2) 
		{
			$size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
			$factor = floor((strlen($bytes) - 1) / 3);
			return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
		}

		/* Destroying the session */
		public function logout_get()
		{
		// Just set logged_in to FALSE and then destroy everything for good measure
			$this->_check_system_login();
			$this->session->set_userdata(array('logged_in' => FALSE));
			$this->session->sess_destroy();

			$employee_id = $_COOKIE['employee_id'];
			$username = $_COOKIE['username'];
			$logmsg = $username.'(EmpID:'.$employee_id.') was logged out';
			$this->userLogs($logmsg);

		//delete_cookie('employee_id'); 
			setcookie('employee_id', '', time()-1000,'/');
			$user_data = array("msg" => "You are not a valid user", "success" => false);

			$this->response($user_data,200);
		}

		public function getCookieData_get()
		{
			if($this->input->cookie('employee_id',TRUE)!="")
			{
				$empData = $this->input->cookie('employee_id',TRUE) ;
			}
			else
				$empData="";

			$this->response($empData);
		}

		public function ResetPwd_post()
		{
			require_once('../onboarding/phpmailer/class.phpmailer.php');
			ini_set('max_execution_time', 300);

			$to = trim($this->input->post('emailID'));
			$resetURL = trim($this->input->post('URL'));

			$query = $this->db->query("SELECT Email FROM employ WHERE employee_id IN(SELECT employ.`Primary_Lead_ID` FROM employ 
				WHERE `Email` LIKE '%".$to."%')");
			$cc = $query->row_array();
			if(!empty($cc))
			{
				$this->load->library('encrypt');
				$enc_Emailid = base64_encode($to);
				$date_enc = base64_encode(date("Y-m-d h:i:s"));
				$resetURL = $resetURL.urlencode($enc_Emailid)."&valid=".urlencode($date_enc);
				$Body = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>
				<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
				<title>Untitled Document</title>
				</head>
				<body>
				<table width='600' border='0' cellspacing='0' cellpadding='0' align='center' style='border:1px solid #276193;'>
				<tr>
				<td height='60' align='center' valign='middle' bgcolor='#276193'>
				<img src='http://203.200.36.181/resources/images/rhythm_logo_new.png' width='240' height='53' />
				</td>
				</tr>
				<tr>
				<td align='left' valign='top' style='padding:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; height:auto'>
				<span style='font-size:14px; font-weight:bold;'>Dear User,</span>
				<br /><br />
				Click the below link to reset your password.<br /><a href='".$resetURL."' >Reset Password Link</a>
				<br /><br /><br />
				Regards,<br />
				Rhythm Support Team
				</td>
				</tr>
				<tr>
				<td align='left' valign='top'>&nbsp;</td>
				</tr>
				</table>
				</body>
				</html>";

				$mail = new PHPMailer(true);
				$mail->IsSMTP();                       // telling the class to use SMTP
				$mail->SMTPDebug = 0;                  

			// 0 = no output, 1 = errors and messages, 2 = messages only.
				$mail->SMTPDebug = 1; 
				$mail->SMTPAuth = true;                // enable SMTP authentication 
				$mail->SMTPSecure = "tls";              // sets the prefix to the servier
				$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
				$mail->Port = 587;                    // set the SMTP port for the GMAIL 
				$mail->Username = "rhythmsupport";  // Gmail username
				$mail->Password = "rhy_th499";      // Gmail password
				$mail->ContentType = 'text/html'; 
				$mail->IsHTML(true);
				$mail->CharSet = 'windows-1250';
				$mail->From = 'rhythmsupport@theoreminc.net';
				$mail->FromName = 'Rhythmsupport@theoreminc.net';
				$mail->addReplyTo('Rhythmsupport@theoreminc.net');

				$mail->AddCC($cc['Email']);		
				$mail->Subject = 'Reset Password Link';
				$mail->Body = $Body;
				$mail->AddAddress ($to);

			// echo $mail->Body; exit;
			// you may also use this format $mail->AddAddress ($recipient);
				if(!$mail->Send()) 
				{
					$data = array("msg" => "Unable to send mail.", "success" => false);

					$this->response($data,200);
				} else {
					$data = array("msg" => "A mail to reset your password.", "success" => true);

					$this->response($data,200);
				}
			} 
			else 
			{
				$data = array("msg" => "Unable to send mail.", "success" => false);

				$this->response($data,200);
			}
		}

		function validatenreset_get()
		{
			$emailid= $_GET['emailid'];
			$this->load->library('encrypt');
			$emailid=str_replace(array('-', '_', '~'), array('+', '/', '='), $emailid);
			$decoded_emailid=$this->encrypt->decode($emailid);
		//echo $decoded_emailid;

			$data['emailid'] = $decoded_emailid; 
			$this->load->view("ResetPassword", $data);
		}	

		function LdapConnect_post()
		{
			$inputfields=$this->input->post();
			$emailid=$inputfields['EmailId'];
			$NewPwd=$inputfields['NewPwd'];

			$parts = explode("@", $emailid);
			$username = $parts[0];
		//echo $username;exit; 

		//Code to change LDAP Password
			$server = "172.19.18.11";
				$user = $username;//'shaik.babu';//$user = 'venktesh.gupta';
				$serachedFor = "samaccountname";
				$ldap_search_filter = "(objectClass=user)";
		// $ldap_search_filter = "(|(uid=$user)(mail=shaik.babu@theoreminc.net))";
				$filter = "(&$ldap_search_filter ($serachedFor=$user))";

				$con = ldap_connect($server);
				ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');

				$ldapc=ldap_bind($con, "Anamika@TheoremIndia.net", "ana_th523");

				$user_search = ldap_search($con, 'OU=Bangalore_Users,OU=Theorem_Bangalore,DC=TheoremIndia,DC=net', "$filter", array('givenname','mail','samaccountname'));
				$user_get = ldap_get_entries($con, $user_search);

		// exit;
				$user_entry = ldap_first_entry($con, $user_search);
				$user_dn = ldap_get_dn($con, $user_entry);

				$auth_entry = ldap_first_entry($con, $user_search);
				$mail_addresses = ldap_get_values($con, $auth_entry, "mail");
				$given_names = ldap_get_values($con, $auth_entry, "givenName");

				/* And Finally, Change the password */
				$entry = array();
				$entry["userPassword"] = $NewPwd;


				if (ldap_modify($con,$user_dn,$entry) === false)
				{
			//print_r($con);
					$error = ldap_error($con);
					$errno = ldap_errno($con);
					$message[] = "E201 - Your password cannot be change, please contact the administrator.";
					$message[] = "$errno - $error";
				} 
				else 
				{
					echo 'else';
				}
			}

			function login_details_get()
			{
				$username 	= $this->security->xss_clean(trim($this->input->get('username')));
				$password 	= $this->security->xss_clean(trim($this->input->get('password')));
				$user_data	= array();

				if(!empty($password)) 
				{
					$checkIfClient = $this->Client_users_model->checkIfClient($username, $password);
					$user_info = $this->authldap->authenticateUser($username, $password);

					if($user_info!="" && is_array($user_info)) 
					{
						if($user_info[0]['Email'] != "") 
						{
							$loginEmail = $user_info[0]['Email'];

							if($this->login_email['name'] !="" && $this->login_email['value']) 
							{
								$loginEmail 	= $this->login_email['name'];
							}

							$data = $this->Employee_Model->getEmployeeData($loginEmail);
							$this->_set_section($user_info,$data);
							$multipleVerticals	= $this->Employee_Model->getEmployeeVerticals();

							if(count($multipleVerticals) >= 1) 
							{
								$assignedVerticals = $multipleVerticals;
							} 
							else 
							{
								$assignedVerticals = $data[0]['AssignedVertical'];
							}
							/* For Transition within Multiple Process  */
					// $processIDs 	= "";
					// $EmpProcessList = "";
							/* 
							if($EmpProcessList=$this->Employee_Model->getEmployeeProcessList($data[0]['employee_id']))
							{
								$processIDs	  = $EmpProcessList[0]['ProcessIDs'];
							} */

							/*End Change */
							if(is_array($user_info) && isset($data[0]['employee_id']) && $data[0]['employee_id'] != '') 
							{
								$EmployVertId 	= $this->Vertical_Model->getVerticalId($data[0]['employee_id'], $assignedVerticals);
								$EmpVertId		= '';
								$EmpProcId		= '';
								$EmpVertName 	= '';
								$empGrade 		= "";
								if($EmployVertId != '') 
								{
									$EmpVertId 		= $EmployVertId['VerticalID'];
									$EmpProcId 		= $EmployVertId['process_id'];
									$EmpVertName 	= $EmployVertId['Name'];
								}

								if(in_array($data[0]['Email'], $this->super_admin)) 
								{
									$empGrade = '7';

									if($EmpVertName == '') {
											$EmpVertName = ''; //'Super Admin';
										} else {
											$EmpVertName = $EmpVertName;
										}
									} 
									else 
									{
										$empGrade = $data[0]['grades'];
									}
						//$notifications	=  $this->Employee_Model->getEmployeeNotifications($data[0]['employee_id']);//For getting Num of Notifications
									$user_data 		= array(
										'Companyemployee_id' =>	$data[0]['companyemployee_id'],
										'Grade'			=>	$empGrade,
										'Designation'	=>	$data[0]['designation_id'],
										'UserName'	 	=> 	$data[0]['first_name'],
										'gender'		=>	$data[0]['gender'],
										'LoginId' 		=> 	$user_info[0]['LoginID'],
										'Email'			=>	$user_info[0]['email'],
										'DelxVer'		=>	INI_VERTICAL_ID,
										'HrVer'			=>	HR_VERTICAL_ID,
										'HrPro'			=>	HR_PROCESS_ID,
										'DelxPro'		=>	INI_PROCESS_ID,
										'Vert'			=>	$EmpVertId,
										'Proc'			=>	$EmpProcId,
										'VertName'		=>	$EmpVertName,
										'DesgnName'		=>	$data[0]['Name'],
										'ONBVer'		=>	ONB_VERTICAL_ID,
										'UserDashboard'	=>	$data[0]['UserDashboard'],
										'VertIDs'		=>	$assignedVerticals,
										'DashboardTools'=>	$data[0]['DashboardTools'],
										'is_onshore'		=>	$data[0]['is_onshore'],
							// 'processIDs'	=>	$processIDs,
										'success'		=>	true
									);
									$user_data 		= array_merge(array(
										'Notifications'	=>	$notifications['count']),
									$user_data
										); //For getting Num of Notifications

						//echo $data[0]['employee_id'];
						//session_name("employee_id");
						//$skillMatrixIp = "http://10.10.10.219:8082/menu.php";
						//setcookie("employee_id", $data[0]['employee_id'], time() + 48 * 3600, '/');

						setcookie("employee_id", $data[0]['employee_id'], COOKIE_EXPIRE,'/');
						setcookie("service_id", $data[0]['service_id'], COOKIE_EXPIRE,'/');
						setcookie("service_name", $data[0]['service_name'], COOKIE_EXPIRE,'/');
						setcookie("username", $data[0]['first_name'], COOKIE_EXPIRE,'/');
						setcookie("grade", $empGrade, COOKIE_EXPIRE,'/');
						setcookie("DsnName", $data[0]['Name'], COOKIE_EXPIRE,'/');
						setcookie("VertName", $EmpVertName, COOKIE_EXPIRE,'/');
						setcookie("gender", $data[0]['gender'], COOKIE_EXPIRE,'/');
						setcookie("VertId", $EmpVertId, COOKIE_EXPIRE,'/');
						setcookie("VertIDs", $assignedVerticals, COOKIE_EXPIRE,'/');
						setcookie("Uemail", $user_info[0]['email'], COOKIE_EXPIRE,'/');
						setcookie("LoginId", $user_info[0]['LoginID'], COOKIE_EXPIRE,'/');
						setcookie("is_onshore", $data[0]['is_onshore'], COOKIE_EXPIRE,'/');
						header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

						//session_set_cookie_params(3600, '/', 'localhost');
						//session_start();
						//echo "emp".$this->input->cookie('employee_id',TRUE);;
					} 
					else 
					{
						if($loginEmail == "") 
						{
							$loginEmail = "Not Found";
						}
						$user_data		= array(
							"msg" 		=> "<span style='text-align:center; display:block;'>Email mismatch from LDAP.</span> Your LDAP Email: ".$loginEmail,
							"success"	=> false
						);
					}
				} 
				else 
				{
					$userData = array("msg" => "Email doesn't exist in LDAP", "success" => false);
				}
			} 
			else 
			{
				$user_data = array("msg" => "You have entered wrong Username/Password, Please enter valid credentials", "success" => false);
			}
		} 
		else 
		{
			$user_data = array("msg" => "You have entered wrong Username/Password, Please enter valid credentials", "success" => false);
		}
		$this->response($user_data,200);
	}
	
	function getAllLdapUsers_get()
	{
		$data = $this->Employee_Model->getAllReporteesData(); 
		$httpCode = 200;
		
		$this->response($data,$httpCode);	
	}

	
	// Reachwell tool
	function getEmployees_get()
	{
	   $username 	= $this->security->xss_clean(trim($this->input->get('username')));
	   $password 	= $this->security->xss_clean(trim($this->input->get('password')));
		
		if($username == '' || $password == '')
		{
			echo 'Please Pass your Username and Password in URL'; exit;
		}
		
		if($username != 'theorememployees' )
		{
			echo 'Invalid Username'; exit;
		}
		
		if($password != '@dm1n-83172$' )
		{
			echo 'Invalid Password'; exit;
		}
	
	
		$data = $this->Employee_Model->getAllEmployeesData(); 
		$httpCode = 200;		
		$this->response($data,$httpCode);	
	}		
}

?>