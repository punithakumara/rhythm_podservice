<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qualification extends INET_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Qualification_Model');
	}

	function qualifications_list_get(){
		
		$data = $this->Qualification_Model->getQualificationData();
			
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else
		{
			$httpCode = 200;
		}
			
		$this->response($data,$httpCode);
	}
}

