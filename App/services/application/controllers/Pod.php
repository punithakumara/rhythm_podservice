<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class Pod extends INET_Controller
{
	public $EmpId = NULL;
	public $pods = NULL;
	public $Grade = NULL;
	var $tempArray = array();
	
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model(array('Pod_Model','Rhythm_Model'));	
		
		$empData = $this->input->cookie();
		
		if (isset($empData['EmployID']) != "")
		{
			$this->EmpId = $empData['EmployID'];
			$this->Grade = $empData['Grade'];
			if(isset($empData['PodIDs']))
			{
				if($empData['Grade'] <6)
				{
					$this->verticals = $empData['PodIDs'];
				}
			}
		}

		$this->config->load('admin_config');
		$this->super_admin = $this->config->item('super_admin');
	}
	
	function pod_view_get()
	{
		$data = $this->Pod_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);
	}
	
	function GroupPodCombo_get()
	{
		$data = $this->Vertical_Model->RevisedGroupPodComboValues();
		$this->response($data);
	}		
	
	function pod_my_get()
	{
		$data = array(array("key"=>"Karthik","name"=>"Karthik"),array("key"=>"Jana","name"=>"Jana"));			
		
		$this->response($data);
	}
	
	function pod_del_delete($idVal = '')
	{
		$retVal = $this->Pod_Model->deletePod($idVal);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "pod deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}
	
	function pod_update_put( $idVal = '' ) 
	{
		$data = $this->put('pod');
		$retVal = $this->Pod_Model->updatePod( $idVal, $data );
		
		if($retVal > 0) 
			$data = array("title"=>"Updated","msg" => "Pod updated successfully.", "success" => "true");
		else if($retVal == -1)
			$data = array("title"=>"Existing","msg" => "Pod Name already exists.", "success" => "true");
		else 
			$data = array("title"=>"Error","msg" => 'Error occured while updating Pod data.', "success" => "false");
		$this->response($data);
	}
	
	
	function pod_add_post() 
	{
		$data = $this->post('pod');
		
		$retVal = $this->Pod_Model->addPod( $data );
			
		if($retVal > 0) 
		{
			$data = array("title"=>"Added","msg" => "Pod added successfully.", "success" => "true");
			$this->response($data);
		}
		else if($retVal == -1)
		{
			$data = array("title"=>"Existing","msg" => "Pod Name already exists.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("title"=>"Error","msg" => 'Error occured while adding Pod data.', "success" => "false");
			$this->response($data);
		}
	}
	
	function parentPod_get() 
	{
		$retVal = $this->Pod_Model->getParentPod();
			
		if($retVal['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		$this->response($retVal,$httpCode);
	}
	
	function getParentPod_get()
	{
		$sql = 	"SELECT v.pod_id as Id,v.pod_name AS pPodName FROM tbl_rhythm_pod v WHERE v.parent_pod_id = 0 ORDER BY v.pod_name ASC";
		$query = $this->db->query($sql);        
		$data = $query->result_array();
		$data = array_merge(array(
			'totalCount' => $query->num_rows()
		), array(
			'rows' => $data
		));
	   
		$this->response( $data);
		
	}

	private function _hierarchpodCount($pID)
	{
		$query = $this->db->select("count(pod_id) as TotalCount")->from("tbl_rhythm_pod")->where("parent_pod_id", $pID);
		$query = $this->db->get();
		if ($query->num_rows() == 0)
			$empCount = 0;
		else
			$empCount = $query->row('TotalCount');
			
		return ($empCount != 0) ? " (" . $empCount . ")" : '';
	}


	function getPodTreeStructureData_get()
	{
		$node = $this->input->get();
		$root = $this->input->get('node');   
		$res  = $this->Pod_Model->repoteespodHierarchyGrid($this->Grade);
		if(isset($res)) 
		{
			foreach($res as $key => $val) 
			{
				$res[$key]['icon'] = '';//"resources/images/tree/DashboardSubVertical.png";
				$res[$key]['leaf'] = 'false';
				$res[$key]['id'] = $res[$key]['ParentPodID'].'-'.'podRoot';
			}
		}

		if($root == 'root') 
		{
			echo json_encode($res);
		} 
		else 
		{
			$podDet = explode('-', $root);          		         	
			// $hrpool = $this->Rhythm_Model->getHRPoolNonBillableEmployees();  
			
			if($podDet[1] != 'subpod')
			{
				$res  = $this->Pod_Model->getsubpodNames($podDet[0]);
				$res[2]['PodID'] = 5;
				$res[2]['Name'] = 'Onboarding';
				
				$res[3]['VerticalID'] = 10;
				$res[3]['Name'] = 'HR Pool';
				
				foreach($res as $key => $val) 
				{
					if($res[$key]['Name'] == 'Operations')
						$icon = "resources/images/tree/operations.png";
					else 
						$icon = "resources/images/tree/support.png";
					
					
					if(($res[$key]['Name'] == 'Operations') || ($res[$key]['Name'] == 'Support'))
					{
						$sql = $this->Rhythm_Model->getBillableCountQuery($Pods = '',$processIds = '',$subPod = $res[$key]['PodID']);
						$query = $this->db->query($sql);
						$billabledata = count($query->result_array());
						$sqlnonbill = $this->Rhythm_Model->getNonBillableCountQuery($Pods = '',$processIds = '',$subPod = $res[$key]['PodID']);
						$querynonbill = $this->db->query($sqlnonbill);
						$nonbillabledata = count($querynonbill->result_array());
						$res[$key]['Billable'] = $billabledata;
						$res[$key]['NonBillable'] = $nonbillabledata;
						$res[$key]['Leadership'] = $this->Rhythm_Model->getLeadpodcount($Pods = '',$processIds = '',$subPod= $res[$key]['PodID']);
						$res[$key]['iconCls'] = '';//$icon;
						$res[$key]['leaf'] = 'false';
						$res[$key]['id'] = $res[$key]['PodID'].'-'.'subVert';
					}
					
					if(($res[$key]['Name'] == 'HR Pool'))
					{
						$res[$key]['Billable'] = $hrpool[0]['Value1'];
						$res[$key]['NonBillable'] = $hrpool[0]['Value2'];
						$res[$key]['Leadership'] = $hrpool[0]['Value3'];
						$res[$key]['iconCls'] = '';
						$res[$key]['leaf'] = 'false';
						$res[$key]['id'] = $res[$key]['PodID'].'-'.'subPod';
					}
					
					if(($res[$key]['Name'] == 'Onboarding'))
					{
						$res[$key]['Billable'] = $hrpool[1]['Value1'];
						$res[$key]['NonBillable'] = $hrpool[1]['Value2'];
						$res[$key]['Leadership'] = $hrpool[1]['Value3'];
						$res[$key]['iconCls'] = '';
						$res[$key]['leaf'] = 'false';
						$res[$key]['id'] = $res[$key]['PodID'].'-'.'subPod';
					}
				}
				echo json_encode($res);
			}
			else
			{
				$res  = $this->Pod_Model->getPodsNames($podDet[0],$this->pods);
				foreach($res as $key => $val) 
				{
					$sql = $this->Rhythm_Model->getBillableCountQuery($Pods = $res[$key]['pod_id'],$processIds = '',$subPod = '');       
					$query = $this->db->query($sql);
					$billabledata = count($query->result_array());
					
					$sqlnonbill = $this->Rhythm_Model->getNonBillableCountQuery($Pods = $res[$key]['pod_id'],$processIds = '',$subPod = '');       
					$querynonbill = $this->db->query($sqlnonbill);
					$nonbillabledata = count($querynonbill->result_array());
					
					$getpodHead = $this->Pod_Model->getpodHead($res[$key]['pod_id']);
					$employID = explode("==",$getpodHead);												
					$sqlleadership = $this->Rhythm_Model->getLeadpodcount($Pods = $res[$key]['pod_id'],$processIds = '',$subPod = '');					
					
					$res[$key]['Billable'] = $billabledata;
					$res[$key]['NonBillable'] = $nonbillabledata;
					$res[$key]['Leadership'] = $sqlleadership;//$leadership;
					$res[$key]['iconCls'] = '';//"resources/images/tree/DashboardVertical.png";
					$res[$key]['leaf'] = 'true';
					$res[$key]['id'] = $res[$key]['pod_id'].'-'.'pods';
					$res[$key]['Employid'] = $employID[1];
					$res[$key]['tool_tip'] = $employID[0];
					$res[$key]['PodID'] = $res[$key]['pod_id'];
				}
				$i = 0;
				
				echo json_encode($res);
			}
		}
	}
	
	function getemployPods($sqlleadership, $employArray = array())
	{
		$querylead = $this->db->query($sqlleadership);
		$employList = array();
		if(count($querylead->result_array()) > 0)
		{
			foreach($querylead->result_array() as $key=> $value){
				array_push($employList, $value['empid']);
			}
				$employArray = array_merge($employArray, $employList);
		}
		return $employArray;
	}
	
	//get all verticals irrespective of user login
	function getAllPods_get()
	{
		$data = $this->Pod_Model->AllGroupPodComboValues();
		$this->response($data);
	}
	
	/**
	* Get Client Contact Vertical
	*/	
	public function getClientContactVertical_get() 
	{
		$filterQuery	= $this->input->get('query');
		$filterName     = $this->input->get('filterName') ? $this->input->get('filterName') : "";
		$sql			= "SELECT vertical_id, Name AS VerticalName FROM verticals WHERE 1 = 1";
		if ($filterName != "") 
		{
			if($filterName == 'VerticalName') 
			{
				$filterName = 'Name';
			}
			$filterWhere = $filterName . " LIKE '%" . $filterQuery . "%'";
			$sql .= " AND " . $filterWhere;
		}	
		
		$res = $this->db->query($sql);

		if(count($res->result_array()) > 0) 
		{
			$results = array_merge(array('totalCount' => count($res->result_array())), array('rows'=> $res->result_array()));
			$this->response($results);
		}
	}
}
?>