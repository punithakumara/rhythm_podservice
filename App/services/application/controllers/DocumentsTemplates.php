<?php
ini_set('upload_max_filesize', '20M');
defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentsTemplates extends INET_Controller 
{
	function __construct() 
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Document_Templates_Model');
		$this->ci =& get_instance();
	}
	
	function template_view_get()
	{	
		$data = $this->get();
		
		$data = $this->Document_Templates_Model->view();
		
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		$this->response($data,$httpCode);
	}
	
	
	private function _addSerialNos($data,$start)
	{
		$l = ($start==0)?1:$start+1;
		$tempArray = $data;
		$i=1;
		foreach($data as $k=>$v)
		{				
			$j=$i-1;
			$tempArray[$j]['SlNo']=$l;
			$i++;
			$l++;
		}			
		//echo "<pre>".print_r($tempArray);			
		return $tempArray;
	}
	
	// for saving
	function template_save_post() 
	{
		$data = $this->post();
		$filesize = floor($_FILES["NewDocument"]["size"]);
		$access = isset($data['Access']) ? $data['Access'] : '';
		$target_file ='';
		
		if(isset($_FILES['NewDocument']))
		{
			if($filesize<=FILE_SIZE_LIMIT) 
			{
				$file = $_FILES['NewDocument']['name'];	
				
				$target_file = $this->Document_Templates_Model->comDocTempCreate(basename($_FILES['NewDocument']['name']),$data['template_type']);
			}					
		}
		if($filesize<=FILE_SIZE_LIMIT) 
		{			
			$retVal = $this->Document_Templates_Model->addTemplates( $data );
		}
		else 
		{
			$retVal = 1;
		}			
		if($retVal > 0) 
		{
			if($filesize>FILE_SIZE_LIMIT) 
			{
				$data = array("msg" => "Document Size must be less than or equals to 20 MB.", "success" => "true", "filesize" => "max");
				$this->response($data);
			}
			else 
			{
				$data = array("msg" => "Document uploaded successfully.", "success" => "true");
				$this->response($data);
			}
		}
		else 
		{
			$data = array("msg" => 'Error occured while Processing data.', "success" => "false");
			$this->response($data);
		}
	}
	
	// for deleting
	function template_dele_post() 
	{
		$data = $this->post();
		
		$retVal = $this->Document_Templates_Model->template_dele( $data );
		if($retVal > 0) 
		{
			$data = array("msg" => "Documents Deleted successfully.", "success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("msg" => 'Error occured while Processing data.', "success" => "false");
			$this->response($data);
		}
	}

	//for validating the duplicate records

	function checkfileExists_post()
	{
		$data = $this->post();
		//var_dump($data);exit;
		if(($data['activeTab']=="Vertical Documents") || ($data['activeTab']=="Company Documents"))
		{
			$sql = "SELECT * FROM document where UploadName = '".$data['filename']."'";
			if($data['verticalID']!="")
			{
				$sql .=  "AND podservice_id='".$data['verticalID']."'";
			}
			else
			{
				$sql .=  "AND Access='All'";
			}
			$query = $this->db->query($sql);
			$totCount = $query->num_rows();

			$data = array("count" => $totCount);
			$this->response($data);
		}
		else
		{
			$sql = "SELECT * FROM document_templates where uploaded_default_name = '".$data['filename']."'";
			if($data['verticalID']!="")
			{
				$sql .=  "AND template_type='3' AND podservice_id='".$data['verticalID']."'";
			}
			else
			{
				$sql .=  "AND template_type='1' AND template_access_level='all'";
			}
			$query = $this->db->query($sql);
			$totCount = $query->num_rows();

			$data = array("count" => $totCount);
			$this->response($data);
		}
		 
	}
}
?>