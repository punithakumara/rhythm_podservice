<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empcategorize extends INET_Controller {
	
	function __construct() 
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model("Empcategore_Model");
	}
	
	
	public function empCategore_view_get() 
	{		
		$data = $this->Empcategore_Model->getCategorization();
			
		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		echo json_encode($data);		
	}
	
	
	public function empVertical_view_get() 
	{
		$data = $this->Empcategore_Model->getVerticalEmp();

		if($data['totalCount'] == 0)
		{
			$httpCode = 204;
		}
		else 
		{
			$httpCode = 200;
		}
		
		$this->response($data,$httpCode);		
	}
	
	
	public function empProcessAM_view_get() 
	{		
		$data = $this->Empcategore_Model->getProcessAm();
		
		$this->response($data);		
	}
	
	
	public function empProcessManagers_view_get() 
	{
		$grade = $this->input->get('Grade');
		$data = $this->Empcategore_Model->getProcessManagers($grade);	
		
		$this->response($data);		
	}
	
	
	public function empAMRelPro_view_get() 
	{
		$data = $this->Empcategore_Model->getAmRelPro();
		
		$this->response($data);		
	}
	
	
	public function empAmRelCli_view_get() 
	{
		$data = $this->Empcategore_Model->getAmRelCli();
		
		$this->response($data);		
	}
	
	
	public function empTLRelPro_view_get() 
	{
		$data = $this->Empcategore_Model->getTLRelPro();
		
		$this->response($data);		
	}
	
	
	public function empTLRelCli_view_get() 
	{
		$data = $this->Empcategore_Model->getTLRelCli();
		
		$this->response($data);
	}
	
	
	public function empProcessLead_view_get() 
	{
		$data = $this->Empcategore_Model->getProcessLead();
		
		$this->response($data);		
	}
	
	
	public function empAM_Leads_view_get() 
	{
		$data_Count = $this->Empcategore_Model->getAM_leads();
		
		$data = array("data" => $data_Count, "success" => "true");

		$this->response($data,200);
	}
	
	
	public function empCategoreDetails_get()
	{
		$retVal = $this->Empcategore_Model->getEmpCategorDetails();

		if($retVal > 0) 
		{			
			$data = array("msg" => "Employee Billable data updated successfully.", "success" => "true");
			
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => 'Error in updating employee data.', "success" => "false");
			
			$this->response($data,400);
		}
	}
		
	
	public function empShftChngeDetails_get()
	{
		$retVal = $this->Empcategore_Model->ShftChangeDetails();

		if($retVal > 0) 
		{
			$data = array("msg" => "Employee Shift Changed Successfully.", "success" => "true");
			
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => 'Error In Updating Employee Shift Data.', "success" => "false");
			
			$this->response($data,400);
		}
	}
	
	
	public function empRemoveDetails_get()
	{
		$retVal = $this->Empcategore_Model->removeDetails();

		if($retVal > 0) 
		{
			$data = array("msg" => "Employee removed successfully.", "success" => "true");
			
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => 'Error in removed employee data.', "success" => "false");
			
			$this->response($data,400);
		}
	}
	
	public function fte_summary_list_get()
	{
		$retVal = $this->Empcategore_Model->get_fte_summary_list();

		if($retVal > 0) 
		{
			$data = array("success" => "true", "data" => $retVal);
			
			$this->response($data,200);
		}
		else 
		{
			$data = array("success" => "false");
			
			$this->response($data,400);
		}
	}
	
	
	public function leadRemoveDetails_get()
	{
		$retVal = $this->Empcategore_Model->removeLead();

		if($retVal > 0) 
		{
			$data = array("msg" => "Employee removed successfully.", "success" => "true");
			
			$this->response($data,200);
		}
		else 
		{				
			$data = array("msg" => 'Error in removed employee data.', "success" => "false");
			
			$this->response($data,400);
		}
	}
	
	
	/**
    * @description : Getting employee billability log for particular employee
    * @return : billable end date
    */
	public function empBillableLogDetails_get() {
		
		$data = $this->Empcategore_Model->getBillableLogDetails();
		
		if($data != "")
		{
			$data = array("success" => "true", "data" => $data);
			
			$this->response($data,200);
		}
		else 
		{				
			$data = array( "success" => "false");
			
			$this->response($data,400);
		}		
	}
	
}

/* End of file empcategorize.php */
/* Location: ./application/controllers/empcategorize.php */