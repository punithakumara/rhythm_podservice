<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeProfile extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model		
		$this->load->model('EmployeeProfile_Model');
	}
	
	function month_list_get()
	{
		$data = $this->EmployeeProfile_Model->list_months();
		$httpCode = 200;
		$this->response($data,$httpCode);
	}

	function year_list_get()
	{
		// $data = $this->EmployeeProfile_Model->list_years();
		$start =  1990;
		$end = date('Y');
		$data = array();
		for( $i=$start; $i <= $end; $i++ ){
    		$year = array(
    			'year_name' => $i
    		);
    		$data[] = $year;
		}
		$httpCode = 200;
		$this->response($data,$httpCode);
	}
	/**
	* Fetch Active Employees list
	*
	* @param 
	* @return JSON
	*/
	function workexperience_list_get()
	{
		$data = $this->EmployeeProfile_Model->list_employees();
		$httpCode = 200;
		$this->response($data,$httpCode);
	}

    function experience_dele_post()
	{
		$data =  $this->post();
	
		$retVal = $this->EmployeeProfile_Model->deleteExperience($data);
		if($retVal > 0) 
		{
			$data = array("success" => "true");
			$this->response($data);
		}
		else 
		{
			$data = array("success" => "false");
			$this->response($data);
		}
	}
	
	function experience_add_post()
    {
        $data   = $this->post();
		
        $retVal = $this->EmployeeProfile_Model->addExperience($data);
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Experience details added successfully.",
                "success" => "true"
            );
        else if ($retVal == -1)
            $data = array(
                "title" => "Error",
                "msg" => "Employee ID or Employee Email already exists.",
                "success" => "true"
            );
        else if ($retVal == -2)
            $data = array(
                "title" => "Success",
                "msg" => "Employee details updated successfully.",
                "success" => "true"
            );
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding Employee data.',
                "success" => "false"
            );
        $this->response($data);
    }
}

?>