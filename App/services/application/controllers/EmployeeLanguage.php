<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
class EmployeeLanguage extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		//Loading model		
		$this->load->model('EmployeeLanguage_Model');
	}
	
	/**
	* Fetch Active Employees list
	*
	* @param 
	* @return JSON
	*/
	function language_list_get()
	{
		$data = $this->EmployeeLanguage_Model->list_languages();
		$httpCode = 200;
		$this->response($data,$httpCode);
	}
	function language_add_post()
    {
        $data   = $this->post();
		// echo "<PRE>";print_r($data);exit;
		
        $retVal = $this->EmployeeLanguage_Model->addLanguage($data);
		//echo $retVal; exit;
        if ($retVal > 0)
            $data = array(
                "title" => "Success",
                "msg" => "Language details added successfully.",
                "success" => "true"
            );      
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while adding language data.',
                "success" => "false"
            );
        $this->response($data);
    }

	function language_edit_post( $idVal = '',$data='')
    {
         $data   = $this->post();
		
		// print_r($data);
		// exit;
        $retVal = $this->EmployeeLanguage_Model->updatelanguage( $idVal,$data);
        
        if ($retVal > 0)
            $data = array(
                "title" => "Updated",
                "msg" => "Employee Language updated successfully.",
                "success" => "true"
            );       
        else
            $data = array(
                "title" => "Error",
                "msg" => 'Error occured while updating language data',
                "success" => "false"
            );
        $this->response($data);
    }
	// for deleting
	
	function language_del_delete($idVal = '')
	{
		$data = $this->delete('language');
		$delData = json_decode(trim($data), true);
		
		$retVal = $this->EmployeeLanguage_Model->deleteLanguage($delData['id']);
		
		if($retVal > 0) 
		{
			$data = array("msg" => "Employee Language detail deleted successfully.", "success" => "true");
			$this->response($data,200);
		}
		else 
		{
			$data = array("msg" => $retVal, "success" => "true");
			$this->response($data,400);
		}
	}	
	
}

?>