<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class ClientContacts extends INET_Controller
	{
		function __construct()
		{
			parent::__construct();
			if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
			$this->load->model('Client_Contacts_Model');
		}
		
		// list view
		function client_contacts_view_get()
		{
			
			$client_id = $this->input->get('client_id');
			$data = $this->Client_Contacts_Model->view($client_id);
			
			
			if($data['totalCount'] == 0) {
				$httpCode = 204;
			} else {
				$httpCode = 200;
			}
			if(is_array($data)){
				$this->response($data,$httpCode);
			}
		}		
		
		
		// for saving
		function Client_Contacts_save_post() 
		{
			$data = $this->post();
			
			$retVal = $this->Client_Contacts_Model->addClient_Contact( $data );
		
			if($retVal > 0) {
				$data = array("msg" => "Client Details Saved Successfully.", "success" => "true");
				$this->response($data);
			}
			else {
				$data = array("msg" => 'Error Occured While Processing Data.', "success" => "false");
				$this->response($data);
			}
		}
		
		
		// for updating
		function Client_Contacts_update_put() 
		{
			$data = $this->put();	
	
			foreach ($this->input->get() as $key => $value){
				$data[$key] = $value;
			}	
							
			$retVal = $this->Client_Contacts_Model->addClient_Contact( $data );
		
			if($retVal > 0) {
				$data = array("msg" => "Client Details Saved Successfully.", "success" => "true");
				$this->response($data);
			}
			else {
				$data = array("msg" => 'Error Occured While Processing Data.', "success" => "false");
				$this->response($data);
			}
		}		
		
	}
?>

