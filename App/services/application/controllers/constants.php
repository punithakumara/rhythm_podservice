<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('VERSION', '3.5');
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('FILE_SIZE_LIMIT', 20971520);
//for insite
define('INSIGHT_LIMIT', 250);
// for menus
define('HR_VERTICAL_ID', 10); // HR vertical id
define('HR_PROCESS_ID', 129); // HR Operations from process ID
//for trasition
define('INI_VERTICAL_ID', 10); // HR Vertical ID
define('INI_CLIENT_ID', 221); // Theroem India client ID
define('INI_PROCESS_ID', 223); // HR Pool process id from process
//for onboarding
define('ONBOARD_PROCESS_ID', 221); // Onboarding process ID
define('VVPROCESS_PROCESS_ID', 246); // Verification & Validation process ID
define('ONB_VERTICAL_ID', 5); // Delax Vertical.
//for shift IST
define('SHIFT_ID', 3); // IST Shift from shift table
define('THEOREM_MAIL','rhythmsupport@theoreminc.net');

// constants for survey related values like database congifuration and other urls
define('SURVEY_HOST', '172.19.18.98');
define('SURVEY_NAME', 'root');
define('SURVEY_PASSWORD', 'root');
define('SURVEY_DATABASE', 'survey');
define('COMMON_DATE_CONTROL', 'd-M-Y');

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

// ops verticle
// 6 - AdTech
// 7 - Tech Services
// 8 - Reporting
// 9 - MarTech
// 11 - Search
// 12 - Media Review
// 20 - Theorem India Pvt Ltd
define('OPS_VERT',json_encode(array(6,7,8,9,11,12,20)));
define('COOKIE_EXPIRE', time() + 24 * 3600);
define('ALLOWED_RANGE', json_encode(array('182.72.168.130','121.243.44.154','72.19.73.254','::1','172.19.19.229','172.19.72.214','115.113.118.10')));
define('BASE_URL',"https://rhythm.theoreminc.net/");