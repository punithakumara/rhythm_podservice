<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends INET_Controller {

	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->library('CSVReader.php');
		$this->load->model(array('Reports_Model'));
	}
	
	/**
	 * $reportOption is array => Utilization, Accuracy...
	 * $reportType is string => Month or Week
	 * $start => date
	 * $end =>date
	 * $ProcessID is Integer if user select a process and is array when user select multipal process.
	 * 
	 */

	public function csvConUtilizationReport_get(){
		$this->load->model('Conutilizationreport_Model');
		$path = FCPATH."download/";
		$filename = "ConUtilizationReports.csv";
		$GroupBy = $this->input->get('ConUtilGroupBy');
		if($GroupBy=='1') {
			$retVal = $this->Conutilizationreport_Model->weekly_reportCSV();
		} else {
			$retVal = $this->Conutilizationreport_Model->getCSVReports();
		}
	 	$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
			
	}
	
	public function csvExecutiveReport_get(){
	
		$path		= FCPATH."download/";
		$filename	= "ExecutiveReports.csv";
		$retVal		= $this->Reports_Model->Executive_reportCSV();
		$retVal 	= $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
			
	}
	
	function Bufferview_get()
	{
		$reports = $this->Reports_Model->getBufferReports();
		if($reports['totalCount'] == 0)
			$httpCode = 204;
		else
			$httpCode = 200;
	
		$this->response($reports,$httpCode);
	}

	function downloadBufferReportCsv_get()
	{
		$path		= FCPATH."download/";
		$filename	= "BufferReport.csv";
		$retVal 	= $this->Reports_Model->getBufferReports();
		$retVal 	= $this->waterMark($retVal);
		ob_start();
		$f 			= fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n 			= 0;
		foreach ($retVal as $line) {
			$n++;
			if ( !fputcsv($f, $line) ) {
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str		= ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
	}
	
	function opsDashboard_get()
	{		
	 	$data = $this->Reports_Model->getOpsDashboard();
        
        if ($data['totalCount'] == 0) {
            $httpCode = 204;
        } else {
            $httpCode = 200;
        }
        
        $this->response($data, $httpCode);
	}
	
	function  DownloadhierarchyCsv_get()
	{
		$retVal = "";
		$path = FCPATH."download/";
		$filename = "Employee.csv";
		$retVal = $this->Reports_Model->getOpsDashboard();
		$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;        
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
	}

	public function ResourceMapping_get()
	{
		$data = $this->Reports_Model->getResourceMappingReport();
        
        if ($data['totalCount'] == 0) {
            $httpCode = 204;
        } else {
            $httpCode = 200;
        }
        
        $this->response($data, $httpCode);
	}

	public function MbrMapping_post()
	{
		ini_set('max_execution_time', 0);
		ini_set("memory_limit","2048M"); 
		$data = $this->post();
		//echo "<pre>"; print_r($_FILES); exit;
		$target_file ='';
		$path = FCPATH."download/";
		$filename = "MBRReport.csv";
		 if(isset($_FILES['Document']))
		 {
			//echo "hi";
			$target_file = $this->Reports_Model->mbrReportCreate(basename($_FILES['Document']['name']));
		}	
		move_uploaded_file($_FILES['Document']['tmp_name'], $target_file);

		$csv = array_map('str_getcsv', file($target_file));
		unset($csv[0]);
		foreach($csv as $key => $value){
			$val[] = $value;
			$sql = "SELECT wor_id,client,wor_type_id FROM wor
			LEFT JOIN client c ON c.client_id = wor.`client`
			 WHERE wor_name='".$value[1]."' AND c.client_name='".$value[0]."'";
			$queryatri = $this->db->query($sql);
			$result[] = $queryatri->result_array();
		}
		foreach($result as $val){
			$sql = "SELECT SUM(CASE billable_type WHEN 'FTE Contract' THEN 1 ELSE 0 END) AS Billable_type FROM (SELECT client_id, employee_id, billable,id, billable_type, project_type_id, 
			wor_id FROM employee_client WHERE 1=1 ) AS temp JOIN employees e ON e.employee_id = temp.employee_id 
			JOIN employees m ON e.primary_lead_id = m.employee_id 
			JOIN project_types p ON p.project_type_id = temp.project_type_id 
			JOIN designation d ON e.designation_id = d.designation_id 
			WHERE e.status != 6 AND temp.wor_id = '".$val[0]['wor_id']."' AND temp.client_id = '".$val[0]['client']."' ";
			$queryatri = $this->db->query($sql);
			$result1[] = $queryatri->result_array();
		}
		
		foreach($result1 as $val){
			$value = $val[0]['Billable_type'];
			$res[] = $value; 
		}
		foreach($csv as $key=> $value){
			foreach($res as $key1=> $val){
				if(($key-1) == $key1){
				$data1[] = array(
					'Client Name' => $value[0],
					'Work Order Name' => $value[1],
					'Work Order Type' => $value[2],
					'Project' => $value[3],
					'Role' => $value[4],
					'FTE Contract' => $val,
				);
				break;
				}
			}
		}
		//var_dump($data1);exit;

		$return_array = array();
		foreach($data1 as $key => $value)
		{
			$return_array = array_keys($value);
			$return_array = str_replace("_"," ",$return_array);	
			$return_array = array_map('ucfirst', $return_array);
		}

		$result2 = array_merge(array(array_unique($return_array)), $data1);
		$result3 = array($result2);

		$result3[0][count($result3[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");			
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;        
		foreach ($result3[0] as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		unlink($target_file);

		$this->response(array("success" => true, "data" => 1));
	}

	public function DownloadResourceMappingCsv_get()
	{			
		$path = FCPATH."download/";
		$filename = "ResourceMapping.csv";
		$retVal = $this->Reports_Model->getResourceMappingReport();
		$retVal = $this->waterMark($retVal);
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0; 

		// echo"<pre>";print_r($retVal);exit;
		       
		foreach ($retVal as $line)
		{
			$n++;
			if ( ! fputcsv($f, $line))
			{
				show_error("Can't write line $n: $line");
			}
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
		$this->response(array("success" => true));
	}
}