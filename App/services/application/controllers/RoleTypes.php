<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoleTypes extends INET_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['user_data'])) {
			header("Location:".BASE_URL."services/index.php/Authenticate/logout");
			exit;
		}	
		$this->load->model('Role_Type_Model');
	}

	// list view
	function role_types_get()
	{
		$data = $this->Role_Type_Model->get_roleTypes();	
		return $this->response($data);
	}

}
?>

