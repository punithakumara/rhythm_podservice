function employdetails(empid,grade){
			$.ajax({
		        url: "competency_assement_emp_data.php",
		        type: "post",
		        sync: true,
		        data: {empid:empid},
			    success: function( result ) {
					console.log(result);
				     obj = JSON.parse(result);
			  	 	if(obj.Communication_1===undefined){
			    		var node_list = document.getElementsByName('Communication_1');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_1=="Knowledgeable") 
					    	  document.getElementById("Communication_1_Knowledgeable").checked=true;
			    		else if(obj.Communication_1=="Practitioner")
						    	  document.getElementById("Communication_1_Practitioner").checked=true;
			    		else if(obj.Communication_1=="Proficient")
						    	  document.getElementById("Communication_1_Proficient").checked=true;
			    		else if(obj.Communication_1=="Champion")
						    	  document.getElementById("Communication_1_Champion").checked=true;
				    	
			    	}

			    //2
			     	if(obj.Communication_2===undefined){
			    		var node_list = document.getElementsByName('Communication_2');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_2=="Knowledgeable") 
					    	  document.getElementById("Communication_2_Knowledgeable").checked=true;
			    		else if(obj.Communication_2=="Practitioner")
						    	  document.getElementById("Communication_2_Practitioner").checked=true;
			    		else if(obj.Communication_2=="Proficient")
						    	  document.getElementById("Communication_2_Proficient").checked=true;
			    		else if(obj.Communication_2=="Champion")
						    	  document.getElementById("Communication_2_Champion").checked=true;
				    	
			    	}
			    	//3
			    	if(obj.Communication_3===undefined){
			    		var node_list = document.getElementsByName('Communication_3');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_3=="Knowledgeable") 
					    	  document.getElementById("Communication_3_Knowledgeable").checked=true;
			    		else if(obj.Communication_3=="Practitioner")
						    	  document.getElementById("Communication_3_Practitioner").checked=true;
			    		else if(obj.Communication_3=="Proficient")
						    	  document.getElementById("Communication_3_Proficient").checked=true;
			    		else if(obj.Communication_3=="Champion")
						    	  document.getElementById("Communication_3_Champion").checked=true;
				    	
			    	}
				//4
			    	if(obj.Communication_4===undefined){
			    		var node_list = document.getElementsByName('Communication_4');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_4=="Knowledgeable") 
					    	  document.getElementById("Communication_4_Knowledgeable").checked=true;
			    		else if(obj.Communication_4=="Practitioner")
						    	  document.getElementById("Communication_4_Practitioner").checked=true;
			    		else if(obj.Communication_4=="Proficient")
						    	  document.getElementById("Communication_4_Proficient").checked=true;
			    		else if(obj.Communication_4=="Champion")
						    	  document.getElementById("Communication_4_Champion").checked=true;
				    	
			    	}   
			    	//5
			    	if(obj.Communication_5===undefined){
			    		var node_list = document.getElementsByName('Communication_5');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_5=="Knowledgeable") 
					    	  document.getElementById("Communication_5_Knowledgeable").checked=true;
			    		else if(obj.Communication_5=="Practitioner")
						    	  document.getElementById("Communication_5_Practitioner").checked=true;
			    		else if(obj.Communication_5=="Proficient")
						    	  document.getElementById("Communication_5_Proficient").checked=true;
			    		else if(obj.Communication_5=="Champion")
						    	  document.getElementById("Communication_5_Champion").checked=true;
				    	
			    	}   
			    	//6
			    	if(obj.Communication_6===undefined){
			    		var node_list = document.getElementsByName('Communication_6');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_6=="Knowledgeable") 
					    	  document.getElementById("Communication_6_Knowledgeable").checked=true;
			    		else if(obj.Communication_6=="Practitioner")
						    	  document.getElementById("Communication_6_Practitioner").checked=true;
			    		else if(obj.Communication_6=="Proficient")
						    	  document.getElementById("Communication_6_Proficient").checked=true;
			    		else if(obj.Communication_6=="Champion")
						    	  document.getElementById("Communication_6_Champion").checked=true;
				    	
			    	}   
			    	//7
			    	if(obj.Communication_7===undefined){
			    		var node_list = document.getElementsByName('Communication_7');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_7=="Knowledgeable") 
					    	  document.getElementById("Communication_7_Knowledgeable").checked=true;
			    		else if(obj.Communication_7=="Practitioner")
						    	  document.getElementById("Communication_7_Practitioner").checked=true;
			    		else if(obj.Communication_7=="Proficient")
						    	  document.getElementById("Communication_7_Proficient").checked=true;
			    		else if(obj.Communication_7=="Champion")
						    	  document.getElementById("Communication_7_Champion").checked=true;
				    	
			    	}
			    	//8
			    	if(obj.Communication_8===undefined){
			    		var node_list = document.getElementsByName('Communication_8');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_8=="Knowledgeable") 
					    	  document.getElementById("Communication_8_Knowledgeable").checked=true;
			    		else if(obj.Communication_8=="Practitioner")
						    	  document.getElementById("Communication_8_Practitioner").checked=true;
			    		else if(obj.Communication_8=="Proficient")
						    	  document.getElementById("Communication_8_Proficient").checked=true;
			    		else if(obj.Communication_8=="Champion")
						    	  document.getElementById("Communication_8_Champion").checked=true;
				    	
			    	}

			    	if(obj.Communication_9===undefined){
			    		var node_list = document.getElementsByName('Communication_9');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_9=="Knowledgeable") 
					    	  document.getElementById("Communication_9_Knowledgeable").checked=true;
			    		else if(obj.Communication_9=="Practitioner")
						    	  document.getElementById("Communication_9_Practitioner").checked=true;
			    		else if(obj.Communication_9=="Proficient")
						    	  document.getElementById("Communication_9_Proficient").checked=true;
			    		else if(obj.Communication_9=="Champion")
						    	  document.getElementById("Communication_9_Champion").checked=true;
				    	
			    	}
			    	//10
			    	if(obj.Communication_10===undefined){
			    		var node_list = document.getElementsByName('Communication_10');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_10=="Knowledgeable") 
					    	  document.getElementById("Communication_10_Knowledgeable").checked=true;
			    		else if(obj.Communication_10=="Practitioner")
						    	  document.getElementById("Communication_10_Practitioner").checked=true;
			    		else if(obj.Communication_10=="Proficient")
						    	  document.getElementById("Communication_10_Proficient").checked=true;
			    		else if(obj.Communication_10=="Champion")
						    	  document.getElementById("Communication_10_Champion").checked=true;
				    	
			    	}
			    	//column2 start
			    	
			    	
			    	if(obj.Outcome_Orientation_1===undefined){
			    		var node_list = document.getElementsByName('Outcome_Orientation_1');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Outcome_Orientation_1=="Knowledgeable") 
					    	      document.getElementById("Outcome_Orientation_1").checked=true;
			    		else if(obj.Outcome_Orientation_1=="Practitioner")
						    	  document.getElementById("Outcome_Orientation_1_Practitioner").checked=true;
			    		else if(obj.Outcome_Orientation_1=="Proficient")
						    	  document.getElementById("Outcome_Orientation_1_Proficient").checked=true;
			    		else if(obj.Outcome_Orientation_1=="Champion")
						    	  document.getElementById("Outcome_Orientation_1_Champion").checked=true;
				    	
			    	}

			    //2
			     	if(obj.Communication_2===undefined){
			    		var node_list = document.getElementsByName('Communication_2');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_2=="Knowledgeable") 
					    	  document.getElementById("Communication_2_Knowledgeable").checked=true;
			    		else if(obj.Communication_2=="Practitioner")
						    	  document.getElementById("Communication_2_Practitioner").checked=true;
			    		else if(obj.Communication_2=="Proficient")
						    	  document.getElementById("Communication_2_Proficient").checked=true;
			    		else if(obj.Communication_2=="Champion")
						    	  document.getElementById("Communication_2_Champion").checked=true;
				    	
			    	}
			    	//3
			    	if(obj.Communication_3===undefined){
			    		var node_list = document.getElementsByName('Communication_3');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_3=="Knowledgeable") 
					    	  document.getElementById("Communication_3_Knowledgeable").checked=true;
			    		else if(obj.Communication_3=="Practitioner")
						    	  document.getElementById("Communication_3_Practitioner").checked=true;
			    		else if(obj.Communication_3=="Proficient")
						    	  document.getElementById("Communication_3_Proficient").checked=true;
			    		else if(obj.Communication_3=="Champion")
						    	  document.getElementById("Communication_3_Champion").checked=true;
				    	
			    	}
				//4
			    	if(obj.Communication_4===undefined){
			    		var node_list = document.getElementsByName('Communication_4');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_4=="Knowledgeable") 
					    	  document.getElementById("Communication_4_Knowledgeable").checked=true;
			    		else if(obj.Communication_4=="Practitioner")
						    	  document.getElementById("Communication_4_Practitioner").checked=true;
			    		else if(obj.Communication_4=="Proficient")
						    	  document.getElementById("Communication_4_Proficient").checked=true;
			    		else if(obj.Communication_4=="Champion")
						    	  document.getElementById("Communication_4_Champion").checked=true;
				    	
			    	}   
			    	//5
			    	if(obj.Communication_5===undefined){
			    		var node_list = document.getElementsByName('Communication_5');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_5=="Knowledgeable") 
					    	  document.getElementById("Communication_5_Knowledgeable").checked=true;
			    		else if(obj.Communication_5=="Practitioner")
						    	  document.getElementById("Communication_5_Practitioner").checked=true;
			    		else if(obj.Communication_5=="Proficient")
						    	  document.getElementById("Communication_5_Proficient").checked=true;
			    		else if(obj.Communication_5=="Champion")
						    	  document.getElementById("Communication_5_Champion").checked=true;
				    	
			    	}   
			    	//6
			    	if(obj.Communication_6===undefined){
			    		var node_list = document.getElementsByName('Communication_6');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_6=="Knowledgeable") 
					    	  document.getElementById("Communication_6_Knowledgeable").checked=true;
			    		else if(obj.Communication_6=="Practitioner")
						    	  document.getElementById("Communication_6_Practitioner").checked=true;
			    		else if(obj.Communication_6=="Proficient")
						    	  document.getElementById("Communication_6_Proficient").checked=true;
			    		else if(obj.Communication_6=="Champion")
						    	  document.getElementById("Communication_6_Champion").checked=true;
				    	
			    	}   
			    	//7
			    	if(obj.Communication_7===undefined){
			    		var node_list = document.getElementsByName('Communication_7');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_7=="Knowledgeable") 
					    	  document.getElementById("Communication_7_Knowledgeable").checked=true;
			    		else if(obj.Communication_7=="Practitioner")
						    	  document.getElementById("Communication_7_Practitioner").checked=true;
			    		else if(obj.Communication_7=="Proficient")
						    	  document.getElementById("Communication_7_Proficient").checked=true;
			    		else if(obj.Communication_7=="Champion")
						    	  document.getElementById("Communication_7_Champion").checked=true;
				    	
			    	}
			    	//8
			    	if(obj.Communication_8===undefined){
			    		var node_list = document.getElementsByName('Communication_8');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_8=="Knowledgeable") 
					    	  document.getElementById("Communication_8_Knowledgeable").checked=true;
			    		else if(obj.Communication_8=="Practitioner")
						    	  document.getElementById("Communication_8_Practitioner").checked=true;
			    		else if(obj.Communication_8=="Proficient")
						    	  document.getElementById("Communication_8_Proficient").checked=true;
			    		else if(obj.Communication_8=="Champion")
						    	  document.getElementById("Communication_8_Champion").checked=true;
				    	
			    	}

			    	if(obj.Communication_9===undefined){
			    		var node_list = document.getElementsByName('Communication_9');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_9=="Knowledgeable") 
					    	  document.getElementById("Communication_9_Knowledgeable").checked=true;
			    		else if(obj.Communication_9=="Practitioner")
						    	  document.getElementById("Communication_9_Practitioner").checked=true;
			    		else if(obj.Communication_9=="Proficient")
						    	  document.getElementById("Communication_9_Proficient").checked=true;
			    		else if(obj.Communication_9=="Champion")
						    	  document.getElementById("Communication_9_Champion").checked=true;
				    	
			    	}
			    	//10
			    	if(obj.Communication_10===undefined){
			    		var node_list = document.getElementsByName('Communication_10');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Communication_10=="Knowledgeable") 
					    	  document.getElementById("Communication_10_Knowledgeable").checked=true;
			    		else if(obj.Communication_9=="Practitioner")
						    	  document.getElementById("Communication_10_Practitioner").checked=true;
			    		else if(obj.Communication_9=="Proficient")
						    	  document.getElementById("Communication_10_Proficient").checked=true;
			    		else if(obj.Communication_9=="Champion")
						    	  document.getElementById("Communication_10_Champion").checked=true;
				    	
			    	}
			
			    <!-- Customer Orientation column start -- >
			    


			//1

				if(obj.Customer_Orientation_1===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_1=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_1_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_1=="Practitioner")
					    	  document.getElementById("Customer_Orientation_1_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_1=="Proficient")
					    	  document.getElementById("Customer_Orientation_1_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_1=="Champion")
					    	  document.getElementById("Customer_Orientation_1_Champion").checked=true;
			    	
		    	}

		    //2
				if(obj.Customer_Orientation_2===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_2=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_2_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_2=="Practitioner")
					    	  document.getElementById("Customer_Orientation_2_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_2=="Proficient")
					    	  document.getElementById("Customer_Orientation_2_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_2=="Champion")
					    	  document.getElementById("Customer_Orientation_2_Champion").checked=true;
			    	
		    	}
		    	//3
				if(obj.Customer_Orientation_3===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_3=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_3_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_3=="Practitioner")
					    	  document.getElementById("Customer_Orientation_3_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_3=="Proficient")
					    	  document.getElementById("Customer_Orientation_3_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_3=="Champion")
					    	  document.getElementById("Customer_Orientation_3_Champion").checked=true;
			    	
		    	}
			//4
				if(obj.Customer_Orientation_4===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_4=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_4_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_4=="Practitioner")
					    	  document.getElementById("Customer_Orientation_4_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_4=="Proficient")
					    	  document.getElementById("Customer_Orientation_4_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_4=="Champion")
					    	  document.getElementById("Customer_Orientation_4_Champion").checked=true;
			    	
		    	}  
		    	//5
				if(obj.Customer_Orientation_5===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_5=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_5_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_5=="Practitioner")
					    	  document.getElementById("Customer_Orientation_5_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_5=="Proficient")
					    	  document.getElementById("Customer_Orientation_5_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_5=="Champion")
					    	  document.getElementById("Customer_Orientation_5_Champion").checked=true;
			    	
		    	}    
		    	//6
				if(obj.Customer_Orientation_6===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_6=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_6_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_6=="Practitioner")
					    	  document.getElementById("Customer_Orientation_6_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_6=="Proficient")
					    	  document.getElementById("Customer_Orientation_6_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_6=="Champion")
					    	  document.getElementById("Customer_Orientation_6_Champion").checked=true;
			    	
		    	}    
		    	//7
				if(obj.Customer_Orientation_7===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_7=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_7_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_7=="Practitioner")
					    	  document.getElementById("Customer_Orientation_7_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_7=="Proficient")
					    	  document.getElementById("Customer_Orientation_7_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_7=="Champion")
					    	  document.getElementById("Customer_Orientation_7_Champion").checked=true;
			    	
		    	} 
		    	//8
				if(obj.Customer_Orientation_8===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_8=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_8_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_8=="Practitioner")
					    	  document.getElementById("Customer_Orientation_8_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_8=="Proficient")
					    	  document.getElementById("Customer_Orientation_8_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_8=="Champion")
					    	  document.getElementById("Customer_Orientation_8_Champion").checked=true;
			    	
		    	} 
				//9
				if(obj.Customer_Orientation_9===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_9');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_9=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_9_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_9=="Practitioner")
					    	  document.getElementById("Customer_Orientation_9_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_9=="Proficient")
					    	  document.getElementById("Customer_Orientation_9_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_9=="Champion")
					    	  document.getElementById("Customer_Orientation_9_Champion").checked=true;
			    	
		    	} 
		    	//10
				if(obj.Customer_Orientation_10===undefined){
		    		var node_list = document.getElementsByName('Customer_Orientation_10');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Customer_Orientation_10=="Knowledgeable") 
				    	  document.getElementById("Customer_Orientation_10_Knowledgeable").checked=true;
		    		else if(obj.Customer_Orientation_10=="Practitioner")
					    	  document.getElementById("Customer_Orientation_10_Practitioner").checked=true;
		    		else if(obj.Customer_Orientation_10=="Proficient")
					    	  document.getElementById("Customer_Orientation_10_Proficient").checked=true;
		    		else if(obj.Customer_Orientation_10=="Champion")
					    	  document.getElementById("Customer_Orientation_10_Champion").checked=true;
			    	
		    	} 
		        <!-- Customer Orientation column End -- >		


		        <!-- Integrity & Work Ethics column Start -- >	

				//1

					if(obj.Integrity_Work_Ethics_1===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_1');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_1=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_1_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_1=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_1_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_1=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_1_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_1=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_1_Champion").checked=true;
				    	
			    	}

			    //2
					if(obj.Integrity_Work_Ethics_2===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_2');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_2=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_2_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_2=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_2_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_2=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_2_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_2=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_2_Champion").checked=true;
				    	
			    	}
			    	//3
					if(obj.Integrity_Work_Ethics_3===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_3');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_3=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_3_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_3=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_3_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_3=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_3_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_3=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_3_Champion").checked=true;
				    	
			    	}
				//4
					if(obj.Integrity_Work_Ethics_4===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_4');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_4=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_4_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_4=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_4_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_4=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_4_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_4=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_4_Champion").checked=true;
				    	
			    	}  
			    	//5
					if(obj.Integrity_Work_Ethics_5===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_5');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_5=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_5_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_5=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_5_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_5=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_5_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_5=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_5_Champion").checked=true;
				    	
			    	}     
			    	//6
					if(obj.Integrity_Work_Ethics_6===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_6');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_6=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_6_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_6=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_6_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_6=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_6_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_6=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_6_Champion").checked=true;
				    	
			    	}    
			    	//7
					if(obj.Integrity_Work_Ethics_7===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_7');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_7=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_7_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_7=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_7_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_7=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_7_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_7=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_7_Champion").checked=true;
				    	
			    	}  
			    	//8
					if(obj.Integrity_Work_Ethics_8===undefined){
			    		var node_list = document.getElementsByName('Integrity_Work_Ethics_8');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Integrity_Work_Ethics_8=="Knowledgeable") 
					    	  document.getElementById("Integrity_Work_Ethics_8_Knowledgeable").checked=true;
			    		else if(obj.Integrity_Work_Ethics_8=="Practitioner")
						    	  document.getElementById("Integrity_Work_Ethics_8_Practitioner").checked=true;
			    		else if(obj.Integrity_Work_Ethics_8=="Proficient")
						    	  document.getElementById("Integrity_Work_Ethics_8_Proficient").checked=true;
			    		else if(obj.Integrity_Work_Ethics_8=="Champion")
						    	  document.getElementById("Integrity_Work_Ethics_8_Champion").checked=true;
				    	
			    	}  
			
		        

		        <!-- Integrity & Work column End -- >		



		        <!--  Dependable & Accountable	Start -- >	
					//1
		    	if(obj.Dependable_Accountable_1===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_1=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_1_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_1=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_1_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_1=="Proficient")
					    	  document.getElementById("Dependable_Accountable_1_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_1=="Champion")
					    	  document.getElementById("Dependable_Accountable_1_Champion").checked=true;
			    	
		    	} 
		    	//2
		    	if(obj.Dependable_Accountable_2===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_2=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_2_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_2=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_2_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_2=="Proficient")
					    	  document.getElementById("Dependable_Accountable_2_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_2=="Champion")
					    	  document.getElementById("Dependable_Accountable_2_Champion").checked=true;
			    	
		    	} 
				//3
		     	if(obj.Dependable_Accountable_3===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_3=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_3_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_3=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_3_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_3=="Proficient")
					    	  document.getElementById("Dependable_Accountable_3_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_3=="Champion")
					    	  document.getElementById("Dependable_Accountable_3_Champion").checked=true;
			    	
		    	}

			//4
		      	if(obj.Dependable_Accountable_4===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_4=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_4_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_4=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_4_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_4=="Proficient")
					    	  document.getElementById("Dependable_Accountable_4_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_4=="Champion")
					    	  document.getElementById("Dependable_Accountable_4_Champion").checked=true;
			    	
		    	}
		    //5
		    
		    	if(obj.Dependable_Accountable_5===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_5=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_5_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_5=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_5_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_5=="Proficient")
					    	  document.getElementById("Dependable_Accountable_5_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_5=="Champion")
					    	  document.getElementById("Dependable_Accountable_5_Champion").checked=true;
			    	
		    	}	
		    	//6
		    	
		    	if(obj.Dependable_Accountable_6===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_6=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_6_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_6=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_6_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_6=="Proficient")
					    	  document.getElementById("Dependable_Accountable_6_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_6=="Champion")
					    	  document.getElementById("Dependable_Accountable_6_Champion").checked=true;
			    	
		    	}


				//7
		    	if(obj.Dependable_Accountable_7===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_7=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_7_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_7=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_7_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_7=="Proficient")
					    	  document.getElementById("Dependable_Accountable_7_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_7=="Champion")
					    	  document.getElementById("Dependable_Accountable_7_Champion").checked=true;
			    	
		    	}
		    	//8
		    	if(obj.Dependable_Accountable_8===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_8=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_8_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_8=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_8_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_8=="Proficient")
					    	  document.getElementById("Dependable_Accountable_8_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_8=="Champion")
					    	  document.getElementById("Dependable_Accountable_8_Champion").checked=true;
			    	
		    	}

		    	//9
		    	if(obj.Dependable_Accountable_9===undefined){
		    		var node_list = document.getElementsByName('Dependable_Accountable_9');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Dependable_Accountable_9=="Knowledgeable") 
				    	  document.getElementById("Dependable_Accountable_9_Knowledgeable").checked=true;
		    		else if(obj.Dependable_Accountable_9=="Practitioner")
					    	  document.getElementById("Dependable_Accountable_9_Practitioner").checked=true;
		    		else if(obj.Dependable_Accountable_9=="Proficient")
					    	  document.getElementById("Dependable_Accountable_9_Proficient").checked=true;
		    		else if(obj.Dependable_Accountable_9=="Champion")
					    	  document.getElementById("Dependable_Accountable_9_Champion").checked=true;
			    	
		    	}
		        <!--  Dependable & Accountable	End -- >	

		        <!--   Team Collaboration	Start -- >	
		        if(obj.Team_Collaboration_1===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_1=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_1_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_1=="Practitioner")
					    	  document.getElementById("Team_Collaboration_1_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_1=="Proficient")
					    	  document.getElementById("Team_Collaboration_1_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_1=="Champion")
					    	  document.getElementById("Team_Collaboration_1_Champion").checked=true;
			    	
		    	}

		    	//2
		        if(obj.Team_Collaboration_2===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_2=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_2_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_2=="Practitioner")
					    	  document.getElementById("Team_Collaboration_2_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_2=="Proficient")
					    	  document.getElementById("Team_Collaboration_2_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_2=="Champion")
					    	  document.getElementById("Team_Collaboration_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Team_Collaboration_3===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_3=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_3_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_3=="Practitioner")
					    	  document.getElementById("Team_Collaboration_3_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_3=="Proficient")
					    	  document.getElementById("Team_Collaboration_3_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_3=="Champion")
					    	  document.getElementById("Team_Collaboration_3_Champion").checked=true;
			    	
		    	}
				//4
		        if(obj.Team_Collaboration_4===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_4=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_4_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_4=="Practitioner")
					    	  document.getElementById("Team_Collaboration_4_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_4=="Proficient")
					    	  document.getElementById("Team_Collaboration_4_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_4=="Champion")
					    	  document.getElementById("Team_Collaboration_4_Champion").checked=true;
			    	
		    	}
		    	//5
		    	
		        if(obj.Team_Collaboration_5===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_5=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_5_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_5=="Practitioner")
					    	  document.getElementById("Team_Collaboration_5_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_5=="Proficient")
					    	  document.getElementById("Team_Collaboration_5_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_5=="Champion")
					    	  document.getElementById("Team_Collaboration_5_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.Team_Collaboration_6===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_6=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_6_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_6=="Practitioner")
					    	  document.getElementById("Team_Collaboration_6_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_6=="Proficient")
					    	  document.getElementById("Team_Collaboration_6_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_6=="Champion")
					    	  document.getElementById("Team_Collaboration_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Team_Collaboration_7===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_7=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_7_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_7=="Practitioner")
					    	  document.getElementById("Team_Collaboration_7_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_7=="Proficient")
					    	  document.getElementById("Team_Collaboration_7_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_7=="Champion")
					    	  document.getElementById("Team_Collaboration_7_Champion").checked=true;
			    	
		    	}
		    	//8
		        if(obj.Team_Collaboration_8===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_8=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_8_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_8=="Practitioner")
					    	  document.getElementById("Team_Collaboration_8_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_8=="Proficient")
					    	  document.getElementById("Team_Collaboration_8_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_8=="Champion")
					    	  document.getElementById("Team_Collaboration_8_Champion").checked=true;
			    	
		    	}
		    	//9
		        if(obj.Team_Collaboration_9===undefined){
		    		var node_list = document.getElementsByName('Team_Collaboration_9');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Team_Collaboration_9=="Knowledgeable") 
				    	      document.getElementById("Team_Collaboration_9_Knowledgeable").checked=true;
		    		else if(obj.Team_Collaboration_9=="Practitioner")
					    	  document.getElementById("Team_Collaboration_9_Practitioner").checked=true;
		    		else if(obj.Team_Collaboration_9=="Proficient")
					    	  document.getElementById("Team_Collaboration_9_Proficient").checked=true;
		    		else if(obj.Team_Collaboration_9=="Champion")
					    	  document.getElementById("Team_Collaboration_9_Champion").checked=true;
			    	
		    	}
		        <!--   Team Collaboration	End -- >	


		        <!--    Influencing Ability	Start -- >	
				//1
		        if(obj.Influencing_Ability_1===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_1=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_1_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_1=="Practitioner")
					    	  document.getElementById("Influencing_Ability_1_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_1=="Proficient")
					    	  document.getElementById("Influencing_Ability_1_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_1=="Champion")
					    	  document.getElementById("Influencing_Ability_1_Champion").checked=true;
			    	
		    	}
		    	//2
		        if(obj.Influencing_Ability_2===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_2=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_2_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_2=="Practitioner")
					    	  document.getElementById("Influencing_Ability_2_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_2=="Proficient")
					    	  document.getElementById("Influencing_Ability_2_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_2=="Champion")
					    	  document.getElementById("Influencing_Ability_2_Champion").checked=true;
			    	
		    	}
				//3
				
		        if(obj.Influencing_Ability_3===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_3=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_3_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_3=="Practitioner")
					    	  document.getElementById("Influencing_Ability_3_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_3=="Proficient")
					    	  document.getElementById("Influencing_Ability_3_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_3=="Champion")
					    	  document.getElementById("Influencing_Ability_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Influencing_Ability_4===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_4=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_4_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_4=="Practitioner")
					    	  document.getElementById("Influencing_Ability_4_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_4=="Proficient")
					    	  document.getElementById("Influencing_Ability_4_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_4=="Champion")
					    	  document.getElementById("Influencing_Ability_4_Champion").checked=true;
			    	
		    	}
				//5
		        if(obj.Influencing_Ability_5===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_5=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_5_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_5=="Practitioner")
					    	  document.getElementById("Influencing_Ability_5_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_5=="Proficient")
					    	  document.getElementById("Influencing_Ability_5_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_5=="Champion")
					    	  document.getElementById("Influencing_Ability_5_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.Influencing_Ability_6===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_6=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_6_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_6=="Practitioner")
					    	  document.getElementById("Influencing_Ability_6_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_6=="Proficient")
					    	  document.getElementById("Influencing_Ability_6_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_6=="Champion")
					    	  document.getElementById("Influencing_Ability_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Influencing_Ability_7===undefined){
		    		var node_list = document.getElementsByName('Influencing_Ability_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Influencing_Ability_7=="Knowledgeable") 
				    	      document.getElementById("Influencing_Ability_7_Knowledgeable").checked=true;
		    		else if(obj.Influencing_Ability_7=="Practitioner")
					    	  document.getElementById("Influencing_Ability_7_Practitioner").checked=true;
		    		else if(obj.Influencing_Ability_7=="Proficient")
					    	  document.getElementById("Influencing_Ability_7_Proficient").checked=true;
		    		else if(obj.Influencing_Ability_7=="Champion")
					    	  document.getElementById("Influencing_Ability_7_Champion").checked=true;
			    	
		    	}
		        <!--    Influencing Ability	End -- >	

		        <!--     Coaching & Mentoring Start -- >

		        //1
		        if(obj.Coaching_Mentoring_1===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_1=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_1_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_1=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_1_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_1=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_1_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_1=="Champion")
					    	  document.getElementById("Coaching_Mentoring_1_Champion").checked=true;
			    	
		    	}

		        //2
		        if(obj.Coaching_Mentoring_2===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_2=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_2_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_2=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_2_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_2=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_2_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_2=="Champion")
					    	  document.getElementById("Coaching_Mentoring_2_Champion").checked=true;
			    	
		    	}
		       //3
		        if(obj.Coaching_Mentoring_3===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_3=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_3_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_3=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_3_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_3=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_3_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_3=="Champion")
					    	  document.getElementById("Coaching_Mentoring_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Coaching_Mentoring_4===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_4=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_4_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_4=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_4_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_4=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_4_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_4=="Champion")
					    	  document.getElementById("Coaching_Mentoring_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Coaching_Mentoring_5===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_5=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_5_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_5=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_5_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_5=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_5_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_5=="Champion")
					    	  document.getElementById("Coaching_Mentoring_5_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.Coaching_Mentoring_6===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_6=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_6_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_6=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_6_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_6=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_6_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_6=="Champion")
					    	  document.getElementById("Coaching_Mentoring_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Coaching_Mentoring_7===undefined){
		    		var node_list = document.getElementsByName('Coaching_Mentoring_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Coaching_Mentoring_7=="Knowledgeable") 
				    	      document.getElementById("Coaching_Mentoring_7_Knowledgeable").checked=true;
		    		else if(obj.Coaching_Mentoring_7=="Practitioner")
					    	  document.getElementById("Coaching_Mentoring_7_Practitioner").checked=true;
		    		else if(obj.Coaching_Mentoring_7=="Proficient")
					    	  document.getElementById("Coaching_Mentoring_7_Proficient").checked=true;
		    		else if(obj.Coaching_Mentoring_7=="Champion")
					    	  document.getElementById("Coaching_Mentoring_7_Champion").checked=true;
			    	
		    	}
		        <!--     Coaching & Mentoring End -- >

		        <!--     Innovation Start -- >


		        if(obj.Innovation_1===undefined){
		    		var node_list = document.getElementsByName('Innovation_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_1=="Knowledgeable") 
				    	      document.getElementById("Innovation_1_Knowledgeable").checked=true;
		    		else if(obj.Innovation_1=="Practitioner")
					    	  document.getElementById("Innovation_1_Practitioner").checked=true;
		    		else if(obj.Innovation_1=="Proficient")
					    	  document.getElementById("Innovation_1_Proficient").checked=true;
		    		else if(obj.Innovation_1=="Champion")
					    	  document.getElementById("Innovation_1_Champion").checked=true;
			    	
		    	}
		        //2
		        
		        if(obj.Innovation_2===undefined){
		    		var node_list = document.getElementsByName('Innovation_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_2=="Knowledgeable") 
				    	      document.getElementById("Innovation_2_Knowledgeable").checked=true;
		    		else if(obj.Innovation_2=="Practitioner")
					    	  document.getElementById("Innovation_2_Practitioner").checked=true;
		    		else if(obj.Innovation_2=="Proficient")
					    	  document.getElementById("Innovation_2_Proficient").checked=true;
		    		else if(obj.Innovation_2=="Champion")
					    	  document.getElementById("Innovation_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Innovation_3===undefined){
		    		var node_list = document.getElementsByName('Innovation_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_3=="Knowledgeable") 
				    	      document.getElementById("Innovation_3_Knowledgeable").checked=true;
		    		else if(obj.Innovation_3=="Practitioner")
					    	  document.getElementById("Innovation_3_Practitioner").checked=true;
		    		else if(obj.Innovation_3=="Proficient")
					    	  document.getElementById("Innovation_3_Proficient").checked=true;
		    		else if(obj.Innovation_3=="Champion")
					    	  document.getElementById("Innovation_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Innovation_4===undefined){
		    		var node_list = document.getElementsByName('Innovation_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_4=="Knowledgeable") 
				    	      document.getElementById("Innovation_4_Knowledgeable").checked=true;
		    		else if(obj.Innovation_4=="Practitioner")
					    	  document.getElementById("Innovation_4_Practitioner").checked=true;
		    		else if(obj.Innovation_4=="Proficient")
					    	  document.getElementById("Innovation_4_Proficient").checked=true;
		    		else if(obj.Innovation_4=="Champion")
					    	  document.getElementById("Innovation_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Innovation_5===undefined){
		    		var node_list = document.getElementsByName('Innovation_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_5=="Knowledgeable") 
				    	      document.getElementById("Innovation_5_Knowledgeable").checked=true;
		    		else if(obj.Innovation_5=="Practitioner")
					    	  document.getElementById("Innovation_5_Practitioner").checked=true;
		    		else if(obj.Innovation_5=="Proficient")
					    	  document.getElementById("Innovation_5_Proficient").checked=true;
		    		else if(obj.Innovation_5=="Champion")
					    	  document.getElementById("Innovation_5_Champion").checked=true;
			    	
		    	}
		    	//6
		    	
		        if(obj.Innovation_6===undefined){
		    		var node_list = document.getElementsByName('Innovation_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_6=="Knowledgeable") 
				    	      document.getElementById("Innovation_6_Knowledgeable").checked=true;
		    		else if(obj.Innovation_6=="Practitioner")
					    	  document.getElementById("Innovation_6_Practitioner").checked=true;
		    		else if(obj.Innovation_6=="Proficient")
					    	  document.getElementById("Innovation_6_Proficient").checked=true;
		    		else if(obj.Innovation_6=="Champion")
					    	  document.getElementById("Innovation_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Innovation_7===undefined){
		    		var node_list = document.getElementsByName('Innovation_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_7=="Knowledgeable") 
				    	      document.getElementById("Innovation_7_Knowledgeable").checked=true;
		    		else if(obj.Innovation_7=="Practitioner")
					    	  document.getElementById("Innovation_7_Practitioner").checked=true;
		    		else if(obj.Innovation_7=="Proficient")
					    	  document.getElementById("Innovation_7_Proficient").checked=true;
		    		else if(obj.Innovation_7=="Champion")
					    	  document.getElementById("Innovation_7_Champion").checked=true;
			    	
		    	}
		    	//8
		        if(obj.Innovation_8===undefined){
		    		var node_list = document.getElementsByName('Innovation_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_8=="Knowledgeable") 
				    	      document.getElementById("Innovation_8_Knowledgeable").checked=true;
		    		else if(obj.Innovation_8=="Practitioner")
					    	  document.getElementById("Innovation_8_Practitioner").checked=true;
		    		else if(obj.Innovation_8=="Proficient")
					    	  document.getElementById("Innovation_8_Proficient").checked=true;
		    		else if(obj.Innovation_8=="Champion")
					    	  document.getElementById("Innovation_8_Champion").checked=true;
			    	
		    	}
		    	//9
		        if(obj.Innovation_9===undefined){
		    		var node_list = document.getElementsByName('Innovation_9');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Innovation_9=="Knowledgeable") 
				    	      document.getElementById("Innovation_9_Knowledgeable").checked=true;
		    		else if(obj.Innovation_9=="Practitioner")
					    	  document.getElementById("Innovation_9_Practitioner").checked=true;
		    		else if(obj.Innovation_9=="Proficient")
					    	  document.getElementById("Innovation_9_Proficient").checked=true;
		    		else if(obj.Innovation_9=="Champion")
					    	  document.getElementById("Innovation_9_Champion").checked=true;
			    	
		    	}
		        <!--     Innovation End -- >

		        <!--     Initiative & tenacity Start -- >

		        if(obj.Initiative_tenacity_1===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_1=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_1_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_1=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_1_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_1=="Proficient")
					    	  document.getElementById("Initiative_tenacity_1_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_1=="Champion")
					    	  document.getElementById("Initiative_tenacity_1_Champion").checked=true;
			    	
		    	}
				//2
				
		        if(obj.Initiative_tenacity_2===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_2=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_2_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_2=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_2_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_2=="Proficient")
					    	  document.getElementById("Initiative_tenacity_2_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_2=="Champion")
					    	  document.getElementById("Initiative_tenacity_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Initiative_tenacity_3===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_3=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_3_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_3=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_3_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_3=="Proficient")
					    	  document.getElementById("Initiative_tenacity_3_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_3=="Champion")
					    	  document.getElementById("Initiative_tenacity_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Initiative_tenacity_4===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_4=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_4_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_4=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_4_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_4=="Proficient")
					    	  document.getElementById("Initiative_tenacity_4_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_4=="Champion")
					    	  document.getElementById("Initiative_tenacity_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Initiative_tenacity_5===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_5=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_5_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_5=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_5_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_5=="Proficient")
					    	  document.getElementById("Initiative_tenacity_5_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_5=="Champion")
					    	  document.getElementById("Initiative_tenacity_5_Champion").checked=true;
			    	
		    	}
		    	//5
		    	
		        if(obj.Initiative_tenacity_6===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_6=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_6_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_6=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_6_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_6=="Proficient")
					    	  document.getElementById("Initiative_tenacity_6_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_6=="Champion")
					    	  document.getElementById("Initiative_tenacity_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Initiative_tenacity_7===undefined){
		    		var node_list = document.getElementsByName('Initiative_tenacity_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Initiative_tenacity_7=="Knowledgeable") 
				    	      document.getElementById("Initiative_tenacity_7_Knowledgeable").checked=true;
		    		else if(obj.Initiative_tenacity_7=="Practitioner")
					    	  document.getElementById("Initiative_tenacity_7_Practitioner").checked=true;
		    		else if(obj.Initiative_tenacity_7=="Proficient")
					    	  document.getElementById("Initiative_tenacity_7_Proficient").checked=true;
		    		else if(obj.Initiative_tenacity_7=="Champion")
					    	  document.getElementById("Initiative_tenacity_7_Champion").checked=true;
			    	
		    	}
		    	
		        <!--     Initiative & tenacity End -- >

		        
		        <!--     Outcome Orientation start -- >

			//1
		        if(obj.Outcome_Orientation_1===undefined){
		    		var node_list = document.getElementsByName('Outcome_Orientation_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Outcome_Orientation_1=="Knowledgeable") 
				    	      document.getElementById("Outcome_Orientation_1_Knowledgeable").checked=true;
		    		else if(obj.Outcome_Orientation_1=="Practitioner")
					    	  document.getElementById("Outcome_Orientation_1_Practitioner").checked=true;
		    		else if(obj.Outcome_Orientation_1=="Proficient")
					    	  document.getElementById("Outcome_Orientation_1_Proficient").checked=true;
		    		else if(obj.Outcome_Orientation_1=="Champion")
					    	  document.getElementById("Outcome_Orientation_1_Champion").checked=true;
			    	
		    	}

		    	//2
		        if(obj.Outcome_Orientation_2===undefined){
		    		var node_list = document.getElementsByName('Outcome_Orientation_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Outcome_Orientation_2=="Knowledgeable") 
				    	      document.getElementById("Outcome_Orientation_2_Knowledgeable").checked=true;
		    		else if(obj.Outcome_Orientation_2=="Practitioner")
					    	  document.getElementById("Outcome_Orientation_2_Practitioner").checked=true;
		    		else if(obj.Outcome_Orientation_2=="Proficient")
					    	  document.getElementById("Outcome_Orientation_2_Proficient").checked=true;
		    		else if(obj.Outcome_Orientation_2=="Champion")
					    	  document.getElementById("Outcome_Orientation_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Outcome_Orientation_3===undefined){
		    		var node_list = document.getElementsByName('Outcome_Orientation_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Outcome_Orientation_3=="Knowledgeable") 
				    	      document.getElementById("Outcome_Orientation_3_Knowledgeable").checked=true;
		    		else if(obj.Outcome_Orientation_3=="Practitioner")
					    	  document.getElementById("Outcome_Orientation_3_Practitioner").checked=true;
		    		else if(obj.Outcome_Orientation_3=="Proficient")
					    	  document.getElementById("Outcome_Orientation_3_Proficient").checked=true;
		    		else if(obj.Outcome_Orientation_3=="Champion")
					    	  document.getElementById("Outcome_Orientation_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Outcome_Orientation_4===undefined){
		    		var node_list = document.getElementsByName('Outcome_Orientation_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Outcome_Orientation_4=="Knowledgeable") 
				    	      document.getElementById("Outcome_Orientation_4_Knowledgeable").checked=true;
		    		else if(obj.Outcome_Orientation_4=="Practitioner")
					    	  document.getElementById("Outcome_Orientation_4_Practitioner").checked=true;
		    		else if(obj.Outcome_Orientation_4=="Proficient")
					    	  document.getElementById("Outcome_Orientation_4_Proficient").checked=true;
		    		else if(obj.Outcome_Orientation_4=="Champion")
					    	  document.getElementById("Outcome_Orientation_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Outcome_Orientation_5===undefined){
		    		var node_list = document.getElementsByName('Outcome_Orientation_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Outcome_Orientation_5=="Knowledgeable") 
				    	      document.getElementById("Outcome_Orientation_5_Knowledgeable").checked=true;
		    		else if(obj.Outcome_Orientation_5=="Practitioner")
					    	  document.getElementById("Outcome_Orientation_5_Practitioner").checked=true;
		    		else if(obj.Outcome_Orientation_5=="Proficient")
					    	  document.getElementById("Outcome_Orientation_5_Proficient").checked=true;
		    		else if(obj.Outcome_Orientation_5=="Champion")
					    	  document.getElementById("Outcome_Orientation_5_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.Outcome_Orientation_6===undefined){
		    		var node_list = document.getElementsByName('Outcome_Orientation_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Outcome_Orientation_6=="Knowledgeable") 
				    	      document.getElementById("Outcome_Orientation_6_Knowledgeable").checked=true;
		    		else if(obj.Outcome_Orientation_6=="Practitioner")
					    	  document.getElementById("Outcome_Orientation_6_Practitioner").checked=true;
		    		else if(obj.Outcome_Orientation_6=="Proficient")
					    	  document.getElementById("Outcome_Orientation_6_Proficient").checked=true;
		    		else if(obj.Outcome_Orientation_6=="Champion")
					    	  document.getElementById("Outcome_Orientation_6_Champion").checked=true;
			    	
		    	}

		        <!--     Outcome Orientation end -- >

		        
		        <!-- Cultural Agility Start --- >

		        if(obj.Cultural_Agility_1===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_1=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_1_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_1=="Practitioner")
					    	  document.getElementById("Cultural_Agility_1_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_1=="Proficient")
					    	  document.getElementById("Cultural_Agility_1_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_1=="Champion")
					    	  document.getElementById("Cultural_Agility_1_Champion").checked=true;
			    	
		    	}
				//2
		        if(obj.Cultural_Agility_2===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_2=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_2_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_2=="Practitioner")
					    	  document.getElementById("Cultural_Agility_2_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_2=="Proficient")
					    	  document.getElementById("Cultural_Agility_2_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_2=="Champion")
					    	  document.getElementById("Cultural_Agility_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Cultural_Agility_3===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_3=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_3_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_3=="Practitioner")
					    	  document.getElementById("Cultural_Agility_3_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_3=="Proficient")
					    	  document.getElementById("Cultural_Agility_3_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_3=="Champion")
					    	  document.getElementById("Cultural_Agility_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Cultural_Agility_4===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_4=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_4_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_4=="Practitioner")
					    	  document.getElementById("Cultural_Agility_4_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_4=="Proficient")
					    	  document.getElementById("Cultural_Agility_4_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_4=="Champion")
					    	  document.getElementById("Cultural_Agility_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Cultural_Agility_5===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_5=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_5_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_5=="Practitioner")
					    	  document.getElementById("Cultural_Agility_5_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_5=="Proficient")
					    	  document.getElementById("Cultural_Agility_5_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_5=="Champion")
					    	  document.getElementById("Cultural_Agility_5_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.Cultural_Agility_6===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_6=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_6_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_6=="Practitioner")
					    	  document.getElementById("Cultural_Agility_6_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_6=="Proficient")
					    	  document.getElementById("Cultural_Agility_6_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_6=="Champion")
					    	  document.getElementById("Cultural_Agility_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Cultural_Agility_7===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_7=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_7_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_7=="Practitioner")
					    	  document.getElementById("Cultural_Agility_7_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_7=="Proficient")
					    	  document.getElementById("Cultural_Agility_7_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_7=="Champion")
					    	  document.getElementById("Cultural_Agility_7_Champion").checked=true;
			    	
		    	}
		    	//8
		        if(obj.Cultural_Agility_8===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_8=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_8_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_8=="Practitioner")
					    	  document.getElementById("Cultural_Agility_8_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_8=="Proficient")
					    	  document.getElementById("Cultural_Agility_8_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_8=="Champion")
					    	  document.getElementById("Cultural_Agility_8_Champion").checked=true;
			    	
		    	}
		    	//9
		        if(obj.Cultural_Agility_9===undefined){
		    		var node_list = document.getElementsByName('Cultural_Agility_9');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Cultural_Agility_9=="Knowledgeable") 
				    	      document.getElementById("Cultural_Agility_9_Knowledgeable").checked=true;
		    		else if(obj.Cultural_Agility_9=="Practitioner")
					    	  document.getElementById("Cultural_Agility_9_Practitioner").checked=true;
		    		else if(obj.Cultural_Agility_9=="Proficient")
					    	  document.getElementById("Cultural_Agility_9_Proficient").checked=true;
		    		else if(obj.Cultural_Agility_9=="Champion")
					    	  document.getElementById("Cultural_Agility_9_Champion").checked=true;
			    	
		    	}
		        <!--- Cultural Agility End ----->

	
		        <!---- People Leadership start ----->
		        //1
		        if(obj.People_Leadership_1===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_1=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_1_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_1=="Practitioner")
					    	  document.getElementById("People_Leadership_1_Practitioner").checked=true;
		    		else if(obj.People_Leadership_1=="Proficient")
					    	  document.getElementById("People_Leadership_1_Proficient").checked=true;
		    		else if(obj.People_Leadership_1=="Champion")
					    	  document.getElementById("People_Leadership_1_Champion").checked=true;
			    	
		    	}
		    	//2
		        if(obj.People_Leadership_2===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_2=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_2_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_2=="Practitioner")
					    	  document.getElementById("People_Leadership_2_Practitioner").checked=true;
		    		else if(obj.People_Leadership_2=="Proficient")
					    	  document.getElementById("People_Leadership_2_Proficient").checked=true;
		    		else if(obj.People_Leadership_2=="Champion")
					    	  document.getElementById("People_Leadership_2_Champion").checked=true;
			    	
		    	}
				//3
		        if(obj.People_Leadership_3===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_3=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_3_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_3=="Practitioner")
					    	  document.getElementById("People_Leadership_3_Practitioner").checked=true;
		    		else if(obj.People_Leadership_3=="Proficient")
					    	  document.getElementById("People_Leadership_3_Proficient").checked=true;
		    		else if(obj.People_Leadership_3=="Champion")
					    	  document.getElementById("People_Leadership_3_Champion").checked=true;
			    	
		    	}

		        //4
		        if(obj.People_Leadership_4===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_4=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_4_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_4=="Practitioner")
					    	  document.getElementById("People_Leadership_4_Practitioner").checked=true;
		    		else if(obj.People_Leadership_4=="Proficient")
					    	  document.getElementById("People_Leadership_4_Proficient").checked=true;
		    		else if(obj.People_Leadership_4=="Champion")
					    	  document.getElementById("People_Leadership_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.People_Leadership_5===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_5=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_1_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_5=="Practitioner")
					    	  document.getElementById("People_Leadership_1_Practitioner").checked=true;
		    		else if(obj.People_Leadership_5=="Proficient")
					    	  document.getElementById("People_Leadership_1_Proficient").checked=true;
		    		else if(obj.People_Leadership_5=="Champion")
					    	  document.getElementById("People_Leadership_1_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.People_Leadership_6===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_6=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_6_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_6=="Practitioner")
					    	  document.getElementById("People_Leadership_6_Practitioner").checked=true;
		    		else if(obj.People_Leadership_6=="Proficient")
					    	  document.getElementById("People_Leadership_6_Proficient").checked=true;
		    		else if(obj.People_Leadership_6=="Champion")
					    	  document.getElementById("People_Leadership_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.People_Leadership_7===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_7=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_7_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_7=="Practitioner")
					    	  document.getElementById("People_Leadership_7_Practitioner").checked=true;
		    		else if(obj.People_Leadership_7=="Proficient")
					    	  document.getElementById("People_Leadership_7_Proficient").checked=true;
		    		else if(obj.People_Leadership_7=="Champion")
					    	  document.getElementById("People_Leadership_7_Champion").checked=true;
			    	
		    	}
		    	//8
		        if(obj.People_Leadership_8===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_8=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_8_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_8=="Practitioner")
					    	  document.getElementById("People_Leadership_8_Practitioner").checked=true;
		    		else if(obj.People_Leadership_8=="Proficient")
					    	  document.getElementById("People_Leadership_8_Proficient").checked=true;
		    		else if(obj.People_Leadership_8=="Champion")
					    	  document.getElementById("People_Leadership_8_Champion").checked=true;
			    	
		    	}
		    	//9
		        if(obj.People_Leadership_9===undefined){
		    		var node_list = document.getElementsByName('People_Leadership_9');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.People_Leadership_9=="Knowledgeable") 
				    	      document.getElementById("People_Leadership_9_Knowledgeable").checked=true;
		    		else if(obj.People_Leadership_9=="Practitioner")
					    	  document.getElementById("People_Leadership_9_Practitioner").checked=true;
		    		else if(obj.People_Leadership_9=="Proficient")
					    	  document.getElementById("People_Leadership_9_Proficient").checked=true;
		    		else if(obj.People_Leadership_9=="Champion")
					    	  document.getElementById("People_Leadership_9_Champion").checked=true;
			    	
		    	}


		      
		        <!---- People Leadership End ----->

		        <!----  Execution_Excellence Start ----->

		        if(obj.Execution_Excellence_1===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_1=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_1_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_1=="Practitioner")
					    	  document.getElementById("Execution_Excellence_1_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_1=="Proficient")
					    	  document.getElementById("Execution_Excellence_1_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_1=="Champion")
					    	  document.getElementById("Execution_Excellence_1_Champion").checked=true;
			    	
		    	}

		    	//2
		    	
		        if(obj.Execution_Excellence_2===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_2=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_2_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_2=="Practitioner")
					    	  document.getElementById("Execution_Excellence_2_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_2=="Proficient")
					    	  document.getElementById("Execution_Excellence_2_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_2=="Champion")
					    	  document.getElementById("Execution_Excellence_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Execution_Excellence_3===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_3=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_3_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_3=="Practitioner")
					    	  document.getElementById("Execution_Excellence_3_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_3=="Proficient")
					    	  document.getElementById("Execution_Excellence_3_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_3=="Champion")
					    	  document.getElementById("Execution_Excellence_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Execution_Excellence_4===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_4=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_4_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_4=="Practitioner")
					    	  document.getElementById("Execution_Excellence_4_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_4=="Proficient")
					    	  document.getElementById("Execution_Excellence_4_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_4=="Champion")
					    	  document.getElementById("Execution_Excellence_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Execution_Excellence_5===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_5=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_5_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_5=="Practitioner")
					    	  document.getElementById("Execution_Excellence_5_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_5=="Proficient")
					    	  document.getElementById("Execution_Excellence_5_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_5=="Champion")
					    	  document.getElementById("Execution_Excellence_5_Champion").checked=true;
			    	
		    	}
				//6
		        if(obj.Execution_Excellence_6===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_6=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_6_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_6=="Practitioner")
					    	  document.getElementById("Execution_Excellence_6_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_6=="Proficient")
					    	  document.getElementById("Execution_Excellence_6_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_6=="Champion")
					    	  document.getElementById("Execution_Excellence_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Execution_Excellence_7===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_7=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_7_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_7=="Practitioner")
					    	  document.getElementById("Execution_Excellence_7_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_7=="Proficient")
					    	  document.getElementById("Execution_Excellence_7_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_7=="Champion")
					    	  document.getElementById("Execution_Excellence_7_Champion").checked=true;
			    	
		    	}
		    	//8
		        if(obj.Execution_Excellence_8===undefined){
		    		var node_list = document.getElementsByName('Execution_Excellence_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Execution_Excellence_8=="Knowledgeable") 
				    	      document.getElementById("Execution_Excellence_8_Knowledgeable").checked=true;
		    		else if(obj.Execution_Excellence_8=="Practitioner")
					    	  document.getElementById("Execution_Excellence_8_Practitioner").checked=true;
		    		else if(obj.Execution_Excellence_8=="Proficient")
					    	  document.getElementById("Execution_Excellence_8_Proficient").checked=true;
		    		else if(obj.Execution_Excellence_8=="Champion")
					    	  document.getElementById("Execution_Excellence_8_Champion").checked=true;
			    	
		    	}
		    	 
		    	 
		        <!----  Execution_Excellence End ----->

		       
		        <!---- start Commercial, Financial Acumen & Business Acument ----->
		        
		        if(obj.Commercial_Financial_Acumen_1===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_1=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_1_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_1=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_1_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_1=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_1_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_1=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_1_Champion").checked=true;
			    	
		    	}
		    	//2
		    	
		        if(obj.Commercial_Financial_Acumen_2===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_2=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_2_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_2=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_2_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_2=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_2_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_2=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_2_Champion").checked=true;
			    	
		    	}
		    	//3
		    	
		        if(obj.Commercial_Financial_Acumen_3===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_3=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_3_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_3=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_3_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_3=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_3_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_3=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_3_Champion").checked=true;
			    	
		    	}
		    	//4
		    	
		        if(obj.Commercial_Financial_Acumen_4===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_4=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_4_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_4=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_4_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_4=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_4_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_4=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Commercial_Financial_Acumen_5===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_5=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_5_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_5=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_5_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_5=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_5_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_5=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_5_Champion").checked=true;
			    	
		    	}
		    	//6
		        if(obj.Commercial_Financial_Acumen_6===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_6=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_6_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_6=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_6_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_6=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_6_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_6=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_6_Champion").checked=true;
			    	
		    	}
			//7
		        if(obj.Commercial_Financial_Acumen_7===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_7=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_7_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_7=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_7_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_7=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_7_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_7=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_7_Champion").checked=true;
			    	
		    	}
		    	//8
		        if(obj.Commercial_Financial_Acumen_8===undefined){
		    		var node_list = document.getElementsByName('Commercial_Financial_Acumen_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Commercial_Financial_Acumen_8=="Knowledgeable") 
				    	      document.getElementById("Commercial_Financial_Acumen_8_Knowledgeable").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_8=="Practitioner")
					    	  document.getElementById("Commercial_Financial_Acumen_8_Practitioner").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_8=="Proficient")
					    	  document.getElementById("Commercial_Financial_Acumen_8_Proficient").checked=true;
		    		else if(obj.Commercial_Financial_Acumen_8=="Champion")
					    	  document.getElementById("Commercial_Financial_Acumen_8_Champion").checked=true;
			    	
		    	}
		        <!---- End Commercial, Financial Acumen & Business Acument ----->


		        <!--- Sales & Brand Promotion ------->
//1
		        if(obj.Sales_Brand_Promotion_1===undefined){
		    		var node_list = document.getElementsByName('Sales_Brand_Promotion_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Sales_Brand_Promotion_1=="Knowledgeable") 
				    	      document.getElementById("Sales_Brand_Promotion_1_Knowledgeable").checked=true;
		    		else if(obj.Sales_Brand_Promotion_1=="Practitioner")
					    	  document.getElementById("Sales_Brand_Promotion_1_Practitioner").checked=true;
		    		else if(obj.Sales_Brand_Promotion_1=="Proficient")
					    	  document.getElementById("Sales_Brand_Promotion_1_Proficient").checked=true;
		    		else if(obj.Sales_Brand_Promotion_1=="Champion")
					    	  document.getElementById("Sales_Brand_Promotion_1_Champion").checked=true;
			    	
		    	}
//2

		        if(obj.Sales_Brand_Promotion_2===undefined){
		    		var node_list = document.getElementsByName('Sales_Brand_Promotion_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Sales_Brand_Promotion_2=="Knowledgeable") 
				    	      document.getElementById("Sales_Brand_Promotion_2_Knowledgeable").checked=true;
		    		else if(obj.Sales_Brand_Promotion_2=="Practitioner")
					    	  document.getElementById("Sales_Brand_Promotion_2_Practitioner").checked=true;
		    		else if(obj.Sales_Brand_Promotion_2=="Proficient")
					    	  document.getElementById("Sales_Brand_Promotion_2_Proficient").checked=true;
		    		else if(obj.Sales_Brand_Promotion_2=="Champion")
					    	  document.getElementById("Sales_Brand_Promotion_2_Champion").checked=true;
			    	
		    	}
		    	//3
		    	
		        if(obj.Sales_Brand_Promotion_3===undefined){
		    		var node_list = document.getElementsByName('Sales_Brand_Promotion_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Sales_Brand_Promotion_3=="Knowledgeable") 
				    	      document.getElementById("Sales_Brand_Promotion_3_Knowledgeable").checked=true;
		    		else if(obj.Sales_Brand_Promotion_3=="Practitioner")
					    	  document.getElementById("Sales_Brand_Promotion_3_Practitioner").checked=true;
		    		else if(obj.Sales_Brand_Promotion_3=="Proficient")
					    	  document.getElementById("Sales_Brand_Promotion_3_Proficient").checked=true;
		    		else if(obj.Sales_Brand_Promotion_3=="Champion")
					    	  document.getElementById("Sales_Brand_Promotion_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Sales_Brand_Promotion_4===undefined){
		    		var node_list = document.getElementsByName('Sales_Brand_Promotion_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Sales_Brand_Promotion_4=="Knowledgeable") 
				    	      document.getElementById("Sales_Brand_Promotion_4_Knowledgeable").checked=true;
		    		else if(obj.Sales_Brand_Promotion_4=="Practitioner")
					    	  document.getElementById("Sales_Brand_Promotion_4_Practitioner").checked=true;
		    		else if(obj.Sales_Brand_Promotion_4=="Proficient")
					    	  document.getElementById("Sales_Brand_Promotion_4_Proficient").checked=true;
		    		else if(obj.Sales_Brand_Promotion_4=="Champion")
					    	  document.getElementById("Sales_Brand_Promotion_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Sales_Brand_Promotion_5===undefined){
		    		var node_list = document.getElementsByName('Sales_Brand_Promotion_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Sales_Brand_Promotion_5=="Knowledgeable") 
				    	      document.getElementById("Sales_Brand_Promotion_5_Knowledgeable").checked=true;
		    		else if(obj.Sales_Brand_Promotion_5=="Practitioner")
					    	  document.getElementById("Sales_Brand_Promotion_5_Practitioner").checked=true;
		    		else if(obj.Sales_Brand_Promotion_5=="Proficient")
					    	  document.getElementById("Sales_Brand_Promotion_5_Proficient").checked=true;
		    		else if(obj.Sales_Brand_Promotion_5=="Champion")
					    	  document.getElementById("Sales_Brand_Promotion_5_Champion").checked=true;
			    	
		    	}
		        <!--- end Sales & Brand Promotion ------->

		        <!-------- Strategic Alignment -------->


		        if(obj.Strategic_Alignment_1===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_1=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_1_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_1=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_1_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_1=="Proficient")
					    	  document.getElementById("Strategic_Alignment_1_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_1=="Champion")
					    	  document.getElementById("Strategic_Alignment_1_Champion").checked=true;
			    	
		    	}
//2
			
		        if(obj.Strategic_Alignment_2===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_2=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_2_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_2=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_2_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_2=="Proficient")
					    	  document.getElementById("Strategic_Alignment_2_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_2=="Champion")
					    	  document.getElementById("Strategic_Alignment_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Strategic_Alignment_3===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_3=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_3_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_3=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_3_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_3=="Proficient")
					    	  document.getElementById("Strategic_Alignment_3_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_3=="Champion")
					    	  document.getElementById("Strategic_Alignment_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Strategic_Alignment_4===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_4=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_4_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_4=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_4_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_4=="Proficient")
					    	  document.getElementById("Strategic_Alignment_4_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_4=="Champion")
					    	  document.getElementById("Strategic_Alignment_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Strategic_Alignment_5===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_5=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_5_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_5=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_5_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_5=="Proficient")
					    	  document.getElementById("Strategic_Alignment_5_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_5=="Champion")
					    	  document.getElementById("Strategic_Alignment_5_Champion").checked=true;
			    	
		    	}
		    	//6
		    	
		        if(obj.Strategic_Alignment_6===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_6=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_6_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_6=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_6_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_6=="Proficient")
					    	  document.getElementById("Strategic_Alignment_6_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_6=="Champion")
					    	  document.getElementById("Strategic_Alignment_6_Champion").checked=true;
			    	
		    	}
		    	//7
		        if(obj.Strategic_Alignment_7===undefined){
		    		var node_list = document.getElementsByName('Strategic_Alignment_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_7=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_7_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_7=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_7_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_7=="Proficient")
					    	  document.getElementById("Strategic_Alignment_7_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_7=="Champion")
					    	  document.getElementById("Strategic_Alignment_7_Champion").checked=true;
			    	
		    	}
		    	//8
		    	  if(obj.Strategic_Alignment_8===undefined){
			    		var node_list = document.getElementsByName('Strategic_Alignment_8');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Strategic_Alignment_8=="Knowledgeable") 
				    	      document.getElementById("Strategic_Alignment_8_Knowledgeable").checked=true;
		    		else if(obj.Strategic_Alignment_8=="Practitioner")
					    	  document.getElementById("Strategic_Alignment_8_Practitioner").checked=true;
		    		else if(obj.Strategic_Alignment_8=="Proficient")
					    	  document.getElementById("Strategic_Alignment_8_Proficient").checked=true;
		    		else if(obj.Strategic_Alignment_8=="Champion")
					    	  document.getElementById("Strategic_Alignment_8_Champion").checked=true;
			    	
		    	}

			    	//9
		    	  if(obj.Strategic_Alignment_91===undefined){
			    		var node_list = document.getElementsByName('Strategic_Alignment_9');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Strategic_Alignment_9=="Knowledgeable") 
					    	      document.getElementById("Strategic_Alignment_9_Knowledgeable").checked=true;
			    		else if(obj.Strategic_Alignment_9=="Practitioner")
						    	  document.getElementById("Strategic_Alignment_9_Practitioner").checked=true;
			    		else if(obj.Strategic_Alignment_9=="Proficient")
						    	  document.getElementById("Strategic_Alignment_9_Proficient").checked=true;
			    		else if(obj.Strategic_Alignment_9=="Champion")
						    	  document.getElementById("Strategic_Alignment_9_Champion").checked=true;
				    	
			    	}
			    	//10
		    	  if(obj.Strategic_Alignment_10===undefined){
			    		var node_list = document.getElementsByName('Strategic_Alignment_10');
				    		for (var i = 0; i < node_list.length; i++) {
				    		    node_list[i].checked = false;
				    		}
			    	}
			    	else
				    {
			    		if(obj.Strategic_Alignment_10=="Knowledgeable") 
					    	      document.getElementById("Strategic_Alignment_10_Knowledgeable").checked=true;
			    		else if(obj.Strategic_Alignment_10=="Practitioner")
						    	  document.getElementById("Strategic_Alignment_10_Practitioner").checked=true;
			    		else if(obj.Strategic_Alignment_10=="Proficient")
						    	  document.getElementById("Strategic_Alignment_10_Proficient").checked=true;
			    		else if(obj.Strategic_Alignment_10=="Champion")
						    	  document.getElementById("Strategic_Alignment_10_Champion").checked=true;
				    	
			    	}

		        <!----- End Strategic Alignment------->



		        <!----- Foresight start------->


		        if(obj.Foresight_1===undefined){
		    		var node_list = document.getElementsByName('Foresight_1');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_1=="Knowledgeable") 
				    	      document.getElementById("Foresight_1_Knowledgeable").checked=true;
		    		else if(obj.Foresight_1=="Practitioner")
					    	  document.getElementById("Foresight_1_Practitioner").checked=true;
		    		else if(obj.Foresight_1=="Proficient")
					    	  document.getElementById("Foresight_1_Proficient").checked=true;
		    		else if(obj.Foresight_1=="Champion")
					    	  document.getElementById("Foresight_1_Champion").checked=true;
			    	
		    	}
		    	//2
		        if(obj.Foresight_2===undefined){
		    		var node_list = document.getElementsByName('Foresight_2');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_2=="Knowledgeable") 
				    	      document.getElementById("Foresight_2_Knowledgeable").checked=true;
		    		else if(obj.Foresight_2=="Practitioner")
					    	  document.getElementById("Foresight_2_Practitioner").checked=true;
		    		else if(obj.Foresight_2=="Proficient")
					    	  document.getElementById("Foresight_2_Proficient").checked=true;
		    		else if(obj.Foresight_2=="Champion")
					    	  document.getElementById("Foresight_2_Champion").checked=true;
			    	
		    	}
		    	//3
		        if(obj.Foresight_3===undefined){
		    		var node_list = document.getElementsByName('Foresight_3');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_3=="Knowledgeable") 
				    	      document.getElementById("Foresight_3_Knowledgeable").checked=true;
		    		else if(obj.Foresight_3=="Practitioner")
					    	  document.getElementById("Foresight_3_Practitioner").checked=true;
		    		else if(obj.Foresight_3=="Proficient")
					    	  document.getElementById("Foresight_3_Proficient").checked=true;
		    		else if(obj.Foresight_3=="Champion")
					    	  document.getElementById("Foresight_3_Champion").checked=true;
			    	
		    	}
		    	//4
		        if(obj.Foresight_4===undefined){
		    		var node_list = document.getElementsByName('Foresight_4');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_4=="Knowledgeable") 
				    	      document.getElementById("Foresight_4_Knowledgeable").checked=true;
		    		else if(obj.Foresight_4=="Practitioner")
					    	  document.getElementById("Foresight_4_Practitioner").checked=true;
		    		else if(obj.Foresight_4=="Proficient")
					    	  document.getElementById("Foresight_4_Proficient").checked=true;
		    		else if(obj.Foresight_4=="Champion")
					    	  document.getElementById("Foresight_4_Champion").checked=true;
			    	
		    	}
		    	//5
		        if(obj.Foresight_5===undefined){
		    		var node_list = document.getElementsByName('Foresight_5');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_5=="Knowledgeable") 
				    	      document.getElementById("Foresight_5_Knowledgeable").checked=true;
		    		else if(obj.Foresight_5=="Practitioner")
					    	  document.getElementById("Foresight_5_Practitioner").checked=true;
		    		else if(obj.Foresight_5=="Proficient")
					    	  document.getElementById("Foresight_5_Proficient").checked=true;
		    		else if(obj.Foresight_5=="Champion")
					    	  document.getElementById("Foresight_5_Champion").checked=true;
			    	
		    	}
		    	//6
		    	
		        if(obj.Foresight_6===undefined){
		    		var node_list = document.getElementsByName('Foresight_6');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_6=="Knowledgeable") 
				    	      document.getElementById("Foresight_6_Knowledgeable").checked=true;
		    		else if(obj.Foresight_6=="Practitioner")
					    	  document.getElementById("Foresight_6_Practitioner").checked=true;
		    		else if(obj.Foresight_6=="Proficient")
					    	  document.getElementById("Foresight_6_Proficient").checked=true;
		    		else if(obj.Foresight_6=="Champion")
					    	  document.getElementById("Foresight_6_Champion").checked=true;
			    	
		    	}
		    	//7
		    	
		        if(obj.Foresight_7===undefined){
		    		var node_list = document.getElementsByName('Foresight_7');
			    		for (var i = 0; i < node_list.length; i++) {
			    		    node_list[i].checked = false;
			    		}
		    	}
		    	else
			    {
		    		if(obj.Foresight_7=="Knowledgeable") 
				    	      document.getElementById("Foresight_7_Knowledgeable").checked=true;
		    		else if(obj.Foresight_7=="Practitioner")
					    	  document.getElementById("Foresight_7_Practitioner").checked=true;
		    		else if(obj.Foresight_7=="Proficient")
					    	  document.getElementById("Foresight_7_Proficient").checked=true;
		    		else if(obj.Foresight_7=="Champion")
					    	  document.getElementById("Foresight_7_Champion").checked=true;
			    	
		    	}

		        <!---- end Foresight ----->
			    }
		   });
			 
		}