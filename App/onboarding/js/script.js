jQuery(function(){
	//new joinee validations
	jQuery("#EmployName").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmployID").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Email").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#MaritalStatus").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#BloodGroup").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Location").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Qualification").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmpContactNumber").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmergencyContactNumber1").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmergencyContactName1").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmergencyContactRelationship1").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmergencyContactNumber2").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmergencyContactName2").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#EmergencyContactRelationship2").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#PANDetails").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#PassportNumber").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Reporter").validate({
		expression: "if (VAL) return true; else return false;"
	});
	
});

	function otherQualification(Other)
	{
		if(Other == "Other")
		{
			jQuery("#OtherQualificationTr").show();
			jQuery("#OtherQualification").validate({
				expression: "if (VAL) return true; else return false;"
			});
		}
		else
		{
			jQuery("#OtherQualificationTr").hide();
		}
		
	}
	
	function EmployeeDetails(EmpId)
	{
		$.ajax({
	        url: "mediassist_EmployeeData.php",
	        type: "post",
	        async: false,
	        data: { EmployID:EmpId},
		       success: function( result ) 
		       {
		        	var ajax_data = jQuery.parseJSON(result);
		        	if(result != 0)
		        	{
		        		$('#DateOfJoin').val(ajax_data.DateOfJoin);
			        	$('#DateOfBirth').val(ajax_data.DateOfBirth);
		        	}
		        	else
		        	{
		        		$('#DateOfJoin').val('');
			        	$('#DateOfBirth').val('');
		        	}
		        }
	      });
	}
	