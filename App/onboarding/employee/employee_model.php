<?php
//error_reporting(0);
// session_start();
require ('../config/mysql_crud.php');

	function getEmployeegrade($EmployID)
    {
		$db = new Database();
        $sql = "SELECT d.grades,e.employee_id AS EmployID, e.designation_id AS DesignationID, e.email AS email FROM employees e JOIN designation d on d.designation_id = e.designation_id WHERE e.employee_id ='" . $EmployID . "'";
        
        $result = $db->sql($sql);
		$result_array = $db->getResult();
		if(is_array($result_array) && count($result_array)!=0)
		{
            return $result_array;
        }
        return '';
    }
    
    function getPreviousSupervisors($grades, $EmployID)
    {
        $db = new Database();
        $sql = "SELECT employees.email AS Email, CONCAT(IFNULL(`employees`.`first_name`, ''),' ',IFNULL(`employees`.`last_name`, '')) AS Name 
			FROM employees 
            JOIN designation ON employees.designation_id = designation.designation_id
            WHERE employees.status!=6 AND designation.grades >" .$grades . " ORDER BY first_name";
        
        $db->sql($sql);      
        $data = getEmployeeSupervisors($EmployID); //function to get employee supervisors  
        
        //return $db->getResult();
        return $data;
    }
    
    function getCurrentSupervisors($grades, $EmployID)
    {
    	$reporteesID = array();
    	$reporteesID[] = getDirectSupervisor($EmployID);
    	
    	$reporteesID = array_unique($reporteesID);
    	if(!empty($reporteesID))
    	{
    		foreach ($reporteesID as $repID)
    		{
    			$finalData[] = getSupervisorDetails($repID); //get supervisors details
    		}
    	}
    	return $finalData;
    }
    
    //get Reporting Manager
    function getReportingManager($EmployID)
    {
    	$managerDetails  = array();
    	$reporteeID = getDirectSupervisor($EmployID);   	
    	$managerID =  getDirectSupervisor($reporteeID);
    	$managerDetails[] = getSupervisorDetails($managerID);    	
    	return $managerDetails;   	
    }
    
    /* function to get employee supervisors */
    function getEmployeeSupervisors($EmployID)
    {
    	$db = new Database();
    	$reporteesData = array();
    	$finalData = array();    	
    	$sql = "SELECT  transitionData FROM employ_transition  where employId ='" . $EmployID . "'";
    	$db->sql($sql);
    	$transitionData = $db->getResult();
    	
    	if (!empty($transitionData))
    	{    	                        	
        	$unserializedData = unserialize($transitionData['transitionData']);       	 
        	
        	$threeMonthsAgoDate = date("d-m-Y",mktime(0,0,0,date("m")-3,date("d")));         	
        	$dataLength = count($unserializedData);        	 
        	for($i=0; $i<$dataLength;$i++)
        	{       		
	        	if($unserializedData[$i]['transition_date'] != '')	
	        	{
	        		if(strtotime($unserializedData[$i]['transition_date']) >= strtotime($threeMonthsAgoDate))
	        		{
	        			if ($unserializedData[$i]['to_reporting'] != '')
	        			{
	        				$k=0;
	        				for($j=0; $j<$dataLength; $j++)
	        				{
	        					if($unserializedData[$j]['transition_date'] != '')
	        					{
	        						if(strtotime($unserializedData[$j]['transition_date']) >= strtotime($threeMonthsAgoDate))
	        						{
	        							if ($unserializedData[$i]['to_reporting'] != '')
	        							{
	        								if($unserializedData[$i]['to_reporting'] == $unserializedData[$k]['from_reporting'])
	        								{
	        									$fromDate = $unserializedData[$i]['transition_date'];
	        									$toDate = $unserializedData[$k]['transition_date'];
	        									$reportingId = $unserializedData[$i]['to_reporting'];
	        									
	        									if(strtotime($fromDate) <= strtotime($toDate))
	        									{	        											        										
	        										$date1 = new DateTime($fromDate);
	        										$date2 = new DateTime($toDate);	        										
	        										$diff = $date2->diff($date1)->format("%a");	        											        											        										
	        									}	        										        									        								
	        								}
	        							}
	        						}
	        					}
	        					$reporteesData[$k]['fromDate'] = $fromDate;
	        					$reporteesData[$k]['toDate'] = $toDate;
	        					$reporteesData[$k]['diffDays'] = $diff;
	        					$reporteesData[$k]['employID'] = $reportingId;
	        					$k++;
	        				}	        					        				
	        			}
	        		}
	        	}	        		
        	}       	        	       	            	        
    	} 
    	
    	$reporteesData = array_unique($reporteesData);
    	    	    	   	       
    	$maxData = array_reduce($reporteesData, function ($a, $b) {
    		return @$a['diffDays'] > $b['diffDays'] ? $a : $b;
    	});
    	
    	//echo '<pre>'; print_r($reporteesData); exit;
    	        
        if(!empty($reporteesData))
        {
        	foreach ($reporteesData as $key=>$value)
        	{        		
        		if( $maxData['diffDays'] == $value['diffDays'])
        		{
        			$finalData[] = getSupervisorDetails($value['employID']); //get supervisors details
        		}        		
        	}
        }               
        return $finalData;      		
    }
    
    //function to get supervisor details by $repID
    function getSupervisorDetails($repID)
    {
    	$db = new Database();
    	if($repID != '')
    	{
    		$sql = "SELECT company_employ_id AS EmployID, CONCAT(IFNULL(`first_name`, ''), ' ', IFNULL(`last_name`, '')) AS Name, email AS Email FROM `employees` WHERE `employee_id` ='" . $repID . "'";
    		$db->sql($sql);
    	    return $db->getResult();    	
    	}
    	 
    }
    
    //function to get direct supervisor ID
    function getDirectSupervisor($EmployID)
    {
    	$db = new Database();  	
    	$sql = "SELECT `primary_lead_id` FROM `employees` WHERE `employee_id` ='" . $EmployID . "'";
    	$db->sql($sql);
    	$directSupervisorData = $db->getResult();    	
    	return $directSupervisorData['primary_lead_id'];
    }
    
    
    function getNotificationDetails($notificationID)
    {
    	$db = new Database();
        $sql = "SELECT * FROM notification WHERE NotificationID=".$notificationID;
        
        $db->sql($sql);
        
        return $db->getResult();
    }
    
    function getEmployeesWorkingUnder($CompanyEmployID,$Email)
    {
    	$db = new Database();
    	$MyDatasql = "SELECT employee_id AS EmployID FROM employees WHERE company_employ_id = $CompanyEmployID AND Email = '$Email' AND status!=6";
    	$db->sql($MyDatasql);
        $EmployID = $db->getResult();
        $empid = $EmployID['EmployID'];
    	if(isset($empid))
        {
        	$_SESSION['Cmpetency_Primary_Lead_ID'] = $empid;
        	$sql = "SELECT employee_id AS EmployID, CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, '')) AS Name FROM employees WHERE primary_lead_id = $empid AND status!=6";
        
        	$db->sql($sql);
        	return $db->getResult();
        }
        else
        {
        	return false;
        }
    }
    
    function getCompetencyAssessID($EmployID)
    {
    	$db = new Database();
        $sql = "SELECT competencyID FROM competency_assessment WHERE EmployID = $EmployID";
        
        $db->sql($sql);
        
        return $db->getResult();
    }
	
	function getEmployName($EmployID)
    {
    	$db = new Database();
        $sql = "SELECT CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, '')) AS Name FROM employees WHERE employee_id = $EmployID";
        
        $db->sql($sql);
        
        return $db->getResult();
    }
    
    function getEmployPreviousVertical()
    {
    	$empArray = array(1154,1943,1601,1912,2054,1778,2112,2102,140,1659,1316,1342,2035,162,391,209,1959,121,1935,1774,72,1881,561,567,1549,1471,651,1275,2055,1105,1539,224,1993,104,144,1545,237,303,1260,117,1583,2057,150,2113,1027,1024,1868,726,1219,1000,358,976,1304,1173,379,1674,1230,2090,1905,1694,1938,1147,1237,809,103,27,1013,1757,1331,106,1823,1885,847,2066,91,147,1894,1400,227,1735,1751,1900,1879,2105,172,396,210,102,1141,344,2029,727,1001,1818,272,801);
    	$db = new Database();
    	$finalData = array();
    	$completeData = array();
    	$threeMonthsAgoDate = date("d-m-Y",mktime(0,0,0,date("m")-3,date("d")));
    	     	 
    	foreach ($empArray as $EmployID)
    	{
    		 
    		$sql = "SELECT  transitionData FROM employ_transition  where employId ='" . $EmployID . "'";
    		$db->sql($sql);
    		$transitionData = $db->getResult();
    
    		if (!empty($transitionData))
    		{
    			$unserializedData = unserialize($transitionData['transitionData']);
    			$dataLength = count($unserializedData);
    			for ($i=count($unserializedData)-1; $i>=0; $i--)
    			{
	    			
    				if(strtotime($unserializedData[$i]['transition_date']) >= strtotime($threeMonthsAgoDate))
    				{
    				if ($unserializedData[$i]['from_vertical_id'] != '')
	    			{
	    				$sql = "SELECT `AssignedVertical` FROM `employ` WHERE `EmployID` ='" . $EmployID . "'";
			    		$db->sql($sql);
			    		$empVertical = $db->getResult(); 
	    				
	    				if ($unserializedData[$i]['from_vertical_id'] != 10 && $unserializedData[$i]['from_vertical_id'] != $empVertical['AssignedVertical']){
		    				$finalData[$EmployID]['PreviousVerticalID'] = $unserializedData[$i]['from_vertical_id'];
		    				$finalData[$EmployID]['PreviousVerticalName'] = getVerticalName($unserializedData[$i]['from_vertical_id']);
		    				break;
		    			}	    				
	    			}
    				}
    			}
    
    			for ($i=count($unserializedData)-1; $i>=0; $i--)
    			{
    				if(strtotime($unserializedData[$i]['transition_date']) >= strtotime($threeMonthsAgoDate))
    				{
    				if ($unserializedData[$i]['to_vertical_id'] != '')
	    			{
	    			  if ($unserializedData[$i]['to_vertical_id'] != 10){
	    				$finalData[$EmployID]['CurrentVertical'] = $unserializedData[$i]['to_vertical_id'];
		    			$finalData[$EmployID]['CurrentVerticalName'] = getVerticalName($unserializedData[$i]['to_vertical_id']);
		    			break;
	    			  }
	    			}
    			}
    			}
    			 
    			 
    			}
    			else
    			{
    				$finalData[$EmployID]['CurrentVerticalName'] = 'no data';
    			}
    
    			}
    			 
    			//echo '<pre>'; print_r($finalData);
    			
    			//echo '====================================================================================';
    				 
    		   foreach ($finalData as $key=>$value)
    		   {
    		   	
    		            $sql = "SELECT `CompanyEmployID` FROM `employ` WHERE `EmployID` ='" . $key . "'";
			    		$db->sql($sql);
			    		$empCompanyId = $db->getResult(); 			    		
			    		$completeData[$empCompanyId['CompanyEmployID']]['PreviousVerticalID'] = $value['PreviousVerticalID'];
			    		$completeData[$empCompanyId['CompanyEmployID']]['PreviousVerticalName'] = $value['PreviousVerticalName'];			    		
			    		$completeData[$empCompanyId['CompanyEmployID']]['CurrentVertical'] = $value['CurrentVertical'];
			    		$completeData[$empCompanyId['CompanyEmployID']]['CurrentVerticalName'] = $value['CurrentVerticalName'];
    		   	
    		   }
    		   
    		   echo '<pre>'; print_r($completeData);
    
    }
    
    
    						function getVerticalName($verticalId)
    						{
    						$db = new Database();
    						$sql = "SELECT `Name` FROM `vertical` WHERE `VerticalID` ='" . $verticalId . "'";
    						$db->sql($sql);
    						$verticalName = $db->getResult();
    						return $verticalName['Name'];
    						}
       
   