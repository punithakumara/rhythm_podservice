<?php

session_start(); //to ensure you are using same session								
$_SESSION['logged_email'] = '';
$_SESSION['logged_name'] = '';
unset($_SESSION['logged_email']);
unset($_SESSION['logged_name']);
session_destroy(); //destroy the session
header('Location: login.php');

?>