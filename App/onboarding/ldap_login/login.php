<?php 
session_start();
include('library/AuthLDAP.php'); // Include Ldap Libraray

	if(isset($_SESSION['logged_email']))
	{		
	    header('Location: list.php');
		exit;
	}
	
	$Msg = '';
	
	if($_POST)
	{
		$userName = $_POST['userName'];
		$password = $_POST['password'];
		
		$AuthLDAP = new AuthLDAP();
		// Connecting the Ldap Server
		$AuthLDAP->connect();
		
		$user_info = $AuthLDAP->authenticateUser($userName, $password);
				
		if(count($user_info) > 0 && $user_info)
		{										
				$_SESSION['logged_email'] = $user_info[0]['Email'];
				$_SESSION['logged_name'] = $user_info[0]['Name'];
				
				header("Location: list.php");
				exit;			
		}
		else
			$Msg = "Invalid Username / Password...!!!";
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LDAP Login</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header">
	<img src="images/logo.png" width="98" height="69" alt=""/>	
</div>
<form action="login.php" method="POST">
<div class="LoginOuter">
	<label>User Name</label>
    <input type="text" class="Txtbox" value="" name="userName" id="EmployID" />
    <label>Password</label>
    <input type="password" class="Txtbox" value="" name="password" id="Email" style="height: 26px !important; padding: 0 !important;"/>
    <div style="margin-left: 82px;color: red; font-family: Tahoma, Geneva, sans-serif;">
	<?php echo $Msg; ?>
	</div>
	<input id="saveForm" type="submit" name="submit" value="Submit" class="LoginBtn">
</div>
</form>
</body>
</html>