<?php
class MySqlConnect 
{
	protected static	$_instance	= null;
    protected			$_conn		= null;
	public	$db;
	public	$conn;

	protected function __construct() {
		/**
		Invoking setConnection Function
		*/
		if( $this->setConnection() ) {
			$this->db	= "prohancestats";
			$this->setDatabase() ;
		}
	}
	/**
		Create Instance of this Class
	*/
	public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

	public function setConnection() {
		$this->conn = mysql_connect('172.19.18.98','root','root') or die("Unable to connect to MySQL");
		return true;
	}

	public function setDatabase() {
		return mysql_select_db($this->db, $this->conn) or die("Could not connect to database");
	}

	public function query($query) {
		if(isset($query) && !empty($query)) {
			return mysql_query($query, $this->conn);
		}
    }
}


?>