<?php
ini_set('max_execution_time', '300');
ini_set("log_errors", 1);
require_once(dirname(__FILE__).'/config/clientEmailConfig.php');
class Email_reader
{
     // imap server connection
	public $conn;
    // inbox storage and inbox message count
    public $inbox;
    public $msg_cnt;
    // email login credentials fetch from config file
    private $clientEmails   = '';
    private $server         = ''; 
    private $user           = ''; 
    private $pass           = ''; 
    private $msgNumbers     = array();
    private $Subj           = 'ProhanceGetAttachments';//Email subject should be start with ticket:
	public $logFile	= ''; 
    //private $port   = 143; // adjust according to server settings
    
    // connect to the server and get the inbox emails
    
    function __construct()
    {
        global $clientEmail;
        $this->clientEmails = $clientEmail;       
		$this->logFile = __DIR__.'./logs/EmailTickets_log_'.date('dMY').'.txt'; 
		//error_log"\r\n Email Read process started!!! :- " . date("d-M-Y, h:i:s a") . "\r\n", 3, $this->logFile);
		
        foreach ($this->clientEmails as $clientMail) {
            $this->server = $clientMail['server'];
            $this->user   = $clientMail['username'];
            $this->pass   = $clientMail['password'];
                    
			//error_log"\r\n Email Connect process started :- Process DateTime : " . date("d-M-Y, h:i:s a") . "\r\n", 3, $this->logFile);
			$this->connect();
            		         
			//error_log"\r\n Emails Read process started :- Process DateTime : " . date("d-M-Y, h:i:s a") . "\r\n", 3, $this->logFile);
			$this->inbox();
			       
			//error_log"\r\n Delete Read Emails process started :- Process DateTime : " . date("d-M-Y, h:i:s a") . "\r\n", 3, $this->logFile);			
            $this->deleteemails();
			//error_log"\r\n Emails Read process completed :- Process DateTime : " . date("d-M-Y, h:i:s a") . "\r\n", 3, $this->logFile);
			
        }      
    } 
    
    // close the server connection
    function close()
    {
        $this->inbox   = array();
        $this->msg_cnt = 0;
        imap_close($this->conn);
    }
    
    // open the server connection
    // the imap_open function parameters will need to be changed for the particular server
    // these are laid out to connect to a Dreamhost IMAP server
    
    function connect()
    {
        //$this->conn = imap_open('{'.$this->server.'/notls}', $this->user, $this->pass);
        $this->conn = imap_open('{' . $this->server . '/pop3}INBOX', $this->user, $this->pass);
        
    }
    
    // move the message to a new folder
    function move($msg_index, $folder = 'INBOX.TICKETS')
    {
        // move on server
        $imap_result = imap_mail_move($this->conn, '1:' . $msg_index, $folder);
        if ($imap_result == false) {
            die(imap_last_error());
        }
        imap_close($this->conn);
    }
    
    // get a specific message (1 = first email, 2 = second email, etc.)
    
    function get($msg_index = NULL)
    {
        if (count($this->inbox) <= 0) {
            return array();
        } elseif (!is_null($msg_index) && isset($this->inbox[$msg_index])) {
            return $this->inbox[$msg_index];
        }
        return $this->inbox[0];
    }
    
    function getCount()
    {
        return $this->msg_cnt;
    }
    
    // read the inbox
    function inbox()
    {
        $date = date("d-M-Y", strToTime("today"));           
        $boxes = imap_search($this->conn, "SINCE \"{$date}\"", SE_UID);
       
        if ($boxes) {
            if ($boxes = imap_search($this->conn, strtolower('subject ') . strtolower($this->Subj) . ' SINCE "' . $date . '"', SE_UID)) {
            }
        }
      
        if (!empty($boxes)) {
            
            $this->msg_cnt = count($boxes);
            
            $in = array();
            for ($i = 0; $i < $this->msg_cnt; $i++) {
                $in[] = array(
                    'MsgIds' => $boxes[$i],
                    'index' => $i,
                    'header' => imap_headerinfo($this->conn, $boxes[$i]),
                    //'body'      => imap_body($this->conn, $boxes[$i]),
                    'structure' => imap_fetchstructure($this->conn, $boxes[$i])
                );
             }
            $this->copyAttachments($in);
            $this->inbox = $in;
        }else 
		  return '';
    }
   
    function copyAttachments($mailinfo)
    {
        $finalData = array();
        $errors    = array();
        for ($i = 0; $i < count($mailinfo); $i++) {
            $finalData['name'] = $mailinfo[$i]['header']->fromaddress;
            $frommailaddress   = $mailinfo[$i]['header']->from;
            foreach ($frommailaddress as $id => $object) {
                $fromaddress[] = $object->mailbox . "@" . $object->host;
                
            }
            
            $finalData['email']   = $fromaddress[$i];
            $finalData['subject'] = $mailinfo[$i]['header']->subject;
            if (isset($mailinfo[$i]['header']->cc)) {
                $ccmailaddress = $mailinfo[$i]['header']->cc;
                $ccaddress     = array();
                $j             = 0;
                foreach ($mailinfo[$i]['header']->cc as $idc => $objectem) {
                    $ccaddress[$i][$j] = $objectem->mailbox . "@" . $objectem->host;
                    $j++;                    
                }                
                $ccAddressmail = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($ccaddress)));
                if (count($ccAddressmail) >= 1) {
                    if (count($ccAddressmail) == 1)
                        $finalData['ccemail'] = $ccAddressmail[0];
                    else
                        $finalData['ccemail'] = implode(",", $ccAddressmail);
                }
                else{
                	$finalData['ccemail'] = $this->user."@theoreminc.net";
                }
                
            }         
            
            $finalData['files'] = $this->attachmentsfetch($mailinfo[$i]['structure'], $mailinfo[$i]['MsgIds']);
            if (empty($finalData['files'])) {
                unset($finalData['files']);
            }                 
        }
    }   
	
	function attachmentsfetch($structure, $msgno)
    {    
        if (isset($structure->parts) && count($structure->parts)) {
            
            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments     = array();
                $attachments[$i] = array(
                    'is_attachment' => false,
                    'filename' => '',
                    'name' => '',
                    'attachment' => '',
                    'type' => '',
                    'size' => '',
                    'tmp_name' => ''
                );
                
                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename']      = $object->value;
                        }
                    }
                }
                
                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name']          = $object->value;
                        }
                    }
                }
                if ($structure->parts[$i]->bytes) {
                    $attachments[$i]['size'] = $structure->parts[$i]->bytes;
                }
                
                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($this->conn, $msgno, $i + 1);
                    
                    if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                    } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                    $ext = $finfo = pathinfo($attachments[$i]['filename']);                 
                    $extension = trim($ext['extension']);
                    if ($extension != "") {                    	
                        switch ($extension) {
                            case 'asf':
                                $type = "video/x-ms-asf";
                                break;
                            case 'avi':
                                $type = "video/avi";
                                break;
                            case 'flv':
                                $type = "video/x-flv";
                                break;
                            case 'fla':
                                $type = "application/octet-stream";
                                break;
                            case 'swf':
                                $type = "application/x-shockwave-flash";
                                break;
                            case 'doc':
                                $type = "application/msword";
                                break;
                            case 'docx':
                                $type = "application/msword";
                                break;
                            case 'zip':
                                $type = "application/zip";
                                break;
                            case 'xls':
                                $type = "application/vnd.ms-excel";
                                break;
                            case 'xlsx':
                               	$type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                break;
                            case 'gif':
                                $type = "image/gif";
                                break;
                            case 'jpg':
                            case 'jpeg':	
                                $type = "image/jpeg";
                                break;
                            case 'png':
                                $type = "image/png";
                                break;
                            case 'wav':
                                $type = "audio/wav";
                                break;
                            case 'mp3':
                                $type = "audio/mpeg3";
                                break;
                            case 'mpg':
                            case 'mpeg':
                                $type = "video/mpeg";
                                break;
                            case 'rtf':
                                $type = "application/rtf";
                                break;
                            case 'htm':
                            case 'html':
                                $type = "text/html";
                                break;
                            case 'xml':
                                $type = "text/xml";
                                break;
                            case 'xsl':
                                $type = "text/xsl";
                                break;
                            case 'css':
                                $type = "text/css";
                                break;
                            case 'php':
                                $type = "text/php";
                                break;
                            case 'txt':
                                $type = "text/txt";
                                break;
                            case 'asp':
                                $type = "text/asp";
                                break;
                            case 'pdf':
                                $type = "application/pdf";                                                             
                                break;
                            case "psd":
                                $type = "application/octet-stream";
                                break;
                            case "csv":
                            	$type = "text/csv";
                            	break;
                            default:
                                $type = "application/octet-stream";
                               
                        }
                        
                        $attachments[$i]['type'] = $type;                        
                        $SaveFile = 'input/' . $msgno . "-" . $attachments[$i]['filename'];
						$fp                      = fopen($SaveFile, "w+");
                        fwrite($fp, $attachments[$i]['attachment']);
                        fclose($fp);
                        $attachments[$i]['tmp_name'] = $SaveFile;
                        unset($attachments[$i]['attachment']);
                    }
                }
            }
            
            $attachments = array_values($attachments);
            
        }
        
        return $attachments;
    }
    function deleteemails()
    {
             if (count($this->msgNumbers) >= 1) {
	            for ($d = 0; $d < count($this->msgNumbers); $d++) {
	                imap_delete($this->conn, $this->msgNumbers[$d]);
	            }
	            imap_expunge($this->conn);
	            echo "<br> Ticket source emails are deleted :".count($this->msgNumbers) ;
				//error_log"\r\n Ticket source emails are deleted :".count($this->msgNumbers). " Process DateTime : " . date("d-M-Y, h:i:s a") . "\r\n", 3, $this->logFile);
	        }        
        imap_close($this->conn, CL_EXPUNGE);
    }

}
$emailRead = new Email_reader();

?>