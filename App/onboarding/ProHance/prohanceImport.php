<?php
require_once (dirname(__FILE__)."/config/db_config.php");
require_once("Classes/PHPExcel.php");
require_once('Classes/PHPExcel/IOFactory.php');
ini_set('max_execution_time', 300); 
ini_set('default_charset', 'utf-8');

class ProhanceImport 
{
	protected static	$instance	= null;
	protected  			$path		= null; 

	/* create database object */
	public function __construct() {
		$this->res	= MySqlConnect::getInstance();
		$this->path	= dirname(__FILE__).'\input';
		$this->path	= (!is_dir($this->path) ? mkdir($this->path) : $this->path);
	}
	
	/*function to scan xls file */
	public function scanExcelFile() {
		if ($handle = opendir($this->path)) {
			while (false !== ($entry = readdir($handle))) {
				$files				= preg_replace( "/^\.+|\.+$/", "", $entry);
				$ext  				= pathinfo($files, PATHINFO_EXTENSION);

				if(!empty($files) && ($ext	== 'xls' || $ext	== 'xlsx')) {
					$fp             = $this->path . "\\". $files;
                	$filename       = substr(basename(($this->path . "\\". $files), ".xls"), 0, 4);
                	$today          = date("d-m-Y");
                    $mkxl           = $this->path.'\Excel';
                    if(!is_dir($mkxl)) {
                        mkdir($mkxl);
                        chmod($mkxl, 0755);
                    }
                    $xlfile         = $mkxl.'\\'.$filename.'_'.$today.'.xlsx';
                	$inputFileType  = PHPExcel_IOFactory::identify($fp);
                	$objReader      = PHPExcel_IOFactory::createReader($inputFileType);
                	$objPHPExcel    = $objReader->load($fp);
                	$objWriter      = new PHPExcel_Writer_Excel2007($objPHPExcel);
                	$objWriter->save($xlfile);
                    $objPHPExcel->disconnectWorksheets();
                    unset($objWriter, $objPHPExcel);
                }
			}
		}
	}
	
	/*function to read data from excel file in to array */
	public function readExcelFile() {
		$objReader	= PHPExcel_IOFactory::createReader("Excel2007");
        $res        = array();
		if(false !== ($handle = opendir($this->path.'\Excel'))) {
			while (false !== ($entry= readdir($handle))) {
				$file	= preg_replace( "/^\.+|\.+$/", "", $entry);
				if(!empty($file) && pathinfo($file, PATHINFO_EXTENSION) == 'xlsx') {
					$objPHPExcel        = $objReader->load($this->path.'\Excel\\'.$file);
            		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            			$res[] = $worksheet->toArray(NULL, TRUE, TRUE);
            		}
            	}
            }
            return $res;
        }
	}

	/*function to save excel data array in to processStats table */
	public function saveExcel() {
		$res	=	$this->readExcelFile();
		if(is_array($res)) {
			for ($i = 0; $i < count($res); $i++) {
				$slicevalues   = array_slice($res[$i], 7);
				foreach ($slicevalues as &$sliceval) {
					if(!empty($sliceval)) {
						$filtervalues   = array_values(array_filter($sliceval, function($v) {
							return !is_null($v);
                		}));
						$finalres       = array(
							'ProcessName'           => stripslashes(str_replace('â€“', "-", $filtervalues[0])),
                    		'ProcessUtil'           => stripslashes($filtervalues[1]),
                    		'ProcessEff'            => stripslashes($filtervalues[2]),
                    		'TotLoggedHrs'          => $this->timeformat($filtervalues[3]),
                    		'AvgLoggedHrs'          => $this->timeformat($filtervalues[4]),
                    		'TotActiveHrs'          => $this->timeformat($filtervalues[5]),
                    		'AvgActiveHrs'          => $this->timeformat($filtervalues[6]),
                    		'TotTimeOnSys'          => $this->timeformat($filtervalues[7]),
                    		'AvgTimeOnSys'          => $this->timeformat($filtervalues[8]),
                    		'TotCoreProd_TOS'       => $this->timeformat($filtervalues[9]),
                    		'AvgCoreProd_TOS'       => $this->timeformat($filtervalues[10]),
                    		'TotNonCoreProd_TOS'    => $this->timeformat($filtervalues[11]),
                    		'AvgNonCoreProd_TOS'    => $this->timeformat($filtervalues[12]),
                    		'TotNonProd_TOS'        => $this->timeformat($filtervalues[13]),
                    		'AvgNonProd_TOS'        => $this->timeformat($filtervalues[14]),
                    		'TotTimeAwayFromSys'    => $this->timeformat($filtervalues[15]),
                    		'AvgTimeAwayFromSys'    => $this->timeformat($filtervalues[16]),
                    		'TotCoreProd_TAFS'      => $this->timeformat($filtervalues[17]),
                    		'AvgCoreProd_TAFS'      => $this->timeformat($filtervalues[18]),
                    		'TotNonCoreProd_TAFS'   => $this->timeformat($filtervalues[19]),
                    		'AvgNonCoreProd_TAFS'   => $this->timeformat($filtervalues[20]),
                    		'TotNonProd_TAFS'       => $this->timeformat($filtervalues[21]),
                    		'AvgNonProd_TAFS'       => $this->timeformat($filtervalues[22]),
                    		'CreatedDate'           => date("Y-m-d")
                		);
					}
					$query  = $this->res->query(" INSERT INTO processstats (".implode(",", array_keys($finalres)).") 
						VALUES ('".implode("','", $finalres)."')");
					if($query) {
						$msg	= "New record created successfully";
                        $succ   = true;
            		} else {
            			$msg	= "Error: " . $query . "<br>" . mysql_error();
                        $succ   = false;
            		}
                }
            }
            if(isset($succ)) {
                $this->moveExcelFiles();
            }
		}
	}

	/* After saving data in DB, move the file from input to processed */
	public function moveExcelFiles() {
		$files			= scandir($this->path.'\Excel');
		$ipfiles		= glob($this->path);
		$source 		= $this->path.'\Excel\\';
		$destination	= dirname(__FILE__)."\processed\\";
		foreach ( $files as $file ) {
			if (in_array($file, array(".",".."))) 
				continue;
			if (copy($source.$file, $destination.$file)) {
				$delete[] = $source.$file;
        	}
    	}
        $this->emptyDir($this->path);
    }

    /*delete the file from folder*/
    public function emptyDir($directory, $delete = false) {
        $contents = glob($directory . '*');
        foreach($contents as $item) {
            if (is_dir($item)) {
                self::emptyDir($item . '/', true);
            } else {
                unlink($item);
            }
        }
        if (count(glob($directory . '*')) === 0) {
            if($delete === true) {
                rmdir($directory);
            }
        }
    }
	
    /*change the time format before storing in db*/
	public function timeformat($timevalue) {
   		$time    = $timevalue* 86400;
   		$hours   = round($time / 3600);
   		$minutes = round($time / 60) - ($hours * 60);
   		$seconds = round($time) - ($hours * 3600) - ($minutes * 60);
   		return sprintf('%02d:%02d:%02d', abs($hours), abs($minutes), abs($seconds));
	}

}

//create object for ProhanceImport class
$obj	= new ProhanceImport;
$obj->scanExcelFile();
$obj->saveExcel();

?>