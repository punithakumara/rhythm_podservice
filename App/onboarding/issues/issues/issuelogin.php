<?php
$error_msg = ""; 
$redirect=isset($_REQUEST["r"])?$_REQUEST["r"]:null;
if($redirect ==1)
	$error_msg =  "Invalid username or password";
else if($redirect ==2)
	$error_msg =  "You don't have permission, Access denied !";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rhythm Issues</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
body { background-image:url(../images/login_bg.png); background-repeat:repeat; background-color :#ffffff}
.LogoOtr					{ width:128px; height:45px;background: #ffffff url(../images/login_bg.png); background-repeat:repeat; float:left;
							  margin-left:5px; text-align:center; }
.Header			{ background:#005589; width:100%; height:45px; }
.Header span	{ width:auto; height:35px; background-color:#ffffff; padding-top:10px; float:left; margin-left:5px; }
.MainLoginOtr				{ width:575px; height:355px; margin:0px auto;  margin-top:5%; padding-top:100px; }
.LoginHeadingArea			{ width:372px; height:41px; border-bottom-left-radius:5px; -moz-border-bottom-left-radius:5px; float:left; 	
							  border-bottom-right-radius:5px; -moz-border-bottom-right-radius:5px; background-color:#005589; text-align:center; 
							  margin:20px 0px 5px 100px; font-family:Tahoma, Geneva, sans-serif; font-size:20px; color:#ffffff; line-height:41px; }
.ErrorMsg					{ color: red; width: 100%; height: 21px; font-family:Tahoma, Geneva, sans-serif; font-size:12px; }
.Footer						{ width:100%; z-index: 10; height:25px; background-color:#005589; float:left; font-family:Tahoma, Geneva, sans-serif;
							  color:#ffffff; font-size:11px; text-align:center; line-height:25px; margin-top:45px; position: fixed; bottom: 0px;  }
.LoginOtr					{ width:370px; height:162px; padding-bottom:5px; background-color:#ffffff; border:1px solid #999999; float:left;
							  margin-left:100px; border-top-left-radius:5px; -moz-border-top-left-radius:5px; float:left; 	
							  border-top-right-radius:5px; -moz-border-top-right-radius:5px; padding-top:11px; }
.Logincontent				{ width:auto; float:left;  position: relative; height: 55px; }
.LoginBG					{ background-image:url(images/logintab_BG.png); width:93px; height:38px; background-repeat:no-repeat; 
							  margin-left:-19px; float:left; }
.PSWBG						{ background-image:url(images/psw_BG.png); width:93px; height:38px; background-repeat:no-repeat; 
							  margin-left:-19px; float:left; }
.LoginBtn					{ width:auto; height:auto; background-color:#dd820b; font-family:Tahoma, Geneva, sans-serif; font-size:14px;
							  padding:5px; border:none; color:#ffffff; cursor:pointer; margin:10px 0px 0px 5px; }
.ContentOtr					{ border: 1px solid #CCCCCC; margin: 5px 0 80px 5px; min-height: 500px; padding: 5px;
						       position: relative; top: 45px; width: 98.3%; padding-top:0px;}
.TxtBox						{ width:270px; height:24px; background-color:#ffffff; border:1px solid #999999; float:left; margin:10px 0px 0px 10px;
							  font-family:Tahoma, Geneva, sans-serif; font-size:12px; color:#000000; text-decoration:none; }			
.ErrorField {
    border: 1px solid #DD0000;
    background: #FFFFFE;
}
#AvalliblityError {
    float: right;
    height: 25px;
    line-height: 20px;
    text-align: center;
    width: 80%;
}
span.ValidationErrors {
    color: #DD0000;
    display: block;
    float: right;
    font-size: 12px;
    padding-left: 10px;
    position: relative;
    width:50%;
}
.ValidationErrors p {
    display: block;
    font-size: 12px;
    color: #D00;
    margin:2px 0;
}
.required_field {
	margin-left:2px;
	color: #D00;
}							  
</style>
<script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(function(){
	jQuery("#username").validate({
        expression: "if (VAL.trim()) return true; else return false;",
        message: "Please Enter User Name."
    });
	jQuery("#password").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter Password."
    });
});

$(document).ready(function(){
	$("#username").focus();
});
</script>
	
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr"><img src="../images/theorem_logo.jpg" width="118" height="29" /></div>
</div>
<div class="MainLoginOtr">
	<div class="LoginHeadingArea">Rhythm</div>
	<form action="../login/loginController.php" method="POST">
		<div class="LoginOtr">
		 	<div class="Logincontent">
		 		<div class="LoginBG"></div>
		    	<input type="text" class="TxtBox" value="" name="username" id="username" />
		    </div>
		    <div class="Logincontent">
		 		<div class="PSWBG"></div>
		    	<input type="password" class="TxtBox" value="" name="password" id="password" />
		    </div>
		    <div style="margin-left: 82px;color: red;">
		 		<?php echo $error_msg; ?>
		    </div>
		    <input type="submit" name="submit" value="Log in" class="LoginBtn">
		</div>
	</form>
</div>
</body>
</html>
	