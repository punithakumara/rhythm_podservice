<?php
session_start();
if(!isset($_SESSION['authemail'])) 
	header("Location: issuelogin.php");
  $empName = isset($_SESSION['authname'])?$_SESSION['authname']:'';
  $admin = isset($_SESSION['admin'])?$_SESSION['admin']: 0;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	<title>Rhythem</title>
		<style>
			.RightOtr					{ width:auto; float:right; margin-right:5px; height:25px; text-align:right; margin-top:10px; }
			.RightOtr span				{ font-family:Tahoma, Geneva, sans-serif; font-size:12px; color:#ffffff; line-height:25px; float:left; margin:0 5px; }
			.Header {  text-align:center; background-color: #f2f2f2; color: #005589; font-size: 12px; font-weight: 600; height: 25px; line-height: 25px; margin: 0; padding: 0;   width: 100%;}
			.link_bar a { padding:6px; font-weight:bold; float:right; color:#d0d0d0; outline: none;}
			.link_bar a:link { color:#d0d0d0;  }
			.link_bar a:visited { color:#d0d0d0; }
			.link_bar a:hover { color:#d0d0d0; background-color:#005589; }
			.link_bar a:active { color:#d0d0d0; }
		</style>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" language="javascript" src="../js/jquery-1.3.2.js"></script>
		<script type="text/javascript" language="javascript" src="../js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#issues').dataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bFilter": true,
					"bDestroy": true, 
					"sPaginationType": "full_numbers",
					"sDom": '<"top"lfp>rt<"bottom"ip><"clear">',
					"oLanguage": {
					"sLengthMenu": "Display _MENU_ records per page",
					"sZeroRecords": "No data found",
					"sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
					"sInfoEmpty": "Showing 0 to 0 of 0 records",
					"sInfoFiltered": "(filtered from _MAX_ total records)",
					"sProcessing" : "",
					"oPaginate": {
						 "sFirst": "First",
						 "sLast": "Last",
						 "sPrevious": "Previous",
						 "sNext": "Next"
					}
				  },
					"sAjaxSource": "issueProcessing.php"					
				} );
			} );
		</script>		
	</head>
	
	<body id="dt_example">
		<div class="TopHeader">
			<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
			<div class="RightOtr">
				<span>Hi &nbsp;<strong><?php echo $empName; ?></strong>&nbsp;</span>
				<a href="../login/logout.php?action=issue" title="Logout"><img width="16" height="22" border="0" src="../images/logout_btn.png"></a>
			</div>
		</div>
		<div id="container">		
			<h2 class="Header"> List issue details </h2>
			<?php if( $admin == 1) { ?>
				<div class='link_bar'>
					<a href="listinnovation.php" title="Logout">Innovations</a>
					<a href="listempuan.php" title="Logout">UAN</a> 
				</div>
			<?php } ?>
			<div class="theorem">
				<input id="saveForm" class="btTxt"  type="button" value="Add New" onclick="window.location.href='Issues.php';"/>
				<div class="exportBtn">
				<div id="ajaxLoader" style="display:none;"><img src="../images/loader.gif" alt="Loading..."></div>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="issues">
				<thead>
					<tr style="background-color:#005589;">
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Employee ID</span></th>
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Employee Name</span></th>
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Employee Email</span></th>
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Module</span></th>
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Submodule</span></th>
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Description</span></th>
						<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Status</span></th>
						<!-- <th width="14%"><span style="color:#FFFFFF; font-weight:normal">Version</span></th> -->
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="5" class="dataTables_empty">Loading data from server</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="Footer">&copy; 2013-14 Theorem</div>
	</body>
</html>