<?php

	ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
	ini_set('memory_limit', '-1');
	
	require ('../config/mysql_crud.php');
	
	$db = new Database();
	
	$data = array();
	$tids = array();
	$surveyID = $_GET['SurveyID'];
	$SenderEmployee = $_GET['SenderEmpID'];
	
	$Title = "Invitation to participate in a Theorem survey";
	$ReadFlag = 0;
	$Type = 4;
	$DeleteFlag = 0;
	
	$mysqli = mysqli_connect(SURVEY_HOST,SURVEY_NAME,SURVEY_PASSWORD,SURVEY_DATABASE);
	// Check connection
	if (mysqli_connect_errno())
	{
		$data['mysql_error'] = "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$surveysql = "Select tid, email, token FROM lime_tokens_".$surveyID;
	$result = $mysqli->query($surveysql);
	//$i=0;
	if ($result->num_rows > 0) 
	{
	    while($row = $result->fetch_assoc()) 
	    {
	    	$token = $row["token"];
	    	$email = $row["email"];
	    	$tid = $row["tid"];
			$Description = SURVEY_TRIGGER_IP_ADDRESS."index.php/survey/index/sid/".$surveyID."/token/".$token."/lang/en";
			
			$EMPDatasql = "SELECT EmployID FROM employ WHERE Email = '$email'";
	    	$db->sql($EMPDatasql);
	        $EmployID = $db->getResult();
	        $ReceiverEmployee = $EmployID['EmployID'];
        
			$NotificationDate = date("Y-m-d h:i");
			
			$notificationsql = "INSERT INTO notification 
				(Title, Description, SenderEmployee, ReceiverEmployee, NotificationDate, ReadFlag, Type, DeleteFlag)
				VALUES ('$Title', '$Description', $SenderEmployee, $ReceiverEmployee, '$NotificationDate', $ReadFlag, $Type, $DeleteFlag)";
			
			if($db->sql($notificationsql))
			{
				array_push($tids, $tid);
			}
			/*if($i == 5)break;
			$i++*/;
	    }
	    
	    $NotificationDate = date("Y-m-d h:i");
	    $tids = implode(",",$tids);
	    $surveySentSql = "UPDATE lime_tokens_".$surveyID." SET sent='$NotificationDate' WHERE tid IN($tids)";
	    $res = $mysqli->query($surveySentSql);
	} 
	else 
	{
	    $data['zeroresult'] = "0 results";
	}
	
	$mysqli->close();
	
	$surveyTrackUpdateSql = "UPDATE survey_track SET surveyNotified = 1 WHERE surveyID=$surveyID";
	if($db->sql($surveyTrackUpdateSql))
	{
		$data['success'] = "Success";
	}
	else 
	{
		$data['error'] = "Error";
	}
	
	$callback = $_REQUEST['callback'];
    if ($callback) 
    {
		header('Content-Type: text/javascript');
		echo $callback . '(' . json_encode($data) . ');';
	} 
	else 
	{
	  header('Content-Type: application/x-json');
	  echo json_encode($data);
	}