<?php
class MysqlConnect 
{
	private $db;
	public $con;
	public function __construct()
	{
		if($this->setConnection())
		{
			$dbname = "rhythm_new";
			// $dbname = "rhythmQA";
			// $dbname = "rhythm_live_21_02_2018";
			$this->setDatabase($dbname) ;
			date_default_timezone_set('Asia/Kolkata');
		}		
	}
	
	private function setConnection()
	{
		// $this->con = mysql_connect('172.19.18.98','admin','@dm1n') or die("Unable to connect to MySQL"); //dev
		$this->con = mysqli_connect('localhost','root','') or die("Unable to connect to MySQL"); //local
		// $this->con = mysql_connect('172.19.19.229','shivaprasad','Sprasad!123') or die("Unable to connect to MySQL"); //QA
		return true;
	}
	
	private function setDatabase($db)
	{
		mysqli_select_db($this->con,$db)or die("Could not connect to database");
	}
}