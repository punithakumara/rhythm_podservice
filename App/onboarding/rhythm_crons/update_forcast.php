<?php
	ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
	ini_set('memory_limit', '-1');
	require ('../config/mysql_crud.php');
	//require_once('../phpmailer/class.phpmailer.php');
	date_default_timezone_set('Asia/Kolkata');
		
	$db = new Database();
	
/*for Multiple Days*/
/* $date = '2018-01-12';
$end_date = '2018-01-19';

while (strtotime($date) <= strtotime($end_date)) {
$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
$msg = forcast($date);
	echo $msg;
}    
 */

	 // $date = '2018-01-16';
	$date = date("Y-m-d");
	$msg = forcast($date);
	echo $msg; 
	
	
	
	/*Get all Forecast work orders for current date */	
	function forcast($today){
			
		$db = new Database();
		$forecastsql = "SELECT * FROM client_forecast WHERE start_date = '$today' AND status = 0";
		$db->sql($forecastsql);
		$forcast_data = $db->getResult();
		
		if (!empty($forcast_data))
		{
		
			if (count($forcast_data) == count($forcast_data, COUNT_RECURSIVE))
			{
				if ($today == $forcast_data['start_date'])
				{
					if ($forcast_data['forecast_impact'] == 1)
					{					
						$get_current_fte = get_current_fte($forcast_data['client_id']);			
						$get_current_appr_buffer = get_current_appr_buffer($forcast_data['client_id']);			
						$final_count = $get_current_fte + $forcast_data['forecast_fte'];				
						$final_buffer_count = $get_current_appr_buffer + $forcast_data['forecast_appr_buffer'];			
							
						$update_headcount = "UPDATE client SET `head_count` = ".$final_count.", approved_buffer = ".$final_buffer_count ." WHERE client_id=".$forcast_data['client_id'];
						$db->sql($update_headcount);	
						$update_forcast_table = update_forcast_table($forcast_data['id']);
						
						$msg = "Records Updated for $today  !!!!!!!!!!!!!!!";
						return $msg;						
					}

					if ($forcast_data['forecast_impact'] == 2)
					{
						$get_current_fte = get_current_fte($forcast_data['client_id']);			
						$get_current_appr_buffer = get_current_appr_buffer($forcast_data['client_id']);
						
						$final_count = $get_current_fte - $forcast_data['forecast_fte'];	
						$final_buffer_count = $get_current_appr_buffer - $forcast_data['forecast_appr_buffer'];	
						
						if($final_count < 0){
							$final_count = 0;
						}
						if($final_buffer_count < 0){
							$final_buffer_count = 0;
						}
						
							
						$update_headcount = "UPDATE client SET `head_count` = ".$final_count.", approved_buffer = ".$final_buffer_count ." WHERE client_id=".$forcast_data['client_id'];
						$db->sql($update_headcount);	
						$update_forcast_table = update_forcast_table($forcast_data['id']);
						
						$msg = "Records Updated for $today  !!!!!!!!!!!!!!!";
						return $msg;						
					}					
				}
			}
			else
			{				
				foreach ($forcast_data as $key=>$value)
				{									
					if ($today == $value['start_date'])
					{
						if ($value['forecast_impact'] == 1)
						{					
							$get_current_fte = get_current_fte($value['client_id']);
							$get_current_appr_buffer = get_current_appr_buffer($value['client_id']);							
							$final_count = $get_current_fte + $value['forecast_fte'];	
							$final_buffer_count = $get_current_appr_buffer + $value['forecast_appr_buffer'];									
							$update_headcount = "UPDATE client SET `head_count` = ".$final_count.", approved_buffer = ".$final_buffer_count ." WHERE client_id=".$value['client_id'];
							$db->sql($update_headcount);	
							$update_forcast_table = update_forcast_table($value['id']);
							
						}

						if ($value['forecast_impact'] == 2)
						{
							$get_current_fte = get_current_fte($value['client_id']);	
							$get_current_appr_buffer = get_current_appr_buffer($value['client_id']);							
							$final_count = $get_current_fte - $value['forecast_fte'];	
							$final_buffer_count = $get_current_appr_buffer - $value['forecast_appr_buffer'];
							
							if($final_count < 0){
								$final_count = 0;
							}
							if($final_buffer_count < 0){
								$final_buffer_count = 0;
							}
							
							$update_headcount = "UPDATE client SET `head_count` = ".$final_count.", approved_buffer = ".$final_buffer_count ." WHERE client_id=".$value['client_id'];
							$db->sql($update_headcount);	
							$update_forcast_table = update_forcast_table($value['id']);							
						}					
					}
				}
				
						$msg = "Records Updated for  $today  !!!!!!!!!!!!!!!";
						return $msg;
			}
		}else{
			$msg = "No records Available for  $today  !!!!!!!!!!!!!!!";
			return $msg;
		}
	}
	/* get current FTE of client*/
	function get_current_fte($client_id)
	{
		$db = new Database();
		$head_count_sql = "SELECT head_count FROM client WHERE client_id = ".$client_id;
		$db->sql($head_count_sql);
		$headcount_data = $db->getResult();		
		return $headcount_data['head_count'];	
	}
	
	/* get current Approved Buffer of client*/
	function get_current_appr_buffer($client_id)
	{
		$db = new Database();
		$approved_buffer = "SELECT approved_buffer FROM client WHERE client_id = ".$client_id;
		$db->sql($approved_buffer);
		$approved_buffer_data = $db->getResult();		
		return $approved_buffer_data['approved_buffer'];	
	}
	
	/* Update Forcast Table*/
	function update_forcast_table($id)
	{
		$db = new Database();
		$update_forecast = "UPDATE client_forecast SET `status` = '1' WHERE id=".$id;
		$db->sql($update_forecast);	
			return '';
	}