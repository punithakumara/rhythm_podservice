<?php

	ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
	ini_set('memory_limit', '-1');
	
	require ('../config/mysql_crud.php');
	require_once('../phpmailer/class.phpmailer.php');
	date_default_timezone_set('Asia/Kolkata');
		
	$db = new Database();
	
	/*An existing WOR auto closes on the expiry date */	
	$clientsql = "SELECT DISTINCT `client_id`, `client_name` FROM `client` WHERE `status` = 1";
	$db->sql($clientsql);
	$clientDatas = $db->getResult();
	
			 
	foreach ($clientDatas as $key=>$value)
	{				
		$getProcessQuery = "SELECT `HeadCount` FROM `process` WHERE `ClientID` = ".$value['client_id'];
		$db->sql($getProcessQuery);
		$headcountdata = $db->getResult();
					
		$count = 0;				
		if (!empty($headcountdata))
		{															
				foreach ($headcountdata as $headcount)
				{					
					if (count($headcountdata) == count($headcountdata, COUNT_RECURSIVE))
					{
						$count += $headcount;
					}
					else
					{						
						$count += $headcount['HeadCount'];
						
					}			
					
			    } 							
		}
		
		$clientDatas[$key]['Afte'] = $count;
	}
		
	foreach ($clientDatas as $insertCount)
	{	
		$query = "UPDATE client SET `head_count` = ".$insertCount['Afte']." WHERE client_id=".$insertCount['client_id'];
	    $db->sql($query);
	
	}