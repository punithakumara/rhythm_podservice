<?php
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class MBRClientUtilizationWeekendSupportCron extends MysqlConnect{
	
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}
		
 	public function UtilizationWeekendSupport($today='')
	{
		
		// $today = date('Y-m-d', strtotime("2018-02-05"));//'2018-01-03';
		
		$day = date('D', strtotime($today));
			
		$Util = array();
		$query ="select utilization_id , project_code,client_id from utilization where task_date =  '$today'  "; 		
			
		$result =  mysql_query($query) or die(mysql_error()."Query error");
	 
		while($res = mysql_fetch_assoc($result)) 
		{ 
			$Util[] = $res;
		}
			
		if (!empty($Util)) 
		{
			foreach($Util as $key => $value)
			{
				$project_code = (explode("_",$value['project_code']));
				$model = $project_code[1];
				$support = $project_code[4];
					
					// echo $today;echo "<BR>";
					
				
				$weekend_support = $this->WeekendSupport($day,$support);
				
				if($weekend_support == 0){
				$project_id = $this->GetProjectIdFromCode($value['project_code']);
				
			
				
				$support_type = $this->HolidayCoverage($value['client_id'],$project_id,$today);
				
				}else{
				$support_type = $weekend_support;
				}
				
				$update_record = $this->UpdateRecord($support_type,$value['utilization_id'],$today);
				
			}
			
			
		}
			
		echo "<PRE>";print_r($Util);
		return '';
	}
	
	
	function UpdateRecord($weekend_support,$utilization_id,$today)
	{
		$update_headcount = "UPDATE utilization SET support_type = $weekend_support WHERE utilization_id = $utilization_id  AND task_date =  '$today' " ;
		
		// echo $update_headcount;echo "<BR>"; return '';
		if( mysql_query($update_headcount) )
		{
			$msg =  "Rec Updated Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  Updating";
		} 
		
		return $msg;
	}
	
	function GetProjectIdFromCode($project_code)
	{
		//return $project_code;
		$sql = "SELECT process_id FROM PROCESS  WHERE name =  '$project_code'  LIMIT 1";
		
		$result = mysql_query($sql);

		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$process_id = $res['process_id'];
				
			}
			return $process_id;
		}
	}
	
	function WeekendSupport($day,$support)
	{
		$query = "SELECT weekend_support FROM support_coverage WHERE  short_code= '$support' ";
		
		$result = mysql_query($query);
		$output = 0;
		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$support = explode(",",$res['weekend_support']);
			}
			
			foreach ($support as $val) {
				
				if (strpos($day, $val) !== FALSE) { 
					$output = 1;
				}
			}
			
			return $output;
			//echo"<PRE>";print_r($support);
			//return $support;
		}
		
	}
	
	function HolidayCoverage($client_id,$project_id,$date)
	{
		$sql = "SELECT holidays.date FROM `client_holidays` JOIN `holidays` ON holidays.`holiday_id` = `client_holidays`.`holiday_id` WHERE client_id = '$client_id' AND process_id = '$project_id' AND DATE = '$date'";
		$result = mysql_query($sql);
		
		if(mysql_num_rows($result) > 0) {
			return 2;
		}else{
			return  0;
		}
		
	}

}

// for only one day
// $date="2018-02-05";

/*Its better to run the cron in daily basis to avoid the overwriting of records which were available in the past
*/
 
/* $date = date("Y-m-d");
$obj  = new MBRClientUtilizationWeekendSupportCron();
$obj->UtilizationWeekendSupport($date); 

  */
   

$obj  = new MBRClientUtilizationWeekendSupportCron();
$date = '2017-12-31';
$end_date = '2018-02-21';

while (strtotime($date) <= strtotime($end_date)) {
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->UtilizationWeekendSupport($date);
	
}    
    
   
/*    
$obj = new MBRClientUtilizationWeekendSupportCron();

$s_date = date('Y-m-d',date(strtotime("-6 day")));
$e_date = date("Y-m-d",strtotime("+1 day"));  

$begin = new DateTime($s_date);
$end = new DateTime($e_date);
 
$interval = DateInterval::createFromDateString('1 day');

$period = new DatePeriod($begin, $interval, $end);
     
foreach ( $period as $dt )
{
	$obj->UtilizationWeekendSupport($dt->format("Y-m-d"));     
}  
  
  
  
  */ 
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>

