<?php 
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class Dct extends MysqlConnect{
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}

	function getEmployees()
	{
        $sql = "CALL sp_dct_employ()";
        $result = mysqli_query($this->con,$sql);

		$dbhost = 'localhost';
		$dbuser = 'root';
		$dbpass = '';
		$dbname = 'rhythm_new';
		$tables = 'tbl_dct_employees';

		//Call the core function
		$this->backup_tables($dbhost, $dbuser, $dbpass, $dbname, $tables);
	}

	function backup_tables($host, $user, $pass, $dbname, $tables) {
    $link = mysqli_connect($host,$user,$pass, $dbname);

    // Check connection
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit;
    }

    mysqli_query($link, "SET NAMES 'utf8'");

    //get all of the tables
    if($tables == '*')
    {
        $tables = array();
        $result = mysqli_query($link, 'SHOW TABLES');
        while($row = mysqli_fetch_row($result))
        {
            $tables[] = $row[0];
        }
    }
    else
    {
        $tables = is_array($tables) ? $tables : explode(',',$tables);
    }

    $return = '';
    //cycle through
    foreach($tables as $table)
    {
        $result = mysqli_query($link, 'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);
        $num_rows = mysqli_num_rows($result);

        $return.= 'DROP TABLE IF EXISTS '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($link, 'SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";
        $counter = 1;

        //Over tables
        for ($i = 0; $i < $num_fields; $i++) 
        {   //Over rows
            while($row = mysqli_fetch_row($result))
            {   
                if($counter == 1){
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                } else{
                    $return.= '(';
                }

                //Over fields
                for($j=0; $j<$num_fields; $j++) 
                {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j<($num_fields-1)) { $return.= ','; }
                }

                if($num_rows == $counter){
                    $return.= ");\n";
                } else{
                    $return.= "),\n";
                }
                ++$counter;
            }
        }
        $return.="\n\n\n";
    }

    //Sending email for confirmation
                $subject = "DC Test Report";
                 $message = "<p style='font-size:15px;'>Hi Team,</p>";
                 // $message .= "<p style='font-size:15px;'>Rhythm to Zoho synchronization.</p>";
                 //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$ShiftResult."</b></p>";
                 //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$VerticalResult."</b></p>";
                 $message .= "<p style='font-size:15px;'>Please find the attachment for DC Test employees sql file @ ".date("Y-m-d H:i:s").".</p>";
                 $message .= "<p style='font-size:15px;'>For any further assistance or queries, reach out to <a href='mailto:rhythmsupport@theoreminc.net'>rhythmsupport@theoreminc.net</a></p>";
                 //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$EmpIds."</b></p>";
                 $message .= "</br>";
                 $message .= "Thank you,</br>";
                 $message .= "Rhythm Support</br>";
               // $Mailresult = $this->sendMail('Rhythmsupport@theoreminc.net',$subject,$message);

    //save file
    $fileName = 'C:/wamp64/www/Rhythm/App/services/download/DCT.sql';
    $handle = fopen($fileName,'w+');
    fwrite($handle,$return);
    $this->sendMail($fileName,$subject, $message);
    if(fclose($handle)){
        echo "Done, the file name is: ".$fileName;
        //exit; 
    }
}

	function sendMail($esat, $subject, $message)
		{
			require_once('C:/wamp64/www/Rhythm/App/onboarding/phpmailer/class.phpmailer.php');
			ini_set('max_execution_time', 300);
			$mail = new PHPMailer(true);
			$mail->IsSMTP();                       // telling the class to use SMTP
			$mail->SMTPDebug = 0;
		
			// 0 = no output, 1 = errors and messages, 2 = messages only.
			$mail->SMTPDebug = 1;
			$mail->SMTPAuth = true;                // enable SMTP authentication
			$mail->SMTPSecure = "tls";              // sets the prefix to the servier
			$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
			$mail->Port = 587;                    // set the SMTP port for the GMAIL
			$mail->Username = "rhythmsupport";  // Gmail username
			$mail->Password = "rhy_th499";      // Gmail password
			$mail->ContentType = 'text/html';
			$mail->IsHTML(true);
			$mail->CharSet = 'windows-1250';
			$mail->From = 'rhythmsupport@theoreminc.net';
			$mail->FromName = 'Rhythmsupport@theoreminc.net';
			$mail->addReplyTo('punithakumara.gopalakrishna@theoreminc.net');
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->AddAddress ('punithakumara.gopalakrishna@theoreminc.net');
			$mail->AddAttachment($esat);
			$error_message = "";
			
			if(!$mail->Send())
			{
				$error_message = "Error in Sending Email Mailer Error: " . $mail->ErrorInfo;
			} else {
				$error_message = "Mail Successfully sent!";
			}
		}	
}

$obj  = new Dct();
// $date = '2019-02-28';
// $end_date = '2019-03-31';
$date = date("Y-m-t", strtotime("last month"));
$end_date = date("Y-m-t");

// while (strtotime($date) <= strtotime($end_date)) {
// 	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->getEmployees();
// }    
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>