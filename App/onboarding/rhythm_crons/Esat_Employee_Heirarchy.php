<?php 
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class EsatEmployeeHeirarchy extends MysqlConnect{
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}

	function getEmployees()
	{
		ini_set('max_execution_time', 0);
		ini_set("memory_limit","2048M"); 

		$path = "C:/wamp64/www/Rhythm/App/services/download/";
	    $esat = "C:/wamp64/www/Rhythm/App/services/download/EsatEmployeeHierarchy.csv";
		$filename = "EsatEmployeeHierarchy.csv";

		$index =0;
		$data = array();

		$query = "SELECT employees.company_employ_id AS ReporteeEmpID,
		CONCAT(IFNULL(employees.first_name, '') ,' ', IFNULL(employees.last_name,'')) AS ReporteeName, 
		empd.name AS ReporteeDesignation,
		employees.email AS email, 
		verticals.name AS Vertical, 
		sup.company_employ_id AS SupervisorEmpID, 
		CONCAT(IFNULL(sup.first_name, '') ,' ', IFNULL(sup.last_name,'')) AS SupervisorName, 
		supd.name AS SupervisorDesignation, 
		empd.grades AS ReporteeGrade, supd.grades AS SupervisorGrade
		FROM employees
		LEFT JOIN employee_vertical ev ON employees.employee_id=ev.employee_id
		LEFT JOIN employees sup ON sup.employee_id = employees.primary_lead_id
		LEFT JOIN designation empd ON empd.designation_id = employees.designation_id
		LEFT JOIN designation supd ON supd.designation_id = sup.designation_id
		LEFT JOIN verticals ON verticals.vertical_id = ev.vertical_id
		WHERE employees.status!=6 AND employees.company_employ_id!=0 AND employees.company_employ_id NOT LIKE 'C%'
		ORDER BY ReporteeName ASC";

		$result = mysqli_query($this->con,$query);

		//if(mysqli_num_rows($result) > 0) {
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$employees[] = $res;
			}
			//var_dump($employees);exit;
			$return_array = array();
			foreach($employees as $key => $value)
			{
				$return_array = array_keys($value);
				$return_array = str_replace("_"," ",$return_array);	
				$return_array = array_map('ucfirst', $return_array);
			}

			$result1 = array_merge(array(array_unique($return_array)), $employees);
			//return array($result1);

			var_dump($result1);

			$result1[count($result1)] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");			
			ob_start();
			$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
			$n = 0;     
			//var_dump($result1[0]);   
			foreach ($result1 as $line)
			{
				//var_dump($line);
				$n++;
				if ( ! fputcsv($f, $line))
				{
					//show_error("Can't write line $n: $line");
				}
			}
			fclose($f) or show_error("Can't close php://output");
			$str = ob_get_contents();
			ob_end_clean();

			//Sending email for confirmation
		        $subject = "ESAT Employee Hierarchy Report";
		         $message = "<p style='font-size:15px;'>Hi Team,</p>";
		         // $message .= "<p style='font-size:15px;'>Rhythm to Zoho synchronization.</p>";
		         //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$ShiftResult."</b></p>";
		         //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$VerticalResult."</b></p>";
				 $message .= "<p style='font-size:15px;'>Please find the attachment for ESAT Employee Hierarchy @ ".date("Y-m-d H:i:s").". Please check the logs for more details</p>";
	             $message .= "<p style='font-size:15px;'>For any further assistance or queries, reach out to <a href='mailto:rhythmsupport@theoreminc.net'>rhythmsupport@theoreminc.net</a></p>";
		         //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$EmpIds."</b></p>";
		         $message .= "</br>";
		         $message .= "Thank you,</br>";
		         $message .= "Rhythm Support</br>";
		       // $Mailresult = $this->sendMail('Rhythmsupport@theoreminc.net',$subject,$message);

			return $this->sendMail($esat, $subject, $message);
		//}
	}

	function sendMail($esat, $subject, $message)
		{
			require_once('C:/wamp64/www/Rhythm/App/onboarding/phpmailer/class.phpmailer.php');
			ini_set('max_execution_time', 300);
			$mail = new PHPMailer(true);
			$mail->IsSMTP();                       // telling the class to use SMTP
			$mail->SMTPDebug = 0;
		
			// 0 = no output, 1 = errors and messages, 2 = messages only.
			$mail->SMTPDebug = 1;
			$mail->SMTPAuth = true;                // enable SMTP authentication
			$mail->SMTPSecure = "tls";              // sets the prefix to the servier
			$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
			$mail->Port = 587;                    // set the SMTP port for the GMAIL
			$mail->Username = "rhythmsupport";  // Gmail username
			$mail->Password = "rhy_th499";      // Gmail password
			$mail->ContentType = 'text/html';
			$mail->IsHTML(true);
			$mail->CharSet = 'windows-1250';
			$mail->From = 'rhythmsupport@theoreminc.net';
			$mail->FromName = 'Rhythmsupport@theoreminc.net';
			$mail->addReplyTo('punithakumara.gopalakrishna@theoreminc.net');
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->AddAddress ('punithakumara.gopalakrishna@theoreminc.net');
			$mail->AddAttachment($esat);
			$error_message = "";
			
			if(!$mail->Send())
			{
				$error_message = "Error in Sending Email Mailer Error: " . $mail->ErrorInfo;
			} else {
				$error_message = "Mail Successfully sent!";
			}
		}	
}

$obj  = new EsatEmployeeHeirarchy();
// $date = '2019-02-28';
// $end_date = '2019-03-31';
$date = date("Y-m-t", strtotime("last month"));
$end_date = date("Y-m-t");

// while (strtotime($date) <= strtotime($end_date)) {
// 	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->getEmployees();
// }    
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>