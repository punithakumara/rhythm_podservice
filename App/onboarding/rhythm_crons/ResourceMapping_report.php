<?php 
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class ResourceMappingReport extends MysqlConnect{
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}

	function getEmployees()
	{
		ini_set('max_execution_time', 0);
		ini_set("memory_limit","2048M"); 

		$path = "C:/wamp64/www/Rhythm/App/services/download/";
	    $esat = "C:/wamp64/www/Rhythm/App/services/download/ResourceMappingReport.csv";
		$filename = "ResourceMappingReport.csv";

		$index =0;
		$data = array();

		$query = "SELECT company_employ_id AS EmployeeID, employees.employee_id AS EmpId, CONCAT(IFNULL(employees.`first_name`, ''),' ',
		IFNULL(employees.`last_name`, '')) AS `EmployeeName`, IFNULL(`client_name`, 'Unassigned') AS ClientName, 
		verticals.name AS Vertical,d.grades AS grades, GROUP_CONCAT(DISTINCT CASE WHEN billable = 1 THEN 'Yes' ELSE 'Unassigned' END) AS Billable, 
		IFNULL(`billable_type`, 'Unassigned') AS BillableType, location.name AS location FROM employees 
		LEFT JOIN employee_client ON employees.employee_id = employee_client.employee_id 
		LEFT JOIN employee_vertical ON employee_vertical.employee_id = employees.employee_id 
		LEFT JOIN verticals ON verticals.vertical_id = employee_vertical.vertical_id 
		LEFT JOIN `client` ON employee_client.client_id = client.client_id 
		LEFT JOIN `location` ON location.location_id = employees.location_id 
		LEFT JOIN designation d ON d.designation_id = employees.designation_id
		WHERE employees.status != 6 AND employees.location_id !=4 
		GROUP BY employees.employee_id, employee_client.client_id";

		$result = mysqli_query($this->con,$query);
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$employees[] = $res;
			}

			$data1 = $this->highGradeReport($employees);

			foreach ($data1 as $data_key => $data_value) {
				unset($data1[$data_key]['EmpId']);
				unset($data1[$data_key]['grades']);
			}

			$add = $this->extraReport();

			$add1 = $this->highGradeReport($add);

			$data = array_merge($data1,$add1);
			$return_array = array();
			foreach($data as $key => $value)
			{
				$return_array = array_keys($value);
				$return_array = str_replace("_"," ",$return_array);	
				$return_array = array_map('ucfirst', $return_array);
			}

			$result1 = array_merge(array(array_unique($return_array)), $data);

			var_dump($result1);

			$result1[count($result1)] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");			
			ob_start();
			$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
			$n = 0;      
			foreach ($result1 as $line)
			{
				$n++;
				if ( ! fputcsv($f, $line))
				{
					//show_error("Can't write line $n: $line");
				}
			}
			fclose($f) or show_error("Can't close php://output");
			$str = ob_get_contents();
			ob_end_clean();

			//Sending email for confirmation
		        $subject = "ESAT Employee Hierarchy Report";
		         $message = "<p style='font-size:15px;'>Hi Team,</p>";
		         // $message .= "<p style='font-size:15px;'>Rhythm to Zoho synchronization.</p>";
		         //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$ShiftResult."</b></p>";
		         //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$VerticalResult."</b></p>";
				 $message .= "<p style='font-size:15px;'>Please find the attachment for Resource Management Report @ ".date("Y-m-d H:i:s").".</p>";
	             $message .= "<p style='font-size:15px;'>For any further assistance or queries, reach out to <a href='mailto:rhythmsupport@theoreminc.net'>rhythmsupport@theoreminc.net</a></p>";
		         //$message .= "<p style='font-size:15px;'>Employee ID: <b>".$EmpIds."</b></p>";
		         $message .= "</br>";
		         $message .= "Thank you,</br>";
		         $message .= "Rhythm Support</br>";
		       // $Mailresult = $this->sendMail('Rhythmsupport@theoreminc.net',$subject,$message);

			return $this->sendMail($esat, $subject, $message);
		//}
	}

	function extraReport()
	{
		$additional = array();
		$date = date('Y-m');
		$sql = "SELECT DISTINCT
		EmployeeId
		,EmployeeName
		,`Client Name`
		,Vertical
		,GROUP_CONCAT(DISTINCT Billable) Billable
		,BillableType
		,Location
		FROM
		(
		SELECT DISTINCT em.company_employ_id AS 'EmployeeId'
		,CONCAT(em.first_name,' ',em.last_name) AS 'EmployeeName'
		,(SELECT c.client_name FROM `client` c INNER JOIN employee_client_history ch 
		ON c.client_id=ch.client_id  WHERE ch.employee_id=em.employee_id LIMIT 1) AS 'Client Name'
		,(SELECT v.name 
		FROM verticals v INNER JOIN employee_vertical ev ON v.vertical_id=ev.vertical_id 
		WHERE ev.employee_id=em.employee_id) AS 'Vertical'
		,(SELECT NAME FROM location WHERE location_id=em.location_id) 
		AS 'Location', GROUP_CONCAT(DISTINCT CASE WHEN billable =  1 THEN 'Yes' ELSE 'Unassigned' END) AS Billable
		,IFNULL(`billable_type`, 'Unassigned') AS BillableType 
		FROM employees em
		LEFT JOIN employee_client ON em.employee_id = employee_client.employee_id
		LEFT JOIN `client` ON employee_client.client_id = client.client_id
		WHERE em.relieve_date LIKE '%$date%' 
		GROUP BY em.employee_id
		ORDER BY em.first_name ASC
		) AS e 
		GROUP BY 
		 EmployeeName
		,`Client Name`
		,Vertical
		,BillableType";

			$relieve = mysqli_query($this->con,$sql);

		while($res = mysqli_fetch_assoc($relieve)) 
			{ 
				$additional[] = array_merge($res);
			}
			return $additional;
	}

	function highGradeReport($data)
	{
		// debug($data);exit;
		$employees_data = $data;
		$highGradeEmpData = array();

		foreach ($employees_data as $e_key => $e_value) 
		{
			// debug($e_value);exit;

			if($e_value['ClientName']=='Unassigned')
			{
				$employees_data[$e_key]['Billable'] = 'Unassigned';
				$employees_data[$e_key]['BillableType'] = 'Unassigned';
			}
			else
			{
				if($e_value['BillableType'] == 'Buffer')
				{
					$employees_data[$e_key]['Billable'] = 'No';
				}
				else
				{
					$employees_data[$e_key]['Billable'] = 'Yes';
					// $employees_data[$e_key]['BillableType'] = $e_value['BillableType'];
				}
			}

			if( $e_value['grades']>2 && $e_value['grades']<7)
			{
				$e_value['e_key'] = $e_key;
				$highGradeEmpData[] = $e_value;
			}
		}

		// return $employees_data;
		// debug($highGradeEmpData);exit;
		// debug($employees_data);exit;

		$wor_sql = "SELECT client,client_name,team_lead,delivery_manager,associate_manager 
		FROM wor w
		lEFT JOIN client c ON c.client_id = w.client
		WHERE w.status = 'open'";

		$wor_query = mysqli_query($this->con,$wor_sql);

		//if(mysqli_num_rows($result) > 0) {
			
			while($res = mysqli_fetch_assoc($wor_query)) 
			{ 
				$wor_result[] = $res;
			}

		//$wor_query = $this->db->query($wor_sql);

		//$wor_result = $wor_query->result_array();
		$wor_result = array_unique($wor_result, SORT_REGULAR);

		// debug($wor_result);exit;

		$highGradeWorData = array();
		
		foreach ($highGradeEmpData as $hg_key => $hg_value) 
		{
			$company_employ_id = $hg_value['EmployeeID'];
			$employee_id = $hg_value['EmpId'];
			$employee_name = $hg_value['EmployeeName'];
			$vertical = $hg_value['Vertical'];
			$grade = $hg_value['grades'];
			$location = $hg_value['location'];
			$Billable = $hg_value['Billable'];
			$BillableType = $hg_value['BillableType'];
			if($Billable == 'Unassigned' && $BillableType == 'Unassigned')
			{
				$Billable = 'No';
				$BillableType = 'Buffer';
			}

			if($grade > 2 && $grade < 4)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$team_lead = explode(',',$wor_res_value['team_lead']);
					if(in_array($employee_id, $team_lead))
					{
						$grades_wor_data['EmployeeID']		= $company_employ_id;
						$grades_wor_data['EmpId']		= $employee_id;
						$grades_wor_data['EmployeeName']	= $employee_name;
						$grades_wor_data['ClientName']		= $wor_res_value['client_name'];
						$grades_wor_data['Vertical']		= $vertical;
						$grades_wor_data['grades']			= $grade;
						$grades_wor_data['Billable']		= $Billable;
						$grades_wor_data['BillableType']	= $BillableType;
						$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
						$highGradeWorData[] = $grades_wor_data;
						unset($employees_data[$hg_value['e_key']]);
					}
				}
			}
			elseif($grade >= 4 && $grade < 5)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$associate_manager = explode(',',$wor_res_value['associate_manager']);
					if(in_array($employee_id, $associate_manager))
					{
						$grades_wor_data['EmployeeID']		= $company_employ_id;
						$grades_wor_data['EmpId']		= $employee_id;
						$grades_wor_data['EmployeeName']	= $employee_name;
						$grades_wor_data['ClientName']		= $wor_res_value['client_name'];
						$grades_wor_data['Vertical']		= $vertical;
						$grades_wor_data['grades']			= $grade;
						$grades_wor_data['Billable']		= 'No';
						$grades_wor_data['BillableType']	= 'Buffer';
						$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
						$highGradeWorData[] = $grades_wor_data;
						unset($employees_data[$hg_value['e_key']]);
					}
				}
			}
			elseif($grade >= 5 && $grade < 6)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$delivery_manager = explode(',',$wor_res_value['delivery_manager']);
					if(in_array($employee_id, $delivery_manager))
					{
						$grades_wor_data['EmployeeID']		= $company_employ_id;
						$grades_wor_data['EmpId']		= $employee_id;
						$grades_wor_data['EmployeeName']	= $employee_name;
						// $grades_wor_data['ClientName']		= $wor_res_value['client_name'];
						$grades_wor_data['ClientName']		= "Theorem India Pvt Ltd";
						$grades_wor_data['Vertical']		= $vertical;
						$grades_wor_data['grades']			= $grade;
						$grades_wor_data['Billable']		= 'No';
						$grades_wor_data['BillableType']	= 'Buffer';
						$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
						$highGradeWorData[] = $grades_wor_data;
						unset($employees_data[$hg_value['e_key']]);
					}
				}
			}
			elseif($grade >= 6 && $grade < 7)
			{
				foreach ($wor_result as $wor_res_key => $wor_res_value) 
				{
					$grades_wor_data['EmployeeID']		= $company_employ_id;
					$grades_wor_data['EmpId']		= $employee_id;
					$grades_wor_data['EmployeeName']	= $employee_name;
					$grades_wor_data['ClientName']		= "Theorem India Pvt Ltd";
					$grades_wor_data['Vertical']		= $vertical;
					$grades_wor_data['grades']			= $grade;
					$grades_wor_data['Billable']		= 'No';
					$grades_wor_data['BillableType']	= 'Buffer';
					$grades_wor_data['location']		= $location;
						// debug($grades_wor_data);
					$highGradeWorData[] = $grades_wor_data;
					unset($employees_data[$hg_value['e_key']]);
				}
			}
		}
		// debug($highGradeWorData);exit;
		$mergedData = array_merge($employees_data,$highGradeWorData);
		$mergedData = array_unique($mergedData, SORT_REGULAR);
		$mergedData = array_values($mergedData);
		return $mergedData;
	}

	function sendMail($esat, $subject, $message)
		{
			require_once('C:/wamp64/www/Rhythm/App/onboarding/phpmailer/class.phpmailer.php');
			ini_set('max_execution_time', 300);
			$mail = new PHPMailer(true);
			$mail->IsSMTP();                       // telling the class to use SMTP
			$mail->SMTPDebug = 0;
		
			// 0 = no output, 1 = errors and messages, 2 = messages only.
			$mail->SMTPDebug = 1;
			$mail->SMTPAuth = true;                // enable SMTP authentication
			$mail->SMTPSecure = "tls";              // sets the prefix to the servier
			$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
			$mail->Port = 587;                    // set the SMTP port for the GMAIL
			$mail->Username = "rhythmsupport";  // Gmail username
			$mail->Password = "rhy_th499";      // Gmail password
			$mail->ContentType = 'text/html';
			$mail->IsHTML(true);
			$mail->CharSet = 'windows-1250';
			$mail->From = 'rhythmsupport@theoreminc.net';
			$mail->FromName = 'Rhythmsupport@theoreminc.net';
			$mail->addReplyTo('punithakumara.gopalakrishna@theoreminc.net');
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->AddAddress ('punithakumara.gopalakrishna@theoreminc.net');
			$mail->AddAttachment($esat);
			$error_message = "";
			
			if(!$mail->Send())
			{
				$error_message = "Error in Sending Email Mailer Error: " . $mail->ErrorInfo;
			} else {
				$error_message = "Mail Successfully sent!";
			}
		}	
}

$obj  = new ResourceMappingReport();
// $date = '2019-02-28';
// $end_date = '2019-03-31';
$date = date("Y-m-t", strtotime("last month"));
$end_date = date("Y-m-t");

// while (strtotime($date) <= strtotime($end_date)) {
// 	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->getEmployees();
// }    
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>