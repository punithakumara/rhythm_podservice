<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MY INNOVATION QUOTIENT</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../css/jquery.validate.css" />
	<link href="../css/style.css" rel="stylesheet" type="text/css">
	<script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
	<script src="../js/jquery.validate.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/myquery.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/tcal.css" />
	<script type="text/javascript" src="../js/tcal.js"></script> 
</head>
<body>

<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
</div>
<div id="container">
<?php
error_reporting(0);
require ('../config/mysql_crud.php');
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
ini_set("smtp", "hub.theoreminc.net");
//$id = $_GET['id'];
$id = isset($_REQUEST["id"])?$_REQUEST["id"]:null;
$db = new Database();

if($id){
 //fetch innovation date and add to form 
 $result = $db->select('innovation','*','','innovation_id='.$id);
 $result_array = $db->getResult(); 
 foreach($result_array as $key => $val) {
 //set all variaables
	  $empId = $result_array['emp_id'];
	  $empName = $result_array['emp_name'];
	  $empEmail = $result_array['emp_email'];
	  $Designation = $result_array['Designation'];
	  $Vertical = $result_array['Vertical'];
	  $idea = $result_array['idea'];
	  $idea_benefit1 = ($result_array['idea_benefit1'])?$result_array['idea_benefit1']:'.';
	  $idea_benefit2 = ($result_array['idea_benefit2'])?$result_array['idea_benefit2']:'.';
	  $idea_benefit3 = ($result_array['idea_benefit3'])?$result_array['idea_benefit3']:'.';
	  $proc_improved = $result_array['proc_improved'];
	  $shortfall1 = ($result_array['shortfall1'])?$result_array['shortfall1']:'.';
	  $shortfall2 = ($result_array['shortfall2'])?$result_array['shortfall2']:'.';
	  $shortfall3 = ($result_array['shortfall3'])?$result_array['shortfall3']:'.';
	  $idea_improved = $result_array['idea_improved']; 
	  $imp_idea_benefit1 = ($result_array['imp_idea_benefit1'])?$result_array['imp_idea_benefit1']:'.';
	  $imp_idea_benefit2 = ($result_array['imp_idea_benefit2'])?$result_array['imp_idea_benefit2']:'.';
	  $imp_idea_benefit3 = ($result_array['imp_idea_benefit3'])?$result_array['imp_idea_benefit3']:'.';
	  $ideaaccepted=$result_array['ideaaccepted'];
	  $responsibility=$result_array['responsibility'];
	  $noreason=$result_array['noreason']; 
	}
}	 
	
$result = $db->select("vertical",'*','','','Name');
$select_vertical = "<option value=''>Select</option>"; 
$result_array = $db->getResult();

	foreach($result_array as $key => $val) {	
		if($id && $val['Name']== $Vertical){
		$select_vertical .= "<option value='".$val['Name']."' selected='selected' >".$val['Name']."</option>";
		}
		else
		{
		$select_vertical .= "<option value='".$val['Name']."'>".$val['Name']."</option>";
		}
    }
	
$result = $db->select("designation",'*','','','Name');
$select_designation = "<option value=''>Select</option>"; 
$result_array1 = $db->getResult();

	foreach($result_array1 as $key => $val1) 
	{	
		if($id && $val1['Name']==$Designation)
		{
				$select_designation .= "<option value='".$val1['Name']."' selected='selected' >".$val1['Name']."</option>";
		}
		else
		{
				$select_designation .= "<option value='".$val1['Name']."'>".$val1['Name']."</option>";
		}		 	
	}
$parameters = $_POST;
if (isset($_POST['submit'])) {	
	$subject = "MY INNOVATION QUOTIENT :".$_POST["innova_emp_id"];	
	$querydata =array();
	$querydata['emp_id'] = $parameters['innova_emp_id'];
	$querydata['emp_name'] =$parameters['innova_emp_name'];
	$querydata['emp_email'] =$parameters['innova_email'];
	if(!$parameters["innovatoinid"]){
	$querydata['Designation'] =$parameters['innova_designation'];
	$querydata['Vertical'] = $parameters['innova_vertical'];
	}
	$querydata['idea'] =$parameters['innova_textarea1'];	
	$querydata['idea_benefit1'] = strlen($parameters['input1'] > 250) ? substr($parameters['input1'],0,249) : $parameters['input1'];
	$querydata['idea_benefit2'] =strlen($parameters['input2'] > 250) ? substr($parameters['input2'],0,249):$parameters['input2'];
	$querydata['idea_benefit3'] = strlen($parameters['input3'] > 250) ? substr($parameters['input3'],0,249):$parameters['input3'];
	$querydata['proc_improved'] = $parameters['innova_textarea2'];
	$querydata['shortfall1'] = strlen($parameters['input4'] > 250) ? substr($parameters['input4'],0,249):$parameters['input4'];
	$querydata['shortfall2'] = strlen($parameters['input5'] > 250) ? substr($parameters['input5'],0,249):$parameters['input5'];
	$querydata['shortfall3'] = strlen($parameters['input6'] > 250) ? substr($parameters['input6'],0,249):$parameters['input6'];
	$querydata['idea_improved'] =$parameters['innova_textarea3'];
	$querydata['imp_idea_benefit1'] = strlen($parameters['input7'] > 250) ? substr($parameters['input7'],0,249):$parameters['input7'];
	$querydata['imp_idea_benefit2'] = strlen($parameters['input8'] > 250) ? substr($parameters['input8'],0,249):$parameters['input8'];
	$querydata['imp_idea_benefit3'] =  strlen($parameters['input9'] > 250) ? substr($parameters['input9'],0,249): $parameters['input9'];
	$querydata['submissiondate'] =  date('Y-m-d');
		
	if($parameters["innovatoinid"] > 0)
	{		
		$status = 'updated'; 
		$uquerydata['ideaaccepted'] = $parameters['accepted'];		
		$uquerydata['responsibility'] = strlen($parameters['responisibility'] > 250) ? substr($parameters['responisibility'],0,249): $parameters['responisibility'];
		$uquerydata['noreason'] = strlen($parameters['reason'] > 250) ? substr($parameters['reason'],0,249): $parameters['reason'];
			
		if($parameters['accepted'] =='Y'){
			$mailsubj = '<html>';			
			$mailsubj  = "<b>Dear ".$parameters["innova_emp_name"].",</b>";		
			$mailsubj .= " <br>Your Innovation idea is Accepted ";
			//$mailsubj .= " <br> <hr><br>Regards,<br> Rhythm Team";
			$mailsubj .='</html>';
			//print $mailsubj ;	 		
			$from= '<TIQ@theoreminc.net>';  
			$to = $parameters["innova_email"] 	;			
			$headers .= 'From:' .$from. "\r\n";
			$mail_status = mail($to, $subject, $mailsubj, $headers);
			if(! $mail_status)
			{
				echo "message was not sent";				
			}		
		}
		$where = 'innovation_id = '.$parameters["innovatoinid"].'';
		$retval = $db->update("innovation", $uquerydata,$where);			
	}
	else
	{
		$status = 'added'; 
		$mailsubj = '<html>';
		$mailsubj  = "<b>Dear</b>,<br>";		
		$mailsubj .= "<br><font color=green> Innovation quotient details as follows</font>"; 
		$mailsubj .= " <br><br> Employee Name : ".$parameters["innova_emp_name"] 	;
		$mailsubj .= " <br> Employee ID : ".$parameters["innova_emp_id"] 	;	
		$mailsubj .= " <br> Vertical : ".$parameters["innova_vertical"] 	;	
		$mailsubj .= " <br> Current Designation : ".$parameters["innova_designation"] 	;	
		$mailsubj .= " <br> Idea submission Date : ".date('d-M-Y'); 	
		$mailsubj .= " <br> <hr> Idea to bring in new Method / System / Process:<br><br><font color=green>".$parameters["innova_textarea1"]."</font>";	
		//$mailsubj .= " <br> <hr><br>Regards,<br> Rhythm Team";
		$mailsubj .='</html>';
		//print $mailsubj ;	 
		
		$from = $parameters["innova_email"] 	;	//'Ideas@theoreminc.net';
		$to = '<TIQ@theoreminc.net>';  // note the comma	
		$headers .= 'From:' .$from. "\r\n";		
		$retval = $db->insert("innovation", $querydata);
	
		if(! $retval )
		{
			die('Could not enter data: ' . mysql_error());
		}
		else{
			$mail_status = mail($to, $subject, $mailsubj, $headers);
			if(! $mail_status)
			{
				echo "message was not sent";				
			}
		}
	}
	if($retval ){
	?>
		<label for="success" class="successful">
			Innovation idea <?php echo $status; ?> successfully !!!
		<?php
			if($status == 'updated'){
				header("Location: listInnovation.php");
			}
		?>
		</label>			
	<?php
	}
				
}	
	
else {
?>
<form name="innovation_quote_form" class="theorem" method="post" onsubmit="return isTextAreaNull();" action="Innovation.php">
	<h1 class="headingTitle">MY INNOVATION QUOTIENT</h1>
    <div class="ContentWrapper">
		<div class="top">
			<table class="formTable">
				<tr>
					<td>
						<label for="Employee" class="desc"> Employee ID:  <font color=red>*</font></label>
						<input type="text" tabindex=1 value="<?php  if(isset($empId)) echo $empId;?>" <?php  if(isset($empId)) { echo "readonly";}?> name="innova_emp_id" id="innova_emp_id" />
					</td>
					<td>
						<label class="desc">Employee Name <font color=red>*</font></label>
						<input type="text" tabindex=2 value="<?php  if(isset($empName)) echo $empName;?>" <?php  if(isset($empId)) { echo "readonly";}?> maxlength="25" name="innova_emp_name" id="innova_emp_name" /></td>
					<td>
						<label class="desc">Employee Email  <font color=red>*</font></label>
						<input type="text" tabindex=3 maxlength="50" value="<?php if(isset($empEmail)) echo $empEmail; else echo '@theoreminc.net'; ?>" <?php  if(isset($empId)) { echo "readonly";}?> name="innova_email" id="innova_email" />
					</td>
				</tr>									
				<tr>
					<td>
						<label class="desc">Vertical <font color=red>*</font></label>
						<select name="innova_vertical" tabindex="4" id="innova_vertical" style="width:310px;" <?php  if(isset($empId)) { echo "disabled";}?>>				
							<?php echo $select_vertical; ?>
						</select>
					</td>
				  	<td>
				  		<label class="desc">Designation  <font color=red>*</font></label>
						<select name="innova_designation" tabindex="5" id="innova_designation" style="width:310px;" <?php  if(isset($empId)) { echo "disabled";}?>>
							<?php echo $select_designation; ?>
						</select>
					</td>
					<td></td>
			    </tr>		
			</table>
		</div>
		<div class="bottom InputGroup" id="ValidTextarea">
			<h2 class="heading">Idea to bring in new Method / System / Process <font color=red>*</font></h2>
			<table class="formTable mainTable">
				<tr>
					<td><textarea class="validTextarea" id="ValidTextarea_1" tabindex="6" name="innova_textarea1" <?php  if(isset($empId)) { echo "readonly";}?>><?php if(isset($idea)) echo $idea;?></textarea></td>
					<td>
						<h4 class="rightHeading"><label class="desc">Benefits (in terms of process improvement, cost reduction, reduction in TAT, etc.)</label></h4>
						<table id="InputGroup1" class="formTable subTable">
							<tr>
								<td><span>1.</span><input tabindex="7" maxlength="250" type="text" value="<?php if(isset($idea_benefit1)) echo  $idea_benefit1 ;?>" <?php  if(isset($empId)) { echo "readonly";}?> name="input1" id="input1" /></td>
							</tr>
							<tr>
								<td><span>2.</span><input tabindex="8" type="text" maxlength="250" value="<?php  if(isset($idea_benefit2)) echo $idea_benefit2 ;?>"  <?php  if(isset($empId)) { echo "readonly";}?> name="input2" id="input2" /></td>
							</tr>
							<tr>
								<td><span>3.</span><input tabindex="9" type="text" value="<?php if(isset($idea_benefit3)) echo $idea_benefit3 ;?>" <?php  if(isset($empId)) { echo "readonly";}?> maxlength="250" name="input3" id="input3" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<h2 class="heading">Idea to improve existing Method / System / Process</h2>
			<table class="formTable mainTable">
				<tr>
					<td><label class="desc">Existing Method / System / Process to be improved</label>
						<textarea class="validTextarea" id="ValidTextarea_2" tabindex="10" name="innova_textarea2" <?php  if(isset($empId)) { echo "readonly";}?> ><?php if(isset($proc_improved)) echo $proc_improved;?></textarea>
					</td>
					<td>
						<h4 class="rightHeading"><label class="desc">Current Shortfalls</label></h4>
						<table id="InputGroup2" class="formTable subTable">
							<tr>
								<td><span>1.</span><input tabindex="11" type="text" maxlength="250" value="<?php if(isset($shortfall1)) echo $shortfall1 ;?>" <?php  if(isset($empId)) { echo "readonly";}?> name="input4" id="input4" /></td>
							</tr>
							<tr>
								<td><span>2.</span><input type="text" tabindex="12" value="<?php if(isset($shortfall2)) echo $shortfall2 ;?>" maxlength="250" <?php  if(isset($empId)) { echo "readonly";}?> name="input5" id="input5" /></td>
							</tr>
							<tr>
								<td><span>3.</span><input type="text" tabindex="13" value="<?php if(isset($shortfall3)) echo $shortfall3 ;?>" maxlength="250" name="input6" <?php  if(isset($empId)) { echo "readonly";}?> id="input6" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table class="formTable mainTable">
				<tr>
					<td>
						<label class="desc">Idea to improve the existing Method / System / Process</label>
						<textarea class="validTextarea" id="ValidTextarea_3" tabindex="14" name="innova_textarea3" <?php if(isset($empId)) { echo "readonly";}?> ><?php if(isset($proc_improved)) echo $proc_improved;?></textarea>
					</td>
					<td>
						<h4 class="rightHeading"><label class="desc">Benefits (in terms of process improvement, cost reduction, reduction in TAT, etc.)</label></h4>
						<table id="InputGroup3" class="formTable subTable">
							<tr>
								<td><span>1.</span><input tabindex="15" type="text" <?php  if(isset($empId)) { echo "readonly";}?> maxlength="250" id="input7" name="input7" value="<?php if(isset($imp_idea_benefit1)) echo $imp_idea_benefit1 ;?>" /></td>
							</tr>
							<tr>
								<td><span>2.</span><input type="text" tabindex="16" id="input8" <?php  if(isset($empId)) { echo "readonly";}?> maxlength="250" name="input8" value="<?php if(isset($imp_idea_benefit2))echo $imp_idea_benefit2 ;?>" /></td>
							</tr>
							<tr>
								<td><span>3.</span><input type="text" tabindex="17" id="input9" <?php  if(isset($empId)) { echo "readonly";}?> maxlength="250" name="input9" value="<?php if(isset($imp_idea_benefit3))echo $imp_idea_benefit3 ;?>" /></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="ideaAccepted" <?php if (!$id){?>style="display:none"<?php } ?>class="ContentWrapper">
	<div class="top">
	<h2 class="heading">For TIQ Council use only:  </h2>
		
		<table class="formTable">

        	<tr>
            	<td><label class="desc">Idea accepted?</label>
                	<input type="radio" tabindex="18" name="accepted" value="Y" <?php if($ideaaccepted == 'Y')  echo ' checked="checked"';?> id="02" style="float:left;" /><span class="checkLabel">Yes</span>
                    <input type="radio"  tabindex="19" style="float:left;" name="accepted" value="N" <?php if($ideaaccepted == 'N')  echo ' checked="checked"';?> id="01" /><span class="checkLabel">No</span>
                </td>
            </tr>
        	<tr>
        	  <td>
        	  	<label class="desc">If yes, Target date &amp; Person responsible for implementation:</label>
				<input tabindex="20" type="text" value="<?php   echo $responsibility;?>" id="responisibility" name="responisibility" /></td>
			</tr>
        	<tr>
        	  <td>
        	  	<label class="desc">If No, Reason for non-acceptance:</label>
        	  	<input tabindex="21" type="text" value="<?php   echo $noreason;?>" id="Reason" name="reason" /></td>
			</tr>
        </table>
       </div>
</div>	
	
	<div class="BtnOtr">
		<input id="saveForm" class="btTxt"  type="submit" name="submit" value="Submit"/>
		<?php if(isset($id) && $id!=0) {?>
			<input id="cancelForm" class="btTxt"  type="button" name="reset" value="Cancel" onclick="window.location.href='listInnovation.php'" />
		<?php } else {?>
			<input id="resetForm" class="btTxt"  type="button" name="reset" value="Reset" onclick="window.location.href='Innovation.php'" />
		<?php }?>
		<input id="innovationid" type="hidden" value=<?php if(isset($id)) echo $id; else echo '0';?>  name="innovatoinid"/>
	</div>  
		
</form>

<?php
}
?>
</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>