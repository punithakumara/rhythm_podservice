<?php
require ('../config/mysql_crud.php');
require ("../config/excelwriter.class.php");

$db = new Database();

$excel=new ExcelWriter("Innovation Details.xls");
if($excel==false)	
echo $excel->error;

$myArr=array("innovation_id","emp_id","emp_name","emp_email","Designation","Vertical","idea","idea_benefit1","idea_benefit2","idea_benefit3","proc_improved","shortfall1","shortfall2","shortfall3","idea_improved","imp_idea_benefit1","imp_idea_benefit2","imp_idea_benefit3","submissiondate","ideaaccepted","responsibility","noreason");
$excel->writeLine($myArr);

$sql="SELECT * FROM innovation";
$result = $db->sql($sql);
$result_array = $db->getResult();
if(is_array($result_array) && count($result_array)!=0)
{
	if(!array_key_exists(0, $result_array))
	{
		$tempResult = array();
		foreach($result as $k=>$v)
			$tempResult[0][$k] = ($v);
		$result_array = $tempResult;	
	}
	foreach($result_array as $key => $res)
	{	
		$myArr=array($res['innovation_id'],$res['emp_id'],$res['emp_name'],$res['emp_email'],$res['Designation'],$res['Vertical'],$res['idea'],$res['idea_benefit1']
		,$res['idea_benefit2'],$res['idea_benefit3'],$res['proc_improved'],$res['shortfall1'],$res['shortfall2'],$res['shortfall3'],$res['idea_improved'],$res['imp_idea_benefit1'],$res['imp_idea_benefit2'],$res['imp_idea_benefit3'],$res['submissiondate'],$res['ideaaccepted'],$res['responsibility'],$res['noreason']);
		$excel->writeLine($myArr);
	}
}
?>