<?php
session_start();
if(!isset($_SESSION['authemail'])) 
	header("Location: ../login/login.php");
  $empName = isset($_SESSION['authname'])?$_SESSION['authname']:'';
  $admin = isset($_SESSION['admin'])?$_SESSION['admin']: 0;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	<title>Theorem</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" language="javascript" src="../js/jquery-1.3.2.js"></script>
		<script type="text/javascript" language="javascript" src="../js/jquery.dataTables.js"></script>
		<style>
			.Header {  text-align:center; background-color: #f2f2f2; color: #005589; font-size: 12px; font-weight: 600; height: 25px; line-height: 25px; margin: 0; padding: 0;   width: 100%;}
			.link_bar a { padding:6px; font-weight:bold; float:right; color:#d0d0d0; outline: none;}
			.link_bar a:link { color:#d0d0d0;  }
			.link_bar a:visited { color:#d0d0d0; }
			.link_bar a:hover { color:#d0d0d0; background-color:#005589; }
			.link_bar a:active { color:#d0d0d0; }
		</style>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bFilter": true,
					"bDestroy": true, 
					"sPaginationType": "full_numbers",
					"sDom": '<"top"lfp>rt<"bottom"ip><"clear">',
					"oLanguage": {
					"sLengthMenu": "Display _MENU_ records per page",
					"sZeroRecords": "No data found",
					"sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
					"sInfoEmpty": "Showing 0 to 0 of 0 records",
					"sInfoFiltered": "(filtered from _MAX_ total records)",
					"sProcessing" : "",
					"oPaginate": {
						 "sFirst": "First",
						 "sLast": "Last",
						 "sPrevious": "Previous",
						 "sNext": "Next"
					}
				  },
					"sAjaxSource": "innovation_processing.php"
					
				} );
			} );
		</script>
		<script language="javascript">
			function download()
			{
				$("#ajaxLoader").show();
				$.ajax({
					url:"downloadInnovation.php",
					success:function(result){
						$("#ajaxLoader").hide();
						window.location='Innovation Details.xls';
					}
				 });
					
			}
		</script>
		

	</head>
	
	<body id="dt_example">
	<div class="TopHeader">
		<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
		<div class="RightOtr">
				<span>Hi &nbsp;<strong><?php echo $empName; ?></strong>&nbsp;</span>
				<a href="logout.php?action=innov" title="Logout"><img width="16" height="22" border="0" src="../images/logout_btn.png"></a>
			</div>
	</div>
	<div id="container">
		<?php 
			//$user = (isset($_COOKIE['ci_session'])) ? unserialize($_COOKIE['ci_session']) : ""  ; 
			//if($user!="" && $user['logged_in'] == 1 && $user['user_data']['EmployID']!="")
			//{
		?>
		<h2 class="Header"> List Innovations </h2>
			<?php if( $admin == 1) { ?>
				<div class='link_bar'>
					<a href="listissues.php" title="Logout">Issues</a> 
					<a href="listempuan.php" title="Logout">UAN</a> 
				</div>
			<?php } ?>
		<div class="theorem">
			<div class="exportBtn">
			<input id="Export To Excel" class="btTxt" type="button" value="Export To Excel" onClick="download();" /><div id="ajaxLoader" style="display:none;"><img src="../images/loader.gif" alt="Loading..."></div>
			</div>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
			<thead>
				<tr style="background-color:#005589;">
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Employee ID</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Employee Name</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Employee Email</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Vertical</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Designation</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Idea</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Submission Date</span></th>
					<th width="14%"><span style="color:#FFFFFF; font-weight:normal">Accepted</span></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5" class="dataTables_empty">Loading data from server</td>
				</tr>
			</tbody>
		</table>
		</div>
		<?php 
			//}
			//else
			//{
		?>
			<!--<ul>
				<li class="desc">
					<label class="successful error" for="success">
						You are not authenticate user to access this page !!!.
					</label>
				
				</li>
			</ul>-->
		<?php 
			//}
		?>
	</div>

	<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>