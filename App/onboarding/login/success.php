<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Theorem</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
</div>
<div id="container">
<?php
require ('../config/mysql_crud.php');
//variables 
$db = new Database();
$params = $_POST;

$dob = $params['DateOfBirth'];
if (strpos($params['DateOfBirth'],'/') !== false) 
	$dob = str_replace("/","-",$params['DateOfBirth']);
$params['DateOfBirth'] = date('Y-m-d', strtotime($dob));

$doj = $params['DateOfJoin'];
if (strpos($params['DateOfJoin'],'/') !== false) 
	$doj = str_replace("/","-",$params['DateOfJoin']);
$params['DateOfJoin'] = date('Y-m-d', strtotime($doj));
$params['RelevantExperience']=$params['releventYear'].".".$params['releventMonth'];
unset($params['releventYear']);
unset($params['releventMonth']);
$params['AddressSame'] = ($_POST['AddressSame']=='on') ? 1 : 0;
$retval = $db->insert("new_joinees", $params);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
else
{?>
	
	<ul>
	<li class="desc">
		<label for="success" class="successful">
			Data added successfully !!!
		</label>
	
	</li>
	</ul>

<?php
}
?>
</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>