<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Theorem</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
</div>
<div id="container">
<?php
require ('../config/mysql_crud.php');
//variables 
$db = new Database();
$PostParams = $_POST;

$dob = $PostParams['DateOfBirth'];
if (strpos($PostParams['DateOfBirth'],'/') !== false) 
	$dob = str_replace("/","-",$PostParams['DateOfBirth']);
$params['DateOfBirth'] = date('Y-m-d', strtotime($dob));

$doj = $PostParams['DateOfJoin'];
if (strpos($PostParams['DateOfJoin'],'/') !== false) 
	$doj = str_replace("/","-",$PostParams['DateOfJoin']);
$params['DateOfJoin'] = date('Y-m-d', strtotime($doj));
$params['EmployID'] = $PostParams['EmployID'];
$params['EmployName'] = $PostParams['EmployName'];
$params['MaritalStatus'] = $PostParams['MaritalStatus'];
$params['BloodGroup'] = $PostParams['BloodGroup'];
$params['Location'] = $PostParams['Location'];
$params['Qualification'] = $PostParams['Qualification'];
$params['OtherQualification'] = $PostParams['OtherQualification'];
$params['EmpContactNumber'] = $PostParams['EmpContactNumber'];
$params['EmergencyContactNumber1'] = $PostParams['EmergencyContactNumber1'];
$params['EmergencyContactName1'] = $PostParams['EmergencyContactName1'];
$params['EmergencyContactRelationship1'] = $PostParams['EmergencyContactRelationship1'];
$params['EmergencyContactNumber2'] = $PostParams['EmergencyContactNumber2'];
$params['EmergencyContactName2'] = $PostParams['EmergencyContactName2'];
$params['EmergencyContactRelationship2'] = $PostParams['EmergencyContactRelationship2'];
$params['PANDetails'] = $PostParams['PANDetails'];
$params['PassportNumber'] = $PostParams['PassportNumber'];
$FamilyDetails = '';
if(isset($PostParams['FamilyName']))
{
	for($i=0; $i < count($PostParams['FamilyName']); $i++)
	{
		if($PostParams['FamilyName'][$i]!="")
		{
			$FamilyDob = $PostParams['MemberDateOfBirth'][$i];
			if (strpos($PostParams['MemberDateOfBirth'][$i],'/') !== false) 
				$FamilyDob = str_replace("/","-",$PostParams['MemberDateOfBirth'][$i]);
			$FamilyDetails[$i] = array("Family Member"=>$PostParams['RelationShip'][$i], "Family Member Name"=>$PostParams['FamilyName'][$i], "Gender"=>$PostParams['Gender'][$i], "DOB"=>date('Y-m-d', strtotime($FamilyDob)));
		}
	}
} 
$params['FamilyDetails'] = json_encode($FamilyDetails);
// checking the employee is exist in table or not.
$db->select("MediAssit", "EmployID", null, "EmployID='" . $params['EmployID'] . "'");     
$res = $db->getResult();
if(count($res)!=0)
	$retval = $db->update("MediAssit", $params, "EmployID='" . $params['EmployID'] . "'");
else
	$retval = $db->insert("MediAssit", $params);
if(! $retval )
{
  die('Could not enter data: ' . mysql_error());
}
else
{?>
	
	<ul>
	<li class="desc">
		<label for="success" class="successful">
			Data added successfully !!!
		</label>
	
	</li>
	</ul>

<?php
}
?>
</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>