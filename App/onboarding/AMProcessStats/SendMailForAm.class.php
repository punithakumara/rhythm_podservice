<?php
	require ('ProcessStats.class.php');
	require_once('../phpmailer/class.phpmailer.php');
	ini_set('max_execution_time', 300);
	class SendMailForAm extends ProcessStats
	{
		public 		$message	= "";

		public function __construct() {
			$this->db		= new Database();
		}

		public function sendMailCron() {
			$day 		    = date('l', strtotime(date('Y-m-d')));
			//if($day === 'Monday') {
				$amdetail	= parent::getAMDetails($getall = "getall");
				$vmdetail	= array();
				$data		= array();
				if(!empty($amdetail)) {
					foreach ($amdetail as $key => $amvalue) {
						$this->message = "";
						$vmdetail	= parent::getVMDetails($amvalue['CompanyEmployID']);
						$data		= array(
							'amname' 	=> $amvalue['AmName'],
							'amemail'	=> $amvalue['MGEmail'],
							'assmgr' 	=> $amvalue['CompanyEmployID'],
							'vmemail' 	=> $vmdetail['vmemail']
						);
						$this->getMailHeader($data);
					}					
				}
			/*} else {
				echo "Mail will be trigger only on Monday's";
			}*/
		}

		public function getMailHeader($val = "") {
			$headers			= "";
			$CC					= "";
			$stats				= "";
			$start_date			= date( 'Y-m-d', strtotime('-2 weeks sunday'));
			$end_date			= date( 'Y-m-d', strtotime('last saturday'));
			$amid				= isset($val['assmgr']) ? $val['assmgr'] : '' ;
			if(is_array($val)) {
				if(isset($amid) && !empty($amid)) {
					$stats	    = parent::getUtilizationStats($val['assmgr']);
					$th			= "<th>Process</th>";
					$vertical	= "";
					//$CC          	   .= "kallesh.kuberappa@theoreminc.net";	
					$CC        .= "rhythmsupport@theoreminc.net,". $val['vmemail'] ."\r\n";
					$this->message		= "<span style='font-family:arial; font-size:12px;'>
											Hi ".$val['amname'].",<br/><br/>
											As on Rhythm, below is the process wise Billable 
											and utilization stats for your process. 
											For the date range ".$start_date." to ".$end_date.".
										</span><br/><br/>";


				} else {					
					//$CC          	   .= "kallesh.kuberappa@theoreminc.net";
					$stats	    = parent::getUtilizationStatsAdmin();
					$CC          	   .= "rhythmsupport@theoreminc.net";	
					$th			 	    = "";
					$vertical			= "<th>Verticals</th>";
					$this->message		= "<span style='font-family:arial; font-size:12px;'>
											Hi All, <br/><br/>
											As on Rhythm, Below is the Client wise Billable and utilization stats for all the clients. 
											For the date range ".$start_date." to ".$end_date.".
										</span><br/><br/>";
										
				}

				$this->message		.= "<table cellspacing='0' cellpadding='5' border='1' style='border-collapse:collapse; font-family:arial; font-size:12px;'>";
				$this->message     	.= "<tr style='background-color:#7FBF3F;'>
											<th>Client</th>
											  ".$vertical."
											 ".$th."
											<th>Approved FTE</th>
											<th>Forecast NRR</th>
											<th>Forecast RDR</th>
											<th>Billable Count(FTE Contract)</th>
											<th>Difference</th>
											<th>Actual Hour</th>
											<th>Expected Hour</th>
											<th>Last Week Utilization</th>
										</tr>";
				

				$this->message      .= parent::getProcessStatsTableView($amid);
				$this->message		.= '</table><br/><br/>';
				$this->message		.= "<span style='font-family:arial; font-size:12px;'>
											Thanks<br/>
											Rhythm Support
										</span>";
				$subject 			= 'Process Stats : '.$val['amname'].'';
				
				$email				= $val['amemail'];
				//$email				=  'adarsh.nagaraja@theoreminc.net';
				//echo $this->message;
				//$this->sendMail($email,$subject,$this->message, $CC);
				unset($headers);
				unset($this->message);
				$this->saveSentMailData($stats);
			}
		}

		public function saveSentMailData($stats)
		{
			$CreatedOn	= date("Y-m-d");
			if(!empty($stats)) {
				foreach ($stats as $result) {
					$result['expected_hour'] = ($result['expected_hour']) ? $result['expected_hour'] : 0;
					$result['actual_hour']   = ($result['actual_hour']) ? $result['actual_hour'] : 0;
					$intpart	= floor( $result['Billablecount'] );  
					$result['AssociateManager']	= isset($result['Associate Manager'])?$result['Associate Manager']:'Admin';  
					$result['PROCESS'] = isset($result['PROCESS'])?$result['PROCESS']:'';
					$result['weekly_utilization'] = isset($result['weekly_utilization'])?$result['weekly_utilization']:0;
					$fraction	= $result['Billablecount'] - $intpart;
					if($fraction == 0) {
						$billableCount = floor($result['Billablecount']);
					} else {
						$billableCount = $result['Billablecount'];
					}
					$diffCount	= $result['ApprovedFTE'] - $billableCount;

					$query 		= "INSERT INTO am_processstats ( Vertical, Utilization, CreatedOn, Client, Process, 
											   ApprovedFTE, ForecastNRR, ForecastRDR, BillableCount, 
											   AssociateManager, ExpectedHours, ActualHours, Difference) 
								   VALUES ( '".mysql_real_escape_string($result['Vertical'])."',
								   			'".mysql_real_escape_string($result['weekly_utilization'])."',
											'".mysql_real_escape_string($CreatedOn)."',
											'".mysql_real_escape_string($result['CLIENT'])."',
											'".mysql_real_escape_string($result['PROCESS'])."',
											'".mysql_real_escape_string($result['ApprovedFTE'])."',
											'".mysql_real_escape_string($result['ForecastNRR'])."',
											'".mysql_real_escape_string($result['ForcastRDR'])."',
											'".mysql_real_escape_string($billableCount)."',
											'".mysql_real_escape_string($result['AssociateManager'])."',
											'".mysql_real_escape_string($result['expected_hour'])."',
											'".mysql_real_escape_string($result['actual_hour'])."',
											'".mysql_real_escape_string($diffCount)."')";
					$res		= $this->db->sql($query);	
				}
			}
		}

		public function sendMail($email,$subject,$message, $cc)
		{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();                       // telling the class to use SMTP
			$mail->SMTPDebug = 0;                  

			// 0 = no output, 1 = errors and messages, 2 = messages only.
			$mail->SMTPDebug = 1; 
			$mail->SMTPAuth = true;                // enable SMTP authentication 
			$mail->SMTPSecure = "tls";              // sets the prefix to the servier
			$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
			$mail->Port = 587;                    // set the SMTP port for the GMAIL 
			$mail->Username = "rhythmsupport";  // Gmail username
			$mail->Password = "rhy_th499";      // Gmail password
			$mail->ContentType = 'text/html'; 
			$mail->IsHTML(true);
			$mail->CharSet = 'windows-1250';
			$mail->From = 'rhythmsupport@theoreminc.net';
			//$mail->SetFrom('rhythmsupport@theoreminc.net','Rhythmsupport@theoreminc.net');
			$mail->FromName = 'Rhythmsupport@theoreminc.net';
			$mail->addReplyTo('Rhythmsupport@theoreminc.net');
			//$cc = 'ravikumar.venkategowda@theoreminc.net,adarsh.nagaraja@theoreminc.net,arunkumar.rudraiah@theoreminc.net';
			$addresses = explode(',', $cc);
			foreach ($addresses as $address) {
				$mail->AddCC($address);
			}
			$mail->Subject = $subject;
			$mail->Body = $message;
			$mail->MsgHTML($message);
			$mail->AddAddress ($email);     
			$error_message = "";
			// you may also use this format $mail->AddAddress ($recipient);
			if(!$mail->Send()) 
			{
				$error_message = "Error in Sending Email Mailer Error: " . $mail->ErrorInfo;
			} else {
				$error_message = "Mail Successfully sent!";
			}
			echo $error_message;			
		}

		public function sendMailForSuperAdmin() {
			 /* $val	= array(
						array('amemail'	=> 'arunkumar.rudraiah@theoreminc.net',
							  'amname'	=> 'Arun Kumar'),//Bhaskar Kalale
					);  */
			/* $val	= array(
						array('amemail'	=> 'managers@theoreminc.net',
							  'amname'	=> ''),
					);*/ 

			for ($i	= 0; $i < count($val) ; $i++) { 
				$res	= $this->getMailHeader($val[$i]);
			}
		}
	}

	$mailobj	= new SendMailForAm();
	/*if(isset($_POST['resp']) && !empty($_POST['resp'])) {
		$res	= array_shift($_POST['resp']);
		$mailobj->getMailHeader($res);
		//$mailobj->sendMailCron();
	} else {
		$mailobj->sendMailCron();
		$mailobj->sendMailForSuperAdmin();
	}*/

?>