<?php
	require ('../config/mysql_crud.php');
	if(isset($_POST['assmgr']) && is_numeric($_POST['assmgr'])) {
		$getvertical  	= getVerticalName($_POST['assmgr']);
		$procesStats	= amProcessStats($_POST['assmgr']);
		$out			= array('am' => $getvertical, 'tbody' => $procesStats);
		echo  json_encode($out);
	}
	if(isset($_POST['assmgr']) && $_POST['assmgr'] == "getallrec") {
		$procesStats	= "";$out = "";
		$procesStats	= amProcessStats($_POST['assmgr']);

		$out			= array('tbody' => $procesStats);
		$out			= array_map('utf8_encode', $out);
		
		echo  json_encode($out);
	}

	function getVerticalName($assmgrId) {
		
		$db 		= new Database();
		$sql = "SELECT employid, 
		       Concat(Ifnull(employ.`firstname`, ''), '',Ifnull(employ.`lastname`,'')) AS `vmname`, 
		       email 
		       AS VMEmail 
			FROM   employ 
			WHERE  employid = (SELECT primary_lead_id 
		    FROM   employ 
		    WHERE  companyemployid = ". $assmgrId .")" ;
		
		$result		= $db->sql($sql);
		$resp_array = $db->getResult($result);
		return $resp_array;

	}

	function amProcessStats($assmgr) {
		$db 		= new Database();
		$query = "	SELECT v.name                                AS Vertical, 
       				c.client_name                                    AS CLIENT, 
       				p.name                                           AS PROCESS, 
       				p.headcount                                      AS ApprovedFTE, 
       				p.forcastnrr                                     AS ForecastNRR, 
       				p.forcastrdr                                     AS ForcastRDR, 
       				(SELECT SUM(IF(billablepercentage >= 100, 1, 0.5)) AS Billablecount 
       				 FROM   employprocess 
       				 WHERE  primaryprocess = 1 
       			        AND billable = 1 
       			        AND processid = p.processid)             AS Billablecount, 
       				e.companyemployid                                AS EmployId, 
       				Concat(e.firstname, ' ', Ifnull(e.lastname, '')) AS 'Associate Manager', 
       				e.email 
				FROM   employprocess ep, 
      				process p, 
      				employ e, 
      				vertical v, 
      				client c 
				WHERE  ep.processid = p.processid 
       				AND p.processstatus = 0 
       				AND ep.employid = e.employid 
       				AND e.relieveflag = 0 
       				AND v.verticalid = p.verticalid 
       				AND p.clientid = c.clientid 
       				AND ep.grades = 4 
       				AND P.name IS NOT NULL ";
       			if(isset($assmgr) && is_numeric($assmgr)) {
       				$query .= " AND e.CompanyEmployID = $assmgr ";
	       		}		
			$query	.= " ORDER BY v.name,c.client_name,p.name";

			$resp 	 	= $db->sql($query);
			$resp_array = $db->getResult($resp);
			$tbody 		= "";

			if (count($resp_array) == count($resp_array, COUNT_RECURSIVE)) {
				$arraymodify	= array();
				$arraymodify[] 	= $resp_array;
				$resp_array		= $arraymodify;
			} else {
				$resp_array = $resp_array;
			}
			
			foreach ($resp_array as $key => $value) {
				$intpart 	= floor( $value['Billablecount'] );    
				$fraction 	= $value['Billablecount'] - $intpart;
				if($fraction == 0) {
					$billableCount = floor($value['Billablecount']);
				} else {
					$billableCount = $value['Billablecount'];
				}
				$diffCount   = $value['ApprovedFTE'] - $billableCount;
				$row = '<tr>';
				$row_bg = '<tr style="background:yellow">';
				$tbody .= (($diffCount!=0)?$row_bg:$row).'<td>'.$value['CLIENT'].'</td><td>'.$value['PROCESS'].'</td><td align="center">'.$value['ApprovedFTE'].'</td><td>'.$value['ForecastNRR'].'</td><td>'.$value['ForcastRDR'].'</td><td>'.$billableCount.'</td><td>'.$diffCount.'</td></tr>'."";
			}
			return $tbody;
			
	}

?>