<?php
require ('../config/mysql_crud.php');
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');

class ProcessStats {
	public 				$db;
	public 				$tbody;
	public 				$mess;
	public function __construct() {
		$this->db	= new Database();

	}

	/**
	Function to fetch all associative managers email id and name.
	*/
	public function getAMDetails($amid = "") {
		$finaldata	= array();		
		$query		=	"SELECT GROUP_CONCAT(DISTINCT(p.ProcessID))			AS ProcessID,
						v.name 						 						AS Vertical,
						p.name						 						AS PROCESS,
						e.companyemployid 				 					AS CompanyEmployID,
						e.employid					 						AS EmployIDs,
						CONCAT(e.firstname, ' ', IFNULL(e.lastname, ''))	AS AmName,
						e.email												AS MGEmail 
					 FROM employprocess ep,
						PROCESS p,
						employ e,
						vertical v,
						CLIENT c
					 WHERE ep.processid 		= p.processid
							AND p.processstatus = 0
							AND ep.employid 	= e.employid
							AND e.relieveflag 	= 0
							AND v.verticalid 	= p.verticalid
							AND p.clientid 		= c.clientid
							AND ep.grades 		= 4
							AND P.name IS NOT NULL";

				if(isset($amid) && is_numeric($amid)) {
					$query	.= " AND e.CompanyEmployID = $amid ";
				}				
				$query		.= " GROUP BY e.companyemployid ORDER  BY e.firstname ";

				$result		= $this->db->sql($query);
				$rec		= $this->countRecursive($this->db->getResult());

				foreach ($rec as $key => $value) {
					if(isset($value['ProcessID'])) {

						$billcount	= $this->getAMBillableCount($value['ProcessID']);
						if(isset($billcount['billable']) && ($billcount['billable'] > 0)) {
							$finaldata[$key]['EmployIDs'] 			= $value['EmployIDs'];
							$finaldata[$key]['CompanyEmployID'] 	= $value['CompanyEmployID'];
							$finaldata[$key]['MGEmail'] 			= $value['MGEmail'];
							$finaldata[$key]['AmName'] 				= $value['AmName'];
						}
					}
				}
			if($amid == 'getall') {
				return $finaldata;
			} else {
				return array_shift($finaldata);
			}
		
	}


	public function getAMBillableCount($pid	= "") {
		$start_date			= date( 'Y-m-d', strtotime('-2 weeks sunday'));
		$end_date			= date( 'Y-m-d', strtotime('last saturday'));
		$rec				= "";
		if(isset($pid) && !empty($pid)) {
			$query			=  "SELECT SUM(billable) AS billable 
							FROM   process_billablenonbillable_cron 
							WHERE  processid IN ($pid) 
							       AND date BETWEEN '".$start_date."' AND '".$end_date."' 
							       AND billable != 0";
			$result		= $this->db->sql($query);
			$rec		= $this->db->getResult();
			if(!empty($rec)) {
				return $rec;
			} 
		}
	}


	/**
	Function to fetch all associative managers stats.
	*/

	public function getAmProcessStats($amid = "") {
			$query	=  "SELECT DISTINCT(p.ProcessID) AS ProcessID, v.name AS Vertical, c.client_name AS CLIENT,
      						c.ClientID  AS ClientID,
      					p.name   AS PROCESS, p.headcount  AS ApprovedFTE, p.forcastnrr AS ForecastNRR,
      						p.forcastrdr AS ForcastRDR,
      						(SELECT SUM(
      									CASE 
      										WHEN 
           										Billabletype = 'FTE Contract' 
          									THEN 
           										IF(BillablePercentage = 100, 1,
           											IF(BillablePercentage  <= 25, 0.25,
           												IF(BillablePercentage = 50, 0.5, 0.75)
            										)
           										)
           									END)
           					FROM employprocess, employ WHERE billable	= 1 AND primaryprocess = 1
       					AND employprocess.processid = p.processid AND employprocess.employID = employ.employID
       						AND employ.relieveFlag = 0 AND employprocess.grades < 4) AS FTEContractCount,
        				(SELECT 
        					SUM(IF(billablepercentage = 100 ,1, 
        							IF((billablepercentage IS NULL AND billable = 1) OR 
        						(billableType = 'FTE' OR billableType = 'Approved Buffer'),
        					IF(billablePercentage = 25 ,0.25 , 
        						IF(billablePercentage = 50 ,0.5, 
        					IF(BillablePercentage = 75, 0.75, 1) ) ),0 ))) 
         				AS Billablecount FROM employprocess,employ WHERE primaryprocess = 1 
       						AND billable	= 1 AND processid	= p.processid 
       					AND employprocess.employID	= employ.employID AND employ.relieveFlag = 0
       						AND BillableType IN ('FTE','Approved Buffer')) AS Billablecount,
           				e.companyemployid AS EmployId, CONCAT(e.firstname, ' ', IFNULL(e.lastname, '')) 
           					AS 'Associate Manager', e.email FROM employprocess ep, PROCESS p, employ e,
            			vertical v, CLIENT c WHERE ep.processid   = p.processid AND p.processstatus = 0
       						AND ep.employid	= e.employid AND e.relieveflag  = 0
       					AND v.verticalid  	= p.verticalid AND p.clientid   = c.clientid
       						AND ep.grades	= 4 AND P.name IS NOT NULL";
					
			if(isset($amid) && is_numeric($amid)) {
				$query	.= " AND e.CompanyEmployID = $amid ";
			}
			$query		.= " ORDER BY c.client_name,p.name ";
			
			$result		 = $this->db->sql($query);
		
			return $this->countRecursive($this->db->getResult()); 
	}

	/**
	Function to fetch vertical manager name and vertical email id for the respective AM's
	*/

	public function getVMDetails($amid = "") {
		$query	= "SELECT Concat(Ifnull(employ.firstname, ''), '', Ifnull(employ.lastname, '')) 
				    AS 
				    vmname, 
				    employ.email 
				    AS vmemail 
					FROM   employ 
					       INNER JOIN employ st2 
					               ON ( employ.employid = st2.primary_lead_id ) 
					WHERE  st2.companyemployid IN ( $amid ) 
					LIMIT  0, 1 ";
				
			
		$result		= $this->db->sql($query);
		return $this->db->getResult();
	}

	/**
	Function to convert associative array to multidimensional array
	*/

	public function countRecursive($checkarray = '') {
		if(!empty($checkarray) && is_array($checkarray)) {
			if (count($checkarray) == count($checkarray, COUNT_RECURSIVE)) {
				$arraymodify	= array();
				$arraymodify[] 	= $checkarray;
				$checkarray		= $arraymodify;
			} else {
				$checkarray 	= $checkarray;
			}
			return $checkarray;
		}
	}
	
	public function getProcessStatsTableView($amid = "", $td = "") {
		$this->mess			= "";
		$Total_AFTE 		= 0;
		$Total_BC 			= 0;
		$Total_ActualHour	= 0;
		$Total_ExpHour 		= 0;
		$TotalNRR 			= 0;
		$TotalFTECont		= 0;
		$TotalRDR 			= 0;
		$totalRow			= "";
		$totalutilization	= 0;

		if(isset($amid) && (!empty($amid) && ($amid != 'getallrec'))) {
			$stats			= $this->countRecursive($this->getUtilizationStats($amid));
		} elseif(($amid == 'getallrec') || ($amid == "")) {
			$stats			= $this->countRecursive($this->getUtilizationStatsAdmin());
		}

		// constant values to calculate Total for last row
		
		if(!empty($stats) && is_array($stats)) {
			foreach ($stats as $result) {
				if($result['ApprovedFTE'] != 0 || $result['ForecastNRR'] != 0
					|| $result['ForcastRDR'] != 0 || $result['Billablecount'] !=0 ) {
					$intpart	= floor( $result['Billablecount'] );    
					$fraction	= $result['Billablecount'] - $intpart;

					if($fraction == 0) {
						$billableCount 		= floor($result['Billablecount']);
					} else {			
						$billableCount 		= $result['Billablecount'];
					}

					if(isset($result['FTEContractCount']) && !empty($result['FTEContractCount'])) {
						$fteContCount		= "(".$result['FTEContractCount'].")";
					} else {
						$fteContCount		= "";
					}	

					$result['actual_hour'] 			= isset($result['actual_hour'])?$result['actual_hour']:'-';
					$result['expected_hour'] 		= isset($result['expectedHours'])?$result['expectedHours']:'-';
					$result['AssociateManager']	 	= isset($result['Associate Manager'])?$result['Associate Manager']:'';
					$result['weekly_utilization']	= isset($result['weekly_utilization'])?$result['weekly_utilization']:'0';
					$td	= "";
					if($amid != 'getallrec') {
						if(isset($result['PROCESS'])) {
							$td	= "<td>".$result['PROCESS']."</td>";
						}
					}
					if(isset($result['VerticalName'])) {
						$vn	= '<td align="left">'.$result['VerticalName'].'</td>';
					} else {
						$vn	= "";
					}
					
					
					$diffCount					= $result['ApprovedFTE'] - $billableCount;				
					$row 						= '<tr>';
					$row_bg 					= '<tr style="background:yellow">';
					$this->mess			   	   .= (($diffCount!=0) ? $row_bg : $row).
													'<td>' .$result['CLIENT'].'</td>
													 '.$vn.'
											 		 '.$td.'
											 		<td align="center">'.$result['ApprovedFTE'].'</td>
											 		<td align="center">'.$result['ForecastNRR'].'</td>
											 		<td align="center">'.$result['ForcastRDR'].'</td>
											 		<td align="center">'.$billableCount.'&nbsp;'.$fteContCount.'</td>
											 		<td align="center">'.$diffCount.'</td>
											 		<td align="center">'.$result['actual_hour'].'</td>
											 		<td align="center">'.$result['expected_hour'].'</td>
											 		<td align="center">'.$result['weekly_utilization'].'%'.'</td>
												</tr>';
					

					$Total_AFTE 		+= $result['ApprovedFTE'];
					$TotalNRR 			+= $result['ForecastNRR'];
					$TotalRDR 			+= $result['ForcastRDR'];
					$Total_BC 			+= $result['Billablecount'];
					$TotalFTECont       += $result['FTEContractCount'];
					
					$Total_ActualHour 	+= $result['actual_hour'];
					if($result['actual_hour'] != '-') {
						$Total_ExpHour 		+= $result['expected_hour'];
					}
					
					$totalRow ="";
				}
			}

			if($Total_ExpHour > 0) {
				$totalutilization	= round(($Total_ActualHour/$Total_ExpHour)*100,2);
			}
			if(!empty($TotalFTECont)) {
					$TotalFTECont			= "&nbsp(".$TotalFTECont.")";
				} else {
					$TotalFTECont			= "";
				}	
			$totalRow .= '<tr><td colspan="2" align="center"><b>Total</b></td>';
			$totalRow .= '<td align="center"> <b>'.$Total_AFTE.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$TotalNRR.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$TotalRDR.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$Total_BC.$TotalFTECont.'</b> </td>';
			$totalRow .= '<td align="center"><b>'.($Total_AFTE - $Total_BC).'</b></td>';
			$totalRow .= '<td align="center"><b>'.$Total_ActualHour.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$Total_ExpHour.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$totalutilization.'%</b></td>';
			$totalRow .= '</tr>';
			return $this->mess.$totalRow;
		}
	}



	public function getTaskData($data = array())
	{
		$actualhour		= array();
		$start_date		= date( 'Y-m-d', strtotime('-2 weeks sunday'));
		$end_date		= date( 'Y-m-d', strtotime('last saturday'));
		

		$query			= " SELECT  GROUP_CONCAT(DISTINCT taskdate) AS taskDate, 
								SUM(TIME_TO_SEC(num_hours)) AS actual_hour,
							SUM(expectedHours) AS expectedHours
								FROM   	employwise_utilization_process eup 
							WHERE  	taskdate  BETWEEN '".$start_date."' AND  '".$end_date."'
								AND clientid   = ".$data['ClientID']. "";

		if(isset($data['VerticalID']) && !empty($data['VerticalID'])) {
			$query		.= " AND VerticalID IN  (".$data['VerticalID'].")";
		}
		if(isset($data['ProcessID']) && !empty($data['ProcessID'])) {
			$query		.= " AND processid  = ".$data['ProcessID']. "";
		}
		
		$result			= $this->db->sql($query);
		$resp			= $this->db->getResult();
		return $resp;
		
	}
	

	public function seconds2minutes($ss) {

		if(isset($ss) && !empty($ss)) {
			$init        = $ss;
			$hours       = floor($init / 3600);
			$minutes     = floor(($init / 60) % 60);
			$FinalHours  = $hours . '.' . $minutes;
			return $FinalHours;
		}
	}

	public function getUtilizationStats($amid = "")
	{
		$stats		= $this->getAmProcessStats($amid);
		$resp		= array();
		if(!empty($stats) && is_array($stats)) {
			foreach ($stats as &$statvalue) {
				$para	= array(
						'ClientID'		=> $statvalue['ClientID'], 
						'ProcessID'		=> $statvalue['ProcessID']
					);

				$resp 								= $this->getTaskData($para);
				$statvalue['actual_hour'] 			= $this->seconds2minutes($resp['actual_hour']);
				
				if(isset($resp['actual_hour']) && !empty($resp['actual_hour'])) {
					$statvalue['expectedHours']			= $resp['expectedHours'];
					if(isset($resp['expectedHours']) && $resp['expectedHours'] != 0) {
						$statvalue['weekly_utilization']	= round(($statvalue['actual_hour']/ 
																			  $resp['expectedHours']) * 100, 2);
					} else {
						$statvalue['weekly_utilization']	= 0;
					}
				}
			}
			return $stats;
		}
	}
/**
	STATS FOR SUPER ADMIN
*/	
	public function getAmProcessStatsAdmin()
	{
		$query = "SELECT DISTINCT ( c.clientid )    AS ClientID, 
  p.processid                             AS ProcessID, 
  v.name                        AS Vertical,
  GROUP_CONCAT(DISTINCT v.VerticalID )        AS VerticalID, 
  GROUP_CONCAT(DISTINCT v.name  )          AS VerticalName,   
  c.client_name                           AS CLIENT, 
  SUM(p.headcount)                        AS ApprovedFTE, 
  SUM(p.forcastnrr)                       AS ForecastNRR, 
  SUM(p.forcastrdr)                       AS ForcastRDR, 
  
  (CASE WHEN (SELECT TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM  SUM( IF(billablepercentage = 100 ,1,  
           IF((billablepercentage IS NULL AND billable = 1) OR (billableType = 'FTE' OR billableType = 'Approved Buffer'),
    IF(billablePercentage = 25 ,0.25 ,IF(billablePercentage = 50 ,0.5, IF(BillablePercentage = 75, 0.75, 1) )
       ),0
    )         
                   ))  ))  AS Billedcount

  FROM employprocess ep,employ,
  PROCESS sp
  WHERE ep.primaryprocess = 1
  AND ep.billable = 1
  AND ep.processid = sp.processid
  AND ep.employID = employ.employID
  AND employ.relieveFlag = 0
  AND sp.clientid = c.clientid

  AND ep.BillableType IN ('FTE','Approved Buffer'))  IS NULL THEN 0 ELSE 
  (SELECT TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM  SUM( IF(billablepercentage = 100 ,1,  
           IF((billablepercentage IS NULL AND billable = 1) OR (billableType = 'FTE' OR billableType = 'Approved Buffer'),
    IF(billablePercentage = 25 ,0.25 ,IF(billablePercentage = 50 ,0.5, IF(BillablePercentage = 75, 0.75, 1) )
       ),0
    )         
                   ))  ))  AS Billedcount

  FROM employprocess ep,employ,
  PROCESS sp
  WHERE ep.primaryprocess = 1
  AND ep.billable = 1
  AND ep.employID = employ.employID
  AND employ.relieveFlag = 0
  AND ep.processid = sp.processid
  AND sp.clientid = c.clientid
  AND ep.BillableType IN ('FTE','Approved Buffer'))

  END) AS Billablecount,
  (SELECT SUM(CASE 
          WHEN 
           sep.Billabletype = 'FTE Contract' 
          THEN 
           IF(sep.BillablePercentage = 100, 1,
            IF(sep.BillablePercentage  <= 25, 0.25,
             IF(sep.BillablePercentage = 50, 0.5, 0.75)
            )
           )  
         END)
  FROM   employprocess sep, 
  PROCESS sp1 
  WHERE  sep.billable = 1 
  AND sep.primaryprocess = 1 
  AND sep.processid = sp1.processid 
  AND sp1.clientid = c.clientid) AS FTEContractCount 
  FROM   PROCESS p, 
  vertical v, 
  CLIENT c 
  WHERE  p.processstatus = 0 
  AND v.verticalid = p.verticalid 
  AND p.clientid = c.clientid 

  GROUP  BY c.clientid 
  ORDER  BY c.client_name";
		$result		 = $this->db->sql($query);
		return $this->countRecursive($this->db->getResult());
		
	}

	public function getUtilizationStatsAdmin($amid = "")
	{

		$stats		= $this->getAmProcessStatsAdmin();

		$resp		= array();
		if(!empty($stats) && is_array($stats)) {
			foreach ($stats as &$statvalue) {
				$para	= array(
						'ClientID'			=> $statvalue['ClientID'], 
						'VerticalID'		=> $statvalue['VerticalID']
					);
				$resp 									= $this->getTaskData($para);
			
				$statvalue['actual_hour'] 				= $this->seconds2minutes($resp['actual_hour']);
				if(isset($resp['actual_hour']) && !empty($resp['actual_hour'])) {
					$statvalue['expectedHours']			= $resp['expectedHours'];
					if(isset($resp['expectedHours']) && $resp['expectedHours'] != 0) {
						$statvalue['weekly_utilization']	= round(($statvalue['actual_hour']/ 
																			  $resp['expectedHours']) * 100, 2);
					} else {
						$statvalue['weekly_utilization']	= 0;
					}
				}
			}
			return $stats;
		}
	}
}

$obj	= new ProcessStats();

//generic for all the AM's
if(isset($_POST['assmgr']) && ($_POST['assmgr'] == 'getallrec')) {
	$statsdata	= $obj->getProcessStatsTableView($_POST['assmgr']);
	$encode		= array(
		'statsdata' => $statsdata
		);
	echo json_encode(array_map('utf8_encode', $encode));
}

else if(isset($_POST['assmgr']) && ($_POST['assmgr'] != 'getallrec')) {
// for single AM's stats
	$amdata		= $obj->getAMDetails($_POST['assmgr']);
	$vmdata		= $obj->getVMDetails($_POST['assmgr']);

	$statsdata	= $obj->getProcessStatsTableView($_POST['assmgr']);

	$encode		= array(
		'amdata' 	=> $amdata, 
		'vmdata' 	=> $vmdata, 
		'statsdata' => $statsdata
		);

	echo json_encode($encode);
}

?>