<?php
	require ('ProcessStats.class.php');
	$obj	= new ProcessStats;
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>
	<body>
		<table style="position:absolute;top:12px;left:72px;font-size:13px;">
			<thead>
				<tr>
					<th>Select AM :</th>
					<th>
						<select style = "width: 220px" id = "assmgr">
							<option value="getallrec">--Select AM--</option>
								<?php foreach ($obj->getAMDetails('getall') as $amkey => $amvalue) {?>
									<option value = "<?php echo $amvalue['CompanyEmployID']?>"><?php echo $amvalue['AmName']; ?></option>
								<?php } ?>
						</select>
					</th>
					<th style="display:none;" class="test">
						AM Email :<span id ="amemail" style="font-weight:normal;"></span>
					</th>
					<th style="display:none;" class="test">
					 	Vertical Manager Name	: <span  id ="vertManagerName" style="font-weight:normal;"></span>
					</th>
					<th style="display:none;" class="test">
						Vertical Manager Email	: <span  id ="vmemail" style="font-weight:normal;"></span>
					</th>
				</tr>
				<tr style="position:absolute;top:57px;left:1px;">
					<th>
						<input type="button" id="sendmail" hidden="true" value="Send Mail"/>
					</th>
				</tr>
			</thead>
		</table>
		<div style="visibility: hidden;" id="amtable">
			<table  style="position:absolute;top:37px;" border="0"  class="Grid" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th>Client</th>
						<th>Process</th>
						<th>Approved FTE</th>
						<th>Forecast NRR</th>
						<th>Forecast RDR</th>
						<th>Billable Count(FTE Contract)</th>
						<th>Difference</th>
						<th>Actual Hour</th>
						<th>Expected Hour</th>
						<th>Last Week Utilization</th>
					</tr>
				</thead>
				<tbody  id="tbodyid">
					<?php
						echo $obj->getProcessStatsTableView();
					?>
				</tbody>
			</table>
		</div>
		<div style="visibility: visible;" id="admintable">
			<table  style="position:absolute;top:37px;" border="0" class="Grid" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th>Client</th>
						<th>Verticals</th>
						<th>Approved FTE</th>
						<th>Forecast NRR</th>
						<th>Forecast RDR</th>
						<th>Billable Count(FTE Contract)</th>
						<th>Difference</th>
						<th>Actual Hour</th>
						<th>Expected Hour</th>
						<th>Last Week Utilization</th>
					</tr>
				</thead>
				<tbody  id="tbodyadmin">
					<?php
						echo $obj->getProcessStatsTableView();
					?>
				</tbody>
			</table>
		</div>
	</body>
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript">
		//$("#assmgr").html();
		$("#assmgr").find('option:selected').removeAttr("selected");
		$("#assmgr").on('change', function() {
			var assmgr = $(this).val();
			if(assmgr != "getallrec") {
				$("#amtable").css('visibility', 'visible');
				$("#admintable").css('visibility', 'hidden');
				$.ajax( {
					url			: 'ProcessStats.class.php',
					type		: 'POST',
					datatype	: 'json',
					data		: {'assmgr'	: assmgr} ,
					success		: function (data) {
						$(".test").show();
						$("#sendmail").show();
						var res		= $.parseJSON(data);
						
						var tbodyid = $("#tbodyid").html("");
						$("#tbodyid").append(res.statsdata);
						
						$("#amemail").html(res.amdata.MGEmail);
						$("#vertManagerName").html(res.vmdata.vmname);
						$("#vmemail").html(res.vmdata.vmemail);
						$("#tbodyid").html("");
						$("#tbodyid").append(res.statsdata);
					},
					error		: function () {
						console.log("error");
					}
				} );
				
			} else if(assmgr == "getallrec") {
				$("#amtable").css('visibility', 'hidden');
				$("#admintable").css('visibility', 'visible');
				$(".test").hide();
				$("#sendmail").hide();
				$("#details").hide();
				$("#amemaildiv").hide();
				$.ajax( {
					url			: 'ProcessStats.class.php',
					type		: 'POST',
					datatype	: 'json',
					data		: { 'assmgr'	: assmgr } ,
					success		: function (data) {
						var res		= $.parseJSON(data);
						var tbodyid = $("#tbodyadmin").html("");
						$("#tbodyadmin").append(res.statsdata);
					},
					error		: function () {
						console.log("error");
					}
				} );
			}
		} ); 
		
		$("#sendmail").click(function() { 
			var amname	= $("#assmgr").find('option:selected').text();
			var assmgr 	= $('#assmgr').val();
			var vmemail	= $("#vmemail").html();
			var amemail	= $("#amemail").html();
			
			var data	= [];

			data.push( {
				'assmgr'	: assmgr, 
				'vmemail'	: vmemail, 
				'amname'	: amname, 
				'amemail' 	: amemail 
			} );

			$.ajax( {
				url			: 'SendMailForAm.class.php',
				type		: 'POST',
				datatype	: 'json',
				data		: {'resp'	: data},
				success		: function (res) {
					alert(res);
				},
				error		: function () {
					console.log("error");
				}
			} )
		} )
	</script>
</html>