Ext.define('AM.util.JSONP',{
	
	statics: {
		
		/**
		 * Read-only queue
		 * @type Array
		 */
		queue: [],

		/**
		 * Read-only current executing request
		 * @type Object
		 */
		current: null,

		/**
		 * Make a cross-domain request using JSONP.
		 * @param {Object} config
		 * Valid configurations are:
		 * <ul>
		 *  <li>url - {String} - Url to request data from. (required) </li>
		 *  <li>params - {Object} - A set of key/value pairs to be url encoded and passed as GET parameters in the request.</li>
		 *  <li>callbackKey - {String} - Key specified by the server-side provider.</li>
		 *  <li>callback - {Function} - Will be passed a single argument of the result of the request.</li>
		 *  <li>scope - {Scope} - Scope to execute your callback in.</li>
		 * </ul>
		 */
		request : function(o) {
			o = o || {};
			if (!o.url) {
				return;
			}

			var me = this;
			o.params = o.params || {};
			if (o.callbackKey) {
				o.params[o.callbackKey] = 'AM.util.JSONP.callback';
			}
			var params = Ext.urlEncode(o.params);

			var script = document.createElement('script');
			script.type = 'text/javascript';

			this.queue.push({
				url: o.url,
				script: script,
				callback: o.callback || function(){},
				scope: o.scope || window,
				params: params || null
			});

			if (!this.current) {
				this.next();
			}
		},

		// private
		next : function() {
			this.current = null;
			if (this.queue.length) {
				this.current = this.queue.shift();
				this.current.script.src = this.current.url + (this.current.params ? ('?' + this.current.params) : '');
				document.getElementsByTagName('head')[0].appendChild(this.current.script);
			}
		},

		// @private
		callback: function(json) {
			this.current.callback.call(this.current.scope, json);
			document.getElementsByTagName('head')[0].removeChild(this.current.script);
			this.next();
		}
	}	
});