Ext.define('AM.util.Constants', {
     singleton: true,
     meetingArray: [],
     meetingInternAtd: [],
     meetingExternAtd: [],
     meetingDateParam: [],
     meetingTypeParam: [],
     meetingExternClient: [],
	 meetingId:"",
	 topicID:"",
	 meetingMinutesPage:"",
	 addActionButton:false,
	 ToolsTech:"",
     bufferReport : ""
});