Ext.define('AM.model.EmployeeSkillset',{
	extend : 'Ext.data.Model',
	fields : [
		{name : 'id', dataType : 'int'},'employee_id','skill_name','skill_id','last_used_month','last_used_year','version','last_used','experience','self_rating',
	]
});