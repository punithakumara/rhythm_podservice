Ext.define('AM.model.Employee',{
	extend : 'Ext.data.Model',
	fields : [
		{name : 'employee_id', dataType : 'int'},'company_employ_id','status','primary_lead_id','first_name','full_name','last_name',
		'Designation_name','designation_id','email','mobile','location_id','doj','gender','dob','shift_id','shift_code','qualification_id','service_id',
		'Grade','VerticalID','vertical_name','transition_status','user_dashboard','dashboard_tools','grade_level',
		'assigned_vertical','vertical_name','resigned_notes','resign_flag','relieve_flag','relieve_date','relieve_type',
		'relieve_notes','training_needed','created_by','qualification_name','service_name','location_name','lead_name','stake_holder','comments',
		'personal_email','alternate_number','permanent_address','permanent_city','permanent_state','permanent_zipcode','same_as',
		'present_address','present_city','present_state','present_zipcode','is_onshore',
	]
});