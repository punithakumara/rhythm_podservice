Ext.define('AM.model.LanguagesList',{
	extend : 'Ext.data.Model',
	
	fields : ['language_id','language']
});