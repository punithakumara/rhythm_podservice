Ext.define('AM.model.WorTypes',{
	extend : 'Ext.data.Model',
	fields: ['wor_type_id','wor_type', 'wor_description','total_projects']
});