Ext.define('AM.model.Transition',{
	extend : 'Ext.data.Model',
	fields : ['company_employ_id', 'Primary_Lead_ID', 'employee_id','Email','first_name','last_name','reportingFirstName','EmpProcessLead','reportingLastName','grades','Name','training_needed','training_status','assigned_vertical','Approved','PVName','VName', {	name: 'FullName', 
		convert: function(v, rec) { 
					var fullName = rec.get('first_name');
					if(!rec.get('last_name'))
					{
						fullName = fullName;
					}
					else if(rec.get('last_name')!='NULL' && rec.get('last_name')!="" && rec.get('last_name')!='null')
						fullName += " "+rec.get('last_name');
					return fullName;
				 }
  	  },{name: 'ReportingTo', 
		convert: function(v, rec) { 
					var fullName = rec.get('reportingFirstName');
					if(!rec.get('reportingLastName'))
					{
						fullName = fullName;
					}
					else if(rec.get('reportingLastName')!='NULL' && rec.get('reportingLastName')!="" && rec.get('reportingLastName')!='null')
						fullName += " "+rec.get('reportingLastName');
					return fullName;
				 }
  	  }, {	name: 'VNameOld', 
		convert: function(v, rec) { 
					var Vert = rec.get('PVName');
					
					return Vert;
				 }
  	  }, {	name: 'VNameNew', 
		convert: function(v, rec) { 
				var Vert = rec.get('VName');
				
				return Vert;
			}
  	  }]
});