Ext.define('AM.model.Technologies',{
	extend : 'Ext.data.Model',
	
	fields : ['skill_id','skill_name','skill_description']
});