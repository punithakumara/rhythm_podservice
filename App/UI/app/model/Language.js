Ext.define('AM.model.Language',{
	extend : 'Ext.data.Model',
	
	fields : [{name : 'language_id', dataType : 'int'},'language']
});