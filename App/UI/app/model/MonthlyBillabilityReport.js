Ext.define('AM.model.MonthlyBillabilityReport',{
	extend : 'Ext.data.Model',
	
	fields : ['client_name','client_id','responsibility_hc','responsibility_ab','fte_total_hc','fte_total_buffer', 'fte_est','fte_emea','fte_ist','fte_lemea','fte_cst','fte_pst','fte_apac','hourly_min','hourly_max','hourly_actual','volume_max','volume_min','volume_actual','remarks','india','usa','approved_Weekend_hrs'],
});