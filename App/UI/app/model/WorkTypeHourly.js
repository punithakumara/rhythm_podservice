Ext.define('AM.model.WorkTypeHourly',{
	extend : 'Ext.data.Model',
    fields : ['hourly_id','fk_cor_id','fk_project_type_id','hours_coverage', 'shift', 'min_hours','anticipated_date', 'max_hours', 'experience', 'skills', 'production_date', 'support_coverage', 'responsibilities','change_type','process_id','comments','project_type','project_name']
});