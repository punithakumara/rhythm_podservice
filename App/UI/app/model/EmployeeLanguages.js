Ext.define('AM.model.EmployeeLanguages',{
	extend : 'Ext.data.Model',
	fields : [
		{name : 'id', dataType : 'int'},'employee_id','language','language_id','proficiency','read','write','speak','created_on','updated_on',
	]
});