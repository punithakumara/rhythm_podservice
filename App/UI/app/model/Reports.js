Ext.define('AM.model.Reports',{
	extend : 'Ext.data.Model',
	
	fields : ['ProcessID','ClientID', 'Vertical_Name', 'Client_Name','Process_Name','Week','Year',{
	name: 'Accuracy', 
	convert: function(v, rec){
			if(v == null)
				return '0';
			else
				return v;
		}
	},{
	name: 'Utilization',
		convert: function(v, rec){
			if(v == null)
				return '0';
			else
				return v;
		}
	},'Error','Appreciation','Escalation',{
		name: 'Billable', 
		convert: function(v, rec){
			if(v == null)
				return '0';
			else
				return v;
		}
	}
	,'Month', {name: 'NonBillable', 
		convert: function(v, rec){
			if(v == null)
				return '0';
			else
				return v;
		}
	}, 'numMonth']
});