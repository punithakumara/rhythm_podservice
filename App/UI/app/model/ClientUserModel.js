Ext.define('AM.model.ClientUserModel',{
	extend : 'Ext.data.Model',
	
	fields : [ 'ClientID','Username','Password', 'Email','FullName','UserID']
});