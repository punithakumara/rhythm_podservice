Ext.define('AM.model.RoleTypes',{
	extend : 'Ext.data.Model',
	
	fields: ['role_type_id', 'wor_type', 'project_type','role_type','description']
});