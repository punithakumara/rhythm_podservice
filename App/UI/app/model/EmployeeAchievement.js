Ext.define('AM.model.EmployeeAchievement',{
	extend : 'Ext.data.Model',
	fields : [
		{name : 'id', dataType : 'int'},'award_name','year','expiry_year','expiry_month','link','created_on','updated_on',
	]
});