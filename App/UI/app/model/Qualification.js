Ext.define('AM.model.Qualification',{
	extend : 'Ext.data.Model',
	
	fields: ['qualification_id', 'name']
});