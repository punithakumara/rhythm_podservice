Ext.define('AM.model.dynamicModel', {
    extend: 'Ext.data.Model',
    proxy: {
      type: 'rest',
      url: AM.app.globals.appPath+'index.php/Charts/dashboardTablesGridOneView/?v='+AM.app.globals.version,
    }
});