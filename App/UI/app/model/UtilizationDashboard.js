Ext.define('AM.model.UtilizationDashboard',{
	extend : 'Ext.data.Model',
	
	fields : ['utilization_id','employee_id', 'task_date','fk_client_id','fk_wor_id','fk_type_id','fk_project_id','si_no', 'project_type','process_name','fk_role_id','fk_task_id','fk_sub_task_id','request_name','client_hrs','total_hrs','remarks','additional_attributes','status','created_on','created_by','updated_on','updated_by','approved_on','approved_by','attributes_array','Sub_task_Values','task_code_value','hours', 'minutes','project_code','task_code','sub_task','remarks','approved','actual_hrs','additionalFields','AdditionalAttributes','TotalHours','OverallTime']
});