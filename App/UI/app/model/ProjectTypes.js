Ext.define('AM.model.ProjectTypes',{
	extend : 'Ext.data.Model',
	
	fields: ['project_type_id','wor_type','project_type', 'project_description','total_roles','project_types','client_id','project_name']
});