Ext.define('AM.model.Document',{
	extend : 'Ext.data.Model',
	
	fields : [{name : 'DocumentID', dataType : 'int'},'UploadName','Name','DocumentDate','EmployID','FirstName','Access','ProcessID','VerticalID','ClientID','Folder','VerticalName']
});