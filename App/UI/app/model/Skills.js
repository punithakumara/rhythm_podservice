Ext.define('AM.model.Skills',{
	extend : 'Ext.data.Model',
	
	fields : ['skill_id','skill_name','skill_description']
});