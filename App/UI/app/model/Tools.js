Ext.define('AM.model.Tools',{
	extend : 'Ext.data.Model',
	
	fields : ['skill_id','skill_name','skill_description']
});