Ext.define('AM.model.EmployeeSkills',{
	extend : 'Ext.data.Model',
	fields : ['emp_name','emp_designation', 'emp_vertical','emp_skills','emp_tools','emp_technologies','emp_shift']
});