Ext.define('AM.model.EmployeeProfile',{
	extend : 'Ext.data.Model',
	fields : [
		{name : 'id', dataType : 'int'},'employee_id','org_name','designation','work_period','job_profile','start_month','start_year','end_month','end_year'	
	]
});