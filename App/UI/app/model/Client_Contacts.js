Ext.define('AM.model.Client_Contacts',{
	extend : 'Ext.data.Model',
	
	fields : ['contact_id', 'client_id','contact_name','contact_title', 'contact_email','contact_phone','contact_mobile','status', 'Name','ServiceID','ProcessID','contact_service', 'Contact_Process','ServiceName','ProcessName','role','ContactIDs']
});