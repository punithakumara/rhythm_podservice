Ext.define('AM.model.Designation',{
	extend : 'Ext.data.Model',
	
	fields : [{name : 'designation_id', dataType : 'int'},'name', 'grades', 'hcm_grade', 'PromoteToFlag', 'PromoteTo', 'EmployID','DeligateTo','SkillDesignation']
});