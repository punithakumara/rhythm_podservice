Ext.define('AM.model.Process',{
	extend : 'Ext.data.Model',
	
	//idProperty : 'ProcessID',
	fields : ['Name', 'ClientID', 'Client_Name','ProcessName','ProcessDescription','ProcessID','Vertical_Name', 'VerticalID', 'OnBoardingDate', 'ProductionStartDate', { name: 'Day_Name', mapping : 'Day_Name' , type:'auto'}, 'AM', 'Process_leads', 'TechnologyID','processID', 'Attribute_Names','Approver','AddedBy','EmployID','Name','ProcessStatus','HeadCount', 'ProcessType', 'ProcessEmployees','Billable','minHours', 
	'fm_util', 'fm_error', 'fm_month', 'sm_util', 'sm_error', 'sm_month', 'tm_util', 'tm_error', 'tm_month', 'TotalDoc','ForcastNRR','ForcastRDR','OrderofAttributes' ]
	
});