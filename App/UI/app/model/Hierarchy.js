Ext.define('AM.model.Hierarchy',{
	extend : 'Ext.data.Model',
	fields: [ 'id','text','icon','leaf','tool_tip',{name: 'qtip', mapping: 'tool_tip'}]
});