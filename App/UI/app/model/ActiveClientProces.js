Ext.define('AM.model.ActiveClientProces',{
	extend : 'Ext.data.Model',
	
	fields : [{name : 'ClientID', dataType : 'int'},'Client_Name','FirstName','vertical_Name','process_Name','resourceCount','ApprovedHC','industryType','accManager','status','inactiveDate','Lead','AManager','ClientSince','ParentVertical']
});