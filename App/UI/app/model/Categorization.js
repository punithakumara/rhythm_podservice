Ext.define('AM.model.Categorization',{
	extend : 'Ext.data.Model',
	
	fields : ['DesignationID','designationName','grades','employee_id','Email', 'supervisor','project_type_id','project_type','wor_id','company_employ_id', 'id', 'billable_type','first_name','last_name','Email','start_date','end_date','Mobile',{name:'modifiedRec',type: 'bool'},'Aim',{name:'billable', type: 'bool', convert:function(v,rec){
						
			if( v == true){
				return "Yes";
			}
			else if(rec.get('billable_type') == 'Approved Buffer' || rec.get('billable_type') == 'FTE' || rec.get('billable_type') == 'FTE Contract' || rec.get('billable_type') == 'Hourly' || rec.get('billable_type') == 'Volume' || rec.get('billable_type') == 'Project') {
				return "Yes";
			}
			else if(rec.get('billable_type') == 'Buffer') {
				return "No";
			} 		
			else{
				return "No";
			}
		}
	},{	name: 'FullName', 
		convert: function(v, rec) { 
					var fullName = rec.get('first_name');
					if(!rec.get('last_name'))
					{
						fullName = fullName;
					}
					else if(rec.get('last_name')!='NULL' && rec.get('last_name')!="" && rec.get('last_name')!='null')
						fullName += " "+rec.get('last_name');
					return fullName;
				 } 
  	  }]
});