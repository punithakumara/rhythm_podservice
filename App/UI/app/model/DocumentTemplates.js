Ext.define('AM.model.DocumentTemplates',{
	extend : 'Ext.data.Model',
	fields : [{name : 'template_id', dataType : 'int'},'template_name','document_templates','uploaded_default_name','upload_by','FirstName','TemplateDate','VerticalName']
});