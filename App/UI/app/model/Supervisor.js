Ext.define('AM.model.Supervisor',{
	extend : 'Ext.data.Model',
	fields: ['supervisor_id','supervisor_name']
});