Ext.define('AM.model.ConsolidatedUtilizationReport',{
	extend : 'Ext.data.Model',
	fields : ['UtilizationID','client_id', 'vertical_name', 'client_name','Resources','Year','Month','No_Tasks','No_Hours','No_Days','Utilization','Error','GroupBy','Resources','expectedHours','Resources2','Billable_Resource','billability_total','WorkedBillable','WorkedNonBillable']
});