Ext.define('AM.model.WorkTypeFTE',{
	extend : 'Ext.data.Model',
	fields : ['fte_id','fk_cor_id','fk_project_type_id','no_of_fte','appr_buffer', 'shift', 'experience', 'skills', 'anticipated_date', 'support_coverage', 'responsibilities','change_type','process_id','comments','project_type','project_name']
});