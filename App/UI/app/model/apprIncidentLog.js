Ext.define('AM.model.apprIncidentLog',{
	extend : 'Ext.data.Model',
	fields : ['id','log_date','appr_id','appr_date','client_id','Client','responsible_member','responsible_member_name','description','createdBy','file','appr_by','attachment','wor_id','wor_name','aprattach','aprattach_ori','createdOn','team_leader','team_lead_name']
});