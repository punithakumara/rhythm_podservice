Ext.define('AM.model.Attributes',{
	extend : 'Ext.data.Model',
	
	fields : ['attributes_id','name','type', 'ListValues','mandatory','ValuesStatus','description','values','selected_checkbox']
});