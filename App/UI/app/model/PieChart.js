Ext.define('AM.model.PieChart',{
	extend : 'Ext.data.Model',
	fields: ['Name', 'Value1','Value2','Value3','Value4','Value5','Value6','Value7','ProcessID', 'ClientID', 'VerticalID', 'location_id', 'shift_id', 'EmployID', 'Grade','Client_Name']
});