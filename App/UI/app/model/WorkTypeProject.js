Ext.define('AM.model.WorkTypeProject',{
	extend : 'Ext.data.Model',
	fields : ['project_id', 'fk_cor_id','fk_project_type_id','project_name', 'shift', 'start_duration', 'end_duration','anticipated_date', 'experience', 'skills', 'production_date', 'support_coverage', 'responsibilities','change_type','project_description','process_id','comments','project_type']
});