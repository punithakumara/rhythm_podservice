Ext.define('AM.model.EmployeeCertification',{
	extend : 'Ext.data.Model',
	fields : [
		{name : 'id', dataType : 'int'},'employee_id','certificate_name','content','year','created_on','updated_on',
	]
});