Ext.define('AM.model.newEscLog',{
	extend : 'Ext.data.Model',
	fields : ['id','error_id','log_date','error_date','client_id','client','manager','employee_id','escalation_date','problem_summary','impact','impact_business','impact_revenue','impact_relationship','rootcause_id','rootcause_analysis','mitigation_plan','mitigation_plan_date','production_member','production_member_name','approved_by','created_by','file','reviewed_by', 'approved_by','attachment','manager_name','reviewed_by_name','approved_by_name','wor_id','wor_name','escattach','escattach_ori','created_on']
});