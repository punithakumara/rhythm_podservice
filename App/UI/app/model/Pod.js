Ext.define('AM.model.Pod',{
	extend : 'Ext.data.Model',
	fields : ['pod_id','Name','name','description','folder','MG','MGName','ShiftCode','shift_id', 'pod_name', 'status', 'podStatusCombo', 'parent_pod_id' ]
});