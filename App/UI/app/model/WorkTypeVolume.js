Ext.define('AM.model.WorkTypeVolume',{
	extend : 'Ext.data.Model',
	fields : ['vb_id','fk_cor_id','fk_project_type_id','volume_coverage', 'shift', 'min_volume', 'max_volume','anticipated_date', 'experience', 'skills', 'production_date', 'support_coverage', 'responsibilities','change_type','process_id','comments','project_type','project_name']
});