Ext.define('AM.model.Location',{
	extend : 'Ext.data.Model',
	
	fields: ['location_id', 'name']
});