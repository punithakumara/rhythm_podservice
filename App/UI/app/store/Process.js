Ext.define('AM.store.Process', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Process',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			create : AM.app.globals.appPath+'index.php/process/process_add/?v='+AM.app.globals.version,
			read : AM.app.globals.appPath+'index.php/process/process_list/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/process/process_update/',
			destroy : AM.app.globals.appPath+'index.php/process/process_del/'
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'processID'
		},
        writer: {
            type: 'json',
            root : 'process',
            idProperty : 'processID'
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});