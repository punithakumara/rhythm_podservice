Ext.define('AM.store.Locations', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Location',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/location/locations_list/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'location',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
	}
});