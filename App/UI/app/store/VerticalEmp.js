Ext.define('AM.store.VerticalEmp', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Categorization',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/empVertical_view/?v='+AM.app.globals.version
			/*create : AM.app.globals.appPath+'index.php/categorization/employee_add/',
			update: AM.app.globals.appPath+'index.php/categorization/employee_update/',
			destroy : AM.app.globals.appPath+'index.php/categorization/employee_del/'*/
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'EmployID'
		},
		 writer: {
            type: 'json',
            root : 'employ',
            idProperty : 'EmployID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'FirstName',
        direction: 'ASC'
    }]
});