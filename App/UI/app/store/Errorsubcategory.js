Ext.define("AM.store.Errorsubcategory", {
    extend: "Ext.data.Store",
    fields: ['error_category_id','category_name','parent_category_id'],
  //  model: "AM.model.Errorcategory",
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/ErrorCategory/list/?v="+AM.app.globals.version,          
        },
        reader: {
            type: "json",
            root: "rows",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "error_category_id"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "error_category_id",
            writeAllFields: true,
            encode: true
        }
    },
    sortOnLoad: true,
   /* sorters: [ {
        property: "first_name",
        direction: "ASC"
    } ]*/
});