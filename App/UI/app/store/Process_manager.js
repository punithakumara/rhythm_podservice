Ext.define('AM.store.Process_manager', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['EmployID','Name'],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/empProcessAM_view?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'CompanyEmployID'
		},
		writer: {
            type: 'json',
            root : 'employ',
            idProperty : 'CompanyEmployID',
            'allowSingle' : false,
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'first_name',
        direction: 'ASC'
    }]
});