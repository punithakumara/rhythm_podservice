Ext.define('AM.store.timesheetTaskCode', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	fields: [
        {'name': 'task_name', 'type': 'string'},  
	],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/timesheet/task_code_list/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
		}
	},
	sortOnLoad:true,
});
