Ext.define('AM.store.newIncidentLog', {
	extend : 'Ext.data.Store',
	model : 'AM.model.newIncidentLog',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/incidentLog/log_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/incidentLog/log_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/incidentLog/log_update/',
			destroy : AM.app.globals.appPath+'index.php/incidentLog/log_del/'
		},		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'LogID'
		},
        writer: {
            type: 'json',
            root : 'log',
            idProperty : 'LogID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Date',
        direction: 'DESC'
    }]
});