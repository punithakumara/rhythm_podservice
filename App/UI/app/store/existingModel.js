Ext.define("AM.store.existingModel", {
    extend: "Ext.data.Store",
    fields: ['name', 'name'],
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/Clientcor/existingModel/",
        },
        reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			idProperty : 'change_order_id'
		},
		writer: {
			type: 'json',
			root : 'rows',
			idProperty : 'cor_id',
			writeAllFields: true,
			encode : true
		}
    },
    sortOnLoad:true,
	
	sorters: [{
        property: 'cor_id',
        direction: 'DESC'
    }]
});