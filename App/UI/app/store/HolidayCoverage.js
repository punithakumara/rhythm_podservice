Ext.define('AM.store.HolidayCoverage', {
	extend : 'Ext.data.Store',
	model : 'AM.model.UtilizationDashboard',
	autoLoad: false,
	storeId:'utilizationHolidayCoverage',
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			update : AM.app.globals.appPath+'index.php/utilization/utilization_update/',		
			read : AM.app.globals.appPath+'index.php/utilization/utilization_dashboard/?dashboard=1&v='+AM.app.globals.version,
			url : AM.app.globals.appPath+'index.php/utilization/utilizationDashboard_upload/?v='+AM.app.globals.version
		},
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'total',
			idProperty : 'utilization_id'
		},
		 writer: {
			type: 'json',
            root : 'utilization',
            idProperty : 'utilization_id',
            writeAllFields: true,
            encode : true
        }
	
	},
	sortOnLoad:true,
	
	sorters: [{
        property: 'created_on',
        direction: 'DESC'
    }]
});