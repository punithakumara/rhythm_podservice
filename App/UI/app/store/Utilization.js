Ext.define('AM.store.Utilization', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Utilization',
	autoLoad: false,
	storeId:'utilizationId',
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/utilization_view/?v='+AM.app.globals.version,
			// create : AM.app.globals.appPath+'index.php/utilization/utilization_create/?v='+AM.app.globals.version,
			// update : AM.app.globals.appPath+'index.php/utilization/utilization_update/',
			// url : AM.app.globals.appPath+'index.php/utilization/utilization_upload/'
		},
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'UtilizationID'
		},
		 writer: {
			type: 'json',
            root : 'utilization',
            idProperty : 'UtilizationID',
            writeAllFields: true,
            encode : true
        }
	
	},
	sortOnLoad:true,
	
	sorters: [{
        property: 'FullName',
        direction: 'ASC'
    }]
});