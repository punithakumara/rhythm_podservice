Ext.define('AM.store.AllReportees', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['employee_id','company_employ_id','FullName','designationName','Email','supervisor_id','supervisor','grades','training_status','training_needed','current_shift','vertName','podName','serviceName'],

	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employees/AllReportees/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'employee_id'
		}
	},
	sortOnLoad:true,
	
	sorters: [{
	    property: 'e.first_name',
	    direction: 'ASC'
	}]

});
