Ext.define('AM.store.Languages', {
	extend : 'Ext.data.Store',
	model: "AM.model.EmployeeLanguages",
/*	fields: [
        {'name': 'language_id', 'type': 'int'},
        {'name': 'language', 'type': 'string'}
	],*/
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read: AM.app.globals.appPath + "index.php/EmployeeLanguage/language_list/?v="+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/EmployeeLanguage/language_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/EmployeeLanguage/language_edit/?v='+AM.app.globals.version,
			destroy : AM.app.globals.appPath+'index.php/EmployeeLanguage/language_del/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'id'
		},
        writer: {
            type: 'json',
            root : 'language',
            idProperty : 'id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	 
});