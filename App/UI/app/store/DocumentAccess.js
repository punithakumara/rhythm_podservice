
Ext.define('AM.store.DocumentAccess', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"0", "name":"All"},
        {"id":"1", "name":"All managers"},
        {"id":"2", "name":"Vertical Specific"},
        {"id":"3", "name":"Process Specific"}
    ]
});