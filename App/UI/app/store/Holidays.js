Ext.define('AM.store.Holidays', {
	extend: 'Ext.data.Store',
	model : 'AM.model.Holidays',
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
	
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Holidays/holiday_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/Holidays/holiday_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/Holidays/holiday_update/?v='+AM.app.globals.version,
			destroy : AM.app.globals.appPath+'index.php/Holidays/holiday_del/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'holiday_id'
		},
        writer: {
            type: 'json',
            root : 'holiday',
            idProperty : 'holiday_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});