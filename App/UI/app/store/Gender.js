
Ext.define('AM.store.Gender', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"male", "name":"Male"},
        {"id":"female", "name":"Female"}
    ]
});