Ext.define('AM.store.Tech', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Tech',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/technology/technology_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/technology/technology_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/technology/technology_update/?v='+AM.app.globals.version,
			destroy : AM.app.globals.appPath+'index.php/technology/technology_del/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'technology_id'
		},
        writer: {
            type: 'json',
            root : 'technology',
            idProperty : 'technology_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'technology_id',
        direction: 'DESC'
    }]
});