Ext.define('AM.store.ReportTo', {
	extend : 'Ext.data.Store',
	//model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['employee_id','Name'],

	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employees/ReportTo/?v='+AM.app.globals.version,

		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'employee_id'
		}
	},
	sortOnLoad:true,
	
	sorters: [{
	    property: 'first_name',
	    direction: 'ASC'
	}]

});
