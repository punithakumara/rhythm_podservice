Ext.define('AM.store.Grade', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Grade',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/designation/grades_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'grade',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'designation_id'
		},
	},
	sortOnLoad:true,
	sorters: [{
        property: 'hcm_grade',
        direction: 'ASC'
    }]
});