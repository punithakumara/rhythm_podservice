Ext.define('AM.store.userProfile', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employees/employees_details/?v='+AM.app.globals.version
		},
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'EmployID'
		}
	},
	sortOnLoad:true,
	sorters: [{
        property: 'first_name',
        direction: 'ASC'
    }]
});