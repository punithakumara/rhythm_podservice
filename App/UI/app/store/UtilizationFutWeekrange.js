Ext.define('AM.store.UtilizationFutWeekrange', {
	extend : 'Ext.data.Store', 
    model : 'AM.model.TimesheetWeekrange',
	autoLoad: false,
	storeId:'utilizationweekrangeId',
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Utilization/utilization_future_range/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			idProperty : 'weekrangeID'
		},
	},
	sortOnLoad:true,
	
	sorters: [{
        property: 'weekrangeID',
        direction: 'DESC'
    }]
});