Ext.define('AM.store.newErrorLog', {
	extend : 'Ext.data.Store',
	model : 'AM.model.newIncidentLog',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/newIncidentLog/log_viewError/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/newIncidentLog/log_addError/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/newIncidentLog/log_updateError/',
			destroy : AM.app.globals.appPath+'index.php/newIncidentLog/log_delError/'
		},		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'LogID'
		},
        writer: {
            type: 'json',
            root : 'log',
            idProperty : 'LogID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'createdOn',
        direction: 'DESC'
    }]
});