Ext.define('AM.store.WorList', {
	extend : 'Ext.data.Store', 
    fields: ['work_order_id'],
    autoLoad: true,
    //pageSize: AM.app.globals.itemsPerPage,
    //autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/wor_list/?v='+AM.app.globals.version,			
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			idProperty : 'work_order_id'
		},
		
		 writer: {
	            type: 'json',
	            root : 'clientwor',
	            idProperty : 'work_order_id',
	            writeAllFields: true,
	            encode : true
	        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'work_order_id',
        direction: 'DESC'
    }]
});