Ext.define('AM.store.DeleteClientContact', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Client_Contacts',
	autoLoad: true,
    // pageSize: 5,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
   
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/ClientContacts/client_contacts_view/?v='+AM.app.globals.version,
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'contact_id'
		},
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'contact_name',
        direction: 'DESC'
    }]
});