Ext.define('AM.store.ErrorsChart', {
	extend : 'Ext.data.Store',
	storeId:'ErrorsChart',
	model : 'AM.model.PieChart',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/DashboardGraph/?LogType=Error&v='+AM.app.globals.version,
		reader :  {
            type: 'json',
             root : 'rows'
        }
	}	
});