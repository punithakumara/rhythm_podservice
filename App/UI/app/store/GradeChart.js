Ext.define('AM.store.GradeChart', {
	extend : 'Ext.data.Store',
	model : 'AM.model.PieChart',
	storeId : 'GradeChart',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getGradeVerticalDetails/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'data'
        }
	}	
});