Ext.define('AM.store.Remarks', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'change_date', type: 'string'},
        {name: 'count', type: 'string'},
        {name: 'change_type', type: 'int'},
        {name: 'shift', type: 'string'},
    ],
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/MonthlyBillabilityReport/MonthlyBillabilityFteRemarks_view/?v='+AM.app.globals.version,
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			//idProperty : 'change_order_id'
		}
	}, 
	
	// groupField: 'client_name',
	idProperty: 'id',
});