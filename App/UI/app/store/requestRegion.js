Ext.define('AM.store.requestRegion', {
	extend : 'Ext.data.Store',
    fields: [
		{'name': 'reg_id', 'type': 'int'},
        {'name': 'reg_name', 'type': 'string'},
    ],
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/requestRegion_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'data',
			totalProperty: 'totalCount',
			idProperty : 'reg_id'
		},
		writer: {
			type: 'json',
			root : 'data',
			idProperty : 'reg_id',
			writeAllFields: true,
			encode : true
		}
	},
});