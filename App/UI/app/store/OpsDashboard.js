Ext.define('AM.store.OpsDashboard', {
	extend : 'Ext.data.Store',
	fields: ['EmployeeID','EmployeeFullName','Location','WorkShift','JoiningDate','Pod','Service','ClientName','Grade','Designation','Supervisor','ReportingManager','PodManager','Billable','NoticePeriod','ResignationDate'],
	autoLoad: true,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	pageSize: AM.app.globals.itemsPerPage,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/reports/opsDashboard?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'EmployeeID'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'EmployeeFullName',
        direction: 'ASC'
    }]
	
});