Ext.define('AM.store.apprIncidentLog', {
	extend : 'Ext.data.Store',
	model : 'AM.model.apprIncidentLog',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/newIncidentLog/log_viewAppr/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/newIncidentLog/log_addAppr/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/newIncidentLog/log_updateAppr/',
			destroy : AM.app.globals.appPath+'index.php/newIncidentLog/log_delAppr/'
		},		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'id'
		},
        writer: {
            type: 'json',
            root : 'rows',
            idProperty : 'id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'createdOn',
        direction: 'DESC'
    }]
});