Ext.define('AM.store.Pod', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Pod',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Pod/pod_view/',
			create : AM.app.globals.appPath+'index.php/Pod/pod_add/',
			update: AM.app.globals.appPath+'index.php/Pod/pod_update/',
			destroy : AM.app.globals.appPath+'index.php/Pod/pod_del/'
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'pod_id'
		},
		
        writer: {
            type: 'json',
            root : 'pod',
            idProperty : 'pod_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});