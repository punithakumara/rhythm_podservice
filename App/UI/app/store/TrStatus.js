Ext.define('AM.store.TrStatus', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"1", "name":"Incomplete"},
        {"id":"2", "name":"Complete"}
    ]
});