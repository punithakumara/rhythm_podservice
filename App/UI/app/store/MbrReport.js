Ext.define('AM.store.MbrReport', {
	extend : 'Ext.data.Store',
	model : 'AM.model.MbrReport',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			create : AM.app.globals.appPath+'index.php/Reports/MbrMapping/?v='+AM.app.globals.version
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'DocumentID'
		},
        writer: {
            type: 'json',
            root : 'document',
            idProperty : 'DocumentID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'UploadName',
        direction: 'ASC'
    }]
});