Ext.define('AM.store.LocationClientChart', {
	extend : 'Ext.data.Store',
	storeId:'LocationClientChart',
	model : 'AM.model.PieChart',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getlocationWiseClientData/?v='+AM.app.globals.version,
		reader :  {
            type: 'json'
        }
	}	
});