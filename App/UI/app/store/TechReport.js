Ext.define('AM.store.TechReport', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Tech',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/technology/technology_view/?v='+AM.app.globals.version
			},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'technology_id'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});