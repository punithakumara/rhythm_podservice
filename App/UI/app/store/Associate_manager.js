/*Ext.define('AM.store.Employees',{
	extend : 'Ext.data.Store',
	model : 'AM.model.Employee',
	data : [
		{name: 'Ed',    email: 'ed@sencha.com'},
		{name: 'Tommy', email: 'tommy@sencha.com'}
	]
});*/
Ext.define('AM.store.Associate_manager', {
	extend : 'Ext.data.Store',
	// model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['employee_id','Name'],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employees/associateManagers/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'company_employ_id'
		},
		 writer: {
            type: 'json',
            root : 'employ',
            idProperty : 'company_employ_id',
            'allowSingle' : false,
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'first_name',
        direction: 'ASC'
    }]
});