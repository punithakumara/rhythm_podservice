Ext.define('AM.store.ParentVertical', {
	extend : 'Ext.data.Store',
	fields: ['name', 'groupBy', 'Id','pVerticalName'],
	groupField: 'groupBy',
	autoLoad: true,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/vertical/getParentVertical?v='+AM.app.globals.version,//parentVertical/',
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'Id'
		},
		writer: {
			type: 'json',
			root : 'rows',
			idProperty : 'Id',
			writeAllFields: true,
			encode : true
		}
	},
	sortOnLoad:true,

	sorters: [{
		property: 'pVerticalName',
		direction: 'ASC'
	}]
});