Ext.define('AM.store.ProcessDocs', {
	extend : 'Ext.data.Store',
	storeId:'ProcessDocs',
	model : 'AM.model.Document',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/documents/document_view/?Access=Process Specific&v='+AM.app.globals.version,
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'DocumentID'
		}
	}	
});