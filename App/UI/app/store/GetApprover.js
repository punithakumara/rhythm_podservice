Ext.define('AM.store.GetApprover', {
	extend : 'Ext.data.Store',
//	model : 'AM.model.Process',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['EmployID','Name'],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employee/GetApproverDetails/?v='+AM.app.globals.version,
		},
			
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			// idProperty : 'EmployID'
		},
	},
	sortOnLoad:true,
	
	sorters: [{
	    property: 'first_name',
	    direction: 'ASC'
	}]

});