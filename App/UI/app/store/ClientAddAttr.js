Ext.define('AM.store.ClientAddAttr', {
	extend : 'Ext.data.Store',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: false,
    autoSync: false,
    fields: ['attributes_id', 'name', 'description', 'selected', 'mandatory'],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/ClientAddAttr/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
	},
	
	sortOnLoad:false,

});
