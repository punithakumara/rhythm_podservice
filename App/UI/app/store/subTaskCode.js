Ext.define('AM.store.subTaskCode', {
	extend : 'Ext.data.Store',
	fields: [	
        {'name': 'sub_task_id', 'type': 'int'},
        {'name': 'task_name', 'type': 'string'},
	],
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/SubTaskList/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
	},
});
