Ext.define('AM.store.TimesheetStatusTeam', {
	extend : 'Ext.data.Store',
	fields: ['Name', 'Value1'],
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/timesheet/getTimesheetStatusTeam/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
});