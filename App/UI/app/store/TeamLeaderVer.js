Ext.define("AM.store.TeamLeaderVer", {
    extend: "Ext.data.Store",
    model: "AM.model.Employee",
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/employees/tl_vertical_based/?v="+AM.app.globals.version,
        },
        reader: {
            type: "json",
            root: "data",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "employee_id"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "employee_id",
            writeAllFields: true,
            encode: true
        }
    },
    sortOnLoad: true,
    sorters: [ {
        property: "first_name",
        direction: "ASC"
    } ]
});