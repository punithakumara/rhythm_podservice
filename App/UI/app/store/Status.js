
Ext.define('AM.store.Status', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
    {"id":"3", "name":"Active"},
    {"id":"5", "name":"Resign"},
    {"id":"6", "name":"Relieve"}
       
    ]
});