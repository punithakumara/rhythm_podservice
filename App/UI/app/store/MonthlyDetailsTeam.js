Ext.define('AM.store.MonthlyDetailsTeam', {
	extend : 'Ext.data.Store',
	fields : ['month','timesheetHrs', 'productionHrs', 'totalHrs'],
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/timesheet/getMonthlyDetailsTeam/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
});