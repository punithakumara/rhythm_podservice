Ext.define('AM.store.workOrder', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['wor_id','wor_name','work_order_name','start_date','end_date'],

	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/get_wors/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'wor_id'
		}
	},
	sortOnLoad:true,
	
	sorters: [{
	    property: 'wor_name',
	    direction: 'ASC'
	}]

});
