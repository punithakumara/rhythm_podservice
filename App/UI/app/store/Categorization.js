Ext.define('AM.store.Categorization', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Categorization',
	autoLoad: true,
    // pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/empCategore_view/?v='+AM.app.globals.version
			/*create : AM.app.globals.appPath+'index.php/categorization/employee_add/',
			update: AM.app.globals.appPath+'index.php/categorization/employee_update/',
			destroy : AM.app.globals.appPath+'index.php/categorization/employee_del/'*/
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'id'
		},
		 writer: {
            type: 'json',
            root : 'data',
            idProperty : 'id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'grades',
        direction: 'DESC'
    }]
});