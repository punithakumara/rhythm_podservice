Ext.define('AM.store.ChangeType', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"1", "name":"Addition"},
        {"id":"2", "name":"Deletion"},
      //  {"id":"3", "name":"Change"},
    ]
});