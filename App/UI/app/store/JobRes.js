Ext.define('AM.store.JobRes', {
	extend : 'Ext.data.Store', 
    fields: ['job_responsibilities'],
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
	autoLoad: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/job_role_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});

