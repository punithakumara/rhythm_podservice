Ext.define('AM.store.billableType', {
    extend : 'Ext.data.Store',
    fields: ['id', 'name'],

    proxy : {
        type : 'rest',
        api : {
            read : AM.app.globals.appPath+'index.php/clientwor/models/?v='+AM.app.globals.version,
        },
        
        reader : {
            type : 'json',
            root: 'data',
            successProperty : 'success',
            totalProperty: 'totalCount',
            idProperty : 'id'
        }
    },
    sortOnLoad:true,
    
    sorters: [{
        property: 'name',
        direction: 'ASC'
    }]

});
