Ext.define('AM.store.Transition', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Transition',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Transition/transition_view/?v='+AM.app.globals.version
		},		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'employee_id'
		},
		writer: {
            type: 'json',
            root : 'employ',
            idProperty : 'employee_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'first_name',
        direction: 'ASC'
    }]
});