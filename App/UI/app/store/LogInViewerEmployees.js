Ext.define('AM.store.LogInViewerEmployees', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employees/TlBillableEmployees?client_id=&client_id=&date=&v='+AM.app.globals.version
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'employee_id'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'first_name',
        direction: 'ASC'
    }]
});