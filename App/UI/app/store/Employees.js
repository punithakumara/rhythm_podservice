Ext.define("AM.store.Employees", {
    extend: "Ext.data.Store",
    model: "AM.model.Employee",
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/employees/employees_details/?v="+AM.app.globals.version,
            create: AM.app.globals.appPath + "index.php/employees/employee_add/?v="+AM.app.globals.version,
            update: AM.app.globals.appPath + "index.php/employees/employee_update/",
            destroy: AM.app.globals.appPath + "index.php/employees/employee_del/",
            url: AM.app.globals.appPath + "index.php/employees/employee_upload/"
        },
        reader: {
            type: "json",
            root: "data",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "employee_id"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "employee_id",
            writeAllFields: true,
            encode: true
        }
    },
    sortOnLoad: true,
    sorters: [ {
        property: "first_name",
        direction: "ASC"
    } ]
});