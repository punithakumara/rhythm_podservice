Ext.define('AM.store.Responsibilities', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
	autoLoad: true,
    autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/role_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});

