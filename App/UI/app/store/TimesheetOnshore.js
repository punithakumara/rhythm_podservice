Ext.define('AM.store.TimesheetOnshore', {
	extend : 'Ext.data.Store', 
    model : 'AM.model.Timesheet',
	autoLoad: false,
	storeId:'timesheetId',
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/timesheetOnshore/timesheet_view/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success',
			idProperty : 'timesheetID'
		},
	},
	sortOnLoad:true,
	
	sorters: [{
        property: 'timesheetID',
        direction: 'DESC'
    }]
});