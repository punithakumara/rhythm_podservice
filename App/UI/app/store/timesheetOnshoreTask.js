Ext.define('AM.store.timesheetOnshoreTask', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	fields: [
        {'name': 'task_type', 'type': 'string'},  
	],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/timesheetOnshore/task_list/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
		}
	},
	sortOnLoad:true,
});
