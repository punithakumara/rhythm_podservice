Ext.define('AM.store.SupportCoverage', {
	extend : 'Ext.data.Store', 
    fields: ['support_id', 'support_type','short_code'],
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/Employees/list_supportcoverage/?v="+AM.app.globals.version,          
        },
        reader: {
            type: "json",
            root: "rows",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "support_id"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "support_id",
            writeAllFields: true,
            encode: true
        }
    },
    sortOnLoad: true,
   /* sorters: [ {
        property: "first_name",
        direction: "ASC"
    } ]*/
   /* data : [
        {"id":"1", "name":"Mon-Fri"},
        {"id":"2", "name":"Tue-Sat"},
        {"id":"3", "name":"Wed-Sun"},
        {"id":"4", "name":"Thu-Mon"},
        {"id":"5", "name":"Fri-Tue"},
        {"id":"6", "name":"Sat-Wed"},
        {"id":"7", "name":"Sun-Thu"},
        {"id":"8", "name":"Mon-Sat"},
        {"id":"9", "name":"Mon-Sun"},
    ]*/
});