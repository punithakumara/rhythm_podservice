Ext.define('AM.store.filterRoleCode', {
	extend : 'Ext.data.Store',
	fields: [
		{'name': 'process_id', 'type': 'int'},
		{'name': 'project_code', 'type': 'string'},
	],
	autoLoad: true,
	pageSize: AM.app.globals.itemsPerPage,
	remoteSort: true,
	remoteFilter: true,
	autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/role_code_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'data',
			totalProperty: 'totalCount',
			idProperty : 'process_id'
		},
		writer: {
			type: 'json',
			root : 'data',
			idProperty : 'process_id',
			writeAllFields: true,
			encode : true
		}
	},
});