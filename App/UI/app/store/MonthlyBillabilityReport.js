Ext.define('AM.store.MonthlyBillabilityReport', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'client_name', type: 'string'},
        {name: 'wor_name', type: 'string'},
        {name: 'wor_type', type: 'string'},
        {name: 'project_type', type: 'string'},
        {name: 'role_type', type: 'string'},
        {name: 'min_hours', type: 'int'},
        {name: 'max_hours', type: 'int'},
        {name: 'min_volume', type: 'int'},
        {name: 'max_volume', type: 'int'},
        {name: 'actual_volume', type: 'int'},
        {name: 'billable_hrs', type: 'int'},
        {name: 'weekend_hrs', type: 'int'},
        {name: 'client_holiday_hrs', type: 'int'},
        {name: 'status', type: 'string'},
    ],
	autoLoad: true,
	autoSync: true,
	//pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/MonthlyBillabilityReport/MonthlyBillabilityReport_view/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			//idProperty : 'change_order_id'
		}
	}, 
	
	groupField: 'client_name',
	idProperty: 'id',
});