Ext.define('AM.store.ClientContact_Role', {
	extend : 'Ext.data.Store', 
    fields: ['RoleId', 'RoleDescription'],
    data : [
        {"RoleId":"1", "RoleDescription":"Day to Day in charge"},
        {"RoleId":"2", "RoleDescription":"Day to Day in charge (UK)"},
        {"RoleId":"3", "RoleDescription":"Day to Day in charge (US)"},
        {"RoleId":"4", "RoleDescription":"Main POC"},
        {"RoleId":"5", "RoleDescription":"Billing Contact"}
    ]
});