Ext.define('AM.store.SkillSets', {
	extend : 'Ext.data.Store',
	model: "AM.model.EmployeeSkillset",
/*	fields: [
		{'name': 'skillset_id', 'type': 'int'},
		{'name': 'skillset', 'type': 'string'}
	],*/
	autoLoad: true,
	pageSize: AM.app.globals.itemsPerPage,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	
	proxy : {
		type : 'rest',
		api : {
			//read : AM.app.globals.appPath+'index.php/skills/getSkillsets/?v='+AM.app.globals.version,
			read: AM.app.globals.appPath + "index.php/EmployeeSkillset/skillset_list/?v="+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/EmployeeSkillset/skillset_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/EmployeeSkillset/skillset_edit/?v='+AM.app.globals.version,
			destroy : AM.app.globals.appPath+'index.php/EmployeeSkillset/skillset_de/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'id'
		},
        writer: {
            type: 'json',
            root : 'certificate',
            idProperty : 'id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
});