Ext.define('AM.store.ApproveUtilization', {
	extend : 'Ext.data.Store', 
    model : 'AM.model.Utilization',
	autoLoad: false,
	storeId:'UtilizationID',
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		// timeout: 120000,
		api : {
			read : AM.app.globals.appPath+'index.php/Utilization/utilization_approve/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success',
			idProperty : 'UtilizationID'
		},
	},
	sortOnLoad:true,
	groupField: 'EmployeeName',
	idProperty: 'UtilizationID',
});