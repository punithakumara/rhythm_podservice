Ext.define('AM.store.ProjectTypes1', {
	extend : 'Ext.data.Store', 
    fields: ['project_type_id', 'project_name'],
	autoLoad: true,
    autoSync: true,
	proxy : {
		type : 'rest',
		timeout: 1800000,
		api : {
			read : AM.app.globals.appPath+'index.php/projectTypes/project_types/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});

