Ext.define('AM.store.AllManagers', {
	extend : 'Ext.data.Store',
	// model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: true,
    fields:['employee_id','Name'],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Employees/AllManagers/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'employee_id'
		},
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'first_name',
        direction: 'ASC'
    }]
});