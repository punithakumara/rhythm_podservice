Ext.define('AM.store.ShiftClientChart', {
	extend : 'Ext.data.Store',
	storeId:'ShiftClientChart',
	model : 'AM.model.PieChart',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getShiftWiseClientData/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
             root : 'rows'
        }
	}	
});