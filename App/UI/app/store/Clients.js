Ext.define('AM.store.Clients', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Client',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/client_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/clients/client_save/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/clients/client_save/',
			destroy : AM.app.globals.appPath+'index.php/clients/client_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'client_id'
		},
		writer: {
            type: 'json',
            root : 'client',
            idProperty : 'client_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'client_name',
        direction: 'ASC'
    }]
});