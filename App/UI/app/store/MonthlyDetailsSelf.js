Ext.define('AM.store.MonthlyDetailsSelf', {
	extend : 'Ext.data.Store',
	fields : ['month','timesheetHrs', 'productionHrs', 'totalHrs'],
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/timesheet/getMonthlyDetailsSelf/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
});