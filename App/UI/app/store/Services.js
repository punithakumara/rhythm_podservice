Ext.define('AM.store.Services', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Services',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Services/services_view/',
			create : AM.app.globals.appPath+'index.php/Services/services_add/',
			update: AM.app.globals.appPath+'index.php/Services/services_update/',
			destroy : AM.app.globals.appPath+'index.php/Services/services_del/'
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'service_id'
		},
		
        writer: {
            type: 'json',
            root : 'services',
            idProperty : 'service_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});