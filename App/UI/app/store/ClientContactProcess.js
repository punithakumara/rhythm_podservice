Ext.define('AM.store.ClientContactProcess', {
	extend		: 'Ext.data.Store',
	autoLoad	: true,
	fields		: ['ProcessID', 'ProcessName'],
    pageSize	: AM.app.globals.itemsPerPage,
    remoteSort	: true,
    remoteFilter: true,
    autoSync	: false,
	proxy		: {
		type	: 'rest',
		api		: {
			read	: AM.app.globals.appPath+'index.php/process/getClientContactProcess/?v='+AM.app.globals.version,
		},
		
		reader	: {
			type			: 'json',
			root			: 'rows',
			successProperty : 'success',
			totalProperty	: 'totalCount',
			idProperty		: 'ProcessID'
		},
	},
	
	sortOnLoad	: true,
	sorters		: [ {
        property	: 'Name',
        direction	: 'ASC'
    } ]
} );