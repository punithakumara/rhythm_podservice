Ext.define('AM.store.Language', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Language',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/EmployeeLanguage/language_view/?v='+AM.app.globals.version,
			//create : AM.app.globals.appPath+'index.php/technology/technology_add/?v='+AM.app.globals.version,
			//update: AM.app.globals.appPath+'index.php/technology/technology_update/?v='+AM.app.globals.version,
			//destroy : AM.app.globals.appPath+'index.php/technology/technology_del/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'language_id'
		},
        writer: {
            type: 'json',
            root : 'language',
            idProperty : 'language_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'language_id',
        direction: 'DESC'
    }]
});