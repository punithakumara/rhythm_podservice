Ext.define("AM.store.Notifications", {
    extend: "Ext.data.Store",
    fields: [ "NotificationID", "ReadDate", "Title", "Description", "SenderEmployee", 
    "ReadFlag", "Type", "NotificationDate","FirstName", "LastName", {
        name: "FullName",
        convert: function(v, rec) {
            var fullName = rec.get("FirstName");
            if (!rec.get("LastName")) 
            	fullName = fullName; 

            else if ("NULL" != rec.get("LastName") 
            	&& "" != rec.get("LastName") 
            	&& "null" != rec.get("LastName")) 
            	fullName += " " + rec.get("LastName");

            return fullName;
        }
    } ],
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    autoLoad: true,
    proxy: {
        type: "rest",
        method: "GET",
        url: AM.app.globals.appPath + "index.php/employees/getNotifications/?v="+AM.app.globals.version,
        reader: {
            type: "json",
            root: "rows",
            totalProperty: 'totalCount'
        },

    },
    sortOnLoad:true

});