Ext.define('AM.store.TlEmployee', {
	extend : 'Ext.data.Store',
	// model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['employee_id','FirstName'],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/employees/TlEmployees/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'CompanyEmployID'
		},
		 writer: {
            type: 'json',
            root : 'employ',
            idProperty : 'CompanyEmployID',
            'allowSingle' : false,
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'FirstName',
        direction: 'ASC'
    }]
});