Ext.define('AM.store.TLProcess', {
	extend : 'Ext.data.Store',
	fields:['ProcessID','Name','EmployID'],
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/empTLRelPro_view/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'ProcessID'
		},
        writer: {
            type: 'json',
            root : 'process',
            idProperty : 'ProcessID'
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});