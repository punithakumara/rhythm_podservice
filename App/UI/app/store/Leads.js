Ext.define('AM.store.Leads', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['employee_id','Name','Grade'],
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/getLeads/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'leads',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});