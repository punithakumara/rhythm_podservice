Ext.define('AM.store.DocumentsTemplates', {
	extend : 'Ext.data.Store',
	model : 'AM.model.DocumentTemplates',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/DocumentsTemplates/template_view/?template_type=1&v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/DocumentsTemplates/template_save/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/DocumentsTemplates/template_save/',
			destroy : AM.app.globals.appPath+'index.php/DocumentsTemplates/template_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'template_id'
		},
        writer: {
            type: 'json',
            root : 'document',
            idProperty : 'template_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'document_templates.created',
        direction: 'DESC'
    }]
	
});