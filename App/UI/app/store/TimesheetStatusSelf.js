Ext.define('AM.store.TimesheetStatusSelf', {
	extend : 'Ext.data.Store',
	fields: ['Name', 'Value1'],
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/timesheet/getTimesheetStatusSelf/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
});