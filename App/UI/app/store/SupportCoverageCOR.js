Ext.define('AM.store.SupportCoverageCOR', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"1", "name":"Mon-Fri"},
        {"id":"2", "name":"Tue-Sat"},
        {"id":"3", "name":"Wed-Sun"},
        {"id":"4", "name":"Thu-Mon"},
        {"id":"5", "name":"Fri-Tue"},
        {"id":"6", "name":"Sat-Wed"},
        {"id":"7", "name":"Sun-Thu"},
        {"id":"8", "name":"Mon-Sat"},
        {"id":"9", "name":"Mon-Sun"},
    ]
});