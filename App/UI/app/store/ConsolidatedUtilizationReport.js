Ext.define('AM.store.ConsolidatedUtilizationReport', {
	extend : 'Ext.data.Store',
	model : 'AM.model.ConsolidatedUtilizationReport',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/ConsolidatedUtilizationReport/view/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'UtilizationID'
		}
		
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Year',
        direction: 'ASC'
    }]
});