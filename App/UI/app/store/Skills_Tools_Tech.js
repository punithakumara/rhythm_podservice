Ext.define('AM.store.Skills_Tools_Tech', {
	extend : 'Ext.data.Store',
	model: "AM.model.Skills_Tools_Tech",
	autoLoad: true,
	pageSize: AM.app.globals.itemsPerPage,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/skills/getSkillToolsTech/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			// idProperty : 'attributes_id'
		},
	},
	
	sortOnLoad:true,
	
});