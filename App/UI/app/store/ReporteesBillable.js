Ext.define('AM.store.ReporteesBillable', {
	extend : 'Ext.data.Store',
	storeId:'ReporteesBillable',
	model : 'AM.model.MyReporteeGridModel',
	autoLoad: true,
	proxy: {
        type: 'rest',
        method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getMyReporteesBillable/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
        }
	}
});