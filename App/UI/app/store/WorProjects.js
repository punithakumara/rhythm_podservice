Ext.define('AM.store.WorProjects', {
	extend : 'Ext.data.Store', 
    fields: ['project_type_id', 'project_type'],
	autoLoad: true,
    autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/worProject_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});

