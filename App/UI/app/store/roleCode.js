Ext.define('AM.store.roleCode', {
	extend : 'Ext.data.Store',
    fields: [
		{'name': 'si_no', 'type': 'int'},
        {'name': 'project_code', 'type': 'string'},
        {'name': 'client_name', 'type': 'string'},
        {'name': 'model', 'type': 'string'},
        {'name': 'shift_name', 'type': 'string'},
        {'name': 'support_coverage', 'type': 'string'},
        {'name': 'responsibilities', 'type': 'string'},
	],
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/role_code_list/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'data',
			totalProperty: 'totalCount',
			idProperty : 'si_no'
		},
		writer: {
			type: 'json',
			root : 'data',
			idProperty : 'si_no',
			writeAllFields: true,
			encode : true
		}
	},
});