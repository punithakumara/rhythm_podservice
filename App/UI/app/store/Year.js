Ext.define('AM.store.Year', {
	extend : 'Ext.data.Store', 
    fields: ['year_id','year_name'],
    autoLoad: true,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/EmployeeProfile/year_list/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});