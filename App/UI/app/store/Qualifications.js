Ext.define('AM.store.Qualifications', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Qualification',
	autoLoad: true,
    //pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/qualification/qualifications_list/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'qualifications',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
	}
});