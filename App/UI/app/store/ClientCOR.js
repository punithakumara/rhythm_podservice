Ext.define('AM.store.ClientCOR', {
	extend : 'Ext.data.Store',
    fields: ['cor_id','change_order_id', 'work_order_id','wor_type','wor_name', 'service_id', 'client', 'start_date', 'end_date', 'status', 'previous_work_order_id', 'name', 'created_by', 'client_partner', 'engagement_manager', 'delivery_manager', 'client_name', 'description', 'created_on', 'created_name','change_in_model', 'cp_name', 'em_name', 'dm_name', 'new_model', 'existing_model','change_request_date','wor_id','count_history','stake_holder','sh_name','wor_start_date','wor_end_date','tl_name','am_name','team_lead', 'associate_manager'],
    autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	remoteSort: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientcor/list/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/Clientcor/cor_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/Clientcor/cor_update/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			idProperty : 'change_order_id'
		},
		writer: {
			type: 'json',
			root : 'clientcor',
			idProperty : 'cor_id',
			writeAllFields: true,
			encode : true
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'cor_id',
        direction: 'DESC'
    }]
});