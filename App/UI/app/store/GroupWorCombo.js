Ext.define('AM.store.GroupWorCombo', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Wor',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/GroupWorCombo/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'VerticalID'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});