Ext.define('AM.store.ShiftChart', {
	extend : 'Ext.data.Store',
	storeId:'ShiftChart',
	model : 'AM.model.PieChart',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getShiftData/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'data'
        }
	}	
});