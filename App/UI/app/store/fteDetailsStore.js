Ext.define('AM.store.fteDetailsStore', {
	extend : 'Ext.data.Store',
	groupField: 'work_order_id',
    fields: [
		{'name': 'fte_id', 'type': 'int'},
		{'name': 'work_order_id', 'type': 'string'},
        {'name': 'responsibilities', 'type': 'string'},
        {'name': 'est_fte', 'type': 'string'},
        {'name': 'emea_fte', 'type': 'string'},
        {'name': 'ist_fte', 'type': 'string'},
        {'name': 'lemea_fte', 'type': 'string'},
        {'name': 'cst_fte', 'type': 'string'},
        {'name': 'pst_fte', 'type': 'string'},
        {'name': 'apac_fte', 'type': 'string'},
	],
 	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/fte_summary_list/?v='+AM.app.globals.version
		},
		reader : {
			type : 'json',
			root: 'data',
			totalProperty: 'totalCount',
			idProperty : 'fte_id'
		},
	},
	
	sortOnLoad:true,
	
});