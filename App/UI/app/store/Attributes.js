Ext.define('AM.store.Attributes', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Attributes',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/attributes/attributes_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/attributes/attributes_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/attributes/attributes_update/',
			destroy : AM.app.globals.appPath+'index.php/attributes/attributes_del/'
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'attributes_id'
		},
        writer: {
            type: 'json',
            root : 'Attributes',
            idProperty : 'attributes_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	 // sorters: [{
  //       property: 'name',
  //       direction: 'ASC'
  //   }] 
});