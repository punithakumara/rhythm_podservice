Ext.define('AM.store.LogInViewerClients', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Client',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    fields: [
        {'name': 'client_name', 'type': 'string'},  
	],
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/client_view?loginViewer=1&v='+AM.app.globals.version
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'client_id'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'client_name',
        direction: 'ASC'
    }]
});