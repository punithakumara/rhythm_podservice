Ext.define('AM.store.VerticalDocs', {
	extend : 'Ext.data.Store',
	storeId:'VerticalDocs',
	model : 'AM.model.Document',
    pageSize: AM.app.globals.itemsPerPage,
	autoLoad: true,
    remoteSort: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/documents/document_view/?Access=Vertical Specific&v='+AM.app.globals.version,
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'DocumentID'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'document.DocumentDate',
        direction: 'DESC'
    }]	
});