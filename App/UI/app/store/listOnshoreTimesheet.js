Ext.define('AM.store.listOnshoreTimesheet', {
	extend : 'Ext.data.Store', 
    model : 'AM.model.TimesheetOnshore',
	autoLoad: false,
	storeId:'timesheetId',
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		// timeout: 120000,
		api : {
			read : AM.app.globals.appPath+'index.php/timesheetOnshore/timesheet_approve/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root : 'data',
			successProperty : 'success',
			idProperty : 'timesheetID'
		},
	},
	sortOnLoad:true,
	groupField: 'EmployeeName',
	idProperty: 'timesheetID',
});