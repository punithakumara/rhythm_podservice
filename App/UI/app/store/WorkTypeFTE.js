Ext.define('AM.store.WorkTypeFTE', {
	extend : 'Ext.data.Store',
	model : 'AM.model.WorkTypeFTE',
	//storeId:'WorkTypeFTE',
	autoLoad: false,
    pageSize: 5,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/fte_list/?v='+AM.app.globals.version
			//create : AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_save/?v='+AM.app.globals.version,
			//update: AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_update/',
			//destroy : AM.app.globals.appPath+'index.php/clients/client_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
        writer: {
            type: 'json',
            root : 'rows',
            idProperty : 'wor_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'wor_id',
        direction: 'DESC'
    }]
});