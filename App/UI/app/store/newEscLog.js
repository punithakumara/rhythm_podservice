Ext.define('AM.store.newEscLog', {
	extend : 'Ext.data.Store',
	model : 'AM.model.newEscLog',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/newIncidentLog/log_viewEscalation/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/newIncidentLog/log_addEsc/?v='+AM.app.globals.version,
			//update: AM.app.globals.appPath+'index.php/newIncidentLog/log_updateError/',
			destroy : AM.app.globals.appPath+'index.php/newIncidentLog/log_delEsc/'
		},		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'LogID'
		},
        writer: {
            type: 'json',
            root : 'log',
            idProperty : 'LogID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'created_on',
        direction: 'DESC'
    }]
});