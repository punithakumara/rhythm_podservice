Ext.define('AM.store.MonthlyBillabilityFteReport', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'wor_id', type: 'int'},
        {name: 'wor_type_id', type: 'int'},
        {name: 'client_name', type: 'string'},
		{name: 'client_id', type: 'int'},
        {name: 'wor_name', type: 'string'},
        {name: 'wor_type', type: 'string'},
        {name: 'project_type', type: 'string'},
        {name: 'role_type', type: 'string'},
        {name: 'fte_total_hc', type: 'float'},
        {name: 'full_month_fte', type: 'float'},
        {name: 'prorated_fte', type: 'float'},
        {name: 'fte_total_buffer', type: 'float'},
        {name: 'fte_apac', type: 'float'},
        {name: 'fte_lemea', type: 'float'},
        {name: 'india', type: 'float'},
        {name: 'usa', type: 'float'},
		{name: 'fte_id', type: 'string'},
		{name: 'responsibility_id', type: 'int'},
	],
	autoLoad: true,
	autoSync: true,
	//pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/MonthlyBillabilityReport/MonthlyBillabilityFteReport_view/?v='+AM.app.globals.version,
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			//idProperty : 'change_order_id'
		}
	}, 
	
	groupField: 'client_name',
	idProperty: 'id',
});