Ext.define('AM.store.TaskDetailsSelf', {
	extend : 'Ext.data.Store',
	fields : ['task','subtask', 'hours'],
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/timesheet/getTaskDetailsSelf/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
});