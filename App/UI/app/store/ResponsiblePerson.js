Ext.define('AM.store.ResponsiblePerson', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'employee_id', type: 'int'},
        {name: 'emp_name', type: 'string'},
    ],
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/newIncidentLog/viewResponsiblePerson/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'data',
			totalProperty: 'totalCount',
		}
	}, 

	idProperty: 'id',
});