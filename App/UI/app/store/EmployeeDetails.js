Ext.define('AM.store.EmployeeDetails', {
	extend : 'Ext.data.Store',
	model : 'AM.model.EmployeeDetails',
	storeId:'EmployeeDetails',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/employDetails/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
	sortOnLoad:true,
	sorters: [{
	    property: 'company_employ_id',
	    direction: 'ASC'
	}]	
});