Ext.define('AM.store.AMClient', {
	extend : 'Ext.data.Store',
	fields:[{name: 'Name', sortType: 'asText', sortDir: 'ASC'},'ClientID'],
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/empAmRelCli_view/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'ClientID'
		},
        writer: {
            type: 'json',
            root : 'client',
            idProperty : 'ClientID'
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});