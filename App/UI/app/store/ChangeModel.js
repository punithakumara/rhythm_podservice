Ext.define('AM.store.ChangeModel', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'value'],
    data : [
        {'id': '1', 'value': 'Yes'},
        {'id': '2', 'value': 'No'},
    ]
});