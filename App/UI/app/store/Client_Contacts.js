Ext.define('AM.store.Client_Contacts', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Client_Contacts',
	autoLoad: true,
    // pageSize: 5,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
   
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/ClientContacts/client_contacts_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/ClientContacts/Client_Contacts_save/?v='+AM.app.globals.version,
			update : AM.app.globals.appPath+'index.php/ClientContacts/Client_Contacts_update/',
			// destroy : AM.app.globals.appPath+'index.php/clients/client_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'contact_id'
		},
        writer: {
            type: 'json',
            root : 'Client_Contacts',
            idProperty : 'contact_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'contact_name',
        direction: 'DESC'
    }]
});