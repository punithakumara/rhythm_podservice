Ext.define('AM.store.LanguagesList', {
	extend : 'Ext.data.Store',
	model: "AM.model.LanguagesList",
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/skills/getLanguages/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			// idProperty : 'attributes_id'
		},
	},
	
	sortOnLoad:true,
	 
});