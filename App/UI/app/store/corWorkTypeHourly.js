Ext.define('AM.store.corWorkTypeHourly', {
	extend : 'Ext.data.Store',
	model : 'AM.model.WorkTypeHourly',
	//storeId:'WorkTypeFTE',
	autoLoad: false,
    pageSize: 5,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientcor/hourly_list/?v='+AM.app.globals.version
			//create : AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_save/?v='+AM.app.globals.version,
			//update: AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_update/',
			//destroy : AM.app.globals.appPath+'index.php/clients/client_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
        writer: {
            type: 'json',
            root : 'rows',
            idProperty : 'cor_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'cor_id',
        direction: 'DESC'
    }]
});