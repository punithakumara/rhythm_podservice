Ext.define("AM.store.EmployeeProfile", {
    extend: "Ext.data.Store",
    model: "AM.model.EmployeeProfile",
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: false,
    sortOnLoad: true,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/EmployeeProfile/workexperience_list/?v="+AM.app.globals.version,
            create: AM.app.globals.appPath + "index.php/EmployeeProfile/experience_add/?v="+AM.app.globals.version,
            update: AM.app.globals.appPath + "index.php/EmployeeProfile/experience_add/?v="+AM.app.globals.version,
        },
        reader: {
            type: "json",
            root: "data",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "id"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "id",
            writeAllFields: true,
            encode: true
        }
    },
    sortOnLoad: true,
    sorters: [ {
        property: "first_name",
        direction: "ASC"
    } ]
});