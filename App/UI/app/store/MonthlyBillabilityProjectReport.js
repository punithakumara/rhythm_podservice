Ext.define('AM.store.MonthlyBillabilityProjectReport', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'wor_id', type: 'int'},
        {name: 'wor_type_id', type: 'int'},
        {name: 'client_name', type: 'string'},
        {name: 'wor_name', type: 'string'},
        {name: 'wor_type', type: 'string'},
        {name: 'project_type', type: 'string'},
        {name: 'project_name', type: 'string'},
        {name: 'role_type', type: 'string'},
        {name: 'start_duration', type: 'string'},
        {name: 'end_duration', type: 'string'},
        {name: 'hours_entered', type: 'int'},
	],
	autoLoad: true,
	autoSync: true,
	//pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/MonthlyBillabilityReport/MonthlyBillabilityProjectReport_view/?v='+AM.app.globals.version,
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			//idProperty : 'change_order_id'
		}
	}, 
	
	groupField: 'client_name',
	idProperty: 'id',
});