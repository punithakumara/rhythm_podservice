Ext.define('AM.store.ParentPod', {
	extend : 'Ext.data.Store',
	fields: ['pod_name', 'groupBy', 'Id','pPodName'],
	groupField: 'groupBy',
	autoLoad: true,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/pod/getParentPod?v='+AM.app.globals.version,//parentVertical/',
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'Id'
		},
		writer: {
			type: 'json',
			root : 'rows',
			idProperty : 'Id',
			writeAllFields: true,
			encode : true
		}
	},
	sortOnLoad:true,

	sorters: [{
		property: 'pPodName',
		direction: 'ASC'
	}]
});