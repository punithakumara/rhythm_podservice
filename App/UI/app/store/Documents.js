Ext.define('AM.store.Documents', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Document',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/documents/document_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/documents/documents_save/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/documents/documents_save/',
			destroy : AM.app.globals.appPath+'index.php/documents/document_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'DocumentID'
		},
        writer: {
            type: 'json',
            root : 'document',
            idProperty : 'DocumentID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'UploadName',
        direction: 'ASC'
    }]
});