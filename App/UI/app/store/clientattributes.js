Ext.define('AM.store.clientattributes', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
    fields: [
        {'name': 'attributes_id', 'type': 'int'},
        {'name': 'mandatory', 'type': 'int'},
        {'name': 'field_type', 'type': 'string'},
        {'name': 'label_name', 'type': 'string'},
        {'name': 'field_name', 'type': 'string'},
        {'name': 'values', 'type': 'string'},
	],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/ClientAttributes/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
	},
	
	sortOnLoad:true,

});
