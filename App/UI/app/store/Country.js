Ext.define('AM.store.Country', {
	extend : 'Ext.data.Store',
	// model : 'AM.model.Employee',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    fields:['country_id','country_name'],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/countries_list/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'CountryID'
		},
		 writer: {
            type: 'json',
            root : 'country',
            idProperty : 'CountryID',
            'allowSingle' : false,
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'CountryName',
        direction: 'ASC'
    }]
});