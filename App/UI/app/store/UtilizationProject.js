Ext.define('AM.store.UtilizationProject', {
	extend : 'Ext.data.Store',
	model : 'AM.model.ProjectTypes',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    fields: [
        {'name': 'project_name', 'type': 'string'},  
	],
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Utilization/project_types/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});