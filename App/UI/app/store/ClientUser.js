Ext.define('AM.store.ClientUser', {
	extend : 'Ext.data.Store',
	model : 'AM.model.ClientUserModel',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/clientuser_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/clients/clientuserSave/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/clients/clientuserUpdate/'
			
		},
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'UserID'
		},
        writer: {
            type: 'json',
            root : 'client',
            idProperty : 'UserID',
            writeAllFields: true,
            encode : true
        }
	},
	sortOnLoad:true,
	sorters: [{
			property: 'FullName',
			direction: 'ASC'
    }]
});