Ext.define('AM.store.CompanyDocs', {
	extend : 'Ext.data.Store',
	storeId:'CompanyDocs',
	model : 'AM.model.Document',
    pageSize: AM.app.globals.itemsPerPage,
	autoLoad: true,
    remoteSort: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/documents/document_view/?Access=All&v='+AM.app.globals.version,
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'DocumentID'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'document.DocumentDate',
        direction: 'DESC'
    }]
});