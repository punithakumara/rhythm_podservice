Ext.define("AM.store.UtilEmployees", {
    extend: "Ext.data.Store",
    model: "AM.model.UtilEmployees",
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/employees/util_employees_details/?v="+AM.app.globals.version,
        },
        reader: {
            type: "json",
            root: "rows",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "employee_id"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "employee_id",
            writeAllFields: true,
            encode: true
        }
    }
});