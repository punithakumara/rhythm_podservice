Ext.define('AM.store.timesheetServiceTypes', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	fields: [
        {'name': 'service_type', 'type': 'string'},  
	],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/timesheetOnshore/servicetype_list/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
		}
	},
	sortOnLoad:true,
});
