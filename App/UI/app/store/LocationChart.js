Ext.define('AM.store.LocationChart', {
	extend : 'Ext.data.Store',
	storeId:'LocationChart',
	model : 'AM.model.PieChart',
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getLocationData/?v='+AM.app.globals.version,
		reader :  {
            type: 'json'
        }
	}	
});