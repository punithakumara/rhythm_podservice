Ext.define('AM.store.newRootCause', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    autoSync: false,
    fields : ['RootCauseID', 'Description'],  
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/newIncidentLog/rootcause/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});