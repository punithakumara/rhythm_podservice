Ext.define('AM.store.summaryStore', {
	extend : 'Ext.data.Store',
	groupField: 'model',
    fields: [
		{'name': 'projectId', 'type': 'int'},
		{'name': 'process_id', 'type': 'int'},
		{'name': 'client_id', 'type': 'int'},
        {'name': 'model', 'type': 'string'},
        {'name': 'support', 'type': 'string'},
        {'name': 'support_coverage', 'type': 'string'},
        {'name': 'project_type', 'type': 'string'},
        {'name': 'responsibilities', 'type': 'string'},
        {'name': 'fte', 'type': 'float'},
        {'name': 'min', 'type': 'string'},
        {'name': 'max', 'type': 'string'},
        {'name': 'shift', 'type': 'string'},
        {'name': 'due', 'type': 'date'}
	],
 	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/summary_list/?v='+AM.app.globals.version
			
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			idProperty : 'change_order_id'
		},
		writer: {
			type: 'json',
			root : 'clientcor',
			idProperty : 'cor_id',
			writeAllFields: true,
			encode : true
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'cor_id',
        direction: 'DESC'
    }]
});