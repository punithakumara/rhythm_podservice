Ext.define('AM.store.Barchart', {
	extend : 'Ext.data.Store',
	storeId:'Barchart',
	fields: ['Name', 'Value1', 'Value2'],
	autoLoad: true,
	proxy: {
        type: 'rest',
		url : AM.app.globals.appPath+'index.php/charts/getBillableDetails/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'data'
        }
	}
});