Ext.define('AM.store.Skills', {
	extend : 'Ext.data.Store',
	model: "AM.model.Skills",
	autoLoad: true,
	pageSize: AM.app.globals.itemsPerPage,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/skills/getSkillsets/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/skills/skill_add_update/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			// idProperty : 'attributes_id'
		},
	},
	
	sortOnLoad:true,
	
});