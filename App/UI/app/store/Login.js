Ext.define('AM.store.Login',{
	extend : 'Ext.data.Store',
	model : 'AM.model.Login',
	autoLoad : false,
	autoSync : true,
	
	proxy : {
		type : 'rest',
		api : {
			read :  AM.app.globals.appPath+'index.php/authenticate/authentication_details?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/authenticate/authentication_details?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			successProperty : 'success'
		},
        writer: {
            type: 'json',
            root : 'login'
        }
	}
});