Ext.define('AM.store.Hierarchy', {
	extend : 'Ext.data.TreeStore',
	model: 'AM.model.Hierarchy',
	autoLoad: true,
    proxy: {
        type: 'rest',
        url: AM.app.globals.appPath+'index.php/charts/getTreeStructuresEmployeeData/',
		reader : {
			type : 'json',
			root: 'data'
		},
		
		// Don't want proxy to include these params in request
        pageParam: undefined,
        startParam: undefined
    }
	
});