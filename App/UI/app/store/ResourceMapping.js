Ext.define('AM.store.ResourceMapping', {
	extend : 'Ext.data.Store',
	fields: ['EmployeeID','EmployeeName','ClientName','Pod','Service','Billable','BillableType','location'],
	autoLoad: true,
	remoteSort: true,
	remoteFilter: true,
	autoSync: false,
	pageSize: AM.app.globals.itemsPerPage,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/reports/ResourceMapping?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'company_employ_id'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'EmployeeName',
        direction: 'ASC'
    }]
	
});