Ext.define('AM.store.CorChart', {
	extend : 'Ext.data.Store',
	storeId:'CorChart',
	fields: ['Name', 'Value1', 'Value2','Value3'],
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getCorDetails/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
			root: 'data'
        }
	}	
});