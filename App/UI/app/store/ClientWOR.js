Ext.define('AM.store.ClientWOR', {
	extend : 'Ext.data.Store', 
    fields: ['wor_id','work_order_id','wor_name','wor_type', 'wor_type_id', 'service_id', 'client', 'start_date', 'end_date', 'count_history','status', 'previous_work_order_id', 'created_by', 'updated_by', 'client_partner', 'engagement_manager', 'delivery_manager', 'client_name', 'name', 'description', 'created_on', 'created_name', 'updated_name', 'cp_name', 'em_name', 'dm_name', 'sh_name','tl_name','am_name','ta_name', 'corids','stake_holder','time_entry_approver','Engagementmanagers','resourceCount','team_lead', 'associate_manager'],
    autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	remoteSort: true,
    // autoSync: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/list/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/Clientwor/wor_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/Clientwor/wor_add/?v='+AM.app.globals.version,
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			idProperty : 'work_order_id'
		},
		
		 writer: {
	            type: 'json',
	            root : 'clientwor',
	            idProperty : 'wor_id',
	            writeAllFields: true,
	            encode : true
	        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'wor_id',
        direction: 'DESC'
    }]
});