Ext.define('AM.store.EmployeeSkills', {
	extend : 'Ext.data.Store',
	model : 'AM.model.EmployeeSkills',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/skills/getEmployees/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'employee_id'
		},
	},
	
	sortOnLoad:true,
	    sorters: [ {
        property: "emp_name",
        direction: "ASC"
    } ] 
});