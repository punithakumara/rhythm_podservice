Ext.define('AM.store.timesheetSubTask', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	fields: [
        {'name': 'sub_task_name', 'type': 'string'},  
	],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/timesheet/sub_task_list/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
		}
	},
	sortOnLoad:true,
});
