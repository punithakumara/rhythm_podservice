Ext.define('AM.store.AllocatedRes', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
   fields: ['DesignationID','designationName','podName','serviceName','grades','employee_id','Email', 'supervisor','company_employ_id', 'id','vertName', 'billable_type','first_name','last_name','Email','start_date','end_date','Mobile',{name:'modifiedRec',type: 'bool'},'Aim',
				{name: 'FullName', 
					convert: function(v, rec) { 
							var fullName = rec.get('first_name');
							if(!rec.get('last_name'))
							{
								fullName = fullName;
							}
							else if(rec.get('last_name')!='NULL' && rec.get('last_name')!="" && rec.get('last_name')!='null')
								fullName += " "+rec.get('last_name');
							return fullName;
						 } 
			  }],

			proxy: {
				type: 'rest',
				api : {
					read : AM.app.globals.appPath+"index.php/Clientwor/allocated_res/?v="+AM.app.globals.version,
				},				
				reader :  {
					type: 'json',
					root : 'data',
					successProperty : 'success',
					totalProperty: 'totalCount'
				}
			},
			sortOnLoad:true,
			sorters: [ {
				property: "grades",
				direction: "DESC"
			} ]

});
