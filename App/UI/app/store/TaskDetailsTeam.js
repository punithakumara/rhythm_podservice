Ext.define('AM.store.TaskDetailsTeam', {
	extend : 'Ext.data.Store',
	fields : ['task','subtask', 'hours'],
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/timesheet/getTaskDetailsTeam/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
});