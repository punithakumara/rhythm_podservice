Ext.define('AM.store.Designations', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Designation',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/designation/designations_list/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/designation/designations_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/designation/designations_update/'
		},
		
		reader : {
			type : 'json',
			root: 'designation',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'designation_id'
		},
		 writer: {
            type: 'json',
            root : 'data',
            idProperty : 'designation_id',
            writeAllFields: true,
            encode : true
        }
	},
	sortOnLoad:true,
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});