Ext.define('AM.store.NrrRdrChart', {
	extend : 'Ext.data.Store',
	storeId:'NrrRdrChart',
	fields: ['Name', 'Value1', 'Value2','Value3'],
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getNrrRdrDetails/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
			root: 'data'
        }
	}	
});