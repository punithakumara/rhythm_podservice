Ext.define('AM.store.Month', {
	extend : 'Ext.data.Store', 
    fields: ['month_id','month_name'],
	autoLoad: true,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/EmployeeProfile/month_list/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});