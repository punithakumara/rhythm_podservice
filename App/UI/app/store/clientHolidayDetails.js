Ext.define('AM.store.clientHolidayDetails', {
	extend: 'Ext.data.Store',
	groupField: 'project_code',
    fields: [
		{name: 'id', type: 'int'},
        {name: 'project_code', type: 'string'},
        {name: 'holiday', type: 'string'},
        {name: 'date', type: 'string'},
    ],
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
	remoteFilter: true,
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientwor/client_holidays/?v='+AM.app.globals.version
		},
		reader : {
			type : 'json',
			root: 'rows',
			totalProperty: 'totalCount',
			idProperty : 'id'
		}
	},
	
	sortOnLoad:true,
});