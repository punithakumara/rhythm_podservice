Ext.define('AM.store.VerticalHierarchy', {
	extend: 'Ext.data.TreeStore',
	fields	: ['VerticalID','ParentVerticalID','VertID', 'id', {name: 'Name', type: 'string'},'NonBillable','Leadership', {name: 'Billable', type: 'float'},{name: 'qtip', mapping: 'tool_tip'}],
	proxy	: {
		type	: 'rest',
		url		: AM.app.globals.appPath+'index.php/vertical/getVerticalTreeStructureData/?v='+AM.app.globals.version
	},
	folderSort	: true,
	sorters		: [ {
		property	: 'text',
		direction	: 'ASC'
	} ]
});