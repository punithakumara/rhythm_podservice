Ext.define('AM.store.ActiveClientProcess', {
	extend : 'Ext.data.Store',
	model : 'AM.model.ActiveClientProces',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    groupers: [{
        property : 'ParentVertical',
        direction: 'ASC'
    }],
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/clients/clientprocess_view/?v='+AM.app.globals.version,
		
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'ProcessID'
		}
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'service_Name',
        direction: 'ASC'
    }]
});