Ext.define('AM.store.EmployeeAchievement', {
	extend : 'Ext.data.Store',
	model: "AM.model.EmployeeAchievement",
/*	fields: [
        {'name': 'certification_id', 'type': 'int'},
        {'name': 'certification', 'type': 'string'}
	],*/
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			//read : AM.app.globals.appPath+'index.php/skills/getCertifications/?v='+AM.app.globals.version,
			read: AM.app.globals.appPath + "index.php/EmployeeAchievement/achievement_list/?v="+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/EmployeeAchievement/achievement_add/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/EmployeeAchievement/achievement_edit/?v='+AM.app.globals.version,
			destroy : AM.app.globals.appPath+'index.php/EmployeeAchievement/achievement_del/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'id'
		},
        writer: {
            type: 'json',
            root : 'achievement',
            idProperty : 'id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	 
});