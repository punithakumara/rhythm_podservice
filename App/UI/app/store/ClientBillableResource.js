Ext.define('AM.store.ClientBillableResource', {
	extend : 'Ext.data.Store',
	fields : ['client_name',
		{name: 'model', 
		convert: function(v, rec){
			var ForcastWOR = (rec.get('ForcastWOR') != null) ? '<span title="Forcast WOR">(+'+rec.get('ForcastWOR')+')</span>' : '';
			var ForcastCOR = (rec.get('ForcastCOR') != null) ? '<span title="Forcast COR">(-'+rec.get('ForcastCOR')+')</span>' : '';
			var ResourceRequested = v +' '+ ForcastWOR + ForcastCOR;
			return ResourceRequested;
		}},
		'AssignedResource', 'Leadership', 'Billable', 'NonBillable', 'ForcastWOR', 'ForcastCOR'],
	storeId:'ClientBillableResource',
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getClientBillableResource/?v='+AM.app.globals.version,
		reader :  {
            type: 'json',
            root : 'rows'
        }
	},
	sorters: [{
		property: 'client_name',
        direction: 'ASC'
	}]
});