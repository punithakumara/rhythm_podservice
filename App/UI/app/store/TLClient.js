Ext.define('AM.store.TLClient', {
	extend : 'Ext.data.Store',
	fields:['Name','client_id'],
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/empcategorize/empTLRelCli_view/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'data',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'ClientID'
		},
        writer: {
            type: 'json',
            root : 'client',
            idProperty : 'ClientID'
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});