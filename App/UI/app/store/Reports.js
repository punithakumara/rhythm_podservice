Ext.define('AM.store.Reports', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Reports',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Reports/view/?v='+AM.app.globals.version
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
		
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Year',
        direction: 'DESC'
    }]
});