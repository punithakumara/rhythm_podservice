Ext.define('AM.store.ClientContactVertical', {
	extend		: 'Ext.data.Store',
	autoLoad	: true,
	fields		: ['service_id', 'ServiceName'],
    pageSize	: AM.app.globals.itemsPerPage,
    remoteSort	: true,
    remoteFilter: true,
    autoSync	: false,
	proxy		: {
		type	: 'rest',
		api		: {
			read	: AM.app.globals.appPath+'index.php/services/getClientContactServices/?v='+AM.app.globals.version,
		},
		
		reader	: {
			type			: 'json',
			root			: 'rows',
			successProperty : 'success',
			totalProperty	: 'totalCount',
			idProperty		: 'ServiceID'
		},
	},
	
	sortOnLoad	: true,
	sorters		: [ {
        property	: 'Name',
        direction	: 'ASC'
    } ]
} );