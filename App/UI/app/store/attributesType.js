Ext.define('AM.store.attributesType', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"text", "name":"Text"},
        {"id":"integer", "name":"Integer"},
        {"id":"list", "name":"List"},
        {"id":"externaltime", "name":"ExternalTime"},
        {"id":"internaltime", "name":"InternalTime"}
    ]
});