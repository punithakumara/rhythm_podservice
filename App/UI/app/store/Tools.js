Ext.define('AM.store.Tools', {
	extend : 'Ext.data.Store',
	model: "AM.model.Tools",
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/skills/getTools/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/skills/tool_add_update/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			// idProperty : 'attributes_id'
		},
	},
	
	sortOnLoad:true,
	 
});