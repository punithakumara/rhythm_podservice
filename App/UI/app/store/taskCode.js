Ext.define('AM.store.taskCode', {
	extend : 'Ext.data.Store',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: true,
	fields: [
        {'name': 'task_id', 'type': 'int'},
        {'name': 'task_category', 'type': 'string'},
        {'name': 'task_name', 'type': 'string'},
        {'name': 'request_type', 'type': 'string'},
        {'name': 'complexity', 'type': 'string'},
        {'name': 'priority', 'type': 'string'},
        {'name': 'task_code', 'type': 'string'},
        {'name': 'project_type_id_test', 'type': 'int'}   
	],
	proxy: {
		type: 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/utilization/task_code_list/?v='+AM.app.globals.version,
		},				
		reader :  {
			type: 'json',
			root : 'data',
			successProperty : 'success',
			totalProperty: 'totalCount'
		}
	},
	sortOnLoad:true,
});
