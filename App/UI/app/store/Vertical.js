Ext.define('AM.store.Vertical', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Vertical',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Vertical/vertical_view/',
			create : AM.app.globals.appPath+'index.php/Vertical/vertical_add/',
			update: AM.app.globals.appPath+'index.php/Vertical/vertical_update/',
			destroy : AM.app.globals.appPath+'index.php/Vertical/vertical_del/'
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'vertical_id'
		},
		
        writer: {
            type: 'json',
            root : 'vertical',
            idProperty : 'vertical_id',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'Name',
        direction: 'ASC'
    }]
});