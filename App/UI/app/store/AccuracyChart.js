Ext.define('AM.store.AccuracyChart', {
	extend : 'Ext.data.Store',
	fields: ['Name', 'Value1'],
	autoLoad: true,
	proxy : {
		type : 'rest',
		method: 'POST',
		url : AM.app.globals.appPath+'index.php/charts/getAccuracyData/?v='+AM.app.globals.version,
		reader :  {
            type: 'json'
        }
	}	
});