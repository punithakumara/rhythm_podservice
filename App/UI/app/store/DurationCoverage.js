Ext.define('AM.store.DurationCoverage', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data: [
		{'id': '0', 'name': 'Monthly'},
		{'id': '1', 'name': 'Per Month'},
	]
});