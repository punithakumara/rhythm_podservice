Ext.define('AM.view.Client_Contacts.List', {
	extend			: 'Ext.grid.Panel',
	alias			: 'widget.Client_ContactsList',
	title			: 'Client Contacts List',
	store			: 'Client_Contacts',
	id				: 'ClientContactsList',
	border			: true,
	minHeight		: 250,
	height : 200,
	initComponent	: function() {
		var me = this;
		var processID = "";
		this.editing	= Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor	: 1,
			saveBtnText			: 'Save',
			listeners			: {
				'beforeedit' :  function(editor, e) 
				{
					Ext.getCmp('ClientSaveBtn').setDisabled(true);
					Ext.getCmp('ClientCancelBtn').setDisabled(true);
				},
				
				'validateedit'	: function(editor,e) {
					
					var Validity	= true;
					var validate 	=  AM.app.getController('Client_Contacts').validateedit(editor,e);
					
					if(validate == false) {
						Validity = false;
					} else if(validate == true) { 
						Validity = true;
					}else if(validate == 2) {
						Validity = 'dup';
					}
					
					if(Validity == true) 
					{		
						Ext.ComponentQuery.query('#when_add_contact')[0].setValue('');
						Ext.getCmp('ClientSaveBtn').setDisabled(false);
						Ext.getCmp('ClientCancelBtn').setDisabled(false);
						//Ext.getCmp('gridTrigger1').setDisabled(true);
						return true;
					} 
					else if(Validity == 'dup')
					{
						Ext.MessageBox.show({
							title:'Alert',
							msg:"Duplicated Line Records.",
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.WARNING,
						});
						
						return false;
					}
					else if(Validity == false)
					{
						Ext.MessageBox.show({
							title:'Alert',
							msg:"Error in  Line Records.",
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.WARNING,
						});
						
						return false;
					}
				},
				canceledit : function(grid,obj) {
					if (typeof obj.store.data.items[0] !== 'undefined') 
					{
						if(obj.store.data.items[0].data['contact_id'] == "") 
						{
							if(obj.rowIdx == "0")
							{
								me.getStore().removeAt(obj.rowIdx);
							}
						}
					}
					Ext.ComponentQuery.query('#when_add_contact')[0].setValue('');
					Ext.getCmp('ClientSaveBtn').setDisabled(false);
					Ext.getCmp('ClientCancelBtn').setDisabled(false);
				}
			}
		});
		
		Ext.apply(me, { plugins: [me.editing] } );

		this.editing.on('edit', function(editor, e, eOpts) {						
			var combineArray	= [];
			var info			= e.grid.store.data.items;
			Ext.each(info, function(value, i) {
			 	var dataArray	= JSON.stringify(value.data);
				combineArray.push(dataArray);
			});
			var arrayToString	= combineArray.toString();
			Ext.ComponentQuery.query('#when_add_contact')[0].setValue(arrayToString);
		});

		this.columns	= [ {
    		header		: 'Name <span style="color:red">*</span>', 
			dataIndex	: 'contact_name', 
			menuDisabled:true,
			draggable: false,
			width		: 150,
			editor		: {
				xtype	:'textfield',
				itemId	:'cont_name',
				id      :'cont_na',
				maskRe: /[A-Za-z ]/,	
				// allowBlank : false,
			},
			renderer	: this.renderTip
		}, {
			header		: 'Designation <span style="color:red">*</span>', 
			dataIndex	: 'contact_title', 
			menuDisabled:true,
			draggable: false,
			width		: 220,
			editor		: {
				xtype	:'textfield',
				itemId	:'cont_desi',
				maskRe: /[A-Za-z ]/,
			},
			renderer	: this.renderTip
		},{
			header		: 'Role <span style="color:red">*</span>',
			menuDisabled:true,
			dataIndex	: 'role',
			draggable: false,
			width		: 200,
			editor		: {
				xtype		: 'combo',
				maskRe: /[A-Za-z ]/,
				minChars	: 1,
				multiSelect	: false,
				store		: Ext.create('AM.store.ClientContact_Role'),
				itemId		: 'client_cont_role',
				queryMode	: 'local',
				displayField: 'RoleDescription', 
				valueField	: 'RoleId',
				emptyText	: 'Select Role',
				forceSelection: true,
			},
			renderer:function(value,metaData,record){			
				switch (value) {
			        case '1':
			            return "Day to Day in charge";
			            break;
			        case '2':
			            return "Day to Day in charge (UK)";
			            break;
			        case '3':
			            return "Day to Day in charge (US)";
			            break;
			        case '4':
			            return "Main POC";
			            break;
			        case '5':
			            return "Billing Contact";
			            break;
			        default:
			            //return "Main POC";
			            break;                           
			    }
			}		
		},{
			header		: 'Email <span style="color:red">*</span>', 
			dataIndex	: 'contact_email', 
			menuDisabled:true,
			draggable: false,
			width		: 300,
			renderer	: this.renderTip,
			editor		: {
				xtype	: 'textfield',
				itemId	: 'cont_email',	
				vtype	: 'email'	
			}
		}, {
			header		: 'Service <span style="color:red">*</span>',
			dataIndex	: 'ServiceName',
			menuDisabled:true,
			draggable: false,
			width		: 200,
			editor		: {
				xtype			: 'combo',
				minChars		: 1,
				itemId			: 'client_cont_serv',
				multiSelect		: false,
				name			: 'ServiceName',
				store			: 'ClientContactVertical',
				emptyText		: 'Select Service',
				forceSelection  : true,
				mode			: 'remote',
				multiSelect		: false,
				displayField	: 'ServiceName', 
				valueField		: 'ServiceName',
				maskRe          : /[A-Za-z]/,
				listeners		: {
			 		beforequery	: function(queryEvent) {
			 			AM.app.getStore('ClientContactVertical').getProxy().extraParams = {
				 			'hasNoLimit'	:'1', 
				 			'filterName' 	: queryEvent.combo.displayField,
				 		};
				 	},
				},
			},
			renderer		: function(val) {
				var rec		= Ext.getStore('ClientContactVertical').findRecord('service_id', val);
				return rec == null ? val : rec.get('ServiceName');
			}
		},{
			header 		: 'Mobile',
			dataIndex	: 'contact_mobile',
			draggable: false,
			menuDisabled:true,
			width		: 150,
			renderer	: this.renderTip,
			editor		: {
				xtype			: 'textfield',
				allowNegative	: false,
		        minValue		: 0,
		        minLength       : 9,
		        maxLength		: 15,
				maskRe			: /[0-9- ()]/,
		    }
		}, {
			header		: 'Phone',
			width		: 150,
			dataIndex	: 'contact_phone',
			draggable: false,
			menuDisabled:true,
			renderer	: this.renderTip,
			editor		: {
				xtype			:'textfield',
		        allowNegative	: false,
		        minValue		: 0,
		        minLength       : 9,
		    	maxLength		: 15,
				maskRe			: /[0-9- ()]/,
		    }
		}, {
			header	: 'Action',
			width	: 70,
			xtype	: 'actioncolumn',
			sortable: false,
			draggable: false,
			menuDisabled:true,
			itemId	: 'clientaction',
			items	: [ {
				icon	: AM.app.globals.uiPath+'resources/images/delete.png',
				tooltip : 'Delete',
				handler : this.Remove_Contact
			} ],
			renderer	: this.renderTip
		}];
		
		me.tbar	= [ {
			xtype	: 'tbfill'
		}, {
			xtype			: 'hidden',
			itemId			: 'gridTrigger1',
			id				:'gridTrigger1',
			fieldLabel		: '',
			labelWidth 		: 60,
			labelCls 		: 'searchLabel',
			triggerCls 		: 'x-form-clear-trigger',
			emptyText 		: 'Search',
			width 			: 250,
			minChars 		: 1,
			enableKeyEvents	: true,
			onTriggerClick	: function() {
				this.reset();
				this.fireEvent('triggerClear');
			},
			listeners	: {
				keyup	: function() {
					AM.app.getController('Client_Contacts').onTriggerKeyUp(this);
				}
			}
			
		},/* { 
			xtype	: 'tbseparator',
			style	: {
				'margin-right' : '10px' 
			}
		},*/ {
			xtype	: 'button',
			text	: 'Add Contact',
			icon	: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function(obj) {
				if(Ext.ComponentQuery.query('#when_add_contact')[0].getValue() == '' || me.store.totalCount >=0) 
				{
					me.getStore().insert(0, me.getStore());
					me.editing.cancelEdit();
					me.editing.startEdit(0, 0);
					Ext.ComponentQuery.query('#when_add_contact')[0].setValue(1);
				} 
				else 
				{
					Ext.MessageBox.alert('Alert', 'Fill the Data');
				}
			}
		}
		],

		this.callParent(arguments);
	},
	
	Remove_Contact	: function(grid, rowIndex, colIndex) {
		rowId = rowIndex;
		colId = colIndex;
    	Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete contact?', 
			function(response) {
				if (response == 'yes') 
				{
					AM.app.getController('Clients').onRemove_Contact(grid, rowId, colId);
				}
	    	}		
    	);
	}, 
	
	renderTip	: function(val, meta, rec, rowIndex, colIndex, store) {
		meta.tdAttr = 'data-qtip="Double click to edit"';
		return val;
	},
	
	clientContactVertCombo	: function() {
		AM.app.getController('Clients').filterProcessCombo();
	}		
});