Ext.define('AM.view.Skills.TechList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.TechList',
	id : 'techListGridID',
    store : 'Technologies',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},    
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	border : true,
	frame: true,
    width: "100%",
    height : 480,
    style: {
        border: "0px solid #157fcc",
        borderRadius:'0px'
    }, 
    initComponent : function() {
		this.columns = [{
			header : 'Name', 
			width    : '39%',
			dataIndex : 'skill_name',
			fixed: true,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Description', 
			dataIndex : 'skill_description',
			fixed: true,
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 1
		}, /*{
			header : 'Vertical', 
			dataIndex : 'vName', 
			fixed: true,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, */{
			header : 'Action', 
			width : '5%',
			menuDisabled:true,
			xtype : 'actioncolumn',
			fixed: true,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit
			}]
		}];

		this.tbar = [{
			xtype : 'trigger',
			itemId : 'searchToolsId',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Add Technology ',
			handler : function() {
				AM.app.getController('Skills').onCreateTechnology();
			}
		}];

		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Technologies to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
	
	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Skills').onEditTechnology(grid, rowIndex, colIndex);
	}
});