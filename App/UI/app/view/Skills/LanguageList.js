Ext.define('AM.view.Skills.LanguageList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.LanguageList',
    store : 'Language',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},    
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	border : true,
	frame: true,
    width: "100%",
    height : 480,
    style: {
        border: "0px solid #157fcc",
        borderRadius:'0px'
    }, 
    initComponent : function() {
		this.columns = [{
			header : 'Language', 
			width    : '39%',
			dataIndex : 'language',
			fixed: true,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Action', 
			width : '5%',
			menuDisabled:true,
			xtype : 'actioncolumn',
			fixed: true,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit
			}]
		}];

		this.tbar = [{
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Add Language ',
			handler : function() {
				AM.app.getController('Language').onCreate();
			}
		}];

		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Languages to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
	
	onEdit : function(grid, rowIndex, colIndex) {
		//AM.app.getController('Language').onEditLanguage(grid, rowIndex, colIndex);
	}
});