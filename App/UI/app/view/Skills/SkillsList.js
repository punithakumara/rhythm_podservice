Ext.define('AM.view.Skills.SkillsList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.SkillsList',
	id : 'skillsListGridID',
	store: 'Skills',
	layout : 'fit',
	viewConfig: { 
		forceFit : true,
	},
	width : '100%',
	height : 480,
	loadMask: true, 
	border : true,
	style: {
		border: "0px solid #157fcc",
		borderRadius:'0px'
	}, 
	initComponent : function() {
		this.columns = [{
			header : 'Name', 
			width    : '30%',
			dataIndex : 'skill_name',
			fixed: true,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Description', 
			dataIndex : 'skill_description',
			fixed: true,
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 1
		}, {
			header : 'Action', 
			width : '5%',
			menuDisabled:true,
			xtype : 'actioncolumn',
			fixed: true,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit
			}]
		}];

		this.tbar = [{
			xtype : 'trigger',
			itemId : 'searchSkillsId',
			id : 'searchSkillsId',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Add Skills ',
			handler : function() {
				AM.app.getController('Skills').onCreateSkills();
			}
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
			store: this.store,
			displayInfo: true,
			pageSize: AM.app.globals.itemsPerPage,
			displayMsg: 'Displaying {0} - {1} of {2}',
			emptyMsg: "No Skills to display",
			plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
		});
		
		this.callParent(arguments);
	},

	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Skills').onEditSkills(grid, rowIndex, colIndex);
	}

});