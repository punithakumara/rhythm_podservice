Ext.define('AM.view.Skills.EmpList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.EmpList',
	title: 'Employees',
	store: 'EmployeeSkills',
    layout : 'fit',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,

    initComponent : function() {

    	this.columns = [{            
            header: 'Name', 
            dataIndex: 'emp_name',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 0.6
        },{            
            header: 'Designation', 
            dataIndex: 'emp_designation',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 0.6
        },{            
            header: 'Vertical', 
            dataIndex: 'emp_vertical',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 0.5
        },{            
            header: 'Skills/Tools/Technologies', 
            dataIndex: 'emp_skills',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 1
        },/*{            
            header: 'Tools', 
            dataIndex: 'emp_tools',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 0.7
        },{            
            header: 'Technologies', 
            dataIndex: 'emp_technologies',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 0.7
        },*/{            
            header: 'Shift', 
            dataIndex: 'emp_shift',
            menuDisabled:true,
            groupable: false,
            draggable: false, 
            flex: 0.3
        },{
            header : 'Action', 
            xtype : 'actioncolumn',
            sortable: false,
            menuDisabled:true,
            groupable: false,
            draggable: false,
            flex: 0.2,
            items : [{
                icon : AM.app.globals.uiPath+'resources/images/view.png',
                tooltip : 'View Employee Skills',
                handler : this.onView
            }]
        }];

        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Employees to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
            listeners: {
                change: function (page, currentPage) { 
                    // alert(Ext.getCmp('utilstatus').getValue());
                    delete this.store.getProxy().extraParams;
                    this.store.getProxy().extraParams = {
                        'filter' : '[{"property":"first_name,last_name","value":"'+Ext.getCmp('gridTrigger').getValue()+'"}]',
                        'skills': Ext.getCmp('skillset').getValue(),
                        'tools': Ext.getCmp('tools').getValue(),
                        'technologies': Ext.getCmp('technologies').getValue(),
                        'shift': Ext.getCmp('shift').getValue(),
                        'designation': Ext.getCmp('designation').getValue(),
                        'experience': Ext.getCmp('experience').getValue(),
                        // 'passport': Ext.getCmp('passport').getValue(),
                        // 'visa': Ext.getCmp('visa').getValue(),
                        // 'stepup': Ext.getCmp('stepup').getValue(),
                        'languages': Ext.getCmp('languages').getValue(),
                        'certifications': Ext.getCmp('certifications').getValue(),
                    }
                },                      
            }
        });

        this.callParent(arguments);
    },

    tools : [{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        fieldLabel : '',
        id : 'gridTrigger',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search Employee',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, {
        xtype:'button',
        id: "DownloadLink",
        anchor: '5%',
        text:'Export To Excel',
        icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
        style:{
            float:'left',
            'margin-left':'10px'
        },
        handler : function() {
            AM.app.getController('Skills').downloadEmployeeSkillsCsv(Ext.getCmp('gridTrigger').getValue());
        }
    }],

    onView : function(grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex);

        AM.app.getController("Employees").onViewReadOnlyProfile(rec.data.id, rec.data.emp_name);
    },
});