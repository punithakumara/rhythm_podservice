Ext.define('AM.view.Skills.List',{
	extend : 'Ext.form.Panel',
	width : '99%',
	layout: 'fit',
	height: 550,
	
	initComponent : function() {

		this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items: [{
				region:'west',
				xtype: 'skillsFilter',
				width: '20%',
				frame: true,
				height: 80,
				margin : '0 0 10px 0'
			}, {
				region:'center',
				xtype: 'EmpList',
				width: '79%',
				frame: true,
				height: 500,
				margin : '0 0 10px 5px'
			}]
		}];
		
		this.callParent(arguments);
	},
});