Ext.define('AM.view.Skills.Configuration',{
	extend : 'Ext.form.Panel',
	width : '99%',
	layout: 'fit',
	height: 535,
	
    initComponent : function() {

    	this.items = [{
			xtype: 'tabpanel',
			activeTab: 0,
			width: '79%',
			height: 480,
			margin : '0 0 10px 5px',
			frame : true,
			items:[{
				title: 'Skills',
				items : [{								
					xtype:AM.app.getView('Skills.SkillsList').create(),
					hidden:false
				}]
			}, {
				title: 'Tools',
				items : [{								
					xtype:AM.app.getView('Skills.ToolsList').create(),
					hidden:false
				}]
			}, {
				title: 'Technologies',
				items : [{								
					xtype:AM.app.getView('Skills.TechList').create(),
					hidden:false
				}]
			},/* {
				title: 'Languages',
				items : [{								
					xtype:AM.app.getView('Skills.LanguageList').create(),
					hidden:false
				}]
			}*/]
		}];
		
		this.callParent(arguments);
	},
});