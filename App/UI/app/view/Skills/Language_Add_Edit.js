Ext.define('AM.view.Skills.Language_Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.LanguageAddEdit',
	layout : 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'addLanguageFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'language_id',
				id : 'language_id',
			},{
				xtype : 'textfield',
				name : 'name',
				itemId : 'nameItemId',
				allowBlank: false,
				fieldLabel : 'Name <span style="color:red">*</span>',
				anchor: '100%',
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
				xtype     : 'textareafield',
				grow: false,
				name : 'description',
				fieldLabel : 'Description <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 1000,
				allowBlank: false,
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()) {
        	//win.fireEvent('saveSkills', form, win);
        }
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});