Ext.define('AM.view.Skills.SearchSkills', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.skillsFilter',
	layout  : 'fit',
	collapsible: true,
	collapsed: false,
	floatable: true,
	width : '100%',
	defaults: {
		autoScroll:true
	},
	title   : 'Filters',
	
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
	},

	initComponent : function() {
		var me = this;	

		this.items = [{
			xtype:'container',
			layout: {
				type: 'vbox',
			},
			items: [{
				xtype:'button',
				id: "ClearFilter",
				anchor: '5%',
				text:'Clear Filters',
				icon: AM.app.globals.uiPath+'resources/images/reset.png',
				style:{
					float:'left',
					'margin':'10px'
				},
				handler : function() {
					 AM.app.getController('Skills').resetAllFilter();
				}
			},{
				//combobox 0
				xtype : 'combobox',
				multiSelect: false,
				id: 'skillset',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('Skills'),
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				displayField: 'skill_name',
				valueField: 'skill_id',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Skills',
				labelWidth: 50,
				emptyText: "Select Skills",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 1
				xtype : 'combobox',
				multiSelect: false,
				id: 'tools',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('Tools'),
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				displayField: 'skill_name',
				valueField: 'skill_id',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Tools',
				labelWidth: 50,
				emptyText: "Select Tools",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 2
				xtype : 'combobox',
				multiSelect: false,
				id: 'technologies',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('Technologies'),
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				displayField: 'skill_name',
				valueField: 'skill_id',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Technologies',
				labelWidth: 50,
				emptyText: "Select Technologies",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 3
				xtype : 'combobox',
				multiSelect: false,
				id: 'shift',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('Shifts'),
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				displayField: 'shift_code',
				valueField: 'shift_id',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Shift',
				labelWidth: 50,
				emptyText: "Select Shift",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 4
				xtype : 'combobox',
				multiSelect: false,
				id: 'designation',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('Designations'),
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				displayField: 'SkillDesignation',
				valueField: 'designation_id',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Designation',
				labelWidth: 50,
				emptyText: "Select Designation",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 5
				xtype : 'combobox',
				multiSelect: false,
				id : 'experience',
				minChars: 0,
				forceSelection: true,
				queryMode: 'local',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Level'],
					data: [
					{'Flag': '0', 'Level': '<1 Year'},
					{'Flag': '1', 'Level': '1-2 Years'},
					{'Flag': '2', 'Level': '2-3 Years'},
					{'Flag': '3', 'Level': '3-4 Years'},
					{'Flag': '4', 'Level': '4-5 Years'},
					{'Flag': '5', 'Level': '5+ Years'}
					]
				}),
				displayField: 'Level', 
				valueField: 'Flag',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Experience Level',
				labelWidth: 50,
				emptyText: "Select Experience",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						/*queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}*/
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},/*{
				//combobox 6
				xtype : 'combobox',
				multiSelect: false,
				id : 'passport',
				minChars: 0,
				forceSelection: true,
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Status'],
					data: [
					{'Flag': '1', 'Status': 'Yes'},
					{'Flag': '0', 'Status': 'No'}
					]
				}),
				displayField: 'Status', 
				valueField: 'Flag',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Passport Status',
				labelWidth: 50,
				emptyText: "Select Passport Status",
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 7
				xtype : 'combobox',
				multiSelect: false,
				id : 'visa',
				minChars: 0,
				forceSelection: true,
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Status'],
					data: [
					{'Flag': '1', 'Status': 'Yes'},
					{'Flag': '0', 'Status': 'No'}
					]
				}),
				displayField: 'Status', 
				valueField: 'Flag',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'VISA Status',
				labelWidth: 50,
				emptyText: "Select VISA Status",
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 8
				xtype : 'combobox',
				multiSelect: false,
				id : 'stepup',
				minChars: 0,
				forceSelection: true,
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Stage'],
					data: [
					{'Flag': '1', 'Stage': 'Stage 1'},
					{'Flag': '2', 'Stage': 'Stage 2'},
					{'Flag': '3', 'Stage': 'Stage 3'},
					{'Flag': '4', 'Stage': 'Stage 4'},
					{'Flag': '5', 'Stage': 'Stage 5'}
					]
				}),
				displayField: 'Stage', 
				valueField: 'Flag',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Step Up',
				labelWidth: 50,
				emptyText: "Select Step Up level",
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},*/{
				//combobox 9
				xtype : 'combobox',
				multiSelect: false,
				id : 'languages',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('LanguagesList'),
				queryMode: 'remote',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				displayField: 'language', 
				valueField: 'language_id',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Languages Known',
				labelWidth: 50,
				emptyText: "Select Languages",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			},{
				//combobox 10
				xtype : 'combobox',
				multiSelect: false,
				id : 'certifications',
				minChars: 0,
				forceSelection: true,
				queryMode: 'local',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Status'],
					data: [
					{'Flag': '1', 'Status': 'Yes'},
					{'Flag': '0', 'Status': 'No'}
					]
				}),
				displayField: 'Status', 
				valueField: 'Flag',
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Certifications',
				labelWidth: 50,
				emptyText: "Select Certifications",
				listeners: {
					afterrender: function(combo) {
						/*var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));*/
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						this.passParams(me);
					},
					change:function(combo){

					},
					scope :this
				}
			}]
		}];

		this.callParent(arguments);
		
	},

	passParams : function (me){
		AM.app.getController('Skills').listEmployees(
			me.query('combobox')[0].getValue(), 
			me.query('combobox')[1].getValue(),
			me.query('combobox')[2].getValue(),
			me.query('combobox')[3].getValue(),
			me.query('combobox')[4].getValue(),
			me.query('combobox')[5].getValue(),
			me.query('combobox')[6].getValue(),
			me.query('combobox')[7].getValue(),
			);
	},

});
