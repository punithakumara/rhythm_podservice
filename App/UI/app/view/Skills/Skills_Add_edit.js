Ext.define('AM.view.Skills.Skills_Add_edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.skillsAddEdit',
	layout : 'fit',
	width : '40%',
	autoShow : true,
	autoSave: false,
	autoHeight : true,
	modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'addSkillFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'skill_id',
				id : 'skill_id',
			},{
				xtype : 'hiddenfield',
				name : 'skill_code',
				value : 'SK',
			},{
				xtype : 'textfield',
				name : 'skill_name',
				itemId : 'nameItemId',
				allowBlank: false,
				fieldLabel : 'Name <span style="color:red">*</span>',
				anchor: '100%',
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
				xtype     : 'textareafield',
				grow: false,
				name : 'skill_description',
				fieldLabel : 'Description',
				anchor: '100%',
				maxLength: 1000,
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()){
        	AM.app.getController('Skills').onSaveSkill(form, win);
        }
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});