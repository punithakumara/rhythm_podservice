Ext.define('AM.view.newIncidentLog.apprView',{
	extend : 'Ext.form.Panel',
	layout : 'fit',
	width : '99%',
	layout : 'fit',
	title: 'View Appereciation Details',
	border : 'true',
	style: {
		'margin-bottom':'35px'
	},

	initComponent : function() {
		var me = this;
		
		this.items = [{
			xtype : 'form',
			itemId : 'addLogFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 150,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hidden',
				name : 'id',
				id : 'id',
				value: '',
				submitValue: true
			},{
				xtype : 'displayfield',
				fieldLabel : 'Log Date <span style="color:red">*</span>',
				anchor: '100%',
				value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
				readOnly : true,
				name:'log_date',
				submitValue: true,
			}, {
				xtype : 'displayfield',
				fieldLabel : 'Appreciation ID <span style="color:red">*</span>',
				anchor: '100%',
				value : 'APPR'+Ext.Date.format(new Date(), 'yhis'),
				name:'appr_id',
				submitValue: true,
				readOnly : true,
			},{
				xtype : 'displayfield',
				fieldLabel: 'Clients <span style="color:red">*</span>',
				name: 'Client',
				id: 'Client',
				width: 600,
			},{
				xtype : 'displayfield',
				fieldLabel: 'Work Order <span style="color:red">*</span>',
				name: 'wor_name',
				width: 600,
			},{
				xtype : 'displayfield',
				width: 630,	
				fieldLabel: 'Employees <span style="color:red">*</span>',
				name: 'responsible_member_name',
			}, {
				xtype : 'displayfield',
				format : AM.app.globals.CommonDateControl,
				name : 'appr_date',
				fieldLabel : 'Appreciation Sent Date <span style="color:red">*</span>',
			}, {
				xtype : 'displayfield',								
				name : 'appr_by',
				fieldLabel : 'Appreciated By <span style="color:red">*</span>',
				id:'AppreciatedBy'
			}, {
				xtype : 'displayfield',
				name : 'description',
				fieldLabel : 'Description <span style="color:red">*</span>',
				anchor: '100%',
			}, {
				xtype : 'hiddenfield',
				name : 'aprattach_ori',
				id:"aprattach_ori"
			}, {
				xtype : 'fieldcontainer',
				fieldLabel : 'File',
				flex: 1,
				layout: 'hbox',
				id: 'attachApprFile',
				items:[{
					xtype: 'displayfield',
					name:"aprattach",
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'10px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/download.png',
					tooltip : 'Download',
					listeners : {
						click: function(){													
							AM.app.getController('newIncidentLog').aprfileDownload(Ext.getCmp('aprattach_ori').getValue());				
						}
					}
				}/*,{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'6px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/delete.png',
					tooltip : 'Delete',
					id   : 'delete_apr',
					listeners : {
					click: function(){													
						AM.app.getController('newIncidentLog').aprfileDelete(Ext.getCmp('id').getValue());				
					},
					afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
				}
				}*/]
			}]
		}];
		
		this.buttons = [{
			xtype: 'button',
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}];
		
		this.callParent(arguments);
	},
		
	onCancel : function() {
		AM.app.getController('newIncidentLog').viewContent('appr');
	},
});