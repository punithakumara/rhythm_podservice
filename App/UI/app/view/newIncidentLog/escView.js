Ext.define('AM.view.newIncidentLog.escView',{
	extend : 'Ext.form.Panel',
	layout : 'fit',
	width : '99%',
	layout : 'fit',
	title: 'View Escalation Log Details',
	border : 'true',
	style: {
		'margin-bottom':'35px'
	},

	initComponent : function() {
		var me = this;
		

		this.items = [{
			xtype : 'form',
			layout: 'column',
			itemId : 'addLogFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 150,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype: 'fieldset',
				margin: '0 10 12 0',
				padding: '8',
				style : {
					width : '54%',
				},
				items:[{
					xtype : 'hidden',
					name : 'id',
					id : 'esc_id',
					value: '',
					submitValue: true
				},{
					xtype : 'hidden',
					name : 'client_id',
					id : 'client_id',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Log Date <span style="color:red">*</span>',
					name: 'log_date',
					id: 'log_date',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					readOnly : true,
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Escalation ID <span style="color:red">*</span>',
					name: 'error_id',
					id: 'error_id',
					value : 'ESC'+Ext.Date.format(new Date(), 'yhis'),
					readOnly : true,
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel: 'Clients <span style="color:red">*</span>',
					name: 'client',
					id: 'client',
					width: 600,
				},{
					xtype : 'displayfield',
					fieldLabel: 'Work Order <span style="color:red">*</span>',
					name: 'wor_name',
					width: 600,
				},{
					xtype : 'displayfield',
					fieldLabel: 'Managers <span style="color:red">*</span>',
					name: 'manager_name',
					width: 600,
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Employees <span style="color:red">*</span>',
					name: 'production_member_name',
					width: 600,
				},{
					xtype : 'displayfield',
					name : 'escalation_date',
					fieldLabel : 'Escalation Raised Date <span style="color:red">*</span>',
					width: 300,
					
				}, {
					xtype : 'displayfield',
					name : 'problem_summary',
					fieldLabel : 'Escalation Statement <span style="color:red">*</span>',
					width: 600,
				}, {
					xtype : 'displayfield',
					name : 'impact',
					fieldLabel : 'Escalation Summary <span style="color:red">*</span>',
					width: 600,
				}]
			} , {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '54%',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'Impact - Business <span style="color:red">*</span>',
					name      : 'impact_business',
				},{
					xtype : 'displayfield',								
					fieldLabel : 'Impact - Revenue ($) <span style="color:red">*</span>',
					name : 'impact_revenue',
					width: 300,
				},{
					xtype : 'displayfield',
					fieldLabel : 'Impact - Relationship <span style="color:red">*</span>',
					name      : 'impact_relationship',
				},{
					xtype : 'displayfield',
					fieldLabel: 'Root Cause <span style="color:red">*</span>',
					name: 'rootcause_id',
					width: 630,
					renderer: function(value) {
						var rec	= Ext.getStore('newRootCause').findRecord('RootCauseID', value);
						return rec == null ? value : rec.get('Description');
					},
				},{
					xtype : 'displayfield',
					name : 'rootcause_analysis',
					fieldLabel : 'Root Cause Analysis <span style="color:red">*</span>',
					width: 600,
					height: 'auto'
				},{
					xtype : 'displayfield',
					name : 'mitigation_plan',
					fieldLabel : 'Mitigation plan <span style="color:red">*</span>',
					autoScroll:true,
					width: 600,
				},{
					xtype : 'displayfield',
					name : 'mitigation_plan_date',
					fieldLabel : 'Mitigation Plan Date <span style="color:red">*</span>',
					width: 300,
				},{
					xtype		: 'hiddenfield',
					name : 'escattach_ori',
					id:"escattach_ori"
				},{
				xtype : 'fieldcontainer',
				fieldLabel : 'File',
				flex: 1,
				layout: 'hbox',
				id: 'attachEscFile',
				items:[{
					xtype: 'displayfield',
					name:"escattach",
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'10px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/download.png',
					tooltip : 'Download',
					listeners : {
						click: function(){													
							AM.app.getController('newIncidentLog').escfileDownload(Ext.getCmp('escattach_ori').getValue());				
						}
					}
				}/*,{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'6px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/delete.png',
					tooltip : 'Delete',
					id   : 'delete_apr',
					listeners : {
					click: function(){													
						AM.app.getController('newIncidentLog').escfileDelete(Ext.getCmp('esc_id').getValue());				
					},
					afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
				}
				}*/]
			}]
			}]
		}];
		
		this.buttons = [{
			xtype: 'button',
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			xtype: 'button',
			text : 'Approve',
			id   : 'approve',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			listeners : {
				click: function(){												
					AM.app.getController('newIncidentLog').reviewApprove(Ext.getCmp('esc_id').getValue(), 'approve', Ext.getCmp('client_id').getValue());				
				}
			}
		}, {
			xtype: 'button',
			text : 'Review',
			id   : 'review',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			listeners : {
				click: function(){													
					AM.app.getController('newIncidentLog').reviewApprove(Ext.getCmp('esc_id').getValue(), 'review', Ext.getCmp('client_id').getValue());				
				}
			}
		}];
		
		this.callParent(arguments);
	},
	
	
	onCancel : function() {
		AM.app.getController('newIncidentLog').viewContent('esc');
	},
});
