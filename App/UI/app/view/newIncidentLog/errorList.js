Ext.define('AM.view.newIncidentLog.errorList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.errorListGrid',	
	title: 'External Error List',    
    store : 'newErrorLog',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
	frame: true,
	id: 'errorListGrid',
	width: "99%",
	height: 'auto',
	style: {
	    border: "0px solid #157fcc",
	    borderRadius:'0px'	      
	},
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	border : true,
	sdfds: '',
    initComponent : function() {
	
		var me = this;
		
		this.tools = [{
	        xtype : 'trigger',
	        itemId : 'gridTrigger',
	        id : 'gridTrigger',
	        fieldLabel : '',
	        labelWidth : 60,
	        labelCls : 'searchLabel',
	        triggerCls : 'x-form-clear-trigger',
	        emptyText : 'Search',
	        width : 250,
	        minChars : 1,
	        enableKeyEvents : true,
	        onTriggerClick : function(){
	            this.reset();
	            this.fireEvent('triggerClear');
	        }
	    }, { 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    }, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			id: 'incidentLogButton',
			text : 'Add External Error',
			handler: function () {
				AM.app.getController('newIncidentLog').onCreate('error');
		    },
			listeners : {
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		}, { 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    },{
			xtype: "button",
	    	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			text : 'Export To Excel',
			style: {
	            "margin-right": "8px"
	        },
			handler: function(obj){
	    		AM.app.getController('newIncidentLog').exportExternalError();
			}
		}];
		
		this.columns = [{
			header : 'Error ID', 
			width    : '39%',
			dataIndex : 'error_id',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Error Date', 
			dataIndex : 'error_date',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 1
		}, {
			header : 'Client', 
			dataIndex : 'Client', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.5
		}, {
			header : 'Responsible Person', 
			width    : '39%',
			dataIndex : 'responsiblePerson',
			flex: 2,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			sortable: false,
			renderer: function(value, metaData, record, rowIdx, colIdx, store) 
			{        		
            	if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
            	return value;        
            },
		}, { 
			header : 'Creator', 
			dataIndex : 'createdBy', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		},{
			header : 'Reviewer', 
			dataIndex : 'reviewed_by', 
			//sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		},{
			header : 'Approver', 
			dataIndex : 'approved_by', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			//sortable: false,
			flex: 1
		},{
			header : 'Action', 
			width : '10%',
			xtype : 'actioncolumn',
			align: 'center',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			sortable: false,			
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				id : 'editError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').onEditErrorLog(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {
				
					if(Ext.util.Cookies.get('grade')<3)
					{
						return 'x-hide-display';
					}
					else if(record.data.reviewed_by != null && record.data.approved_by != null && record.data.reviewed_by != "" && record.data.reviewed_by !=" " && record.data.approved_by != "" && record.data.approved_by != " ")
				    {
						return 'x-hide-display';
				    }
				    else
				    { 
						return 'rowVisible';
				    }
				   
				}
			}, '-', {
				icon : AM.app.globals.uiPath+'resources/images/copy.png',
				tooltip : 'Copy',
				id : 'copyError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnCopyError(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {
					
					if(Ext.util.Cookies.get('grade')>2)
				    {
						return 'rowVisible';
				    }
				    else
				    { 
						return 'x-hide-display';
				    }
				}
			}, '-', {
				icon : AM.app.globals.uiPath+'resources/images/view.png',
				tooltip : 'View',
				id : 'viewError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnViewError(grid, rowIndex, colIndex);
				},
				getTip: function(value, meta, record){
					if(record.data.reviewed_by != null && record.data.approved_by == null && Ext.util.Cookies.get('grade')>2)
					{
						return 'Approve';
					}
					if(record.data.reviewed_by != null && record.data.approved_by != null)
					{
						return 'View';
					}
					if(record.data.reviewed_by == null && record.data.approved_by == null && Ext.util.Cookies.get('grade')>2)
					{
						return 'Review / Approve';
					}
					if(record.data.reviewed_by == null && record.data.approved_by == null && Ext.util.Cookies.get('grade')<3)
					{
						return 'View';
					}
				}
			}, '-', {
				icon : AM.app.globals.uiPath+'resources/images/delete.png',
				getClass: function(value, meta, record) {
					if(Ext.util.Cookies.get('grade')<4)
					{
						return 'x-hide-visibility';
					}
					if(record.data.reviewed_by != null && record.data.approved_by != null && record.data.reviewed_by != "" && record.data.reviewed_by !=" " && record.data.approved_by != "" && record.data.approved_by != " ")
				    {
						return 'x-hide-display';
				    }
				    else
				    { 
						return 'rowVisible';
				    }
				},
				tooltip : 'Delete',
				id : 'delError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnDelete(grid, rowIndex, colIndex);
				},
			}]
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No error to display.",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
});