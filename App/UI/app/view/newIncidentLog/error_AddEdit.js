Ext.define('AM.view.newIncidentLog.error_AddEdit',{
	extend : 'Ext.form.Panel',
	layout : 'fit',
	width : '99%',
	layout : 'fit',
	id : 'error_add_edit',
	title: 'Add External Error Details',
	border : 'true',
	style: {
		'margin-bottom':'35px'
	},
	initComponent : function() {
		var me = this;
		var date = new Date(), 
		startMinDate = Ext.Date.add(date, Ext.Date.YEAR, -12), startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 1), 
		endMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		
		this.items = [{
			xtype : 'form',
			layout: 'column',
			id : 'addLogFrm',
			
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 150,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			fileUpload:true,
			isUpload: true,
			enctype:'multipart/form-data',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype: 'fieldset',
				//margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '52.8%',
				},
				items:[{
					xtype : 'hidden',
					name : 'id',
					value: '',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Log Date <span style="color:red">*</span>',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					name : 'log_date',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Error ID <span style="color:red">*</span>',
					value : 'ERR'+Ext.Date.format(new Date(), 'yhis'),
					name : 'error_id',
					id:'error_id',
					submitValue: true
				}, {
					xtype : 'boxselect',
					multiSelect: false, 
					fieldLabel: 'Clients <span style="color:red">*</span>',
					name: 'Client',
					id:'Client',
					store: 'Clients',
					queryMode: 'remote',
					minChars:0,
					displayField: 'client_name',
					valueField: 'client_id',
					emptyText: 'Select Client',
					allowBlank: false,
					value: '',
					width: 630,
					listeners:{
						change: function(obj){
							Ext.getStore('workOrder').load({
								params:{ 'comboClient': obj.getValue()}
							});
						},
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1', 'comboClient':'1','filterName' : queryEvent.combo.displayField};
						},
						select : function() {
							Ext.getCmp('wor_id').enable();
							Ext.getCmp('production_member').setDisabled(true);
							Ext.getCmp('qa_member').setDisabled(true);
							Ext.getCmp('team_leader').setDisabled(true);
							Ext.getCmp('associate_manager').setDisabled(true);

							Ext.getCmp('wor_id').setValue('');
							Ext.getCmp('production_member').setValue('');
							Ext.getCmp('qa_member').setValue('');
							Ext.getCmp('team_leader').setValue('');
							Ext.getCmp('associate_manager').setValue('');
						}
					},
				} , {
					xtype : 'boxselect',
					multiSelect: false, 
					fieldLabel: 'Work Order <span style="color:red">*</span>',
					name: 'wor_id',
					id:'wor_id',
					store: 'workOrder',
					queryMode: 'remote',
					minChars:0,
					displayField: 'wor_name',
					valueField: 'wor_id',
					emptyText: 'Select Work Order',
					allowBlank: false,
					width: 630,
					disabled: true,
					listeners:{
						change: function(obj){
							var ClntID =   Ext.getCmp('Client').getValue();
							Ext.getStore('Employees').load({
								params:{  'comboClient':ClntID}
							});
						},
						 beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							var ClntID =  me.query('combobox')[0].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'filterName' : queryEvent.combo.displayField};
						},
						select : function() {
							Ext.getCmp('wor_id').enable();
							Ext.getCmp('production_member').enable();
							Ext.getCmp('qa_member').enable();
							//Ext.getCmp('domain_expert').enable();
							Ext.getCmp('team_leader').enable();
							Ext.getCmp('associate_manager').enable();	

							Ext.getCmp('production_member').setValue('');
							Ext.getCmp('qa_member').setValue('');
							Ext.getCmp('team_leader').setValue('');
							Ext.getCmp('associate_manager').setValue('');
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Error Category<span style="color:red">*</span>',
					store : Ext.getStore('Errorcategory'),
					queryMode: 'remote',
					name: 'error_category',
					id:"ErrCat",
					minChars:0,
					displayField: 'category_name',
					valueField	: 'error_category_id',
					emptyText: 'Select Error Category',
					width: 630,
					mode: 'remote',
					renderer: function(value) {
						var rec	= Ext.getStore('Errorcategory').findRecord('error_category_id', value);
						return rec == null ? value : rec.get('category_name');
					},
					listeners:{
						change: function(obj){
							Ext.getStore('Errorsubcategory').load({
								params:{ 'parent_id': obj.getValue()}
							});		
						},
						select : function() {
							Ext.getCmp('ErrSubCat').enable();	
							Ext.getCmp('ErrSubCat').setValue('');								
						},
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							var ClntID =  me.query('combobox')[0].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'filterName' : queryEvent.combo.displayField};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Error SubCategory<span style="color:red">*</span>',
					store : Ext.getStore('Errorsubcategory'),
					//queryMode: 'remote',
					name: 'error_subcategory',
					minChars:0,
					displayField: 'category_name',
					valueField	: 'error_category_id',
					emptyText: 'Select Error SubCategory',
					width: 630,
					id:"ErrSubCat",
					mode: 'remote',
					disabled: true,
					renderer: function(value) {
						var rec	= Ext.getStore('Errorcategory').findRecord('error_category_id', value);
						return rec == null ? value : rec.get('category_name');
					},
					listeners:{
						beforequery: function(queryEvent){

							Ext.Ajax.abortAll();
							var ClntID =  me.query('combobox')[0].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {'parent_id':Ext.getCmp('ErrCat').getValue(),'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'filterName' : queryEvent.combo.displayField};
						},
					}
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					fieldLabel: 'Associate Manager<span style="color:red">*</span>',
					name: 'associate_manager',
					id: 'associate_manager',
					store: 'AssociateManagerIncident',
					mode: 'local',
					allowBlank: false,
					minChars: 1,
					width: 630,
					displayField: 'full_name', 
					valueField: 'employee_id',
					emptyText: 'Select Associate Manager',
					disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'client_id':me.query('combobox')[0].getValue(),'wor_id' : me.query('combobox')[1].getValue(), 'employee_type':'associate_manager'};
						}
					}
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					fieldLabel: 'Team Lead',
					name: 'team_leader',
					id: 'team_leader',
					store: 'TeamLeaderIncident',
					mode: 'local',
					minChars: 1,
					width: 630,
					displayField: 'full_name', 
					valueField: 'employee_id',
					emptyText: 'Select Team Lead',
					disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'client_id':me.query('combobox')[0].getValue(),'wor_id' : me.query('combobox')[1].getValue(), 'employee_type':'team_leader'};
						}
					}
				},{
					xtype : 'boxselect',
					multiSelect: true,
					fieldLabel: 'Production Member(s) <span style="color:red">*</span>',
					name: 'production_member[]',
					id: 'production_member',
					store: 'Employees',
					mode: 'local',
					minChars: 0,
					width: 630,
					displayField: 'full_name', 
					valueField: 'employee_id',
					allowBlank: false,
					emptyText: 'Select Employee',
					disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							var ClntID =  me.query('combobox')[0].getValue();
							
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'wor_id' : me.query('combobox')[1].getValue(),'filterName' : 'employees.first_name'};
						}
					}
				}, {
					xtype : 'boxselect',
					multiSelect: true,
					fieldLabel: 'QA Member(s)',
					name: 'qa_member[]',
					id: 'qa_member',
					store: 'Employees',
					mode: 'local',
					minChars: 0,
					width: 630,
					displayField: 'full_name', 
					valueField: 'employee_id',
					emptyText: 'Select Employee',
					disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							var ClntID =  me.query('combobox')[0].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'wor_id' : me.query('combobox')[1].getValue(),'filterName' : 'employees.first_name'};
						}
					}
				}, {
					xtype:'panel',
					layout : 'hbox',
					items: [{
						xtype : 'datefield',
						format : AM.app.globals.CommonDateControl,
						grow : true,
						name : 'error_date',
						fieldLabel : 'Error Date <span style="color:red">*</span>',
						value : new Date(),
						//maxValue : new Date(),
						allowBlank : false,
						width: 280,
						minValue: startMinDate,
						//maxValue: startMaxDate,
						maxValue : new Date(),
						listeners: {
							select : function(combo) {
								me.query('datefield')[1].setMinValue(me.query('datefield')[0].getValue());
								me.query('datefield')[2].setMinValue(me.query('datefield')[0].getValue());
								me.query('datefield')[3].setMinValue(me.query('datefield')[0].getValue());
							}
						}
					}, {
						xtype : 'datefield',
						format : AM.app.globals.CommonDateControl,
						grow : true,
						name : 'error_notified_date',
						id : 'error_notified_date',
						fieldLabel : 'Error Notified Date <span style="color:red">*</span>',
						value : new Date(),
						minValue : new Date(),
						allowBlank : false,
						width: 280,
						padding: '1 0 4 40',
						listeners: {
							select : function(combo) {
								me.query('datefield')[2].setMinValue(me.query('datefield')[1].getValue());
								me.query('datefield')[3].setMinValue(me.query('datefield')[1].getValue());
							}
						}
					}]
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'problem_summary',
					fieldLabel : 'Error Statement <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 630,
					height: 150,
				}, {
					xtype : 'numberfield',								
					fieldLabel : '# of units impacted <span style="color:red">*</span>',
					name : 'units_impacted',
					id:'units_impacted',
					width: 300,
					allowBlank: false,
					minValue: 1,
					maxValue: 100000,
		           
       
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'impact',
					fieldLabel : 'Error Summary <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 630,
					height: 150,
				}, {
					xtype : 'numberfield',
					grow : false,
					minValue : 0,
					maxValue : 99,
					name : 'missed_tat',
					fieldLabel : 'Missed TAT <span style="color:red">*</span>',
					allowBlank: false,
					valueField: 'missed_tat',
				}]
			} , {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '52.8%',
				},
				items:[{
					xtype : 'radiogroup',
					fieldLabel : 'Impact - Business <span style="color:red">*</span>',
					layout: 'hbox',
					id:'errimpact_business',
					items: [{
						boxLabel  : 'Yes',
						name      : 'impact_business',
						width     : 70,
						inputValue: 'yes',
					}, {
						boxLabel  : 'No',
						name      : 'impact_business',
						inputValue: 'no',
						checked: true,
					}]
				}, {
					xtype : 'numberfield',								
					fieldLabel : 'Impact - Revenue ($) <span style="color:red">*</span>',
					name : 'impact_revenue',
					id:'impact_revenue',
					width: 300,
					value: 0,
					minValue:0,
				}, {
					xtype : 'radiogroup',
					fieldLabel : 'Impact - Relationship <span style="color:red">*</span>',
					layout: 'hbox',
					id:'errimpact_relationship',
					items: [{
						boxLabel  : 'Yes',
						name      : 'impact_relationship',
						width	  : 70,
						inputValue: 'yes',
					}, {
						boxLabel  : 'No',
						name      : 'impact_relationship',
						inputValue: 'no',
						checked: true,
					}]
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					fieldLabel: 'Root Cause <span style="color:red">*</span>',
					name: 'rootcause_id',
					id: 'rootcause_id',
					store: 'newRootCause',
					mode: 'remote',
					minChars: 0,
					displayField: 'Description', 
					valueField: 'RootCauseID',
					width: 630,
					allowBlank: false,
					value: '',
					emptyText: 'Select Root Cause',
					listeners:{
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'filterName' : queryEvent.combo.displayField};
						}
					},
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'rootcause_analysis',
					fieldLabel : 'Root Cause Analysis <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 630,
					height: 150,
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'correction',
					fieldLabel : 'Correction <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 630,
					height: 150,					
				}, {
					xtype:'panel',
					layout : 'hbox',
					items: [{
						xtype : 'datefield',
						format : AM.app.globals.CommonDateControl,
						grow : true,
						name : 'correction_date',
						id : 'correction_date',
						fieldLabel : 'Correction Date <span style="color:red">*</span>',
						anchor : '100%',
						value : new Date(),
						minValue : new Date(),
						allowBlank : false,
						width: 280,
						//maxValue: endMaxDate,
						listeners: {
							select : function(combo) {
								me.query('datefield')[0].setMaxValue(me.query('datefield')[2].getValue());
								me.query('datefield')[1].setMaxValue(me.query('datefield')[2].getValue());
							}
						}
					}, {
						xtype : 'datefield',
						format : AM.app.globals.CommonDateControl,
						grow : true,
						name : 'corrective_action_date',
						id : 'corrective_action_date',
						fieldLabel : 'Corrective Action Date <span style="color:red">*</span>',
						value : new Date(),
						minValue : new Date(),
						Value : new Date(),
						allowBlank : false,
						width: 280,
						padding: '1 0 4 40'
					}]
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'corrective_action',
					fieldLabel : 'Corrective Action <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 630,
					height: 150
				}, {
					xtype		: 'hiddenfield',
					name		: 'attachment',
					id			: 'attachment'
				},{
					//xtype: 'multiupload',
					xtype: 'filefield',
					multiple: 'true',
					name: 'file',
					fieldLabel: 'Attachments <br/>(Max file size 5 MB)',
					emptyText: 'Single attachment only.',
					msgTarget: 'side',
					buttonText: 'Browse..',
					id: 'browse_btn_enable',
						listeners: {
                        change: function(fld, value) {
                            var newValue = value.replace(/C:\\fakepath\\/g, '');
							var file_ext = newValue.split('.').pop();
							var stringArray = ["doc","docx","pdf","xlsx","xls","pst","txt","rtf","csv","zip","msg"];

							if (stringArray.indexOf(file_ext) >= 0) 
							{
								fld.setRawValue(newValue);
								this.disable();
							}
							else
							{
								Ext.Msg.alert("Alert", "Allowed file types are doc, docx, pdf, xlsx, xls, pst, txt, rtf, csv, zip, msg ");
								fld.setRawValue("");
								//return false
							}
                        }
                    }
				}, {
					xtype		: 'hiddenfield',
					name : 'errattach_ori',
					id:"errattach_ori"

				},{
				xtype : 'hiddenfield',
				name : 'attachid',
				id:"attachid"
			},  {
				xtype : 'fieldcontainer',
				fieldLabel : 'File',
				flex: 1,
				layout: 'hbox',
				id: 'attachErrFile',
				items:[{
					xtype: 'displayfield',
					name:"errattach",
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'10px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/download.png',
					tooltip : 'Download',
					listeners : {
						click: function(){													
							AM.app.getController('newIncidentLog').errfileDownload(Ext.getCmp('errattach_ori').getValue());				
						}
					}
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'6px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/delete.png',
					tooltip : 'Delete',
					id   : 'delete_err',
					listeners : {
					/*click: function(){													
						AM.app.getController('newIncidentLog').aprfileDelete(Ext.getCmp('id').getValue());				
					},*/
					click : function() {
						Ext.MessageBox.show({
						title: "Delete",
						msg: "Are you sure want to delete this file?",
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.MessageBox.YESNO,
						fn: function(buttonId) {
					if (buttonId === "yes") {
					var atachid=Ext.getCmp('error_id').getValue();
					Ext.getCmp('attachid').setValue(atachid);
					Ext.getCmp('attachErrFile').hide();
					
				
					}
			}
		});
					},
					afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
				}
				}]
			}]
			}]
		}];
		
		this.buttons = [{
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : function() {
				Ext.getCmp('browse_btn_enable').enable();
				var pnl = this.up('panel');
				var form = pnl.down('form').getForm();
				
				if (form.isValid())
				{
					form.submit({
						url: AM.app.globals.appPath+'index.php/newIncidentLog/log_addError/?v='+AM.app.globals.version,
						headers : {'Content-Type':'multipart/form-data; charset=UTF-8'},
                        waitMsg: 'Uploading your data...',
						method: 'POST',
                        success: function(batch, o) 
						{
							Ext.MessageBox.show({
								title: "Success",
								msg: "Error Log Added Successfully.",
								buttons: Ext.Msg.OK
							});
							AM.app.getController('newIncidentLog').viewContent('error');
                        },
                         failure: function(response) {
                        	Ext.MessageBox.show({
								title: "Alert",
								msg: "File size exceeded",
								buttons: Ext.Msg.OK
							});
                        }
                    });
				}
				// AM.app.getController('newIncidentLog').onSaveErrorLog(form);
			}
		}];
		
		this.callParent(arguments);
	},
	
	onCancel : function() {
		AM.app.getController('newIncidentLog').viewContent('error');
	},
});