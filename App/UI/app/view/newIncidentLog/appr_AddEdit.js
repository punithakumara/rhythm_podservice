Ext.define('AM.view.newIncidentLog.appr_AddEdit',{
	extend : 'Ext.panel.Panel',
	layout : 'fit',
	width : '99%',
	layout : 'fit',
	id : 'appr_add_edit',
	title: 'Add Appreciation Details',
	border : 'true',
	initComponent : function() {
		var me = this;

		this.items = [{
			xtype : 'form',
			itemId : 'addLogFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 150,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hidden',
				name : 'id',
				value: '',
				submitValue: true
			},{
				xtype : 'displayfield',
				fieldLabel : 'Log Date <span style="color:red">*</span>',
				anchor: '100%',
				value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
				readOnly : true,
				name:'log_date',
				submitValue: true,
			}, {
				xtype : 'displayfield',
				fieldLabel : 'Appreciation ID <span style="color:red">*</span>',
				anchor: '100%',
				value : 'APPR'+Ext.Date.format(new Date(), 'yhis'),
				name:'appr_id',
				id:'appr_id',
				submitValue: true,
				readOnly : true,
			}, {
				xtype : 'boxselect',
				multiSelect: false, 
				fieldLabel: 'Client <span style="color:red">*</span>',
				name: 'client_id',
				id: 'client',
				store: Ext.getStore('Clients'),
				queryMode: 'remote',
				minChars:0,
				displayField: 'client_name',
				valueField: 'client_id',
				emptyText: 'Select Client',
				width: 630,
				allowBlank: false,
				value: '',
				listeners		: {
					change: function(obj){
						Ext.getStore('workOrder').load({
							params:{ 'comboClient': obj.getValue()}
						});
					},
					beforequery	: function(queryEvent) {
						AM.app.getStore('Clients').getProxy().extraParams = {
							'hasNoLimit'	:'1', 
							'filterName' 	: queryEvent.combo.displayField,
							'clientStatusCombo':'1',
						};
					},
					select : function() {							
						Ext.getCmp('wor_id').setDisabled(false);	
						Ext.getCmp('wor_id').enable();	
						Ext.getCmp('wor_id').setValue('');
						
						Ext.getCmp('team_leader').setDisabled(true);
						Ext.getCmp('team_leader').reset();
					}
				}
			}, {
				xtype : 'boxselect',
				multiSelect: false, 
				fieldLabel: 'Work Order <span style="color:red">*</span>',
				name: 'wor_id',
				id:'wor_id',
				store: 'workOrder',
				queryMode: 'remote',
				minChars:0,
				displayField: 'wor_name',
				valueField: 'wor_id',
				emptyText: 'Select Work Order',
				allowBlank: false,
				width: 600,
				disabled: true,
				listeners:{
					change: function(obj){
						var ClntID =   Ext.getCmp('client').getValue();
						Ext.getStore('Employees').load({
							params:{  'comboClient':ClntID}
						});
					},
					select : function() {							
						Ext.getCmp('responsible_member').enable();					
						Ext.getCmp('wor_id').setDisabled(false);
						Ext.getCmp('responsible_member').setValue('');			
					},
					beforequery: function(queryEvent){
						Ext.Ajax.abortAll();
						var ClntID =  me.query('combobox')[0].getValue();
						queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'filterName' : queryEvent.combo.displayField};
					}
				}
			}, {
				xtype : 'boxselect',
				multiSelect: true,
				fieldLabel: 'Employees <span style="color:red">*</span>',
				id: 'responsible_member',
				name: 'responsible_member[]',
				store: Ext.getStore('Employees'),
				mode: 'local',
				minChars: 0,
				width: 600,
				displayField: 'full_name', 
				valueField: 'employee_id',
				allowBlank: false,
				emptyText: 'Select Employee',
				listeners : {
					beforequery : function(queryEvent) {
						var ClntID =  me.query('combobox')[0].getValue();
						var wor_id =  me.query('combobox')[1].getValue();

						queryEvent.combo.getStore().getProxy().extraParams = {
							'hasNoLimit' : "1", 
							'comboClient':ClntID,
							'wor_id':wor_id,
							'utilCombo' : 1,
							'filterName' :  'employees.first_name'
						};
					},
					afterrender: function() {
						Ext.getStore('Employees').getProxy().extraParams = {
							'hasNoLimit' : "1", 
							'filterName' : 'employees.first_name'
						};
					}
				}	
			}, {
				xtype : 'datefield',
				format : AM.app.globals.CommonDateControl,
				grow : true,
				name : 'appr_date',
				fieldLabel : 'Appreciation Sent Date <span style="color:red">*</span>',
				anchor : '100%',
				value : new Date(),
				maxValue : new Date(),
				allowBlank : false
			}, {
				xtype : 'textfield',
				maskRe: /[A-Za-z, ]/,								
				name : 'appr_by',
				fieldLabel : 'Appreciated By <span style="color:red">*</span>',
				id:'AppreciatedBy',
				allowBlank: false,
				listeners : {
					blur : function (d) {
						var newVal = d.getValue().trim();
						var newVal2 = newVal.replace(/[0-9]/g, '');
						d.setValue(newVal2.replace(/[^a-zA-Z, ]/g, ''));
					}
				}
			},{
				xtype : 'textareafield',
				grow : false,
				name : 'description',
				fieldLabel : 'Description <span style="color:red">*</span>',
				anchor: '100%',
				allowBlank: false,
				autoScroll:true,
				height:200
			},{
				xtype		: 'hiddenfield',
				name		: 'attachment',
				id			: 'attachment'
			},{
				xtype: 'filefield',
				name: 'apprFile',
				fieldLabel: 'Attachments <br/>(Max file size 5 MB)',
				emptyText: 'Single attachment only.',
				msgTarget: 'side',
				buttonText: 'Browse..',
				anchor: '100%',
				listeners: {
					change: function(fld, value) {
						var newValue = value.replace(/C:\\fakepath\\/g, '');
						var file_ext = newValue.split('.').pop();
						var stringArray = ["doc","docx","pdf","xlsx","xls","pst","txt","rtf","csv","zip","msg"];

						if (stringArray.indexOf(file_ext) >= 0) 
						{
							fld.setRawValue(newValue);
							
						}
						else
						{
							Ext.Msg.alert("Alert", "Allowed file types are doc, docx, pdf, xlsx, xls, pst, txt, rtf, csv, zip, msg ");
							fld.setRawValue("");
						}
					}
				}
			}, {
				xtype : 'hiddenfield',
				name : 'aprattach_ori',
				id:"aprattach_ori"
			},{
				xtype : 'hiddenfield',
				name : 'attachid',
				id:"attachid"
			},  {
				xtype : 'fieldcontainer',
				fieldLabel : 'File',
				flex: 1,
				layout: 'hbox',
				id: 'attachApprFile',
				items:[{
					xtype: 'displayfield',
					name:"aprattach",
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'10px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/download.png',
					tooltip : 'Download',
					listeners : {
						click: function(){													
							AM.app.getController('newIncidentLog').aprfileDownload(Ext.getCmp('aprattach_ori').getValue());				
						}
					}
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'6px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/delete.png',
					tooltip : 'Delete',
					id   : 'delete_apr',
					listeners : {
					/*click: function(){													
						AM.app.getController('newIncidentLog').aprfileDelete(Ext.getCmp('id').getValue());				
					},*/
					click : function() {
						Ext.MessageBox.show({
						title: "Delete",
						msg: "Are you sure want to delete this file?",
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.MessageBox.YESNO,
						fn: function(buttonId) {
					if (buttonId === "yes") {
					var atachid=Ext.getCmp('appr_id').getValue();
					Ext.getCmp('attachid').setValue(atachid);
					Ext.getCmp('attachApprFile').hide();
					
				
					}
			}
		});
					},
					afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
				}
				}]
			}]
		}];
		
		this.buttons = [{
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : function() {
				var pnl = this.up('panel');
				var form = pnl.down('form').getForm();
				
				if (form.isValid())
				{
					form.submit({
						url: AM.app.globals.appPath+'index.php/newIncidentLog/log_addAppr/?v='+AM.app.globals.version,
						headers : {'Content-Type':'multipart/form-data; charset=UTF-8'},
                        waitMsg: 'Uploading your data...',
						method: 'POST',
                        success: function(batch, o) 
						{
							Ext.MessageBox.show({
								title: "Success",
								msg: "Appreciation Added Successfully.",
								buttons: Ext.Msg.OK
							});
							AM.app.getController('newIncidentLog').viewContent('appr');
                        },
                        failure: function(response) {
                        	Ext.MessageBox.show({
								title: "Alert",
								msg: "File size exceeded",
								buttons: Ext.Msg.OK
							});
                        }
                    });
				}
			}
			
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'apprVerticalId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();		

		if(Ext.getCmp('incidentType').getValue()== "Team")
			Ext.getCmp('IL_EmployID').allowBlank = true;
		
		if (form.isValid()) {
			AM.app.getController('IncidentLog').onSaveLog(form);
        	win.close();
        }
	},
	
	onCancel : function() {
		AM.app.getController('newIncidentLog').viewContent('appr');
	},
});