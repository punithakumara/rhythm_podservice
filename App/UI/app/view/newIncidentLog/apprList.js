Ext.define('AM.view.newIncidentLog.apprList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.apprList',	
	title: 'Appreciation List',    
    store : 'apprIncidentLog',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
	frame: true,
	width: "99%",
	height: 'auto',
	style: {
	    border: "0px solid #157fcc",
	    borderRadius:'0px'	      
	},
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	border : true,
	sdfds: '',
    initComponent : function() {
	
		var me = this;
		
		this.tools = [{
	        xtype : 'trigger',
	        itemId : 'gridTrigger',
	        id : 'gridTrigger',
	        fieldLabel : '',
	        labelWidth : 60,
	        labelCls : 'searchLabel',
	        triggerCls : 'x-form-clear-trigger',
	        emptyText : 'Search',
	        width : 250,
	        minChars : 1,
	        enableKeyEvents : true,
	        onTriggerClick : function(){
	            this.reset();
	            this.fireEvent('triggerClear');
	        }
	    }, { 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    }, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			id: 'incidentLogButton',
			handler: function () {
				AM.app.getController('newIncidentLog').onCreate('appr');
		    },
		    listeners : {
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
			text : 'Add Appreciation'
		}];
		
		this.columns = [{
			header : 'Appreciation ID', 
			width    : '39%',
			dataIndex : 'appr_id',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 2
		},{
			header : 'Appreciation Date', 
			width    : '39%',
			dataIndex : 'appr_date',
			format : AM.app.globals.CommonDateControl,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 2
		},{
			header : 'Client', 
			width    : '39%',
			dataIndex : 'Client',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 2
		},{
			header : 'Responsible Person', 
			width    : '39%',
			dataIndex : 'responsible_member_name',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 2
		},{
			header : 'Creator', 
			width    : '39%',
			dataIndex : 'createdBy',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 2
		}, {
			header : 'Action', 
			width : '10%',
			xtype : 'actioncolumn',
			align: 'center',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').onEditAppr(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {
					
					if(Ext.util.Cookies.get('grade')>2)
				    {
						return 'rowVisible';
				    }
				    else
				    { 
						return 'x-hide-display';
				    }
				}
				
			}, '-', {
				icon : AM.app.globals.uiPath+'resources/images/copy.png',
				tooltip : 'Copy',
				id : 'copyError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnCopyAppr(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {
					
					if(Ext.util.Cookies.get('grade')>2)
				    {
						return 'rowVisible';
				    }
				    else
				    { 
						return 'x-hide-display';
				    }
				}
				
			}, '-', {
				icon : AM.app.globals.uiPath+'resources/images/view.png',
				tooltip : 'View',
				id : 'viewError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnViewAppr(grid, rowIndex, colIndex);
				}
			}, '-',{
				icon : AM.app.globals.uiPath+'resources/images/delete.png',
				getClass: function(value, meta, record) {
					if(Ext.util.Cookies.get('grade')<4)
					{
						return 'x-hide-visibility';
					}
				},
				tooltip : 'Delete',
				id : 'delOpt',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnDeleteAppr(grid, rowIndex, colIndex);
				}
			}]
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No appreciation to display.",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
});