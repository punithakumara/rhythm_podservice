Ext.define('AM.view.newIncidentLog.errorView',{
	extend : 'Ext.form.Panel',
	layout : 'fit',
	width : '99%',
	layout : 'fit',
	title: 'View External Error Details',
	border : 'true',
	style: {
		'margin-bottom':'35px'
	},
	initComponent : function() {
		var me = this;

		this.items = [{
			xtype : 'form',
			layout: 'column',
			id : 'addLogFrm',
			
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 150,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			fileUpload:true,
			isUpload: true,
			enctype:'multipart/form-data',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				items:[{
					xtype : 'hidden',
					name : 'id',
					id : 'err_id',
					submitValue: true
				},{
					xtype : 'hidden',
					name : 'client_id',
					id : 'client_id',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Log Date <span style="color:red">*</span>',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					name : 'log_date',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Error ID <span style="color:red">*</span>',
					value : 'ERR'+Ext.Date.format(new Date(), 'yhis'),
					name : 'error_id',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Clients <span style="color:red">*</span>',
					name: 'Client',
					width: 610,
				},{
					xtype : 'displayfield',
					fieldLabel: 'Error Category <span style="color:red">*</span>',
					name: 'cat_name',
					width: 610,
				},{
					xtype : 'displayfield',
					fieldLabel: 'Error SubCategory <span style="color:red">*</span>',
					name: 'subcat_name',
					width: 610,
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Work Order <span style="color:red">*</span>',
					name: 'wor_name',
					width: 610,
				},{
					xtype : 'displayfield',
					fieldLabel: 'Associate Manager <span style="color:red">*</span>',
					name: 'associate_manager_name',
					width: 610,
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Team Lead',
					name: 'team_lead_name',
					width: 610,
				},{
					xtype : 'displayfield',
					fieldLabel: 'Production Member(s) <span style="color:red">*</span>',
					name: 'responsiblePerson',
					width: 610,
				}, {
					xtype : 'displayfield',
					fieldLabel: 'QA Member(s)',
					name: 'responsibleQA',
					width: 610,
				},{
					xtype : 'displayfield',
					name : 'error_date',
					fieldLabel : 'Error Date <span style="color:red">*</span>',
					width: 280,
				}, {
					xtype : 'displayfield',
					name : 'error_notified_date',
					fieldLabel : 'Error Notified Date <span style="color:red">*</span>',
					width: 280,
				}, {
					xtype : 'displayfield',
					name : 'problem_summary',
					fieldLabel : 'Error Statement <span style="color:red">*</span>',
					width: 610,
					height: 20,
					overflowY: 'scroll', 
					autoScroll: true
				}, {
					xtype : 'displayfield',								
					fieldLabel : '# of units impacted <span style="color:red">*</span>',
					name : 'units_impacted',
					width: 300,
				}, {
					xtype : 'displayfield',
					name : 'impact',
					fieldLabel : 'Error Summary <span style="color:red">*</span>',
					width: 610,
					height: 'auto'
				}, {
					xtype : 'displayfield',
					name : 'missed_tat',
					fieldLabel : 'Missed TAT <span style="color:red">*</span>',
				}]
			} , {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'Impact - Business <span style="color:red">*</span>',
					name      : 'impact_business',
				}, {
					xtype : 'displayfield',								
					fieldLabel : 'Impact - Revenue ($) <span style="color:red">*</span>',
					name : 'impact_revenue',
					width: 300,
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Impact - Relationship <span style="color:red">*</span>',
					name      : 'impact_relationship',
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Root Cause <span style="color:red">*</span>',
					name: 'rootcause_id',
					width: 630,
					renderer: function(value) {
						var rec	= Ext.getStore('newRootCause').findRecord('RootCauseID', value);
						return rec == null ? value : rec.get('Description');
					}
				}, {
					xtype : 'displayfield',
					name : 'rootcause_analysis',
					fieldLabel : 'Root Cause Analysis <span style="color:red">*</span>',
					width: 630,
					height: 'auto'
				}, {
					xtype : 'displayfield',
					name : 'correction',
					fieldLabel : 'Correction <span style="color:red">*</span>',
					width: 630,
					height: 'auto'
				},{
					xtype : 'displayfield',
					name : 'correction_date',
					fieldLabel : 'Correction Date <span style="color:red">*</span>',
					width: 280,
				}, {
					xtype : 'displayfield',
					name : 'corrective_action_date',
					fieldLabel : 'Corrective Action Date <span style="color:red">*</span>',
					width: 280,
				}, {
					xtype : 'displayfield',
					name : 'corrective_action',
					fieldLabel : 'Corrective Action <span style="color:red">*</span>',
					width: 630,
					height: 'auto'
				},{
					xtype		: 'hiddenfield',
					name : 'errattach_ori',
					id:"errattach_ori"
				},{
				xtype : 'fieldcontainer',
				fieldLabel : 'File',
				flex: 1,
				layout: 'hbox',
				id: 'attachErrFile',
				items:[{
					xtype: 'displayfield',
					name:"errattach",
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'10px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/download.png',
					tooltip : 'Download',
					listeners : {
						click: function(){													
							AM.app.getController('newIncidentLog').errfileDownload(Ext.getCmp('errattach_ori').getValue());				
						}
					}
				}/*,{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'6px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/delete.png',
					tooltip : 'Delete',
					id   : 'delete_apr',
					listeners : {
					click: function(){													
						AM.app.getController('newIncidentLog').errfileDelete(Ext.getCmp('err_id').getValue());				
					},
					afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
				}
				}*/]
			}],
			}]
		}];
		
		this.buttons = [{
			xtype: 'button',
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			xtype: 'button',
			text : 'Approve',
			id   : 'approve',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			listeners : {
				click: function(){													
					AM.app.getController('newIncidentLog').reviewApproveError(Ext.getCmp('err_id').getValue(), 'approve', Ext.getCmp('client_id').getValue());				
				}
			}	
		}, {
			xtype: 'button',
			text : 'Review',
			id   : 'review',
			icon : AM.app.globals.uiPath+'resources/images/save.png',									
			listeners : {
				click: function(){													
					AM.app.getController('newIncidentLog').reviewApproveError(Ext.getCmp('err_id').getValue(), 'review', Ext.getCmp('client_id').getValue());				
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>3)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		}];
		
		this.callParent(arguments);
	},
	
	onCancel : function() {
		AM.app.getController('newIncidentLog').viewContent('error');
	},
});