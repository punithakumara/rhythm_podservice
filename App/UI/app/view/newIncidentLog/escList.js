Ext.define('AM.view.newIncidentLog.escList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.logList',	
	title: 'Escalation List',    
	store : 'newEscLog',
	layout : 'fit',
	viewConfig: {
		forceFit: true
	},
	frame: true,
	width: "99%",
	height: 'auto',
	style: {
		border: "0px solid #157fcc",
		borderRadius:'0px'	      
	},
	minHeight : 200,
	loadMask: true,
	disableSelection: false,
	stripeRows: false,
	remoteFilter:true,
	border : true,
	sdfds: '',
	initComponent : function() {

		var me = this;
		
		this.tools = [{
			xtype : 'combobox',
			multiSelect: false, 
			allowBlank: true,
			store: Ext.getStore('ResponsiblePerson'),
			queryMode: 'remote',
			minChars:0,
			id: 'respEmp_id',
			displayField: 'emp_name',
			valueField: 'employee_id',
			emptyText: 'Select Responsible Person',
			width: 200,
			listeners: {
				change: function(combo) 
				{
					var resp_emp_id = combo.getValue();
					var MyStore = "";
					MyStore = Ext.getStore('newEscLog').load({
						params:{'resp_emp_id':resp_emp_id,'text':Ext.getCmp('gridTrigger').getValue()}
					})
				},
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'textfield',
			itemId : 'gridTrigger',
			id : 'gridTrigger',
			labelCls : 'searchLabel',
			width : 250,
			emptyText : 'Search',
			listeners: {
				change: function(obj){
					Ext.getStore('newEscLog').load({
						params:{'resp_emp_id':Ext.getCmp('respEmp_id').getValue(),'text':obj.value}
					})
				}
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			id: 'incidentLogButton',
			handler: function () {
				AM.app.getController('newIncidentLog').onCreate('esc');
			},
			listeners : {
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
			text : 'Add Escalation'
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		},{
			xtype: "button",
			icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			text : 'Export To Excel',
			style: {
				"margin-right": "8px"
			},
			handler: function(obj){
				AM.app.getController('newIncidentLog').exportEscalation();
			}
		}];
		
		this.columns = [{
			header : 'Escalation ID', 
			width    : '39%',
			dataIndex : 'error_id',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 2
		}, {
			header : 'Escalation Date', 
			dataIndex : 'escalation_date', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.5
		}, {
			header : 'Client', 
			dataIndex : 'client', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.5
		},{
			header : 'Responsible Person', 
			dataIndex : 'production_member_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			sortable: false,
			flex: 1.5
		},{
			header : 'Creator', 
			dataIndex : 'created_by', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		},{
			header : 'Reviewer', 
			dataIndex : 'reviewed_by',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			//sortable: false,
			flex: 1
		},{
			header : 'Approver', 
			dataIndex : 'approved_by', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			//sortable: false,
			flex: 1
		},{
			header : 'Action', 
			width : '10%',
			xtype : 'actioncolumn',
			align: 'center',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,			
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').onEditEscalationLog(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {

					if(Ext.util.Cookies.get('grade')<3)
					{
						return 'x-hide-display';
					}
					else if(record.data.reviewed_by != null && record.data.approved_by != null && record.data.reviewed_by != "" && record.data.reviewed_by !=" " && record.data.approved_by != "" && record.data.approved_by != " ")
					{
						return 'x-hide-display';
					}
					else
					{ 
						return 'rowVisible';
					}
				}
			},  '-', {
				icon : AM.app.globals.uiPath+'resources/images/copy.png',
				tooltip : 'Copy',
				id : 'copyError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnCopyEscalation(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {
					
					if(Ext.util.Cookies.get('grade')>2)
					{
						return 'rowVisible';
					}
					else
					{ 
						return 'x-hide-display';
					}
				}
			},'-',{
				icon : AM.app.globals.uiPath+'resources/images/view.png',
				tooltip : 'View',
				id : 'viewError',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnViewEscalation(grid, rowIndex, colIndex);
				},
				getTip: function(value, meta, record){
					if(record.data.reviewed_by != null && record.data.approved_by == null && Ext.util.Cookies.get('grade')>2)
					{
						return 'Approve';
					}
					if(record.data.reviewed_by != null && record.data.approved_by != null)
					{
						return 'View';
					}					
					if(record.data.reviewed_by == null && record.data.approved_by == null && Ext.util.Cookies.get('grade')>2)
					{
						return 'Review / Approve';
					}
					if(record.data.reviewed_by == null && record.data.approved_by == null && Ext.util.Cookies.get('grade')<3)
					{
						return 'View';
					}
				}
			},'-',{
				icon : AM.app.globals.uiPath+'resources/images/delete.png',
				getClass: function(value, meta, record) {
					if(Ext.util.Cookies.get('grade')<4)
					{
						return 'x-hide-visibility';
					}
					if(record.data.reviewed_by != null && record.data.approved_by != null && record.data.reviewed_by != "" && record.data.reviewed_by !=" " && record.data.approved_by != "" && record.data.approved_by != " ")
					{
						return 'x-hide-display';
					}
					else
					{ 
						return 'rowVisible';
					}
				},
				tooltip : 'Delete',
				id : 'delOpt',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('newIncidentLog').OnDeleteEscalation(grid, rowIndex, colIndex);
				},
			}]
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
			store: this.store,
			displayInfo: true,
			pageSize: AM.app.globals.itemsPerPage,
			displayMsg: 'Displaying {0} - {1} of {2}',
			emptyMsg: "No escalation to display.",
			plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
		});
		
		this.callParent(arguments);
	},
});