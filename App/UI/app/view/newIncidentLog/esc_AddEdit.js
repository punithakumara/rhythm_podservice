Ext.define('AM.view.newIncidentLog.esc_AddEdit',{
	extend : 'Ext.panel.Panel',
	layout : 'fit',
	width : '99%',
	layout : 'fit',
	id : 'add_edit_escalation',
	title: 'Add Escalation Details',
	border : 'true',
	style: {
		'margin-bottom':'35px'
	},
	initComponent : function() {
		var me = this;

		this.items = [{
			xtype : 'form',
			layout: 'column',
			itemId : 'addLogFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 150,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				items:[{
					xtype : 'hidden',
					name : 'id',
					value: '',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Log Date <span style="color:red">*</span>',
					name: 'log_date',
					id: 'log_date',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					readOnly : true,
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Escalation ID <span style="color:red">*</span>',
					name: 'error_id',
					id:'error_id',
					value : 'ESC'+Ext.Date.format(new Date(), 'yhis'),
					readOnly : true,
					submitValue: true
				}, {
					xtype : 'boxselect',
					multiSelect: false, 
					fieldLabel: 'Clients <span style="color:red">*</span>',
					allowBlank: false,
					name: 'client',
					store: 'Clients',
					queryMode: 'remote',
					minChars:0,
					displayField: 'client_name',
					valueField: 'client_id',
					id:'client',
					width: 600,
					emptyText: 'Select Client',
					value: '',
					listeners		: {
						change: function(obj){
							
							Ext.getStore('workOrder').load({
								params:{ 'comboClient': obj.getValue()}
							});
						},
				 		beforequery	: function(queryEvent) {
				 			AM.app.getStore('Clients').getProxy().extraParams = {
					 			'hasNoLimit'	:'1', 
					 			'filterName' 	: queryEvent.combo.displayField,
								'clientStatusCombo':'1',
					 		};
					 	},
						select : function() {
							Ext.getCmp('manager').enable();
							Ext.getCmp('production_member').enable();
							Ext.getCmp('wor_id').setDisabled(false);
							Ext.getCmp('wor_id').enable();	
							Ext.getCmp('wor_id').setValue('');
							
							Ext.getCmp('manager').setValue('');
							Ext.getCmp('production_member').setValue('');
						}
					 }
				}, {
					xtype : 'boxselect',
					multiSelect: false, 
					fieldLabel: 'Work Order <span style="color:red">*</span>',
					name: 'wor_id',
					id:'wor_id',
					store: 'workOrder',
					queryMode: 'remote',
					minChars:0,
					displayField: 'wor_name',
					valueField: 'wor_id',
					emptyText: 'Select Work Order',
					allowBlank: false,
					width: 600,
					disabled: true,
					value: '',
					listeners:{
						change: function(obj){
							
							var ClntID =   Ext.getCmp('client').getValue();
							Ext.getStore('Employees').load({
								params:{  'comboClient':ClntID}
							});
						},
						select : function() {
							Ext.getCmp('manager').enable();
							Ext.getCmp('production_member').enable();

							Ext.getCmp('manager').setValue('');
							Ext.getCmp('production_member').setValue('');
						},
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							var ClntID =  me.query('combobox')[0].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'filterName' : queryEvent.combo.displayField};
						}
					}
				},{
					xtype : 'boxselect',
					// multiSelect: true,
					fieldLabel: 'Managers <span style="color:red">*</span>',
					allowBlank: false,
					name: 'manager',
					store: 'Managers',
					id : 'manager',
					queryMode: 'remote',
					displayField: 'Name', 
					valueField: 'employee_id',
					multiSelect: false,
					emptyText: 'Select Manager',
					mode: 'local',
					minChars: 0,
					width: 600,
					value: '',
					listeners	: {
						beforequery	: function(queryEvent) {
						
							var wor_id =  me.query('combobox')[1].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								
								 'wor_id':wor_id,
								'utilCombo' : 1,
								'filterName'	: 	'employees.first_name'
							};
						},
						
					}
				}, {
					xtype : 'boxselect',
					multiSelect: true,
					fieldLabel: 'Employees <span style="color:red">*</span>',
					allowBlank: false,
					id: 'production_member',
					name: 'production_member[]',
					store: Ext.getStore('Employees'),
					mode: 'local',
					minChars: 0,
					width: 600,
					displayField: 'full_name', 
					valueField: 'employee_id',
					emptyText: 'Select Employees',
					value: '',
					listeners	: {
						beforequery	: function(queryEvent) {
							var ClntID =  me.query('combobox')[0].getValue();
							var wor_id =  me.query('combobox')[1].getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'comboClient':ClntID,
								 'wor_id':wor_id,
								'utilCombo' : 1,
								'filterName'	: 	'employees.first_name'
							};
						},
						afterrender: function() {
							Ext.getStore('Employees').getProxy().extraParams = {
								'hasNoLimit'	: "1", 
								'filterName'	: 'employees.first_name'
							};
						}
					}
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					grow : true,
					name : 'escalation_date',
					id : 'escalation_date',
					fieldLabel : 'Escalation Raised Date <span style="color:red">*</span>',
					width: 300,
					value : new Date(),
					maxValue : new Date(),
					allowBlank : false,
					listeners: {
						select: function() {
							Ext.getCmp("mitigation_plan_date").setMinValue(me.query('datefield')[0].getValue());
						}
					}
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'problem_summary',
					fieldLabel : 'Escalation Statement <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 600,
					height: 110,
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'impact',
					fieldLabel : 'Escalation Summary <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 600,
					height:110,
				}]
			} , {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '49%',
				},
				items:[{
					xtype : 'radiogroup',								
					// name : 'impact_business',
					fieldLabel : 'Impact - Business <span style="color:red">*</span>',
					id:'impact_business',
					layout: 'hbox',
					items: [{
						boxLabel  : 'Yes',
						name      : 'impact_business',
						width     : 70,
						inputValue: 'yes',
					}, {
						boxLabel  : 'No',
						name      : 'impact_business',
						inputValue: 'no',
						checked: true,
					}]
				}, {
					xtype : 'numberfield',								
					name : 'impact_revenue',
					fieldLabel : 'Impact - Revenue ($) <span style="color:red">*</span>',
					id:'impact_revenue',
					width: 300,
					minValue : 0,
					value: 0,
					step: 1,
					decimalPrecision : 2
				}, {
					xtype : 'radiogroup',								
					// name : 'impact_relationship',
					fieldLabel : 'Impact - Relationship <span style="color:red">*</span>',
					id:'impact_relationship',
					layout: 'hbox',
					items: [{
						boxLabel  : 'Yes',
						name      : 'impact_relationship',
						width	  : 70,
						inputValue: 'yes',
					}, {
						boxLabel  : 'No',
						name      : 'impact_relationship',
						inputValue: 'no',
						checked: true,
					}]
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					fieldLabel: 'Root Cause <span style="color:red">*</span>',
					allowBlank: false,
					name: 'rootcause_id',
					id: 'rootcause_id',
					store: 'newRootCause',
					mode: 'local',
					displayField: 'Description', 
					valueField: 'RootCauseID',
					minChars: 0,
					width: 600,
					emptyText: 'Select Root Cause',
					value: '',
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'rootcause_analysis',
					id : 'rootcause_analysis',
					fieldLabel : 'Root Cause Analysis <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 600,
					height:110,
				}, {
					xtype : 'textareafield',
					grow : false,
					name : 'mitigation_plan',
					fieldLabel : 'Mitigation Plan <span style="color:red">*</span>',
					allowBlank: false,
					autoScroll:true,
					width: 600,
					height:110,
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					grow : true,
					name : 'mitigation_plan_date',
					id : 'mitigation_plan_date',
					fieldLabel : 'Mitigation Plan Date <span style="color:red">*</span>',
					value : new Date(),
					minValue : new Date(),
					allowBlank : false,
					width: 300,
				}, {
					xtype		: 'hiddenfield',
					name		: 'attachment',
					id			: 'attachment'
				}, {
					xtype: 'filefield',
					name: 'escFile',
					fieldLabel: 'Attachments <br/>(Max file size 5 MB)',
					emptyText: 'Single attachment only.',
					msgTarget: 'side',
					buttonText: 'Browse..',
					listeners: {
                        change: function(fld, value) {
                            var newValue = value.replace(/C:\\fakepath\\/g, '');
							var file_ext = newValue.split('.').pop();
							var stringArray = ["doc","docx","pdf","xlsx","xls","pst","txt","rtf","csv","zip","msg"];

							if (stringArray.indexOf(file_ext) >= 0) 
							{
								fld.setRawValue(newValue);
							}
							else
							{
								Ext.Msg.alert("Alert", "Allowed file types are doc, docx, pdf, xlsx, xls, pst, txt, rtf, csv, zip, msg ");
								fld.setRawValue("");
							}
                        }
                    }
				},{
					xtype : 'hiddenfield',
					name : 'escattach_ori',
					id:"escattach_ori"
				},{
				xtype : 'hiddenfield',
				name : 'attachid',
				id:"attachid"
			},  {
				xtype : 'fieldcontainer',
				fieldLabel : 'File',
				flex: 1,
				layout: 'hbox',
				id: 'attachEscFile',
				items:[{
					xtype: 'displayfield',
					name:"escattach",
				},{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'10px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/download.png',
					tooltip : 'Download',
					listeners : {
						click: function(){													
							AM.app.getController('newIncidentLog').escfileDownload(Ext.getCmp('escattach_ori').getValue());				
						}
					}
				},
				{
					xtype: 'button',
					style: {
						'background': 'white',
						'border': 'none',
						'float':'left',
						'margin-left':'6px',
						'margin-top':'2px'
					},
					icon : AM.app.globals.uiPath+'resources/images/delete.png',
					tooltip : 'Delete',
					id   : 'delete_esc',
					listeners : {
					/*click: function(){													
						AM.app.getController('newIncidentLog').aprfileDelete(Ext.getCmp('id').getValue());				
					},*/
					click : function() {
						Ext.MessageBox.show({
						title: "Delete",
						msg: "Are you sure want to delete this file?",
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.MessageBox.YESNO,
						fn: function(buttonId) {
					if (buttonId === "yes") {
					var atachid=Ext.getCmp('error_id').getValue();
					Ext.getCmp('attachid').setValue(atachid);
					Ext.getCmp('attachEscFile').hide();
					
				
					}
			}
		});
					},
					afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
				}
				}]
			}]
			}]
		}];
		
		this.buttons = [{
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel		
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			// handler : this.onSave
			handler : function() {
				var pnl = this.up('panel');
				var form = pnl.down('form').getForm();
				
				if (form.isValid())
				{
					form.submit({
						url: AM.app.globals.appPath+'index.php/newIncidentLog/log_addEsc/?v='+AM.app.globals.version,
						headers : {'Content-Type':'multipart/form-data; charset=UTF-8'},
                        waitMsg: 'Uploading your data...',
						method: 'POST',
                        success: function(batch, o) 
						{
							Ext.MessageBox.show({
								title: "Success",
								msg: "Escalation Log Added Successfully.",
								buttons: Ext.Msg.OK
							});
							AM.app.getController('newIncidentLog').viewContent('esc');
                        },
                        failure: function(response) {
                        	Ext.MessageBox.show({
								title: "Alert",
								msg: "File size exceeded",
								buttons: Ext.Msg.OK
							});
                        }
                    });
				}
				// AM.app.getController('newIncidentLog').onSaveErrorLog(form);
			}
		}];
		
		this.callParent(arguments);
	},
	
	onCancel : function() {
		AM.app.getController('newIncidentLog').viewContent('esc');
	},
});