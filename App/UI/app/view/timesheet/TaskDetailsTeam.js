Ext.define('AM.view.timesheet.TaskDetailsTeam' ,{
    extend: 'Ext.Panel',
    alias: 'widget.TaskDetailsTeam',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me = this;
		this.activeItem  = 0;
		Ext.apply(me, {
			store : Ext.create('AM.store.TaskDetailsTeam',{
				remoteSort: true,
				autoLoad: true,
			})
        });
		
		this.items =[{
			xtype: 'grid',
			autoScroll: true,
			store : me.store,
			id: 'taskDetailsTeam',
			columns : [{
				header: 'Task', 
				dataIndex: 'task', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.6
			}, {
				header: 'Sub Task', 
				dataIndex: 'subtask', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5
			},{
				header: 'Hours', 
				dataIndex: 'hours', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 0.7,
				renderer: function(value, meta, record, row, col) {
					if(value == '' || value < 0 )
					{
						return '0';
					}
					else
					{
						var sec_num = parseInt(value, 10);
						var hours   = Math.floor(sec_num / 3600);
						var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
						var seconds = sec_num - (hours * 3600) - (minutes * 60);

						if (hours   < 10) {hours   = "0"+hours;}
						if (minutes < 10) {minutes = "0"+minutes;}
						if (seconds < 10) {seconds = "0"+seconds;}
						return hours+':'+minutes ;
						
					}
				}
			}],
        },{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }]
		
        this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
					obj.getLayout().setActiveItem(1); //made 0 @refrence RHYT-844 issue
				else
					obj.getLayout().setActiveItem(0);
			}); 
		}
	}
});