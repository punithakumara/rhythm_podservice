Ext.define('AM.view.timesheet.approvelist' ,{
	extend: 'Ext.grid.Panel',
	alias: 'widget.approvelist',
	requires : ['Ext.ux.grid.plugin.PagingSelectionPersistence'],
	store: 'ApproveTimesheet',
	layout : 'fit',
	id: "TimesheetApproveGridID",
	itemId: "TimesheetApproveGridID",
	border : true,
	height: 500,
	width:'99%',
	title: "Approve Timesheet",
	features: [{
		ftype : 'summary',
		groupHeaderTpl : '{name}',
		hideGroupedHeader : false,
		enableGroupingMenu : false,
		collapsible: false
	}],
	selType: 'checkboxmodel',
	selModel: {
		checkOnly: true,
		renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
			var html = '<div class = "' + Ext.baseCSSPrefix + 'grid-row-checker"></div>';
            if (record.get('Task') == 'Production Work') { // some judgment
            	html = '';
            }
            return html;
        }
    },
    plugins : [{ ptype : 'pagingselectpersist' }],

    initComponent: function() {
    	var me = this;
    	var timesheet = "";
    	var timesheetClient = "";
    	var combobox=0;

    	var config = {
    		columns:[],
    		rowNumberer: false,
    	};

		// appy to this config
		Ext.apply(me, config);

		// apply to the initialConfig
		Ext.apply(me.initialConfig, config);
		

		this.tools = [{
			xtype: 'combobox',
			store: Ext.create('Ext.data.Store', {
				fields: ['Flag', 'Status'],
				data: [
				{'Flag': 'All', 'Status': 'All'},
				{'Flag': '1', 'Status': 'Pending'},
				{'Flag': '2', 'Status': 'Approved'},
				{'Flag': '0', 'Status': 'Rejected'},
				]
			}),
			fieldLabel: 'Status',
			labelCls : 'searchLabel',
			displayField: 'Status', 
			valueField: 'Flag',
			id: 'timesheetStaus',
			width: 200,
			value:'All',
			listeners: {
				select: function(combo){
					combobox = combo.getValue();
					AM.app.getController('Timesheet').listApproveRecords(Ext.getCmp('TimesheetApproveGridID'), combo.getValue(), Ext.getCmp('timeWeekRange').getValue(), Ext.getCmp('chk_group').getValue());
				}
			}
		},{
			xtype: 'combobox',
			store: 'TimesheetWeekrange',
			fieldLabel: 'For Week',
			labelCls : 'searchLabel',
			displayField: 'week', 
			valueField: 'id',
			id: 'timeWeekRange',
			width: 380,
			emptyText: 'Select Week',
			listeners: {
				change: function(combo){
					combobox = combo.getValue();
					AM.app.getController('Timesheet').listApproveRecords(Ext.getCmp('TimesheetApproveGridID'), Ext.getCmp('timesheetStaus').getValue(), combo.getValue(), Ext.getCmp('chk_group').getValue());
				}
			}
		},{ 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '15px'
			}
		},{
			xtype : 'trigger',
			itemId : 'timeGridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search Text',
			//width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		}];
		
		this.tbar = [{
			xtype: "button",
			text: 'Reject',
			id: 'rejectTime',
			disabled: true,
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : function() {
				var selection = this.up('grid').getSelectionModel().selected;
				var status = [];
				var recordData = selection.items;
				
				for(var i = 0;i < recordData.length;i++) 
				{
					status.push(recordData[i].data.Status);
				}
				
				if(selection.items.length < 1)
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: "Select a record before Reject. !!!",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
					
				}
				else if(status.indexOf("Rejected") >= 0)
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: "One or more time entries are already rejected. Please deselect those entries.",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
				else 
				{
					var win = new Ext.Window({
						modal       : true,
						height      : 250,
						width       : 350,
						border      : false,
						resizable   : false,
						maximizable : false,
						draggable   : true,
						closable    : true,
						closeAction : 'destroy',
						title       : 'Provide Reason for Rejection',  
						autoScroll  : true,
						xtype : 'form',
						fieldDefaults: {
							labelAlign: 'left',
							labelWidth: 140,
							anchor: '100%'
						},
						bodyStyle : {
							border : 'none',
							display : 'block',
							float : 'left',
							padding : '5px',
						},
						items: [{
							xtype: 'textareafield',
							id :  'rejectcomments',
							height : 158,
							width : 325,
							allowBlank: false,
							value:''
						}],
						buttons: [{
							text     : 'Submit',
							icon : AM.app.globals.uiPath+'resources/images/save.png',
							onClick : function (obj) {
								if (Ext.getCmp('rejectcomments').getValue()!="")
								{
									AM.app.getController('Timesheet').onApprove(me, Ext.getCmp('TimesheetApproveGridID'), 0, Ext.getCmp('rejectcomments').getValue());
									win.close();
								}
							}                
						}]
					}).show();
				}
			}
		},{
			xtype: "button",
			text: 'Approve',
			id: 'approveTime',
			disabled: true,
			icon : AM.app.globals.uiPath+'resources/images/assign.png',
			handler : function() {	
				var selection = this.up('grid').getSelectionModel().selected;
				var status = [];
				var recordData = selection.items;
				for(var i = 0;i < recordData.length;i++) 
				{
					if(recordData[i].data.Task != "Production Work")
					{
						status.push(recordData[i].data.Status);
					}
				}
				console.log(status);
				
				if(selection.items.length < 1) 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: "Select a record before Approve. !!!",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
				else if(status.indexOf("Approved") >= 0)
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: "One or more time entries are already approved. Please deselect those entries.",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
				else 
				{
					AM.app.getController('Timesheet').onApprove(me, this.up('grid'), 2, "");
				}
			}
		},{
			xtype: 'panel',
			width: 330,
			style:{
				'margin-left': '880px',
			},
			items: [{
				xtype: 'checkboxgroup',
				id: 'chk_group',
				defaults: {
					name: 'chk_group',
					listeners: {
						scope: this,
						change: function(chkbox) {
							if (chkbox.checked) {
								this.resetBoxes(chkbox.ownerCt, chkbox.inputValue);
								var level_members = chkbox.inputValue;
							}
							else
							{
								Ext.getCmp('chk_group1').enable();
								Ext.getCmp('chk_group2').enable();
								var level_members = 'default';
							}
							AM.app.getController('Timesheet').listApproveRecords(Ext.getCmp('TimesheetApproveGridID'), Ext.getCmp('timesheetStaus').getValue(), Ext.getCmp('timeWeekRange').getValue(),level_members);
						}
					}
				},
				width : '99%',
				items: [{
					boxLabel: '2nd Level Members',				
					id: 'chk_group1',
					inputValue: 'second'
				}, {
					boxLabel: 'All Members',
					id: 'chk_group2',
					inputValue: 'all'
				}],
			}]
		}];

		this.bbar = Ext.create('Ext.toolbar.Paging', {
			store		: this.store,
            displayInfo	: true,
            cls: "peopleTool",
			prependButtons: false,
            displayMsg: "Total {2} Records",
            emptyMsg	: "No data to display",
        } );
		
		this.callParent(arguments);
		//this.plugins
	},
	
	resetBoxes:function(group, val) {
		if(val=="second")
		{
			Ext.getCmp('chk_group2').reset();
			Ext.getCmp('chk_group2').disable();
		}
		if(val=="all")
		{
			Ext.getCmp('chk_group1').reset();
			Ext.getCmp('chk_group1').disable();
		}
	},

	/**
    * When the store is loading then reconfigure the column model of the grid
    */
    storeLoad: function(store,me)
    {
        /**
        * JSON data returned from server has the column definitions
        */
        if(typeof(store.proxy.reader.jsonData.columns) == 'object') 
        {
        	var columns = [];

			/**
            * assign new columns from the json data columns
            */
            Ext.each(store.proxy.reader.jsonData.columns, function(column) {

            	if(column.dataIndex!="EmployeeName" && column.dataIndex!="ClientName" && column.dataIndex!="Task" && column.dataIndex!="SubTask"  && column.dataIndex!="Status" )
            	{
            		var field = column.dataIndex;
            		var sumType = function(records, values)
            		{
            			var hour = 0, minute = 0, total="";
            			for (var i=0; i < records.length; ++i) 
            			{
            				var time = records[i].get(field);
            				if(time!="")
            				{
            					var splitTime= time.split(':');

            					var splitHour = (splitTime[0]!="") ? parseInt(splitTime[0]) : 0;
            					var splitMinute = (splitTime[1]!="") ? parseInt(splitTime[1]) : 0;

            					if(splitHour!="0")
            					{
            						hour += splitHour;
            					}
            					if(splitMinute!="0")
            					{
            						minute += splitMinute;
            					}
            				}
            			}

            			if (minute >= 60) 
            			{
            				var h = Math.floor(minute / 60);
            				hour += h;
            				minute = minute%60;
            			}

            			total = ("0" + hour).slice(-2)+':'+("0" + minute).slice(-2);
            			total = (total!='00:00') ? ('<span style="font-size:18px;">'+total+'</span>') : '';

            			return total;
            		}

            		column.summaryType = sumType;
            	}

            	columns.push(column);
            });

            columns.push({
            	header : 'Action', 
            	xtype: 'actioncolumn',
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	items: [{
            		icon : AM.app.globals.uiPath+'/resources/images/view.png',
            		tooltip : 'Comments',
            		handler : this.onView,
            		getClass: function(v, meta, record) {

            			if(record.data.Task=="Production Work" || record.data.timesheetID == undefined || record.data.timesheetID == '0a')
            			{
            				return 'x-hide-display';
            			}
            			else
            			{ 
            				return 'rowVisible';
            			}
            		}
            	}]
            });

            /**
            * reconfigure the column model of the grid
            */
            me.reconfigure(store, columns);
        }
    },	

    onView : function(grid, rowIndex, colIndex) {
    	AM.app.getController('Timesheet').approveTimesheetComments(grid, rowIndex, colIndex);
    },

    listeners: {
    	afterrender: function(obj) {
    		var dickItems = obj.getDockedItems();
    		var weekCombo = dickItems[0].items.items[2];

    		weekCombo.store.load({
    			callback: function(records)
    			{
    				weekCombo.setValue(records[2].get('id'));
    			}
    		});
    	}
    },

}); 