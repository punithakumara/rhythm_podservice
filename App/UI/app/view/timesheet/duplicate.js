Ext.define('AM.view.timesheet.duplicate',{
	extend : 'Ext.window.Window',
	alias : 'widget.timesheetDuplicate',
	id: 'comments_add_edit',
	layout: 'fit',
	title: 'Duplicate',
	border : 'true',
	width : 400,
	height: 130,
	autoShow : true,
	modal: true,
	autoScroll : true,
	resizable:false,
	
	initComponent : function() {
		
		this.items = [{
			xtype : 'form',
			id: 'utilizationForm',
			fieldDefaults: {
				labelAlign: 'left',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px 10px 6px 10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype : 'hidden',
				id:'duptimesheetID',
				name : 'timesheetID',
			},{
				xtype: 'boxselect',
				store: Ext.getStore('TimesheetFutWeekrange'),
				fieldLabel: 'Duplicate To <span style="color:red">*</span>',
				displayField: 'week', 
				valueField: 'id',
				allowBlank : false,
				name:'duplicateTo',
				forceSelection: true,
				multiSelect: false,
				id:'duplicateTo',
				width: 380,
				emptyText: 'Select Week',
			}]
		}];
		
		this.buttons = ['->', {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : function(obj) {
				var win  = this.up('window');
				var form  = win.down('form').getForm();
				
				var	values = form.getValues();
				
				if (form.isValid()) 
				{
					obj.setDisabled(true);
					form.submit({
						url: AM.app.globals.appPath+'index.php/timesheet/duplicateRow/?v='+AM.app.globals.version,
						params:{
							'teamMember':Ext.getCmp('teamMember').getValue(),
						},
						method: 'POST',
						success: function(form,o) {
							var retrData = JSON.parse(o.response.responseText);
							
							Ext.MessageBox.show({
								title: "Success",
								msg: "Record Duplicated Successfully",
								buttons: Ext.Msg.OK,
								icon : 'success-icon',
								closable:false,
								fn: function(buttonId) {
									if (buttonId === "ok") 
									{
										Ext.getCmp('duptimesheetID').setValue();
										AM.app.getController('Timesheet').listRecords(Ext.getCmp('TimesheetGridID'), Ext.getCmp('duplicateTo').getValue());
										Ext.getCmp('timeWeekRange').setValue(Ext.getCmp('duplicateTo').getValue());
										Ext.getCmp('dupTimeRow').disable();
										win.close();
									}
								}
							});
						},
						failure: function(form,o) {
							var retrData = JSON.parse(o.response.responseText);
							
							Ext.MessageBox.show({
								title: "Warning",
								icon: Ext.MessageBox.WARNING,
								msg: retrData.msg,
								buttons: Ext.Msg.OK
							});
							win.close();
						}
					});
				}
			}
		}];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},

	
});