Ext.define('AM.view.timesheet.dashboard' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.dashboard',
	layout : {
		type: 'vbox',
		pack: 'center',
        align: 'stretch'		
	},
	scroll:true,
	width: '99%',
	
    initComponent: function() {
		var me = this;
		
		var firstdayOfmonth = new Date();
        firstdayOfmonth.setDate(1);
        firstdayOfmonth.setMonth(firstdayOfmonth.getMonth());
		
		this.items = [{
			xtype:'panel',			
			layout: {
				type: 'hbox',
				pack: 'end',
			},
			margin:'5px 0 10px 0',
			items:[{
				xtype: 'monthfield',
				submitFormat: 'Y-m-d',
				name: 'mbrmonth',
				id: 'timesheetMonth',
				value:firstdayOfmonth,
				editable : false,
				format: 'F, Y',
				width: 210,	
				style:{
					'margin-right':'20px'
				},
				listeners: {
					select:function (v){
						var date = new Date(Ext.getCmp('timesheetMonth').getValue()),
							mnth = ("0" + (date.getMonth()+1)).slice(-2),
							day  = "01";
						var sel = [ date.getFullYear(), mnth, day ].join("-");
						
						Ext.getCmp('taskDetailsSelf').getStore().load({params: {'timemonth':sel}});
						Ext.getCmp('selfPieChart').getStore().load({params: {'timemonth':sel}});
						
						Ext.getCmp('taskDetailsTeam').getStore().load({params: {'emp_id' : Ext.getCmp('TS_EmployID').getValue(), 'timemonth':sel}});
						Ext.getCmp('teamPieChart').getStore().load({params: {'emp_id' : Ext.getCmp('TS_EmployID').getValue(), 'timemonth':sel}});
					}
				}
			},{
				xtype:'button',
				text: 'Export To Excel',
				icon : AM.app.globals.uiPath+'resources/images/excel_Icon.png',
				style:{
					'margin-right':'2px'
				},
				handler : function() {
					/*var date = new Date(Ext.getCmp('timesheetMonth').getValue()),
						mnth = ("0" + (date.getMonth()+1)).slice(-2),
						day  = "01";
					var start_date = [ date.getFullYear(), mnth, day ].join("-");
					var end_date = [ date.getFullYear(), mnth, "31" ].join("-");
					
					AM.app.getController('Timesheet').onDownload(start_date, end_date);*/

					AM.app.getController('Timesheet').slectDateRange();
				}				
			}]
		},{
			xtype:'panel',
			id : 'selfPanel',
			title: 'My Data',
			border: true,			
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
			},
			// margin:5,
			items: [{
				xtype:'panel',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Monthly Details</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.timesheet.MonthlyDetailsSelf')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId2',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Task Details</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.timesheet.TaskDetailsSelf')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId3',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Timesheet Status</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.timesheet.selfBarPanel')
				}],
				flex:1
			}]
		},{
			xtype:'panel',
			id : 'teamPanel',
			title: 'Team Data',
			border: true,
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'	
			},
			// margin:5,
			style:{
				'margin-top': '20px'
			},
			items: [{
				xtype:'panel',
				// id : 'panelId4',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Monthly Details</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.timesheet.MonthlyDetailsTeam')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId5',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Task Details</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.timesheet.TaskDetailsTeam')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId6',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Timesheet Status</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.timesheet.teamBarPanel')
				}],
				flex:1
			}],
			tools:[{
				xtype : 'boxselect',
				multiSelect: false,
				width:300,
				labelCls : 'searchLabel',
				fieldLabel: 'Select Member',
				emptyText: 'Select Employee Name',
				id: 'TS_EmployID',
				name: 'EmployID',
				store: Ext.getStore('Employees'),
				mode: 'local',
				minChars: 1,
				displayField: 'full_name', 
				valueField: 'employee_id',
				listeners:{
					beforequery: function(queryEvent){
						queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1, 'filterName': 'employees.first_name'};
					},
					change: function(obj){
						var date = new Date(Ext.getCmp('timesheetMonth').getValue()),
							mnth = ("0" + (date.getMonth()+1)).slice(-2),
							day  = "01";
						var sel = [ date.getFullYear(), mnth, day ].join("-");
						
						Ext.getCmp('MonthlyDetailsTeam').getStore().load({params: {'emp_id' : obj.getValue(), 'timemonth':sel}});
						Ext.getCmp('taskDetailsTeam').getStore().load({params: {'emp_id' : obj.getValue(), 'timemonth':sel}});
						Ext.getCmp('teamPieChart').getStore().load({params: {'emp_id' : obj.getValue(), 'timemonth':sel}});
					}
				}	
			}]
		}];

		this.callParent(arguments);
    }
	
}); 