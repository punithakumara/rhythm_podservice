Ext.define('AM.view.timesheet.SelectDateRange',{
	extend : 'Ext.window.Window',
	alias : 'widget.timesheetSelectDateRange',
	id: 'select_date_range',
	layout: 'fit',
	title: 'Select Date Range',
	border : 'true',
	width : 370,
	height: 160,
	autoShow : true,
	modal: true,
	autoScroll : false,
	resizable:false,
	
	initComponent : function() {
		var me = this;
		
		var date = new Date(), 
		startMinDate = Ext.Date.add(date, Ext.Date.MONTH, -2); 
		today = Ext.Date.add(date);
		
		var attrStore = new Ext.data.SimpleStore({
			fields: ['attr_id', 'attr_name'],
			data: [
			['1', 'Static Page'],
			['2', 'Template'],
			]
		});

		this.items = [{
			xtype : 'form',
			layout: 'vbox',
			id: 'utilizationForm',
			fieldDefaults: {
				labelAlign: 'left',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px 10px 6px 10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			layout: 'vbox',
			items : [{
				xtype : 'datefield',
				format : AM.app.globals.CommonDateControl,
				fieldLabel : 'Start Date <span style="color:red">*</span>',
				name : 'start_date',
				maskRe: /[0-9\/]/,
				id : 'start_date',
				editable: false,
				allowBlank : false,
				minValue:'01/01/2018',
				maxValue:new Date(),
				value:new Date(date.getFullYear(), date.getMonth(), 1),
				width: 280,
				listeners: {
					select : function(combo) {
						me.query('datefield')[1].setMinValue(me.query('datefield')[0].getValue());
					}
				}
			},{
				xtype : 'datefield',
				format : AM.app.globals.CommonDateControl,
				fieldLabel : 'End Date <span style="color:red">*</span>',
				name : 'end_date',
				maskRe: /[0-9\/]/,
				id : 'end_date',
				allowBlank : false,
				editable: false,
				value:new Date(),
				minValue:new Date(date.getFullYear(), date.getMonth(), 1),
				maxValue:new Date(),
				width: 280,
				listeners: {
					select : function(combo) {
						me.query('datefield')[0].setMaxValue(me.query('datefield')[1].getValue());
					}
				}
			}],
			
			buttons : ['->', {
				xtype: 'displayfield',
				value : '<span style="color:red">* marked fields are mandatory</span>',
			},{
				text : 'Cancel',
				icon : AM.app.globals.uiPath+'resources/images/cancel.png',
				handler : this.onCancel
			}, {
				text : 'Submit',
				id:'saveButtonID',
				icon : AM.app.globals.uiPath+'resources/images/save.png',
				handler : this.onSave
			}]
		}];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},

	onSave :  function() {
		var start_date = Ext.getCmp('start_date').getValue();
		var end_date = Ext.getCmp('end_date').getValue();
		var form = this.up('form').getForm();

		if(form.isValid())
		{
        	var win = this.up('window');
			if( start_date != null && end_date != null)
			{
				start_date = Ext.Date.format(start_date, 'Y-m-d');
				end_date = Ext.Date.format(end_date, 'Y-m-d');

				AM.app.getController('Timesheet').onDownload(start_date, end_date, win);
			}
		}
	},
});