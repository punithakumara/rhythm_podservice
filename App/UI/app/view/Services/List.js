Ext.define('AM.view.services.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.servicesList',
	require : ['Ext.ux.grid.FiltersFeature'],
	title: 'Services List',
    store : 'Services',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
    width : '99%',
    minHeight : 200,
    height : 'auto',
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
	
    initComponent : function() {
    	
		this.columns = [{
			header : 'Name', 
			flex: 1,
			dataIndex : 'name',
			menuDisabled:true,
			groupable: false,
			draggable: false,
		}, {
			header : 'Description', 
			dataIndex : 'description', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Folder', 
			dataIndex : 'folder', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Action', 
			width : '5%',
			xtype : 'actioncolumn',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('Services').onEditServices(grid, rowIndex, colIndex);
				},
				getClass: function(v, meta, record) {

					if(Ext.util.Cookies.get('grade')<=5)
					{
						return 'x-hide-display';
					}
					else
					{ 
						return 'rowVisible';
					}
				}
			}, 
			'-',{
				icon : AM.app.globals.uiPath+'resources/images/delete.png',
				getClass: function(value, meta, record) {
					if(Ext.util.Cookies.get('grade') <= 6 || Ext.util.Cookies.get('grade') >= 8 )
					{
						return 'x-hide-visibility';
					}
					else if(Ext.util.Cookies.get('grade') > 6 && Ext.util.Cookies.get('grade') < 8 && Ext.util.Cookies.get('employee_id') == 3392)
					{
						return 'x-hide-visibility';
					}
					else
					{ 
						return 'rowVisible';
					}
				},
				tooltip : 'Delete',
				id : 'delOpt',
				handler : function(grid, rowIndex, colIndex) {
					AM.app.getController('Services').onDeleteServices(grid, rowIndex, colIndex);
				},
			}],
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Services to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.tools = [{
		xtype: 'combobox',
		itemId : 'servicesStatusCombo',
		store: Ext.create('Ext.data.Store', {
			fields: ['status', 'servicesStatus'],
			data : [{'status': '1', 'servicesStatus': 'Active'}, {'status': '0', 'servicesStatus': 'InActive'}]
		}),
		displayField: 'servicesStatus', 
		valueField: 'status',
		value: '1',
		listeners: {
			select: function(combo){
				this.fireEvent('servicesChangeStatusCombo', combo);
				Ext.getCmp('gridTrigger').reset();
			}
		}
		},{ 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    },{
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function()
			{
				this.reset();
				this.fireEvent('triggerClear');
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Add Service',
			handler: function () {
				AM.app.getController('Services').onCreate();
			},
			listeners : {
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=5)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
		}],

		this.callParent(arguments);
	},
	
	onDelete : function(grid, rowIndex, colIndex) 
	{
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('deleteServices', grid, rowIndex, colIndex);
	},
	
	onEdit : function(grid, rowIndex, colIndex) 
	{
		AM.app.getController('Services').onEditServices(grid, rowIndex, colIndex);
	}
});