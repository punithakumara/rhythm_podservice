Ext.define('AM.view.wor_cor.ListWor',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.worList',
	title: 'Work Order List',
    store: 'ClientWOR',
    id: 'wor_list',
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
    	this.columns = [ {
			header : 'Work Order ID',
			tooltip : 'Work Order ID',
			dataIndex : 'work_order_id',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.3
		}, {
			header : 'Work Order Name', 
			tooltip : 'Work Order Name',
			dataIndex : 'wor_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("wor_name") + '"';
				return value;
			}
		}, {
			header : 'Client Name', 
			tooltip : 'Client Name',
			dataIndex : 'client_name',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 2,
		}, {
			header : 'Work Order Type', 
			tooltip : 'Work Order Type', 
			dataIndex : 'wor_type', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("wor_type") + '"';
				return value;
			}
		}, {
			header : 'Start Date',
			tooltip : 'Start Date',
			dataIndex : 'start_date',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'End Date',
			tooltip : 'End Date',
			dataIndex : 'end_date',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Client Partner', 
			tooltip : 'Client Partner', 
			dataIndex : 'cp_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.5
		},{
			header : 'Engagement Manager', 
			tooltip : 'Engagement Manager', 
			dataIndex : 'em_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.5
		},{
			header : 'Delivery Manager', 
			tooltip : 'Delivery Manager', 
			dataIndex : 'dm_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1.5
		},  {
			header : 'Status', 
			tooltip : 'Status', 
			dataIndex : 'status',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 0.9
		},{
			header : 'Action', 
			width : '7%',
			xtype : 'actioncolumn',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			sortable: false,
			items : [ {
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'View',
				handler : this.onView
			},'-',{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
				getClass: function(v, meta, record) {
					
					if(record.data.status != 'Open' && record.data.status != 'Closed' && record.data.status != 'Deleted' && Ext.util.Cookies.get('grade')>=4)
				    {
						return 'rowVisible';
				    }
				    else
				    { 
						return 'x-hide-display';
				    }
				}
			}]
		}];
		
    	this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2} Records',
            emptyMsg: "No records to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

		this.callParent(arguments);
	},
	
	tools : [{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        id : 'gridTrigger',
        fieldLabel : '',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        //width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
		xtype: 'combobox',
		id:'WORStatusComboBox',
		store: Ext.create('Ext.data.Store', {
			fields: ['Flag', 'Status'],
			data: [
				{'Flag': 'All', 'Status': 'All'},
				{'Flag': 'In-Progress', 'Status': 'In-Progress'},
				{'Flag': 'Open', 'Status': 'Open'},
				{'Flag': 'Closed', 'Status': 'Closed'},
				{'Flag': 'Deleted', 'Status': 'Deleted'}
			]
		}),
		displayField: 'Status', 
		valueField: 'Flag',
		value:'All',
		listeners: {
			select: function(combo){
				AM.app.getController('ClientWOR').onStatusChange(combo);
			}
		}
	}, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }, {
		xtype : 'button',
		name:'addwor',
		id:'addwor',
		icon: AM.app.globals.uiPath+'/resources/images/Add.png',
		handler: function () {
			AM.app.getController('ClientWOR').onCreateWOR();
	    },
		text : 'Add WOR'
	}],
		
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientWOR').onEditWOR(grid, rowIndex, colIndex);
	},

	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientWOR').onViewWOR(grid, rowIndex, colIndex);
	},
	
});