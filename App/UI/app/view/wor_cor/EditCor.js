Ext.define('AM.view.wor_cor.EditCor',{
	extend : 'Ext.form.Panel',
	alias  : 'widget.EditCor',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	title: 'Edit Change Order Details',
	border : 'true',
	initComponent : function() {

    	me = this;
    	
    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addCORFrm',
			width: 1347,
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 141,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				float : 'left',
				padding : '8px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 650,
				title:"Change Order Information",
				items:[{
					xtype:'hidden',
					name:'cor_id',
					id:'cor_id',
				},{
					xtype:'hidden',
					name:'wor_id',
					id:'work_id',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Change Order ID <span style="color:red">*</span>',
					value : 'COR'+Ext.Date.format(new Date(), 'ymdhis'),
					name : 'change_order_id',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Order ID <span style="color:red">*</span>',
					name : 'work_order_id',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Order Name <span style="color:red">*</span>',
					id: 'previous_wor_name',
					name : 'wor_name',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel : 'Client Name <span style="color:red">*</span>',
					id: 'cor_client',
					name : 'client_name',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Order Type <span style="color:red">*</span>',
					id: 'previous_wor_type',
					name : 'wor_type',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel : 'WOR Start Date <span style="color:red">*</span>',
					name : 'wor_start_date',
					id : 'wor_start_date',
					renderer:function(value,displayfield) {
						return Ext.util.Format.date(value,"d-M-Y")
					}
				},{
					xtype : 'displayfield',
					fieldLabel : 'WOR End Date <span style="color:red">*</span>',
					name : 'wor_end_date',
					id : 'wor_end_date',
					renderer:function(value,displayfield) {
						return Ext.util.Format.date(value,"d-M-Y")
					}
				},{
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Change Request Date <span style="color:red">*</span>',
					anchor : '100%',
					name : 'change_request_date',
					minValue: AM.app.globals.cormindate,
					maskRe: /[0-9\/]/,
					id : 'change_request_date',
					allowBlank : false,
				},{
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Status',
					name:'status',
				    id:'editStatus',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue=='Delete')
							{
								Ext.getCmp('editCORDescription').setVisible(true);
								Ext.getCmp('editSave').setVisible(true);
								Ext.getCmp('myEditCorTabPanel').items.getAt(0).setDisabled(true);
								Ext.getCmp('myEditCorTabPanel').items.getAt(1).setDisabled(true);
								Ext.getCmp('myEditCorTabPanel').items.getAt(2).setDisabled(true);
								Ext.getCmp('myEditCorTabPanel').items.getAt(3).setDisabled(true);
							}
							else
							{
								Ext.getCmp('editCORDescription').setVisible(false);
								Ext.getCmp('editSave').setVisible(false);
								Ext.getCmp('myEditCorTabPanel').items.getAt(0).setDisabled(false);
								Ext.getCmp('myEditCorTabPanel').items.getAt(1).setDisabled(false);
								Ext.getCmp('myEditCorTabPanel').items.getAt(2).setDisabled(false);
								Ext.getCmp('myEditCorTabPanel').items.getAt(3).setDisabled(false);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					hidden : true,
					id : 'editCORDescription',
					name : 'description',
					allowBlank: true
				}]
			}, {
				xtype:'fieldset',
				padding: '10',
				width: 650,
				title:"People Information",
				items:[{
					xtype:'displayfield',
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					name: 'cp_name',
					id:'edit_cor_client_cp',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					name: 'em_name',
					id:'edit_cor_client_em',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					name: 'dm_name',
					id:'edit_cor_client_dm',
				},{
					xtype : 'displayfield',
					fieldLabel: 'Associate Manager',
					name: 'am_name',
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Team Lead',
					name: 'tl_name',
				},{
					xtype:'displayfield',
					fieldLabel: 'Stake Holder(s)',
					name: 'sh_name',			
				},{
					xtype:'hidden',
					name:'client',
					id:'cor_client_id',
					submitValue: true
				},{
					xtype:'hidden',
					name:'wor_type_id',
					id:'cor_wor_type_id',
				}, {
					xtype:'hidden',
					name:'client',
					id:'cor_client_id',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'client_partner',
					id:'cor_client_partner',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'engagement_manager',
					id:'cor_engagement_manager',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'delivery_manager',
					id:'cor_delivery_manager',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'stake_holder',
					id:'cor_stake_holder',
					submitValue: true
				},  {
					xtype:'hidden',
					name:'team_lead',
					id:'cor_team_lead',
				},  {
					xtype:'hidden',
					name:'associate_manager',
					id:'cor_associate_manager',
					submitValue: true
				}]
			}]
    	}, {
			xtype: 'displayfield',
			fieldLabel: 'Existing Details',
			labelCls: 'biggertext'
		}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1347,
			minHeight: 300,
			id: 'ExistCorTabPanel',
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				id: 'existFTE',
				items : [{								
					xtype:AM.app.getView('wor_cor.existFTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('wor_cor.existHourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('wor_cor.existVolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('wor_cor.existProjectList').create(),
					hidden:false
				}]
			}]
		}, {
			xtype: 'displayfield',
			fieldLabel: 'New Details',
			labelCls: 'biggertext'
		},  {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1347,
			height: 300,
			id: 'myEditCorTabPanel',
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				id: 'edit_FTE_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corFTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				id: 'edit_Hourly_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corHourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				id: 'edit_Volume_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corVolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				id: 'edit_Project_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
    		text : 'Save as Draft',
			id:'editSave',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
        }, {
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Submit',
			id:'editSubmit',
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				AM.app.getController('ClientCOR').initCOR(AM.app.globals.wor_cor_combostatus);
			}
		});
	},
	
	onSave :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) 
		{
			Ext.getCmp('editSave').setDisabled(true);
			AM.app.getController('ClientCOR').onSaveCOR(form, Ext.getCmp('editStatus').getValue(),AM.app.globals.redir)
		}
		else
		{
			Ext.Msg.alert('Alert', 'Some mandatory fields are left blank!!');
		}
	},

	onSubmit :  function() {
		var form = this.up('form').getForm();
		var worstatus = Ext.getCmp('editStatus').getValue();
		console.log(form);
		if(worstatus == "In-Progress")
		{
			var status = 'Open';
		}
		else
		{
			var status = Ext.getCmp('editStatus').getValue();
		}
		
		if (form.isValid()) 
		{
			Ext.getCmp('editSubmit').setDisabled(true);
			if(worstatus == "Delete")
			{
				AM.app.getController('ClientCOR').onDeleteCOR(form)
			}
			else
			{
				AM.app.getController('ClientCOR').onSaveCOR(form,status,AM.app.globals.redir)
			}
		}
		else
		{
			Ext.Msg.alert('Alert', 'Some mandatory fields are left blank!!');
		}
	},
});