Ext.define('AM.view.wor_cor.ViewCor',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	border : 'true',
    initComponent : function() {

    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addCORFrm',
			width: 1347,
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 141,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 650,
				title:"Change Order Information",
				items:[{
				    xtype:'hidden',
				    name:'cor_id',
				    id:'cor_id',
				}, /*{
					xtype : 'displayfield',
					fieldLabel : 'Change Order Creation Date <span style="color:red">*</span>',
					name : 'created_on',
				},*/ {
					xtype : 'displayfield',
					fieldLabel : 'Change Order ID <span style="color:red">*</span>',
					name : 'change_order_id',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Work Order ID <span style="color:red">*</span>',
					name: 'previous_work_order_id',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Work Order Name <span style="color:red">*</span>',
					name: 'wor_name',
				},{
					xtype : 'displayfield',
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'client_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Work Order Type <span style="color:red">*</span>',
					name: 'wor_type',
				},{
					xtype:'displayfield',
					fieldLabel: 'Service Type <span style="color:red">*</span>',
					name: 'name',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Start Date <span style="color:red">*</span>',
					name : 'wor_start_date',
					id : 'wor_start_date',
					renderer:function(value,displayfield) {
						return Ext.util.Format.date(value,"d-M-Y")
					}
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work End Date <span style="color:red">*</span>',
					name : 'wor_end_date',
					id : 'wor_end_date',
					renderer:function(value,displayfield) {
						return Ext.util.Format.date(value,"d-M-Y")
					}
				}, {
					xtype:'displayfield',
					fieldLabel: 'Status',
					name:'status',
					id: 'inCorStatus',
					hidden: true
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					hidden: true,
					fieldLabel: 'Status <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': 'Open', 'Status': 'Open'},
							{'Flag': 'Close', 'Status': 'Close'},
						]
					}),
					name:'status',
					id: 'corOpenStatus',
					displayField: 'Status',
					valueField: 'Flag',
					value : 'Open',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue=='Close')
							{
								Ext.getCmp('ViewCORDescription').setVisible(true);
								if(Ext.util.Cookies.get('grade') >=4)
								{
									Ext.getCmp('viewSaveButton').setVisible(true);
									Ext.getCmp('viewSaveButton').setText("Submit");
								}
							}
							else
							{
								Ext.getCmp('ViewCORDescription').setVisible(false);
								Ext.getCmp('viewSaveButton').setVisible(false);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					id : 'ViewCORDescription',
					name : 'description',
					allowBlank: false,
					hidden: true,
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Change Request Date <span style="color:red">*</span>',
					name : 'change_request_date'
				}]
			}, {
				xtype:'fieldset',
				padding: '10',
				width: 650,
				title:"People Information",
				items:[{
					xtype:'displayfield',
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					name: 'cp_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					name: 'em_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					name: 'dm_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Associate Manager',
					name: 'am_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Team Lead',
					name: 'tl_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Stake Holder(s)',
					name: 'sh_name',
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			id:'myCorTabPanel',
			activeTab: 0,
			width: 1347,
			height: 300,
			padding: '0 0 10px 0',
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			id:'viewSaveButton',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : function() {
				var win = this.up('panel');
				var form = win.down('form').getForm();
				var record = form.getRecord(),values = form.getValues();
				if (form.isValid()) 
				{
					Ext.getCmp('viewSaveButton').setDisabled(true);
					AM.app.getController('ClientCOR').saveStatus(form);
				}
			}
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				AM.app.getController('ClientCOR').initCOR(AM.app.globals.wor_cor_combostatus);
			}
		});
	},
});