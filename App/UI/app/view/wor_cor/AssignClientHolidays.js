Ext.define('AM.view.wor_cor.AssignClientHolidays',{
	extend : 'Ext.window.Window',
	alias : 'widget.AssignClientHolidays',
	requires: ['Ext.ux.MultiSelect'],
	layout: 'fit',
	layout: {
        type: 'vbox',
    },
	// store: 'Remarks',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
    closable: false,
	border : 'true',
	width: 520,
	height: 600,
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '5px',
	},
    initComponent : function() {

    	var me = this;
		var comp = Ext.Date.add(new Date(), Ext.Date.DAY, -1);
  	
    	this.items = [{
			xtype : 'form',
			itemId : 'assgnHolidayFrm',
			fieldDefaults: {
				labelWidth: 100,
				anchor: '100%',
				labelAlign: 'top'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			layout: 'hbox',
			items : [{
				xtype : 'hidden',
				id:'holiday_client_id',
				name : 'holiday_client_id',
			}, {
				xtype : 'hidden',
				id:'holiday_wor_id',
				name : 'holiday_wor_id',
			}, {
				xtype: 'multiselect',
                name: 'holidayProjectCode',
                store: 'roleCode',
                displayField: 'project_code',
                valueField: 'si_no',
				fieldLabel : 'Role Code<span style="color:red">*</span>',
				labelStyle: 'padding:0 0 5px 0',
				anchor: '100%',
				margin: '0 0 5px 0',
				width: 230,
				height: 160,
				allowBlank: false
			},{
				xtype: 'multiselect',
                name: 'holidayName',
                store: 'Holidays',
                displayField: 'name_date',
                valueField: 'holiday_id',
				fieldLabel : 'Holiday List <span style="color:red">*</span>',
				labelStyle: 'padding:0 0 5px 0',
				anchor: '100%',
				margin: '0 0 5px 20px',
				width: 230,
				height: 160,
				allowBlank: false
			}]
		},{
			layout: {
				type: 'hbox',
				pack: 'center',
			},
			width: 490,
			style:{
				margin: '0 0 15px 0',
			},
			items: [{
				xtype: 'tbfill'
			},{
				xtype: 'button',
				text : 'Cancel',
				icon : AM.app.globals.uiPath+'resources/images/cancel.png',
				handler : this.onCancel
			}, {
				xtype: 'button',
				text : 'Save',
				margin: '0 0 0 10px',
				icon : AM.app.globals.uiPath+'resources/images/save.png',
				handler : this.onSave
			}]
		},{
			xtype:'gridpanel',
			border:true,
			margin:'0',
			height:320,
			width:'99%',
			store: Ext.getStore('clientHolidayDetails'),
			id: 'holidays_list',
			features: [{
				id: 'group',
				ftype: 'groupingsummary',
				groupHeaderTpl: '{name}',
				hideGroupedHeader: true,
				enableGroupingMenu: false,
				startCollapsed: true,
			}],
			columns:[{
				header : 'Holiday',
				dataIndex : 'holiday',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
			}, {
				header : 'Project',
				dataIndex : 'project_code', 
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
			}, {
				header : 'Date',
				dataIndex : 'date',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
			},{
				header : 'Action', 
				width : '15%',
				xtype : 'actioncolumn',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				items : ['-', {
					icon : AM.app.globals.uiPath+'/resources/images/delete.png',
					tooltip : 'Delete',
					handler : this.onDelete,
					getClass: function(v, meta, record)
					{
						var d = Ext.Date.parse(record.data.date,'d-M-Y');
						
						if (d < comp)
						{
							return 'x-hide-display';
						}
						else
						{ 
							return 'rowVisible';
						}
					},
				}]
			}]
		}];
		
		this.callParent(arguments);
	},	
	
	onCancel : function(win) {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				win.up('window').close();
			}
		});
	},	
	
	onSave : function() {
		var win  = this.up('window');
		var form  = win.down('form').getForm();
		
		var	values = form.getValues();
		
		if(values.holidayProjectCode=="")
		{
			Ext.MessageBox.show({
				title: "Warning",
				msg: "Please select Role Code",
				icon: Ext.MessageBox.INFO,
				buttons: Ext.Msg.OK
			});
		}
		else if(values.holidayName=="")
		{
			Ext.MessageBox.show({
				title: "Warning",
				msg: "Please select Holiday",
				icon: Ext.MessageBox.INFO,
				buttons: Ext.Msg.OK
			});
		}
		else if (form.isValid())
		{
			form.submit({
				url: AM.app.globals.appPath+'index.php/Clientwor/client_holidays/?v='+AM.app.globals.version,
				method: 'POST',
				params:{
					"holidayProjectCode": values.holidayProjectCode,
					"holiday_id" 		: values.holidayName,
					"holiday_client_id" : values.holiday_client_id,
					"holiday_wor_id" 	: values.holiday_wor_id,
				},
				success: function(form,o) {
					var retrData = JSON.parse(o.response.responseText);
					Ext.MessageBox.show({
						title: "Success",
						msg: retrData.msg,
						buttons: Ext.Msg.OK,
					});
					
					Ext.getCmp('holidays_list').getStore().load({params: {'client_id' : values.holiday_client_id,'wor_id':values.holiday_wor_id}});
				},
				failure: function(response) {
					Ext.MessageBox.show({
						title: "Warnings",
						msg: "Error in adding Record",
						buttons: Ext.Msg.OK
					});
				}
			});
		}
	},

	onDelete : function(grid, rowIndex, colIndex, me) {
		var win  = this.up('window');
		var form  = win.down('form').getForm();
		
		var	values = form.getValues();
		
		AM.app.getController('ClientWOR').onDeleteHoliday(grid, rowIndex, values.holiday_client_id);
	},	
	
});