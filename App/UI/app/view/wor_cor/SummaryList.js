Ext.define('AM.view.wor_cor.SummaryList', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.SummaryList',
    width : '100%',
	minHeight: 250,
	layout: 'fit',
	frame: true,
	title: 'Summary Details',
	id : 'summary_list',
	store: 'summaryStore',
	emptyText: '<div align="center">No Data To Display</div>',
	features: [{
		id: 'group',
		ftype: 'groupingsummary',
		groupHeaderTpl: '{name}',
		hideGroupedHeader: true,
		enableGroupingMenu: false,
	}],
	initComponent : function() {
		this.columns = [{
			header: 'Project',
			dataIndex: 'project_type',
			width: 260,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			summaryType: 'count',
            summaryRenderer: function(value, summaryData, dataIndex) {
				return "<b>Total<b>"
            }
		}, {
			text: 'Roles',
			flex: 1,
			tdCls: 'task',
			sortable: false,
			menuDisabled:true,
			dataIndex: 'responsibilities',
			hideable: false,
			draggable: false,
			fixed: true,
		}, {
			header: 'Models',
			width: 180,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex: 'model'
		}, {
			header: '# of FTE',
			width: 100,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			fixed: true,
			dataIndex: 'fte',
			renderer: function(value, metaData, record, rowIdx, colIdx, store, view){//return 
				if(value > 0)
				{
					return value;
				}
				else 
				{
					return 0
				}	
			},
				
			summaryType: function(records, values) {
				
				var i = 0,
					length = records.length,
					total = 0,
					record;

				for (; i < length; ++i) {
					record = records[i];
					total += record.get('fte');
				}
				
				Ext.getCmp('fte_total').setValue(total);
				
				return total;
			},
			
		}, {
			text: 'Expected Duration',
			menuDisabled:true,
			draggable: false,
			fixed: true,
			columns:[{
				header: 'Minimum',
				width: 120,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex: 'min',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){//return 
					if(value == 'pro'){return ''; }
					
					if (value.match(/[a-z]/i)) 
					{
						return value;
					}
					else if(parseInt(value) > 0)
					{
						return value +' hrs';
					}
					else 
					{
						return 0
					}	
				},
				summaryType: function(records, values) 
				{
					var i = 0,
						length = records.length,
						total = 0,
						record;

					for (; i < length; ++i) 
					{
						record = records[i];
						if(record.get('model') != 'Project Details')
						{
							if(record.get('min') > 0)
							total += parseInt(record.get('min'));
						}
						else
						{
							total = 'pro';
						}
					}
					return total;
				},
			}, {
				header: 'Maximum',
				width: 120,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex: 'max',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					if(value == 'pro')
					{
						return ''; 
					}
					if (value.match(/[a-z]/i)) 
					{
						return value;
					}
					else if(parseInt(value) > 0)
					{
						return value +' hrs';
					}
					else 
					{
						return 0
					}
					
				},
				summaryType: function(records, values) {
					var i = 0,
						length = records.length,
						total = 0,
						record;

					for (; i < length; ++i) 
					{
						record = records[i];
						if(record.get('model') != 'Project Details')
						{
							if(record.get('max') > 0)
							total += parseInt(record.get('max'));
						}
						else
						{
							total = 'pro';
						}
					}
					return total;
				},
			}]
		},{
			xtype: 'hidden',
			id: 'fte_total',
		}, {
			header: 'Shift',
			width: 120,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex: 'shift',
		}, {
			header: 'Support Coverage',
			width: 150,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex: 'support',
		}, {
			header: 'Anticipated Start Date',
			width: 186,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex: 'due',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
            summaryRenderer: Ext.util.Format.dateRenderer('d-M-Y')
		}];
		
		this.callParent(arguments);
	},
});