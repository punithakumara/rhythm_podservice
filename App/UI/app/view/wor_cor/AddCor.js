Ext.define('AM.view.wor_cor.AddCor',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	title: 'Add COR Details',
	border : 'true',
    initComponent : function() {

    	me = this;
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
    	
    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addCORFrm',
			width: 1347,
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 141,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				float : 'left',
				padding : '8px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 650,
				title:"Change Order Information",
				items:[{
					xtype:'hidden',
					name:'cor_id',
					id:'cor_id',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Change Order ID <span style="color:red">*</span>',
					value : 'COR'+Ext.Date.format(new Date(), 'ymdhis'),
					name : 'change_order_id',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order ID <span style="color:red">*</span>',
					id: 'previous_work_order_id',
					name: 'previous_work_order_id',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order Name <span style="color:red">*</span>',
					id: 'previous_wor_name',
					name: 'previous_wor_name',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'client_name',
					id: 'cor_client',
					submitValue: true,
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order Type <span style="color:red">*</span>',
					id: 'previous_wor_type',
					name: 'previous_wor_type',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel : 'Service Type <span style="color:red">*</span>',
					id: 'previous_service_type',
					name: 'name',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Order Start Date <span style="color:red">*</span>',
					name : 'wor_start_date',
					id : 'wor_start_date',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Order End Date <span style="color:red">*</span>',
					name : 'wor_end_date',
					id : 'wor_end_date',
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					readOnly: true,
					name : 'status',
					id : 'wor_status',
					maskRe: /[]/,
					fieldLabel: 'Status <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': 'In-Progress', 'Status': 'In-Progress'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: 'In-Progress',
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Change Request Date <span style="color:red">*</span>',
					anchor : '100%',
					name : 'change_request_date',
					id : 'change_request_date',
					minValue: AM.app.globals.cormindate,
					maxValue : startMaxDate,
					maskRe: /[0-9\/]/,
					allowBlank : false,
				}]
			}, {
				xtype:'fieldset',
				padding: '10',
				width: 650,
				title:"People Information",
				items:[ {
					xtype:'displayfield',
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					name: 'client_partner',
					id:'cor_cp',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					name: 'engagement_manager',
					id:'cor_em',
					submitValue: true
				}, {
					xtype:'displayfield',
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					name: 'delivery_manager',
					id:'cor_dm',
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Associate Manager',
					name: 'associate_manager',
					id: 'cor_am',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Team Lead',
					name: 'team_lead',
					id: 'cor_tl',
					submitValue: true
				}, {
					xtype:'displayfield',
					fieldLabel: 'Stake Holder(s)',
					name: 'stake_holder',
					id:'cor_sh',
					submitValue: true				
				}, {
					xtype:'hidden',
					name:'work_id',
					id:'work_id',
				}, {
					xtype:'hidden',
					name:'client',
					id:'cor_client_id',
					submitValue: true
				},{
					xtype:'hidden',
					name:'wor_type_id',
					id:'cor_wor_type_id',
				}, {
					xtype:'hidden',
					name:'client',
					id:'cor_client_id',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'client_partner',
					id:'cor_client_partner',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'engagement_manager',
					id:'cor_engagement_manager',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'delivery_manager',
					id:'cor_delivery_manager',
					submitValue: true
				}, {
					xtype:'hidden',
					name:'stake_holder',
					id:'cor_stake_holder',
					submitValue: true
				},  {
					xtype:'hidden',
					name:'team_lead',
					id:'cor_team_lead',
				},  {
					xtype:'hidden',
					name:'associate_manager',
					id:'cor_associate_manager',
					submitValue: true
				}]
			}]
    	}, {
			xtype: 'displayfield',
			fieldLabel: 'Existing Details',
			labelCls: 'biggertext'
		}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1347,
			minHeight: 300,
			id: 'ExistCorTabPanel',
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				id: 'existFTE',
				items : [{								
					xtype:AM.app.getView('wor_cor.existFTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('wor_cor.existHourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('wor_cor.existVolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('wor_cor.existProjectList').create(),
					hidden:false
				}]
			}]
		}, {
			xtype: 'displayfield',
			fieldLabel: 'New Details',
			labelCls: 'biggertext'
		}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1347,
			height: 300,
			id: 'myAddCorTabPanel',
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				id: 'FTE_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corFTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				id: 'Hourly_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corHourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				id: 'Volume_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corVolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				id: 'Project_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.corProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
    		text : 'Save as Draft',
			id:'addSave',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
        }, {
		   	text : 'Back',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
    	  
        }, {
		   text : 'Submit',
    	   id:'addSubmit',
    	   icon : AM.app.globals.uiPath+'resources/images/submit.png',
    	   handler : this.onSubmit
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		if(AM.app.globals.wor_cor_combostatus == '')
		{
			Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
				if(btn === 'yes'){
					AM.app.globals.cormindate = '';
					AM.app.getController('ClientCOR').initCOR();
				}
			});
		}
		else
		{
			Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
				if(btn === 'yes'){
					AM.app.globals.cormindate = '';
					AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus);
				}
			});
		}
	},
	
	onSave :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) 
		{
			AM.app.globals.cormindate = '';
			Ext.getCmp('addSave').setDisabled(true);
			AM.app.getController('ClientCOR').onSaveCOR(form, 'In-Progress',AM.app.globals.redir)
		}
		else
		{
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Some mandatory fields are left blank!!',
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK,
				closable: false,
			});
		}
	},

	onSubmit :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) 
		{
			AM.app.globals.cormindate = '';
			Ext.getCmp('addSubmit').setDisabled(true);
			AM.app.getController('ClientCOR').onSaveCOR(form, 'Open',AM.app.globals.redir)
		}
		else
		{
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Some mandatory fields are left blank!!',
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK,
				closable: false,
			});
		}
	},
});