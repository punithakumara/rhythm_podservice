Ext.define('AM.view.wor_cor.AddResources',{
	extend : 'Ext.window.Window',
	alias : 'widget.AddResources',
	layout: 'fit',
	layout: {
        type: 'vbox',
    },
	store: 'AllReportees',
	width : '90%',
	height: 555,
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	title: 'Add Resources',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
    	this.items = [{
			layout : 'hbox',
			margin:'0 0 10 0',
			items: [{
				xtype:'displayfield',
				fieldLabel: '<b>WOR ID</b> ',
				id: 'res_work_order_id',
				name: 'res_work_order_id',
				labelWidth : 60,
				width: 220,
				submitValue : true,
			}, {
				xtype : 'displayfield',
				fieldLabel: '<b>Client Name</b> ',
				name: 'res_client',
				id: 'res_client',
				labelWidth : 90,
				width : 300,
			}, {
				xtype : 'displayfield',
				fieldLabel: '<b>Job Responsibilities</b> ',
				name: 'job_responsibilities ',
				id: 'add_job_responsibilities',
				labelWidth : 135,
				width : 360,
			}, {
				xtype : 'displayfield',
				fieldLabel: '<b>Support Coverage</b> ',
				name: 'support_coverage ',
				id: 'add_support_coverage',
				labelWidth : 125,
				width : 200,
			}, {
				xtype : 'hiddenfield',
				name: 'res_project_id',
				id: 'res_add_project_id',
				submitValue : true
			}, {
				xtype : 'hiddenfield',
				name: 'res_client_id',
				id: 'res_add_client_id',
				submitValue : true
			}]
    	}, {
			xtype : 'form',
			layout: 'hbox',
			id : 'addResFrm',
			items:[{
				xtype : 'textfield',
				format : AM.app.globals.CommonDateControl,
				fieldLabel : 'Employee ID/Name ',
				anchor : '100%',
				width: 500,
				name : 'resource_id',
				id : 'resource_id',
				labelWidth : 150,
				listeners: {
					change: function(field, e) {
						//if(e.getKey() == e.ENTER) {
							me.onClick(me)
						//}
					}
				}
			}, {
				xtype:'tbspacer',
				width: 15
			}, {
				xtype:'button',
				text:'Search',
				id:'transSecondSearch',
				flex : 1,
				listeners:{
					'click':function(){
						me.onClick(me)
					}
				}
			}]
		}, {
			xtype:'gridpanel',
			border:true,
			margin:'10 0 5 0',
			height:350,
			width:'100%',
			id:'WORtransGridID',
			selType: 'checkboxmodel',
			selModel: {
				checkOnly: true,
				mode:'MULTI',
				listeners: {
					deselect: function(model, record, index) {
						var myAry = AM.app.globals.transEmployRes;						
						for(var i =0; i < myAry.length; i++)
						{
							if (myAry[i] != null && (myAry[i].data.employee_id == record.data.employee_id))
							{
								myAry[i] = null;
							}
						}
					},
					select: function(model, record, index) {
						if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
						{							
							AM.app.globals.transEmployRes.push(record);
						}
					}
				}
			} ,
			columns: [{
				header: 'Employee ID',  
				dataIndex: 'company_employ_id',  
				flex: 2,
				menuDisabled:true,
			},{
            	header: 'Employee Name',  
            	dataIndex: 'FullName',  
            	flex: 3,
				menuDisabled:true,
            },{
            	header: 'Designation',  
            	dataIndex: 'designationName',  
            	flex: 3,
				menuDisabled:true,
            },{
            	header: 'Email', 
        		dataIndex: 'Email', 
        		flex: 3,
				menuDisabled:true,
            },{
            	header: 'Reporting To', 
        		dataIndex: 'supervisor', 
        		flex: 3,
				menuDisabled:true,
            }],
			
			bbar: Ext.create('Ext.PagingToolbar', {
				id:'WORtransGridPage',
				// store: store,
				displayInfo: true,
				listeners: {
					beforechange: function(item1,item2,item3){
						this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(), 'manager' : Ext.util.Cookies.get('employee_id'), 'ProjectID':Ext.getCmp('res_add_project_id').getValue(),'client_id':Ext.getCmp('res_add_client_id').getValue()}																		
					},
					change:function(){
						
						var myGrid = Ext.getCmp('WORtransGridID');
						
						for(i=0; i < myGrid.store.data.length; i++)
						{
							var user = myGrid.store.getAt(i);							
							var myAry = AM.app.globals.transEmployRes;
						
							for (var j=0; j< myAry.length; j++)
							{
								if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
									myGrid.selModel.doMultiSelect(user,true);
							}	
						}
					}
				},
				displayMsg: 'Displaying employees {0} - {1} of {2}',
				emptyMsg: "No employees to display."
			}),
			listeners: {
				sortchange :  function(ct, column) {
					this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(),'manager' : Ext.util.Cookies.get('employee_id'), 'ProjectID':Ext.getCmp('res_add_project_id').getValue(),'client_id':Ext.getCmp('res_add_client_id').getValue()}																		
					this.store.load();
				}
			}
		}, {
			xtype : 'datefield',
			format : AM.app.globals.CommonDateControl,
			fieldLabel : 'Assign Date <span style="color:red">*</span>',
			anchor : '100%',
			name : 'transition_date',
			id :   'transition_date',
			maskRe: /[0-9\/]/,
			labelWidth : 120,
			width: 300,
			allowBlank : false, 
			submitValue : true,
			style: {
				'margin-top': '8px'
			}
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Confirm',
			id:'addSubmit',
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},

	onSubmit :  function(btn) {
		var win = this.up('window');
		
		var newArray = new Array();
		for (var i = 0; i < AM.app.globals.transEmployRes.length; i++) {
			if (AM.app.globals.transEmployRes[i]) {
				newArray.push(AM.app.globals.transEmployRes[i]);
			}
		}
		if(newArray.length>0)
		{
			Ext.getCmp('addSubmit').setDisabled(true);
			AM.app.getController('ClientWOR').onSaveResources(win);
		}
		else
		{
			Ext.Msg.show({
			   title:'Alert',
			   msg:"Please Select Employee(s)",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
		}
	},
	
	onClick:function(my)
	{
		var empParam = Ext.getCmp('resource_id').getValue();
		
		if(empParam != "" && empParam != null)
		{
			var MyStore = Ext.getStore('AllReportees').load({
				params:{'manager' : Ext.util.Cookies.get('employee_id'), 'searchTxt': empParam, 'ProjectID':Ext.getCmp('res_add_project_id').getValue(),'client_id':Ext.getCmp('res_add_client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}
			})
			
			my.intialStore = MyStore;			
			Ext.getCmp('WORtransGridID').bindStore(MyStore);
			Ext.getCmp('WORtransGridPage').bindStore(MyStore);
		}
		else
		{
			Ext.getCmp('WORtransGridID').store.removeAll();
			var MyStore = Ext.getStore('AllReportees').load({
				params:{'manager' : Ext.util.Cookies.get('employee_id'), 'searchTxt': empParam, 'ProjectID':Ext.getCmp('res_add_project_id').getValue(),'client_id':Ext.getCmp('res_add_client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}
			})
		}
	}
	
});