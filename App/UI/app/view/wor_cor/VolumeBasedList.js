Ext.define('AM.view.wor_cor.VolumeBasedList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.VolumeBasedList',
	title: 'Volume Based Details',
	id : 'volume_list',
	store: 'WorkTypeVolume',
    layout : 'fit',
    selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false,
		stripeRows: false, 
        getRowClass: function(record) {
			if(record.get('process_id') == '' || record.get('process_id') != '0' )
			{
				return ''; 
			} 
			else
			{
				return 'child-row';
			}
        } 
	},
    width : '100%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	
	initComponent : function() {
		var me = this;
		var project_type_id="";
		
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		startMinDate = new Date(date.getFullYear(), date.getMonth(), 1);
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true,
			
			listeners : {
				'beforeedit' :  function(editor, e) 
				{
					if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(true);
						Ext.getCmp('editSubmit').setDisabled(true);
						Ext.getCmp('edit_FTE_id').setDisabled(true);
						Ext.getCmp('edit_Hourly_id').setDisabled(true);
						Ext.getCmp('edit_Project_id').setDisabled(true);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(true);
						Ext.getCmp('FTE_id').setDisabled(true);
						Ext.getCmp('Hourly_id').setDisabled(true);
						Ext.getCmp('Project_id').setDisabled(true);
						Ext.getCmp('addSubmit').setDisabled(true);
					}						
				},
				
				'validateedit'  : function(editor, e) 
				{
				   var project_type = project_name = volume_coverage = shift = volume_min = volume_max = experience = skills = start_date = support = responsibilities = null;
					editor.editor.form.getFields().items.forEach(function(entry) {
						project_type   		= Ext.ComponentQuery.query("#volume_project_type")[0].getValue();
						project_name    	= Ext.ComponentQuery.query("#project_name")[0].getValue();
						volume_coverage   	= Ext.ComponentQuery.query("#volume_coverage")[0].getValue();
						shift    			= Ext.ComponentQuery.query("#vb_shift")[0].getValue();
						volume_min 			= Ext.ComponentQuery.query("#min_volume")[0].getValue();
						volume_max 			= Ext.ComponentQuery.query("#max_volume")[0].getValue();
						experience 			= Ext.ComponentQuery.query("#vb_experience")[0].getValue();
						skills           	= Ext.ComponentQuery.query("#vb_skills")[0].getValue();
						anticipated_date    = Ext.ComponentQuery.query("#volume_anticipated_date")[0].getValue();
						support         	= Ext.ComponentQuery.query("#vb_support_coverage")[0].getValue();
						responsibilities	= Ext.ComponentQuery.query("#vb_responsibilities")[0].getValue();
					} );
					
					var startDate = Ext.getCmp('start_date').getValue();
					var endDate   = Ext.getCmp('end_date').getValue();
					
					if(anticipated_date != null)
					{
						if(startDate != null && endDate != null)
						{
							if(anticipated_date < startDate || anticipated_date > endDate)
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Anticipated start Date should be With in Work Order StartDate and EndDate",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
					            return false;
							}
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Warning',
								msg: "Please Enter Work Order StartDate and EndDate",
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.Msg.OK,
								closable: false,
							});
				            return false;
						}
					}
					
					if(project_name != null && volume_coverage != null && shift != null && volume_min != null && volume_max != null && experience != null && skills != null && skills != '' && anticipated_date != null && support != null && responsibilities != null) 
					{
						if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
						{
							Ext.getCmp('editSave').setDisabled(false);
							Ext.getCmp('editSubmit').setDisabled(false);
							Ext.getCmp('edit_FTE_id').setDisabled(false);
							Ext.getCmp('edit_Hourly_id').setDisabled(false);
							Ext.getCmp('edit_Project_id').setDisabled(false);
						}
						else
						{
							Ext.getCmp('addSave').setDisabled(false);
							Ext.getCmp('addSubmit').setDisabled(false);	
							Ext.getCmp('Hourly_id').setDisabled(false);
							Ext.getCmp('Project_id').setDisabled(false);
							Ext.getCmp('FTE_id').setDisabled(false);
						}
						
						return true;
					} 
					else 
					{						
						Ext.MessageBox.show({
							title: 'Warning',
							msg: "Please Enter all the Data",
							icon: Ext.MessageBox.WARNING,
							buttons: Ext.Msg.OK,
							closable: false,
						});
						return false;
					}
				},

				canceledit: function(grid,obj)
				{
					if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(false);
						Ext.getCmp('editSubmit').setDisabled(false);
						Ext.getCmp('edit_FTE_id').setDisabled(false);
						Ext.getCmp('edit_Hourly_id').setDisabled(false);
						Ext.getCmp('edit_Project_id').setDisabled(false);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);	
						Ext.getCmp('Hourly_id').setDisabled(false);
						Ext.getCmp('Project_id').setDisabled(false);
						Ext.getCmp('FTE_id').setDisabled(false);
					}
					 
					if(obj.store.data.items[0].data['volume_coverage'] == "")					 
						me.getStore().removeAt(obj.rowIdx);
				},
				 
				scope:me 
			},
		});
		
		Ext.apply(this, {
			plugins: [this.editing]
		});
		
		//**** End Editing Plugin ****//
		 
		//**** List View Coloumns ****//
    	this.columns = [{
			header:'Project',
			tooltip:'Project',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'fk_project_type_id',
			editor: {
				xtype:'combobox',
				name: 'fk_project_type_id',
				store:'WorProjects',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 0,
				displayField: 'project_type',
				valueField: 'project_type_id',
				multiSelect: false,
				itemId : 'volume_project_type',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(AM.app.globals.wor_type_id!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_type_id':AM.app.globals.wor_type_id};
						}
					},
					select : function(obj, metaData, record) {
						project_type_id = obj.getValue();
						Ext.getStore('Responsibilities').load({
							params:{ 'project_type_id': obj.getValue() }
						});
					}
				}
			},
			renderer: function(value) {
                var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
 				return rec == null ? value : rec.get('project_type');
            },
		},{
			header: 'Project Name',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_name',
			tooltip: 'Project Name',
			editor: {
				xtype:'textfield',
				emptyText: 'ClientName_ServiceType',
				itemId: 'project_name',
				maskRe: /[A-Za-z -,_]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
			header: 'Roles',
			flex: 1.4,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'responsibilities',
			tooltip : 'Roles',
			editor: {
				xtype:'combobox',
				name: 'responsibilities',
				store:'Responsibilities',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 2,
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId : 'vb_responsibilities',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(project_type_id!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'project_type_id':project_type_id};
						}
					}
				}
			},
			renderer: function(value) {
				console.log(value);
                var rec	= Ext.getStore('Responsibilities').findRecord('id', value, 0, false, true, true);
                console.log(rec);
				return rec == null ? value : rec.get('name');
            },
		}, {
			header: 'Volume Coverage',
			flex: 0.8,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'volume_coverage',
			tooltip: 'Volume Coverage',
			editor: {
				xtype:'combobox',
				displayField: 'name',
				valueField: 'id',
				store:'DurationCoverage',				
				itemId: 'volume_coverage',
				forceSelection: true,
			},
			renderer: function(value, meta, record) {
                var rec	= Ext.getStore('DurationCoverage').findRecord('id', value);
				var val = (rec == null) ? value : rec.get('name');
				if(record.data.fk_cor_id!=null)
				{
					val = '<a href="#" style="text-decoration: none" onclick=AM.app.getController("ClientCOR").onViewCORFromWor("'+record.data.fk_cor_id+'")>'+ val +'</a>';
				}
				return val;
            },			
		}, {
			header : 'Shift', 
			flex: 0.6,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'shift',
			tooltip : 'Shift', 
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				queryMode: 'remote',
				displayField: 'shift_code',
				valueField: 'shift_id',
				multiSelect: false,
				itemId: 'vb_shift',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				if(value <= 7){
					return value;
				} else {
	                var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
					return rec == null ? value : rec.get('shift_code');
				}
            },
		}, {
			text: "Expected Volume",
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			columns: [{
				header : 'Minimum',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex : 'min_volume',
				tooltip : 'Minimum',
				renderer: function(value, meta, record, row, col) {
					if(value <= 0 ){
						return 0;
					}
					else if(record.data.change_type == 2){
						return '-' + value;
					}else{
						return value;
					}
				},
				editor: {
					xtype:'numberfield',
					itemId: 'min_volume',
					minValue: 1,
					decimalPrecision : 0,
					listeners: {
						change : function(obj) {
							obj.next().setMinValue(obj.getValue());
						}
					}
				}
			}, {
				header : 'Maximum',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex : 'max_volume',
				renderer: function(value, meta, record, row, col) {
					if(record.data.change_type == 2){
						return '-' + value;
					}else{
						return value;
					}
				},
				tooltip : 'Maximum',
				editor: {
					xtype:'numberfield',
					itemId: 'max_volume',
					minValue: 1,
					decimalPrecision : 0,
					listeners: {
						change : function(obj) {
							obj.prev().setMaxValue(obj.getValue());
						}
					}
				}
			}]
		}, {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			flex: 0.8,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Support Coverage',
			editor: {
				xtype:'combobox',
				name: 'support_coverage',
				store : Ext.getStore('SupportCoverage'),
				queryMode: 'remote',
				displayField: 'support_type',
				valueField: 'support_id',
				multiSelect: false,
				itemId: 'vb_support_coverage',
				forceSelection: true,
			},
			renderer: function(value) {
                var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
            },
		}, {
			header: 'Anticipated Start Date',
			dataIndex: 'anticipated_date',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Anticipated Start Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
				itemId: 'volume_anticipated_date',
				maxValue: startMaxDate,
				minValue: startMinDate
			}
		}, {
			xtype:'hidden',
			header: 'Actual Start Date',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'actual_date',
			tooltip : 'Actual Start Date',
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
				itemId : 'vb_actual_date',
			}
		}, {
			header : 'Experience in Years', 
			flex: 0.8,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'experience',
			tooltip : 'Experience in Years',
			editor: {
				xtype:'numberfield',
				minValue: 0,
				maxValue: 50,
				itemId: 'vb_experience',
				allowDecimals: false
			}
		}, {
			header : 'Skills',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'skills',
			tooltip : 'Skills',
			editor: {
				xtype:'textfield',
				itemId: 'vb_skills',
				maskRe: /[A-Za-z0-9 ,]/,
			}
		}, {
			header: 'Comments',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Comments',
			dataIndex : 'comments',
			editor: {
				xtype:'textfield',
				itemId: 'volume_comments',
				maskRe: /[A-Za-z0-9 ,]/,
			},
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("comments") + '"';
				return value;
			}
		}];

		//**** End List View Coloumns ****//

		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'AddVolume',
			style: {
				'margin-right':'10px;'
			},
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function() {
				var r = Ext.create('AM.store.WorkTypeVolume');
				me.editing.cancelEdit();
				me.store.insert(0, r);
				me.editing.startEdit(0, 0);
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});