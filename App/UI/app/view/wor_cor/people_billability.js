Ext.define('AM.view.wor_cor.people_billability',{
	extend : 'Ext.window.Window',
	alias : 'widget.people_billability',
	layout: 'fit',
	store: 'Categorization',
	width : '90%',
	height: 555,
	autoScroll: true,
	autoHeight : true,   
	autoShow : true,
	closable: false,
	modal: true,
	title: 'Billability',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
	initComponent : function() {

		var me = this;
		var intialStore = "";
		
		this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items:[{
				region:'west',
				// xtype: 'insightTopPanel',
				width: '21%',
				border: true,
				height: 400,
				autoScroll:true,
				margin : '0 5px 5px 0',
				layout: {
					type: 'vbox',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : '<b>Client</b> <span style="color:red">*</span>',
					width: 220,
					name: 'res_client',
					id: 'res_client',
					labelAlign: 'top',
					style:{
						'margin':'10px 0 0 5px'
					},
				}, {
					xtype : 'hiddenfield',
					name: 'res_client_id',
					id: 'res_client_id',
				},{
					xtype:'hidden',
					name:'res_add_client_id',
					id:'res_add_client_id',
				}, {
					xtype : 'hiddenfield',
					name: 'res_wor_id',
					id: 'res_wor_id',
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order ID</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_id',
					name: 'res_work_order_id',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order Name</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_name',
					name: 'res_work_order_name',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Project</b>',
					anchor : '100%',
					width: 220,
					id:'project_type_id',
					name: 'project_type_id',
					labelAlign: 'top',
					labelWidth : 150,
					store: 'WorProjects',
					queryMode: 'remote',
					displayField: 'project_type',
					submitValue : true,
					valueField: 'project_type_id',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'work_order_id':Ext.getCmp('wor_id').getValue()};
						},
						select: function(obj){
							if(obj.getValue()!="")
							{
								Ext.Ajax.request({
									url: AM.app.globals.appPath+'index.php/Clientwor/wor_project_roles?v='+AM.app.globals.version,
									method: 'POST',
									params: {
										'wor_id':Ext.getCmp('wor_id').getValue(),
										'project_id':obj.getValue(),
									},
									scope: this,
									success: function(response) {
										var myObject = Ext.JSON.decode(response.responseText);
										Ext.getCmp('view_job_responsibilities').setValue(myObject.rows.role_type);
									}
								});
								Ext.getCmp('people_billability_grid').getStore().load({params: {'searchTxt': Ext.getCmp('resource_id').getValue(), 'project_type_id':Ext.getCmp('project_type_id').getValue(), 'wor_id':Ext.getCmp('wor_id').getValue(), 'client_id':Ext.getCmp('client_id').getValue(),'processID':AM.app.globals.categorPro,'ClientID':Ext.getCmp('client_id').getValue(),'Lead':AM.app.globals.categorLead,'project_type_id':Ext.getCmp('project_type_id').getValue() }});
							}
							else
							{
								Ext.getCmp('view_job_responsibilities').setValue('');
							}

							Ext.getStore('billableType').load({
								params:{'wor_id' :Ext.getCmp('wor_id').getValue(),
								'project_type_id' :Ext.getCmp('project_type_id').getValue(),
								'hasNoLimit':'1'
							}
						});
						},
					}
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Roles under this project</b>',
					width: 220,
					id: 'view_job_responsibilities',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'radiogroup',
					fieldLabel : '<b>Billable?</b> <span style="color:red">*</span>',
					width: 130,
					id:'radio_name',
					labelAlign: 'top',
					allowBlank: false,
					hidden: true,
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					// hidden: true,
					items: [{ 
						boxLabel: 'Yes', 
						name: 'name', 
						inputValue: '1' 
					},{ 
						boxLabel: 'No', 
						name: 'name', 
						inputValue: '0' 
					}]
				},{
					xtype: 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Billable Type</b> <span style="color:red">*</span>',
					multiSelect:false,
					anchor : '100%',
					id: 'bill_type',
					name: 'bill_type',
					hidden: true,
					width: 220,
					labelWidth : 150,
					labelAlign: 'top',
					store: 'billableType',
					queryMode: 'remote',
					minChars: 1 ,
					displayField: 'name', 
					valueField: 'id',
					allowBlank: false,
					style:{
						'position':'absolute',
						'margin':'28px 0 0 5px'
					},
					listeners:{
						beforequery	: function(queryEvent) {
							queryEvent.combo.getStore().getProxy().extraParams = {
								'wor_id' 	:Ext.getCmp('wor_id').getValue(),
								'project_type_id' 	:Ext.getCmp('project_type_id').getValue(),
								'hasNoLimit':'1', 
								'filterName' : queryEvent.combo.displayField
							};
						},
					}
				},{
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Billable Date</b> <span style="color:red">*</span>',
					name : 'billable_date',
					id :   'billable_date',
					anchor : '100%',
					maskRe: /[0-9\/]/,
					hidden: true,
					width: 220,
					labelWidth : 150,
					allowBlank : false, 
					submitValue : true,
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 10px 5px'
					}
				}]
			}, {
				region:'center',
				width: '69%',
				items:[{
					xtype : 'form',
					layout: 'hbox',
					id : 'addResFrm',
					items:[{
						xtype : 'textfield',
						format : AM.app.globals.CommonDateControl,
						fieldLabel : 'Employee ID/Name ',
						anchor : '100%',
						width: 500,
						name : 'resource_id',
						id : 'resource_id',
						labelWidth : 150,
						listeners: {
							change: function(field, e) {
								//if(e.getKey() == e.ENTER) {
									me.onClick(me)
								//}
							}
						}
					}, {
						xtype:'tbspacer',
						width: 15
					}, {
						xtype:'button',
						text:'Search',
						id:'transSecondSearch',
						width:60,
						listeners:{
							'click':function(){
								me.onClick(me)
							}
						}
					},]
				},{
					xtype:'gridpanel',
					border:true,
					margin:'10 0 5 0',
					height:415,
					id:'people_billability_grid',
					selType: 'checkboxmodel',
					selModel: {
						checkOnly: true,
						mode:'MULTI',
						listeners: {
							deselect: function(model, record, index) {
								var myAry = AM.app.globals.transEmployRes;						
								for(var i =0; i < myAry.length; i++)
								{
									if (myAry[i] != null && (myAry[i].data.employee_id == record.data.employee_id))
									{
										myAry[i] = null;
									}
								}
								
								myAry = myAry.filter(function(e){return e;});
								myAry = Ext.Array.unique(myAry);
								if(myAry.length<1)
								{
									Ext.getCmp('save_bill').setVisible(false);
									Ext.getCmp('bill_type').setVisible(false);
									Ext.getCmp('radio_name').setVisible(false);
									Ext.getCmp('billable_date').setVisible(false);
								}
							},
							select: function(model, record, index) {
								if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
								{							
									AM.app.globals.transEmployRes.push(record);	
									Ext.getCmp('save_bill').setVisible(true);
									Ext.getCmp('bill_type').setVisible(true);
									Ext.getCmp('radio_name').setVisible(true);
									Ext.getCmp('billable_date').setVisible(true);
									Ext.getCmp('billable_date').setMinValue(Ext.getCmp('view_start_date').getValue());
									Ext.getCmp('billable_date').setMaxValue(Ext.getCmp('view_end_date').getValue());
								}
							}
						}
					} ,
					columns: [{
						header: 'Employee ID',  
						dataIndex: 'company_employ_id',  
						flex: 1,
						menuDisabled:true,
					},{
						header: 'Employee Name',  
						dataIndex: 'FullName',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Designation',  
						dataIndex: 'designationName',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Supervisor', 
						dataIndex: 'supervisor', 
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Project', 
						dataIndex: 'project_type', 
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Billable',
						dataIndex: 'billable',
						sortable:true,
						flex: 1,
						menuDisabled:true,
						fixed: true	
					},{
						header: 'Billable Type',
						dataIndex: 'billable_type',
						sortable:true,
						flex: 2,
						renderer: this.renderTip,
						menuDisabled:true,					
					}],
					
					bbar: Ext.create('Ext.PagingToolbar', {
						id:'people_billability_gridPage',
						store: this.store,
						displayInfo: true,
						listeners: {
							beforechange: function(item1,item2,item3){
								this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(),  'wor_id':Ext.getCmp('wor_id').getValue(), 'client_id':Ext.getCmp('client_id').getValue(), 'processID':AM.app.globals.categorPro,'ClientID':Ext.getCmp('client_id').getValue(),'Lead':AM.app.globals.categorLead,'project_type_id':Ext.getCmp('project_type_id').getValue()}
								AM.app.globals.categorEmployRes = [];																
							},
							change:function(){
								var myGrid = Ext.getCmp('people_billability_grid');
								
								for(i=0; i < myGrid.store.data.length; i++)
								{
									var user = myGrid.store.getAt(i);							
									var myAry = AM.app.globals.transEmployRes;
									
									for (var j=0; j< myAry.length; j++)
									{
										if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
											myGrid.selModel.doMultiSelect(user,true);
									}	
								}
								Ext.getCmp('resource_id').focus();
							}
						},
						displayMsg: 'Displaying employees {0} - {1} of {2}',
						emptyMsg: "No employees to display.",
						pageSize: AM.app.globals.itemsPerPage,
						plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
					}),
					listeners: {
						sortchange :  function(ct, column) {
							this.store.getProxy().extraParams = {
								'searchTxt': Ext.getCmp('resource_id').getValue(),  
								'wor_id':Ext.getCmp('wor_id').getValue(), 
								'client_id':Ext.getCmp('client_id').getValue(), 
								'processID':AM.app.globals.categorPro,
								'ClientID':Ext.getCmp('client_id').getValue(),
								'Lead':AM.app.globals.categorLead,
								'project_type_id':Ext.getCmp('project_type_id').getValue()
							}																		
							this.store.load();
						}
					}
				}]
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Submit',
			id:'save_bill',
			hidden:true,
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
		}];

		this.callParent(arguments);
	},	

	onCancel : function(win) {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				win.up('window').close();
			}
		});
	},

	onSubmit :  function(btn) {
		var win = this.up('window');
		var newArray = new Array();

		for (var i = 0; i < AM.app.globals.transEmployRes.length; i++) 
		{
			if (AM.app.globals.transEmployRes[i]) {
				newArray.push(AM.app.globals.transEmployRes[i]);
			}
		}
		if(newArray.length>0)
		{
			// console.log(newArray);
			AM.app.getController('ClientWOR').onSavePeopleBillability(win);
		}
		else
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Employee(s)",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
	},

	onClick:function(my)
	{
		var empParam = Ext.getCmp('resource_id').getValue();
		
		if(empParam != "" && empParam != null)
		{
			var MyStore = Ext.getStore('Categorization').load({
				params:{'searchTxt': empParam, 'wor_id':Ext.getCmp('wor_id').getValue(), 'client_id':Ext.getCmp('client_id').getValue(), 'processID':AM.app.globals.categorPro,'ClientID':Ext.getCmp('client_id').getValue(),'Lead':AM.app.globals.categorLead,'project_type_id':Ext.getCmp('project_type_id').getValue()}
			})
			
			my.intialStore = MyStore;			
			Ext.getCmp('people_billability_grid').bindStore(MyStore);
			Ext.getCmp('people_billability_gridPage').bindStore(MyStore);
		}
		else
		{
			Ext.getCmp('people_billability_grid').store.removeAll();
			var MyStore = Ext.getStore('Categorization').load({
				params:{'searchTxt': empParam, 'wor_id':Ext.getCmp('wor_id').getValue(), 'client_id':Ext.getCmp('client_id').getValue(), 'processID':AM.app.globals.categorPro,'ClientID':Ext.getCmp('client_id').getValue(),'Lead':AM.app.globals.categorLead,'project_type_id':Ext.getCmp('project_type_id').getValue()}
			})
		}
	}
	
});