Ext.define('AM.view.wor_cor.FTEList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.FTEList',
	title: 'FTE Details',
	id : 'fte_list',
	layout : 'fit',
	selType: 'rowmodel',
	store: 'WorkTypeFTE',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		forceFit : true,
		stripeRows: false, 
		getRowClass: function(record) {
			if(record.get('process_id') == '' || record.get('process_id') != '0' )
			{
				return ''; 
			} 
			else{
				return 'child-row';
			}
		} 
	},
	width : '100%',
	minHeight : 225,
	loadMask: true, 
	border : true,
	plugins             : [
	Ext.create('Ext.grid.plugin.RowEditing', {
		clicksToEdit    : 1
	} ) 
	],
	
	initComponent : function() {
		var me = this;
		var project_type_id = "";
		
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		startMinDate = new Date(date.getFullYear(), date.getMonth(), 1);
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true,
			listeners : {
				'beforeedit' :  function(editor, e) 
				{
					if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
					{											
						Ext.getCmp('editSave').setDisabled(true);
						Ext.getCmp('editSubmit').setDisabled(true);
						Ext.getCmp('edit_Volume_id').setDisabled(true);
						Ext.getCmp('edit_Hourly_id').setDisabled(true);
						Ext.getCmp('edit_Project_id').setDisabled(true);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(true);
						Ext.getCmp('Hourly_id').setDisabled(true);
						Ext.getCmp('Volume_id').setDisabled(true);
						Ext.getCmp('Project_id').setDisabled(true);
						Ext.getCmp('addSubmit').setDisabled(true);
					}
				},

				'validateedit'  : function(editor, e) 
				{
					var project_type = project_name = no_of_fte = shift  = experience = skills = anticipated_date = support = responsibilities = null;
					editor.editor.form.getFields().items.forEach(function(entry) {
						project_type   		= Ext.ComponentQuery.query("#fte_project_type")[0].getValue();
						project_name    	= Ext.ComponentQuery.query("#project_name")[0].getValue();
						no_of_fte   		= Ext.ComponentQuery.query("#no_of_fte")[0].getValue();
						shift    			= Ext.ComponentQuery.query("#shift")[0].getValue();
						appr_buffer    		= Ext.ComponentQuery.query("#appr_buffer")[0].getValue();
						experience 			= Ext.ComponentQuery.query("#experience")[0].getValue();
						skills           	= Ext.ComponentQuery.query("#skills")[0].getValue();
						anticipated_date    = Ext.ComponentQuery.query("#anticipated_date")[0].getValue();
						support				= Ext.ComponentQuery.query("#support_coverage")[0].getValue();
						responsibilities    = Ext.ComponentQuery.query("#responsibilities")[0].getValue();

						//Rounding value to .5 or whole
						if(no_of_fte != '' && no_of_fte != null)
							Ext.ComponentQuery.query("#no_of_fte")[0].setValue((Math.round(no_of_fte*4)/4).toFixed(2));
					});
					
					var startDate = Ext.getCmp('start_date').getValue();
					var endDate   = Ext.getCmp('end_date').getValue();
					
					if(anticipated_date != null)
					{
						if(startDate != null && endDate != null)
						{
							if(anticipated_date < startDate || anticipated_date > endDate)
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Anticipated Start Date should be with in Work Order StartDate and EndDate",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
								return false;
							}
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Warning',
								msg: "Please Enter Work Order StartDate and EndDate",
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.Msg.OK,
								closable: false,
							});
							return false;
						}
					}
					
					if(project_type != null && project_name != null && no_of_fte != null && appr_buffer != null && shift != null && anticipated_date != null && experience != null && skills != null && skills != '' && support != null && responsibilities != null) 
					{
						if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
						{
							Ext.getCmp('editSave').setDisabled(false);
							Ext.getCmp('editSubmit').setDisabled(false);
							Ext.getCmp('edit_Volume_id').setDisabled(false);
							Ext.getCmp('edit_Hourly_id').setDisabled(false);
							Ext.getCmp('edit_Project_id').setDisabled(false);
						}
						else if(no_of_fte>='0.25' && appr_buffer>='0')
						{
							Ext.getCmp('addSave').setDisabled(false);
							Ext.getCmp('addSubmit').setDisabled(false);	
							Ext.getCmp('Hourly_id').setDisabled(false);
							Ext.getCmp('Volume_id').setDisabled(false);
							Ext.getCmp('Project_id').setDisabled(false);
						}
						
						return true;
					} 
					else 
					{
						Ext.MessageBox.show({
							title: 'Warning',
							msg: 'Please Enter all the Data',
							icon: Ext.MessageBox.WARNING,
							buttons: Ext.Msg.OK,
							closable: false,
						});
						return false;
					}
				},

				canceledit: function(grid,obj)
				{
					if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(false);
						Ext.getCmp('editSubmit').setDisabled(false);
						Ext.getCmp('edit_Volume_id').setDisabled(false);
						Ext.getCmp('edit_Hourly_id').setDisabled(false);
						Ext.getCmp('edit_Project_id').setDisabled(false);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);	
						Ext.getCmp('Hourly_id').setDisabled(false);
						Ext.getCmp('Volume_id').setDisabled(false);
						Ext.getCmp('Project_id').setDisabled(false);
					}
					if(obj.store.data.items[0].data['no_of_fte'] == "")					 
						me.getStore().removeAt(obj.rowIdx);					 
				},
				scope:me 
			},
		});

Ext.apply(this, {
	plugins: [this.editing]
});

		//**** End Editing Plugin ****//

		//**** List View Coloumns ****//
		this.columns = [{
			header:'Project',
			tooltip:'Project',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'fk_project_type_id',
			editor: {
				xtype:'combobox',
				name: 'fk_project_type_id',
				store:'WorProjects',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 0,
				displayField: 'project_type',
				valueField: 'project_type_id',
				multiSelect: false,
				itemId : 'fte_project_type',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(AM.app.globals.wor_type_id!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_type_id':AM.app.globals.wor_type_id};
						}
					},
					select : function(obj, metaData, record) {
						project_type_id = obj.getValue();
						Ext.getStore('Responsibilities').load({
							params:{ 'project_type_id': obj.getValue() }
						});
					}
				}
			},
			renderer: function(value) {
				var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
                // REFERENCE findRecord(String fieldName, String/RegExp value, [Number startIndex], [Boolean anyMatch], [Boolean caseSensitive], Boolean exactMatch)
                return rec == null ? value : rec.get('project_type');
            },
        },{
			header: 'Project Name',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_name',
			tooltip: 'Project Name',
			editor: {
				xtype:'textfield',
				emptyText: 'ClientName_ServiceType',
				itemId: 'project_name',
				maskRe: /[A-Za-z -,_]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
        	header: 'Roles',
        	flex: 1.5,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	tooltip : 'Roles',
        	dataIndex : 'responsibilities',
        	editor: {
        		xtype:'combobox',
        		name: 'responsibilities',
        		store:'Responsibilities',
        		queryMode: 'local',
        		typeAhead: true, 
        		minChars: 0,
        		displayField: 'name',
        		valueField: 'id',
        		multiSelect: false,
        		flex: 1,
        		itemId : 'responsibilities',
        		forceSelection: true,
        		maskRe: /[A-Za-z -]/,
        		listeners : {
        			beforequery : function(queryEvent) {
        				if(project_type_id!=undefined)
        				{
        					queryEvent.combo.getStore().getProxy().extraParams = {'project_type_id':project_type_id};
        				}
        			}
        		}
        	},
        	renderer: function(value) {
        		console.log(value);
        		var rec	= Ext.getStore('Responsibilities').findRecord('id', value, 0, false, true, true);
        		console.log(rec);
        		return rec == null ? value : rec.get('name');
        	},
        }, {
        	header: '# of FTEs',
        	flex: 0.6,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	dataIndex : 'no_of_fte',
        	maskRe: /[0-9]/,
        	renderer: function(value, meta, record, row, col) {

        		var val = value;
        		if(val % 1 ===0){
        			val=Math.round(val);
        		}
        		if(record.data.change_type == 2 && val > 0)
        		{
        			val = '-' + value;
        		}

        		if(record.data.fk_cor_id!=null)
        		{
        			val = '<a href="#" style="text-decoration: none" onclick=AM.app.getController("ClientCOR").onViewCORFromWor("'+record.data.fk_cor_id+'")>'+ val +'</a>';
        		}

        		return val;
        	},
        	tooltip : '# of FTEs',
        	editor: {								
        		xtype:'numberfield',
        		editable: true,
        		minValue: 0.25,
        		maxValue: 1000,
        		step: 0.25,
        		value: '',
        		itemId: 'no_of_fte',
        	}
        },{
        	header: 'Approved Buffer',
        	flex: 0.7,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	dataIndex : 'appr_buffer',
        	tooltip : 'Approved Buffer',
        	maskRe: /[0-9]/,
        	editor: {								
        		xtype:'numberfield',
        		editable: true,
        		minValue: 0,
        		maxValue: 1000,
        		step: 1,
        		itemId: 'appr_buffer',
        		value: 0,
        		allowDecimals: false,
        	}
        }, {
        	header : 'Shift', 
        	flex: 0.7,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	dataIndex : 'shift',
        	tooltip : 'Shift',
        	editor: {
        		xtype:'combobox',
        		store: Ext.getStore('Shifts'),
        		queryMode: 'remote',
        		displayField: 'shift_code',
        		valueField: 'shift_id',
        		multiSelect: false,
        		itemId: 'shift',
        		maskRe: /[A-Za-z]/,
        		forceSelection: true,
        	},
        	renderer: function(value) {
        		if(value <= 7){
					return value;
				} else {
	        		var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
	        		return rec == null ? value : rec.get('shift_code');
        		}
        	},
        }, {
        	header: 'Support Coverage',
        	dataIndex: 'support_coverage',
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	flex: 0.8,
        	tooltip : 'Support Coverage',
        	editor: {
        		xtype:'combobox',
        		name: 'support_coverage',
        		store : Ext.getStore('SupportCoverage'),
        		queryMode: 'remote',
        		displayField: 'support_type',
        		valueField: 'support_id',
        		multiSelect: false,
        		itemId: 'support_coverage',
        		forceSelection: true,
        		maskRe: /[A-Za-z]/,
        		mode: 'remote'
        	},
        	renderer: function(value) {
        		var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
        		return rec == null ? value : rec.get('support_type');
        	},
        }, {
        	header: 'Anticipated Start Date',
        	flex: 1,
        	tooltip : 'Anticipated Start Date',
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	dataIndex : 'anticipated_date',
        	renderer : Ext.util.Format.dateRenderer('d-M-Y'),
        	editor: {
        		xtype:'datefield',
        		format: 'd-M-Y',
        		itemId : 'anticipated_date',
        		maxValue : startMaxDate,
        		minValue: startMinDate
        	}
        }, {
        	xtype:'hidden',
        	header: 'Actual Start Date',
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	flex: 1,
        	tooltip : 'Actual Start Date',
        	dataIndex : 'actual_date',
        	editor: {
        		xtype:'datefield',
        		format: 'd-M-Y',
        		itemId : 'actual_date',
        		maxValue : startMaxDate,
        	}
        }, {
        	header : 'Exp in Years', 
        	flex: 0.7,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	tooltip : 'Experience in Years',
        	dataIndex : 'experience',
        	editor: {
        		xtype:'numberfield',
        		minValue: 0,
        		maxValue: 50,
        		itemId: 'experience',
        		allowDecimals: false
        	}
        }, {
        	header : 'Skills',
        	flex: 1,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	tooltip : 'Skills',
        	dataIndex : 'skills',
        	editor: {
        		xtype:'textfield',
        		itemId: 'skills',
        		maskRe: /[A-Za-z0-9 ,]/,
        	}
        }, {
        	header: 'Comments',
        	flex: 1,
        	sortable: false,
        	menuDisabled:true,
        	draggable: false,
        	fixed: true,
        	tooltip : 'Comments',
        	dataIndex : 'comments',
        	editor: {
        		xtype:'textfield',
        		itemId: 'fte_comments',
        		maskRe: /[A-Za-z0-9 ,]/,
        	},
        	renderer:function(value,metaData,record,colIndex,store,view) {					
        		metaData.tdAttr = 'data-qtip="' + record.get("comments") + '"';
        		return value;
        	}
        }];

		//**** End List View Coloumns ****//
		
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'AddFTE',
			style: {
				'margin-right':'10px;'
			},
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function() {
				var r = Ext.create('AM.store.WorkTypeFTE');
				me.editing.cancelEdit();
				me.store.insert(0, r);
				me.editing.startEdit(0, 0);
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});