Ext.define('AM.view.wor_cor.corFTEList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.corFTEList',
	title: 'FTE Details',
	id : 'cor_fte_list',
	layout : 'fit',
	selType: 'rowmodel',
	store: 'corWorkTypeFTE',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		forceFit : true,
	},
	width : '100%',
	minHeight : 225,
	loadMask: true, 
	border : true,
	plugins             : [
	Ext.create('Ext.grid.plugin.RowEditing', {
		clicksToEdit    : 1
	} ) 
	],
	
	initComponent : function() {
		var me = this;
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		startMinDate = new Date(date.getFullYear(), date.getMonth(), 1);

		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true,
			listeners : {
				'beforeedit' :  function(editor, e) 
				{
					if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
					{											
						Ext.getCmp('editSave').setDisabled(true);
						Ext.getCmp('editSubmit').setDisabled(true);
						Ext.getCmp('edit_Volume_id').setDisabled(true);
						Ext.getCmp('edit_Hourly_id').setDisabled(true);
						Ext.getCmp('edit_Project_id').setDisabled(true);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(true);
						Ext.getCmp('addSubmit').setDisabled(true);
						Ext.getCmp('Hourly_id').setDisabled(true);
						Ext.getCmp('Volume_id').setDisabled(true);
						Ext.getCmp('Project_id').setDisabled(true);
					}					
				},

				'validateedit'  : function(editor, e) 
				{
					AM.app.globals.linerecord = '';
					var project_name = no_of_fte = communication = shift  = experience = skills = anticipated_date = support = responsibilities = change_type = null;
					editor.editor.form.getFields().items.forEach(function(entry) {
						no_of_fte   		= Ext.ComponentQuery.query("#no_of_fte")[0].getValue();
						shift    			= Ext.ComponentQuery.query("#shift")[0].getValue();
						appr_buffer    = Ext.ComponentQuery.query("#appr_buffer")[0].getValue();
						experience 			= Ext.ComponentQuery.query("#experience")[0].getValue();
						skills           	= Ext.ComponentQuery.query("#skills")[0].getValue();
						anticipated_date 	= Ext.ComponentQuery.query("#anticipated_date")[0].getValue();
						support         	= Ext.ComponentQuery.query("#support_coverage")[0].getValue();
						responsibilities    = Ext.ComponentQuery.query("#responsibilities")[0].getValue();
						change_type         = Ext.ComponentQuery.query("#change_type")[0].getValue();
						project_name = Ext.ComponentQuery.query("#project_name")[0].getValue();
						if(appr_buffer == undefined || appr_buffer == '')
						{ 
							Ext.ComponentQuery.query("#appr_buffer")[0].setValue(0); 
						}
						
						//Rounding value to 0.25,0.5,0.75 or whole
						Ext.ComponentQuery.query("#no_of_fte")[0].setValue((Math.round(no_of_fte*4)/4).toFixed(2));
					});
					
					if(appr_buffer == 0 && no_of_fte ==0) 
					{
						Ext.MessageBox.alert('Alert', 'Both "Approved Buffer" and "# of FTEs" cannot be Zero');
						return false;
					}
					if(change_type == 2)
					{
						Ext.Ajax.request({
							url: AM.app.globals.appPath+'index.php/Clientcor/chkLineRecords/?v='+AM.app.globals.version,
							method: 'GET',
							params: {
								// 'work_order_id' : Ext.getCmp('previous_work_order_id').getValue(),
								'wor_id' : Ext.getCmp('work_id').getValue(),
								'no_of_fte':no_of_fte,	
								'shift':shift,	
								'experience':experience,	
								'support':support,	
								'responsibilities':responsibilities,	
								'table':'fte_model',	
								'change_type':change_type,	
							},
							scope: this,
							success: function(response) 
							{
								var myObject = Ext.JSON.decode(response.responseText); 
								
								if(myObject.val != 0)
								{
									var startDate = Ext.getCmp('change_request_date').getValue();
									
									if (anticipated_date != null)
									{
										if(startDate != null)
										{
											if(anticipated_date < startDate)
											{
												me.editing.startEdit(0, 0);
												AM.app.globals.linerecord = 1,
												Ext.Msg.alert("Alert", "Change Effective Date should be greater than or equal to Change Request Date");
												return false;
											}
										}
										else
										{
											Ext.Msg.alert("Alert", "Please Enter Change Request Date");
											return false;
										}
									}

									if(project_name != null && no_of_fte != null && shift != null && anticipated_date != null && experience != null && skills != null && skills != '' && support != null && responsibilities != null && change_type != null) 
									{						
										if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
										{
											Ext.getCmp('editSave').setDisabled(false);
											Ext.getCmp('editSubmit').setDisabled(false);
											Ext.getCmp('edit_Volume_id').setDisabled(false);
											Ext.getCmp('edit_Hourly_id').setDisabled(false);
											Ext.getCmp('edit_Project_id').setDisabled(false);
										}
										else if(no_of_fte>='0' && appr_buffer>='0')
										{
											Ext.getCmp('addSave').setDisabled(false);
											Ext.getCmp('addSubmit').setDisabled(false);
											Ext.getCmp('Hourly_id').setDisabled(false);
											Ext.getCmp('Volume_id').setDisabled(false);
											Ext.getCmp('Project_id').setDisabled(false);
										}
										
										return true;
									} 
									else 
									{
										Ext.MessageBox.alert('Alert', 'Please Enter all the Data');
										return false;
									}
								}
								else
								{
									me.editing.startEdit(0, 0);
									AM.app.globals.linerecord = 1,
									Ext.MessageBox.alert('Alert', 'Record is not available for Deletion');
									return false;
								}
							},
						});
					}
					else
					{
						var startDate = Ext.getCmp('change_request_date').getValue();

						if (anticipated_date != null)
						{
							if(startDate != null)
							{
								if(anticipated_date < startDate)
								{
									me.editing.startEdit(0, 0);
									AM.app.globals.linerecord = 1,
									Ext.Msg.alert("Alert", "Change Effective Date should be greater than or equal to Change Request Date");
									return false;
								}
							}
							else
							{
								Ext.Msg.alert("Alert", "Please Enter Change Request Date");
								return false;
							}
						}

						if(project_name != null && no_of_fte != null && shift != null && anticipated_date != null && experience != null && skills != null && skills != '' && support != null && responsibilities != null && change_type != null) 
						{						
							if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
							{
								Ext.getCmp('editSave').setDisabled(false);
								Ext.getCmp('editSubmit').setDisabled(false);
								Ext.getCmp('edit_Volume_id').setDisabled(false);
								Ext.getCmp('edit_Hourly_id').setDisabled(false);
								Ext.getCmp('edit_Project_id').setDisabled(false);
							}
							else if(no_of_fte>='0' && appr_buffer>='0')
							{
								Ext.getCmp('addSave').setDisabled(false);
								Ext.getCmp('addSubmit').setDisabled(false);	
								Ext.getCmp('Hourly_id').setDisabled(false);
								Ext.getCmp('Volume_id').setDisabled(false);
								Ext.getCmp('Project_id').setDisabled(false);
							}
							
							return true;
						} 
						else 
						{
							Ext.MessageBox.alert('Alert', 'Please Enter all the Data');
							return false;
						}
					}
				},

				canceledit: function(grid,obj)
				{
					if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(false);
						Ext.getCmp('editSubmit').setDisabled(false);
						Ext.getCmp('edit_Volume_id').setDisabled(false);
						Ext.getCmp('edit_Hourly_id').setDisabled(false);
						Ext.getCmp('edit_Project_id').setDisabled(false);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);
						Ext.getCmp('Hourly_id').setDisabled(false);
						Ext.getCmp('Volume_id').setDisabled(false);
						Ext.getCmp('Project_id').setDisabled(false);
					}
					
					if(obj.store.data.items[0].data['no_of_fte'] == "")					 
						me.getStore().removeAt(obj.rowIdx);	
					
					if(AM.app.globals.linerecord == 1){			 
						me.getStore().removeAt(obj.rowIdx);
						AM.app.globals.linerecord ='';
					}					 
				},
				scope:me 
			},
		});

Ext.apply(this, {
	plugins: [this.editing]
});

		//**** End Editing Plugin ****//

		//**** List View Coloumns ****//
		this.columns = [{
			header: 'Change Type',
			dataIndex : 'change_type',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			tooltip: 'Change Type',
			editor: {
				xtype:'combobox',
				name: 'change_type',
				store:'ChangeType',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId : 'change_type',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('ChangeType').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			},
		},{
			header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			tooltip: 'Project',
			dataIndex : 'fk_project_type_id',
			editor: {
				xtype:'combobox',
				name: 'fk_project_type_id',
				store:'WorProjects',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 0,
				displayField: 'project_type',
				valueField: 'project_type_id',
				multiSelect: false,
				itemId : 'fte_project_type',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
				listeners : {
					select : function(obj, metaData, record) {
						project_type_id = obj.getValue();
						Ext.getStore('Responsibilities').load({
							params:{ 'project_type_id': obj.getValue() }
						});
					}
				}
			},
			renderer: function(value) {
				var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value);
				return rec == null ? value : rec.get('project_type');
			},
		},{
			header: 'Project Name',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_name',
			tooltip: 'Project Name',
			editor: {
				xtype:'textfield',
				emptyText: 'ClientName_ServiceType',
				itemId: 'project_name',
				maskRe: /[A-Za-z -,_]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
			header: 'Roles',
			flex: 1.5,
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			dataIndex : 'responsibilities',
			tooltip: 'Roles',
			editor: {
				xtype:'combobox',
				name: 'responsibilities',
				store:'Responsibilities',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
				itemId : 'responsibilities',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			},
		},{
			header: '# of FTEs',
			flex: 0.6,
			dataIndex : 'no_of_fte',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			tooltip: '# of FTEs',
			maskRe: /[0-9]/,
			renderer: function(value, meta, record, row, col) {
				if(value % 1 ===0){
					value=Math.round(value);
				}
				return value;
				
			},
			editor: {								
				xtype:'numberfield',
				editable: true,
				minValue: 0,
				maxValue: 1000,
				step: 0.25,
				itemId: 'no_of_fte',
			}
		},{
			header: 'Approved Buffer',
			flex: 0.7,
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			dataIndex : 'appr_buffer',
			tooltip : 'Approved Buffer',
			maskRe: /[0-9]/,
			editor: {								
				xtype:'numberfield',
				editable: true,
				minValue: 0,
				maxValue: 1000,
				step: 1,
				itemId: 'appr_buffer',
				value: 0,
				allowDecimals: false
			}
		},{
			header : 'Shift', 
			flex: 0.7,
			dataIndex : 'shift',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			tooltip : 'Shift', 
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				queryMode: 'remote',
				displayField: 'shift_code',
				valueField: 'shift_id',
				multiSelect: false,
				itemId: 'shift',
				maskRe: /[A-Za-z]/,
				forceSelection: true,
			},
			renderer: function(value) {
				if(value <= 7){
					return value;
				} else {
					var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
					return rec == null ? value : rec.get('shift_code');
				}
			},
		}, {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 0.8,
			tooltip: 'Support Coverage',
			editor: {
				xtype:'combobox',
				name: 'support_coverage',
				store:'SupportCoverage',
				store : Ext.getStore('SupportCoverage'),
				queryMode: 'remote',
				displayField: 'support_type',
				valueField: 'support_id',
				multiSelect: false,
				itemId: 'support_coverage',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
			},
		},{
			header: 'Change Effective Date',
			flex: 1,
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			dataIndex : 'anticipated_date',
			tooltip: 'Change Effective Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			editor: {
				xtype:'datefield',
				format: 'd-M-Y', 
				itemId : 'anticipated_date',
				maxValue: startMaxDate,
				minValue: startMinDate
			}
		}, {
			header : 'Experience in Years', 
			flex: 1,
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			dataIndex : 'experience',
			tooltip : 'Experience in Years', 
			editor: {
				xtype:'numberfield',
				minValue: 0,
				maxValue: 50,
				itemId: 'experience',
				allowDecimals: false
			}
		}, {
			header : 'Skills',
			flex: 1,
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			dataIndex : 'skills',
			tooltip : 'Skills',
			editor: {
				xtype:'textfield',
				itemId: 'skills',
				maskRe: /[A-Za-z0-9 ,]/,
			}
		}, {
			header: 'Comments',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Comments',
			dataIndex : 'comments',
			editor: {
				xtype:'textfield',
				itemId: 'fte_comments',
				maskRe: /[A-Za-z0-9 ,]/,
			},
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("comments") + '"';
				return value;
			}
		}];

		//**** End List View Coloumns ****//
		
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'corAddFTE',
			style: {
				'margin-right':'10px;'
			},
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function() {
				var change_req_date = Ext.getCmp('change_request_date').getValue();
				if(change_req_date != null)
				{
					var r = Ext.create('AM.store.corWorkTypeFTE');
					me.editing.cancelEdit();
					me.store.insert(0, r);
					me.editing.startEdit(0, 0);
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Please Enter Change Request Date',
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK,
						closable: false,
					});
					return false;
				}
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});