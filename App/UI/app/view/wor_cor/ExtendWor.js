Ext.define('AM.view.wor_cor.ExtendWor',{
	extend : 'Ext.window.Window',
	alias : 'widget.ExtendWor',
	layout: 'fit',
	width : 540,
    autoShow : true,
    autoSave: false,
    autoHeight : true,  
    modal: true,
	resizable: false,
    initComponent : function() {

    	me = this;
    	
    	this.items = [{
			xtype : 'form',
			layout: 'vbox',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 145,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype : 'displayfield',
				fieldLabel : 'Date of Entry <span style="color:red">*</span>',
				value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
				name : 'log_date',
				submitValue: true
			}, {
				xtype : 'displayfield',
				fieldLabel : 'New Start Date <span style="color:red">*</span>',
				width : 500,
				name : 'new_start_date',
				value: Ext.getCmp('view_end_date').getValue(),
				submitValue: true
			}, {
				xtype : 'datefield',
				format : AM.app.globals.CommonDateControl,
				fieldLabel : 'New End Date <span style="color:red">*</span>',
				width : 500,
				name : 'new_end_date',
				allowBlank : false,
				minValue: Ext.getCmp('view_end_date').getValue()
			}, {
				xtype : 'textareafield',
				fieldLabel : 'Description <span style="color:red">*</span>',
				width : 500,
				id : 'extenDescription',
				name : 'extenDescription',
				allowBlank: false
			}]
    	}];
		
    	this.buttons = [{
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        },{
			text : 'Submit',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
        }];

		this.callParent(arguments);
	},

	onCancel : function() {
		this.up('window').close();
	},
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();
		
		if (form.isValid()) 
		{
        	AM.app.getController('ClientWOR').onSaveExtendWor(form, win);
        }
	},
});