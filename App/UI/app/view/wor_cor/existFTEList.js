Ext.define('AM.view.wor_cor.existFTEList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.existFTEList',
	title: 'FTE Details',
	id : 'exist_fte_list',
	layout : 'fit',
	selType: 'rowmodel',
	store: 'WorkTypeFTE',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		forceFit : true,
	},
	width : '100%',
	minHeight : 225,
	loadMask: true, 
	border : true,
	
	initComponent : function() {
		var me = this;

		//**** List View Coloumns ****//
		this.columns = [{
			header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			dataIndex : 'fk_project_type_id',
			tooltip: 'Project',
			renderer: function(value) {
                var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
 				return rec == null ? value : rec.get('project_type');
            },
		}, {
			header: 'Roles',
			flex: 1.5,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			dataIndex : 'responsibilities',
			tooltip: 'Roles',
			renderer: function(value) {
				var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			},
		},{
			header: '# of FTEs',
			flex: 0.6,
			dataIndex : 'no_of_fte',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			tooltip: '# of FTEs',
			renderer: function(value, meta, record, row, col) {
				if(value % 1 ===0){
					value=Math.round(value);
				}
				return value;
				
			}
		},{
			header: 'Approved Buffer',
			flex: 0.7,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			dataIndex : 'appr_buffer',
			tooltip : '# of FTEs',
		},{
			header : 'Shift', 
			flex: 0.7,
			dataIndex : 'shift',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			tooltip : 'Shift',
			renderer: function(value) {
				var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
				return rec == null ? value : rec.get('shift_code');
			},
		}, {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			flex: 0.8,
			tooltip: 'Support Coverage',
			renderer: function(value) {
				var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
			},
		},{
			header: 'Anticipated Start Date',
			flex: 1,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			dataIndex : 'anticipated_date',
			tooltip: 'Anticipated Start Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
		}, {
			header : 'Experience in Years', 
			flex: 1,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			dataIndex : 'experience',
			tooltip : 'Experience in Years',
		}, {
			header : 'Skills',
			flex: 1,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			dataIndex : 'skills',
			tooltip : 'Skills',
		}];

		//**** End List View Coloumns ****//
		
		
		this.callParent(arguments);
	},		
});