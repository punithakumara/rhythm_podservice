Ext.define('AM.view.wor_cor.AddWor',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	id : 'add_wor',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	title: 'Add Work Order details',
	border : 'true',
    initComponent : function() {

    	// if (window.history && window.history.pushState) {
			//window.history.pushState('', null, './');
			//window.history.pushState(null, "", window.location.href);
			// window.onpopstate = function () {
			// 	Ext.MessageBox.confirm('Save Changes?', 'Would you like to save your changes?', function(btn){
			// 		if (btn === 'yes'){
			// 			//document.location.href = 'index.html';
			// 		}else {
			// 			var win = Ext.WindowManager.getActive();
			// 			if (win) {
			// 			    win.close();
			// 			}
			// 		}
			// 	});
	
			// };
		 // }
		 if(Ext.getCmp('add_wor')){
    	window.onpopstate = function () {
				Ext.MessageBox.confirm('Save Changes?', 'Would you like to save your changes?', function(btn){
					if (btn === 'yes'){
						//document.location.href = 'index.html';
					}else {
						var win = Ext.WindowManager.getActive();
						if (win) {
						    win.close();
						}
					}
				});
	
			};
		}

    	var me = this;
		var date = new Date(), 
		startMinDate = Ext.Date.add(date, Ext.Date.YEAR, -12), startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 1), 
		endMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
    	
    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addWORFrm',
			width: 1347,
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 145,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '10 10 9 10',
				width: 650,
				title:"Work Order Information",
				items:[{
					xtype:'hidden',
					name:'wor_id',
					id:'wor_id',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order ID <span style="color:red">*</span>',
					value : 'WOR'+Ext.Date.format(new Date(), 'ymdhis'),
					name : 'work_order_id',
					submitValue: true
				}, {
					xtype : 'textfield',
					fieldLabel: 'Work Order Name <span style="color:red">*</span>',
					width: 366,
					allowBlank: false,
					name: 'wor_name',
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'client',
					id: 'client',
					store: Ext.getStore('Clients'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'client_name',
					valueField: 'client_id',
					value: '',
					listeners		: {
						change: function(obj){
							Ext.getStore('WorList').load({
								params:{ 'comboClient': Ext.getCmp('client').getValue()}
							});
						},
						select : function() {							
							Ext.getCmp('previous_work_order_id').setDisabled(false);	
							Ext.getCmp('previous_work_order_id').enable();	
							Ext.getCmp('previous_work_order_id').setValue('');

						},
						beforequery	: function(queryEvent) {
							AM.app.getStore('Clients').getProxy().extraParams = {
								'hasNoLimit'	:'1', 
								'filterName' 	: queryEvent.combo.displayField,
								'clientStatusCombo':'1',
								'worClientsList':'1'
							};
						},
					}
				},{
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Work Order Type <span style="color:red">*</span>',
					name: 'wor_type_id',
					id: 'wor_type_id',
					store: 'WorTypes',
					queryMode: 'remote',
					minChars:0,
					displayField: 'wor_type',
					valueField: 'wor_type_id',
					value:'',
					listeners : {
						select: function(combo)
						{
							var values = combo.value;
							AM.app.globals.wor_type_id = values.toString();
							Ext.getStore('WorProjects').load({
								params:{ 'wor_type_id': values.toString() }
							});
						}
					}
				},{
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Service Type <span style="color:red">*</span>',
					name: 'service_id',
					id: 'service_id',
					store: 'Services',
					queryMode: 'remote',
					minChars:0,
					displayField: 'name',
					valueField: 'service_id',
					value:'',
					listeners : {
						beforequery	: function(queryEvent) {
							AM.app.getStore('Services').getProxy().extraParams = {
								'hasNoLimit'	:'1', 
								'filterName' 	: queryEvent.combo.displayField,
								'servicesStatusCombo':'1',
							};
						}
					}
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Work Order Start Date <span style="color:red">*</span>',
					anchor : '100%',
					name : 'start_date',
					maskRe: /[0-9\/]/,
					id : 'start_date',
					allowBlank : false,
					minValue: startMinDate,
					maxValue: startMaxDate,
					listeners: {
					    select : function(combo) {
							me.query('datefield')[1].setMinValue(me.query('datefield')[0].getValue());
					    }
					}
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Work Order End Date <span style="color:red">*</span>',
					name : 'end_date',
					maskRe: /[0-9\/]/,
					id : 'end_date',
					allowBlank : false,
					maxValue: endMaxDate,
					listeners: {
					    select : function(combo) {
							me.query('datefield')[0].setMaxValue(me.query('datefield')[1].getValue());
					    }
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					readOnly: true,
					maskRe: /[]/,
					name : 'status',
					id : 'wor_status',
					fieldLabel: 'Status <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': 'In-Progress', 'Status': 'In-Progress'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: 'In-Progress',
				}, {
					xtype:'boxselect',
					multiSelect : false,					
					fieldLabel: 'Previous Work Order ID',
					name : 'previous_work_order_id',
					id   : 'previous_work_order_id',
					store: Ext.getStore('WorList'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'work_order_id',
					valueField: 'work_order_id',
					disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							var ClntID =   Ext.getCmp('client').getValue();
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboClient':ClntID,'filterName' : queryEvent.combo.displayField};
						}
					}								
				}]
			}, {
				xtype:'fieldset',
				padding: '10',
				width: 650,
				title:"People Information",
				items:[  {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					store : Ext.getStore('Clientpartner'),
					queryMode: 'remote',
					name: 'client_partner',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					value: '',
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					store: Ext.getStore('Engagementmanagers'),
					queryMode: 'remote',
					name: 'engagement_manager',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					value: '',
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					store: Ext.getStore('Deliverymanagers'),
					queryMode: 'remote',
					name: 'delivery_manager',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					value: '',
				}, {
					xtype : 'boxselect',
					multiSelect: true,
					fieldLabel: 'Associate Manager',
					name: 'associate_manager',
					id: 'associate_manager_add',
					store: 'AssociateManager',
					queryMode: 'remote',
					minChars: 1,
					displayField: 'full_name', 
					valueField: 'employee_id',
					//disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'employee_type':'associate_manager'};
						}
					}
				}, {
					xtype : 'boxselect',
					multiSelect: true,
					fieldLabel: 'Team Lead',
					name: 'team_lead',
					id: 'team_lead',
					store: 'TeamLeader',
					queryMode: 'remote',
					minChars: 1,
					width: 630,
					displayField: 'full_name', 
					valueField: 'employee_id',
					//disabled: true,
					listeners:{
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'employee_type':'team_leader'};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : true,
					allowBlank: true,
					fieldLabel: 'Stake Holder(s)',
					store: Ext.getStore('Stakeholders'),
					queryMode: 'remote',
					name: 'stake_holder',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					width: 630					
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1347,
			minHeight: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				id: 'FTE_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				id: 'Hourly_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				id: 'Volume_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				id: 'Project_id',
				items : [{								
					xtype:AM.app.getView('wor_cor.ProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
    		text : 'Save as Draft',
			id:'addSave',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
        }, {
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
    	   text : 'Submit',
    	   id:'addSubmit',
    	   icon : AM.app.globals.uiPath+'resources/images/submit.png',
    	   handler : this.onSubmit
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus);
			}
		});
	},
	
	onSave :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) 
		{
			Ext.getCmp('addSave').setDisabled(true);
			AM.app.getController('ClientWOR').onSaveWOR(form, 'In-Progress')
		}
		else
		{
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Some mandatory fields are left blank!!',
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK,
				closable: false,
			});
		}
	},

	onSubmit :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) 
		{
			Ext.getCmp('addSubmit').setDisabled(true);
			AM.app.getController('ClientWOR').onSaveWOR(form, 'Open')
		}
		else
		{
			Ext.MessageBox.show({
				title: 'Warning',
				msg: 'Some mandatory fields are left blank!!',
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK,
				closable: false,
			});
		}
	},
});