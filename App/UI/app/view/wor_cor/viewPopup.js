Ext.define('AM.view.wor_cor.viewPopup',{
	extend : 'Ext.window.Window',
	alias : 'widget.worViewPopup',
	layout: 'fit',
	width : '90%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,  
    modal: true,
    initComponent : function() {

    	me = this;
    	
    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addWORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 630,
				items:[{
					xtype:'hidden',
					name:'wor_id',
					id:'wor_id',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Date of Entry <span style="color:red">*</span>',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					name : 'log_date',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'WOR ID <span style="color:red">*</span>',
					value : 'WOR'+Ext.Date.format(new Date(), 'yhis'),
					name : 'work_order_id',
					submitValue: true
				}, {
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Status',
					name:'status',
				    id:'editStatus',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue=='Delete')
							{
								Ext.getCmp('editWORDescription').setVisible(true);
							}
							else
							{
								Ext.getCmp('editWORDescription').setVisible(false);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					width : 630,
					hidden : true,
					id : 'editWORDescription',
					name : 'description',
					allowBlank: true
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'WOR Start Date <span style="color:red">*</span>',
					anchor : '100%',
					name : 'start_date',
					allowBlank : false,
					listeners: {
					    select : function(combo) {
							me.query('datefield')[1].setMinValue(me.query('datefield')[0].getValue());
					    }
					}
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'WOR End Date <span style="color:red">*</span>',
					name : 'end_date',
					allowBlank : false,
					listeners: {
					    select : function(combo) {
							me.query('datefield')[0].setMaxValue(me.query('datefield')[1].getValue());
					    }
					}
				}]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				items:[{
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Previous WOR ID',
					name : 'previous_work_order_id',
					store: Ext.getStore('ClientWOR'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'work_order_id',
					valueField: 'work_order_id',
					emptyText: 'Select Previous Work Order ID',
					width: 630,
					listeners : {
				 		beforequery	: function(queryEvent) {
				 			AM.app.getStore('ClientWOR').getProxy().extraParams = {
					 			'all_wor_ids'	:'1', 
					 		};
					 	},
					}
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'client',
					store: Ext.getStore('Clients'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'client_name',
					valueField: 'client_id',
					emptyText: 'Select Client',
					width: 630,
					listeners : {
				 		beforequery	: function(queryEvent) {
				 			AM.app.getStore('Clients').getProxy().extraParams = {
					 			'hasNoLimit'	:'1', 
					 			'filterName' 	: queryEvent.combo.displayField,
					 		};
					 	},
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					store : Ext.getStore('Employees'),
					queryMode: 'remote',
					name: 'client_partner',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					emptyText: 'Select Client Partner',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'first_name'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					name: 'engagement_manager',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					emptyText: 'Select Engagement Manager',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'first_name'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					name: 'delivery_manager',
					minChars:0,
					displayField: 'full_name',
					valueField	: 'employee_id',
					emptyText: 'Select Delivery Manager',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'first_name'
							};
						}
					}
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1300,
			height: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				items : [{								
					xtype:AM.app.getView('wor_cor.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('wor_cor.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('wor_cor.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('wor_cor.ProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }];

		this.callParent(arguments);
	},
	
	onSave :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) {			  			  
			AM.app.getController('ClientWOR').onSaveWOR(form, Ext.getCmp('editStatus').getValue())
		}
	},
	 onSubmit :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) {
			AM.app.getController('ClientWOR').onSaveWOR(form, 'Open')
		}
	},

	onCancel : function() {
		var win = Ext.WindowManager.getActive();
		win.close();
	},
});