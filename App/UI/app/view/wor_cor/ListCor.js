Ext.define('AM.view.wor_cor.ListCor',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.corList',
	title: 'Change Order List',
    store: 'ClientCOR',
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
    	this.columns = [ {
			header : 'Change Order ID',
			dataIndex : 'change_order_id',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Work Order Name', 
			dataIndex : 'wor_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Client', 
			dataIndex : 'client_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Work Order Type', 
			dataIndex : 'wor_type', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Change Request Date',
			dataIndex : 'change_request_date',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Client Partner',
			dataIndex : 'cp_name',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Engagement Manager',
			dataIndex : 'em_name',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}, {
			header : 'Delivery Manager', 
			dataIndex : 'dm_name', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		},   {
			header : 'Status', 
			dataIndex : 'status',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Action', 
			width : '8%',
			xtype : 'actioncolumn',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			items : ['-', {
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'View',
				handler : this.onView
			},'-',{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
				getClass: function(v, meta, record) {
					if(record.data.status != 'Open' && record.data.status != 'Closed' && record.data.status != 'Deleted' && Ext.util.Cookies.get('grade')>=4)
					{
					  return 'rowVisible';
					}
					else
					{ 
					  return 'x-hide-display';
					}
				}
			}]
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
			store: this.store,
			displayInfo: true,
			pageSize: AM.app.globals.itemsPerPage,
			displayMsg: 'Displaying {0} - {1} of {2} Records',
			emptyMsg: "No records to display",
			plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

		this.callParent(arguments);
	},
	
	tools : [{
		xtype : 'trigger',
        itemId : 'gridTrigger',
        id : 'gridTrigger',
        fieldLabel : '',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
		xtype: 'combobox',
		id:'CORStatusComboBox',
		store: Ext.create('Ext.data.Store', {
			fields: ['Flag', 'Status'],
			data: [
				{'Flag': 'All', 'Status': 'All'},
				{'Flag': 'In-Progress', 'Status': 'In-Progress'},
				{'Flag': 'Open', 'Status': 'Open'},
				{'Flag': 'Closed', 'Status': 'Closed'},
				{'Flag': 'Deleted', 'Status': 'Deleted'}
			]
		}),
		displayField: 'Status', 
		valueField: 'Flag',
		value:'All',
		listeners: {
			select: function(combo){
				AM.app.getController('ClientCOR').onStatusChange(combo);
			}
		}
	}],
		
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientCOR').onEditCOR(grid, rowIndex, colIndex);
	},

	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientCOR').onViewCOR(grid, rowIndex, colIndex);
	}
	
});