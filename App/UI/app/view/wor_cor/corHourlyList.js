Ext.define('AM.view.wor_cor.corHourlyList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.corHourlyList',
	title: 'Hourly Details',
	id : 'cor_hourly_list',
	layout : 'fit',
	selType: 'rowmodel',
	store: 'corWorkTypeHourly',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
	width : '100%',
	minHeight : 225,
	loadMask: true, 
	border : true,
	listeners : {
		sortchange : function(grid, sortInfo){
			this.getStore().getProxy().extraParams = {'RDR_ID':AM.app.globals.mainClientRDR_ID};
		}
	},
	
	initComponent : function() {
		var me = this;
		
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		startMinDate = new Date(date.getFullYear(), date.getMonth(), 1);
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true,
			listeners : {
				'beforeedit' :  function(editor, e) 
				{					
					if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(true);
						Ext.getCmp('editSubmit').setDisabled(true);
						Ext.getCmp('edit_FTE_id').setDisabled(true);
						Ext.getCmp('edit_Volume_id').setDisabled(true);
						Ext.getCmp('edit_Project_id').setDisabled(true);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(true);
						Ext.getCmp('addSubmit').setDisabled(true);
						Ext.getCmp('FTE_id').setDisabled(true);
						Ext.getCmp('Volume_id').setDisabled(true);
						Ext.getCmp('Project_id').setDisabled(true);
					}						
				},
				
				'validateedit'  : function(editor, e) 
				{					
					AM.app.globals.linerecord = '';

					var project_name = hours = shift = hours_min = hours_max = experience = skills = effective_date = support = responsibilities = change_type = null;

					editor.editor.form.getFields().items.forEach(function(entry) {
						hours   			= Ext.ComponentQuery.query("#hours_coverage")[0].getValue();
						shift    			= Ext.ComponentQuery.query("#hourly_shift")[0].getValue();
						hours_min 			= Ext.ComponentQuery.query("#min_hours")[0].getValue();
						hours_max 			= Ext.ComponentQuery.query("#max_hours")[0].getValue();
						experience 			= Ext.ComponentQuery.query("#hourly_experience")[0].getValue();
						skills          	= Ext.ComponentQuery.query("#hourly_skills")[0].getValue();
						effective_date      = Ext.ComponentQuery.query("#hourly_effective_date")[0].getValue();
						support         	= Ext.ComponentQuery.query("#hourly_support_coverage")[0].getValue();
						responsibilities    = Ext.ComponentQuery.query("#hourly_responsibilities")[0].getValue();
						change_type         = Ext.ComponentQuery.query("#hourly_change_type")[0].getValue();
						project_name = Ext.ComponentQuery.query("#project_name")[0].getValue();
					} );

					if(change_type == 2)
					{
						Ext.Ajax.request({
							url: AM.app.globals.appPath+'index.php/Clientcor/chkLineRecords/?v='+AM.app.globals.version,
							method: 'GET',
							params: {
								// 'work_order_id' : Ext.getCmp('previous_work_order_id').getValue(),
								'wor_id' : Ext.getCmp('work_id').getValue(),
								'shift':shift,	
								'experience':experience,	
								'support':support,	
								'responsibilities':responsibilities,	
								'table':'hourly_model',	
								'change_type':change_type,	
							},
							scope: this,
							success: function(response) 
							{
								var myObject = Ext.JSON.decode(response.responseText); 
								
								if(myObject.val != 0)
								{
									var startDate = Ext.getCmp('change_request_date').getValue();

									if (effective_date != null)
									{
										if(startDate != null)
										{
											if(effective_date < startDate)
											{
												Ext.Msg.alert("Alert", "Change Effective Date should be greater than or equal to Change Request Date");
												return false;
											}
										}
										else
										{
											Ext.Msg.alert("Alert", "Please Enter Change Request Date");
											return false;
										}
									}

									if(project_name != null && hours != null && shift != null && hours_min != null && hours_max != null && experience != null && skills != null && skills != '' && effective_date != null && support != "" && responsibilities != null && change_type != null) 
									{						
										if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
										{
											Ext.getCmp('editSave').setDisabled(false);
											Ext.getCmp('editSubmit').setDisabled(false);
											Ext.getCmp('edit_FTE_id').setDisabled(false);
											Ext.getCmp('edit_Volume_id').setDisabled(false);
											Ext.getCmp('edit_Project_id').setDisabled(false);
										}
										else
										{
											Ext.getCmp('addSave').setDisabled(false);
											Ext.getCmp('addSubmit').setDisabled(false);
											Ext.getCmp('FTE_id').setDisabled(false);
											Ext.getCmp('Volume_id').setDisabled(false);
											Ext.getCmp('Project_id').setDisabled(false);	
										}

										return true;
									} 
									else 
									{
										Ext.MessageBox.alert('Alert', 'Please Enter all the Data');
										return false;
									}	
								}
								else
								{
									me.editing.startEdit(0, 0);
									AM.app.globals.linerecord = 1,
									Ext.MessageBox.alert('Alert', 'Record is not available for Deletion');
									return false;
								}
							}
						});
					}
					else
					{	
						var startDate = Ext.getCmp('change_request_date').getValue();

						if (effective_date != null)
						{
							if(startDate != null)
							{
								if(effective_date < startDate)
								{
									Ext.Msg.alert("Alert", "Change Effective Date should be greater than or equal to Change Request Date");
									return false;
								}
							}
							else
							{
								Ext.Msg.alert("Alert", "Please Enter Change Request Date");
								return false;
							}
						}

						if(project_name != null && hours != null && shift != null && hours_min != null && hours_max != null && experience != null && skills != null && skills != '' && effective_date != null && support != "" && responsibilities != null && change_type != null) 
						{						
							if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
							{
								Ext.getCmp('editSave').setDisabled(false);
								Ext.getCmp('editSubmit').setDisabled(false);
								Ext.getCmp('edit_FTE_id').setDisabled(false);
								Ext.getCmp('edit_Volume_id').setDisabled(false);
								Ext.getCmp('edit_Project_id').setDisabled(false);
							}
							else
							{
								Ext.getCmp('addSave').setDisabled(false);
								Ext.getCmp('addSubmit').setDisabled(false);
								Ext.getCmp('FTE_id').setDisabled(false);
								Ext.getCmp('Volume_id').setDisabled(false);
								Ext.getCmp('Project_id').setDisabled(false);	
							}

							return true;
						} 
						else 
						{
							Ext.MessageBox.alert('Alert', 'Please Enter all the Data');
							return false;
						}	

					}
					
					
				},

				canceledit: function(grid,obj)
				{							
					if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(false);
						Ext.getCmp('editSubmit').setDisabled(false);
						Ext.getCmp('edit_FTE_id').setDisabled(false);
						Ext.getCmp('edit_Volume_id').setDisabled(false);
						Ext.getCmp('edit_Project_id').setDisabled(false);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);
						Ext.getCmp('FTE_id').setDisabled(false);
						Ext.getCmp('Volume_id').setDisabled(false);
						Ext.getCmp('Project_id').setDisabled(false);
					}
					if(obj.store.data.items[0].data['hours_coverage'] == "")					 
						me.getStore().removeAt(obj.rowIdx);
					
					if(AM.app.globals.linerecord == 1){			 
						me.getStore().removeAt(obj.rowIdx);
						AM.app.globals.linerecord ='';
					}
				},
				scope:me 
			},
		});

Ext.apply(this, {
	plugins: [this.editing]
});

		//**** End Editing Plugin ****//

		//**** List View Coloumns ****//
		this.columns = [{
			header: 'Change Type',
			dataIndex : 'change_type',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 0.8,
			tooltip: 'Change Type',
			editor: {
				xtype:'combobox',
				name: 'change_type',
				store:'ChangeType',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId : 'hourly_change_type',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('ChangeType').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			}
		},{
			header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			tooltip: 'Project',
			dataIndex : 'fk_project_type_id',
			editor: {
				xtype:'combobox',
				name: 'fk_project_type_id',
				store:'WorProjects',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 0,
				displayField: 'project_type',
				valueField: 'project_type_id',
				multiSelect: false,
				forceSelection: true,
				maskRe: /[A-Za-z]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(Ext.getCmp('cor_wor_type_id').getValue()!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_type_id':Ext.getCmp('cor_wor_type_id').getValue()};
						}
					},
					select : function(obj, metaData, record) {
						project_type_id = obj.getValue();
						Ext.getStore('Responsibilities').load({
							params:{ 'project_type_id': obj.getValue() }
						});
					}
				}
			},
			renderer: function(value) {
				var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value);
				return rec == null ? value : rec.get('project_type');
			},
		},{
			header: 'Project Name',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_name',
			tooltip: 'Project Name',
			editor: {
				xtype:'textfield',
				emptyText: 'ClientName_ServiceType',
				itemId: 'project_name',
				maskRe: /[A-Za-z -,_]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
			header: 'Roles',
			dataIndex: 'responsibilities',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 1.5,
			tooltip: 'Roles',
			editor: {
				xtype:'combobox',
				name: 'responsibilities',
				store:'Responsibilities',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 2,
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId: 'hourly_responsibilities',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			},
		},{
			header: 'Hours Coverage',
			flex: 0.7,
			dataIndex : 'hours_coverage',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip: 'Hours Coverage',
			editor: {
				xtype:'combobox',
				displayField: 'name',
				valueField: 'id',
				store:'DurationCoverage',
				itemId: 'hours_coverage',
				forceSelection: true,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('DurationCoverage').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			},
		}, {
			header : 'Shift',
			flex: 0.7,
			dataIndex : 'shift',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Shift',
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				displayField: 'shift_code',
				valueField: 'shift_id',
				multiSelect: false,
				itemId: 'hourly_shift',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				if(value <= 7){
					return value;
				} else {
					var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
					return rec == null ? value : rec.get('shift_code');
				}
			},
		}, {
			text: "Expected Hours",
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			columns: [{
				header : 'Minimum',
				flex: 1,
				dataIndex : 'min_hours',
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				tooltip : 'Minimum',
				renderer: function(value, meta, record, row, col) {
					if(value <= 0 ){
						return 0;
					}
					else if(record.data.change_type == 2){
						return '-' + value;
					}else{
						return value;
					}
				},
				editor: {
					xtype:'numberfield',
					itemId: 'min_hours',
					decimalPrecision : 0,
					minValue: 0,
					listeners: {
						change : function(obj) {
							obj.next().setMinValue(obj.getValue());
						}
					}
				}
			}, {
				header : 'Maximum',
				flex: 1.2,
				dataIndex : 'max_hours',
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				tooltip : 'Maximum',
				renderer: function(value, meta, record, row, col) {
					if(record.data.change_type == 2){
						return '-' + value;
					}else{
						return value;
					}
				},
				editor: {
					xtype:'numberfield',
					itemId: 'max_hours',
					decimalPrecision : 0,
					minValue: 1,
					listeners: {
						change : function(obj) {
							obj.prev().setMaxValue(obj.getValue());
						}
					}
				}
			}]
		},  {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 0.8,
			tooltip: 'Support Coverage',
			editor: {
				xtype:'combobox',
				name: 'support_coverage',
				store : Ext.getStore('SupportCoverage'),
				queryMode: 'remote',
				displayField: 'support_type',
				valueField: 'support_id',
				multiSelect: false,
				itemId: 'hourly_support_coverage',
				forceSelection: true,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
			},
		}, {
			header: 'Change Effective Date',
			dataIndex: 'anticipated_date',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 1,
			tooltip: 'Change Effective Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
				itemId: 'hourly_effective_date',
				maxValue: startMaxDate,
				minValue: startMinDate
			}
		}, {
			header : 'Experience in Years', 
			dataIndex: 'experience',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 0.9,
			tooltip : 'Experience in Years', 
			editor: {
				xtype:'numberfield',
				itemId: 'hourly_experience',
				minValue: 1,
				maxValue: 50,
				allowDecimals: false
			}
		}, {
			header : 'Skills', 
			dataIndex: 'skills',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 1,
			tooltip : 'Skills', 
			editor: {
				xtype:'textfield',
				itemId: 'hourly_skills',
				maskRe: /[A-Za-z0-9 ,]/,
			}
		}, {
			header: 'Comments',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Comments',
			dataIndex : 'comments',
			editor: {
				xtype:'textfield',
				itemId: 'hourly_comments',
				maskRe: /[A-Za-z0-9 ,]/,
			},
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("comments") + '"';
				return value;
			}
		}];

		//**** End List View Coloumns ****//
		
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'AddHourly',
			style: {
				'margin-right':'10px;'
			},
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function() {
				var change_req_date = Ext.getCmp('change_request_date').getValue();
				if(change_req_date != null)
				{
					var r = Ext.create('AM.store.corWorkTypeHourly');
					me.editing.cancelEdit();
					me.store.insert(0, r);
					me.editing.startEdit(0, 0);
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Please Enter Change Request Date',
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK,
						closable: false,
					});
					return false;
				}
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});