Ext.define('AM.view.wor_cor.corProjectList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.corProjectList',
	title: 'Project Details',
	id: 'cor_project_list',
	layout : 'fit',
	store: 'corWorkTypeProject',
	selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
	width : '100%',
	minHeight : 225,
	loadMask: true, 
	border : true,
	
	initComponent : function() {
		var me = this;
		
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		startMinDate = new Date(date.getFullYear(), date.getMonth(), 1);
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true,
			listeners : {
				'beforeedit' :  function(editor, e) 
				{	
					if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(true);
						Ext.getCmp('editSubmit').setDisabled(true);
						Ext.getCmp('edit_FTE_id').setDisabled(true);
						Ext.getCmp('edit_Volume_id').setDisabled(true);
						Ext.getCmp('edit_Hourly_id').setDisabled(true);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(true);
						Ext.getCmp('addSubmit').setDisabled(true);
						Ext.getCmp('FTE_id').setDisabled(true);
						Ext.getCmp('Hourly_id').setDisabled(true);
						Ext.getCmp('Volume_id').setDisabled(true);
					}					
				},

				'validateedit'  : function(editor, e) 
				{
					AM.app.globals.linerecord = '';
					
					var project_name = shift = duration_start = duration_end = experience = skills = effective_date = support = responsibilities = change_type = project_description = null;
					editor.editor.form.getFields().items.forEach(function(entry) {
						shift    			= Ext.ComponentQuery.query("#project_shift")[0].getValue();
						duration_start 		= Ext.ComponentQuery.query("#start_duration")[0].getValue();
						duration_end 		= Ext.ComponentQuery.query("#end_duration")[0].getValue();
						experience 			= Ext.ComponentQuery.query("#project_experience")[0].getValue();
						skills           	= Ext.ComponentQuery.query("#project_skills")[0].getValue();
						effective_date      = Ext.ComponentQuery.query("#project_effective_date")[0].getValue();
						support         	= Ext.ComponentQuery.query("#project_support_coverage")[0].getValue();
						responsibilities    = Ext.ComponentQuery.query("#project_responsibilities")[0].getValue();
						change_type         = Ext.ComponentQuery.query("#project_change_type")[0].getValue();
						project_description = Ext.ComponentQuery.query("#project_description")[0].getValue();
						project_name = Ext.ComponentQuery.query("#project_name")[0].getValue();
					} );
					
					if(change_type == 2)
					{
						Ext.Ajax.request({
							url: AM.app.globals.appPath+'index.php/Clientcor/chkLineRecords/?v='+AM.app.globals.version,
							method: 'GET',
							params: {
								// 'work_order_id' : Ext.getCmp('previous_work_order_id').getValue(),
								'wor_id' : Ext.getCmp('work_id').getValue(),
								'shift':shift,	
								'experience':experience,	
								'support':support,	
								'responsibilities':responsibilities,	
								'table':'project_model',	
								'change_type':change_type,	
							},
							scope: this,
							success: function(response) 
							{
								var myObject = Ext.JSON.decode(response.responseText); 
								
								if(myObject.val != 0)
								{
									var startDate = Ext.getCmp('change_request_date').getValue();
									
									if (effective_date != null)
									{
										if(startDate != null)
										{
											if(effective_date < startDate)
											{
												Ext.MessageBox.show({
													title: 'Warning',
													msg: "Change Effective Date should be greater than or equal to Change Request Date",
													icon: Ext.MessageBox.WARNING,
													buttons: Ext.Msg.OK,
													closable: false,
												});
												return false;
											}
										}
										else
										{
											Ext.MessageBox.show({
												title: 'Warning',
												msg: "Please Enter Change Request Date",
												icon: Ext.MessageBox.WARNING,
												buttons: Ext.Msg.OK,
												closable: false,
											});
											return false;
										}
									}

									if(startDate != null)
									{
										if(duration_start < startDate)
										{
											Ext.MessageBox.show({
												title: 'Warning',
												msg: "Duration Start Date should be greater than or equal to Change Request Date",
												icon: Ext.MessageBox.WARNING,
												buttons: Ext.Msg.OK,
												closable: false,
											});
											return false;
										}
									}

									if(project_name != '' && shift != null && duration_start != null && duration_end != null && experience != null && skills != null && skills != '' && effective_date != null && support != "" && responsibilities != "" && change_type != "" && project_description != '' && responsibilities != null) 
									{						
										if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
										{
											Ext.getCmp('editSave').setDisabled(false);
											Ext.getCmp('editSubmit').setDisabled(false);
											Ext.getCmp('edit_FTE_id').setDisabled(false);
											Ext.getCmp('edit_Volume_id').setDisabled(false);
											Ext.getCmp('edit_Hourly_id').setDisabled(false);
										}
										else
										{
											Ext.getCmp('addSave').setDisabled(false);
											Ext.getCmp('addSubmit').setDisabled(false);
											Ext.getCmp('Hourly_id').setDisabled(false);
											Ext.getCmp('Volume_id').setDisabled(false);
											Ext.getCmp('FTE_id').setDisabled(false);
										}
										
										return true;
									} 
									else 
									{
										Ext.MessageBox.alert('Alert', 'Please Enter all the Data');
										return false;
									}
									
								}
								else
								{
									me.editing.startEdit(0, 0);
									AM.app.globals.linerecord = 1,
									Ext.MessageBox.alert('Alert', 'Record is not available for Deletion');
									return false;
								}
							}
						});
					}
					else
					{
						var changeStartDate = Ext.getCmp('change_request_date').getValue();

						var startDate = new Date(Ext.getCmp('wor_start_date').getValue());
						var endDate   = new Date(Ext.getCmp('wor_end_date').getValue());

						if (effective_date != null)
						{
							if(startDate != null && endDate != null)
							{
								if((effective_date < startDate || effective_date > endDate))
								{
									Ext.MessageBox.show({
										title: 'Warning',
										msg: "Change Effective Date should be With in Work Order StartDate and EndDate",
										icon: Ext.MessageBox.WARNING,
										buttons: Ext.Msg.OK,
										closable: false,
									});
									return false;
								}
							}
							else
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Please Enter Change Request Date",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
								return false;
							}
						}

						if(changeStartDate != null)
						{
							if(effective_date < changeStartDate)
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Change Effective Date should be greater than or equal to Change Request Date",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
								return false;
							}
						}

						if(changeStartDate != null)
						{
							if(duration_start < changeStartDate)
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Duration Start Date should be greater than or equal to Change Request Date",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
								return false;
							}
						}

						if(duration_start != null && duration_end != null)
						{
							if((duration_start < startDate || duration_start > endDate) || (duration_end < startDate || duration_end > endDate))
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Duration Date should be With in Work Order StartDate and EndDate",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
								return false;
							}
						}
						
						if(shift != null && duration_start != null && duration_end != null && experience != null && skills != null && skills != '' && effective_date != null && support != "" && responsibilities != "" && change_type != "" && project_description != null) 
						{						
							if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
							{
								Ext.getCmp('editSave').setDisabled(false);
								Ext.getCmp('editSubmit').setDisabled(false);
								Ext.getCmp('edit_FTE_id').setDisabled(false);
								Ext.getCmp('edit_Volume_id').setDisabled(false);
								Ext.getCmp('edit_Hourly_id').setDisabled(false);
							}
							else
							{
								Ext.getCmp('addSave').setDisabled(false);
								Ext.getCmp('addSubmit').setDisabled(false);	
								Ext.getCmp('Hourly_id').setDisabled(false);
								Ext.getCmp('Volume_id').setDisabled(false);
								Ext.getCmp('FTE_id').setDisabled(false);
							}
							
							return true;
						} 
						else 
						{
							Ext.MessageBox.alert('Alert', 'Please Enter all the Data');
							return false;
						}
					}
				},

				canceledit: function(grid,obj)
				{	
					if(Ext.getCmp('cor_id').getValue() != undefined && Ext.getCmp('cor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(false);
						Ext.getCmp('editSubmit').setDisabled(false);
						Ext.getCmp('edit_FTE_id').setDisabled(false);
						Ext.getCmp('edit_Volume_id').setDisabled(false);
						Ext.getCmp('edit_Hourly_id').setDisabled(false);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);
						Ext.getCmp('Hourly_id').setDisabled(false);
						Ext.getCmp('Volume_id').setDisabled(false);
						Ext.getCmp('FTE_id').setDisabled(false);	
					} 
					if(obj.store.data.items[0].data['project_name'] == "")					 
						me.getStore().removeAt(obj.rowIdx);
					if(AM.app.globals.linerecord == 1){			 
						me.getStore().removeAt(obj.rowIdx);
						AM.app.globals.linerecord ='';
					}
				},
			},
		});

Ext.apply(this, {
	plugins: [this.editing]
});

		//**** End Editing Plugin ****//
		
		//**** List View Coloumns ****//
		this.columns = [{
			header: 'Change Type',
			dataIndex : 'change_type',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip: 'Change Type',
			editor: {
				xtype:'combobox',
				name: 'change_type',
				store:'ChangeType',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId : 'project_change_type',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('ChangeType').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			}
		},{
			header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			tooltip: 'Project',
			dataIndex : 'fk_project_type_id',
			editor: {
				xtype:'combobox',
				name: 'fk_project_type_id',
				store:'WorProjects',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 0,
				displayField: 'project_type',
				valueField: 'project_type_id',
				multiSelect: false,
				forceSelection: true,
				maskRe: /[A-Za-z]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(Ext.getCmp('cor_wor_type_id').getValue()!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_type_id':Ext.getCmp('cor_wor_type_id').getValue()};
						}
					},
					select : function(obj, metaData, record) {
						project_type_id = obj.getValue();
						Ext.getStore('Responsibilities').load({
							params:{ 'project_type_id': obj.getValue() }
						});
					}
				}
			},
			renderer: function(value) {
				var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value);
				return rec == null ? value : rec.get('project_type');
			},
		},  {
			header: 'Roles',
			dataIndex: 'responsibilities',
			flex: 1.4,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip: 'Roles',
			editor: {
				xtype:'combobox',
				name: 'responsibilities',
				store:'Responsibilities',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 2,
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId: 'project_responsibilities',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
			},
		},{
			header: 'Project Name',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_name',
			tooltip: 'Project Name',
			editor: {
				xtype:'textfield',
				emptyText: 'ClientName_ServiceType',
				itemId: 'project_name',
				maskRe: /[A-Za-z -,_]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
			header: 'Project Description',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_description',
			tooltip: 'Project Description',
			editor: {
				xtype:'textfield',
				itemId: 'project_description',
				maxLength : 150,
				maskRe: /[A-Za-z -,]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
			header : 'Shift',
			flex: 0.8,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'shift',
			tooltip : 'Shift',
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				displayField: 'shift_code',
				valueField: 'shift_id',
				multiSelect: false,
				itemId: 'project_shift',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				if(value <= 7){
					return value;
				} else {
					var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
					return rec == null ? value : rec.get('shift_code');
				}
			},
		}, {
			text: "Duration",
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			columns:[{
				header : 'Start Date',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex : 'start_duration',
				renderer : Ext.util.Format.dateRenderer('d-M-Y'),
				tooltip : 'Start Date',
				editor: {
					xtype:'datefield',
					format: 'd-M-Y',
					itemId: 'start_duration',
					maxValue : startMaxDate,
					minValue: startMinDate,
					listeners: {
						select : function(obj) {
							obj.next().setMinValue(obj.getValue());
						}
					}
				}
			}, {
				header : 'End Date',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex : 'end_duration',
				renderer : Ext.util.Format.dateRenderer('d-M-Y'),
				tooltip : 'End Date',
				editor: {
					xtype:'datefield',
					format: 'd-M-Y',
					itemId: 'end_duration',
					maxValue: startMaxDate,
					minValue: startMinDate,
					listeners: {
						select : function(obj) {
							obj.prev().setMaxValue(obj.getValue());
						}
					}
				}
			}]
		}, {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip: 'Support Coverage',
			editor: {
				xtype:'combobox',
				name: 'support_coverage',
				store : Ext.getStore('SupportCoverage'),
				queryMode: 'remote',
				displayField: 'support_type',
				valueField: 'support_id',
				multiSelect: false,
				itemId: 'project_support_coverage',
				forceSelection: true,
			},
			renderer: function(value) {
				var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
			},
		}, {
			header: 'Change Effective Date',
			dataIndex: 'anticipated_date',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip: 'Change Effective Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
				itemId: 'project_effective_date',
				maxValue: startMaxDate,
				minValue: startMinDate,
			}
		}, {
			header : 'Experience in Years', 
			dataIndex: 'experience',
			flex: 0.8,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Experience in Years', 
			editor: {
				xtype:'numberfield',
				itemId: 'project_experience',
				minValue: 0,
				maxValue: 50,
				allowDecimals: false
			}
		}, {
			header : 'Skills', 
			dataIndex: 'skills',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			flex: 1,
			tooltip : 'Skills', 
			editor: {
				xtype:'textfield',
				itemId: 'project_skills',
				maskRe: /[A-Za-z0-9 ,]/,
			}
		}, {
			header: 'Comments',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Comments',
			dataIndex : 'comments',
			editor: {
				xtype:'textfield',
				itemId: 'project_comments',
				maskRe: /[A-Za-z0-9 ,]/,
			},
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("comments") + '"';
				return value;
			}
		}];

		//**** End List View Coloumns ****//
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'corAddProject',
			style: {
				'margin-right':'10px;'
			},
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function() {
				var change_req_date = Ext.getCmp('change_request_date').getValue();
				if(change_req_date != null)
				{
					var r = Ext.create('AM.store.corWorkTypeProject');
					me.editing.cancelEdit();
					me.store.insert(0, r);
					me.editing.startEdit(0, 0);
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Please Enter Change Request Date',
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK,
						closable: false,
					});
					return false;
				}
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});