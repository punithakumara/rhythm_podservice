Ext.define('AM.view.wor_cor.HourlyList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.HourlyList',
	title: 'Hourly Details',
	id : 'hourly_list',
    layout : 'fit',
    selType: 'rowmodel',
    store: 'WorkTypeHourly',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false,
		stripeRows: false, 
        getRowClass: function(record) {
			if(record.get('process_id') == '' || record.get('process_id') != '0' ){
            return ''; } 
			else{
			return 'child-row';
		}
        } 
	},
    width : '100%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	listeners : {
		sortchange : function(grid, sortInfo){
			this.getStore().getProxy().extraParams = {'RDR_ID':AM.app.globals.mainClientRDR_ID};
		}
	},
	
	initComponent : function() {
		var me = this;
		var project_type_id="";
		
		var date = new Date(), 
		startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		startMinDate = new Date(date.getFullYear(), date.getMonth(), 1);
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true,
			listeners : {
				'beforeedit' :  function(editor, e) 
				{				      
					if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(true);
						Ext.getCmp('editSubmit').setDisabled(true);
						Ext.getCmp('edit_FTE_id').setDisabled(true);
						Ext.getCmp('edit_Volume_id').setDisabled(true);
						Ext.getCmp('edit_Project_id').setDisabled(true);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(true);
						Ext.getCmp('FTE_id').setDisabled(true);
						Ext.getCmp('Volume_id').setDisabled(true);
						Ext.getCmp('Project_id').setDisabled(true);
						Ext.getCmp('addSubmit').setDisabled(true);
					}						
				},
				
				'validateedit'  : function(editor, e) 
				{
					var project_type = project_name = hours = shift = hours_min = hours_max = experience = skills = anticipated_date = support = responsibilities = null;

					editor.editor.form.getFields().items.forEach(function(entry) {
						project_type   		= Ext.ComponentQuery.query("#hourly_project_type")[0].getValue();
						project_name    	= Ext.ComponentQuery.query("#project_name")[0].getValue();
						hours   			= Ext.ComponentQuery.query("#hours_coverage")[0].getValue();
						shift    			= Ext.ComponentQuery.query("#hourly_shift")[0].getValue();
						hours_min 			= Ext.ComponentQuery.query("#min_hours")[0].getValue();
						hours_max 			= Ext.ComponentQuery.query("#max_hours")[0].getValue();
						experience 			= Ext.ComponentQuery.query("#hourly_experience")[0].getValue();
						skills           	= Ext.ComponentQuery.query("#hourly_skills")[0].getValue();
						anticipated_date    = Ext.ComponentQuery.query("#hourly_anticipated_date")[0].getValue();
						support         	= Ext.ComponentQuery.query("#hourly_support_coverage")[0].getValue();
						responsibilities	= Ext.ComponentQuery.query("#hourly_responsibilities")[0].getValue();
					} );
					if(hours_max<hours_min)
					{
						Ext.MessageBox.show({
							title: 'Warning',
							msg: "Maximum hours is less than minimum hours",
							icon: Ext.MessageBox.WARNING,
							buttons: Ext.Msg.OK,
							closable: false,
						});
					    return false;
					}
					var startDate = Ext.getCmp('start_date').getValue();
					var endDate   = Ext.getCmp('end_date').getValue();
					if(anticipated_date != null)
					{
						if(startDate != null && endDate != null)
						{
							if(anticipated_date < startDate || anticipated_date > endDate)
							{
								Ext.MessageBox.show({
									title: 'Warning',
									msg: "Anticipated start Date should be With in Work Order StartDate and EndDate",
									icon: Ext.MessageBox.WARNING,
									buttons: Ext.Msg.OK,
									closable: false,
								});
					            return false;
							}
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Warning',
								msg: "Please Enter Work Order StartDate and EndDate",
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.Msg.OK,
								closable: false,
							});
				            return false;
						}
					}
	 
					if(project_name != null && hours != null && shift != null && hours_min != null && hours_max != null && experience != null && skills != null && skills != '' && anticipated_date != null && support != "" && responsibilities != "" && responsibilities != null) 
					{
						if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
						{
							Ext.getCmp('editSave').setDisabled(false);
							Ext.getCmp('editSubmit').setDisabled(false);
							Ext.getCmp('edit_FTE_id').setDisabled(false);
							Ext.getCmp('edit_Volume_id').setDisabled(false);
							Ext.getCmp('edit_Project_id').setDisabled(false);
						}
						else
						{
							Ext.getCmp('addSave').setDisabled(false);
							Ext.getCmp('addSubmit').setDisabled(false);	
							Ext.getCmp('FTE_id').setDisabled(false);
							Ext.getCmp('Volume_id').setDisabled(false);
							Ext.getCmp('Project_id').setDisabled(false);
						}
						
						return true;
					} 
					else 
					{
						Ext.MessageBox.show({
							title: 'Warning',
							msg: "Please Enter all the Data",
							icon: Ext.MessageBox.WARNING,
							buttons: Ext.Msg.OK,
							closable: false,
						});
						return false;
					}
				},

				canceledit: function(grid,obj)
				{
				    if(Ext.getCmp('wor_id').getValue() != undefined && Ext.getCmp('wor_id').getValue() != '')
					{
						Ext.getCmp('editSave').setDisabled(false);
						Ext.getCmp('editSubmit').setDisabled(false);
						Ext.getCmp('edit_FTE_id').setDisabled(false);
						Ext.getCmp('edit_Volume_id').setDisabled(false);
						Ext.getCmp('edit_Project_id').setDisabled(false);
					}
					else
					{
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);	
						Ext.getCmp('FTE_id').setDisabled(false);
						Ext.getCmp('Volume_id').setDisabled(false);
						Ext.getCmp('Project_id').setDisabled(false);
					}
					if(obj.store.data.items[0].data['hours_coverage'] == "")					 
						me.getStore().removeAt(obj.rowIdx);
				},
				scope:me 
			},
		});
		
		Ext.apply(this, {
			plugins: [this.editing]
		});
		
		//**** End Editing Plugin ****//
		 
		//**** List View Coloumns ****//
    	this.columns = [{
			header:'Project',
			tooltip:'Project',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'fk_project_type_id',
			editor: {
				xtype:'combobox',
				name: 'fk_project_type_id',
				store:'WorProjects',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 0,
				displayField: 'project_type',
				valueField: 'project_type_id',
				multiSelect: false,
				itemId : 'hourly_project_type',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(AM.app.globals.wor_type_id!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_type_id':AM.app.globals.wor_type_id};
						}
					},
					select : function(obj, metaData, record) {
						project_type_id = obj.getValue();
						Ext.getStore('Responsibilities').load({
							params:{ 'project_type_id': obj.getValue() }
						});
					}
				}
			},
			renderer: function(value) {
                var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
 				return rec == null ? value : rec.get('project_type');
            },
		},{
			header: 'Project Name',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'project_name',
			tooltip: 'Project Name',
			editor: {
				xtype:'textfield',
				emptyText: 'ClientName_ServiceType',
				itemId: 'project_name',
				maskRe: /[A-Za-z -,_]/,
			},
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
				if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
				return value;        
			},
		}, {
			header: 'Roles',
			dataIndex: 'responsibilities',
			flex: 1.5,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Roles',
			editor: {
				xtype:'combobox',
				name: 'responsibilities',
				store:'Responsibilities',
				queryMode: 'local',
				typeAhead: true, 
				minChars: 2,
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				itemId: 'hourly_responsibilities',
				forceSelection: true,
				maskRe: /[A-Za-z -]/,
				listeners : {
					beforequery : function(queryEvent) {
						if(project_type_id!=undefined)
						{
							queryEvent.combo.getStore().getProxy().extraParams = {'project_type_id':project_type_id};
						}
					}
				}
			},
			renderer: function(value) {
				console.log(value);
                var rec	= Ext.getStore('Responsibilities').findRecord('id', value, 0, false, true, true);
                console.log(rec);
				return rec == null ? value : rec.get('name');
            },
		},{
			header: 'Hours Coverage',
			flex: 0.7,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip: 'Hours Coverage',
			dataIndex : 'hours_coverage',
			editor: {
				xtype:'combobox',
				displayField: 'name',
				valueField: 'id',
				store:'DurationCoverage',
				itemId: 'hours_coverage',
				forceSelection: true,
			},
			renderer: function(value, meta, record) {
                var rec	= Ext.getStore('DurationCoverage').findRecord('id', value);
				var val = (rec == null) ? value : rec.get('name');
				if(record.data.fk_cor_id!=null)
				{
					val = '<a href="#" style="text-decoration: none" onclick=AM.app.getController("ClientCOR").onViewCORFromWor("'+record.data.fk_cor_id+'")>'+ val +'</a>';
				}
				return val;
            },
		}, {
			header : 'Shift',
			flex: 0.7,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'shift',
			tooltip : 'Shift',
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				displayField: 'shift_code',
				valueField: 'shift_id',
				multiSelect: false,
				itemId: 'hourly_shift',
				forceSelection: true,
				maskRe: /[A-Za-z]/,
			},
			renderer: function(value) {
				if(value <= 7){
					return value;
				} else {
	                var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
					return rec == null ? value : rec.get('shift_code');
				}
            },
		}, {
			text: "Expected Hours",
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			columns: [{
				header : 'Minimum',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				dataIndex : 'min_hours',
				tooltip : 'Minimum',
				renderer: function(value, meta, record, row, col) {
					if(value <= 0 ){
						return 0;
					}
					else if(record.data.change_type == 2){
						return '-' + value;
					}else{
						return value;
					}
				},
				editor: {
					xtype:'numberfield',
					itemId: 'min_hours',
					decimalPrecision : 0,
					minValue: 1,
					listeners: {
						change : function(obj) {
							obj.next().setMinValue(obj.getValue());
						}
					}
				}
			}, {
				header : 'Maximum',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				fixed: true,
				dataIndex : 'max_hours',
				tooltip : 'Maximum',
				renderer: function(value, meta, record, row, col) {
					if(record.data.change_type == 2){
						return '-' + value;
					}else{
						return value;
					}
				},
				editor: {
					xtype:'numberfield',
					itemId: 'max_hours',
					decimalPrecision : 0,
					minValue: 1,
					listeners: {
						change : function(obj) {
							obj.prev().setMaxValue(obj.getValue());
						}
					}
				}
			}]
		},  {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			flex: 0.8,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Support Coverage',
			editor: {
				xtype:'combobox',
				name: 'support_coverage',
			    store : Ext.getStore('SupportCoverage'),
				queryMode: 'remote',
				displayField: 'support_type',
				valueField: 'support_id',
				multiSelect: false,
				itemId: 'hourly_support_coverage',
				forceSelection: true,
			},
			renderer: function(value) {
                var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
            },
		}, {
			header: 'Anticipated Start Date',
			dataIndex: 'anticipated_date',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Anticipated Start Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
				itemId: 'hourly_anticipated_date',
				maxValue: startMaxDate,
				minValue: startMinDate
			}
		}, {
			xtype:'hidden',
			header: 'Actual Start Date',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			dataIndex : 'actual_date',
			tooltip : 'Actual Start Date',
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
				itemId : 'hourly_actual_date',
				maxValue: startMaxDate,
				minValue: startMinDate
			}
		}, {
			header : 'Experience in Years', 
			dataIndex: 'experience',
			flex: 0.9,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Experience in Years',
			editor: {
				xtype:'numberfield',
				itemId: 'hourly_experience',
				minValue: 0,
				maxValue: 50,
				allowDecimals: false
			}
		}, {
			header : 'Skills', 
			dataIndex: 'skills',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Skills',
			editor: {
				xtype:'textfield',
				itemId: 'hourly_skills',
				maskRe: /[A-Za-z0-9 ,]/,
			}
		}, {
			header: 'Comments',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			fixed: true,
			tooltip : 'Comments',
			dataIndex : 'comments',
			editor: {
				xtype:'textfield',
				itemId: 'hourly_comments',
				maskRe: /[A-Za-z0-9 ,]/,
			},
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("comments") + '"';
				return value;
			}
		}];

		//**** End List View Coloumns ****//
		
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'AddHourly',
			style: {
				'margin-right':'10px;'
			},
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			handler : function() {
				var r = Ext.create('AM.store.WorkTypeHourly');
				me.editing.cancelEdit();
				me.store.insert(0, r);
				me.editing.startEdit(0, 0);
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});