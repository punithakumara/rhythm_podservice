Ext.define('AM.view.wor_cor.ViewRes',{
	extend : 'Ext.window.Window',
	alias : 'widget.ViewRes',
	layout: 'fit',
	store: 'AllocatedRes',
	width : '90%',
	height: 555,
	autoScroll: true,
	autoHeight : true,   
	autoShow : true,
	closable: false,
	modal: true,
	title: 'View Employees',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
	initComponent : function() {

		var me = this, intialStore = "";
		
		this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items:[{
				region:'west',
				// xtype: 'insightTopPanel',
				width: '20%',
				border: true,
				height: 400,
				margin : '0 5px 5px 0',
				layout: {
					type: 'vbox',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : '<b>Client</b> <span style="color:red">*</span>',
					width: 220,
					name: 'res_client',
					id: 'res_client',
					labelAlign: 'top',
					style:{
						'margin':'10px 0 0 5px'
					},
				}, {
					xtype : 'hiddenfield',
					name: 'res_client_id',
					id: 'res_client_id',
				},{
					xtype:'hidden',
					name:'res_add_client_id',
					id:'res_add_client_id',
				}, {
					xtype : 'hiddenfield',
					name: 'res_wor_id',
					id: 'res_wor_id',
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order ID</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_id',
					name: 'res_work_order_id',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order Name</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_name',
					name: 'res_work_order_name',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Project</b>',
					anchor : '100%',
					width: 220,
					id:'project_type_id',
					name: 'project_type_id',
					labelAlign: 'top',
					labelWidth : 150,
					store: 'WorProjects',
					queryMode: 'remote',
					displayField: 'project_type',
					submitValue : true,
					valueField: 'project_type_id',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					listeners:{
		                beforequery: function(queryEvent){
		                    Ext.Ajax.abortAll();
		                    queryEvent.combo.getStore().getProxy().extraParams = {'work_order_id':Ext.getCmp('wor_id').getValue()};
		                },
						select: function(obj){
							if(obj.getValue()!="")
							{
								Ext.Ajax.request({
									url: AM.app.globals.appPath+'index.php/Clientwor/wor_project_roles?v='+AM.app.globals.version,
									method: 'POST',
									params: {
										'wor_id':Ext.getCmp('wor_id').getValue(),
										'project_id':obj.getValue(),
									},
									scope: this,
									success: function(response) {
										var myObject = Ext.JSON.decode(response.responseText);
										Ext.getCmp('view_job_responsibilities').setValue(myObject.rows.role_type);
									}
								});
								Ext.getCmp('view_resources_grid').getStore().load({params: {'wor_id':Ext.getCmp('wor_id').getValue(),'client_id' : Ext.getCmp('client_id').getValue(), 'project_type_id': Ext.getCmp('project_type_id').getValue() }});
							}
							else
							{
								Ext.getCmp('view_job_responsibilities').setValue('');
							}
						},
		            }
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Roles under this project</b>',
					width: 220,
					id: 'view_job_responsibilities',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Remove Date</b> <span style="color:red">*</span>',
					anchor : '100%',
					hidden: true,
					name : 'transition_date',
					id :   'transition_date',
					maskRe: /[0-9\/]/,
					allowBlank : false,
					maxValue : new Date(),
					minValue: Ext.Date.add(new Date(), Ext.Date.MONTH, -3), 
					submitValue : true,
					width: 220,
					labelWidth : 150,
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					}
				}]
			}, {
				region:'center',
				width: '69%',
				items:[{
					xtype : 'form',
					layout: 'hbox',
					id : 'addResFrm',
					items:[{
						xtype : 'textfield',
						format : AM.app.globals.CommonDateControl,
						fieldLabel : 'Employee ID/Name ',
						anchor : '100%',
						width: 500,
						name : 'resource_id',
						id : 'resource_id',
						labelWidth : 150,
						listeners: {
							change: function(field, e) {
								//if(e.getKey() == e.ENTER) {
									me.onClick(me)
								//}
							}
						}
					}, {
						xtype:'tbspacer',
						width: 15
					}, {
						xtype:'button',
						text:'Search',
						id:'transSecondSearch',
						width:60,
						listeners:{
							'click':function(){
								me.onClick(me)
							}
						}
					}]
				},{
					xtype:'gridpanel',
					border:true,
					margin:'10 0 5 0',
					height:415,
					id:'view_resources_grid',
					selType: 'checkboxmodel',
					selModel: {
						checkOnly: true,
						mode:'MULTI',
						listeners: {
							deselect: function(model, record, index) {
								var myAry = AM.app.globals.transEmployRes;						
								for(var i =0; i < myAry.length; i++)
								{
									if (myAry[i] != null && (myAry[i].data.employee_id == record.data.employee_id))
									{
										myAry[i] = null;
									}
								}
								
								myAry = myAry.filter(function(e){return e;});
								myAry = Ext.Array.unique(myAry);
								if(myAry.length<1)
								{
									Ext.getCmp('removeEmploySubmit').setVisible(false);
									Ext.getCmp('transition_date').setVisible(false);
								}
							},
							select: function(model, record, index) {
								if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
								{							
									AM.app.globals.transEmployRes.push(record);	
									Ext.getCmp('removeEmploySubmit').setVisible(true);
									Ext.getCmp('transition_date').setVisible(true);
								}
							}
						}
					} ,
					columns: [{
						header: 'POD',  
						dataIndex: 'podName',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Service',  
						dataIndex: 'serviceName',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Employee ID',  
						dataIndex: 'company_employ_id',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Employee Name',  
						dataIndex: 'FullName',  
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Designation',  
						dataIndex: 'designationName',  
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Email', 
						dataIndex: 'Email', 
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Supervisor', 
						dataIndex: 'supervisor', 
						flex: 3,
						menuDisabled:true,
					}],
					
					bbar: Ext.create('Ext.PagingToolbar', {
						id:'view_resources_gridPage',
						store: this.store,
						displayInfo: true,
						listeners: {
							beforechange: function(item1,item2,item3){
								this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(), 'manager' : Ext.util.Cookies.get('employee_id'), 'wor_id':Ext.getCmp('wor_id').getValue(), 'client_id':Ext.getCmp('client_id').getValue()}																
							},
							change:function(){
								var myGrid = Ext.getCmp('view_resources_grid');
								
								for(i=0; i < myGrid.store.data.length; i++)
								{
									var user = myGrid.store.getAt(i);							
									var myAry = AM.app.globals.transEmployRes;
									
									for (var j=0; j< myAry.length; j++)
									{
										if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
											myGrid.selModel.doMultiSelect(user,true);
									}	
								}
								Ext.getCmp('resource_id').focus();
							}
						},
						displayMsg: 'Displaying employees {0} - {1} of {2}',
						emptyMsg: "No employees to display.",
						pageSize: AM.app.globals.itemsPerPage,
						plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
					}),
					listeners: {
						sortchange :  function(ct, column) {
							this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(),'manager' : Ext.util.Cookies.get('employee_id'),'wor_id':Ext.getCmp('wor_id').getValue(),'client_id':Ext.getCmp('client_id').getValue()}																		
							this.store.load();
						}
					}
				}]
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Remove Employee',
			id:'removeEmploySubmit',
			hidden:true,
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
		}];

		this.callParent(arguments);
	},	

	onCancel : function(win) {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				win.up('window').close();
			}
		});
	},

	onSubmit :  function(btn) {
		AM.app.getController('ClientWOR').onSubmit(this.up('grid'), Ext.getCmp('client_id').getValue(), Ext.getCmp('wor_id').getValue(), Ext.getCmp('project_type_id').getValue());
	},

	onClick:function(my)
	{
		var empParam = Ext.getCmp('resource_id').getValue();
		
		if(empParam != "" && empParam != null)
		{
			var MyStore = Ext.getStore('AllocatedRes').load({
				params:{'manager' : Ext.util.Cookies.get('employee_id'), 'searchTxt': empParam,'client_id':Ext.getCmp('client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}
			})
			
			my.intialStore = MyStore;			
			Ext.getCmp('view_resources_grid').bindStore(MyStore);
			Ext.getCmp('view_resources_gridPage').bindStore(MyStore);
		}
		else
		{
			Ext.getCmp('view_resources_grid').store.removeAll();
			var MyStore = Ext.getStore('AllocatedRes').load({
				params:{'manager' : Ext.util.Cookies.get('employee_id'), 'searchTxt': empParam,'client_id':Ext.getCmp('client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}
			})
		}
	}
	
});