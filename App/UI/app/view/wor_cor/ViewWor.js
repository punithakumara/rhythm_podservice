Ext.define('AM.view.wor_cor.ViewWor',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
		type: 'vbox',
		align: 'center'
	},
	border : 'true',
	initComponent : function() {

		// if (window.history && window.history.pushState) {
		// 	//window.history.pushState('', null, './');
		// 	window.history.pushState(null, "", window.location.href);
		// 	window.onpopstate = function () {
		// 		Ext.MessageBox.confirm('Save Changes?', 'Would you like to save your changes?', function(btn){
		// 			if (btn === 'yes'){
		// 				//document.location.href = 'index.html';
		// 			}else if(btn === 'no') {
		// 				//var win = Ext.WindowManager.getActive();
		// 				//if (win) {
		// 					AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus);
		// 				//}
		// 			} else {
		// 				window.close();
		// 			}
		// 		});
	
		// 	};
		// }

		this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addWORFrm',
			width: 1347,
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 145,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '10',
				width: 650,
				title:"Work Order Information",
				items:[{
					xtype:'hidden',
					name:'wor_id',
					id:'wor_id',
				},{
					xtype:'hidden',
					name:'wor_type_id',
					id:'view_wor_type_id',
				},{
					xtype:'hidden',
					name:'service_id',
					id:'view_service_id',
				},{
					xtype:'hidden',
					name:'client',
					id:'client_id',
				},{
					xtype:'hidden',
					name:'client_partner',
					id:'client_partner',
				},{
					xtype:'hidden',
					name:'engagement_manager',
					id:'engagement_manager',
				},{
					xtype:'hidden',
					name:'delivery_manager',
					id:'delivery_manager',
				},{
					xtype:'hidden',
					name:'stake_holder',
					id:'stake_holder',
				},{
					xtype:'hidden',
					name:'associate_manager',
					id:'associate_manager',
				},{
					xtype:'hidden',
					name:'team_lead',
					id:'team_lead',
				}, {
					xtype : 'hidden',
					//fieldLabel : 'count',
					name : 'count_history',
					id:'count_history',
				},  {
					xtype : 'hidden',
					//fieldLabel : 'orginalcount',
					name : 'count_history',
					id:'orig_history',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Work Order ID <span style="color:red">*</span>',
					name : 'work_order_id',
					id : 'work_order_id',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order Name <span style="color:red">*</span>',
					name : 'wor_name',
					id : 'wor_name',
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'client_name',
					id: 'Vclient_name',
					width: 630,
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order Type <span style="color:red">*</span>',
					name : 'wor_type',
					id : 'wor_type',
				},{
					xtype : 'displayfield',
					fieldLabel : 'Service Type <span style="color:red">*</span>',
					name : 'name',
					id : 'view_service',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order Start Date <span style="color:red">*</span>',
					name : 'start_date',
					id : 'view_start_date'
				}, {
					xtype : 'fieldcontainer',
					fieldLabel : 'Work Order End Date <span style="color:red">*</span>',
					flex: 1,
					layout: 'hbox',
					items:[{
						xtype : 'displayfield',
						name : 'end_date',
						id : 'view_end_date'
					}, {
						xtype: 'displayfield',
						id: 'extendWOR',
						hidden: true,
						value: '<a href="#" onclick=AM.app.getController("ClientWOR").onExtendWor()>Extend</a>',
						style: {
							'margin-left':'15px',
						},
					}]
				},{
					xtype:'displayfield',
					fieldLabel: 'Status',
					name:'status',
					id: 'inprogressStatus',
					hidden: true
				},{
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					hidden: true,
					fieldLabel: 'Status <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
						{'Flag': 'Open', 'Status': 'Open'},
						{'Flag': 'Close', 'Status': 'Close'},
						]
					}),
					name:'status',
					id: 'openStatus',
					displayField: 'Status',
					valueField: 'Flag',
					value : 'Open',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue=='Close')
							{
								Ext.getCmp('ViewWORDescription').setVisible(true);
								if(Ext.util.Cookies.get('grade') >=4)
								{
									Ext.getCmp('saveButtonID').setVisible(true);
									Ext.getCmp('saveButtonID').setText("Submit");
								}
								Ext.getCmp('cp_edit').setVisible(false);
								Ext.getCmp('em_edit').setVisible(false);
								Ext.getCmp('dm_edit').setVisible(false);
								Ext.getCmp('am_edit').setVisible(false);
								Ext.getCmp('tl_edit').setVisible(false);
								Ext.getCmp('sh_edit').setVisible(false);
								Ext.getCmp('ta_edit').setVisible(false);
								Ext.getCmp('addResources').setVisible(false);
								Ext.getCmp('viewResources').setVisible(false);
								Ext.getCmp('configureFields').setVisible(false);
								Ext.getCmp('peopleBillability').setVisible(false);
								Ext.getCmp('assignHolidays').setVisible(false);
								Ext.getCmp('addCor').setVisible(false);
							}
							else
							{
								Ext.getCmp('ViewWORDescription').setVisible(false);
								Ext.getCmp('saveButtonID').setVisible(false);
								Ext.getCmp('cp_edit').setVisible(true);
								Ext.getCmp('em_edit').setVisible(true);
								Ext.getCmp('dm_edit').setVisible(true);
								Ext.getCmp('am_edit').setVisible(true);
								Ext.getCmp('tl_edit').setVisible(true);
								Ext.getCmp('sh_edit').setVisible(true);
								Ext.getCmp('ta_edit').setVisible(true);
								Ext.getCmp('addCor').setVisible(true);
								Ext.getCmp('addResources').setVisible(true);
								Ext.getCmp('viewResources').setVisible(true);
								Ext.getCmp('peopleBillability').setVisible(true);
								Ext.getCmp('configureFields').setVisible(true);
								Ext.getCmp('assignHolidays').setVisible(true);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					width : 630,
					id : 'ViewWORDescription',
					name : 'description',
					allowBlank: false,
					hidden: true,
				},{
					xtype : 'displayfield',
					fieldLabel : 'Associated Change Order(s)',
					name : 'corids',
					id : 'corids',
					cls: 'text-wrapper',
				}]
			}, {
				xtype:'fieldset',
				padding: '10',
				width: 650,
				title:"People Information",
				items:[{
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "cpLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel: 'Client Partner <span style="color:red">*</span>',
						name: 'cp_name',
						id: 'cp_name',
					},{
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'cp_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('cpLayout');
						}
					}]
				}, {
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "emLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
						name: 'em_name',
						id: 'em_name',
					},{
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'em_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('emLayout');
						}
					}]
				}, {
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "dmLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
						name: 'dm_name',
						id: 'dm_name',
					},{
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'dm_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('dmLayout');
						}
					}]
				}, {
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "amLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel: 'Associate Manager',
						name: 'am_name',
						id: 'am_name',
						renderer: function(value, field) {
							if(value.length>40)
							{
								return value.substring(0,60)+'...';
							}
							else
							{
								return value;
							}
						}
					},{
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'am_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('amLayout');
						}
					}]
				}, {
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "tlLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel : 'Team Lead',
						name: 'tl_name',
						id: 'tl_name',
						renderer: function(value, field) {
							if(value.length>40)
							{
								return value.substring(0,60)+'...';
							}
							else
							{
								return value;
							}
						}
					},{
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'tl_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('tlLayout');
						}
					}]
				}, {
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "shLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel : 'Stakeholder(s)',
						name: 'sh_name',
						id: 'sh_name',
						renderer: function(value, field) {
							if(value.length>40)
							{
								return value.substring(0,60)+'...';
							}
							else
							{
								return value;
							}
						}
					}, {
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'sh_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('shLayout');
						}
					}]
				}, {
					xtype : 'panel',
					flex: 1,
					layout: 'hbox',
					id: "taLayout",
					style: {
						'padding-bottom':'8px'
					},
					items:[{
						xtype:'displayfield',
						fieldLabel : 'Time Entry Approver(s)',
						name: 'ta_name',
						renderer: function(value, field) {
							if(value.length>40)
							{
								return value.substring(0,60)+'...';
							}
							else
							{
								return value;
							}
						}
					}, {
						xtype:'button',
						icon : AM.app.globals.uiPath+'/resources/images/edit.png',
						value : 'Change',
						id: 'ta_edit',
						hidden: true,
						style: {
							'margin-left':'10px',
						},
						handler: function() {
							AM.app.getController('ClientWOR').onReplaceNew('taLayout');
						}
					}]
				}, {
					xtype:'hidden',
					id: 'resourceCount',
					name: 'resourceCount',
					width: 630,
					//submitValue: true,
				}]
			}]
		}, {
			xtype: 'tabpanel',
			id:'myTabPanel',
			activeTab: 0,
			width: 1347,
			minHeight: 300,
			padding: '0 0 10px 0',
			autoHeight: true,
			scroll: 'vertical',
		}];
		
		this.tools = [{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'/resources/images/assignemployees_32x32.png',
			tooltip : 'Assign Employee(s)',
			id: 'addResources',
			hidden:true,
			width: 35,
			height: 32,
			style :{
				padding : '0px',
				background : 'none',
				border : 'none',
			},
			cls :'wor-tab-icon',
			handler : this.assignResources
		},{
			xtype: 'tbspacer',
			width: 20
		},{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'/resources/images/viewemployees_32x32.png',
			handler : this.viewResources,
			tooltip : 'View Assigned Employee(s)',
			id : 'viewResources',
			cls :'wor-tab-icon',
			hidden:true,
			width: 35,
			height: 32,
			style :{
				padding : '0px',
				background : 'none',
				border : 'none',
			}
		},{
			xtype: 'tbspacer',
			width: 20
		},{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'/resources/images/billability_32x32.png',
			handler : this.peopleBillability,
			tooltip : 'Billability',
			id : 'peopleBillability',
			cls :'wor-tab-icon',
			hidden:true,
			width: 35,
			height: 32,
			style :{
				padding : '0px',
				background : 'none',
				border : 'none',
			}
		},{
			xtype: 'tbspacer',
			width: 20
		},{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'/resources/images/configuretime_32x32.png',
			handler: this.ConfigureAttributes,
			tooltip : 'Configure Time Entry Fields',
			id: 'configureFields',
			cls :'wor-tab-icon',
			hidden:true,
			width: 35,
			height: 32,
			style :{
				padding : '0px',
				background : 'none',
				border : 'none',
			}
		},{
			xtype: 'tbspacer',
			width: 20
		},{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'/resources/images/assignclientholidays_32x32.png',
			handler: function () {
				AM.app.getController('ClientWOR').onAddClientHoliday();
			},
			tooltip : 'Assign Client Holiday',
			id: 'assignHolidays',
			cls :'wor-tab-icon',
			hidden:true,
			width: 35,
			height: 32,
			style :{
				padding : '0px',
				background : 'none',
				border : 'none',
			}
		},{
			xtype: 'tbspacer',
			width: 10
		},{
			xtype : 'button',
			id:'addCor',
			height: 32,
			hidden:true,
			icon: AM.app.globals.uiPath+'/resources/images/Add.png',
			handler: function () {
				AM.app.getController('ClientCOR').onCreateCOR();
			},
			text : 'Add COR'
		},{
			xtype: 'tbspacer',
			width: 10
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : function() {
				var win = this.up('panel');
				var form = win.down('form').getForm();
				var record = form.getRecord(),values = form.getValues();
				if (form.isValid()) 
				{					
					Ext.getCmp('saveButtonID').setDisabled(true);
					AM.app.getController('ClientWOR').saveStatus(form, Ext.getCmp('fte_total').getValue());
				}
			}
		}];

		this.callParent(arguments);
	},

	assignResources : function() {
		AM.app.globals.transEmployRes = [];
		AM.app.getController('ClientWOR').onAssignResources(Ext.getCmp('Vclient_name').getValue(), Ext.getCmp('client_id').getValue(), Ext.getCmp('work_order_id').getValue(), Ext.getCmp('wor_id').getValue(),Ext.getCmp('wor_name').getValue());
	},
	
	ConfigureAttributes : function() {
		AM.app.globals.transEmployRes = [];
		AM.app.getController('ClientWOR').onConfigureAttributes(Ext.getCmp('Vclient_name').getValue(), Ext.getCmp('client_id').getValue(), Ext.getCmp('work_order_id').getValue(), Ext.getCmp('wor_id').getValue(),Ext.getCmp('wor_name').getValue());
	},

	viewResources : function() {
		AM.app.globals.transEmployRes = [];
		AM.app.getController('ClientWOR').onViewResources('view_res', Ext.getCmp('Vclient_name').getValue(), Ext.getCmp('client_id').getValue(), Ext.getCmp('work_order_id').getValue(), Ext.getCmp('wor_id').getValue(),Ext.getCmp('wor_name').getValue());
	},

	peopleBillability : function() {
		AM.app.globals.transEmployRes = [];
		AM.app.getController('ClientWOR').onPeopleBillability('people_billability', Ext.getCmp('Vclient_name').getValue(), Ext.getCmp('client_id').getValue(), Ext.getCmp('work_order_id').getValue(), Ext.getCmp('wor_id').getValue(),Ext.getCmp('wor_name').getValue());
	},

	remResources : function() {
		AM.app.globals.transEmployRes = [];
		AM.app.getController('ClientWOR').onRemoveResources('rem_res', Ext.getCmp('Vclient_name').getValue(), Ext.getCmp('work_order_id').getValue(), Ext.getCmp('wor_id').getValue());
	},

	onCancel : function() {
		if(Ext.getCmp('inprogressStatus').getValue() != 'In-Progress' && Ext.getCmp('inprogressStatus').getValue() != 'Closed'){
			Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
				if(btn === 'yes'){
					AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus );	
				}
			});
		}else {
			AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus );	
		}
	},

});