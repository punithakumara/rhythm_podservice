Ext.define('AM.view.wor_cor.existVolumeBasedList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.existVolumeBasedList',
	title: 'Volume Based Details',
	id : 'exist_volume_list',
	store: 'WorkTypeVolume',
    layout : 'fit',
    selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
    width : '100%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	
	initComponent : function() {
		var me = this;

		//**** List View Coloumns ****//
    	this.columns = [{
    		header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			dataIndex : 'fk_project_type_id',
			tooltip: 'Project',
			renderer: function(value) {
                var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
 				return rec == null ? value : rec.get('project_type');
            },
    	},  {
			header: 'Roles',
			flex: 1.4,
			sortable: false,
			menuDisabled:true,
			hideable: false,
			dataIndex : 'responsibilities',
			tooltip: 'Roles',
			renderer: function(value) {
                var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
            },
		},{
			header: 'Volume Coverage',
			flex: 0.8,
			sortable: false,
			hideable: false,
			dataIndex : 'volume_coverage',
			menuDisabled:true,
			tooltip: 'Volume Coverage',
			renderer: function(value) {
                var rec	= Ext.getStore('DurationCoverage').findRecord('id', value);
				return rec == null ? value : rec.get('name');
            },
		}, {
			header : 'Shift', 
			flex: 0.6,
			sortable: false,
			hideable: false,
			dataIndex : 'shift',
			tooltip : 'Shift',
			menuDisabled:true,
			renderer: function(value) {
                var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
				return rec == null ? value : rec.get('shift_code');
            },
		}, {
			text: "Expected Volume",
			menuDisabled:true,
			columns: [{
				header : 'Minimum',
				flex: 1,
				sortable: false,
				hideable: false,
				dataIndex : 'min_volume',
				menuDisabled:true,
				tooltip : 'Minimum',
			}, {
				header : 'Maximum',
				flex: 1,
				sortable: false,
				hideable: false,
				dataIndex : 'max_volume',
				tooltip : 'Maximum',
				menuDisabled:true,
			}]
		}, {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			menuDisabled:true,
			flex: 0.8,
			sortable: false,
			hideable: false,
			tooltip: 'Support Coverage',
				renderer: function(value) {
                var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
            },
		}, {
			header: 'Anticipated Start Date',
			dataIndex: 'anticipated_date',
			menuDisabled:true,
			flex: 1,
			sortable: false,
			hideable: false,
			tooltip: 'Anticipated Start Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
		},  {
			header : 'Experience in Years', 
			flex: 0.8,
			menuDisabled:true,
			sortable: false,
			hideable: false,
			dataIndex : 'experience',
			tooltip : 'Experience in Years', 
		}, {
			header : 'Skills',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			hideable: false,
			dataIndex : 'skills',
			tooltip : 'Skills',
		}];

		//**** End List View Coloumns ****//
		
		this.callParent(arguments);
	},		
});