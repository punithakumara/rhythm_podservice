Ext.define('AM.view.wor_cor.ConfigureAttributes',{
	extend : 'Ext.window.Window',
	alias : 'widget.ConfigureAttributes',
	layout: 'fit',
	// store: 'Attributes',
	width : '90%',
	height: 555,
	autoScroll: true,
	autoHeight : true,   
	autoShow : true,
	closable: false,
	modal: true,
	title: 'Configure Time Entry Fields',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
    requires:['AM.view.SearchTrigger'],
	initComponent : function() {

		var me = this, intialStore = "";
		
		this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items:[{
				region:'west',
				// xtype: 'insightTopPanel',
				width: '20%',
				border: true,
				height: 400,
				margin : '0 5px 5px 0',
				layout: {
					type: 'vbox',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : '<b>Client</b> <span style="color:red">*</span>',
					width: 220,
					name: 'res_client',
					id: 'res_client',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'hiddenfield',
					name: 'res_client_id',
					id: 'res_client_id',
					submitValue : true
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order ID</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_id',
					name: 'res_work_order_id',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order Name</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_name',
					name: 'res_work_order_name',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'hiddenfield',
					name: 'res_project_id',
					id: 'res_add_project_id',
					submitValue : true
				},{
					xtype : 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Project</b> <span style="color:red">*</span>',
					anchor : '100%',
					width: 220,
					id:'project_type_id',
					name: 'project_type_id',
					labelAlign: 'top',
					labelWidth : 150,
					store: Ext.getStore('WorProjects'),
					queryMode: 'remote',
					allowBlank: false,
					submitValue : true,
					displayField: 'project_type',
					valueField: 'project_type_id',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					listeners:{
		                beforequery: function(queryEvent){
		                    Ext.Ajax.abortAll();
		                    queryEvent.combo.getStore().getProxy().extraParams = {'work_order_id':Ext.getCmp('wor_id').getValue()};
		                },
						select : function(combo) {
							Ext.getCmp('role_type_id').setValue('');
							
							me.query('combobox')[1].bindStore("Roles");
							Ext.getStore('Roles').load({
								params:{'wor_id':Ext.getCmp('wor_id').getValue(), 'project_id':Ext.getCmp('project_type_id').getValue()}
							});
						}
					}
				},{
					xtype : 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Roles under this project</b> <span style="color:red">*</span>',
					anchor : '100%',
					width: 220,
					id:'role_type_id',
					name: 'role_type_id',
					labelAlign: 'top',
					labelWidth : 150,
					store: Ext.getStore('Roles'),
					queryMode: 'remote',
					submitValue : true,
					allowBlank: false,
					displayField: 'role_type',
					valueField: 'role_type_id',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					listeners:{
						select : function(combo) {
							me.clientAttrDisp();
						},
						change : function(combo) {
							var myGrid = Ext.getCmp('attributesGridID');
							if(combo.getValue()=='' || combo.getValue()==null)
							{
								myGrid.setDisabled(true);
							}
							else
							{
								myGrid.setDisabled(false);
							}
						},
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_id':Ext.getCmp('wor_id').getValue(), 'project_id':Ext.getCmp('project_type_id').getValue() };
						}
					}
				}]
			}, {
				region:'center',
				width: '69%',
				items:[{
					xtype:'gridpanel',
					border:true,
					margin:'0 0 0 0',
					height:445,
					id:'attributesGridID',
					columns: [{
						header: 'Field Name',  
						dataIndex: 'name',  
						sortable: false,
						hideable: false,
						menuDisabled: true,
						flex: 3,
					},{
						header: 'Field Description',  
						dataIndex: 'description',  
						sortable: false,
						hideable: false,
						menuDisabled: true,
						flex: 3,
					},{
						xtype: 'checkcolumn',
						header: 'Visible',
						headerCheckbox: true, 
						sortable: false,
						hideable: false,
						menuDisabled: true,
						dataIndex:'selected',
						listeners: {
							checkchange: function(column, rowIdx, checked, eOpts){
								if(!checked)
								{
									var grid = this.up('grid');
									var record = grid.getStore().getAt(rowIdx);
									record.set('mandatory', false);
								}
							}
						}
					},{
						xtype: 'checkcolumn',
						header: 'Mandatory',
						headerCheckbox: true, 
						sortable: false,
						hideable: false,
						menuDisabled: true,
						dataIndex:'mandatory',
						id:'mandatory',
						itemId:'mandatory',
						listeners: {
							checkchange: function(column, rowIdx, checked, eOpts){
								var grid = this.up('grid');
								var record = grid.getStore().getAt(rowIdx);
								record.set('selected', true);
							}
						}
					}],
				}]
			}]
		}];

		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Submit',
			id:'addSubmit',
			hidden:true,
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
		}];

		this.callParent(arguments);
	},


	onCancel : function(win) {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				win.up('window').close();
			}
		});
	},

	onSubmit :  function(btn) {
		var win = this.up('window');
		if(Ext.getCmp('project_type_id').getValue()=="" || Ext.getCmp('project_type_id').getValue()==null)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Project",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(Ext.getCmp('role_type_id').getValue()=='' || Ext.getCmp('role_type_id').getValue()==null)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Roles",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			return false;
		}

		var recordItems = Ext.getCmp('attributesGridID').store.data.items;
		if(Ext.getCmp('attributesGridID').store.getModifiedRecords().length<1)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Attribute(s)",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else
		{
			Ext.getCmp('addSubmit').setDisabled(true);
			AM.app.getController('ClientWOR').onSaveConfigAttributes(recordItems, win);
		}
	},

	clientAttrDisp: function()
	{
		var myGrid = Ext.getCmp('attributesGridID');
		myGrid.getSelectionModel().deselectAll();

		var client_attributes = Ext.getStore('ClientAddAttr').load({
			params:{
				'wor_id':Ext.getCmp('wor_id').getValue(),
				'client_id':Ext.getCmp('res_client_id').getValue(),
				'project_type_id':Ext.getCmp('project_type_id').getValue(), 
				'role_id':Ext.getCmp('role_type_id').getValue()
			},
		});
		
		Ext.getCmp('attributesGridID').bindStore(client_attributes);
		myGrid.setDisabled(false);
		Ext.getCmp('addSubmit').setVisible(true);
	}

});