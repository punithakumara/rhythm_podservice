Ext.define('AM.view.wor_cor.ViewCorFromwor',{
	extend : 'Ext.window.Window',
	alias : 'widget.ViewCorFromwor',
	title: 'COR details',
	// layout: 'fit',
	width : '90%',
	height: '82%',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,

    initComponent : function() {

    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addCORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 141,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '40%',
				},
				width: 590,
				title:"Work Order Information",
				items:[{
				    xtype:'hidden',
				    name:'cor_id',
				    id:'cor_id',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'COR ID <span style="color:red">*</span>',
					name : 'change_order_id',
					id: 'change_order_id'
				}, {
					xtype:'displayfield',
					fieldLabel: 'Work Order ID <span style="color:red">*</span>',
					name: 'previous_work_order_id',
					id:'previous_work_order_id',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Work Order Name <span style="color:red">*</span>',
					name : 'wor_name',
					id : 'view_cor_wor_name',
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'client_name',
					id:'client_name',
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Work Order Type <span style="color:red">*</span>',
					name: 'wor_type',
					id: 'view_cor_wor_type',
				},{
					xtype : 'displayfield',
					fieldLabel: 'Service Type <span style="color:red">*</span>',
					name: 'wor_type',
					id: 'view_cor_service_type',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'Change Request Date <span style="color:red">*</span>',
					name : 'change_request_date',
					id:'change_request_date'
				}, {
					xtype:'displayfield',
					fieldLabel: 'Status <span style="color:red">*</span>',
					name:'status',
					id: 'inCorStatus',
					//hidden: true
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					id : 'ViewCORDescription',
					name : 'description',
					allowBlank: false,
					hidden: true,
				}, ]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				width: 590,
				title:"People Information",
				items:[{
					xtype:'displayfield',
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					name: 'cp_name',
					id:'view_cor_cp_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					name: 'em_name',
					id:'view_cor_em_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					name: 'dm_name',
					id: 'view_cor_dm_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Associate Manager',
					name: 'am_name',
					id: 'view_cor_am_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Team Lead',
					name: 'tl_name',
					id: 'view_cor_tl_name',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Stakeholder(s)',
					name: 'sh_name',
					id: 'view_cor_sh_name',
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			id:'myCorTabPanel123',
			activeTab: 0,
			padding: '0 0 2px 0',
		}];
		
    	this.buttons = ['->',  {
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }];

		this.callParent(arguments);
	},	

	onCancel : function(win) {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				win.up('window').close();
			}
		});
	},
});