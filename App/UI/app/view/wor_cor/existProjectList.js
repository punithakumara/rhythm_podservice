Ext.define('AM.view.wor_cor.existProjectList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.existProjectList',
	title: 'Project Details',
	id: 'exist_project_list',
    layout : 'fit',
    store: 'WorkTypeProject',
    selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
    width : '100%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	
	initComponent : function() {
		var me = this;
		 
		//**** List View Coloumns ****//
    	this.columns = [{
    		header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			dataIndex : 'fk_project_type_id',
			tooltip: 'Project',
			renderer: function(value) {
                var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
 				return rec == null ? value : rec.get('project_type');
            },
    	},  {
			header: 'Roles',
			menuDisabled:true,
			dataIndex: 'responsibilities',
			flex: 1.4,
			sortable: false,
			hideable: false,
			tooltip: 'Roles',
			renderer: function(value) {
                var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
            },
		},{
			header: 'Project Name',
			flex: 1,
			sortable: false,
			hideable: false,
			dataIndex : 'project_name',
			menuDisabled:true,
			tooltip: 'Project Name',
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
            	if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
            	return value;        
            },
		}, {
			header: 'Project Description',
			flex: 1,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			dataIndex : 'project_description',
			tooltip: 'Project Description',
			renderer: function(value, metaData, record, rowIdx, colIdx, store) {        		
            	if (value != null) 
				{ 
					metaData.tdAttr = 'data-qtip="' + value + '"'; 
				}       		
            	return value;        
            },
		}, {
			header : 'Shift',
			flex: 0.8,
			sortable: false,
			hideable: false,
			dataIndex : 'shift',
			menuDisabled:true,
			tooltip : 'Shift',
			renderer: function(value) {
                var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
				return rec == null ? value : rec.get('shift_code');
            },
		}, {
			text: "Duration",
			menuDisabled:true,
			columns:[{
				header : 'Start Date',
				sortable: false,
				hideable: false,
				menuDisabled:true,
				dataIndex : 'start_duration',
				tooltip : 'Start Date',
				renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			}, {
				header : 'End Date',
				sortable: false,
				menuDisabled:true,
				hideable: false,
				dataIndex : 'end_duration',
				tooltip : 'End Date',
				renderer : Ext.util.Format.dateRenderer('d-M-Y'),
			}]
		}, {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			flex: 1,
			sortable: false,
			hideable: false,
			menuDisabled:true,
			tooltip: 'Support Coverage',
			renderer: function(value) {
                var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
            },
		}, {
			header: 'Anticipated Start Date',
			dataIndex: 'anticipated_date',
			menuDisabled:true,
			flex: 1,
			sortable: false,
			hideable: false,
			tooltip: 'Anticipated Start Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
		}, {
			header : 'Experience in Years', 
			dataIndex: 'experience',
			menuDisabled:true,
			flex: 0.8,
			sortable: false,
			hideable: false,
			tooltip : 'Experience in Years',
		}, {
			header : 'Skills', 
			dataIndex: 'skills',
			sortable: false,
			menuDisabled:true,
			hideable: false,
			flex: 1,
			tooltip : 'Skills', 
		}];

		//**** End List View Coloumns ****//
		
		this.callParent(arguments);
	},		
});