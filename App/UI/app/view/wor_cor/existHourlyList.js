Ext.define('AM.view.wor_cor.existHourlyList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.existHourlyList',
	title: 'Hourly Details',
	id : 'exist_hourly_list',
    layout : 'fit',
    selType: 'rowmodel',
    store: 'WorkTypeHourly',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
    width : '100%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	listeners : {
		sortchange : function(grid, sortInfo){
			this.getStore().getProxy().extraParams = {'RDR_ID':AM.app.globals.mainClientRDR_ID};
		}
	},
	
	initComponent : function() {
		var me = this;
		
		//**** List View Coloumns ****//
    	this.columns = [{
    		header: 'Project',
			sortable: false,
			hideable: false,
			draggable: false,
			menuDisabled:true,
			fixed: true,
			flex: 1,
			dataIndex : 'fk_project_type_id',
			tooltip: 'Project',
			renderer: function(value) {
                var rec	= Ext.getStore('WorProjects').findRecord('project_type_id', value, 0, false, true, true);
				console.log(value, rec);
 				return rec == null ? value : rec.get('project_type');
            },
    	}, {
			header: 'Roles',
			dataIndex: 'responsibilities',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			flex: 1.5,
			tooltip: 'Roles',
			renderer: function(value) {
                var rec	= Ext.getStore('Responsibilities').findRecord('id', value);
				return rec == null ? value : rec.get('name');
            },
		},{
			header: 'Hours Coverage',
			flex: 0.7,
			dataIndex : 'hours_coverage',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			tooltip: 'Hours Coverage',
			renderer: function(value) {
			var rec	= Ext.getStore('DurationCoverage').findRecord('id', value);
			return rec == null ? value : rec.get('name');
			},
		}, {
			header : 'Shift',
			flex: 0.7,
			dataIndex : 'shift',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			tooltip : 'Shift',
			renderer: function(value) {
                var rec	= Ext.getStore('Shifts').findRecord('shift_id', value);
				return rec == null ? value : rec.get('shift_code');
            },
		}, {		
			text: "Expected Hours",
			menuDisabled:true,
			columns: [{
				header : 'Minimum',
				flex: 1,
				dataIndex : 'min_hours',
				sortable: false,
				hideable: false,
				menuDisabled:true,
				tooltip : 'Minimum',
				editor: {
					xtype:'numberfield',
					itemId: 'min_hours',
					minValue: 0,
				}
			}, {
				header : 'Maximum',
				flex: 1,
				menuDisabled:true,
				dataIndex : 'max_hours',
				sortable: false,
				hideable: false,
				tooltip : 'Maximum',
			}]
		},  {
			header: 'Support Coverage',
			dataIndex: 'support_coverage',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			flex: 0.8,
			tooltip: 'Support Coverage',
			renderer: function(value) {
                var rec	= Ext.getStore('SupportCoverage').findRecord('support_id', value);
				return rec == null ? value : rec.get('support_type');
            },
		}, {
			header: 'Anticipated Start Date',
			dataIndex: 'anticipated_date',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			flex: 1,
			tooltip: 'Anticipated Start Date',
			renderer : Ext.util.Format.dateRenderer('d-M-Y'),
		}, {
			header : 'Experience in Years', 
			dataIndex: 'experience',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			flex: 0.9,
			tooltip : 'Experience in Years', 
		}, {
			header : 'Skills', 
			dataIndex: 'skills',
			sortable: false,
			hideable: false,
			menuDisabled:true,
			flex: 1,
			tooltip : 'Skills', 
		}];

		//**** End List View Coloumns ****//
		
		this.callParent(arguments);
	},		
});