Ext.define('AM.view.wor_cor.RemoveResources',{
	extend : 'Ext.window.Window',
	alias : 'widget.RemoveResources',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
    },
	store: 'AllocatedResources',
	width : '90%',
	height: 525,
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	title: 'Remove Resources',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
    	this.items = [ {
			layout : 'hbox',
			items: [{
				xtype:'displayfield',
				fieldLabel: '<b>WOR ID</b> ',
				id: 'res_work_order_id',
				name: 'res_work_order_id',
				labelWidth : 60,
				width: 220,
				submitValue : true,
			}, {
				xtype : 'displayfield',
				fieldLabel: '<b>Client Name</b> ',
				name: 'res_client',
				id: 'res_client',
				labelWidth : 90,
				width : 300,
			}, {
				xtype : 'displayfield',
				fieldLabel: '<b>Job Responsibilities</b> ',
				name: 'job_responsibilities',
				id: 'add_job_responsibilities',
				labelWidth : 135,
				width : 360,
			}, {
				xtype : 'displayfield',
				fieldLabel: '<b>Support Coverage</b> ',
				name: 'support_coverage',
				id: 'add_support_coverage',
				labelWidth : 125,
				width : 200,
			}, {
				xtype : 'hiddenfield',
				name: 'res_project_id',
				id: 'res_project_id',
				submitValue : true
			}, {
				xtype : 'hiddenfield',
				name: 'res_client_id',
				id: 'res_client_id',
				submitValue : true
			}, {
				xtype : 'hiddenfield',
				name: 'res_wor_id',
				id: 'res_wor_id',
				submitValue : true
			}]
		}, {
			xtype:'gridpanel',
			border:true,
			margin:'10 0 5 0',
			height:350,
			width:'100%',
			id:'remove_resources_grid',
			selType: 'checkboxmodel',
			selModel: {
				checkOnly: true,
				id: 'sel_model',
				//showHeaderCheckbox: false,
				mode:'MULTI',
				listeners: {
					deselect: function(model, record, index) {
						var myAry = AM.app.globals.transEmployRes;						
						for(var i =0; i < myAry.length; i++)
						{
							if (myAry[i] != null && (myAry[i].data.employee_id == record.data.employee_id))
							{
								myAry[i] = null;
							}
						}
					},
					select: function(model, record, index) {														
						if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
						{							
							AM.app.globals.transEmployRes.push(record);
						}
					}
				}
			} ,
			columns: [{ header:"Employee ID", id: "company_employ_id", dataIndex:"company_employ_id",flex:0.5},{ header:"Name",dataIndex:"FullName",flex:1}, { header:"Designation",dataIndex:"designationName",flex:1}, { header:"Supervisor",dataIndex:"supervisor",flex:1}],
			bbar: Ext.create('Ext.PagingToolbar', {
				displayInfo: true,
				listeners: {
					beforechange: function(item1,item2,item3){
						this.store.getProxy().extraParams = {'client_id' : Ext.getCmp('res_client_id').getValue(), 'project_id': Ext.getCmp('res_project_id').getValue(), 'wor_id': Ext.getCmp('res_wor_id').getValue() }
					},
					change:function(){						
						var myGrid = Ext.getCmp('remove_resources_grid');	
						for(i=0; i < myGrid.store.data.length; i++)
						{
							var user = myGrid.store.getAt(i);							
							var myAry = AM.app.globals.transEmployRes;
						
							for (var j=0; j< myAry.length; j++)
							{
								if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
									myGrid.selModel.doMultiSelect(user,true);
							}	
						}
					}
				},
				id:'remove_resources_grid_bb',
				pageSize: AM.app.globals.itemsPerPage,
				displayMsg: 'Displaying {0} - {1} of {2}',
				emptyMsg: "No Data To Display",
				//store   : storeDetails
			}),
			listeners: {
				sortchange :  function(ct, column) {															
					this.store.getProxy().extraParams = {'client_id' : Ext.getCmp('res_client_id').getValue(), 'project_id': Ext.getCmp('res_project_id').getValue() , 'wor_id': Ext.getCmp('res_wor_id').getValue() }
					this.store.load();
				}
			}
		}, {
			xtype : 'datefield',
			format : AM.app.globals.CommonDateControl,
			fieldLabel : 'Remove Date <span style="color:red">*</span>',
			anchor : '100%',
			name : 'transition_date',
			id :   'transition_date',
			maskRe: /[0-9\/]/,
			labelWidth : 120,
			width: 300,
			allowBlank : false,
			maxValue : new Date(),
			minValue: Ext.Date.add(new Date(), Ext.Date.MONTH, -3), 
			submitValue : true,
			style: {
				'margin-top': '8px'
			}
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			id:'exit_button',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Remove',
			id:'remove_button',
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : function() {																		
				AM.app.getController('ClientWOR').onSubmit(this.up('grid'), Ext.getCmp('res_client_id').getValue(), Ext.getCmp('res_project_id').getValue());						   
			} 
		}];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},

	onSubmit :  function(btn) {
		var win = this.up('window');
		Ext.getCmp('remove_button').setDisabled(true);	
		AM.app.getController('ClientWOR').onSubmit(this.up('grid'), );
	},
	
	onClick:function(my)
	{
		var empParam = Ext.getCmp('resource_id').getValue();
		
		if(empParam != "" && empParam != null)
		{
			var MyStore = Ext.getStore('AllocatedResources').load({
				params:{'manager' : '2109', 'searchTxt': empParam}
			})
			
			my.intialStore = MyStore;			
			Ext.getCmp('view_resources_grid').bindStore(MyStore);
			Ext.getCmp('WORtransGridPage').bindStore(MyStore);
		}
		else
		{
			Ext.getCmp('view_resources_grid').store.removeAll();
			Ext.getCmp('view_resources_grid').getView().Refresh();
		}
	}
	
});