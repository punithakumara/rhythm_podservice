Ext.define('AM.view.wor_cor.AssignResources',{
	extend : 'Ext.window.Window',
	alias : 'widget.AssignResources',
	layout: 'fit',
	store: 'AllReportees1',
	width : '90%',
	height: 555,
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
    closable: false,
	title: 'Assign Employee',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
	// listeners: {
	// 	beforeclose: function (thisWindow) {
 //            if (!thisWindow.isConfirmed) {
 //                Ext.MessageBox.confirm('Confirmation', 'Are you sure you wish to close this window before saving your changes?', function (btn) {
 //                    if (btn == 'yes') {
 //                        thisWindow.isConfirmed = true;
 //                        thisWindow.close();
 //                    }
 //                });
 //                return false;
 //            }
 //        }
	// },
    initComponent : function() {

    	var me = this, intialStore = "";

    	if (window.history && window.history.pushState) {
			//window.history.pushState('', null, './');
			window.history.pushState(null, "", window.location.href);
			window.onpopstate = function () {
				Ext.MessageBox.confirm('Save Changes?', 'Would you like to save your changes?', function(btn){
					if (btn === 'yes'){
						//document.location.href = 'index.html';
					}else {
						var win = Ext.WindowManager.getActive();
						if (win) {
						    win.close();
						}
					}
				});
	
			};
		 }
    	
    	this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items:[{
				region:'west',
				// xtype: 'insightTopPanel',
				width: '20%',
				border: true,
				height: 400,
				margin : '0 5px 5px 0',
				layout: {
					type: 'vbox',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : '<b>Client</b> <span style="color:red">*</span>',
					width: 220,
					name: 'res_client',
					id: 'res_client',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'hiddenfield',
					name: 'res_client_id',
					id: 'res_client_id',
					submitValue : true
				},{
				    xtype:'hidden',
				    name:'res_wor_type_id',
					id:'res_wor_type_id',
				},{
					xtype : 'hiddenfield',
					name: 'res_project_id',
					id: 'res_add_project_id',
					submitValue : true
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order ID</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_id',
					name: 'res_work_order_id',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'displayfield',
					fieldLabel : '<b>Work Order Name</b> <span style="color:red">*</span>',
					width: 220,
					id: 'res_work_order_name',
					name: 'res_work_order_name',
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Project</b> <span style="color:red">*</span>',
					anchor : '100%',
					width: 220,
					id:'project_type_id',
					name: 'project_type_id',
					labelAlign: 'top',
					labelWidth : 150,
					store: Ext.getStore('WorProjects'),
					queryMode: 'remote',
					allowBlank: false,
					submitValue : true,
					displayField: 'project_type',
					valueField: 'project_type_id',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					listeners:{
		                beforequery: function(queryEvent){
		                    Ext.Ajax.abortAll();
		                    queryEvent.combo.getStore().getProxy().extraParams = {'work_order_id':Ext.getCmp('wor_id').getValue()};
		                },
						select: function(obj){
							Ext.getStore('AllReportees1').load({
								params:{ 'project_type_id': obj.getValue()}
							});
							if(obj.getValue()!="")
							{
								// Ext.Ajax.request({
								// 	url: AM.app.globals.appPath+'index.php/Clientwor/wor_project_roles?v='+AM.app.globals.version,
								// 	method: 'POST',
								// 	params: {
								// 		'wor_id':Ext.getCmp('wor_id').getValue(),
								// 		'project_id':obj.getValue(),
								// 	},
								// 	scope: this,
								// 	success: function(response) {
								// 		var myObject = Ext.JSON.decode(response.responseText);
								// 		Ext.getCmp('job_responsibilities').setValue(myObject.rows.role_type);
								// 	}
								// });
								Ext.getCmp('role_type_id').setValue('');
							
								me.query('combobox')[1].bindStore("Roles");
								Ext.getStore('Roles').load({
									params:{'wor_id':Ext.getCmp('wor_id').getValue(), 'project_id':Ext.getCmp('project_type_id').getValue()}
								});
							}
							// else
							// {
							// 	Ext.getCmp('job_responsibilities').setValue('');
							// }
						}
		            }
				},{
					xtype : 'combobox',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Roles under this project</b> <span style="color:red">*</span>',
					anchor : '100%',
					width: 220,
					id:'role_type_id',
					name: 'role_type_id',
					labelAlign: 'top',
					labelWidth : 150,
					store: Ext.getStore('Roles'),
					queryMode: 'remote',
					submitValue : true,
					allowBlank: false,
					displayField: 'role_type',
					valueField: 'role_type_id',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					listeners:{
						// select : function(combo) {
						// 	me.clientAttrDisp();
						// },
						// change : function(combo) {
						// 	var myGrid = Ext.getCmp('attributesGridID');
						// 	if(combo.getValue()=='' || combo.getValue()==null)
						// 	{
						// 		myGrid.setDisabled(true);
						// 	}
						// 	else
						// 	{
						// 		myGrid.setDisabled(false);
						// 	}
						// },
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'wor_id':Ext.getCmp('wor_id').getValue(), 'project_id':Ext.getCmp('project_type_id').getValue() };
						}
					}
				},
			// {
			// 		xtype : 'displayfield',
			// 		fieldLabel : '<b>Roles under this project</b>',
			// 		width: 220,
			// 		labelAlign: 'top',
			// 		name: 'job_responsibilities ',
			// 		id: 'job_responsibilities',
			// 		style:{
			// 			'position':'absolute',
			// 			'margin':'10px 0 0 5px'
			// 		},
			// 	},
				{
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : '<b>Assign Date</b> <span style="color:red">*</span>',
					name : 'transition_date',
					id :   'transition_date',
					anchor : '100%',
					maskRe: /[0-9\/]/,
					width: 220,
					labelWidth : 150,
					allowBlank : false, 
					submitValue : true,
					labelAlign: 'top',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					}
				}]
			}, {
				region:'center',
				width: '69%',
				items:[{
					xtype : 'form',
					layout: 'hbox',
					id : 'addResFrm',
					items:[{
						xtype : 'textfield',
						format : AM.app.globals.CommonDateControl,
						fieldLabel : 'Employee ID/Name ',
						anchor : '100%',
						width: 500,
						name : 'resource_id',
						id : 'resource_id',
						labelWidth : 150,
						listeners: {
							change: function(field, e) {
								//if(e.getKey() == e.ENTER) {
									me.onClick(me)
								//}
							}
						}
					}, {
						xtype:'tbspacer',
						width: 15
					}, {
						xtype:'button',
						text:'Search',
						id:'transSecondSearch',
						width:60,
						listeners:{
							'click':function(){
								me.onClick(me)
							}
						}
					}]
				},{
					xtype:'gridpanel',
					border:true,
					margin:'10 0 5 0',
					height:415,
					id:'WORtransGridID',
					selType: 'checkboxmodel',
					selModel: {
						checkOnly: true,
						mode:'MULTI',
						listeners: {
							deselect: function(model, record, index) {
								var myAry = AM.app.globals.transEmployRes;						
								for(var i =0; i < myAry.length; i++)
								{
									if (myAry[i] != null && (myAry[i].data.employee_id == record.data.employee_id))
									{
										myAry[i] = null;
									}
								}
							},
							select: function(model, record, index) {
								if(!(Ext.Array.contains(AM.app.globals.transEmployRes, record)))
								{							
									AM.app.globals.transEmployRes.push(record);
								}
							}
						}
					} ,
					columns: [{
						header: 'POD',  
						dataIndex: 'podName',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Service',  
						dataIndex: 'serviceName',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Employee ID',  
						dataIndex: 'company_employ_id',  
						flex: 2,
						menuDisabled:true,
					},{
						header: 'Employee Name',  
						dataIndex: 'FullName',  
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Designation',  
						dataIndex: 'designationName',  
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Email', 
						dataIndex: 'Email', 
						flex: 3,
						menuDisabled:true,
					},{
						header: 'Supervisor', 
						dataIndex: 'supervisor', 
						flex: 3,
						menuDisabled:true,
					}],
					
					bbar: Ext.create('Ext.PagingToolbar', {
						id:'WORtransGridPage',
						store: this.store,
						displayInfo: true,
						listeners: {
							beforechange: function(item1,item2,item3){
								this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(), 'manager' : Ext.util.Cookies.get('employee_id'), 'client_id':Ext.getCmp('client_id').getValue()}																		
							},
							change:function(){
								
								var myGrid = Ext.getCmp('WORtransGridID');
								
								for(i=0; i < myGrid.store.data.length; i++)
								{
									var user = myGrid.store.getAt(i);							
									var myAry = AM.app.globals.transEmployRes;
								
									for (var j=0; j< myAry.length; j++)
									{
										if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
											myGrid.selModel.doMultiSelect(user,true);
									}	
								}
								Ext.getCmp('resource_id').focus();
							}
						},
						displayMsg: 'Displaying employees {0} - {1} of {2}',
						pageSize: AM.app.globals.itemsPerPage,
						plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
						emptyMsg: "No employees to display."
					}),
					listeners: {
						sortchange :  function(ct, column) {
							this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(),'manager' : Ext.util.Cookies.get('employee_id'), 'client_id':Ext.getCmp('client_id').getValue()}																		
							this.store.load();
						}
					}
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Confirm',
			id:'addSubmit',
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
        }];

		this.callParent(arguments);
	},	

	onCancel : function(win) {
		Ext.MessageBox.confirm('Cancel', 'Are you sure ?', function(btn){
			if(btn === 'yes'){
				win.up('window').close();
			}
		});
	},

	onSubmit :  function(btn) {
		var win = this.up('window');
		
		var newArray = new Array();
		for (var i = 0; i < AM.app.globals.transEmployRes.length; i++) 
		{
			if (AM.app.globals.transEmployRes[i]) {
				newArray.push(AM.app.globals.transEmployRes[i]);
			}
		}
		if(newArray.length>0)
		{
			Ext.getCmp('addSubmit').setDisabled(true);
			AM.app.getController('ClientWOR').onSaveResources(win);
		}
		else
		{
			Ext.Msg.show({
			   title:'Alert',
			   msg:"Please Select Employee(s)",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
		}
	},

	onClick:function(my)
	{
		var empParam = Ext.getCmp('resource_id').getValue();
		
		if(empParam != "" && empParam != null)
		{
			var MyStore = Ext.getStore('AllReportees1').load({
				params:{'manager' : Ext.util.Cookies.get('employee_id'), 'searchTxt': empParam,'client_id':Ext.getCmp('client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}
			})
			
			my.intialStore = MyStore;			
			Ext.getCmp('WORtransGridID').bindStore(MyStore);
			Ext.getCmp('WORtransGridPage').bindStore(MyStore);
		}
		else
		{
			Ext.getCmp('WORtransGridID').store.removeAll();
			var MyStore = Ext.getStore('AllReportees1').load({
				params:{'manager' : Ext.util.Cookies.get('employee_id'), 'searchTxt': empParam,'client_id':Ext.getCmp('client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}
			})
		}
	}
	
});