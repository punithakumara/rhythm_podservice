Ext.define('AM.view.timesheetonshore.list' ,{
	extend: 'Ext.grid.Panel',
	alias: 'widget.list',
	store: 'TimesheetOnshore',
	layout : 'fit',
	id: "TimesheetGridID",
	itemId: "TimesheetGridID",
	border : true,
	minHeight:300,
	height: 'auto',
	width:'99%',
	title: "Onshore Timesheet",
	selType: 'cellmodel',
	features: [{
		ftype: 'summary',
		groupHeaderTpl: '{name}',
	}],
	
	initComponent: function() {
		var me = this;
		var timesheet = "";
		var timesheetClient = "";
		var combobox=0;

		this.viewConfig = { 
			deferEmptyText: false,
			getRowClass: function(record, rowIndex, rowParams, store) {
				if (record.get('Task')!="Production Work" && record.get('Status')==2) 
					return 'approved';
				else if (record.get('Task')!="Production Work" && record.get('Status')==0 && record.get('Status')!="") 
					return 'rejected';
			},
			listeners: {
				refresh: function(view) {
					if(this.store.getProxy().reader.jsonData != undefined && this.store.getProxy().reader.jsonData.TotalWeekHours != undefined && this.store.getProxy().reader.jsonData.TotalMonthHours != undefined)
					{
						Ext.getCmp('TotalMonthHours').setValue(this.store.getProxy().reader.jsonData.TotalMonthHours);
						Ext.getCmp('TotalWeekHours').setValue(this.store.getProxy().reader.jsonData.TotalWeekHours);
					}
				}
			} 
		};

		me.editing = Ext.create('Ext.grid.plugin.CellEditing',{
			clicksToEdit: 1,
			listeners: {
				beforeedit: function(editor, e){
				/*	var bool = "";
					timesheet = "";
					
					if (e.record.data.Task == "Production Work")
						return false;
					
					if (e.field == "Task")
					{
						AM.app.globals.timesheetClient = e.record.data.ClientName;
						Ext.getStore('timesheetTaskCode').load({
							params:{ 'client_id':AM.app.globals.timesheetClient }
						});
					}
					if (e.field == "SubTask")
					{
						AM.app.globals.timesheetTask = e.record.data.Task;
						Ext.getStore('timesheetSubTask').load({
							params:{ 'client_id':AM.app.globals.timesheetClient,'task_name': e.record.data.Task }
						});
					}*/

					/* if (e.field == "day1" || e.field == "day2" ||e.field == "day3" ||e.field == "day4" ||e.field == "day5" ||e.field == "day6" ||e.field == "day7" )
					{
						Ext.Ajax.request({
							url: AM.app.globals.appPath+'index.php/timesheet/validateDate',
							method: 'POST',
							async: false,
							params: {
								'weekno' : Ext.getCmp('timeWeekRange').getValue(),
								'day' : e.field,
							},
							success: function(response) {
								var myObject = Ext.JSON.decode(response.responseText);
								
								bool = myObject.success;
								
							},
						});
						if(bool<1)
						{
							return false;
						}
					} */
					
					if(editor.context.field!="Task" && editor.context.field!="ClientName" && editor.context.field!="ServiceType" && editor.context.field!="Engagement" && editor.context.field!="Role")
					{
						timesheet = editor.context.field;
					}
				/*	if(editor.context.field=="ClientName")
					{
						timesheetClient = editor.context.field;
					}*/
				},
				afteredit: function(editor, e){
					var record = me.getStore().getAt(e.rowIdx);
					if (e.field == "ClientName")
					{
						//record.set("Task", "");
						//Ext.getStore('timesheetTaskCode').load({
							//params:{ 'client_id':AM.app.globals.timesheetClient }
						//});
					}
					/*if (e.field == "Task")
					{
						record.set("SubTask", "");
						Ext.getStore('timesheetSubTask').load({
							params:{ 'client_id':AM.app.globals.timesheetClient,'task_name': e.record.data.Task }
						});
					}*/
					if((record.get(timesheet) != "") && (record.get(timesheet) != undefined)) {
						var datetime = String(record.get(timesheet));
						if(datetime.split(':').length == 2){
						var hh = datetime.split(':')[0],
						mm = datetime.split(':')[1]
						var d1 = new Date();
						d1.setHours(hh);
						d1.setMinutes(mm);
						}else{
						d1 = new Date(datetime);
						}

						//console.log(d1, datetime );
						var minute = d1.getMinutes();
						var hour = d1.getHours();

						minute = minute > 9 ? minute : '0' + minute;
						hour = hour > 9 ? hour : '0' + hour;
						console.log("hour > ",hour, "minute > ", minute );
						record.set(timesheet, hour+':'+minute);
					}
					else
					{
						record.set(timesheet, "");
					}
				/*	if(record.get(timesheetClient)!="")
					{
						record.set(timesheetClient, record.get(timesheetClient));
					}
					if(record.get("SubTask")!="")
					{
						record.set("SubTask", record.get("SubTask"));
					}*/
				}
			}
		});
		
		Ext.apply(me, {
			plugins: [me.editing]
		});
		
		var config = {
			columns:[],
			rowNumberer: false
		};
		
		// appy to this config
		Ext.apply(me, config);

		// apply to the initialConfig
		Ext.apply(me.initialConfig, config);
		
		
		this.tools = [{
			xtype: 'boxselect',
			store: 'TimesheetWeekrange',
			fieldLabel: 'For Week',
			labelCls : 'searchLabel',
			displayField: 'week', 
			valueField: 'id',
			id: 'timeWeekRange',
			multiSelect: false,
			forceSelection: true,
			allowBlank: false,
			width: 380,
			emptyText: 'Select Week',
			listeners: {
				change: function(combo){
					combobox=combo.getValue();
					Ext.getCmp('addTimeRow').setDisabled(false);
					Ext.getCmp('dupTimeRow').setDisabled(true);
					Ext.getCmp('saveTimeRow').setDisabled(false);
					AM.app.getController('TimesheetOnshore').listRecords(Ext.getCmp('TimesheetGridID'), combo.getValue(), Ext.getCmp('teamMember').getValue());
				}
			}
		}];
		
		this.tbar = [{
			xtype: "button",
			text: 'Add Entry',
			id: 'addTimeRow',
			disabled: true,
			icon : AM.app.globals.uiPath+'resources/images/Add.png',
			handler: function ()
			{
				var val= true;
				var r2 = Ext.create('AM.store.TimesheetOnshore');
				var recordItems = me.getStore().data;
				
				recordItems.each(function (recordLi) {
					if (recordLi.data.ClientName=="" && recordLi.data.Task=="" && recordLi.data.ServiceType=="" && recordLi.data.Engagement=="" && recordLi.data.Role=="" && recordLi.data.day1=="" && 
						recordLi.data.day2=="" && recordLi.data.day3=="" && recordLi.data.day4=="" && recordLi.data.day5=="" && recordLi.data.day6=="" && 
						recordLi.data.day7=="" ) 
					{
						val = false;
					}
				});
				
				if(val)
				{
					me.getStore().insert(0,r2);
					me.editing.cancelEdit();
					me.editing.startEdit(0, 1);
					me.editing.addnew = true;
					Ext.getCmp('saveTimeRow').enable();
				}
				else
				{
					Ext.MessageBox.show({
						title: "Alert",
						msg: "Please make use of existing empty row",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.WARNING,
						closable:false,
					});
				}
			}
		},{
			xtype: 'combobox',
			multiSelect: false,
			store: 'Employees',
			displayField: 'full_name', 
			valueField: 'employee_id',
			id: 'teamMember',
			minChars:0,
			width: 300,
			emptyText: 'Select Member',
			listeners : {
				beforequery : function(queryEvent) {
					// Ext.Ajax.abortAll();
					queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':1,'filterName':'employees.first_name'}; 
				},
				change : function(obj)
				{
					AM.app.getController('TimesheetOnshore').listRecords(Ext.getCmp('TimesheetGridID'), Ext.getCmp('timeWeekRange').getValue(), obj.getValue());
				}
			}
		}];
		
		this.bbar = [{
			xtype: "button",
			text: 'Save Changes',
			disabled: true,
			id: 'saveTimeRow',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler: function ()
			{
				var validate = ""; 
				var val = true;
				var recordItems = me.store.data;
				if(me.store.getModifiedRecords().length<1)
				{
					Ext.MessageBox.show({
						title: "Alert",
						msg: "No changes have been made to Timesheet",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.WARNING,
						closable:false,
					});
					val = false;
				}

				if(combobox == null)
				{
					Ext.MessageBox.show({
						title: "Alert",
						msg: "Please select week",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.WARNING,
						closable:false,
					});
					val = false;
				}
				
				var hasSubTasks = [];
				var hasTasks = [];
				recordItems.each(function (recordLi) {
					console.log(recordLi);
					//if (recordLi.data.ClientName=="" || recordLi.data.ServiceType=="" ||recordLi.data.Engagement=="" || recordLi.data.Task=="") 
					if (recordLi.data.Task!="Production Work" && (recordLi.data.ClientName=="" || recordLi.data.Task=="" || recordLi.data.ServiceType=="" ||recordLi.data.Engagement=="") ) 
					{
						Ext.MessageBox.show({
							title: "Alert",
							msg: "Mandatory fields are left blank",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.WARNING,
							closable:false,
						});
						val = false;
					}
					
				/*	if(recordLi.data.ClientName=="Theorem India Pvt Ltd" && recordLi.data.ClientName!="")
					{
						var noSubTasks = ["Production Work","Administrative Work", "Documentation", "Fun Activities","Non-Working Hrs","Holiday","Personal Leave","Inbox Mgmt.","Research","Ops. Mgmt.","Query Resolution","Prohance Administration"];
						if (noSubTasks.indexOf(recordLi.data.Task)=="-1") 
						{
							if(recordLi.data.SubTask=="")
							{
								hasSubTasks.push(recordLi.data.Task);
							}
						}
					}*/
				/*	if(recordLi.data.ClientName=="")
					{
						hasClient.push(recordLi.data.ClientName);
					}*/
				/*	if(recordLi.data.ClientName!="Theorem India Pvt Ltd" && recordLi.data.ClientName!="")
					{
						if(recordLi.data.Task!="Query Resolution" && recordLi.data.Task!="Production Work" && recordLi.data.Task!="Prohance Administration")
						{
							if(recordLi.data.SubTask=="")
							{
								hasSubTasks.push(recordLi.data.Task);
							}
						}
					}*/
				});
				
			/*	if(hasClient.length>0)
				{					
					
					Ext.MessageBox.show({
						title: "Alert",
						msg: "Please select appropriate <b>Client</b> <br>",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.WARNING,
						closable:false,
					});
					val = false;
				}*/
			/*	if(hasSubTasks.length>0)
				{
					var msgTask = "";
					hasSubTasks.forEach(function(rec) {
						msgTask += "<li>"+rec+"</li>";
					});
					
					Ext.MessageBox.show({
						title: "Alert",
						msg: "Please select appropriate <b>Sub Task</b> for the below Task(s) <br>"+msgTask,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.WARNING,
						closable:false,
					});
					val = false;
				}*/
				
				if(val)
				{
					recordItems.each(function (recordLi) {
						
						if (recordLi.data.Task!="Production Work" && recordLi.data.day1=="" && recordLi.data.day2=="" && recordLi.data.day3=="" 
							&& recordLi.data.day4=="" && recordLi.data.day5=="" && recordLi.data.day6=="" && recordLi.data.day7=="" ) 
						{
							Ext.MessageBox.show({
								title: "Alert",
								msg: "Enter permissible hours in the appropriate columns.",
								buttons: Ext.Msg.OK,
								icon: Ext.MessageBox.WARNING,
								closable:false,
							});
							val = false;
						}
					});
				}

				if(val)
				{	
					var timeArr = [];
					recordItems.each(function (recordLi) {
						if(recordLi.data.Task=="Holiday" || recordLi.data.Task=="Personal Leave")
						{
							if(recordLi.data.day1!= "" && recordLi.data.day1!="04:00" && recordLi.data.day1!="08:00")
							{
								timeArr.push(recordLi.data.day1);
							}
							if(recordLi.data.day2!= "" && recordLi.data.day2!="04:00" && recordLi.data.day2!="08:00")
							{
								timeArr.push(recordLi.data.day2);
							}
							if(recordLi.data.day3!= "" && recordLi.data.day3!="04:00" && recordLi.data.day3!="08:00")
							{
								timeArr.push(recordLi.data.day3);
							}
							if(recordLi.data.day4!= "" && recordLi.data.day4!="04:00" && recordLi.data.day4!="08:00")
							{
								timeArr.push(recordLi.data.day4);
							}
							if(recordLi.data.day5!= "" && recordLi.data.day5!="04:00" && recordLi.data.day5!="08:00")
							{
								timeArr.push(recordLi.data.day5);
							}
							if(recordLi.data.day6!= "" && recordLi.data.day6!="04:00" && recordLi.data.day6!="08:00")
							{
								timeArr.push(recordLi.data.day6);
							}
							if(recordLi.data.day7!= "" && recordLi.data.day7!="04:00" && recordLi.data.day7!="08:00")
							{
								timeArr.push(recordLi.data.day7);
							}
						}
					});
					timeArr.sort();
					if(timeArr.length>0 && timeArr[0]!="04:00" && timeArr[0]!="08:00")
					{
						Ext.MessageBox.show({
							title: "Alert",
							msg: "Time entered for Holiday/Personal Leave is not valid.<br>Please enter 04:00 for half day or 08:00 for full day.",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.WARNING,
							closable:false,
						});
						val = false;
					}
				}
				
				if(val)
				{
					var myArray = [];
					recordItems.each(function (recordLi) {
						if(recordLi.data.Task!="Production Work")
						{
							var cdata = recordLi.data.ClientName+recordLi.data.Task+recordLi.data.SubTask;
							myArray.push(cdata);
						}
					});
					var countArr = [];
					var noofDup = 0;
					for(var i = 0;i < myArray.length; i++)
					{
						if(countArr.indexOf(myArray[i]) == -1)
						{
							countArr.push(myArray[i]);
						}
						else
						{
							noofDup++;
						}
					}
					if(noofDup>0)
					{
						Ext.MessageBox.show({
							title: "Alert",
							msg: "Please use the pre-existing time entry row to avoid duplication of timesheet details.",
							buttons: Ext.Msg.OK,
							icon: Ext.MessageBox.WARNING,
							closable:false,
						});
						val = false;
					}
				}
				
				if(val)
				{
					Ext.getCmp('saveTimeRow').disable();
					for (var i = 0; i < me.store.data.items.length; i++) 
					{
						var record = me.store.data.items[i];
						if (record.dirty) 
						{
							// there was a change, it is necessary to record data
							// Looks at the changes to record.modified
							AM.app.getController('TimesheetOnshore').saveTimesheet(record.data, combobox, Ext.getCmp('teamMember').getValue(), Ext.getCmp('TimesheetGridID'));
						}
					}
				}
			}
		},{
			xtype: "button",
			text: 'Duplicate',
			disabled: true,
			icon : AM.app.globals.uiPath+'resources/images/duplicate.png',
			id: 'dupTimeRow',
			handler: function() {				
				var idAry = [];
				var myAry = AM.app.globals.dupTimeRec;
				if(myAry.length != 0 && !(myAry.length == 1 && myAry[0] == null))
				{
					for (var i=0; i< myAry.length; i++)
					{
						idAry.push(myAry[i].data.timesheetID);
					}
				}
				idAry = (Ext.Array.unique(idAry)).toString();
				
				if(idAry!="")
				{
					AM.app.getController('TimesheetOnshore').duplicateTimesheet(idAry);
				}
				else
				{
					Ext.MessageBox.show({
						title: "Alert",
						msg: "Please select row(s) to duplicate.",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.WARNING,
						closable:false,
					});
				}
			}
		},{
			xtype: 'displayfield',
			id: "TotalMonthHours",
			name: "OverallTime",
			width : '32%',
			style : {
				'text-align' : 'center'
			}
		},{
			xtype: 'displayfield',
			id: "TotalWeekHours",
			name: "TotalHours",
			width : '32%',
			style : {
				'text-align' : 'center'
			}
		},{
			xtype: 'displayfield',
			value: '<span style="color:red">* marked column are mandatory</span>',
			width : '20%',
			style : {
				'text-align' : 'right'
			}
		}];

		this.callParent(arguments);
		//this.plugins
	},

	/**
    * When the store is loading then reconfigure the column model of the grid
    */
    storeLoad: function(store,me)
    {
        /**
        * JSON data returned from server has the column definitions
        */
        if(typeof(store.proxy.reader.jsonData.columns) === 'object') 
        {
        	console.log(store.proxy.reader.jsonData.columns);
        	var columns = [];

			/**
            * assign new columns from the json data columns
            */
            columns.push({
            	xtype: 'checkcolumn',
            	flex: 0.3,
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	renderer: function(value, meta, record, row, col){
            		if (record.data['past']=="hide" || record.data['timesheetID'] == undefined || record.data['timesheetID'] == '0')
            		{
            			meta['tdCls'] = 'x-item-disabled x-unselectable';
            		} 
            		else 
            		{
            			meta['tdCls'] = '';
            		}
            		return new Ext.ux.CheckColumn().renderer(value);
            	}, 
            	listeners : {
            		checkchange: function(column, rowIdx, checked, eOpts){
            			var grid = this.up('grid');
            			var record = grid.getStore().getAt(rowIdx);

            			if(checked)
            			{
            				if(!(Ext.Array.contains(AM.app.globals.dupTimeRec,record)))
            				{	
            					AM.app.globals.dupTimeRec.push(record);
            				}
            			}
            			else
            			{
            				var myAry = AM.app.globals.dupTimeRec;						
            				var index = myAry.indexOf(record);
            				if (index > -1) {
            					myAry.splice(index, 1);
            				}
            				AM.app.globals.dupTimeRec = myAry;
            			}

            			if((AM.app.globals.dupTimeRec).length<1)
            			{
            				Ext.getCmp('dupTimeRow').setDisabled(true);
            				Ext.getCmp('saveTimeRow').setDisabled(false);
            			}
            			else
            			{
            				Ext.getCmp('dupTimeRow').setDisabled(false);
            				Ext.getCmp('saveTimeRow').setDisabled(true);
            			}
            		}
            	}
            },{
            	header : 'Client Name <span style="color:red">*</span>', 
            	dataIndex:'ClientName',
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	width: 180,
            	editor: {
            		xtype: 'combobox',
            		multiSelect: false,
            		store: Ext.getStore('timesheetClients'),
            		displayField: 'client_name',
            		valueField: 'client_name',
            		forceSelection: true,
            		minChars:0,
					width:'100%',
            		listeners : {
            			select : function(obj, metaData, record) {
            				AM.app.globals.timesheetClient = obj.getValue();
            				Ext.getStore('timesheetTaskCode').load({
            					params:{ 'client_id': obj.getValue() }
            				});
            			}
            		}
            	}
            },{
            	header : 'Service Type <span style="color:red">*</span>', 
            	dataIndex:'ServiceType',
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	width: 160,
            	editor: {
            		xtype: 'combobox',
            		multiSelect: false,
            		store: Ext.getStore('timesheetServiceTypes'),
            		displayField: 'service_type',
            		valueField: 'service_type',
            		forceSelection: true,
            		minChars:0,
					width:'100%'
            	}
            },{
            	header : 'Engagement <span style="color:red">*</span>', 
            	dataIndex:'Engagement',
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	width: 110,
            	editor: {
            		xtype: 'combobox',
            		multiSelect: false,
            		store: Ext.getStore('timesheetEngagement'),
            		displayField: 'engagement_type',
            		valueField: 'engagement_type',
            		forceSelection: true,
            		minChars:0,
					width:'100%'
            	}
            },{
            	header : 'Role', 
            	dataIndex:'Role',
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	width: 140,
            	editor: {
            		xtype: 'combobox',
            		multiSelect: false,
            		store: Ext.getStore('timesheetRole'),
            		displayField: 'role_type',
            		valueField: 'role_type',
            		forceSelection: false,
            		minChars:0,
					width:'100%'
            	}
            },{
            	header : 'Task <span style="color:red">*</span>', 
            	dataIndex:'Task',
            	menuDisabled:true,
            	groupable: false,
            	draggable: false,
            	sortable: false,
            	width: 150,
            	editor: {
            		xtype: 'combobox',
            		multiSelect: false,
            		store: Ext.getStore('timesheetOnshoreTask'),
            		displayField: 'task_type',
            		valueField: 'task_type',
            		forceSelection: true,
            		minChars:0
            	}
			});

Ext.each(store.proxy.reader.jsonData.columns, function(column) {

	if(column.dataIndex!="ClientName" && column.dataIndex!="Task" && column.dataIndex!="Engagement" && column.dataIndex!="ServiceType" && column.dataIndex!="Role" && column.dataIndex!="Production Work" )
	{
		var field = column.dataIndex;
		console.log(store.proxy.reader.jsonData.columns);
		var sumType = function(records, values)
		{
			console.log(records);
			var hour = 0, minute = 0, total="";

			for (var i=0; i < records.length; ++i) 
			{
				var time = records[i].get(field);
				if(time!="" && time != undefined)
				{
					var splitTime= time.split(':');

					var splitHour = (splitTime[0]!="") ? parseInt(splitTime[0]) : 0;
					var splitMinute = (splitTime[1]!="") ? parseInt(splitTime[1]) : 0;

					if(splitHour!="0")
					{
						hour += splitHour;
					}
					if(splitMinute!="0")
					{
						minute += splitMinute;
					}
				}
			}

			if (minute >= 60) 
			{
				var h = Math.floor(minute / 60);
				hour += h;
				minute = minute%60;
			}

			total = ("0" + hour).slice(-2)+':'+("0" + minute).slice(-2);
			total = (total!='00:00') ? ('<span style="font-size:18px;">'+total+'</span>') : '';

			return total;
		}

		column.summaryType = sumType;
	}

	columns.push(column);
});

columns.push({
	header : 'Action', 
	xtype: 'actioncolumn',
	menuDisabled:true,
	groupable: false,
	draggable: false,
	sortable: false,
	items: [{
		icon : AM.app.globals.uiPath+'/resources/images/delete.png',
		tooltip : 'Delete Row',
		handler : this.onDelete,
		getClass: function(v, meta, record) {

			if(record.data.Task == 'Production Work')
			{
				return 'x-hide-display';
			}
			else
			{ 
				return 'rowVisible';
			}
		}
	}, {
		xtype: 'tbspacer'
	}, {
		icon : AM.app.globals.uiPath+'/resources/images/view.png',
		tooltip : 'Comments',
		handler : this.onView,
		getClass: function(v, meta, record) {

			if((record.data.Task == 'Production Work' || record.data.timesheetID == undefined || record.data.timesheetID == '0'))
			{
				return 'x-hide-display';
			}
			else
			{ 
				return 'rowVisible';
			}
		}
	}]
});

            /**
            * reconfigure the column model of the grid
            */
            me.reconfigure(store, columns);
        }
    },


    onDelete : function(grid, rowIndex, colIndex) {
    	AM.app.getController('TimesheetOnshore').deleteTimesheet(grid, rowIndex, colIndex);
    },


    onView : function(grid, rowIndex, colIndex) {
    	var row = grid.getStore().getAt(rowIndex);
    	if(!row.dirty)
    	{
    		AM.app.getController('TimesheetOnshore').viewTimesheetComments(grid, rowIndex, colIndex);
    	}
    	else
    	{
    		Ext.MessageBox.show({
    			title: "Alert",
    			msg: "Please save the changes before adding comments.",
    			buttons: Ext.Msg.OK,
    			icon: Ext.MessageBox.WARNING,
    			closable:false,
    		});
    	}
    },	

    listeners: {
    	afterrender: function(obj) {
    		var dickItems = obj.getDockedItems();
    		var weekCombo = dickItems[0].items.items[1];

    		weekCombo.store.load({
    			callback: function(records)
    			{
    				weekCombo.setValue(records[2].get('id'));

    				obj.storeLoad(obj.view.store, obj);
    			}
    		});
			Ext.ComponentQuery.query('#TimesheetGridID')[0].getView().refresh();
    	}
    },

}); 