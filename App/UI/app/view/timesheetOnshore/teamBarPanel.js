Ext.define('AM.view.timesheet.teamBarPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.teambarpanel',
    requires:['AM.view.chart.TimePieChart'],	
	layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	minHeight:275,
    initComponent: function() {
		var me=this;
		this.activeItem  = 0;
		
    	Ext.apply(me, {
			store : Ext.create('AM.store.TimesheetStatusTeam')
        });
		
		// to here	
		this.items =[{
			store : me.store,
			id: 'teamPieChart',
			xtype:'chartTimePieChart'
        },{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }]
		
        this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
				{
					obj.getLayout().setActiveItem(1);
				}
				else
					obj.getLayout().setActiveItem(0);
			});
		}
	}
	
});