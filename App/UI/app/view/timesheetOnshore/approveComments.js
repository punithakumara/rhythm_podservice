Ext.define('AM.view.utilization.approveComments',{
	extend : 'Ext.window.Window',
	alias : 'widget.approveComments',
	layout: 'fit',
	title: 'View Comments',
	border : 'true',
	minWidth : 350,
	maxWidth : 600,
	minHeight: 200,
	maxHeight: 400,
	autoShow : true,
    modal: true,
	autoScroll : true,
	resizable:false,
	centered : true,
	
    initComponent : function() {
		
    	this.items = [{
			xtype: 'form',
			fieldDefaults: {
				labelAlign: 'left',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px 10px 6px 10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype : 'displayfield',
				fieldLabel : 'Client Name <span style="color:red">*</span>',
				name : 'ClientName',
			},{
				xtype : 'displayfield',
				fieldLabel : 'Task Name <span style="color:red">*</span>',
				name : 'Task',
			}, {
				xtype : 'displayfield',
				fieldLabel : 'Sub Task',
				name : 'SubTask',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Mon',
				name : 'comment1',
				id : 'comment1',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Tue',
				name : 'comment2',
				id : 'comment2',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Wed',
				name : 'comment3',
				id : 'comment3',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Thu',
				name : 'comment4',
				id : 'comment4',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Fri',
				name : 'comment5',
				id : 'comment5',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Sat',
				name : 'comment6',
				id : 'comment6',
			}, {
				xtype:'displayfield',
				fieldLabel: 'Sun',
				name : 'comment7',
				id : 'comment7',
			}]
    	}];
		
    	this.buttons = ['->', {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},

	
});