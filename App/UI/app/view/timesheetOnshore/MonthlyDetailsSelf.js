Ext.define('AM.view.timesheet.MonthlyDetailsSelf' ,{
    extend: 'Ext.Panel',
    alias: 'widget.MonthlyDetailsSelf',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me=this;
		this.activeItem  = 0;
		Ext.apply(me, {
			store : Ext.create('AM.store.MonthlyDetailsSelf',{
				remoteSort: true,
				autoLoad: true,
			})
        });
		
		this.items =[{
			xtype: 'grid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Month', 
				dataIndex: 'month',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1
			}, {
				header: 'Timesheet Hrs', 
				dataIndex: 'timesheetHrs', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
				renderer: function(value, meta, record, row, col) {
					if(value == '' || value < 0 )
					{
						return '0';
					}
					else
					{
						var sec_num = parseInt(value, 10);
						var hours   = Math.floor(sec_num / 3600);
						var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
						var seconds = sec_num - (hours * 3600) - (minutes * 60);

						if (hours   < 10) {hours   = "0"+hours;}
						if (minutes < 10) {minutes = "0"+minutes;}
						if (seconds < 10) {seconds = "0"+seconds;}
						return hours+':'+minutes ;
						
					}
				}
			},{
				header: 'Production Hrs', 
				dataIndex: 'productionHrs', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
				renderer: function(value, meta, record, row, col) {
					if(value == '' || value < 0 )
					{
						return '0';
					}
					else
					{
						var sec_num = parseInt(value, 10);
						var hours   = Math.floor(sec_num / 3600);
						var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
						var seconds = sec_num - (hours * 3600) - (minutes * 60);

						if (hours   < 10) {hours   = "0"+hours;}
						if (minutes < 10) {minutes = "0"+minutes;}
						if (seconds < 10) {seconds = "0"+seconds;}
						return hours+':'+minutes ;
						
					}
				}
			},{
				header: 'Total Hrs', 
				dataIndex: 'totalHrs', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1,
				renderer: function(value, meta, record, row, col) {
					if(value == '' || value < 0 )
					{
						return '0';
					}
					else
					{
						var sec_num = parseInt(value, 10);
						var hours   = Math.floor(sec_num / 3600);
						var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
						var seconds = sec_num - (hours * 3600) - (minutes * 60);

						if (hours   < 10) {hours   = "0"+hours;}
						if (minutes < 10) {minutes = "0"+minutes;}
						if (seconds < 10) {seconds = "0"+seconds;}
						return hours+':'+minutes ;
						
					}
				}
			}],
        },{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }]
		
        this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
					obj.getLayout().setActiveItem(1); //made 0 @refrence RHYT-844 issue
				else
					obj.getLayout().setActiveItem(0);
			}); 
		}
	}
});