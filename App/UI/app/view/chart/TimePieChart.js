Ext.define('AM.view.chart.TimePieChart',{
	extend: 'Ext.chart.Chart',    
	alias: 'widget.chartTimePieChart',
	requires:['Ext.ux.chart.LegendUnclickable'],
	// store:'ErrorsChart',
	animate: true,
	shadow: false,
	legend: {
	    position: 'right',
		labelFont: 'tahoma',
		boxStrokeWidth: 0,
	},
	insetPadding:30,
	theme: 'Base:gradients',
	series: [{
	    type: 'pie',
		angleField: 'Value1',
	    showInLegend: true,
	    tips: {
			trackMouse: true,
			width: 200,
			height: 40,
			renderer: function(storeItem, item) {
				if(storeItem.get('Name')=="Pending" || storeItem.get('Name')=="Approved" || storeItem.get('Name')=="Rejected")
				{
					var value = storeItem.get('Value1');
					var sec_num = parseInt(value, 10);
					var hours   = Math.floor(sec_num / 3600);
					var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
					var seconds = sec_num - (hours * 3600) - (minutes * 60);

					if (hours   < 10) {hours   = "0"+hours;}
					if (minutes < 10) {minutes = "0"+minutes;}
					if (seconds < 10) {seconds = "0"+seconds;}
					value = hours+':'+minutes ;
					
					this.setTitle('<div style="text-align:center;font-size:18px;padding-top:5px">'+ storeItem.get('Name')+' : '+value+'</div>');
				}
				else
				{
					this.setTitle('<div style="text-align:center;font-size:18px;padding-top:5px">'+ storeItem.get('Name')+' : '+storeItem.get('Value1')+'</div>');
				}
			}
	    },
	    highlightCfg : {opacity : '0.8'},
	    highlight: {
			segment: {
				margin:'10'
			}
	    },
	    label: {
	        field: 'Name',
	    },
		style: {
			'stroke-width': 0.7,
			'stroke': '#fff'
		},
		listeners:{
			itemmousedown : function(obj) {	
			
				var docsitems = obj.series.chart.ownerCt.up().items.items[0];
				
				var WorID = '';
				if(docsitems != undefined)
				{
					if(docsitems.items.items[1].items.items[1].items != undefined)
					{
						var ClientId = (docsitems.items.items[1].items.items[0].getValue() == undefined)?'':docsitems.items.items[1].items.items[0].getValue();
					}
					else
					{
						var WorID = docsitems.items.items[1].items.items[1].getValue();
						var ClientId = (docsitems.items.items[1].items.items[0].getValue() == undefined)?'':docsitems.items.items[1].items.items[0].getValue();
					}
					
					AM.app.getController('Dashboard').popupContents(obj,obj.storeItem.store.storeId,ClientId,WorID);
				}
			}
		},
		getLegendColor: function(index) {
			var me = this;
            var store = me.chart.substore || me.chart.store;
            var record = store.getAt(index);
			
			if(record.data.Name=="Pending")
			{
				return "#FEDC4B";
			}
			else if(record.data.Name=="Approved")
			{
				return "#9BAD3A";
			}
			else if(record.data.Name=="Rejected")
			{
				return "#9C3D47";
			}
		},
		renderer: function(sprite, record, attr, index, store) {
			if(record.data.Name=="Pending")
			{
				return Ext.apply(attr, {
					fill: [ "#FEDC4B"]
				});
			}
			if(record.data.Name=="Approved")
			{
				return Ext.apply(attr, {
					fill: [ "#9BAD3A"]
				});
			}
			if(record.data.Name=="Rejected")
			{
				return Ext.apply(attr, {
					fill: [ "#9C3D47"]
				});
			}
		},
	}],
})