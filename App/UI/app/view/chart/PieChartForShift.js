Ext.define('AM.view.chart.PieChart',{
	extend: 'Ext.chart.Chart',    
	alias: 'widget.chartPieChart',
	requires:['Ext.ux.chart.LegendUnclickable'],
	// store:'ErrorsChart',
	animate: true,
	shadow: false,
	
	height: 150,
	//autoScroll: true,

	/*legend: {
	    position: 'right',
		labelFont: 'tahoma',
		boxStrokeWidth: 0,

	},*/
	insetPadding:30,
	theme: 'Base:gradients',
	
	series: [{
	    type: 'pie',
		angleField: 'Value1',
	    showInLegend: true,
	    tips: {
			trackMouse: true,
			width: 300,
			height: 40,
			renderer: function(storeItem, item) {
			this.setTitle('<div style="text-align:center;font-size:18px;padding-top:5px">'+ storeItem.get('Name')+' : '+storeItem.get('Value1')+'</div>');
			
			}
	    },
	    highlightCfg : {opacity : '0.8'},
	    highlight: {
			segment: {
				margin:'10'
				
			}
	    },
		
	    /*label: {
	       field: 'Name'
			display : 'outside',
	        contrast: false,
			
			//orientation: 'rightContext',
	        font: '12px Arial',
	        renderer: function (name,e,obj) {
	            return obj.data.Value1 > 0 ? name : '';
	        }
	    },	*/
		

		style: {
			'stroke-width': 0.7,
			'stroke': '#fff',
			

		},
		listeners:{
			itemmousedown : function(obj) {	
			
				var docsitems = obj.series.chart.ownerCt.up().items.items[0];
				
				var ClientId = docsitems.items.items[1].items.items[0].getValue();
				AM.app.getController('Dashboard').popupContents(obj,obj.storeItem.store.storeId,null,ClientId,'');
			}
		}
	}]
})