Ext.define('AM.view.chart.BarChart' ,{
	extend: 'Ext.chart.Chart',    
	alias: 'widget.chartBarChart',
	requires:['Ext.ux.chart.LegendUnclickable'],
    animate: true,
    shadow: true,
    legend: {
		position: 'bottom'  ,
		boxStrokeWidth: 0,
		//clickable: false
    },
    axes: [ {
        type: 'Category',
        position: 'bottom',
        fields: ['Name'],
        title: 'Month of the year'
    },{
        type: 'Numeric',
        position: 'left',
        fields: ['Value1', 'Value2'],
        minimum: 0,
        label: {
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        grid: true,
        title: 'Number of Hits'
    }],
	plotOptions: {
        column: {
            stacking: 'normal'
        }
    },
    series: [{
        type: 'column',
		axis: 'bottom',
		xField: 'Name',
		yField: ['Value1', 'Value2'],
		title: ['Billable', 'Non Billable'],
		tips: {
            trackMouse: true,
            width: 70,
            height: 40,
			align:"center",
            renderer: function(storeItem, item) {
				this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        }
    }]
});