Ext.define('AM.view.chart.ProhanceAvgLoggChart', {
    extend          : 'Ext.chart.Chart',
    alias           : 'widget.chartProhanceAvgLogg',
    requires        : ['Ext.ux.chart.LegendUnclickable'],
    animate         : false,
    shadow          : true,
    legend          : { position    : 'bottom', boxStrokeWidth  : 0 },
    axes            : [ {
        type        : 'Category',
        position    : 'bottom',
        fields      : ['Name'],
        title       : 'Months in a Year'
    }, {
        type        : 'Numeric',
        position    : 'left',
        fields      : ['Value1', 'Value2'],
        minimum     : 0,
        grid        : true,
        title       : 'Hours',
    } ],
    plotOptions     : { column  : { stacking    : 'normal'} },
    series          : [ {
        type    : 'column',
        axis    : 'bottom',
        xField  : 'Name',
        yField  : ['Value1'],
        title   : ['Time In Hours'],
        tips    : { trackMouse  : true, width   : 70, height    : 40, align :"center",
            renderer: function(storeItem, item) {
                this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        }
    } ]
} );