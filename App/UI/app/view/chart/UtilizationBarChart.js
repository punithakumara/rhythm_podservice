Ext.define('AM.view.chart.UtilizationBarChart' ,{
   extend: 'Ext.chart.Chart',    
	alias: 'widget.chartUtilizationBarChart',
	requires:['Ext.ux.chart.LegendUnclickable'],
    animate: false,
    shadow: true,
    legend: {
      position: 'bottom'  ,
      boxStrokeWidth: 0
      //clickable: false
    },
    axes: [ {
        type: 'Category',
        position: 'bottom',
        fields: ['Name'],
        title: 'Month of the year'
    },{
        type: 'Numeric',
        position: 'left',
        fields: ['Value1', 'Value2'],
        minimum: 0,
        label: {
            //renderer: Ext.util.Format.numberRenderer('0,0')
        },
        grid: true,
        title: 'Percentage'
    }],
	plotOptions: {
        column: {
            stacking: 'normal'
        }
    },
    series: [{
        type: 'column',
		axis: 'bottom',
		xField: 'Name',
		yField: ['Value1'],
		title: ['Utilization'],
		tips: {
            trackMouse: true,
            width: 80,
            height: 40,
			align:"center",
            renderer: function(storeItem, item) {
                // console.log(item);
				// tips["background-color"] = 	;
				this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        }
    }]
});