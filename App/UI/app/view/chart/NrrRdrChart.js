Ext.define('AM.view.chart.NrrRdrChart' ,{
	extend: 'Ext.chart.Chart',    
	alias: 'widget.ChartNrrRdr',
	requires:['Ext.ux.chart.LegendUnclickable'],
    animate: true,
    shadow: true,
    legend: {
		position: 'bottom',
		boxStrokeWidth: 0,
		//clickable: false
    },
    axes: [ {
        type: 'Category',
        position: 'bottom',
        fields: ['Name'],
        title: 'Month'
    },{
        type: 'Numeric',
        position: 'left',
        maximum: 20,
        fields: ['Value1', 'Value2','Value3'],
        minimum: 0,
        label: {
            renderer: Ext.util.Format.numberRenderer('0,0')
        },
        grid: true,
        title: 'Numbers'
    }],
	plotOptions: {
        column: {
            stacking: 'normal'
        }
    },
    series: [{
        type: 'column',
		axis: 'bottom',
		xField: 'Name',
		yField: ['Value1', 'Value2','Value3'],
		title: ['Open WOR', 'Closed WOR', 'FTE'],
		tips: {
            trackMouse: true,
            width: 70,
            height: 40,
			align:"center",
            renderer: function(storeItem, item) {
				this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        },
        listeners:{
			itemmousedown : function(obj) {
				var docsitems = obj.series.chart.ownerCt.up().items.items[0].getDockedItems()[0];
				var ClientId = docsitems.items.items[0].items.items[0].getValue();
				AM.app.getController('Dashboard').popupContents(obj,obj.storeItem.store.storeId,ClientId);
			}
        }
    }]
});