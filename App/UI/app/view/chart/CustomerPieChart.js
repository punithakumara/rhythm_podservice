Ext.define('AM.view.chart.CustomerPieChart', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.customerpiechart',
    requires: ['Ext.ux.chart.LegendUnclickable'],
    animate: true,
    shadow: false,
    legend: {
        position: 'right',
        labelFont: 'tahoma',
        boxStrokeWidth: 0,
    },
    insetPadding: 30,
    theme: 'Base:gradients',
    series: [{
        type: 'pie',
        angleField: 'Value1',
        showInLegend: true,
        tips: {
            trackMouse: true,
            width: 100,
            height: 40,
            renderer: function(storeItem, item) {
                this.setTitle(
                    '<div style="text-align:center;font-size:24px">' + storeItem.get('Value1') + '</div>'
                );
            }
        },
        highlightCfg: {
            opacity: '0.8'
        },
        highlight: {
            segment: {
                margin: '0'
            }
        },
        label: {
            field: 'Name',
            display: 'rotate',
            contrast: true,
            orientation: 'rotate',
            font: '12px Arial',
            renderer: function(name, e, obj) {
                return obj.data.Value1 > 0 ? name : '';
            }
        }
    }]
})