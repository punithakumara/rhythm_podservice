Ext.define('AM.view.chart.monthlyErrorCountChart', {
    extend          : 'Ext.chart.Chart',
    alias           : 'widget.chartMonthlyErrorCount',
    requires        : ['Ext.ux.chart.LegendUnclickable'],
    animate         : false,
    shadow          : true,
    legend          : { position    : 'bottom', boxStrokeWidth  : 0 },
    axes            : [ {
        type        : 'Category',
        position    : 'bottom',
        fields      : ['Month'],
        title       : '',      
    }, {
        type        : 'Numeric',
        position    : 'left',
        fields      : ['count'],
        minimum     : 0,
        grid        : true,
        title       : '',
        //maximum: '',
    } ],
    plotOptions     : { column  : { stacking    : 'normal'} },
    series          : [ {
        type    : 'column',
        axis    : 'bottom',
        xField  : 'Name',
        yField  : ['count'],
        label   : { field: ['count'], display: 'outside', orientation: 'horizontal', font: 'bold 14px Arial', color: '#FE8DOO'},
        title   : ['Month'],
        tips    : { trackMouse  : true, width   : 70, height    : 40, align :"center",
            renderer: function(storeItem, item) {
                this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        },
        listeners:{
			itemmousedown : function(obj) {									
				var storeName = obj.storeItem.store.$className;					
				var verticalID = Ext.getCmp('verticalCombo2').getValue();
				var selMonth = obj.storeItem.data.Month;
				if (storeName == 'AM.store.monthlyErrorCountChart')
				{
					logType = 'Error'
				}
				else if (storeName == 'AM.store.monthlyAppreciationCountChart')
				{
					logType = 'Appreciation';
				}
				else if (storeName == 'AM.store.monthlyEscalationCountChart')
				{
					logType = 'Escalation';
				}
				AM.app.getController('MprDashboard').ChartpopupContents(logType, verticalID, selMonth);
			}
        }
    } ]
} );