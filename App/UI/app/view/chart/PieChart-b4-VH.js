Ext.define('AM.view.chart.PieChart',{
	extend: 'Ext.chart.Chart',    
	alias: 'widget.chartPieChart',	
	requires:['Ext.ux.chart.LegendUnclickable'],
	// store:'ErrorsChart',
	animate: true,
	shadow: false,
	legend: {
	    position: 'right',
		labelFont: 'tahoma',
		boxStrokeWidth: 0,
		//clickable: false
	},
	insetPadding:30,
	theme: 'Base:gradients',
	series: [{
	    type: 'pie',
		angleField: 'Value1',
	    showInLegend: true,
	    tips: {
			trackMouse: true,
			width: 100,
			height: 40,
			renderer: function(storeItem, item) {
				this.setTitle('<div style="text-align:center;font-size:24px">'+ storeItem.get('Value1')+'</div>');
			}
	    },
	    highlightCfg : {opacity : '0.8'},
	    highlight: {
			segment: {
				margin:'0'
			}
	    },
	    label: {
	        field: 'Name',
	        display: 'rotate',
	        contrast: true,
			orientation: 'rotate',
	        font: '12px Arial',
	        renderer: function (name,e,obj) {
	            return obj.data.Value1 > 0 ? name : '';
	        }
	    },
		listeners:{
			itemmousedown : function(obj) {
				var docsitems = obj.series.chart.ownerCt.up().items.items[0].getDockedItems()[0];
				if(docsitems != undefined)
				{
					var ClientId=docsitems.items.items[0].items.items[0].getValue();
					var ProcessId=docsitems.items.items[0].items.items[1].getValue();
					AM.app.getController('Dashboard').popupContents(obj,obj.storeItem.store.storeId,ClientId,ProcessId);
				}
			}
		}
	}]   
})