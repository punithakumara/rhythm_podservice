Ext.define('AM.view.chart.ErrorCountChart', {
    extend          : 'Ext.chart.Chart',
    alias           : 'widget.chartErrorCount',
    requires        : ['Ext.ux.chart.LegendUnclickable'],
    animate         : false,
    shadow          : true,
    height			: 500,
    legend          : { position    : 'bottom', boxStrokeWidth  : 0 },
    axes            : [ {
        type        : 'Category',
        position    : 'bottom',
        fields      : ['verticalName'],
        label : {
            renderer: function(v){
              return v.substring(0, 12); 
            },
            rotate: {
                degrees: -90
            },          
         },
    }, {
        type        : 'Numeric',
        position    : 'left',
        fields      : ['count'],
        minimum     : 0,
        grid        : true,
        title       : '',
        //maximum		: '',
    } ],
    plotOptions     : { column  : { stacking    : 'normal'} },
    series          : [ {
        type    : 'column',
        axis    : 'bottom',
        xField  : 'Name',
        yField  : ['count'],
        label   : { field: ['count'], display: 'outside', orientation: 'horizontal', font: 'bold 14px Arial', color: '#FE8DOO'},
        title   : ['Verticals'],
        tips    : { trackMouse  : true, width   : 70, height    : 40, align :"center",
            renderer: function(storeItem, item) {
                this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        },
        listeners:{
			itemmousedown : function(obj) {							
				var storeName = obj.storeItem.store.$className;	
				var selMonth = Ext.getCmp('monthYear').getValue();
				var verticalID = obj.storeItem.data.VerticalID;
				if (storeName == 'AM.store.ErrorCountStore')
				{
					logType = 'Error'
				}
				else if (storeName == 'AM.store.AppreciationCountStore')
				{
					logType = 'Appreciation';
				}
				else if (storeName == 'AM.store.EscalationCountStore')
				{
					logType = 'Escalation';
				}
				AM.app.getController('MprDashboard').ChartpopupContents(logType, verticalID, selMonth);
			}
         }
    } ]
} );