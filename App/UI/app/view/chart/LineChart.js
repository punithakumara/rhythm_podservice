Ext.define("AM.view.chart.LineChart", {
		extend: 'Ext.chart.Chart',
		alias: 'widget.chartLineChart',
        animate: false,
        axes: [{
            type: 'Numeric',
            position: 'left',
            fields: ['Value1'],
            label: {
                renderer: Ext.util.Format.numberRenderer('0,0')
            },
            title: 'Percentage',
            grid: true,
            minimum: 50
        }, {
            type: 'Category',
            position: 'bottom',
            fields: ['Name'],
            title: 'Month of the year',
			//grid: true
        }],
		series: [{
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            xField: 'Name',
            yField: 'Value1',
			tips: {
				trackMouse: true,
				width: 80,
				height: 40,
				align:"center",
				renderer: function(storeItem, item) {
				this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
				}
			},
            markerConfig: {
                type: 'cross',
                size: 4,
                radius: 4,
                'stroke-width': 0
            }
        }]
        
    });