Ext.define('AM.view.chart.ShiftBarChart',{
   extend   : 'Ext.chart.Chart',
   alias    : 'widget.chartShiftBarChart',
   requires : ['Ext.ux.chart.LegendUnclickable'],
   animate  : false,
   shadow   : true,
   legend   : { position    : 'bottom', boxStrokeWidth  : 0 },
   axes     : [ {
        type    : 'Category',
        position: 'bottom',
        fields  : ['Name'],
        title   : 'Shifts'
    }, {
        type    : 'Numeric',
        position: 'left',
        fields  : ['Value1', 'Value2'],
        minimum : 0,
        grid    : true,
        title   : 'Count'
    } ],
    plotOptions : { column  : { stacking    : 'normal' } },
    series      : [ {
        type      : 'column',
		axis      : 'bottom',
		xField    : 'Name',
		yField    : ['Value1'],
		title     : ['Number of Employees'],
        tips      : {
            trackMouse  : true,
            width       : 70,
            height      : 40,
			align       :"center",
            renderer    : function(storeItem, item) {
                this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
            }
        },
        listeners   : {
            itemmousedown   : function(obj) {
                var docsitems       = obj.series.chart.ownerCt.up().items.items[0].getDockedItems()[0];
                var docsitems       = obj.series.chart.ownerCt.up().items.items[0];
                var groupvertical   = '';
                var ClientId        = docsitems.items.items[1].items.items[0].getValue();
               // var groupvertical   = docsitems.items.items[1].items.items[0].getValue();
            
                AM.app.getController('Dashboard').popupContents(obj, obj.storeItem.store.storeId, null, ClientId, groupvertical);
			}
        }
    } ]
} );