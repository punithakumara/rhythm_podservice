Ext.define('AM.view.tool&techReport.ToolTechFilters', {
	extend  	: 'Ext.form.Panel',
	alias   	: 'widget.ToolTechFilters',
	layout  	: 'fit',
	collapsible	: true,
	collapsed	: true,
	floatable	: true,
	width 		: '99%',
	id			:'ToolTechFilters',
	title   	: 'Filter Reports',
	listeners	: {
		afterrender	: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
  
	initComponent : function() {
  		var me = this;
  		
		this.items = [ {
			xtype	:'container',
			layout	: { type: 'vbox', align: 'stretch'},
			width	: 800,
			heigth	: 500,
			style	: { 'margin-left':'20px' },
			items	: [ {
				xtype		:'container',
				layout		: { type: 'hbox'},
				defaults	: { 
					labelWidth: 80,
					style	: {'padding': '20px'} 
				},
				width	: 800,			        
				items	: [ {
					xtype : 'boxselect',
					style:{
						'position':'absolute',
						'margin-right':'40px',
						'margin-top':'10px'
					},
					width:350,
					multiSelect: false,
					fieldLabel: 'Department',
					name: 'VerticalID',
					id :"Parentvrtclbufferrpt",			        			
					store           : Ext.create('AM.store.ParentVertical'),			        			
					displayField    : 'pVerticalName',
					width           : 280,			        	        
					valueField      : 'id',
					queryMode: 'remote',
					minChars:0,			        		
					emptyText: 'Select Department',
					listeners: {
						change: function(obj){
							if(obj.getValue()==null )
								if(Ext.getCmp('BuffermanagerID') == undefined )
									console.log();
								else	
								Ext.getCmp('BuffermanagerID').enable();
							else
							{
								if(Ext.getCmp('BufferAMID')!=undefined)
								{		
									Ext.getCmp('BufferAMID').reset();
									Ext.getCmp('BufferTLId').reset();
									Ext.getCmp('BuffermanagerID').reset();
									Ext.getCmp('BufferClientID').reset();
									Ext.getCmp('BufferProcessID').reset();
									Ext.getCmp('BuffermanagerID').disable();
								}		
							}
							Ext.getStore('Vertical').load({
								params:{ 'pvertical': obj.getValue()}
							});
							obj.next().bindStore('Vertical');		
						}
					}
				},{
					xtype 		: 'boxselect',
					style		: { 'position':'absolute', 'margin-right':'40px', 'margin-top':'10px'},
					width		: 300,
					multiSelect	: false,
					fieldLabel	: 'Vertical',
					name		: 'VerticalID',
					id 			: "ToolUtilVerticalID",
					store		: 'Vertical',
					queryMode	: 'remote',
					minChars	: 0,
					displayField: 'Name', 
					valueField	: 'VerticalID',
					emptyText	: 'Select Vertical',
					listeners	: {
						select		: function(obj){
							var AMStore 				= obj.next().store;
							AMStore.proxy.extraParams	= {'VerticalID': obj.getValue()};
							AMStore.load();
						},
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'pvertical':Ext.getCmp('Parentvrtclbufferrpt').getValue(),'hasNoLimit':'1','comboVertical':'1','filterName' : queryEvent.combo.displayField};
						}
					}, 
				},{
					xtype 		: 'boxselect',
					style			: { 'position':'absolute','margin-top':'10px'},
					width			: 300,
					multiSelect	: false,
					fieldLabel	: 'Client',
					// name			: 'ClientID',
					//id			: 'reportClientId',
					store			: AM.app.getStore('Clients'),
					id 			:'ToolUtilClientID',
					queryMode		: 'remote',
					minChars		: 0,
					displayField	: 'Client_Name',
					valueField	: 'ClientID',
					emptyText		: 'All Clients',
					listeners		: {
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							var VerticalID = queryEvent.combo.prev().getValue() ? queryEvent.combo.prev().getValue() : '';
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1', 'VerticalID': VerticalID, 'filterName' : queryEvent.combo.displayField, 'comboClient':'1'};
						},
						select		: function(combo){
							var VerticalID = combo.prev().getValue() ? combo.prev().getValue() : '';
							Ext.getStore('Process').load({params:{'VerticalID': VerticalID, ClientID: combo.getValue()}});
							combo.next().bindStore('Process');
						}
					}
				},{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',
						'margin-left':'40px',
						'margin-top':'10px'
					},
					id:'ToolUtilProcessID',
					fieldLabel: 'Process',
					store			: AM.app.getStore('Process'),
					queryMode: 'remote',
					name: 'processID',
					multiSelect: false,
					minChars:0,
					displayField: 'Name', 
					valueField: 'processID',
					emptyText: 'All Process',
					listeners: {
						beforequery: function(queryEvent){
							var VerticalID = Ext.getCmp('ToolUtilVerticalID').getValue() ? Ext.getCmp('ToolUtilVerticalID').getValue() : '';
							
							queryEvent.combo.store.proxy.extraParams ={
								'hasNoLimit':'1',
								'comboProcess':'1',
								'ClientID':queryEvent.combo.prev().getValue(),
								'VerticalID': VerticalID,
								'filterName' : queryEvent.combo.displayField
							}
						}
					}
				}]
			},{
				xtype:'container',
				layout: {
					type: 'hbox',
					//align: 'stretch'
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',
					}
				},
            	width: 800,
             	items: [{
					xtype: 'boxselect',
					width:280,
					style:{
						'position':'absolute',				        					
						//'margin-left':'40px',
						'margin-left':'0px',
						'margin-top':'10px'
					},
					fieldLabel: 'Manager',
					name: 'EmployID',
					store: AM.app.getStore('Managers'),
					id : 'ToolUtilmanagerID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'EmployID',
					multiSelect: false,
					emptyText: 'Select Manager',
					listeners: {
						select: function(obj){
							//AM.app.getController('ConsolidatedUtilizationReport').onSelectManager(obj)
							var AMStore = obj.next().store;
							AMStore.proxy.extraParams = {'EmployID': obj.getValue()};
							AMStore.load();
						}
					 
					}
				},{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-top':'10px'
					},
					fieldLabel: 'Associate Manager',
					name: 'EmployID',
					store: AM.app.getStore('Associate_manager'),
					id: 'ToolUtilAMID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'EmployID',
					multiSelect: false,
					emptyText: 'Associate Manager',
					
				}]
			},{
				xtype:'container',
				layout: {
					type: 'hbox',
					//align: 'stretch'
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',
					   
					}
				},
				width: 800,
				items: [{
					xtype: 'boxselect',
					width:300,
					style:{
					'position':'absolute',				        										        				
						'margin-top':'10px',
						'margin-bottom':'5px',
						//'margin-left':'40px'
					},
					fieldLabel: 'Team Lead',
					name: 'EmployID',
					store: AM.app.getStore('Leads'),
					id:'ToolUtilTLId',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'EmployID',
					multiSelect: false,
					emptyText: 'Team Lead',
					listeners: {
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'EmplyID':Ext.util.Cookies.get("empid"),'ProID':Ext.getCmp('ConUtilmanagerID')};
						}
					}
				}]
			}]
		} ]
		
		this.buttons = [ {
			text: 'Submit',
			name: 'ShowToolTech',
			//action:"showGridPanel",
			handler: function() {
				me.showGridPanel();
			}
		},{
			text: 'Reset',
			name: 'ShowToolTechReset',
			handler: function() {
				this.up('ToolTechFilters').getForm().reset();
			}
		}];
		
		this.callParent(arguments);
	
		if(Ext.util.Cookies.get("grade") == 3)
		{
			Ext.getCmp('ToolUtilmanagerID').setVisible(false);
			Ext.getCmp('ToolUtilAMID').setVisible(false);
			Ext.getCmp('ToolUtilTLId').setVisible(false);
			Ext.getCmp('ToolUtilVerticalID').setVisible(false);
			Ext.getCmp('Parentvrtclbufferrpt').setVisible(false);
		}
		else if(Ext.util.Cookies.get("grade") == 4)
		{
			Ext.getCmp('ToolUtilmanagerID').setVisible(false);
			Ext.getCmp('ToolUtilAMID').setVisible(false);
			// Ext.getCmp('ToolUtilVerticalID').setVisible(false);
			Ext.getCmp('Parentvrtclbufferrpt').setVisible(false);

		}
		else if(Ext.util.Cookies.get("grade") >=5)
		{
			Ext.getCmp('ToolUtilTLId').setVisible(false);
		}
		else
		{
			Ext.getCmp('ToolUtilVerticalID').setVisible(false);
			Ext.getCmp('ToolUtilmanagerID').setVisible(false);
			Ext.getCmp('ToolUtilAMID').setVisible(false);
			Ext.getCmp('ToolUtilTLId').setVisible(false);
			Ext.getCmp('Parentvrtclbufferrpt').setVisible(false);
		}	 
	},
	
	showGridPanel: function()
	{
		var ProcessID	= Ext.ComponentQuery.query('#ToolUtilProcessID')[0].value;
		var ClientID  	= Ext.ComponentQuery.query('#ToolUtilClientID')[0].value;
		var VerticalID 	= Ext.ComponentQuery.query('#ToolUtilVerticalID')[0].value;
		var managerID 	= Ext.ComponentQuery.query('#ToolUtilmanagerID')[0].value;
		var AMID 		= Ext.ComponentQuery.query('#ToolUtilAMID')[0].value;
		var TLId 		= Ext.ComponentQuery.query('#ToolUtilTLId')[0].value;
		var data = Ext.getStore('TechReport').getProxy().extraParams = {tooltechReport:1,ClientID:ClientID, VerticalID: VerticalID,ProcessID:ProcessID, managerID:managerID,AMID:AMID,TLId:TLId};
		Ext.getStore('TechReport').load();
	}
});
