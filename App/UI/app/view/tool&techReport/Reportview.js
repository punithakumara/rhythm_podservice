Ext.define('AM.view.tool&techReport.Reportview', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.toolreptView',
	width : '99%',
	minHeight : 500,
	border : false,
	layout  : 'fit',
	initComponent : function() {
	
		this.items= [{
			xtype:'container',
			layout:
			{
			   "type": "vbox",
			   "align": "stretch"
			},
			width: "100%",
			height:"100%",
			items:[{
				xtype: 'ToolTechFilters',
				margin:'0 0 10 0'
			},{
				xtype:AM.app.getView('tool&techReport.List').create()	
			}]
		}]
		
		this.callParent(arguments);
	},
	
});
