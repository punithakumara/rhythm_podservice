Ext.define('AM.view.tool&techReport.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.tooltechList',	
	title: 'Tools And Technology List',    
    store : 'TechReport',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
    width : '95%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	border : true,	
	style:{
 		border		: "0px solid #157fcc",
 		borderRadius:'0px'
	},
	frame: true,
	height: 'auto',
    initComponent : function() {
		this.columns = [ {
			header : 'Client', 
			dataIndex : 'Client_Name', 
			flex: 0.4
		}, {
			header : 'Client Type', 
			dataIndex : 'industryType', 
			flex: 0.4
		},{
			header : 'Tools/Platforms', 
			width    : '39%',
			dataIndex : 'Name',
			flex: 1,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("Name") + '"';
				return value;
			}
		},{
			header : 'Activities', 
			width    : '39%',
			dataIndex : 'Activities',
			flex: 1,
			renderer:function(value,metaData,record,colIndex,store,view) {	
				if(record.get("Activities")!=null){
					metaData.tdAttr = 'data-qtip="' + record.get("Activities") + '"';
					return value;
				}
			}
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Technologies to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
	
	tools : [{
    	xtype: "button",
    	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
		text : 'Export To Excel',
		style: {
            "margin-right": "8px"
        },
        handler: function(obj){
    		AM.app.getController('Tech').downloadtooltechCSV();
		}
	}, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
	},{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        fieldLabel : '',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }]
});