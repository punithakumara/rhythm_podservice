Ext.define('AM.view.SearchTrigger', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.searchtrigger',
    triggerCls: 'x-form-clear-trigger',
    trigger2Cls: 'x-form-search-trigger',
    onTriggerClick: function() {
        this.setValue('')
        this.setFilter(this.up().dataIndex, '')
    },
    onTrigger2Click: function() {
        this.setFilter(this.up().dataIndex, this.getValue())
    },
    setFilter: function(filterId, value){
        var store = this.up('grid').getStore();
        var delextrapram = this.up('grid').getStore();
        delete delextrapram.getProxy().extraParams;
        
		store.removeFilter(filterId, false);
		var filter = {id: filterId, property: filterId, value: value};
		if(true) filter.anyMatch = true
		if(this.caseSensitive) filter.caseSensitive = this.caseSensitive
		if(this.exactMatch) filter.exactMatch = this.exactMatch
		if(this.operator) filter.operator = this.operator
		store.addFilter(filter)
    },
    listeners: {
        render: function(){
            var me = this;
            me.ownerCt.on('resize', function(){
                me.setWidth(this.getEl().getWidth())
            })
        },
        change: function() {
            if(this.autoSearch) this.setFilter(this.up().dataIndex, this.getValue())
        }
    }
})