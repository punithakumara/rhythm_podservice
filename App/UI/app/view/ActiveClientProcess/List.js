Ext.define('AM.view.ActiveClientProcess.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.activeclientprocesList',
	
	title: 'Active Client/Process List',
    
    store : 'ActiveClientProcess',
    id : 'activeclientGridID',
    layout : 'fit',
    viewConfig : {
	  forceFit : true,
	},
    width : '99%',
//    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
    style:{
  		border: "0px solid #157fcc",
        borderRadius:'0px'
	},
	frame: true,
	height: 510,
   // cls: "custom-grid",
    initComponent : function() {
		
		this.tools = [ {
			xtype: 'boxselect',
			multiSelect: false,	
			labelStyle      : 'color: white', 
			store: 'GroupVerticalCombo',
			queryMode: 'remote',
			minChars:0,				
			displayField: 'Name', 
			valueField: 'VerticalID',
			fieldLabel: 'Department',
			id              : 'clientProcessRept',
			width           : 400,
			style           : {'margin-right' : '5px'},
			listeners: {
				'change': function(combo, record, index) {		
				 AM.app.getController('Clients').changeParentVertical(combo);
				},
				afterrender : function(combo) {
	               		combo.setValue(combo.store.first().data.VerticalID)              		
	            }
			},
			
		},/*{
	        xtype           : 'combo',
	        multiSelect     :  false,
	        fieldLabel      : 'Department',
	        labelStyle      : 'color: white',        
	        style           : {'margin-right' : '5px'},
	        store           : 'ParentVertical',			        			
	        displayField    : 'pVerticalName',
	        valueField      : 'Id',
			queryMode		: 'remote',
	        width           : 240,
	        id              : 'clientProcessRept',    
	        listeners       : {
	            select  : function(combo) {           	
	               AM.app.getController('Clients').changeParentVertical(combo);
	            },
	            afterrender : function(combo) {
	            	combo.store.on( 'load', function( store, records, options ) {
	            		if(store.getCount() > 0)
	                	{
	                		combo.setValue(store.first().data.Id)
	                		
	                	}
	            	});              	
	            }
	        }
	    },*/{
	    	//xtype: 'exporterbutton',
	    	xtype: "button",
	    	icon: AM.app.globals.uiPath+'resources/images/add.png',
			text : 'Export To Excel',
			style: {
	            "margin-right": "8px"
	        },
	      handler: function(obj){
	    		AM.app.getController('Clients').downloadactClientProcessCSV();
			}
		},{ 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    },{
	        xtype : 'trigger',
	        itemId : 'gridTrigger',
	        fieldLabel : '',
	        labelWidth : 60,
	        labelCls : 'searchLabel',
	        triggerCls : 'x-form-clear-trigger',
	        emptyText : 'Search',
	        width : 250,
	        minChars : 1,
	        enableKeyEvents : true,
	        onTriggerClick : function(){
	            this.reset();
	            this.fireEvent('triggerClear');
	        }
	    }, {
	        xtype: "tbseparator",
	        style: {
	            "margin-right": "5px"
	        }
	    }
	  
	     ]
    	this.columns = [      
				{
					header : 'Vertical', 
					id : 'vertical_Name',
					flex: 1.5,
					dataIndex : 'vertical_Name',
				},
				{
					header : 'Client Name', 
					id : 'Name',
					flex: 1.5,
					dataIndex : 'Client_Name',
					renderer:function(value,metaData,record,colIndex,store,view) {					
						metaData.tdAttr = 'data-qtip="' + record.get("Client_Name") + '"';
						return value;
					}
				},
				{
					header : 'Process', 
					id : 'process_Name',
					flex: 1,
					dataIndex : 'process_Name',
					renderer:function(value,metaData,record,colIndex,store,view) {					
					metaData.tdAttr = 'data-qtip="' + record.get("process_Name") + '"';
					return value;
				}
				},
				{
					header : 'Type', 
					id : 'industryType',
			
					dataIndex : 'industryType',
				},
				{
					header : 'Status', 
					id : 'status',
					dataIndex : 'status',
					renderer:function(){
							return 'Live'
					}
				},
				{
					header : 'Billed Count', 
					id : 'ApprovedHC',
					
					dataIndex : 'ApprovedHC',
					
				},
				{
					header : 'Approved FTE', 
					id : 'resourceCount',
				
					dataIndex : 'resourceCount',
					
				},
				{
					header : 'Client Since', 
					id : 'ClientSince',
				
					dataIndex : 'ClientSince',
					
				},
				/*{
					header : 'Termination Date', 
					id : 'inactiveDate',
				
					dataIndex : 'inactiveDate',
					
				},*/
				{
					header : 'Process Owner', 
					id : 'Lead',
					flex: 2,
					dataIndex : 'Lead',
					renderer:function(value,metaData,record,colIndex,store,view) {					
						metaData.tdAttr = 'data-qtip="' + record.get("Lead") + '"';
						return value;
					}
				},
				{
					header : 'Associate Manager', 
					id : 'AManager',
					flex: 1.5,
					dataIndex : 'AManager',
					
				},
				{
					header : 'Account Manager', 
					id : 'accManager',
					flex: 1.5,
					dataIndex : 'accManager',
					renderer:function(value,metaData,record,colIndex,store,view) {					
						metaData.tdAttr = 'data-qtip="' + record.get("accManager") + '"';
						return value;
					}
				}];

		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No clients to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
});