//Designation
Ext.define('AM.view.Designation.Add_edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.DesignationAdd_edit',
			
	title: 'Designation List',
	layout : 'fit',
	width : '40%',
    autoShow : true,
    modal: true,
	
    initComponent : function() {
	
	var status = new Ext.data.SimpleStore({
		fields: ['opvalue', 'grades'],
		data: [
		       ['1', 'Grade 1'],
		       ['2', 'Grade 2'],
		       ['3', 'Grade 3'],
		       ['3.1', 'Grade 3.1'],
		       ['4', 'Grade 4'],
		       ['5', 'Grade 5'],
		       ['6', 'Grade 6'],
		       ['7', 'Grade 7'],
		       ]
	});
	
	this.items = [
	       	   		{
	       	   		  xtype : 'form',
	       	   		  fieldDefaults: {
		    		    labelAlign: 'left',
		    		    labelWidth: 150,
		    	        anchor: '100%'
	       	   			},
	       	   			bodyStyle : {
	    	    			border : 'none',
	    	    			display : 'block',
	    	    			float : 'left',
	    	    			padding : '10px'
	    	    		},
	    	    		items : [
									{
										 itemId :'Name',
										 xtype : 'textfield',
										 maskRe: /[A-Za-z0-9.-]/,
										 name : 'name',
										 allowBlank: false,
										 listeners:{
											 scope: this,
											 'blur': function(combo){
											   combo.setValue(combo.getValue().trim());
											 }
											 },
										 fieldLabel : 'Designation Name',
										 
										 anchor: '100%'
									},
									{
										 xtype : 'boxselect',
										 multiSelect: false,
										 fieldLabel: 'Grade',
										 name: 'grades',
										 store: status,
										 queryMode: 'local',
										 minChars:0,
										 displayField: 'grades', 
										 valueField: 'opvalue',										 
										 allowBlank: false, //added blank validation
										 emptyText: 'Select Grade',
										 anchor: '100%',
									 }
	 							],
	 							
 							buttons : ['->', {
 				        	   text : 'Save',
							   // visible: AM.app.globals.accessObj[0]['addEdit']['designation']['visible'],
							   // hidden:AM.app.globals.accessObj[0]['addEdit']['designation']['hidden'],
 				        	   icon : AM.app.globals.uiPath+'resources/images/save.png',
 				        	   handler : this.onSave
 				        	   
 				           },{
	 		        	   text : 'Cancel',
	 		        	   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
	 		        	   handler : this.onCancel
 				           }
 				     	]
	       	   		}
	       	   	];
	       			
	   		this.callParent(arguments);
	},
	defaultFocus:'Name',
	onSave :  function() {
		var form = this.up('form').getForm();

        if (form.isValid()) {
        	var win = this.up('window');
        	AM.app.getController("Designation").onSaveDesignation(form);
            win.close();
        }
	},
	onCancel : function() {
		this.up('window').close();
	}

});