//Designation
Ext.define('AM.view.Designation.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.DesignationList',
	title: 'Designation List',
	store: 'Designations',
	layout : 'fit',
    viewConfig: {
		forceFit: true
	},
	frame: true,
	width: "99%",
	height: 'auto',
	style: {
		border: "0px solid #157fcc",
		borderRadius:'0px'
	},
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
	
    initComponent : function() {	
		
		this.columns = [{
			header : 'Designation Name', 
			flex: 1,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			dataIndex : 'name'
		}, {
			header : 'Grade', 
			dataIndex : 'hcm_grade', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Grade Level', 
			dataIndex : 'grades', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}];		

		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Designation to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
	
	tools : [{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        fieldLabel : '',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }],
	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController("Designation").onEditDesignation(grid, rowIndex, colIndex);
	}
	
});