Ext.define('AM.view.insight.GeneralList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.GeneralList',
	requires : ['Ext.ux.grid.plugin.PagingSelectionPersistence'],
	store : 'UtilizationDashboard',
	id : 'InsightGridID',
	layout : 'fit',
	selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		forceFit : true,
	},
	width : '100%',
	height : 640,
	loadMask: true, 
	border : true,
	plugins : [{ ptype : 'pagingselectpersist' }],
	viewConfig: { 
		deferEmptyText: false,
		getRowClass: function(record, rowIndex, rowParams, store) {						
			if (record.get('status')=="Client Holiday Hours") 
				return 'holiday_billable';
			else if (record.get('status')=="Weekend Hours") 
				return 'weekend_billable';
			else if (record.get('status')=="Approved") 
				return 'approved';
			else if (record.get('status')=="Rejected") 
				return 'rejected';
		},

		listeners: {
			refresh: function(view) {
				if(this.store.getProxy().reader.jsonData != undefined) {
					Ext.getCmp('currentList').setValue(this.store.getProxy().reader.jsonData.TotalHours);
					Ext.getCmp('overAllList').setValue(this.store.getProxy().reader.jsonData.OverallTime);
				}
			}
		}
	},
	
	initComponent : function() {
		var me = this;

		var sm = Ext.create('Ext.selection.CheckboxModel',{
			checkOnly: true,
		});
		
		var config = {
			selModel : sm,
			columns:[],
			rowNumberer: false
		};
		
		// appy to this config
		Ext.apply(me, config);
        // apply to the initialConfig
        Ext.apply(me.initialConfig, config);

        this.tbar = [{
        	text : 'Reject',
        	id: "rejectButton",
        	icon: AM.app.globals.uiPath+'resources/images/cancel.png',
        	handler : function() {

        		var clientID =  Ext.getCmp('utilization_client').getValue(); 
        		if(clientID)
        		{
        			AM.app.getController('insight').onApprove(me, this.up('grid'), 2);
        		}
        		else
        		{
        			Ext.MessageBox.show({
        				title: 'Warning',
        				msg: "Select Client for Rejecting the utilization !!!",
        				icon: Ext.MessageBox.WARNING,
        				buttons: Ext.Msg.OK
        			});
        		}
        	}
        }, {
        	text : 'Approve',
        	id: "approveButton",
        	icon: AM.app.globals.uiPath+'resources/images/assign.png',
        	handler : function() {								
        		var clientID =  Ext.getCmp('utilization_client').getValue(); 
        		if(clientID)
        		{
        			AM.app.getController('insight').onApprove(me, this.up('grid'), 1);
        		}
        		else
        		{
        			Ext.MessageBox.show({
        				title: 'Warning',
        				msg: "Select Client for Approve the utilization !!!",
        				icon: Ext.MessageBox.WARNING,
        				buttons: Ext.Msg.OK
        			});
        		}
        	}
        }, {
        	xtype: "button",
        	id: 'download_all',
        	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
        	text : 'Export To Excel',
        	handler : function() {							
        		AM.app.getController('UtilizationDashboard').onDownload('all_data');
        	}
        }];

		//**** Pagination ****//
		this.bbar = [{
			xtype: 'pagingtoolbar',
			store: this.store,
			displayInfo: true,
			pageSize: AM.app.globals.itemsPerPage,
			displayMsg: 'Displaying {0} - {1} of {2}',
			emptyMsg: "No Records to display",
			plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
			listeners: {
				change: function (page, currentPage) { 
					// alert(Ext.getCmp('utilstatus').getValue());
					delete this.store.getProxy().extraParams;
					this.store.getProxy().extraParams = {
						'utilization_start_date':Ext.getCmp('utilization_start_date').getValue(),
						'utilization_end_date':Ext.getCmp('utilization_end_date').getValue(),
						'client_id':Ext.getCmp('utilization_client').getValue(),
						'wor_id': Ext.getCmp('utilization_wor_id').getValue(),
						'project_code':Ext.getCmp('util_project_code').getValue(),
						'EmployID':Ext.getCmp('IL_EmployID').getValue(),
						'utilstatus':Ext.getCmp('utilstatus').getValue(),
						'tab_type':'all_data',
						'wor_type_id':Ext.getCmp('wor_type').getValue(),
						'project_type_id':Ext.getCmp('project_type').getValue(),
						'supervisor':Ext.getCmp('supervisor').getValue()
					}
				},					  
			}
		},'->',{
			xtype : 'displayfield',
			id: 'overAllList',
			visible:false,
			value:''
		},'->',{
			xtype    : 'displayfield',
			id: 'currentList',
			visible:false,
			value:''
		}];
		//**** Pagination ****//
		
		this.callParent(arguments);
	},
	
	/**
    * When the store is loading then reconfigure the column model of the grid
    */
    storeLoad: function(store,me)
    {
        /**
        * JSON data returned from server has the column definitions
        */
        if(typeof(store.proxy.reader.jsonData.columns) === 'object') 
        {
        	var columns = [];
            /**
            * Assign new columns from the json data columns
            */
            Ext.each(store.proxy.reader.jsonData.columns, function(column){
            	//console.log(column);
            	columns.push(column);
            });
            console.log(store.proxy.reader.jsonData.data);

            /**
            * Reconfigure the column model of the grid
            */			
            me.reconfigure(store, columns);
        }
    }
});