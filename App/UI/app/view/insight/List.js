Ext.define('AM.view.insight.List',{
	extend : 'Ext.form.Panel',
	width : '99%',
	layout: 'fit',
	height: 720,
	
    initComponent : function() {

    	this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items: [{
				region:'west',
				xtype: 'insightTopPanel',
				width: '20%',
				frame: true,
				margin : '0 0 10px 0'
			}, {
				region:'center',
				xtype: 'tabpanel',
				activeTab: 0,
				width: '79%',
				height: 480,
				margin : '0 0 10px 5px',
				frame: true,
				items:[{
					title: 'All Data',
					items : [{								
						xtype:AM.app.getView('insight.GeneralList').create(),
						hidden:false
					}]
				}, {
					title: 'Weekend Coverage',
					items : [{								
						xtype:AM.app.getView('insight.WeekendList').create(),
						hidden:false
					}]
				}, {
					title: 'Holiday Coverage',
					items : [{								
						xtype:AM.app.getView('insight.ClientHolidaysList').create(),
						hidden:false
					}]
				}]
			}]
		}];
		
		this.callParent(arguments);
	},
});