Ext.define('AM.view.insight.TopPanel', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.insightTopPanel',
	layout  : 'fit',
	collapsible: true,
	collapsed: false,
	floatable: true,
	width : '100%',
	title   : 'Filters',
	
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
	},

	initComponent : function() {
		var me = this;
		
		var date = new Date(), 
		startMinDate = Ext.Date.add(date, Ext.Date.YEAR, -12), startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 1), 
		endMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
		
		var firstday = new Date(date.getFullYear(), date.getMonth(), 1);
		var today = new Date(date.getTime());
		
		this.items = [{
			xtype:'container',
			layout: {
				type: 'vbox',
			},
			items: [{
				xtype:'button',
				id: "ClearFilter",
				anchor: '5%',
				text:'Clear Filters',
				icon: AM.app.globals.uiPath+'resources/images/reset.png',
				style:{
					float:'left',
					'margin':'10px'
				},
				handler : function() {
					 AM.app.getController('insight').viewContent();
				}
			},{
				xtype : 'datefield',
				format: AM.app.globals.CommonDateControl,
				name : 'utilization_start_date',
				fieldLabel: 'Start Date <span style="color:red">*</span>',
				id: 'utilization_start_date',
				displayField: 'abbr',
				valueField: 'Start Date',
				labelWidth: 75,
				emptyText: 'Select Start Date',
				style:{
					'position':'absolute',
					'margin':'0px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				minValue: startMinDate,
				maxValue: startMaxDate,
				value: firstday,
				listeners: {
					select : function(combo) {
						me.query('datefield')[1].setMinValue(me.query('datefield')[0].getValue());
						var clientID =  me.query('combobox')[0].getValue();
						if(clientID)
						{
							me.utilDate = combo.getValue();
							this.passParams(me);
						}
					},
					scope : this
				}
			},{
				xtype : 'datefield',
				format: AM.app.globals.CommonDateControl,
				name : 'utilization_end_date',
				maxValue: endMaxDate,
				id: 'utilization_end_date',
				displayField: 'abbr',
				valueField: 'End Date',
				fieldLabel: 'End Date <span style="color:red">*</span>',
				labelWidth: 70,
				emptyText: 'Select End Date',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				value: today,
				listeners: {
					select : function(combo) {
						me.query('datefield')[0].setMaxValue(me.query('datefield')[1].getValue());
						var clientID =  me.query('combobox')[0].getValue();
						if(clientID)
						{	
							me.utilDate = combo.getValue();
							this.passParams(me);
						}
					},
					scope : this
				}
			},{
				//combobox 0
				xtype : 'combobox',
				multiSelect: false,
				//allowBlank: false,
				id: 'utilization_client',
				name: 'utilization_client',
				//typeAhead: true,
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('LogInViewerClients'),
				queryMode: 'remote',
				displayField: 'client_name',
				valueField: 'client_id',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				fieldLabel: 'Client <span style="color:red">*</span>',
				labelWidth: 50,
				emptyText: "Select Client",
				listeners: {
					afterrender: function(combo) {
						var recordSelected = combo.getStore().getAt(0);                     
						combo.setValue(recordSelected.get('field1'));
					},
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					},
					select: function(combo) {
						me.ClientId = combo.getValue();

						Ext.getCmp('utilization_wor_id').setValue('');
						Ext.getCmp('wor_type').setValue('');
						Ext.getCmp('project_type').setValue('');
						Ext.getCmp('util_project_code').setValue('');
						Ext.getCmp('supervisor').setValue('');
						Ext.getCmp('IL_EmployID').setValue('');

						this.passParams(me);

						//-Get Work Order- START -//
						Ext.getStore('workOrder').load({
							params:{'comboClient':combo.getValue(),'utilClientWor':1}
						});
						me.query('combobox')[1].enable();	
						me.query('combobox')[1].bindStore("workOrder");
						//-Get Work Order- END -//

						//-Get Work Order Types- START -//
						Ext.getStore('WorTypes').load({
							params:{'comboClient':combo.getValue()}
						});
						me.query('combobox')[2].enable();
						me.query('combobox')[2].bindStore("WorTypes");
						//-Get Work Order Types- END -//

						//-Get Project Types- START -//
						Ext.getStore('ProjectTypes').load({
							params:{'hasNoLimit':"1",'comboClient':combo.getValue()}
						});
						me.query('combobox')[3].enable();
						me.query('combobox')[3].bindStore("ProjectTypes");
						//-Get Project Types- END -//

						//-Get Role Code- START -//
						Ext.getStore('filterRoleCode').load({
							params:{'comboClient':combo.getValue(),'filter_rolecode':'1'}
						});
						me.query('combobox')[4].enable();	
						me.query('combobox')[4].bindStore("filterRoleCode");
						//-Get Role Code- END -//
						
						//-Get Supervisor- START -//
						Ext.getStore('Supervisor').load({
							params:{'comboClient':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[5].enable();
						me.query('combobox')[5].bindStore("Supervisor");
						//-Get Supervisor- END -//
						
						//-Get Employees- START -//
						Ext.getStore('UtilEmployees').load({
							params:{'util_emp':"1",'comboClient':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[6].enable();	
						me.query('combobox')[6].bindStore("UtilEmployees");
						//-Get Employees- END -//

						me.query('combobox')[7].enable();	
					},
					change:function(combo){																											 
						if(me.getEl() != undefined)
						{										 							
							me.ClientId = combo.getValue();
							
							if (combo.getValue() == null || combo.getValue() == "")
							{						
								Ext.getCmp('utilization_wor_id').setValue('');
								Ext.getCmp('wor_type').setValue('');
								Ext.getCmp('project_type').setValue('');
								Ext.getCmp('util_project_code').setValue('');
								Ext.getCmp('supervisor').setValue('');
								Ext.getCmp('IL_EmployID').setValue('');

								this.passParams(me);

								//-Get Work Order- START -//
								Ext.getStore('workOrder').load({
									params:{'comboClient':combo.getValue(),'utilClientWor':1}
								});
								me.query('combobox')[1].enable();	
								me.query('combobox')[1].bindStore("workOrder");
								//-Get Work Order- END -//

								//-Get Work Order Types- START -//
								Ext.getStore('WorTypes').load({
									params:{'comboClient':combo.getValue()}
								});
								me.query('combobox')[2].enable();
								me.query('combobox')[2].bindStore("WorTypes");
								//-Get Work Order Types- END -//

								//-Get Project Types- START -//
								Ext.getStore('ProjectTypes').load({
									params:{'hasNoLimit':"1",'comboClient':combo.getValue()}
								});
								me.query('combobox')[3].enable();
								me.query('combobox')[3].bindStore("ProjectTypes");
								//-Get Project Types- END -//

								//-Get Role Code- START -//
								Ext.getStore('filterRoleCode').load({
									params:{'comboClient':combo.getValue(),'filter_rolecode':'1'}
								});
								me.query('combobox')[4].enable();	
								me.query('combobox')[4].bindStore("filterRoleCode");
								//-Get Role Code- END -//
								
								//-Get Supervisor- START -//
								Ext.getStore('Supervisor').load({
									params:{'comboClient':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[5].enable();
								me.query('combobox')[5].bindStore("Supervisor");
								//-Get Supervisor- END -//
								
								//-Get Employees- START -//
								Ext.getStore('UtilEmployees').load({
									params:{'util_emp':"1",'comboClient':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[6].enable();	
								me.query('combobox')[6].bindStore("UtilEmployees");
								//-Get Employees- END 
								me.query('combobox')[7].enable();
							}

						}
					},
					scope :this
				}
			},{
				//combobox 1
				xtype : 'combobox',
				fieldLabel: 'Work Order',
				multiSelect: false,
				id: 'utilization_wor_id',
				name: 'utilization_wor_id',
				typeAhead: true,
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('workOrder'),
				queryMode: 'remote',
				displayField: 'wor_name',
				valueField: 'wor_id',
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				labelWidth: 50,
				emptyText: 'Select Work Order',
				disabled: true,
				listeners:{
					beforequery	: function(queryEvent) {
						queryEvent.combo.getStore().getProxy().extraParams = {
							'comboClient' 	:me.query('combobox')[0].getValue(),
							'utilClientWor':1,
							'hasNoLimit':'1', 
							'filterName' : queryEvent.combo.displayField
						};
					},
					select : function(combo) {
						
						Ext.getCmp('wor_type').setValue('');
						Ext.getCmp('project_type').setValue('');
						Ext.getCmp('util_project_code').setValue('');
						Ext.getCmp('supervisor').setValue('');
						Ext.getCmp('IL_EmployID').setValue('');

						this.passParams(me);

						Ext.getStore('WorTypes').load({
							params:{'wor_id':combo.getValue(),'comboClient':me.query('combobox')[0].getValue()}
						});
						me.query('combobox')[2].bindStore("WorTypes");


						Ext.getStore('ProjectTypes').load({
							params:{'hasNoLimit':"1",'wor_id':combo.getValue(),'comboClient':me.query('combobox')[0].getValue()}
						});
						me.query('combobox')[3].bindStore("ProjectTypes");


						Ext.getStore('filterRoleCode').load({
							params:{'comboClient' 	:me.query('combobox')[0].getValue(),'wor_id':combo.getValue(),'filter_rolecode':'1'}
						});
						me.query('combobox')[4].bindStore("filterRoleCode");


						Ext.getStore('Supervisor').load({
							params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[5].bindStore("Supervisor");

						Ext.getStore('UtilEmployees').load({
							params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[6].enable();	
						me.query('combobox')[6].bindStore("UtilEmployees");

					},
					change:function(combo){																															 
						if(me.getEl() != undefined)
						{										 														
							if (combo.getValue() == null || combo.getValue() == "")
							{	

								Ext.getCmp('wor_type').setValue('');
								Ext.getCmp('project_type').setValue('');
								Ext.getCmp('util_project_code').setValue('');
								Ext.getCmp('supervisor').setValue('');
								Ext.getCmp('IL_EmployID').setValue('');

								this.passParams(me);

								me.query('combobox')[2].bindStore("WorTypes");
								Ext.getStore('WorTypes').load({
									params:{'wor_id':combo.getValue(),'comboClient':me.query('combobox')[0].getValue()}
								});


								Ext.getStore('ProjectTypes').load({
									params:{'hasNoLimit':"1",'wor_id':combo.getValue(),'comboClient':me.query('combobox')[0].getValue()}
								});
								me.query('combobox')[3].bindStore("ProjectTypes");


								Ext.getStore('filterRoleCode').load({
									params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'project_type_id':me.query('combobox')[3].getValue(),'filter_rolecode':'1',}
								});
								me.query('combobox')[4].bindStore("filterRoleCode");


								me.query('combobox')[5].bindStore("Supervisor");
								Ext.getStore('Supervisor').load({
									params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});

								Ext.getStore('UtilEmployees').load({
									params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[6].enable();	
								me.query('combobox')[6].bindStore("UtilEmployees");
							}						
						}
					},
					scope : this
				}
				

			},{
				//combobox 2
				xtype : 'combobox',
				fieldLabel: 'Work Order Type',
				multiSelect: false,
				id: 'wor_type',
				name: 'wor_type_id',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('WorTypes'),
				queryMode: 'remote',
				displayField: 'wor_type',
				valueField: 'wor_type_id',
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				labelWidth: 50,
				disabled: true,
				emptyText: "Select Work Order Type",
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.getStore().getProxy().extraParams = {
							'hasNoLimit':'1', 
							'comboClient':me.query('combobox')[0].getValue(),
							'wor_id':me.query('combobox')[1].getValue(),
							'filterName' : queryEvent.combo.displayField
						};
					},
					select: function(combo) {
						Ext.getCmp('util_project_code').setValue('');
						Ext.getCmp('supervisor').setValue('');
						Ext.getCmp('IL_EmployID').setValue('');

						Ext.getStore('filterRoleCode').load({
							params:{ 'comboClient' 	:me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'project_type_id':me.query('combobox')[3].getValue(),'filter_rolecode':'1',}
						});
						me.query('combobox')[4].bindStore("filterRoleCode");

						Ext.getStore('Supervisor').load({
							params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[5].bindStore("Supervisor");

						Ext.getStore('UtilEmployees').load({
							params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[6].enable();	
						me.query('combobox')[6].bindStore("UtilEmployees");
						this.passParams(me);
					},
					change:function(combo){																															 
						if(me.getEl() != undefined)
						{										 														
							if (combo.getValue() == null || combo.getValue() == "")
							{	
								Ext.getCmp('util_project_code').setValue('');
								Ext.getCmp('supervisor').setValue('');
								Ext.getCmp('IL_EmployID').setValue('');

								Ext.getStore('filterRoleCode').load({
									params:{ 'comboClient' 	:me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'project_type_id':me.query('combobox')[3].getValue(),'filter_rolecode':'1',}
								});
								me.query('combobox')[4].bindStore("filterRoleCode");

								Ext.getStore('Supervisor').load({
									params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[5].bindStore("Supervisor");

								Ext.getStore('UtilEmployees').load({
									params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[6].enable();	
								me.query('combobox')[6].bindStore("UtilEmployees");
								this.passParams(me);
							}
						}
					},
					scope : this
				}
			},{
				//combobox 3
				xtype : 'combobox',
				fieldLabel: 'Project Type',
				multiSelect: false,
				id: 'project_type',
				name: 'project_type_id',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('ProjectTypes'),
				queryMode: 'remote',
				displayField: 'project_type',
				valueField: 'project_type_id',
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				labelWidth: 50,
				emptyText: "Select Project",
				disabled: true,
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.getStore().getProxy().extraParams = {
							'hasNoLimit':'1', 
							'comboClient':me.query('combobox')[0].getValue(),
							'wor_id':me.query('combobox')[1].getValue(),
							'filterName' : queryEvent.combo.displayField
						};
					},
					select: function(combo) {
						Ext.getCmp('util_project_code').setValue('');
						Ext.getCmp('supervisor').setValue('');
						Ext.getCmp('IL_EmployID').setValue('');

						Ext.getStore('filterRoleCode').load({
							params:{ 'comboClient' 	:me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':combo.getValue(),'filter_rolecode':'1',}
						});
						me.query('combobox')[4].bindStore("filterRoleCode");

						Ext.getStore('Supervisor').load({
							params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[5].bindStore("Supervisor");

						Ext.getStore('UtilEmployees').load({
							params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[6].enable();	
						me.query('combobox')[6].bindStore("UtilEmployees");

						this.passParams(me);

					},
					change:function(combo){
						if(me.getEl() != undefined)
						{										 														
							if (combo.getValue() == null || combo.getValue() == "")
							{
								Ext.getCmp('util_project_code').setValue('');
								Ext.getCmp('supervisor').setValue('');
								Ext.getCmp('IL_EmployID').setValue('');

								Ext.getStore('filterRoleCode').load({
									params:{ 'comboClient' 	:me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':combo.getValue(),'filter_rolecode':'1',}
								});
								me.query('combobox')[4].bindStore("filterRoleCode");

								Ext.getStore('Supervisor').load({
									params:{'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[5].bindStore("Supervisor");

								Ext.getStore('UtilEmployees').load({
									params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[6].enable();	
								me.query('combobox')[6].bindStore("UtilEmployees");
								this.passParams(me);
							}
						}

					},
					scope : this
				}
			},{
				//combobox 4
				xtype : 'combobox',
				fieldLabel: 'Role Code',
				multiSelect: false,
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				labelAlign: 'top',
				store: Ext.getStore('filterRoleCode'),
				queryMode: 'remote',
				minChars:0,
				forceSelection: true,
				name: 'project_code',
				id: 'util_project_code',
				displayField: 'project_code',
				valueField: 'process_id',
				width:230,
				labelWidth: 80,
				disabled: true,
				emptyText: 'Select Role Code',
				listeners		: {
					beforequery	: function(queryEvent) {
						AM.app.getStore('filterRoleCode').getProxy().extraParams = {
							'comboClient' 	:me.query('combobox')[0].getValue(),
							'wor_id':me.query('combobox')[1].getValue(),
							'wor_type_id':me.query('combobox')[2].getValue(),
							'project_type_id':me.query('combobox')[3].getValue(),
							'filter_rolecode':'1',
						};
					},
					select: function(combo) {
						Ext.getCmp('supervisor').setValue('');
						Ext.getCmp('IL_EmployID').setValue('');

						Ext.getStore('UtilEmployees').load({
							params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':me.query('combobox')[3].getValue(),'role_code':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[6].enable();	
						me.query('combobox')[6].bindStore("UtilEmployees");
						this.passParams(me);
					},
					change:function(combo){																															 
						if(me.getEl() != undefined)
						{
							if (combo.getValue() == null || combo.getValue() == "")
							{														
								Ext.getCmp('supervisor').setValue('');
								Ext.getCmp('IL_EmployID').setValue('');
								

								Ext.getStore('UtilEmployees').load({
									params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':me.query('combobox')[3].getValue(),'role_code':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
								});
								me.query('combobox')[6].enable();	
								me.query('combobox')[6].bindStore("UtilEmployees");
								this.passParams(me);
							}
						}
					},
					scope : this
				}
			},{
				//combobox 5
				xtype : 'combobox',
				fieldLabel: 'Supervisor',
				multiSelect: false,
				id: 'supervisor',
				name: 'supervisor',
				minChars: 0,
				forceSelection: true,
				store: Ext.getStore('Supervisor'),
				queryMode: 'remote',
				displayField: 'supervisor_name',
				valueField: 'supervisor_id',
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				labelWidth: 50,
				emptyText: "Select Supervisor",
				disabled: true,
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':me.query('combobox')[0].getValue(),
							'wor_id':me.query('combobox')[1].getValue(),
							'wor_type_id':me.query('combobox')[2].getValue(),
							'filterName' : queryEvent.combo.displayField,
							'start_date' : me.query('datefield')[0].getValue(), 
							'end_date' : me.query('datefield')[1].getValue()
						}
					},
					select: function(combo) {
						Ext.getStore('UtilEmployees').load({
							params:{'util_emp':"1",'comboClient':me.query('combobox')[0].getValue(),'wor_id':me.query('combobox')[1].getValue(),'wor_type_id':me.query('combobox')[2].getValue(),'project_type_id':me.query('combobox')[3].getValue(),'role_code':me.query('combobox')[4].getValue(),'supervisor':combo.getValue(),'start_date' : me.query('datefield')[0].getValue(),'end_date' : me.query('datefield')[1].getValue()}
						});
						me.query('combobox')[6].enable();	
						me.query('combobox')[6].bindStore("UtilEmployees");
						this.passParams(me);	
					},
					change:function(combo){
						if(me.getEl() != undefined)
						{										 														
							if (combo.getValue() == null || combo.getValue() == "")
							{						
								this.passParams(me);							
							}						
						}	
					},
					scope : this
				}
			},{
				//combobox 6
				xtype : 'boxselect',
				fieldLabel: 'Employees',
				multiSelect: false,
				id: 'IL_EmployID',
				name: 'EmployID',
				minChars: 1,
				forceSelection: true,
				store: 'UtilEmployees',
				queryMode: 'remote',
				displayField: 'full_name', 
				valueField: 'employee_id',
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				labelAlign: 'top',
				width:230,
				labelWidth: 100,
				emptyText: 'Select Employee Name',
				disabled: true,
				listeners:{
					beforequery: function(queryEvent){
						queryEvent.combo.getStore().getProxy().extraParams = {
							'comboClient':me.query('combobox')[0].getValue(),
							'wor_id':me.query('combobox')[1].getValue(),
							'wor_type_id':me.query('combobox')[2].getValue(),
							'project_type_id':me.query('combobox')[3].getValue(),
							'role_code':me.query('combobox')[4].getValue(),
							'filterName' : queryEvent.combo.displayField,
							'start_date' : me.query('datefield')[0].getValue(), 
							'end_date' : me.query('datefield')[1].getValue()
						}
					},
					select :function (combo){
						this.passParams(me);
					},
					change:function(combo){																															 
						if(me.getEl() != undefined)
						{										 														
							if (combo.getValue() == null || combo.getValue() == "")
							{						
								this.passParams(me);							
							}						
						}
					},
					scope : this
				}
			},{
				//combobox 7
				xtype : 'combobox',
				fieldLabel: 'Status',
				style:{
					'position':'absolute',
					'margin':'20px 0 0 5px'
				},
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Status'],
					data: [
					{'Flag': 'All', 'Status': 'All'},
					{'Flag': '0', 'Status': 'Pending'},
					{'Flag': '1', 'Status': 'Approved'},
					{'Flag': '2', 'Status': 'Rejected'},
					{'Flag': '3', 'Status': 'Approved Billable Hours'},
					]
				}),
				labelAlign: 'top',
				width:230,
				emptyText: 'Status',
				labelWidth: 100,
				id: 'utilstatus',
				name: 'utilstatus',
				forceSelection: true,
				mode: 'local',
				minChars: 1,
				displayField: 'Status', 
				// disabled: true,
				valueField: 'Flag',
				value:'0',
				listeners:{
					select :function (combo){
						var warnMsg="";
						if(me.query('combobox')[0].getValue()==null || me.query('combobox')[0].getValue()=="")
						{
							Ext.MessageBox.show({
								title: "Warning",
								msg: "Please Select Client",
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.Msg.OK,
								closable: false,
							});
						}
						else if(me.query('datefield')[0].getValue()==null || me.query('datefield')[0].getValue()=="")
						{
							Ext.MessageBox.show({
								title: "Warning",
								msg: "Please Select Start Date",
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.Msg.OK,
								closable: false,
							});
						}
						else if(me.query('datefield')[1].getValue()==null || me.query('datefield')[1].getValue()=="")
						{
							Ext.MessageBox.show({
								title: "Warning",
								msg: "Please Select End Date",
								icon: Ext.MessageBox.WARNING,
								buttons: Ext.Msg.OK,
								closable: false,
							});
						}
						else
						{
							AM.app.getController('insight').listAllDataUtilRecords(Ext.getCmp('InsightGridID'), me.query('datefield')[0].getValue(), me.query('datefield')[1].getValue(), me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue(), me.query('combobox')[4].getValue(),me.query('combobox')[6].getValue(),me.query('combobox')[7].getValue(),'all_data',me.query('combobox')[2].getValue(),me.query('combobox')[3].getValue(),me.query('combobox')[5].getValue());
							var weekendBillable = me.query('combobox')[6].getValue();
							var holidayBillable = me.query('combobox')[6].getValue();
							if(me.query('combobox')[6].getValue()==3)
							{
								//weekendBillable =3;
								holidayBillable =4;
							}
							
							AM.app.getController('insight').listHolidayUtilRecords(Ext.getCmp('holidaylistGridID'), me.query('datefield')[0].getValue(), me.query('datefield')[1].getValue(), me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue(), me.query('combobox')[4].getValue(),holidayBillable,me.query('combobox')[7].getValue(),'holiday_coverage',me.query('combobox')[2].getValue(),me.query('combobox')[3].getValue(),me.query('combobox')[5].getValue());
							AM.app.getController('insight').listWeekendRecords(Ext.getCmp('weekendlistGridID'), me.query('datefield')[0].getValue(), me.query('datefield')[1].getValue(), me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue(), me.query('combobox')[4].getValue() ,weekendBillable,me.query('combobox')[7].getValue(),'weekend_coverage',me.query('combobox')[2].getValue(),me.query('combobox')[3].getValue(),me.query('combobox')[5].getValue());

						}
					},
				}
			}]
		}];

		this.callParent(arguments);
		
	},

	passParams : function (me){
		AM.app.getController('insight').listAllDataUtilRecords(Ext.getCmp('InsightGridID'), me.query('datefield')[0].getValue(), me.query('datefield')[1].getValue(), me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue(), me.query('combobox')[4].getValue(),me.query('combobox')[6].getValue(),me.query('combobox')[7].getValue(),'all_data',me.query('combobox')[2].getValue(),me.query('combobox')[3].getValue(),me.query('combobox')[5].getValue());
		AM.app.getController('insight').listHolidayUtilRecords(Ext.getCmp('holidaylistGridID'), me.query('datefield')[0].getValue(), me.query('datefield')[1].getValue(), me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue(), me.query('combobox')[4].getValue(),me.query('combobox')[6].getValue(),'All','holiday_coverage',me.query('combobox')[2].getValue(),me.query('combobox')[3].getValue(),me.query('combobox')[5].getValue());
		AM.app.getController('insight').listWeekendRecords(Ext.getCmp('weekendlistGridID'), me.query('datefield')[0].getValue(), me.query('datefield')[1].getValue(), me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue(), me.query('combobox')[4].getValue() ,me.query('combobox')[6].getValue(),'All','weekend_coverage',me.query('combobox')[2].getValue(),me.query('combobox')[3].getValue(),me.query('combobox')[5].getValue());
	},

});
