Ext.define('AM.view.Holidays.AddEdit',{
	extend : 'Ext.window.Window',
	alias : 'widget.holidayAddEdit',
	title: 'Add Client Holiday',
	layout : 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'holidayFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'holiday_id',
				name : 'holiday_id',
			}, {
				xtype : 'textfield',
				name : 'name',
				allowBlank: false,
				fieldLabel : 'Holiday Name <span style="color:red">*</span>',
				anchor: '100%',
				maxLength : 50,
				maskRe: /[A-Za-z0-9-!@#\$%\^\&*\)\(+=._-{6,}$/ ]/
			}, {
				xtype : 'datefield',
				name : 'date',
				editable: false,
				maskRe: /[0-9\/]/,
				allowBlank: false,
				format: AM.app.globals.CommonDateControl,
				fieldLabel : 'Date <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 500,
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];

		this.callParent(arguments);
	},
	
	onCancel : function() {
		this.up('window').close();
	},
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()) 
		{
        	AM.app.getController('Holidays').onSaveHoliday(form, win);
        }
	},
	
});