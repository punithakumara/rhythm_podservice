Ext.define('AM.view.Holidays.List',{
	extend : 'Ext.grid.Panel',
	title: 'Client Holidays List',
	store: Ext.getStore('Holidays'),
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
		
		this.store  = Ext.getStore("Holidays");
		
    	this.columns = [ {
			header : 'Name', 
			dataIndex : 'name', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1,
		}, {
			header : 'Date',
			dataIndex : 'date',
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1,
		}, {
			header : 'Added By', 
			dataIndex : 'added_by', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1
		}, {
			header : 'Action', 
			width : '8%',
			xtype : 'actioncolumn',
            sortable: false,
            menuDisabled:true,
            draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
			} ,'-', {
				icon : AM.app.globals.uiPath+'/resources/images/delete.png',
				tooltip : 'Delete',
				handler : this.onDelete
			}]
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No holidays to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

		this.callParent(arguments);
	},
	
	tools : [{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('Holidays').onCreateHoliday();
	    },
		text : 'Add Client Holiday'
	}],
	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Holidays').onEditHoliday(grid, rowIndex, colIndex);
	},
	
	onDelete : function(grid, rowIndex, colIndex, me) {
		AM.app.getController('Holidays').onDeleteHoliday(grid, rowIndex, colIndex, me);
	},
	
});