Ext.define('AM.view.ProjectTypes.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.ProjectTypes',
	title: 'Projects',
	store: Ext.getStore('ProjectTypes'),
	layout : 'fit',
	width : '99%',
	minHeight : 200,
	loadMask: true,
	disableSelection: false,
	border : true,
	
	initComponent : function() {
		
		this.store  = Ext.getStore("ProjectTypes");
		
		this.columns = [ {
			header : 'WOR Type', 
			dataIndex : 'wor_type', 
			menuDisabled:true,
			draggable: false,
			flex: 1,
		},{
			header : 'Project Name', 
			dataIndex : 'project_type', 
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Description',
			dataIndex : 'project_description',
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : '# of Roles', 
			dataIndex : 'total_roles', 
			menuDisabled:true,
			draggable: false,
			flex: 1
		}];
	

		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Projects to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

		this.callParent(arguments);
	},

	tools : [{
		xtype : 'trigger',
		itemId : 'gridTrigger',
		fieldLabel : '',
		labelWidth : 60,
		labelCls : 'searchLabel',
		triggerCls : 'x-form-clear-trigger',
		emptyText : 'Search',
		width : 250,
		minChars : 1,
		enableKeyEvents : true,
		onTriggerClick : function(){
			this.reset();
			this.fireEvent('triggerClear');
		}
	}]
	
	
});