Ext.define('AM.view.RoleTypes.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.RoleTypes',
	title: 'Roles',
	store: Ext.getStore('RoleTypes'),
	layout : 'fit',
	width : '99%',
	minHeight : 200,
	loadMask: true,
	disableSelection: false,
	border : true,
	
	initComponent : function() {
		
		this.store  = Ext.getStore("RoleTypes");

		this.columns = [{
			header : 'Project Name', 
			dataIndex : 'project_type',
			menuDisabled:true,
			draggable: false,
			flex: 1,
		},{
			header : 'Role Name',
			dataIndex : 'role_type',
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Description',
			dataIndex : 'role_description',
			menuDisabled:true,
			draggable: false,
			flex: 1,
		} ];

		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Roles to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

		this.callParent(arguments);
	},

	tools : [{
		xtype : 'trigger',
		itemId : 'gridTrigger',
		fieldLabel : '',
        id : 'gridTrigger',
		labelWidth : 60,
		labelCls : 'searchLabel',
		triggerCls : 'x-form-clear-trigger',
		emptyText : 'Search',
		width : 250,
		minChars : 1,
		enableKeyEvents : true,
		onTriggerClick : function(){
			this.reset();
			this.fireEvent('triggerClear');
		}
	}]
	
});