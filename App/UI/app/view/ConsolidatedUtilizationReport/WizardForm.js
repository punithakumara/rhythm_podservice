Ext.define('AM.view.ConsolidatedUtilizationReport.WizardForm', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.wizardCon',
	width : '99%',
	minHeight : 500,
	border : false,
	layout  : 'fit',
	itemId  : 'wizardFormCon',
	
	//requires:['ConsolidatedUtilizationReport.GridPanel'],
	initComponent : function() {
		this.items = [{
			xtype:'container',
			"layout":{
			   "type": "vbox",
			   "align": "stretch"
			},
			"width": "100%",
			"height":"100%",
			items:[{
				xtype: 'TopPanelCon',
				margin:'0 0 10 0'
			},{
				xtype:AM.app.getView('ConsolidatedUtilizationReport.GridPanel').create()	
			}]
		}];
		
		this.callParent(arguments);
	}
});
