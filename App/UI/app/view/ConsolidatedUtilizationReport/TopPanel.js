Ext.define('AM.view.ConsolidatedUtilizationReport.TopPanel', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.TopPanelCon',
	layout  : 'fit',
	collapsible: true,
	collapsed: true,
	floatable: true,
	width : '99%',
	id:'TopPanelCon',
	//border : true,
	title   : 'Filter Reports',
	// cls:'TopPanelCon',
	// itemId  : 'TopPanelCon',
	//defaults: { anchor: '30%', msgTarget: 'side', margin: '6 0 2 6'},
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
  
	initComponent : function() {
  		var me = this;
		var dataStore = new Ext.data.SimpleStore({
			fields: ['monthValue', 'monthRange'],
			data: [['0', 'Daily'],['1', 'Weekly'],['2', 'Monthly']]
		});
		var utilStore = new Ext.data.SimpleStore({
			fields: ['utilValue', 'utilRange'],
			data: [['0', 'All'],['1', 'Less than'],['2', 'Greater than']]
		});
		var labelWidth = 300;
		var enddatewidth = 300;
		if(Ext.util.Cookies.get("grade") > 4){
			labelWidth = 250;
			enddatewidth = 350;
		}
		this.items = [{
			xtype:'container',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},		
			width: 800,
			heigth:500,
			style:{
	        	'margin-left':'20px'
	        },
			items:[{
			    xtype:'container',
				layout: {
				    type: 'hbox',
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',
					}
				},
				width: 800,
				heigth:500,
				items: [{
					xtype : 'boxselect',
					style:{
						'position':'absolute',
						'margin-right':'40px',
						'margin-top':'10px'
					},
					width:350,
					multiSelect: false,
					fieldLabel: 'Department',
					name: 'VerticalID',
					id :"Parentvrtclconsutili",			        			
					store           : Ext.create('AM.store.ParentVertical'),			        			
					displayField    : 'pVerticalName',
					width           : 250,			        	        
					valueField      : 'Id',
					queryMode: 'remote',
					minChars:0,			        		
					emptyText: 'Select Department',
					listeners: {
						change: function(obj){
							if(obj.getValue()==null ){
								if(Ext.getCmp('ConUtilmanagerID') == undefined )
									console.log();
								else	
									Ext.getCmp('ConUtilmanagerID').enable();
							}
							else
							{
								if(Ext.getCmp('ConUtilAMID')!=undefined)
								{		
									Ext.getCmp('ConUtilAMID').reset();
									Ext.getCmp('ConUtilTLId').reset();
									Ext.getCmp('ConUtilmanagerID').reset();
									Ext.getCmp('ConUtilClientID').reset();
									
								}		
							}
							Ext.getStore('Vertical').load({
								params:{ 'pvertical': obj.getValue()}
							});
							obj.next().bindStore('Vertical');		
						},
					}
				},{
					xtype : 'boxselect',
					style:{
						'position':'absolute',
						'margin-right':'40px',
						'margin-top':'10px'
					},
					width:300,
					multiSelect: true,
					fieldLabel: 'Vertical',
					name: 'VerticalID',
					id :"ConUtilVerticalID",
					store: 'Vertical',
					queryMode: 'remote',
					minChars:0,
					displayField: 'name', 
					valueField: 'vertical_id',
					emptyText: 'All Verticals',
					listeners: {
						beforedeselect: function(combo, record, index, eOpts) {
							Ext.getCmp('ConUtilmanagerID').enable();
						
						},
						select: function(obj){
							if(Ext.getCmp('ConUtilAMID')!=undefined)
							{	
								Ext.getCmp('ConUtilAMID').reset();
								Ext.getCmp('ConUtilTLId').reset();
								Ext.getCmp('ConUtilmanagerID').reset();
								Ext.getCmp('ConUtilClientID').reset();
								
								
								var AMStore = Ext.getCmp('ConUtilAMID').store;
								AMStore.proxy.extraParams = {'VerticalID': obj.getValue()};
								
								var StoreAM =	Ext.getStore('Associate_manager');
								StoreAM.proxy.extraParams = {'VerticalID': obj.getValue()};
								
								AMStore.load();
								StoreAM.load();
								Ext.getCmp('ConUtilmanagerID').disable();
								if(obj.getValue().length > 1){
									Ext.getCmp('ConUtilClientID').disable();
									Ext.getCmp('ConUtilAMID').disable();
									
								}
								else{
									Ext.getCmp('ConUtilClientID').enable();
									Ext.getCmp('ConUtilAMID').enable();
									
								}
							}
						},
					
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'pvertical':Ext.getCmp('Parentvrtclconsutili').getValue(),'hasNoLimit':'1','comboVertical':'1','filterName' : queryEvent.combo.displayField};
						}
					}
				},{
					xtype : 'boxselect',
					style:{
						'position':'absolute',
						'margin-top':'10px'
					},
					width:300,
					multiSelect: false,
					fieldLabel: 'Client',
					name: 'ClientID',
					store: 'Clients',
					id :'ConUtilClientID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'client_name',
					valueField: 'client_id',
					// allowBlank: false,
					emptyText: 'All Clients',
					listeners: {
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							var VerticalID = queryEvent.combo.prev().getValue() ? queryEvent.combo.prev().getValue() : '';
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1', 'VerticalID': VerticalID, 'filterName' : queryEvent.combo.displayField, 'comboClient':'1'};
						},
				  
						select: function(combo){							
							//Ext.getCmp('ConUtilProcessID').enable();
							var VerticalID = combo.prev().getValue() ? combo.prev().getValue() : '';
							/*Ext.getStore('Process').load({
								params:{'VerticalID': VerticalID, ClientID: combo.getValue()}
							});
							combo.next().bindStore('Process');	*/				
						}
					}
				}/*,{
					xtype: 'boxselect',
					width:285,
					style:{
						'position':'absolute',
						'margin-left':'40px',
						'margin-top':'10px'
					},
					fieldLabel: 'Process',
					id: 'ConUtilProcessID',
					name: 'ProcessID',
					multiSelect: false,
					minChars:0,
					displayField: 'Name', 
					valueField: 'processID',
					disabled: true,
					emptyText: 'All Process',
					listeners: {						
						beforequery: function(queryEvent){
							Ext.Ajax.abortAll();
							ClientID = queryEvent.combo.prev().getValue();
							var VerticalID = Ext.getCmp('ConUtilVerticalID').getValue() ? Ext.getCmp('ConUtilVerticalID').getValue() : '';
							queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','comboProcess': '1','VerticalID': VerticalID,'ClientID': ClientID,'conUtil': '1', 'filterName' : queryEvent.combo.displayField};
						}
					}
				}*/]
			},{
				xtype:'container',
				layout: {
					type: 'hbox',
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',
					}
				},
				width: 800,
				heigth:500,
			},{
				xtype:'container',
				layout: {
					type: 'hbox',
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',
					}
				},
				width: 800,
				heigth:500,
				items: [{
					xtype: 'boxselect',
					width: labelWidth,
					style:{
						'position':'absolute',
						'margin-top':'10px'
					},
					fieldLabel: 'Shifts',
					name: 'ShiftID',
					store: 'Shifts',
					id  :'ConUtilShiftID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'shift_code', 
					valueField: 'shift_id',
					multiSelect: false,
					emptyText: 'Select Shift',
				},{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-top':'10px'
					},
					fieldLabel: 'Manager',
					name: 'EmployID',
					store: 'Managers',
					id : 'ConUtilmanagerID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'employee_id',
					multiSelect: false,
					emptyText: 'Select Manager',
					listeners: {
						change: function(obj){
							if(Ext.getCmp('ConUtilAMID')!=undefined)
							{		
								if(obj.getValue()==null){
									if(Ext.getCmp('ConUtilmanagerID') == undefined )
										console.log();
									else
									{console.log('2elseeeeeeeIF');
										Ext.getCmp('Parentvrtclconsutili').enable();
										Ext.getCmp('ConUtilVerticalID').enable();
										Ext.getCmp('ConUtilClientID').enable();
									}
								}
								else
								{
									Ext.getCmp('ConUtilAMID').reset();
									Ext.getCmp('ConUtilTLId').reset();
									var AMStore = obj.next().store;
									AMStore.proxy.extraParams = {'EmployID': obj.getValue()};
									AMStore.load();
									Ext.getCmp('Parentvrtclconsutili').disable();
									Ext.getCmp('ConUtilVerticalID').disable();
									Ext.getCmp('ConUtilClientID').disable();
									
								}
							}			
						}					 
					}
				},/*{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-top':'10px'
					},
					fieldLabel: 'Associate Manager',
					name: 'EmployID',
					store: Ext.getStore('Associate_manager'),
					id: 'ConUtilAMID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'employee_id',
					multiSelect: false,
					emptyText: 'Associate Manager',
				},*/
				{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-top':'10px'
					},
					fieldLabel: 'Associate Manager',
					name: 'EmployID',
					store: Ext.getStore('Associate_manager'),
					id: 'ConUtilAMID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'employee_id',
					multiSelect: false,
					emptyText: 'Associate Manager',
				},{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',				        										        				
						'margin-top':'10px',
						'margin-left':'40px'
					},
					fieldLabel: 'Team Lead',
					name: 'EmployID',
					store: 'Leads',
					id:'ConUtilTLId',
					queryMode: 'remote',
					minChars:0,
					displayField: 'Name', 
					valueField: 'employee_id',
					multiSelect: false,
					emptyText: 'Team Lead',
					listeners: {
						beforequery : function(queryEvent) {
							Ext.Ajax.abortAll();
							queryEvent.combo.getStore().getProxy().extraParams = {'EmplyID':Ext.util.Cookies.get("empid")};
						}
					}
				},{
					xtype: 'boxselect',
					width:300,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-top':'10px'
					},
					fieldLabel: 'Resources',
					name: 'EmployID',
					store: 'TlEmployee',
					id: 'ConUtilEmployID',
					queryMode: 'remote',
					minChars:0,
					displayField: 'FirstName', 
					valueField: 'employee_id',
					multiSelect: false,
					emptyText: 'Select Resources',					
				}]
			},{
				xtype:'container',
				layout: {
					type: 'hbox',
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',
					}
				},
				width: 800,
				heigth:500,
			},{
				xtype:'container',
				layout: {
					type: 'hbox',
				},
				defaults: {
					labelWidth: 80,
					style: {
						'padding': '20px',					   
					}
				},
				width: 800,
				heigth:500,
				items: [{
					xtype: 'datefield',
					width:labelWidth,
					style:{
						'position':'absolute',				        					
						'margin-bottom':'10px',
						'margin-top':'10px'
					},
					fieldLabel: 'StartDate',
					format: AM.app.globals.CommonDateControl,
					name: 'reportSstartDate',
					id: 'ConUtilreportSstartDate',
					value : Ext.Date.getFirstDateOfMonth(new Date()),
					hidden: false,
					listeners: {
						'focus': function(me) {
							this.onTriggerClick();
						}
					}
				},{
					xtype: 'datefield',
					//width:enddatewidth,
					width:300,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-bottom':'10px',
						'margin-top':'10px'
					},
					fieldLabel: 'EndDate',
					format: AM.app.globals.CommonDateControl,
					name: 'reportsEndDate',
					id: 'ConUtilreportsEndDate',
					hidden: false,
					value : new Date(),
					listeners: {
						'focus': function(me) {
							this.onTriggerClick();
						}
					}
				},{
					xtype: 'boxselect',
					store: dataStore,
					width:200,
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-bottom':'10px',
						'margin-top':'10px'
					},
					multiSelect: false,
					mode: 'local',
					name: 'GroupBy',
					id :'ConUtilGroupBy',
					queryMode: 'remote',
					fieldLabel: 'Group By',
					displayField: 'monthRange',
					valueField: 'monthValue',
					value: '0'				 
				},{
					xtype			: 'checkbox',
					boxLabel	: 'WeekEnd Support',
					name		: 'Weekendsupport',
					inputValue	: '1',
					id        	: 'WeekendSupport',
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-bottom':'10px',
						'margin-top':'10px'
					},
				},{
					xtype			: 'checkbox',
					boxLabel	: 'Client Holiday',
					name		: 'ClientHoliday',
					inputValue	: '1',
					id        	: 'ClientHoliday',
					style:{
						'position':'absolute',				        					
						'margin-left':'40px',
						'margin-bottom':'10px',
						'margin-top':'10px'
					},
				}]
			}]
		}];
		
		this.buttons = [{
			text: 'Submit',
			name: 'ShowUtilization',
			handler: function() {
				AM.app.getController('ConsolidatedUtilizationReport').showGridPanel();
			}	
		},{
			text: 'Reset',
			name: 'ResetUtilization',
			handler: function() {
				this.up('TopPanelCon').getForm().reset();
			}	
		}];

		this.callParent(arguments);
		
	}
});
