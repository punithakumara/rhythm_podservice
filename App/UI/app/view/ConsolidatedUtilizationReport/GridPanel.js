Ext.define('AM.view.ConsolidatedUtilizationReport.GridPanel',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.GridPanelCon',
	title: 'Consolidated Utilizaton Report',    
    id : 'ConsildatedRepGrid',
    layout : 'fit',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
    width : '99%',
    minHeight : 225,
    loadMask: true, 
    border : true,
    style :{
    	border: "0px solid #157fcc",
        borderRadius:'0px'
	},
	frame : true,
	height: 520,
	store : 'ConsolidatedUtilizationReport',
    initComponent : function() {
		var me = this;
    	this.columns = [{
			header: 'Group By', dataIndex: 'GroupBy', flex: 1
		},{
			header: 'Vertical', dataIndex: 'vertical_name',flex: 1, 
				renderer:function(value,metaData,record,colIndex,store,view) {					
					metaData.tdAttr = 'data-qtip="' + record.get("vertical_name") + '"';
					return value;
				}
		},{
			header: 'Client', dataIndex: 'client_name',flex: 1,
				renderer:function(value,metaData,record,colIndex,store,view) {					
					metaData.tdAttr = 'data-qtip="' + record.get("client_name") + '"';
					return value;
				}
		},/*{
			header: 'Process', dataIndex: 'Process_Name',flex: 1,
				renderer:function(value,metaData,record,colIndex,store,view) {					
					metaData.tdAttr = 'data-qtip="' + record.get("Process_Name") + '"';
					return value;
				}
		},*/{
			header: 'Utilization %', dataIndex: 'Utilization', flex: 1
		},/*{
			header: 'No.of Tasks', dataIndex: 'No_Tasks', flex: 1, id: 'No_Tasks'
		},*/{
			header: 'No.of Hours', dataIndex: 'No_Hours', flex: 1, id: 'No_Hours',
				renderer: function(value, meta, rec) {
					var secs = value;
					var hr = Math.floor(secs / 3600);
					var min = Math.floor((secs - (hr * 3600))/60);
					var sec = secs - (hr * 3600) - (min * 60);
					
					if(hr=='0'){hr =''+hr;}
					
					while (min.length < 2) 
					{
						min = '0' + min;
					}

					if(Math.ceil(Math.log(min + 1) / Math.LN10) == '1')
					{
						min = '0' + min;
					}
					if (hr) hr += ':';
					return hr + min ;
				}
		},{
			header: 'Exp Hours', dataIndex: 'expectedHours', flex: 1, id: 'expectedHours',
				renderer: function(value, meta, rec) {
					var Resource 		= rec.get( 'Resources');
					var GroupBy 		= rec.get('GroupBy');
					var Billable_Resource = rec.get('Billable_Resource');
					var expectedHours = rec.get('expectedHours');
					
				/*	if(GroupBy=="Week 1" || GroupBy =="Week 2" || GroupBy =="Week 3" || GroupBy =="Week 4" || GroupBy =="Week 5"){
						var expectedHours = Billable_Resource * 8;
					}
					else if(GroupBy =="Jan" || GroupBy =="Feb" || GroupBy =="Mar" || GroupBy =="Apr" || GroupBy =="May" || GroupBy =="Jun" || GroupBy =="Jul" || GroupBy =="Aug" || GroupBy =="Sep" || GroupBy =="Oct" || GroupBy =="Nov" || GroupBy =="Dec" ){
						var expectedHours = Billable_Resource * 8;
					}
					else if(Resource !=""){
						var expectedHours = Resource * 8;
					}
					else{
						var expectedHours= rec.get( 'expectedHours' );
					} */
					var expectedHours= rec.get( 'expectedHours' );
					return expectedHours;
				}
		},{
			header: '<div> No.of Resource</div> Worked (Billable)', dataIndex: 'Resources2', flex: 1, id: 'Resources2',css: 'height:20px',flex: 1.3,
				renderer: function(value, meta, rec) {
					var Resources2			= rec.get( 'Resources2' );
					var WorkedNonBillable 	= rec.get('WorkedNonBillable');
					var WorkedBillable		= rec.get( 'WorkedBillable' );
					var expectedHours		= rec.get( 'expectedHours' );
					var	Resources1			= expectedHours/8;
					var Resources2			= parseInt(WorkedBillable);
					var TotalResource =   Resources2+" ("+Resources1+")";
					return TotalResource;						
				}
		},/*{
			header: '<div>Billable </div> Resources', dataIndex: 'Resources', flex: 1,		    	
				renderer: function(value, meta, rec) {
					var Billable_Resource = rec.get('Billable_Resource');
					var Resources = rec.get('Resources');
					if(Billable_Resource!="" && Billable_Resource!='0')
					{
						Resources= Billable_Resource
					}
					else{
						Resources = rec.get('Resources');
					}
					return Resources;					
				}		    	
		},*/{
			header: 'No.of Days', dataIndex: 'No_Days', flex: 1, id: 'No_Days'
		},/*{
			header: 'Errors', dataIndex: 'Error', flex: 1, id: 'Error'
		},*/{
			header: 'Month', dataIndex: 'Month', flex: 1, id: 'Reports_Month'
		},{
			header: 'Year', dataIndex: 'Year', flex: 1, id: 'Reports_Year'
		}],
		
		//**** Pagination ****//
		this.bbar = Ext.create('Ext.toolbar.Paging', {			
            store: this.store,
            displayInfo: true,
            pageSize: 5,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Items to Display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		//**** Pagination ****//
	
    	this.tools = [{
	    	xtype: "button",
	    	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			text : 'Export To Excel',
			style: {
	            "margin-right": "8px"
	        },
			handler: function(obj) {
				AM.app.getController('ConsolidatedUtilizationReport').downloadConUtiliaztionCSV();
			}
		}]
		
		this.callParent(arguments);
	},		
});