Ext.define('AM.view.MbrReport.List',{
	//extend : 'Ext.grid.Panel',
	extend : 'Ext.window.Window',
	//alias : 'widget.docAddEdit',
	alias : 'widget.MbrReportgrid',
	title: 'MBR Report',
    constrain: true,	
	layout: 'fit',
	width : '45%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal: true,
    y : 100,
    style:{
  		border: "0px solid #157fcc",
        borderRadius:'0px'
	},
	frame: true,
    initComponent : function() {
    	var me = this;

    	// var firstdayOfmonth = new Date();
     //    firstdayOfmonth.setDate(1);
     //    firstdayOfmonth.setMonth(firstdayOfmonth.getMonth());
    	
		this.items = [{
			xtype : 'form',
			itemId : 'addClientFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			fileUpload:true,
			isUpload: true,
			enctype:'multipart/form-data',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			
			items : [{
				xtype : 'hiddenfield',
				name : 'Access',
				value:'All'
			},{
				xtype : 'hiddenfield',
				name : 'template_type',
				value:'1'
			},{
				xtype : 'filefield',
				emptyText : 'Select File to Upload',
				name : 'Document',
				id : 'mbrre',
				fieldLabel : 'Upload File <span style="color:red">*</span><br>(File should be in CSV format)',
				anchor: '100%',
				allowBlank: false,
				buttonText: 'Browse File',
				listeners: {
                        change: function(fld, value) {
                            var newValue = value.replace(/C:\\fakepath\\/g, '');
                            fld.setRawValue(newValue);
                        }
                    }
			},{
				xtype : 'displayfield',
				id: "mbrReportDocument",
				value : '',
			}]
		}];

		this.buttons = ['->', {
			text : 'Sample Template',
			cls : 'sample',
			//icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.sampleTemplate
		},{
			xtype: 'displayfield',
			value : '<span style="color:red">* marked field is mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Upload',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];

		this.callParent(arguments);
		
	},

	onSave :  function() {			
		var win = this.up('window');
		var fname = win.down('form').query('filefield')[0].getValue();					
		
		// validation
		
		//var activeTab = Ext.getCmp('companyTabs').getActiveTab().tab.text;
		var exttype = fname.substr((Math.max(0, fname.lastIndexOf(".")) || Infinity) + 1);
		var extTypeList = new Array('csv','CSV');

		if(Ext.getCmp('mbrre').getValue() == ''){
			Ext.MessageBox.alert('Alert', 'Select file to upload');
			return false;
		}
		var form = win.down('form').getForm();
		var record = form.getRecord(), values = form.getValues();
		if (form.isValid()) 
		{
			if(extTypeList.indexOf(exttype) != -1 || win.down('form').query('filefield')[0].disabled == true)
			{
				var tempPath = Ext.getCmp('mbrre').getValue(); //C:\fakepath\sample.pdf
				var filename = tempPath.replace("C:\\fakepath\\","");

				AM.app.getController('MbrReport').onSaveDocument(form, win);
			}
			else if(win.down('form').query('filefield')[0].disabled != true)
			{
				Ext.MessageBox.alert('Alert', 'Invalid file format');
			}		
		}

	},

	sampleTemplate : function()
	{
		this.getEl().down('x-btn-inner').setStyle({'left':'0px'});
		window.location = AM.app.globals.appPath+'download/Sample Template.csv';
		var a = document.createElement("a");
        // safari doesn't support this yet
        a.href = window.location;
        document.body.appendChild(a);
        a.click();
	},

	onCancel : function() 
	{
		this.up('window').destroy();
	}
});