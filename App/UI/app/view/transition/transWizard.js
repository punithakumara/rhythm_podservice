Ext.define('AM.view.transition.transWizard', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.wizardtrans',
	layout  : { 
		type: 'card',
		//True to render each contained item at the time it becomes active, false to render all contained items as soon as the layout is rendered (defaults to false).
		deferredRender: true
	},
	itemId : 'wizardForm',
	title : 'People Transition',
	border : true,
	minHeight:150,
	autoScroll: true,
	width:'99%',
	activeItem: 0,
	defaults: {
		bodyPadding: 20
	},
	items: [{
		xtype:'tranclntpage'
	},{
		xtype:'tranpullhrpage'
	},{
		xtype:'transhiftpage'
	},{
		xtype:'transrelpage'
	},{
		xtype:'transshiftpage'
	},{
		xtype:'tranleadpage'
	},{
		xtype:'transleadpage'
	},{
		xtype:'tranleadpage'
	},{
		xtype: 'transpullhrrelpage'
	},{
		xtype:'tranfourthpage'
	}]
});