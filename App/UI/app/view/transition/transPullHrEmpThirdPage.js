Ext.define('AM.view.transition.transPullHrEmpThirdPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transpullhrrelpage',
    id: 'transPullHrEmpThirdPage',
	layout : 'anchor',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
    initComponent: function() {
		var me = this;
		var Av = Ext.getStore('Vertical').load({params: {hasNoLimit: '1',ComboNoVerticalID:'20'}});
		this.items=[{
			layout: {
                align: 'stretch',
                type: 'vbox'
            },
			xtype:'radiogroup',
			columns: 1,
			items: [{
				xtype:'gridpanel',
				border:true,
				id:'transPullHRThirdGridID',
				minHeight:300,
				style: 'padding-right:10px',
				columns: [{
					header: 'Employee ID',  
					dataIndex: 'company_employ_id',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 2
				},{
					header: 'Name',  
					dataIndex: 'FullName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Designation',  
					dataIndex: 'designationName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Email', 
					dataIndex: 'Email', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'POD', 
					dataIndex: 'podName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Service', 
					dataIndex: 'serviceName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				}]	
			}]
		},{
            xtype: 'datefield',
            id:'PullfromHR_date',
            format : AM.app.globals.CommonDateControl,
            labelWidth: 140,
            allowBlank: false,
            width: 400,
			fieldLabel: "Date <span style='color:red'>*</span>",
        },{
            xtype: 'combobox',
            id:'new_pod',
            name: 'new_pod',
            store : 'Pod',
            labelWidth: 140,
            allowBlank: false,
            multiSelect: false,
            minChars : 0,
            width: 400,
            queryMode: 'remote',
            displayField: 'pod_name', 
			valueField: 'pod_id',
			fieldLabel: "New POD <span style='color:red'>*</span>",
			listeners: {
				beforequery: function(queryEvent) {
					AM.app.getStore('Pod').getProxy().extraParams = {'hasNoLimit':'1', 'filterName' : queryEvent.combo.displayField};
				},
				select: function() {
					var myAry =  AM.app.globals.transEmployRes;
					myAry = myAry.filter(function(e){return e;});
					var empids = [];
					for (var i=0; i< myAry.length; i++)
					{
						empids.push(myAry[i].data.employee_id);						
					}
					var empidStr = (Ext.Array.unique(empids)).toString();
					
					Ext.getCmp('new_team_lead').setValue('');
					Ext.getCmp('new_team_lead').setDisabled(false);
					Ext.getStore('ReportTo').load({
						params:{'hasNoLimit':'1','PodId':this.getValue(),'EmployId':empidStr}
					});	
					Ext.getCmp('new_team_lead').bindStore("ReportTo");
				}
			}
        },{
            xtype: 'combobox',
            id:'new_service',
            name: 'new_service',
            store : 'Services',
            labelWidth: 140,
            allowBlank: false,
            multiSelect: false,
            minChars : 0,
            width: 400,
            queryMode: 'remote',
            displayField: 'name', 
			valueField: 'service_id',
			fieldLabel: "New Service <span style='color:red'>*</span>",
			listeners: {
				beforequery: function(queryEvent) {
					AM.app.getStore('Services').getProxy().extraParams = {'hasNoLimit':'1', 'filterName' : queryEvent.combo.displayField};
				},
				// select: function() {
				// 	var myAry =  AM.app.globals.transEmployRes;
				// 	myAry = myAry.filter(function(e){return e;});
				// 	var empids = [];
				// 	for (var i=0; i< myAry.length; i++)
				// 	{
				// 		empids.push(myAry[i].data.employee_id);						
				// 	}
				// 	var empidStr = (Ext.Array.unique(empids)).toString();
					
				// 	Ext.getCmp('new_team_lead').setValue('');
				// 	Ext.getCmp('new_team_lead').setDisabled(false);
				// 	Ext.getStore('ReportTo').load({
				// 		params:{'hasNoLimit':'1','ServiceId':this.getValue(),'EmployId':empidStr}
				// 	});	
				// 	Ext.getCmp('new_team_lead').bindStore("ReportTo");
				// }
			}
        },{
            xtype: 'combobox',
            id:'new_team_lead',
            name: 'new_team_lead',
            valueField: 'employee_id',
            displayField: 'Name',
            queryMode: 'remote',
            labelWidth: 140,
            width: 400,
            allowBlank: false,
            multiSelect: false,
            disabled: true,
			fieldLabel: "New Supervisor <span style='color:red'>*</span>",
			listeners:{
				scope: this,
				'beforequery': function(queryEvent, eOpts){
					var myAry =  AM.app.globals.transEmployRes;
					myAry = myAry.filter(function(e){return e;});
					var empids = [];
					for (var i=0; i< myAry.length; i++)
					{
						empids.push(myAry[i].data.employee_id);						
					}
					var empidStr = (Ext.Array.unique(empids)).toString();
					
					queryEvent.combo.store.proxy.extraParams = {
						'hasNoLimit':'1', 'PodId':Ext.getCmp('new_pod').getValue(),'EmployId':empidStr
					}								 
				}
			}
        },/*{
            xtype: 'combobox',
            id:'assign_team_lead',
            name: 'assign_team_lead',
            allowBlank: false,
            valueField: 'employee_id',
            displayField: 'full_name',
            queryMode: 'remote',
            store: 'TeamLead_AM',
            fieldLabel: "Assign Reportees To <span style='color:red'>*</span>",
            minChars : 0,
            labelWidth: 140,
            width: 400,
            listeners:{
                beforequery: function(queryEvent){
                    Ext.Ajax.abortAll();
                    queryEvent.combo.getStore().getProxy().extraParams = {'grades':Ext.getCmp('selectedGrade').getValue(), 'supervisor':Ext.getCmp('selectedCurrentLead').getValue()};
                }
            }
        },*/{
            xtype: 'combobox',
			fieldLabel: "New Shift <span style='color:red'>*</span>",
			id:'new_shift',
            name: 'new_shift',
			store: 'Shifts',
			labelWidth: 140,
            width: 400,
            allowBlank: false,
			displayField: 'shift_code',
			valueField: 'shift_id',
        },{
			xtype:'textareafield',
			id		  :'transPullHRComment',
			grow      : true,
			hidden	  : false,
			allowBlank: false,
			labelWidth: 140,
			name      : 'message',
			fieldLabel: "Comments <span style='color:red'>*</span>",
			anchor    : '99%',
			
		}];
		
		
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text  : '<< Previous',
			name  : 'back',
			handler: this.showSecondPage
		},{
			text  : 'Confirm >>',
			id:'pullconfirm',
			name  : 'confirmvert',
			handler: this.showEmpFourthpage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },	
	
	showSecondPage: function(btn){
		AM.app.getController('Transition').showTransSecondPage(btn);
	},	
	
	showEmpFourthpage: function(btn){
		if(!(Ext.getCmp('PullfromHR_date').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Date",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('new_pod').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select POD",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('new_service').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Service",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('new_team_lead').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Supervisor",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		// else if(!(Ext.getCmp('assign_team_lead').value)) 
		// {
		// 	Ext.Msg.show({
		// 	   title:'Warning',
		// 	   msg:"Please Select Assign Reportees To",
		// 	   icon: Ext.MessageBox.WARNING,
		// 	   buttons: Ext.Msg.OK
		// 	});
		// 	return false;
		// }
		else if(!(Ext.getCmp('new_shift').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Shift",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('transPullHRComment').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Enter Comments",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else			
		{
			Ext.getCmp('pullconfirm').setDisabled(true);
			AM.app.getController('Transition').onPullFromHRSavePage(btn);
		}
	}
});