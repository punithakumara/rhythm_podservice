Ext.define('AM.view.transition.transShiftThirdPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transshiftpage',
    id: 'transShiftThirdPage',
	layout : 'anchor',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
    initComponent: function() {
		var me = this;
		// var shiftStore = Ext.create('Ext.data.Store', {
		// 	autoLoad: true,
		// 	fields: ['name'],
		// 	data : [
		// 		{"name" : "IST"},{"name" : "CST"},{"name" : "IST"},{"name" : "PST"},{"name" : "APAC"},{"name" : "EMEA"},{"name" : "Late EMEA"},
		// 	]
		// });
		
		this.items=[{
			layout: {
                align: 'stretch',
                type: 'vbox'
            },
			xtype:'radiogroup',
			columns: 1,
			items: [{
				xtype:'gridpanel',
				border:true,
				id:'transShiftThirdGridID',
				minHeight:300,
				style: 'padding-right:10px',
				columns: [{
					header: 'Employee ID',  
					dataIndex: 'company_employ_id',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 2
				},{
					header: 'Name',  
					dataIndex: 'FullName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Designation',  
					dataIndex: 'designationName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Email', 
					dataIndex: 'Email', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'POD', 
					dataIndex: 'podName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Service', 
					dataIndex: 'serviceName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Shift', 
					dataIndex: 'current_shift', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				}]	
			}]
		},{
            xtype: 'datefield',
			id:'dateShiftStatus',
			format : AM.app.globals.CommonDateControl,
			fieldLabel: "Date <span style='color:red'>*</span>",
        },{
        	xtype: 'hiddenfield',
	        name: 'selectedShift',
	        id: 'selectedShift',
	        value: ''
        },{
            xtype: 'combobox',
			id:'shiftStatus',
			name: 'shiftStatus',
            allowBlank: false,
			queryMode: 'remote',
			fieldLabel: "New Shift <span style='color:red'>*</span>",
			store: 'Shifts',
			displayField: 'shift_code',
			valueField: 'shift_id',
			listeners:{
                beforequery: function(queryEvent){
                    Ext.Ajax.abortAll();
                    queryEvent.combo.getStore().getProxy().extraParams = {'shift_code':Ext.getCmp('selectedShift').getValue()};
                }
            }
        },{
			xtype	:'textareafield',
			id		  :'transShiftComment',
			grow      : true,
			hidden	  : false,
			allowBlank: false,
			name      : 'message',
			fieldLabel: "Comments <span style='color:red'>*</span>",
			anchor    : '99%',
			
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text  : '<< Previous',
			name  : 'back',
			handler: this.showSecondPage
		},{
			text  : 'Confirm >>',
			id:'shiftconfirm',
			name  : 'confirmvert',
			handler: this.showEmpFourthpage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },	
	
	showSecondPage: function(btn){
		AM.app.getController('Transition').showTransSecondPage(btn);
	},	
	
	showEmpFourthpage: function(btn){
		if(!(Ext.getCmp('dateShiftStatus').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Date",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('shiftStatus').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Shift",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('transShiftComment').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Enter Comments",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else			
		{
			Ext.getCmp('shiftconfirm').setDisabled(true);
			AM.app.getController('Transition').onShiftSavePage(btn);
		}
	}
});