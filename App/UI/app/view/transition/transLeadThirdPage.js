Ext.define('AM.view.transition.transLeadThirdPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transleadpage',
    id: 'transLeadThirdPage',
	layout : 'anchor',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
	itemId  : 'transLeadThirdPage',
    initComponent: function() {
		var me = this;
		this.items=[{
			layout: {
                align: 'stretch',
                type: 'vbox'
            },
			xtype:'radiogroup',
			columns: 1,
			items: [{
				xtype:'gridpanel',
				border:true,
				id:'transLeadThirdGridID',
				minHeight:300,
				style: 'padding-right:10px',
				columns: [{
					header: 'Employee ID',  
					dataIndex: 'company_employ_id',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 2
				},{
					header: 'Name',  
					dataIndex: 'FullName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Designation',  
					dataIndex: 'designationName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Email', 
					dataIndex: 'Email', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'POD', 
					dataIndex: 'podName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Service', 
					dataIndex: 'serviceName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				}]	
			}]
		},{
            xtype: 'datefield',
            id:'dateStatus',
            format : AM.app.globals.CommonDateControl,
            labelWidth: 120,
            allowBlank: false,
            width: 400,
			fieldLabel: "Date <span style='color:red'>*</span>",
        },{
			xtype:'hiddenfield',
			id:'selectedGrade',
			value:''
		},{
        	xtype: 'hiddenfield',
	        name: 'selectedCurrentLead',
	        id: 'selectedCurrentLead',
	        value: ''
        },{
            xtype: 'combobox',
            id:'team_lead',
            name: 'team_lead',
            allowBlank: false,
            valueField: 'employee_id',
            displayField: 'full_name',
            queryMode: 'remote',
            store: 'TeamLead_AM',
            fieldLabel: "New Supervisor <span style='color:red'>*</span>",
            minChars : 0,
            labelWidth: 120,
            width: 400,
            listeners:{
                beforequery: function(queryEvent){
                    Ext.Ajax.abortAll();
                    queryEvent.combo.getStore().getProxy().extraParams = {'grades':Ext.getCmp('selectedGrade').getValue(), 'supervisor':Ext.getCmp('selectedCurrentLead').getValue()};
                }
            }
        },{
			xtype:'textareafield',
			id		  :'transLeadComment',
			grow      : true,
			hidden	  : false,
			allowBlank: false,
			name      : 'message',
			fieldLabel: "Comments <span style='color:red'>*</span>",
            labelWidth: 120,
            anchor: '99%',
			
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text  : '<< Previous',
			name  : 'back',
			handler: this.showSecondPage
		},{
			text  : 'Confirm >>',
			id:'leadconfirm',
			name  : 'confirmvert',
			handler: this.showEmpFourthpage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },	
	
	showSecondPage: function(btn){
		AM.app.getController('Transition').showTransSecondPage(btn);
	},	
	
	showEmpFourthpage: function(btn){
		if(!(Ext.getCmp('dateStatus').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Date",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('team_lead').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Supervisor",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('transLeadComment').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Enter Comments",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else			
		{
			Ext.getCmp('leadconfirm').setDisabled(true);
			AM.app.getController('Transition').onLeadSavePage(btn);
		}
	}
});