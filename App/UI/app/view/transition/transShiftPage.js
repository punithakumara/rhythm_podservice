Ext.define('AM.view.transition.transShiftPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transhiftpage',
    id: 'transitionShiftPage',
	layout : 'anchor',
	store: 'AllReportees',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
	defaults     : { flex : 1 }, //auto stretch
    initComponent: function() {
		var me = this;
		this.items=[{
			layout: {
                type: 'hbox'
            },
			xtype:'panel',
			width:'60%',
			margin:'0 0 10 0',
			items: [{
				xtype : 'textfield',
				format : AM.app.globals.CommonDateControl,
				fieldLabel : 'Employee ID/Name ',
				anchor : '100%',
				emptyText: 'Enter Employee ID/Name',
				width: 500,
				name : 'resourceShift_id',
				id : 'resourceShift_id',
				labelWidth : 150,
				listeners: {
					change: function(field, e) {
						me.onClick(me)
					}
				}
			}, {
				xtype:'tbspacer',
				width: 15
			}, {
				xtype:'button',
				text:'Search',
				width: 70,
				listeners:{
					'click':function(){
						me.onClick(me)
					}
				}
			}]
		
		},{
			xtype:'gridpanel',
			border:true,
			margin:'0 0 10 0',
			store: 'AllReportees',
			selType: 'checkboxmodel',
			selModel: {
				checkOnly: true,
				mode:'MULTI',
				listeners: {
					deselect: function(model, record, index) {
						var myAry = AM.app.globals.transEmployRes;						
						for(var i =0; i < myAry.length; i++)
						{
							if (myAry[i] != null && (myAry[i].data.employee_id === record.data.employee_id))
							{
								myAry[i] = null;
							}
						}
					},
					select: function(model, record, index) {
						if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
						{	
							AM.app.globals.transEmployRes.push(record);
						}
					}
				}
			} ,
		    id:'transShiftGridID',
			minHeight:300,
			columns: [{
				header: 'Employee ID',  
				dataIndex: 'company_employ_id',
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 2
			},{
            	header: 'Name',  
            	dataIndex: 'FullName',
				menuDisabled:true,
				groupable: false,
				draggable: false,  
            	flex: 3
            },{
            	header: 'Designation',  
            	dataIndex: 'designationName',
				menuDisabled:true,
				groupable: false,
				draggable: false,  
            	flex: 3
            },{
            	header: 'Email', 
        		dataIndex: 'Email',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'POD', 
        		dataIndex: 'podName',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'Service', 
        		dataIndex: 'serviceName',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'Reporting To', 
        		dataIndex: 'supervisor',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'Current Shift', 
        		dataIndex: 'current_shift',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 2,				
            }],
			bbar: Ext.create('Ext.PagingToolbar', {
				id:'transShiftGridPage',
				store: this.store,
				displayInfo: true,
				pageSize: AM.app.globals.itemsPerPage,
				displayMsg: 'Displaying {0} - {1} of {2}',
				emptyMsg: "No Employees to display",
				plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
				listeners: {
					beforechange: function(item1,item2,item3){
						this.store.getProxy().extraParams = {
							'searchTxt': Ext.getCmp('resourceShift_id').getValue(),
							'manager' : Ext.util.Cookies.get('employee_id'),
							'NoHrPoolEmployees': '1',
						}
					},
					change:function(){
						this.store.getProxy().extraParams = {
							'searchTxt': Ext.getCmp('resourceShift_id').getValue(),
							'manager' : Ext.util.Cookies.get('employee_id'),
							'NoHrPoolEmployees': '1',
						}
						var myGrid = Ext.getCmp('transShiftGridID');
						
						for(i=0; i < this.store.data.length; i++)
						{
							var user = this.store.getAt(i);							
							var myAry = AM.app.globals.transEmployRes;
						
							for (var j=0; j< myAry.length; j++)
							{
								if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
									myGrid.selModel.doMultiSelect(user,true);
							}	
						}
					}					
				},
			}),
			listeners: {
				sortchange :  function(ct, column) {
					this.store.getProxy().extraParams = {
						'searchTxt': Ext.getCmp('resourceShift_id').getValue(),
						'manager' : Ext.util.Cookies.get('employee_id'),
						'NoHrPoolEmployees': '1',
					}
					this.store.load();
				}
			}
		}];
		
		this.buttons = [{
			text: 'Next >>',
			name:'clienttothirdpage',
			handler: this.showThirdPage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },
	
	showHomePage: function(){
		AM.app.getController('Transition').viewContent();
	},
	
	showThirdPage: function(btn){
		AM.app.getController('Transition').showTransThirdPage(btn);
	},
	
	onClick:function(my){	
		var resourceShift_id = "";
		
	//	if(Ext.getCmp('resourceTrans_id').getValue() != "")
		//{
			resourceShift_id = Ext.getCmp('resourceShift_id').getValue();			
			
			var MyStore = "";
			MyStore = Ext.getStore('AllReportees').load({
				params: {'searchTxt': Ext.getCmp('resourceShift_id').getValue(),'manager' : Ext.util.Cookies.get('employee_id')}
			})
			
			my.intialStore = MyStore;			
			Ext.getCmp('transShiftGridID').bindStore(MyStore);			
			Ext.getCmp('transShiftGridPage').bindStore(MyStore);
		//}
	}

}); 