Ext.define('AM.view.transition.transRelEmpThirdPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transrelpage',
    id: 'transReleaseEmpThirdPage',
	layout : 'anchor',
    loadMask: true,
    autoCreate: true,
    border : false,
	height:570,
	autoScroll: true,
	width:'98%',
	itemId  : 'transReleaseEmpThirdPage',
    initComponent: function() {
		var me = this;
		this.items=[{
			layout: {
                align: 'stretch',
                type: 'vbox'
            },
			xtype:'radiogroup',
			columns: 1,
			items: [{
				xtype:'gridpanel',
				border:true,
				id:'transRelThirdGridID',
				minHeight:300,
				style: 'padding-right:10px',
				columns: [{
					header: 'Employee ID',  
					dataIndex: 'company_employ_id',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 2
				},{
					header: 'Name',  
					dataIndex: 'FullName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Designation',  
					dataIndex: 'designationName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Email', 
					dataIndex: 'Email', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'POD', 
					dataIndex: 'podName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Service', 
					dataIndex: 'serviceName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				}]	
			}]
		},{
            xtype: 'datefield',
            id:'release_date',
            format : AM.app.globals.CommonDateControl,
            labelWidth: 100,
            allowBlank: false,
            width: 250,
			fieldLabel: "Release Date <span style='color:red'>*</span>",
        },{
            xtype: 'radiogroup',
			id:'hrPoolStatus',
			labelWidth: 100,
            // cls: 'x-check-group-alt',
            columns: 3,
			width: 600,
			fieldLabel: "Type <span style='color:red'>*</span>",
            items: [
                {boxLabel: 'Available for Transition', width:190, name: 'hrPoolStatus', inputValue:2},
                // {boxLabel: 'Training Needed', width:160, name: 'hrPoolStatus', inputValue:7},
                {boxLabel: 'Long Leave', width:160, name: 'hrPoolStatus', inputValue:4}              
            ]
        },{
			xtype:'textareafield',
			id		  :'transRelComment',
			grow      : true,
			hidden	  : false,
			allowBlank: false,
			name      : 'message',
			fieldLabel: "Comments <span style='color:red'>*</span>",
			anchor    : '99%',
			
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text  : '<< Previous',
			name  : 'back',
			handler: this.showSecondPage
		},{
			text  : 'Confirm >>',
			id:'releaseconfirm',
			name  : 'confirmvert',
			handler: this.showEmpFourthpage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },	
	
	showSecondPage: function(btn){
		AM.app.getController('Transition').showTransSecondPage(btn);
	},	
	
	showEmpFourthpage: function(btn){
		if(!(Ext.getCmp('release_date').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Date",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(Ext.getCmp('hrPoolStatus').isVisible()==true && !Ext.getCmp('hrPoolStatus').getValue().hrPoolStatus && Ext.getCmp('hrPoolStatus').getValue().hrPoolStatus!=0) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Select Type",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else if(!(Ext.getCmp('transRelComment').value)) 
		{
			Ext.Msg.show({
			   title:'Warning',
			   msg:"Please Enter Comments",
			   icon: Ext.MessageBox.WARNING,
			   buttons: Ext.Msg.OK
			});
			return false;
		}
		else			
		{
			Ext.getCmp('releaseconfirm').setDisabled(true);
			AM.app.getController('Transition').onSavePage(btn);
		}
	}
});