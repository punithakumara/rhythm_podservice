Ext.define('AM.view.attributes.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.attributesList',
	require : ['Ext.ux.grid.FiltersFeature'],
	title: 'Time Entry Fields',
    store : 'Attributes',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
	frame: true,
	width: "99%",
	minheight: 500,
	style: {
		border: "0px solid #157fcc",
		borderRadius:'0px'
	},
    height : 'auto',
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
	
    initComponent : function() {
    	
		this.columns = [{
			header : 'Name', 
			flex: 1,
			dataIndex : 'name',
			sortable: true,
			menuDisabled:true,
			draggable: false
		}, {
			header : 'Type', 
			dataIndex : 'type', 
			flex: 1,
			sortable: true,
			menuDisabled:true,
			draggable: false
		}, {
			header : 'Description', 
			dataIndex : 'description', 
			flex: 1,
			sortable: true,
			menuDisabled:true,
			draggable: false
		}, {
			header : 'Values', 
			dataIndex : 'values', 
			flex: 1,
			sortable: true,
			menuDisabled:true,
			draggable: false
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Attributes to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.tools =  [{
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		}]
		
		this.callParent(arguments);
	},
	
	onDelete : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('deleteAttributes', grid, rowIndex, colIndex);
	},
	
	onEdit : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		AM.app.getController('Attributes').onEditAttributes(grid, rowIndex, colIndex);
	}
});