Ext.define('AM.view.attributes.Add_edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.attributesAddEdit',
	
	title: 'Attributes Details',
	maxHeight:600,
	minHeight: 100,
	layout : 'fit',
	width : '40%',
    autoShow : true,
    modal: true,
	
	initComponent : function() {
		
		var statusStore = new Ext.data.SimpleStore({
			fields: ['opvalue', 'ValuesStatus'],
			data: [
			       ['0', 'Disabled'],
			       ['1', 'Active'],			       
			      ]
		});
		
		this.items = [
    	   {
	    		xtype : 'form',
	    		itemId : 'addAttributesFrm',
	    		bodyStyle : {
	    			border : 'none',
	    			display : 'block',
	    			float : 'left',
	    			padding : '10px'
	    		},
	    		fieldDefaults: {
	    		    labelAlign: 'left',
	    		    labelWidth: 90,
	    	        anchor: '100%'
	    		},
				autoScroll:true,
				//overflow: 'scroll',
				maxHeight: 200,
				minHeight: 100,
	    		items : [
							{
								 itemId :'Name',
								 xtype : 'textfield',
								 name : 'name',
								 allowBlank: false,
								 listeners:{
									 scope: this,
									 'blur': function(combo){
									   combo.setValue(combo.getValue().trim());
									 }
									 },
								 fieldLabel : 'Name',
								 
								 anchor: '100%'
							 },
			                    
								{
									 xtype : 'boxselect',
									 multiSelect: false,
									 fieldLabel: 'Type',
									 name: 'type',
									 store : 'attributesType',
									 displayField: 'name', 
									 valueField: 'id',
									 allowBlank: false,
									 listeners:{
										 scope: this,
										 'blur': function(combo){
										   combo.setValue(combo.getValue().trim());
										 },
										 select: this.setVisibility
										 },
									 typeAhead: false,
									 queryMode: 'local',
									 minChar: 0,
									 emptyText: 'Select type',
									 anchor: '100%'
								},{
								 xtype : 'fieldcontainer',
								 hidden: true,
								 layout : 'hbox',
								 hideLabel : true,
								 items : [{
									 xtype : 'textfield',
									 fieldLabel: ' ',
									 labelSeparator : "",
									 name: 'ListValues',
									 style : {
										'margin-right' : '10px',
										'float': 'right',
									 },
									 id: "attrfirst_text",
									 //allowBlank: false,
									 anchor: '100%',
									 flex : 5,
									 
								 }, 
						 
								 {
									 xtype : 'boxselect',
									 multiSelect: false,
									 name: 'ValuesStatus',
									 style: { 'margin-right' : '10px', 'float': 'right' },
									 store: statusStore,
									 queryMode: 'local',
									 minChars:0,
									 displayField: 'ValuesStatus', 
									 id: "attrsecond_text",
									 valueField: 'opvalue',
									 anchor: '100%',
									 flex : 3,
									 value : '1'
									 //allowBlank: false
								 },
								 								 								 
								 {
									 xtype : 'button',
									 text : 'Add',
									 flex : 1,
									 handler : function(btn) {
										var form 	  = btn.up('window').down('form');
										var attr_text = Ext.getCmp('attrfirst_text').getValue();
										var attr_second = Ext.getCmp('attrsecond_text').getValue();
										AM.app.getController('Attributes').onAddRow(form, [attr_text], [attr_second]);
									 	Ext.getCmp('attrfirst_text').setValue("");
									 }
								 } ]
							 }
								],
	    		
		    	buttons : ['->', {
		        	   text : 'Save',
					   // visible: AM.app.globals.accessObj[0]['addEdit']['attributes']['visible'],
					   // hidden:AM.app.globals.accessObj[0]['addEdit']['attributes']['hidden'],
		        	   icon : AM.app.globals.uiPath+'resources/images/save.png',
		        	   handler : this.onSave
		        	   
		           },{
        	   text : 'Cancel',
        	   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
        	   handler : this.onCancel
           }
		     	]
    	   }
    	];
    			
		this.callParent(arguments);
	},
	defaultFocus:'Name',
	onSave :  function() {
		var form = this.up('form').getForm();

        if (form.isValid()) {
        	var win = this.up('window');
        	AM.app.getController('Attributes').onSaveAttributes(form, win);
        }
	},
	onCancel : function() {
		this.up('window').close();
	},
	setVisibility: function(combo){
		var form = combo.up('window').query('fieldcontainer');
		if(combo.getValue() == 'list'){
			for(key in form) form[key].show();
		}else{
			for(key in form) form[key].hide();
		}
	}
});