Ext.define("AM.view.Profile.ProfilePopUp", {
    extend: "Ext.Window",
    alias: "widget.profilepopupx",
    id: "profilepopupcc",
    cls: "profilepopupcc",
    autoShow: true,
    width: "24%",
    height: 200,
    border: false,
    layout: "fit",
    frame: false,
    closable: false,
    moveable: false,
    draggable: false,
    resizable: false,
    tbar: false,
    modal: false,
    footer: true,
    x: 1010,
    y: 50,
    overflowY: "hidden",
    style: {
        display: "block"
    },
    initComponent: function() {
        var me      = this;

		if(Ext.util.Cookies.get("gender")=="Female")
		{
			var tpl = new Ext.XTemplate(
				"<tpl>",
				'<div id="profilepic">',
				'<img src="'+AM.app.globals.uiPath+'resources/images/female_64x64.png" width=64 height=64>',
				'</div>',
				'<div id="profilewatermark">',
				'<h3>'+Ext.util.Cookies.get("username")+'</h3>',
				'<p id="profilewatermark2">'+Ext.util.Cookies.get("DsnName")+'<br>'+Ext.util.Cookies.get("PodName")+'</p>',
				'</div>',
				'<div class="myprofilenotific">',
				'<div class="logoutnotif">',
				'<a href="#" onclick=AM.app.getController("Employees").onViewProfile()>My Profile</a>',
				'</div>',
				'</div>', 
				'<div class="profilenotific">',
				'<div class="logoutnotif">',
				'<a href="#" onclick=AM.app.getController("AM.controller.Login").logout() id="clearallnotfic">Logout</a>',
				'</div>',
				'</div>', 
				"</tpl>"
			);
		}
		else
		{
			var tpl = new Ext.XTemplate(
				"<tpl>",
				'<div id="profilepic">',
				'<img src="'+AM.app.globals.uiPath+'resources/images/male_64x64.png" width=64 height=64>',
				'</div>',
				'<div id="profilewatermark">',
				'<h3>'+Ext.util.Cookies.get("username")+'</h3>',
				'<p id="profilewatermark2">'+Ext.util.Cookies.get("DsnName")+'<br>'+Ext.util.Cookies.get("PodName")+'</p>',
				'</div>',
				'<div class="myprofilenotific">',
				'<div class="logoutnotif">',
				'<a href="#" onclick=AM.app.getController("Employees").onViewProfile()>My Profile</a>',
				'</div>',
				'</div>', 
				'<div class="profilenotific">',
				'<div class="logoutnotif">',
				'<a href="#" onclick=AM.app.getController("AM.controller.Login").logout() id="clearallnotfic">Logout</a>',
				'</div>',
				'</div>', 
				"</tpl>"
			);
		}

        this.items = [{
            xtype: "panel",
            html: tpl,
            id: "profiletpl",
        }];

        this.callParent(arguments);
    },

    listeners: {
        render: function(obj) {
            Ext.select(".profilepopupcc").on("mouseleave", function() {
                 var profPopup = Ext.getCmp('logoutButton');
                if(profPopup.flag == 1)
                {
                    Ext.getCmp("profilepopupcc").destroy();
                    profPopup.flag--;
                }
            });
        }
    }
});