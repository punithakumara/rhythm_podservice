Ext.define('AM.view.vertical.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.verticalList',
	require : ['Ext.ux.grid.FiltersFeature'],
	title: 'Vertical List',
    store : 'Vertical',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
    width : '99%',
    minHeight : 200,
    height : 'auto',
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
	
    initComponent : function() {
    	
		this.columns = [{
			header : 'Name', 
			flex: 1,
			dataIndex : 'name',
			menuDisabled:true,
			groupable: false,
			draggable: false,
		}, {
			header : 'Description', 
			dataIndex : 'description', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Folder', 
			dataIndex : 'folder', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Manager', 
			dataIndex : 'MGName', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		}, {
			header : 'Action', 
			width : '5%',
			xtype : 'actioncolumn',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
			
			}],
			listeners : {
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=5)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Vertical to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.tools = [{
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function()
			{
				this.reset();
				this.fireEvent('triggerClear');
			}
		}, { 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Add Vertical',
			handler: function () {
				AM.app.getController('Vertical').onCreate();
			},
			listeners : {
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=5)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
		}],

		this.callParent(arguments);
	},
	
	onDelete : function(grid, rowIndex, colIndex) 
	{
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('deleteVertical', grid, rowIndex, colIndex);
	},
	
	onEdit : function(grid, rowIndex, colIndex) 
	{
		AM.app.getController('Vertical').onEditVertical(grid, rowIndex, colIndex);
	}
});