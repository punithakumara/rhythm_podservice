Ext.define('AM.view.vertical.Add_edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.verticalAddEdit',
	requires: ['Ext.ux.GroupComboBox'],
	title: 'Vertical Details',
	layout : 'fit',
	width : '43%',
    autoShow : true,
    modal: true,
	
	initComponent : function() {
	
		this.items = [{
			xtype : 'form',
			itemId : 'addVerticalFrm',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 105,
				anchor: '100%'
			},
			items : [{
				xtype : 'boxselect',
				multiSelect: false,
				fieldLabel: 'Parent Vertical <span style="color:red">*</span>',
				name : 'parent_vertical_id',
				store: 'ParentVertical',
				queryMode: 'local',
				minChars: 1 ,
				emptyText: 'Select Parent Vertical',
				groupField: 'groupBy',
				displayField: 'pVerticalName', 
				valueField: 'Id',
				allowBlank: false,
				anchor: '100%',
				value: '',
			}, {
				itemId :'mngName',
				xtype : 'textfield',
				name : 'name',
				maskRe: /[A-Za-z0-9&, -]/,
				allowBlank: false,
				fieldLabel : 'Name <span style="color:red">*</span>',
				listeners:{
					scope: this,
					'blur': function(combo){
						combo.setValue(combo.getValue().trim());
					}
				},
				anchor: '100%'
			}, {
				xtype : 'textfield',
				name : 'description',
				fieldLabel : 'Description',
				anchor: '100%',
				listeners:{
					scope: this,
					'blur': function(combo){
						combo.setValue(combo.getValue().trim());
					}
				},
				allowBlank: true
			}, {
				xtype : 'textfield',
				name : 'folder',
				fieldLabel : 'Folder <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 15,
				listeners:{
					scope: this,
					'blur': function(combo){
						combo.setValue(combo.getValue().trim());
					}
				},
				allowBlank: false
			}, {
				xtype : 'boxselect',
				multiSelect: false,
				fieldLabel: 'Manager <span style="color:red">*</span>',
				name: 'MG',
				store: 'AllManagers',
				queryMode: 'remote',
				minChars: 1 ,
				emptyText: 'Select Manager',
				displayField: 'Name', 
				valueField: 'employee_id',
				allowBlank: false,
				anchor: '100%',
				value: '',
				
			}],
			
			buttons : [{
				xtype: 'displayfield',
				value : '<span style="color:red">* marked fields are mandatory</span>',
			},{
			    text : 'Cancel',
				icon : AM.app.globals.uiPath+'resources/images/cancel.png',
				handler : this.onCancel
				
			}, {
				text : 'Save',
				icon : AM.app.globals.uiPath+'resources/images/save.png',
				handler : this.onSave
			}]
		}];
		
		this.callParent(arguments);
	},

	onSave :  function() {
		var form = this.up('form').getForm();

        if (form.isValid()) 
		{
        	var win = this.up('window');
        	AM.app.getController('Vertical').onSaveVertical(form, win);
        }
	},
	
	controlverTriggered:function(combo, value){
		combo.allowBlank = false;
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
});