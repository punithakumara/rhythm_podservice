Ext.define('AM.view.ResourceMapping.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.ResourceMappinggrid',
	title: 'Resource Mapping Report',
    store : 'ResourceMapping',    
    layout : 'fit',
    width : '99%',
	minheight : 500,
	height : 'auto',
    loadMask: true,   
    remoteFilter:true,
    border : true,
    style:{
  		border: "0px solid #157fcc",
        borderRadius:'0px'
	},
	frame: true,
    initComponent : function() {
    	var me = this;

    	var firstdayOfmonth = new Date();
        firstdayOfmonth.setDate(1);
        firstdayOfmonth.setMonth(firstdayOfmonth.getMonth());
    	
		this.columns = [{
			header : 'Employee ID', 					
			dataIndex : 'EmployeeID',
			flex: 1,
			menuDisabled:true,
			groupable: false,
			draggable: false,
		},{
			header : 'Employee Name', 
			dataIndex : 'EmployeeName',
			flex: 1,
			menuDisabled:true,
			groupable: false,
			draggable: false,
		},{
			header : 'Client Name',
			dataIndex: 'ClientName',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		},{
			header : 'Pod', 
			dataIndex : 'Pod',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		},{
			header : 'Service', 
			dataIndex : 'Service',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		},{
			header : 'Billable', 
			dataIndex: 'Billable',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		},{
			header : 'Billable Type',
			dataIndex: 'BillableType',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		},{
			header : 'Location',
			dataIndex: 'location',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1,
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Attributes to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });	

        this.tools =  [{
			/* xtype: 'monthfield',
			submitFormat: 'Y-m-d',
			name: 'mbrmonth',
			fieldLabel: 'Month',
			id: 'mbrmonth',
			value:firstdayOfmonth,
			format: 'F, Y',
			labelCls : 'searchLabel',
			width: 250,
			listeners: {
				'focus': function(me) {
					this.onTriggerClick();
				},
				select:function (v){
					AM.app.getController('ResourceMapping').rmrMonthFilter(me,Ext.getCmp('mbrmonth').getValue(),v.getValue(),false);
				}
			} 
		}, {
			xtype: "tbseparator",
			style: {
				"margin-right": "10px"
			}
		}, {*/
	    	xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		},{ 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    },{
			xtype: "button",
	    	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			text : 'Export To Excel',
			style: {
	            "margin-right": "8px"
	        },
			handler: function(obj){
	    		AM.app.getController('ResourceMapping').downloadResourceMappingCSV();
			}
		}]	
		
		this.callParent(arguments);
		
	},
});