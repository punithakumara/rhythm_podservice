Ext.define('AM.view.Menu', { 
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.menu',
	id: 'ExtheaderMenuToolbar',
	cls: 'appMenu',
   // margin: '0 0 10 0',
   style: {'padding-bottom':'0', background: '#276193'},
   transitionType    : 'slide',
   delay        : 0.9,
   animate: true,
   initComponent : function() {

   	var me = this;
   	this.items = [];
   	
   	
   	/* Home Menu starts here */
   	me.items.push({text: 'Home', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'homeIcon',
   		listeners : {
   			click:function() {
   				AM.app.getController('Login').userSppecificDashboard();
   				AM.app.getController('Menu').viewDashboard();
   			},
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>2 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		},
   	});
   	
   	/* Home Menu ends here */
   	

   	/* A/C Management Menu starts */
   	var AccManagementSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
            }
         }
      });
   	
   	me.items.push({text : 'A/C Management', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'otherAppsIcon otherAppsIcon', 
   		listeners : {
   			mouseover : function() {
   				this.showMenu();
   			},
            // menushow: function (b) {
            //     this.mouseLeaveMonitor = this.menu.el.monitorMouseLeave(100, this.hideMenu,this);
            // },
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>=1 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
					//console.log(+'onshore');
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		},
   		menu: AccManagementSubmenu
   	});

   	AccManagementSubmenu.add({text: 'Work Orders', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
   		group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/menu/wor.png',
   		listeners : {
   			click: function(){
   				AM.app.getController('Menu').worDashboardList();
   			},
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>= 3)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		}
   	});
   	
   	AccManagementSubmenu.add({text: 'Change Orders', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
         group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/cor.png',
   		listeners : {
   			click: function(){
   				AM.app.getController('Menu').corDashboardList();
   			},
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>=3)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		}
   	});
   	
   	
   	
   	var IncidentSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
               AccManagementSubmenu.hide();
            }
         }
      });
   	
   	IncidentSubmenu.add({text: 'Appreciation', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon appreciationIcon', 
   		listeners:{
            mouseover : function() {
               this.showMenu();
            },
             menushow: function (b) {
                this.mouseLeaveMonitor = this.menu.el.monitorMouseLeave(100, this.hideMenu, this);
            },
   			click: function(){
   				AM.app.getController('Menu').appreciationList();
   			},
   		}
   	});
   	
   	IncidentSubmenu.add({text: 'Escalation', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon escalationIcon', 
   		listeners:{
            mouseover : function() {
               this.showMenu();
            },
             menushow: function (b) {
                this.mouseLeaveMonitor = this.menu.el.monitorMouseLeave(100, this.hideMenu, this);
            },
   			click: function(){
   				AM.app.getController('Menu').escalationList();
   			}
   		}
   	});
   	
   	IncidentSubmenu.add({text: 'External Error', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon errorIcon', 
   		listeners:{
            mouseover : function() {
               this.showMenu();
            },
             menushow: function (b) {
                this.mouseLeaveMonitor = this.menu.el.monitorMouseLeave(100, this.hideMenu, this);
            },
   			click: function(){
   				AM.app.getController('Menu').errorList();
   			}
   		}
   	});

   	
   	AccManagementSubmenu.add({text: 'Incident Log', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'opsIcon incidentlogIcon', group: 'theme',
   		listeners : {
   			mouseover : function() {
   				this.showMenu();
   			},
             menushow: function (b) {
                this.mouseLeaveMonitor = this.submenu.el.monitorMouseLeave(100, this.hideMenu, this);
            },
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>=1)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		},
   		menu:IncidentSubmenu
   	});

   	var ConfigurationSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
               AccManagementSubmenu.hide();
            }
         }
      });

   	ConfigurationSubmenu.add({text: 'Time Entry Fields', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
         group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/timeentryfields_16x16.png',
   		listeners:{
   			click: function(){
               AM.app.getController('Menu').attributesList();
            },
   		}
   	});

   	ConfigurationSubmenu.add({text: 'Work Order Types', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
         group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/menu/wor.png',
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').worTypes();
   			},
   		}
   	});

   	ConfigurationSubmenu.add({text: 'Projects', checked: false, style:{width:'auto', padding:'6px', height:'30px'},
         group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/projects_16x16.png', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').projectTypes();
   			},
   		}
   	});

   	ConfigurationSubmenu.add({text: 'Roles', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
         group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/roles_16x16.png', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').roleTypes();
   			},
   		}
   	});

   	AccManagementSubmenu.add({text: 'Configurations', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, 
         group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/configurations_16x16.png',
   		listeners : {
   			mouseover : function() {
   				this.showMenu();
   			},
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>=4)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		},
   		menu:ConfigurationSubmenu
   	});
   	
   	/* A/C Management Menu ends */

   	
   	
   	/* TIme management sub menu starts */
   	
   	var timeUtilSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
            }
         }
      });
   	
   	var TimesheetSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
               timeUtilSubmenu.hide();
            }
         }
      });
	   var OnshoreTimesheetSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
               timeUtilSubmenu.hide();
            }
         }
      });
   	
   	TimesheetSubmenu.add({text: 'Add Time', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'addTimeIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').timesheetList();
   			},
   		}
   	});		
   	
   	TimesheetSubmenu.add({text: 'Approve Timesheet', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'approveTimeIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').approveTimesheetList();
   			},
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>2)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		}
   	});	
   	
   	TimesheetSubmenu.add({text: 'Dashboard', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'dashboardTimeIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').timesheetDashboard();
   			},
   		},
		afterRender: function() {
   				if(Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   	});
	
	OnshoreTimesheetSubmenu.add({text: 'Add Time', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'addTimeIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').onshoreTimesheetList();
   			},
   		}
   	});	
	OnshoreTimesheetSubmenu.add({text: 'View Timesheet', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'approveTimeIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').viewOnshoreTimesheetList();
   			}
		},
		afterRender: function() {
   				if(Ext.util.Cookies.get('is_onshore')==1 && (Ext.util.Cookies.get('grade')!=3.1 || Ext.util.Cookies.get('grade')>=4))
   				{
   					this.show();
   				}
				else if(Ext.util.Cookies.get('grade')==7 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}  			  		
   	});	
   	
   	timeUtilSubmenu.add({text: 'Theorem Timesheet', checked: false, style:{width:'168px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'timesheetIcon', 
   		listeners : {
   			mouseover : function() {
   				this.showMenu();
   			},
   		},
		afterRender: function() {
   				if(Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			},
   		menu:TimesheetSubmenu
   	});
	
		timeUtilSubmenu.add({text: 'Onshore Timesheet', checked: false, style:{width:'168px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'timesheetIcon', 
   		listeners : {
   			mouseover : function() {
   				this.showMenu();
   			},
   		},
			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')==7 || Ext.util.Cookies.get('is_onshore')==1)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			},
   		menu:OnshoreTimesheetSubmenu
   	});
   	
   	
   	var MetricsSubmenu = Ext.create('Ext.menu.Menu',{
         listeners:{
            mouseleave: function(){
               this.hide();
               timeUtilSubmenu.hide();
            }
         }
      });
   	
   	MetricsSubmenu.add({text: 'Add Utilization', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'opsIcon utilisationIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').utilizationList();
   			}
   		},
		afterRender: function() {
   				if(Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   	});
   	
   	MetricsSubmenu.add({text: 'Approve Utilization', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
   		iconCls : 'opsIcon metricDashboardIcon', 
   		listeners:{
   			click: function(){
   				AM.app.getController('Menu').approveUtilizationList();
   			},
   			afterRender: function() {
   				if(Ext.util.Cookies.get('grade')>=3 || Ext.util.Cookies.get('approver')=="true")
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			}
   		}
   	});	
   	
		/* MetricsSubmenu.add({text: 'Accuracy', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'opsIcon accuracyIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').accuracyList();
				}
			}
		}); */
		
		timeUtilSubmenu.add({text: 'Client Utilization', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'opsIcon metricsIcon', group: 'theme',
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			afterRender: function() {
   				if(Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
   				{
   					this.show();
   				}
   				else
   				{
   					this.hide();
   				}
   			},
			menu:MetricsSubmenu
		});

		me.items.push({text : 'Time & Util', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},
			iconCls : 'timeAndUnitIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu: timeUtilSubmenu
		});
		
		/* Time management sub menu ends */

		/* Resource Management Data Menu starts */
		var ResourceManagementSubmenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
               }
            }
         });
		me.items.push({text : 'Resource Management', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},
			iconCls : 'resourceManagementIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			afterRender: function() {
				if((Ext.util.Cookies.get('grade')>2 && Ext.util.Cookies.get('is_onshore')!=1)|| (Ext.util.Cookies.get('VertId') == 10 && Ext.util.Cookies.get('is_onshore')!=1) || Ext.util.Cookies.get('employee_id') == 3086)
				{
					this.show();
				}
				else
				{
					this.hide();
				}
			},
			menu: ResourceManagementSubmenu
		});

		
		var transSubmenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
                  ResourceManagementSubmenu.hide();
               }
            }
         });
		transSubmenu.add({text: 'Shift Transition', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'ShiftTransitionIcon',
			listeners: {
				click: function(){
					AM.app.getController('Menu').shiftTransPage();
				},
			}
		});
		transSubmenu.add({text: 'Supervisor Transition', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'SupervisorTransitionIcon',
			listeners: {
				click: function(){
					AM.app.getController('Menu').leadTransPage();
				},
			}
		});
		
		ResourceManagementSubmenu.add({text: 'People Transition', checked: false, style:{width:'170px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon peplTransitionIcon', 
			listeners: {
				afterRender: function() {
					if((Ext.util.Cookies.get('grade')>=3)|| (Ext.util.Cookies.get('VertId') == 10))
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
			menu: transSubmenu
		});
		
		
		var rmgSubmenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
                  ResourceManagementSubmenu.hide();
               }
            }
         });
		rmgSubmenu.add({text: 'Release to RMG', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'ReleaseToHRPoolIcon', 
			listeners: {
				click: function(){
					AM.app.getController('Menu').releaseHrPage();
				},
			}
		});
		
		rmgSubmenu.add({text: 'Pull from RMG', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'PullFromHRPoolIcon',
			listeners: {
				click: function(){
					AM.app.getController('Menu').pullHrPage();
				},
			}
		});
		
		ResourceManagementSubmenu.add({text: 'RMG', checked: false, style:{width:'170px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon resourceManagementGroupIcon', 
			listeners: {
				afterRender: function() {
					if((Ext.util.Cookies.get('grade')>=3)|| (Ext.util.Cookies.get('VertId') == 10))
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
			menu: rmgSubmenu
		});

		/* Resource Management Data Menu ends */
		

		/* Report sub menu starts */
		
		var reportsMenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
               }
            }
         });

		reportsMenu.add({text: 'Employee View', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
			iconCls : 'reportsMenuIcon hierarchyIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').OpsDashboard();
				},
				afterRender: function() {
					if((Ext.util.Cookies.get('grade')>2) || (Ext.util.Cookies.get('VertId') == 10))
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});
		
		var billabilitySubmenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
                  reportsMenu.hide();
               }
            }
         });
		
		billabilitySubmenu.add({text: 'FTE Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon ftereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').monthlyBillabilityFteReportPage();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=3)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});
		
		billabilitySubmenu.add({text: 'Hourly & Volume Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon volumereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').monthlyBillabilityReportPage();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=3)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});
		
		billabilitySubmenu.add({text: 'Project Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon volumereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').monthlyBillabilityProjectReportPage();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=3)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});
		
		reportsMenu.add({text: 'Monthly Billability Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'reportsMenuIcon acMngFRIcon',
			menu:billabilitySubmenu,
			listeners:{
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=3 || (Ext.util.Cookies.get('VertId') != 10))
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});
		
		
		reportsMenu.add({text: 'Resource Mapping Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
			iconCls : 'reportsMenuIcon resourcesMappingReportIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').ResourceMapping();
				},
				afterRender: function() {
					if((Ext.util.Cookies.get('grade')>2) || (Ext.util.Cookies.get('VertId') == 10))
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});

      reportsMenu.add({text: 'MBR Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
         iconCls : 'reportsMenuIcon resourcesMappingReportIcon',
         listeners:{
            click: function(){
               AM.app.getController('Menu').MbrReport();
            },
            afterRender: function() {
               if((Ext.util.Cookies.get('grade')>4) || (Ext.util.Cookies.get('VertId') == 10))
               {
                  this.show();
               }
               else
               {
                  this.hide();
               }
            }
         }
      });
		
		me.items.push({text : 'Reports', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'reportsMenuIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
				afterRender: function() {
					if((Ext.util.Cookies.get('grade')>=3 && Ext.util.Cookies.get('is_onshore')!=1) || (Ext.util.Cookies.get('VertId') == 10 && Ext.util.Cookies.get('is_onshore')!=1) || Ext.util.Cookies.get('employee_id') == 3086)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			},
			menu: reportsMenu
		});
		
		/* Reports sub menu ends */
		
		
		/* Documents sub menu starts */
		
		var documentsSubMenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
               }
            }
         });
		documentsSubMenu.add({text : 'Company Documents', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, group: 'theme',iconCls : 'companyDocIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').companyDocumentsPage();
				},
			}
		});
		documentsSubMenu.add({text : 'Vertical Documents', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, group: 'theme',iconCls : 'verticalDocIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').verticalDocumentsPage();
				},
			}
		});
		
		me.items.push({text : 'Documents', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'otherAppsIcon docmtIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				}
			},
			afterRender: function() {
					if(Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				},
			menu: documentsSubMenu
		});	
		
		/* Documents sub Menu ends */

		/* Master Data Menu starts */
		
		var MasterDataSubmenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
               }
            }
         });

      MasterDataSubmenu.add({text: 'PODs', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
         iconCls : 'mstrDataIcon verticalIcon',
         listeners : {
            click: function(){
               AM.app.getController('Menu').podList();
            },
            afterRender: function() {
               if(Ext.util.Cookies.get('grade')>=6 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
               {
                  this.show();
               }
               else
               {
                  this.hide();
               }
            }
         }
      });   

		// MasterDataSubmenu.add({text: 'Verticals', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		// 	iconCls : 'mstrDataIcon verticalIcon',
		// 	listeners : {
		// 		click: function(){
		// 			AM.app.getController('Menu').verticalList();
		// 		},
		// 		afterRender: function() {
		// 			if(Ext.util.Cookies.get('grade')>=4 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
		// 			{
		// 				this.show();
		// 			}
		// 			else
		// 			{
		// 				this.hide();
		// 			}
		// 		}
		// 	}
		// });	
		
		MasterDataSubmenu.add({text: 'Clients', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
			iconCls : 'mstrDataIcon clientIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').clientList();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')==2 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==206)
					{
						this.show();
					}
					else if(Ext.util.Cookies.get('grade')==2 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==207)
					{
						this.show();
					}
					else if(Ext.util.Cookies.get('grade')>=4 || Ext.util.Cookies.get('employee_id') == 3086)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});

      MasterDataSubmenu.add({text: 'Services', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
         iconCls : 'mstrDataIcon verticalIcon',
         listeners : {
            click: function(){
               AM.app.getController('Menu').servicesList();
            },
            afterRender: function() {
               if(Ext.util.Cookies.get('grade')>=6 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
               {
                  this.show();
               }
               else
               {
                  this.hide();
               }
            }
         }
      }); 	
		
		
		MasterDataSubmenu.add({text: 'Designations', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon designationIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').designationList();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>2 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});	

		MasterDataSubmenu.add({text: 'Employees', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
			iconCls : 'mstrDataIcon employeeIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').empList();
				},
				afterRender: function() {
					//console.log(Ext.util.Cookies);
					if(Ext.util.Cookies.get('grade')>2 || Ext.util.Cookies.get('VertId')=='10' || Ext.util.Cookies.get('is_onshore')==1)
					{						
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});		

		MasterDataSubmenu.add({text: 'Client Holidays', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon clientholiday',
			listeners : {
				click: function(){
					AM.app.getController('Menu').clientHolidaysList();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>6 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});


		var skillsSubmenu = Ext.create('Ext.menu.Menu',{
            listeners:{
               mouseleave: function(){
                  this.hide();
                  MasterDataSubmenu.hide();
               }
            }
         });
		skillsSubmenu.add({text: 'Configuration', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon ftereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').configurationList();
				}
			}
		});
		
		skillsSubmenu.add({text: 'Search Skills', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon volumereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').searchSkills();
				},
			}
		});

		MasterDataSubmenu.add({text: 'Skills', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon clientholiday',
			menu:skillsSubmenu,
			listeners:{
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=7 && Ext.util.Cookies.get('is_onshore')!=1 || Ext.util.Cookies.get('employee_id') == 3086)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});		

		me.items.push({text: 'Master Data', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'mstrDataIcon',
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')==7 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==206)
					{
						this.show();
					}
					else if(Ext.util.Cookies.get('grade')==7 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==207)
					{
						this.show();
					}
					else if((Ext.util.Cookies.get('grade')>2 || Ext.util.Cookies.get('VertId')=='10') && Ext.util.Cookies.get('is_onshore')!=1)
					{
							this.show();
					}
					else
					{
						this.hide();
					}
				}

			},
			menu:MasterDataSubmenu
		});
		
		/* Master Data Menu ends */
		
		this.callParent(arguments);
		
	}
});