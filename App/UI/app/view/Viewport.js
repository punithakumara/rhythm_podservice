Ext.define("AM.view.Viewport", {
    extend: "Ext.container.Viewport",
    layout : {
        type: 'card',
        deferredRender: true
    },
    id: "viewport",
    activeItem: 0,
    items: [ {
        id: "card-0",
        xtype: "container",
        region: "center",
        layout: {
            type: "vbox",
            align: "center"
        },
        style: {
            position: "fixed",
            top: "43%",
            left: "40%"
        },
        html: '<img src="./resources/images/loader-img.gif" alt="Loading...." />'
    }, {
        id: "card-1",
        xtype: "container",
        layout: "border",
        width: "100%",
        defaults: {
            split: false
        },
        items: [ {
            region: "north",
            bodyStyle: {
                "background-color": "#276193",
                color: "white",
                width: "100%",
                border: "none",
                "float": "left"
            },
            border: "none",
            height: 65,
            items: [ {
                xtype: "container",
                height: 65,
                width: "250px",
                style: {
                    "float": "left",
                    padding: "5px"
                },
                layout: {
                    type: "column",
                    align: "center",
                    pack: "center"
                },
                html: '<img src="./resources/images/rhythm_logo_new.png" alt="Theorem Rhythm"/>'
            }, {
                xtype: "container",
                height: 65,
                width: "75%",
                style: {
                    "float": "left"
                },
                layout: {
                    type: "vbox",
                    align: "left",
                    pack: "end"
                },
                id: "menuToolbar"
            },{
                xtype: "container",
                height: 40,
                width: "15%",
                style: {
                    "float": "right",
                    "margin-top": "12px",
                    "position": "fixed",
                    "right": "0px",
                },
                border: "none",
                layout: {
                    type: "vbox",
                    align: "center",
                    pack: "center"
                },
                items: [ {
                    xtype: "container",
                    style: {
                        "float": "right"
                    },
                    items: [ {
                        xtype: "container",
                        id: "NotifyToolbar",
                        border: true,
                        style: {
                            "float": "left",
                            margin: "9px 0 0 -15px"
                        },
                        flag: 0,
                        listeners: {
                            render: function() {
                                var notifypopup = Ext.getCmp("NotifyToolbar");
                                notifypopup.getEl().on("click", function() {
                                    if (0 == notifypopup.flag) {
                                        AM.app.getController("Notification").viewPopNotify();
                                        notifypopup.flag++;
                                    }
                                    else
                                    {
                                        Ext.getCmp("notifypopupcc").destroy();
                                        notifypopup.flag--;
                                    }
                                });
                            }
                        }
                    },{
                        layout: "vbox",
                        pack: "end",
                        id: "logoutButton",
                        xtype: "button",
                        icon: "./resources/images/user_profile.png",
                        width:32,
                        height:32,
                        // action: "logout",
                        flag: 0,
                        listeners: {
                            click: function() {
                                var profPopup = Ext.getCmp('logoutButton');
                                if(profPopup.flag == 0)
                                {
                                    AM.app.getController("Profile").viewProfile();
                                    profPopup.flag++;
                                }
                                else
                                {
                                    Ext.getCmp("profilepopupcc").destroy();
                                    profPopup.flag--;
                                }
                            }
                        }
                    },{
                        xtype: "container",
                        layout: "column",
                        style: {
                            "float": "right",
                        },
                        border: "none",
                        margin: "15 10 0 0",
                        id: "logInCont",
                        value:'1223456',
                        listeners: {
                            render: function(obj) {
                                var viewProfile = Ext.getCmp("logInCont");
                                viewProfile.getEl().on("click", function() {
                                    //AM.app.getController("Employees").userProfile();
                                });
                            }
                        }
                    }]
                }]
            }]
        }, {
            region: "center",
            layout: {
                type: "vbox",
                align: "center"
            },
            width: "104%",
            bodyStyle: {
                border: "none",
                padding: "10px 0"
            },
            style: {
                clear: "both"
            },
            autoScroll: true,
            id: "mainContent"
        }, {
            region: "south",
            layout: {
                type: "vbox",
                align: "center"
            },
            bodyStyle: {
                "background-color": "#276193",
                color: "white",
                width: "100%",
                border: "none",
                "float": "left",
                "margin-top": '5px'
            },
            border: "none",
            height: 40,
            items: [ {
                xtype: "container",
                height: 35,
                width: "auto",
                style: {
                    "float": "left",
                    padding: "10px"
                },
                layout: {
                    type: "column",
                    align: "center",
                    pack: "center"
                },
                html: '&#169; 2019 Theorem Inc., - All Rights Reserved v3.7.1'
            }]
        }]
    } ]
});