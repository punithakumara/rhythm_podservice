Ext.define('AM.view.process.Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.processAddEdit',
	//requires: ['AM.view.process.ProcessTabs'],
	title: 'Process details',
    layout: 'fit',
	width: '70%',
    autoShow : true,
	autoHeight : true,
    modal: true,
	techFlag: 1,
	
    initComponent : function() {
		var me = this;
		var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			//autoCancel: false,
			saveBtnText: 'Save',
			listeners: {
				validateedit:function(editor, e, eOpts){
					var Validity = true;
					editor.editor.form.getFields().items.forEach(function(entry) {
						if(entry.value === "" || entry.value === null){
							Validity = false;
						}
					});
					
					if(Validity) 
						return true;
					else 
						return false;
				},
				edit: function(grid, obj, eOpts){
					var data = obj.view.store.data;
					var dataArray1 = [];
					Ext.each(data.items, function(value, i) {
						dataArray1.push(value.data.Name);
					});
					dataArray1 = JSON.stringify(dataArray1);
					var form = obj.grid.up('window').down('form').getForm();
					form.findField("TechnologyID").setValue(dataArray1.toString());
					//console.log(form.findField("TechnologyID").getValue());
					
				},
				canceledit: function(grid,obj){
					if(obj.store.data.items[0].data['Name'] == "")
						obj.grid.getStore().remove(obj.record);
				}
			}
		});
		var rEditing = Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			autoCancel: false,
			saveBtnText: 'Save',
			listeners: {			
			validateedit:function(editor, e, eOpts){
					var Validity = true;
					editor.editor.form.getFields().items.forEach(function(entry) {	
						if(entry.name == "Name"){
							if(entry.value === "" || entry.value === null){
								Validity = false;
							}	
						}
					});
					if(Validity) 
						return true;
					else 
						return false;
				},
				edit: function(grid, obj, eOpts){
				
					var data = obj.view.store.data;										
					var dataArray = [];			
					Ext.each(data.items, function(value, i) {
						dataArray.push(value.data.Name);
					});
					/* added to order Process Attributes*/
					//console.log(dataArray);
					dataArray = JSON.stringify(dataArray);
					var form = obj.grid.up('window').down('form').getForm();
					form.findField("Attribute_Names").setValue(dataArray.toString());
					//console.log(form.findField("Attribute_Names").getValue());
					/* added to hide and show of save Process Attributes*/
					if(Ext.util.Cookies.get('grade')  >= 3)
					Ext.ComponentQuery.query('#ProcessSaveBtn')[0].show();
					/* added to hide and show of save Process Attributes*/
				},
				canceledit: function(grid,obj){
					if(obj.store.data.items[0].data['Name'] == "")
						obj.grid.getStore().remove(obj.record);
					/* added to hide and show of save Process Attributes*/
					if(Ext.util.Cookies.get('grade')  >= 3 && Ext.util.Cookies.get('grade') < 4)
					Ext.ComponentQuery.query('#ProcessSaveBtn')[0].hide();
					/* added to hide and show of save Process Attributes*/
				}
			}
		});	
		
		var techStore = Ext.create('Ext.data.Store', {
			fields : [{name : 'TechnologyID', dataType : 'int'}, 'Name'],
			data: [],
		});
		var attrStore = Ext.create('Ext.data.Store', {
			fields : [{name : 'AttributesID', dataType : 'int'}, 'Name','OrderofAttributes'],
			data: [],
		});
		
		var techGrid = Ext.create('Ext.grid.Panel', {
			title: 'Tools & Technologies',
			store: techStore,
			border: true,
			height: 200,
			plugins: [rowEditing],
			columns: [{
				header: 'Name',
				dataIndex: 'Name',
				flex: 1,
				editor: {
					xtype: 'boxselect',
					//editable: true,
					minChars:1,
					multiSelect: true,
					name: 'Name',
					store: 'Tech',
					mode: 'remote',
					displayField: 'Name', 
					valueField: 'Name',
					listeners:{
						beforequery: function() {
							var verticalId = Ext.getCmp('VerticalNameCombo').getValue();
							AM.app.getStore('Tech').getProxy().extraParams = {'hasNoLimit':'1', VerticalID: verticalId};
						}
					}
					// allowBlank: false,
				}
			},{
				header: 'Action',
				xtype: 'actioncolumn',
				width: '8%',
				items: [{
					icon: 'resources/images/delete.png',
					tooltip : 'Delete',
					handler: function(grid, rowIndex, colIndex){
						var array = [];
						var form = grid.up('form').getForm();
						grid.store.removeAt(rowIndex);
						grid.store.data.each(function(){
							array.push(this.data.Name);
						});
						array = JSON.stringify(array);
						form.findField("TechnologyID").setValue(array.toString());
					}
				}]
			}],
			tbar: [{
				xtype: 'tbfill'
			},{
				text: 'Choose Technology',
				handler : function(obj) {
					if(me.techFlag == 1){
						var grid = obj.up('gridpanel');
						rowEditing.cancelEdit();
						grid.store.insert(0, grid.store);
						rowEditing.startEdit(0, 0);
					}
				}
			},{
				text: 'Add New Technology',
				handler : this.onNewTechClick
			}]
		});
			
		var attrGrid = Ext.create('Ext.grid.Panel', {
			title: 'Process Attributes',
			store: attrStore,
			itemId:'ProcAttrGrid',
			border: true,
			height: 200,
			plugins: [rEditing],
			viewConfig: {
		        plugins: {
		            ptype: 'gridviewdragdrop',
		            dragText: 'Drag and drop to reorder the Process Atrributes'
		        },
		        listeners:{
		        	drop: function( node, data, overModel, dropPosition, eOpts ) {
		        	//console.log(data.view);
		        	var sortDataitems = [];
		        	var dataArray = [];
		        	Ext.each(data.view.store.data.items, function(value, i) {
						sortDataitems.push(value.data.Name);
					});
		        	dataArray = JSON.stringify(sortDataitems);
					var form = data.view.up('window').down('form').getForm();
					form.findField("Attribute_Names").setValue(dataArray.toString());
					//console.log(form.findField("Attribute_Names").getValue());
					/* added to hide and show of save Process Attributes*/
					if(Ext.util.Cookies.get('grade')  >= 3)
					Ext.ComponentQuery.query('#ProcessSaveBtn')[0].show();
					/* added to hide and show of save Process Attributes*/
		        }
		        }
		    },
			columns: [{
				header: 'Name',
				dataIndex: 'Name',
				flex: 1,
				renderer: this.renderTip,
				editor: {
					xtype: 'boxselect',
					//editable: false,
					minChars:1,
					multiSelect: true, //MULTI SELECT ENABLED
					name: 'Name',
					store: 'Attributes',
					mode: 'remote',
					displayField: 'Name', 
					valueField: 'Name',
					listeners:{
						beforequery: function() {
							AM.app.getStore('Attributes').getProxy().extraParams = {'hasNoLimit':'1'};
						}
					}
				}
			},/* added to order Process Attributes*/{
				header: 'Order Attributes',
				dataIndex: 'OrderofAttributes',
				renderer: this.renderTip,
				flex: 1,
				hidden:true
			}/* end added to order Process Attributes*/,{
				header: 'Action',
				xtype: 'actioncolumn',
				width: '8%',
				items: [{
					icon: 'resources/images/delete.png',
					tooltip : 'Delete',
					handler: function(grid, rowIndex, colIndex){
						var array = [];
						var form = grid.up('form').getForm();
						grid.store.removeAt(rowIndex);
						grid.store.data.each(function(data, i){
							array.push(grid.store.data.items[i].data.Name);
						});
						array = JSON.stringify(array);
						form.findField("Attribute_Names").setValue(array.toString());
						console.log(form.findField("Attribute_Names").getValue());
					}
				}]
			}],
			tbar: [{
				xtype: 'tbfill'
			},{
				text: 'Choose Attributes',
				handler : function(obj) {
					var grid = obj.up('gridpanel');
					rEditing.cancelEdit();
					grid.store.insert(0, grid.store);
					rEditing.startEdit(0, 0);
				}
			},{
				text: 'Add New Attributes',
				handler : this.onNewAttributes
			}]
		});
	
		var processDescription = new Ext.FormPanel({
		title: 'Process Description',
		bodyStyle:'padding:5px 5px 0',
		layout: 'fit',
		items: [{
		xtype:'htmleditor',
		name: 'ProcessDescription',
		anchor:'100%',
		enableColors: true,         
		}]
		});
	
    	var ProcessType = new Ext.data.SimpleStore({
  	      fields: ['Flag', 'processType'],
  	      data: [
   	             ['FTE', 'FTE'],
   	             ['Hourly', 'Hourly'],
   	             ['Mix', 'Mix']
  	             ]
  	     });   
    	
		if(me.action == 'edit')
		{
			var assocManger = {xtype : 'boxselect', multiSelect: false, fieldLabel: 'Associate Manager', id: 'verticalmanager', name: 'AM', queryMode: 'local',
			typeAhead:true, displayField: 'Name', valueField: 'EmployID', flex: 1, readOnly: true};
			
			var pleads = { xtype : 'boxselect', fieldLabel: 'Leads', id: 'Process_leads', name: 'Process_leads', queryMode: 'local', displayField: 'Name', valueField: 'EmployID', width: '48.8%', readOnly: true };
		}
		else{
			var assocManger = {xtype : 'boxselect', multiSelect: false, fieldLabel: 'Associate Manager', id: 'verticalmanager', name: 'AM', queryMode: 'local',
			typeAhead:true, displayField: 'Name', valueField: 'EmployID', allowBlank: false, flex: 1, emptyText: 'Select Associate Manager...',
			
			listeners: {
				select: function(obj){
					Ext.getStore('Leads').load({ params:{ EmplyID: obj.getValue()}});
					Ext.getCmp('Process_leads').bindStore('Leads');
				}
			}};
			
			var pleads = { xtype : 'boxselect', fieldLabel: 'Leads', id: 'Process_leads', name: 'Process_leads', queryMode: 'local', displayField: 'Name', valueField: 'EmployID', allowBlank: true, width: '48.8%', emptyText: 'Select Lead...', };
		}
    	this.items = [
    	   {
	    		xtype : 'form',
	    		itemId : 'addProcessFrm',
	    		bodyStyle : {
	    			border : 'none',
	    			display : 'block',
	    			float : 'left',
	    			padding : '10px'
	    		},
	    		fieldDefaults: {
	    		    labelAlign: 'left',
	    		    labelWidth: 165,
	    	        anchor: '100%'
	    		},
	    		items : [{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					margin:1,
					items: [{
						xtype : 'textfield',
						name : 'Name',
						allowBlank: false,
						listeners:{
							scope: this,
							'blur': function(text){
							   text.setValue(text.getValue().trim());
							}
						},
						fieldLabel : 'Name',
						flex: 1,
					},{
						xtype : 'boxselect',
						multiSelect: false,
						store: ProcessType,
						id: 'ProcessType',
						fieldLabel: 'Process Type',
						name: 'ProcessType',
						queryMode: 'local',
						minChars:1,
						displayField: 'processType', 
						valueField: 'Flag',
						margins : '0 0 0 20',
						flex: 1,
						listeners: {
							select : function(obj){
								if(obj.getValue() == 'Hourly'){
									Ext.getCmp('ClientNameCombo').next().enable();
									Ext.getCmp('ClientNameCombo').next().allowBlank = false;
									if(typeof me.record != 'undefined')
										Ext.getCmp('ClientNameCombo').next().setValue(me.record.get('minHours'));
								}
								else{
									Ext.getCmp('ClientNameCombo').next().disable();
									Ext.getCmp('ClientNameCombo').next().allowBlank = true;
									Ext.getCmp('ClientNameCombo').next().setValue('');
								}
							}
						}
					},{
						xtype: 'hidden',
						name: 'TechnologyID',
					},{
						xtype: 'hidden',
						name: 'Attribute_Names',
					}]
				},{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					margin:'7 0 0 0',
					items: [{
						xtype : 'boxselect',
						multiSelect: false,
						fieldLabel: 'Client',
						id : 'ClientNameCombo',
						name: 'ClientID',
						store: Ext.getStore('Clients'),
						queryMode: 'remote',
						minChars:0,
						displayField: 'Client_Name',
						valueField: 'ClientID',
						allowBlank: false,
						emptyText: 'Select Client',
						flex: 1,
					},{
						xtype : 'textfield',
						fieldLabel: 'Min Hours',
						name: 'minHours',
						margins : '0 0 0 20',
						flex: 1,
					}]
				},{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					margin:'4 0 0 0',
					items: [{
						xtype : 'boxselect',
						multiSelect: false,
						fieldLabel: 'Vertical',
						id : 'VerticalNameCombo',
						name: 'VerticalID',
						store: 'Vertical',
						queryMode: 'remote',
						minChars:0,
						displayField: 'Name', 
						valueField: 'VerticalID',
						allowBlank: false,
						flex: 1,
						emptyText: 'Select Vertical',
						listeners: {
							select: this.verticalCombo
						}
					},{
						xtype: 'datefield',
						fieldLabel: 'Production Start Date',
						format: AM.app.globals.CommonDateControl,
						name: 'ProductionStartDate',
						invalidText: 'This value is not a valid date - it must be in the format DD-MM-YYYY',
						listeners: {
							'focus': function(me) {
								this.onTriggerClick();
								//alert(me.getSubmitValue());
							}
						},
						flex: 1,
						margins : '0 0 0 20'
					}]
				},{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					margin:'7 0 0 0',
					items: [
					assocManger,
					{
						xtype : 'radiogroup',
						fieldLabel: "Status",
						columns:[100,100],
						items: [
								 {boxLabel:'Active', name: 'ProcessStatus', inputValue:'0', checked: true},
								 {boxLabel:'InActive', name: 'ProcessStatus', inputValue:'1'}
								],
						flex: 1,
						margins : '0 0 0 20',
					}]
				},{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					margin:'7 0 0 0',
					items: [
					pleads,
					{
						 xtype : 'boxselect',								
						 fieldLabel: 'Approver',
						 id: 'Approver',
						 name: 'Approver',
						 // store: 'Approver',
						 queryMode: 'local',
						 displayField: 'Name', 
						 valueField: 'EmployID',
						 //allowBlank: false,
						 // width: '50.5%',
						 flex: 1,
						 emptyText: 'Select Approver...',
						 visible:false,
						 hidden: true,
						 margin:'0 0 0 41',
						 listeners: {
						  beforequery: function(queryEvent, eOpts) {
										queryEvent.combo.store.proxy.extraParams = {
												'ProcessID':AM.app.globals.processID,
										}
								  }
						},
					}]
				},{
					xtype: 'container',
					layout: {
						type: 'hbox',
					},
					margin:'2 0 10 0',
					items: [{
						xtype: 'displayfield',
						fieldLabel: 'Approved FTE',
						name: 'HeadCount',
						value: '0',
						width: '48.5%',
					}]
				},{
					xtype: 'tabpanel',
					activeTab: 0,
					items:[techGrid, attrGrid, processDescription]
				}],
	    		
		    	buttons : ['->', {
		        	   text : 'Save',
		        	   itemId:'ProcessSaveBtn',
					   visible: AM.app.globals.accessObj[0]['addEdit']['process']['visible'],
						hidden:AM.app.globals.accessObj[0]['addEdit']['process']['hidden'],
		        	   icon : AM.app.globals.uiPath+'resources/images/save.png',
		        	   handler : this.onCreate
		        	   
		           }, {
		        	   text : 'Cancel',
		        	   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
		        	   handler : this.onCancel
		           }
		     	]
    	   }
    	];
    			
		this.callParent(arguments);
	},
	
	onCreate :  function() {
		var form = this.up('form').getForm();
		var record = form.getRecord(), values = form.getValues();
		var status = false;
		
		// If process is inactive and no Employees inder that process
		if((values.ProcessStatus[0] == 1 && record.get('ProcessEmployees') == 0) )
			status = true;
		// If process is active
		if(values.ProcessStatus[0] == 0)
			status = true;
		
		if(status)
		{
			if (form.isValid()) 
			{
				var win = this.up('window');
				AM.app.getController("Process").onSaveClick(form);
				win.close();
			}
		}else{
			Ext.MessageBox.show({
				title: 'Alert',
				msg: record.get('ProcessEmployees')+' employee/s under this process, if you want to inactive, please transit them to HR pool.',
				buttons: Ext.Msg.OK
			});
		}
	},
	onCancel : function() {
		this.up('window').close();
	},
	onNewTechClick : function() {
		var form = this.up('form').getForm();
		var win = this.up('window');
		AM.app.getController('Process').dispNewTech(form);
	},
	onNewAttributes : function() {
		var form = this.up('form').getForm();
		var win = this.up('window');
		AM.app.getController('Process').dispNewProcessAttrs(form);
	},
	verticalCombo: function()
	{
		var form = this.up('form').getForm();
		var win = this.up('window');
    	win.fireEvent('selectVerticalCombo', form, this, win);
	},
	renderTip : function(val, meta, rec, rowIndex, colIndex, store) {
		meta.tdAttr = 'data-qtip="Drag and drop to re-order process attributes"';
		return val;
	}
});