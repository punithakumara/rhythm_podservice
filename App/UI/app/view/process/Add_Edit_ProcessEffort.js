Ext.define('AM.view.process.Add_Edit_ProcessEffort',{
	extend : 'Ext.window.Window',
	alias : 'widget.processAddEditEffort',
	
	title: 'Process Effort Details',
    
	fieldDefaults: {
	    labelAlign: 'left',
	    labelWidth: 90,
        anchor: '100%'
	},
	layout: 'fit',
	width: '30%',
    autoShow : true,
    modal: true,
	
    initComponent : function() {
    	 var currentTime = new Date();
		var now = currentTime.getFullYear();
		var nowMonth = currentTime.getMonth();
		var years = new Array();
		var i = 1;
		maxVal = 10;
		
		j = 2;
		for(y= now+maxVal;;y--){
			temp = ""+y;
			years.push([temp,temp]);
		
			if(j > maxVal){
				break;
			}
			j++;
		
		}
		
		for(y = now;;y--){
			
			temp = ""+y;
			years.push([temp,temp]);
			// years[y]=y;
			
			
			if(i > maxVal){
				break;
			}
			i++;
			
		
		}
		
		
		
		//console.log(years);
		var storeThn = new Ext.data.SimpleStore({
			fields: ['Year','abbr'],        
			data: years
		});
		
		
		
		this.items = [
    	   {
	    		xtype : 'form',
	    		itemId : 'addProcessFrm',
	    		bodyStyle : {
	    			border : 'none',
	    			display : 'block',
	    			float : 'left',
	    			padding : '10px'
	    		},
				
	    		items : [
							 {
								 xtype : 'hiddenfield',
								 name : 'Effort_ID',
								 fieldLabel : 'Effort_ID',
								 anchor: '100%'
							 },{
								 xtype : 'hiddenfield',
								 name : 'processID',
								 fieldLabel : 'ProcessID',
								 anchor: '100%'
							 }, {
								 xtype : 'displayfield',
								 name : 'Name',
								 //allowBlank: false,
								 readOnly: true,
								 fieldLabel : 'Name',
								 anchor: '100%'
							 },{
							 
								xtype: 'boxselect',
								name: 'Year',
								multiSelect: false,
								selectOnFocus: true,
							    fieldLabel: 'Month/Year',
								style:{
									float:'left',
									width:'200px'
								},
								//value:(now -1),
								allowBlank: false,
								emptyText: 'Select Year',
								store: storeThn,
								displayField: 'abbr',
								width: 200,
								valueField: 'Year',
								forceSelection: true,
							 },{
							 
								xtype: 'boxselect',
								name: 'Month',
								
								multiSelect: false,
								selectOnFocus: true,
								style:{
									float:'left',
									'margin-left':'5px',
								},
								allowBlank: false,
								emptyText: 'Select Month',
								store: new Ext.data.ArrayStore({
									fields: ['Month','abbr'],
									data  : [
											 ['1','January'], 
											 ['2','Febraury'], 
											 ['3','March'], 
											 ['4','April'],
											 ['5','May'],
											 ['6','June'],
											 ['7','July'],
											 ['8','August'],
											 ['9','September'],
											 ['10','October'],
											 ['11','November'],
											 ['12','December'],
											]
								}),
								displayField: 'abbr',
								width: 150,
								valueField: 'Month',
								forceSelection: true,
								
							 },
							{
								xtype: 'textfield',    
								name: 'Utilization', 
								allowBlank: false,
								maskRe:/[0-9.]/,
								fieldLabel : 'Utilization',
								style:{
									clear:'both',
									float:'left',
									'margin-left':'5px',
								}
							},
							{
								xtype: 'displayfield', 
								value: '%',
								style:{
									float:'left'
								}
							},{
								xtype: 'textfield',    
								name: 'Accuracy', 
								allowBlank: false, 
								maskRe:/[0-9.]/,
								fieldLabel : 'Accuracy',
								style:{
									clear:'both',
									float:'left',
									'margin-left':'5px',
								}
							},
							{
								xtype: 'displayfield', 
								value: '%',
								style:{
									float:'left'
								}
							},{
								xtype: 'numberfield',    
								name: 'Errors', 
								allowBlank: false, 
								fieldLabel : 'Errors',
								minValue: 1,
								style:{
									clear:'both',
									float:'left',
									'margin-left':'5px',
								}
							},{
								xtype: 'numberfield',    
								name: 'Escalation', 
								allowBlank: false, 
								fieldLabel : 'Escalations',
								style:{
									clear:'both',
									float:'left',
									'margin-left':'5px',
								},
								minValue: 1,
							},{
								xtype: 'numberfield',    
								name: 'Appreciation', 
								allowBlank: false, 
								fieldLabel : 'Appreciations',
								style:{
									clear:'both',
									float:'left',
									'margin-left':'5px',
								},
								minValue: 1,
							}
	    		],
	    		
		    	buttons : ['->', {
		        	   text : 'Save',
		        	   icon : AM.app.globals.uiPath+'resources/images/save.png',
		        	   handler : this.onaddEffort
		        	   
		           }, {
		        	   text : 'Cancel',
		        	   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
		        	   handler : this.onCancel
		           }
		     	]
    	   }
    	];
    			
		this.callParent(arguments);
	},
	
	onaddEffort :  function() {
		var form = this.up('form').getForm();

        if (form.isValid()) {
        	var win = this.up('window');
        	win.fireEvent('addProcesspecificEffort', form);
            win.close();
        }
	},
	
	onCancel : function() {
		this.up('window').close();
	}
});