Ext.define('AM.view.process.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.processList',

    title: 'Process List',
    
    store : 'Process',
    id : 'processGrid',
    layout : 'fit',
    viewConfig: {
  	  forceFit: true
  	},
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
    frame: true,
    width: "99%",
    height: 500,
    style: {
        border: "0px solid #157fcc",
        borderRadius:'0px'
      
    },
    initComponent: function() {

        this.columns = [ {
            header      : 'Name',
            dataIndex   : 'Name', 
            flex        : 1
        }, {
            header      : 'Client', 
            dataIndex   : 'Client_Name', 
            flex        : 1
        }, {
            header      : 'Vertical', 
            dataIndex   : 'Vertical_Name', 
            flex        : 1
        }, {
            header      : 'Approved FTE', 
            dataIndex   : 'HeadCount', 
            flex        : 1
        }, {
            header      : 'Production Start Date', 
            dataIndex   : 'ProductionStartDate', 
            flex        : 1,
        }, {
	        	text: 'Edit',
		        xtype: 'actioncolumn',
		        items: [{ 
						icon: 'resources/images/edit.png',
						tooltip : 'Edit',
						// disabled: AM.app.globals.accessObj[0]['addEdit']['process']['value'],
						// disabledCls:AM.app.globals.accessObj[0]['addEdit']['process']['disCls'],
						handler : this.onEdit
		                  }],
		          
		          

		          
		          }
        ];
        
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No records to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

        this.callParent(arguments);
    },
    
    tools : [{
		xtype: 'combobox',
		itemId : 'processStatusCombo',
		store: Ext.create('Ext.data.Store', {
				fields: ['status', 'processStatus'],
				data : [{'status': '0', 'processStatus': 'Active'}, {'status': '1', 'processStatus': 'InActive'}]
			}),
		displayField: 'processStatus', 
		valueField: 'status',
		value: '0',
		listeners: {
			select: function(combo){
				this.fireEvent('changeStatusCombo', combo);
			}
		}
	},{ 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        //fieldLabel : 'Search ',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }, {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'resources/images/add.png',
		handler: function(){
			AM.app.getController("Process").ProcessViewForm();
		},
		text : 'Add Process'
	}],
	onEdit : function(grid, rowIndex, colIndex){
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		AM.app.getController("Process").editProcess(grid, rowIndex, colIndex);
	},
});