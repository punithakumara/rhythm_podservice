Ext.define('AM.view.process.Add_Nrr_Process',{
	extend : 'Ext.window.Window',
	alias : 'widget.nrrProcessAddEdit',
	
	title: 'Add Process',
    	
	layout: 'fit',
	width : '30%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    //autoScroll: true,
    //overflow: 'scroll',
    modal: true,
    //y : 100,
	
    initComponent : function() {		
    	this.items = [
    	{
	    		xtype : 'form',
	    		itemId : 'nrrProcessAddEditFrm',
	    		fieldDefaults: {
	    		    labelAlign: 'left',
	    		    labelWidth: 130,
	    	        anchor: '100%'
	    		},
				fileUpload:true,
	    	    isUpload: true,
	    		bodyStyle : {
	    			border : 'none',
	    			display : 'block',
	    			float : 'left',
	    			padding : '10px'
	    		},
	    		
	    		items : [
							{
								xtype: 'hidden',
								allowBlank: true,
								name: 'ClientID',
								id: 'hiddenClientID',
								//flex: 1,
							},{
								xtype: 'hidden',
								allowBlank: true,
								name: 'VerticalID',
								id: 'hiddenVerticalID',
								//flex: 1,
							},{
								 xtype : 'textfield',
								 name : 'Name',
								 allowBlank: false,
								 listeners:{
								 scope: this,
								 'blur': function(text){
								   text.setValue(text.getValue().trim());
								 }
								 },
								 fieldLabel : 'Name',
								 anchor: '100%'
							 },
	    		]
    	   }
    	];
		
    	this.buttons = ['->', {
        	   text : 'Save',
        	   icon : AM.app.globals.uiPath+'resources/images/save.png',
        	   handler : this.onSave
           },{
        	   text : 'Cancel',
        	   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
        	   handler : this.onCancel
           }
     	];
    			
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',

	onSave :  function() {			
		var win = this.up('window');
		var form = win.down('form').getForm();
		if (form.isValid()){
			AM.app.getController('Process').onSaveNrrProcess(form, win)
		}
	},
	onCancel : function() {
		this.up('window').close();
	}		
});