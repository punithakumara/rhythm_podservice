
Ext.define('Ext.chart.theme.CustomCharts', {
    extend: 'Ext.chart.theme.Base',

    config: {
        axis: {
            stroke: '#7F8C8D'
        },
        colors: [ '#F08080', '#1ABC9C', '#800000' ]
    },

    constructor: function(config) {
        var titleLabel = {
                font: 'bold 18px Helvetica'
            },
            axisLabel = {
                fill: '#7F8C8D',
                font: '12px Helvetica',
                spacing: 2,
                padding: 5
            };

        this.callParent([Ext.apply(this.config, config,  {
            axisLabelLeft: axisLabel,
            axisLabelBottom: axisLabel,
            axisTitleLeft: titleLabel,
            axisTitleBottom: titleLabel
        })]);
    }
});

Ext.define('AM.view.dashboard.corPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.corpanel',
    requires:['AM.view.chart.CorChart'],
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	minHeight:275,
	 store: 'NrrRdrChart',

    initComponent: function() {
		var me=this;
		this.activeItem  = 0;
		var topBar = "";
		var filterLbl = "";
		var groupvertical	= '';

		Ext.apply(me, {
           store : Ext.create('AM.store.CorChart')
        });
		me.store.getProxy().extraParams = {'groupvertical': me.groupverticalcombo}
		
		this.tbar = [{
     	  	xtype: 'fieldcontainer',
     	  	layout: 'hbox',
     	  	items : [{
				xtype: 'label',
				forId: "filterLabel",
				width: 398,
				text: ''
			},{
				xtype: 'button',
				name: 'noDataImg',
				tooltip:'Select Vertical',
				cls:'buttonImg',
				listeners: {
					el: {
						click: function() {
							if(Ext.getCmp('GroupVertical_combo') != undefined)
							{
								me.groupvertical = Ext.getCmp('GroupVertical_combo').getValue();							
								var comboVal = (me.query('combobox')[0].getValue()!=null)? me.query('combobox')[0].getValue() : Ext.getCmp('GroupVertical_combo').getValue();
								me.query('combobox')[0].setValue(comboVal);
							}							
							me.getLayout().setActiveItem(1);
							topBar = this;
							this.hide();
							
							filterLbl = me.getDockedItems()[0].items.items[0].items.items[0];
							filterLbl.hide();
						}
					}
				},
			}]
		}];
		
		this.items =[{
			theme: 'CustomCharts',
			store: me.store,
			xtype: 'ChartCor'
        },{
			layout: "vbox",
			width:400,
			height:258,
			autoScroll: true,
			items : [{
				xtype: 'boxselect',
				multiSelect: true,
				width:365,
				labelWidth:90,
				fieldLabel: 'Client',
				store: 'LogInViewerClients',
				displayField: 'client_name',
				valueField: 'client_id',
				emptyText:"All Clients",
				style:{
					margin:'25px 0 0 30px'
				},
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						var string = me.query('combobox')[0].getRawValue();
						var pos = string.lastIndexOf("; ");
						string = string.substring(pos+1);
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'VerticalID': (string == 'TheoremIndia Ltd') ? '':me.query('combobox')[0].getValue(),
							'filterName' : queryEvent.combo.displayField
						}
					},
				}
			},{
				layout: 'hbox',
				style:{
					margin:'0 0 0 108px'
				},
				items : [{
					xtype: 'button',
					text : 'Submit',
					style:{
						margin:'25px 0 0 30px'
					},
					handler: function(){
						me.store.load({
							params:{'hasNoLimit':"1",'ClientID': (me.query('combobox')[0].getValue()!="") ? Ext.encode(me.query('combobox')[0].getValue()) : ''},
							callback:function(records, options, success) {
								me.getEl().unmask("Loading...","x-mask-loading");
								if (success) 
								{
									this.activeItem = 0;
									me.getLayout().setActiveItem(0);
								}
								else
								{
									this.activeItem = 2;
									me.getLayout().setActiveItem(2);
								}
							}
						});
						
						topBar.show();
						
						var clients = me.query('combobox')[0].getValue();
						var clientNames = me.query('combobox')[0].getRawValue();
						var stringValue = AM.app.getController('Dashboard').showGraphSelectionTitle(clientNames, null, null);							
						filterLbl.setText('<ext title="'+stringValue[1]+'">'+stringValue[0]+'</ext>', false);
						filterLbl.show();
					}
				},{
					xtype: 'button',
					text : 'Cancel',
					style:{
						margin:'25px 0 0 20px'
					},
					handler: function(){
						topBar.show();
						me.getLayout().setActiveItem(this.activeItem);
						filterLbl.show();															
					}
				}]
			}]
        },{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }]
    	
		this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
				{
					obj.down('toolbar').setVisible(false);
					obj.getLayout().setActiveItem(2);
				}
				else
					obj.getLayout().setActiveItem(0);
			}); 
			var nrrrdrpanel = obj.items.items[1].items.items[0];
			nrrrdrpanel.setValue(obj.groupverticalcombo);
		}
	}
});