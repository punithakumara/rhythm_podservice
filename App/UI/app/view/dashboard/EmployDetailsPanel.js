Ext.define('AM.view.dashboard.EmployDetailsPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.EmployDetailsPanel',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me=this;
		this.activeItem  = 0;

		var empStore = Ext.create('Ext.data.Store', {
			fields: ['Name', 'Value'],
			autoLoad: false,
			remoteSort: true,
			proxy: {
				type: 'rest',
				url : AM.app.globals.appPath+'index.php/charts/employDetailsPopup/',
				reader :  {
					type: 'json',
					root : 'rows',
				}
			}
		});

		Ext.apply(me, {
			store : Ext.create('AM.store.EmployeeDetails', {
				remoteSort: true
			})
        });
		
		this.items =[{
			xtype: 'grid',
			store : me.store,
			viewConfig: {
				deferEmptyText:false,
				emptyText   : '<div align="center">No Records Found.</div>',
				listeners: {
					itemclick: function(dataview, record, item, index, e) {
						me.getLayout().setActiveItem(1);
						empStore.proxy.extraParams = {EmployID: record.data.employee_id};
						empStore.load();
					}
				},
				getRowClass: function(record, index) {
					return 'cursor_hand';
                }
			},
			autoScroll: true,
			columns: [{
				header: 'Employee ID',
				dataIndex: 'company_employ_id', 
				flex: 1,
				menuDisabled:true
			},{
				header: 'Name', 
				dataIndex: 'Employ_Name', 
				flex: 2,
				menuDisabled:true
			}],
			tbar: [{
				xtype: 'textfield',
				emptyText: 'Employee ID / Name',
				style: {'margin-left' : '60px'},
				listeners: {
					specialkey: function(f,e){
						var txtVal = Ext.util.Format.trim(f.getValue());
						if (e.getKey() == e.ENTER) {
							if(txtVal != ''){
								me.store.proxy.extraParams = {searchKey: txtVal};
								me.store.load();
							}
						}
					}
				}
			},{
				text: 'Search',
				handler : function(obj) {
					var txtVal = Ext.util.Format.trim(obj.prev().getValue());
					if(txtVal != ''){
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
				}
			}]
        },{
			xtype: 'grid',
			viewConfig: {
				enableTextSelection: true
			},
			width: '100%',
			autoScroll: true,
			store: empStore,
			hideHeaders: true,
			columns: [{ header:"Name",dataIndex:"Name",flex:1}, { header:"Value",dataIndex:"Value",flex:2, tdCls:'wrap-text'}],
			tbar: ['->',
			{
				text: 'History',
				handler : function(obj) {
					AM.app.getController('Dashboard').popupContents(empStore.proxy.extraParams.EmployID, 'BillableEmployeeHistory&popup');
				}
			},{
				text: 'Back',
				handler : function(obj) {
					me.getLayout().setActiveItem(0);
				}
			}]
		}];
		
        this.callParent(arguments);
    }
});