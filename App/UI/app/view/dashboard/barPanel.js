Ext.define('AM.view.dashboard.barPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.dashboardbarpanel',
    requires:['AM.view.chart.PieChart'],	
	layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	minHeight:275,
    initComponent: function() {
		var me=this;
		this.activeItem  = 0;
		var topBar = "";
		var filterLbl = "";
		
    	Ext.apply(me, {
           store : Ext.create('AM.store.Barchart')
        });
		me.store.getProxy().extraParams = {'grouppod': me.grouppodcombo}
		
		this.tbar = [{
     	  	xtype: 'fieldcontainer',
     	  	layout: 'hbox',
     	  	items : [{
				xtype: 'label',
				forId: "filterLabel",
				width: 398,
				text: ''
			},{
				xtype: 'button',
				name: 'noDataImg',
				tooltip:'Select Pod',
				cls:'buttonImg',
				listeners: {
					el: {
						click: function() {
							if(Ext.getCmp('GroupPod_combo') != undefined)
							{
								me.grouppod = Ext.getCmp('GroupPod_combo').getValue();							
								var comboVal = (me.query('combobox')[0].getValue()!=null)? me.query('combobox')[0].getValue() : Ext.getCmp('GroupPod_combo').getValue();
								me.query('combobox')[0].setValue(comboVal);
							}
							me.getLayout().setActiveItem(1);
							topBar = this;
							this.hide();
							filterLbl = me.getDockedItems()[0].items.items[0].items.items[0];
							filterLbl.hide();
						}
					}
				},
			}]
       }];
		
		// to here	
		this.items =[{
			store : me.store,
			xtype:'chartPieChart'
        },{
			layout: "vbox",
			width:400,
			height:258,
			autoScroll: true,
			style: {
				overflow: 'auto'
			},
			items : [{
				xtype: 'boxselect',
				multiSelect: true,
				width:365,
				labelWidth:90,
				id:'resbillablepodid',
            	name: 'podid',
				fieldLabel: 'Pod',
				store: 'Pod',
				// queryMode: 'remote',
				displayField: 'name',
				minChars:0,
				valueField: 'pod_id',
				emptyText:"Select Pod",
				style:{
					margin:'25px 0 0 30px'
				},
				listeners	: {
					beforequery: function(queryEvent) {
						AM.app.getStore('Pod').getProxy().extraParams = {'hasNoLimit':'1', 'filterName' : queryEvent.combo.displayField};
					}
				}
			},{
				layout: 'hbox',
				style:{
					margin:'0 0 0 108px'
				},
				items : [{
					xtype: 'button',
					text : 'Submit',
					style:{
						margin:'25px 0 0 30px'
					},
					handler	: function() {
						me.store.load( {
							params	: {
								'hasNoLimit'	: "1", 
								'PodsID': (me.query('combobox')[0].getValue()!="") ? Ext.encode(me.query('combobox')[0].getValue()) : '',
								//'groupvertical'	: me.query('combobox')[0].getValue()
							},
							callback	: function(records, options, success) {
								me.getEl().unmask("Loading...","x-mask-loading");
								if(records.length > 0) 
								{
									this.activeItem = 0;
									me.getLayout().setActiveItem(0);
								} 
								else 
								{
									this.activeItem = 2;
									me.getLayout().setActiveItem(2);
								}
							}
						} );
						topBar.show();
						
						//var deptName = me.query('combobox')[0].getRawValue();
						var pods = me.query('combobox')[0].getValue();
						var podNames = me.query('combobox')[0].getRawValue();
						var stringValue = AM.app.getController('Dashboard').showGraphSelectionTitle(podNames);							
						filterLbl.setText('<ext title="'+stringValue[1]+'">'+stringValue[0]+'</ext>', false);
						filterLbl.show();
					}
				},{
					xtype: 'button',
					text : 'Cancel',
					style:{
						margin:'25px 0 0 20px'
					},
					handler: function(){
						topBar.show();
						me.getLayout().setActiveItem(this.activeItem);
						filterLbl.show();
					}
				},{
					xtype:'button',
					//id: "ClearFilter",
					anchor: '5%',
					style	: { margin	: '25px 0 0 20px' },
					text:'Clear Filters',
					icon: AM.app.globals.uiPath+'resources/images/reset.png',
					handler : function() {
						 AM.app.getController('Dashboard').resetnrrRdrAllFilter();
					}
				}]
			}]
        },{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }]
		
        this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
				{
					obj.down('toolbar').setVisible(false);
					obj.getLayout().setActiveItem(2);
				}
				else
					obj.getLayout().setActiveItem(0);
			}); 
			var billablenonbillablepanel = obj.items.items[1].items.items[0];
			billablenonbillablepanel.setValue(obj.grouppodcombo);
		}
	}
	
});