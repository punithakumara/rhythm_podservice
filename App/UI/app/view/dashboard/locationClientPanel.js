Ext.define('Ext.chart.theme.CustomClientCharts', {
    extend: 'Ext.chart.theme.Base',

    config: {
        axis: {
            stroke: '#7F8C8D'
        },
        colors: [ '#FF4500', '#9370DB', '#B22222', '#8B008B', '#800000', '#FFD700', '#0000FF', '#4682B4', '#778899', '#2F4F4F', '#BDB76B', '#DC143C', '#191970', '#A9A9A9', '#00CED1'  ]
    },

    constructor: function(config) {
        var titleLabel = {
                font: 'bold 18px Helvetica'
            },
            axisLabel = {
                fill: '#7F8C8D',
                font: '12px Helvetica',
                spacing: 2,
                padding: 5
            };

        this.callParent([Ext.apply(this.config, config,  {
            axisLabelLeft: axisLabel,
            axisLabelBottom: axisLabel,
            axisTitleLeft: titleLabel,
            axisTitleBottom: titleLabel
        })]);
    }
});
Ext.define('AM.view.dashboard.locationClientPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.dashboardlocationclientpanel',
    requires:['AM.view.chart.PieChart'],
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	minHeight:275,
    initComponent: function() {
		var me = this;
		var topBar = "";
		
		var grouppod = '';
		Ext.apply(me, {
		   store : Ext.create('AM.store.LocationClientChart')
		});	
		me.store.getProxy().extraParams = {'grouppod': me.grouppodcombo}
		this.activeItem  = 0;
		
		this.tbar = [{
			xtype: 'fieldcontainer',
			layout: 'hbox',
			items : [{
				xtype: 'label',
				forId: "filterLabel",
				width: 398,
				text: ''
			}, {
				xtype: 'button',
				name: 'noDataImg',
				tooltip:'Select PODS',
				cls:'buttonImg',
				listeners	: {
					el	: {
						click	: function() {
							if(Ext.getCmp('GroupPod_combo') != undefined)
							{
								me.grouppod = Ext.getCmp('GroupPod_combo').getValue();							
								var comboVal = (me.query('combobox')[0].getValue()!=null)? me.query('combobox')[0].getValue() : Ext.getCmp('GroupPod_combo').getValue();
								me.query('combobox')[0].setValue(comboVal);
							}
							me.getLayout().setActiveItem(1);
							topBar = this;
							this.hide();
							filterLbl = me.getDockedItems()[0].items.items[0].items.items[0];
							filterLbl.hide();
						}
					}
				},
			}/*,{
				xtype: 'button',
				name: 'noDataImg',
				tooltip:'Filter',
				cls:'buttonImg',
				listeners: {
					el: {
						click: function() {
							
							if(Ext.getCmp('GroupVertical_combo') != undefined)
							{
								me.groupvertical = Ext.getCmp('GroupVertical_combo').getValue();							
								var comboVal = (me.query('combobox')[0].getValue()!=null)? me.query('combobox')[0].getValue() : Ext.getCmp('GroupVertical_combo').getValue();
								me.query('combobox')[0].setValue(comboVal);
							}							
							me.getLayout().setActiveItem(1);
							topBar = this;
							this.hide();
							filterLbl = me.getDockedItems()[0].items.items[0].items.items[0];
							filterLbl.hide();
						}
					}
				},
			}*/]
		}];
	
		this.items =[{
			theme: 'CustomClientCharts',
			store : me.store,
            xtype: 'chartPieChart',
        },{
			layout: "vbox",
			width:400,
			height:258,
			autoScroll: true,
			items : [{
				xtype		: 'boxselect',
				multiSelect	: true,
				width		: 365,
				labelWidth	: 90,
				id:'locClientpodId',
				name: 'pod',				
				fieldLabel: 'Pod',
				store: 'Pod',
				minChars:0,
				displayField: 'name',
				valueField: 'pod_id',
				emptyText:"All PODS",
				style		: { margin	: '25px 0 0 30px' },
				listeners	: {
					beforequery	: function(queryEvent, eOpts) {
						var string = me.query('combobox')[0].getRawValue();
						var pos = string.lastIndexOf("; ");
						string = string.substring(pos+1);
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'PodID': (string == 'TheoremIndia Ltd') ? '':me.query('combobox')[0].getValue(),
							'filterName' : queryEvent.combo.displayField
						}
					},
				}
			},{
				layout: 'hbox',
				style:{
					margin:'0 0 0 108px'
				},
				items : [{
					xtype: 'button',
					text : 'Submit',
					style:{
						margin:'25px 0 0 30px'
					},
					handler: function()
					{
						me.store.load({
							params:{'hasNoLimit':"1",'grouppod':me.query('combobox')[0].getValue()},
							callback:function(records, options, success) {
									me.getEl().unmask("Loading...","x-mask-loading");
								if (success) {
									me.getLayout().setActiveItem(0);
								}else{
									me.getLayout().setActiveItem(2);
								}
							}
						});
						
						topBar.show();
						
						var string = me.query('combobox')[0].getRawValue();
						var pos = string.lastIndexOf("; ");
						string = string.substring(pos+1);
						filterLbl.setText("Department : "+string);
						filterLbl.show();
					}
				},{
					xtype: 'button',
					text : 'Cancel',
					style:{
						margin:'25px 0 0 20px'
					},
					handler: function(){
						topBar.show();
						me.getLayout().setActiveItem(0);
						
						filterLbl.show();
					}
				}, {
					xtype:'button',
					//id: "ClearFilter",
					anchor: '5%',
					style:{
						margin:'25px 0 0 20px'
					},
					text:'Clear Filters',
					icon: AM.app.globals.uiPath+'resources/images/reset.png',
					handler : function() {
						 AM.app.getController('Dashboard').resetLocationClientAllFilter();
					}
				}]
			}]
		},{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }]
		
        this.callParent(arguments);
    },
	
    listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
				{
					// obj.down('toolbar').setVisible(false);
					obj.getLayout().setActiveItem(2);
				}
				else
					obj.getLayout().setActiveItem(0);
			}); 
			
			var locclientpanel = obj.items.items[1].items.items[0];
			locclientpanel.setValue(obj.grouppodcombo);
		}
	}
});