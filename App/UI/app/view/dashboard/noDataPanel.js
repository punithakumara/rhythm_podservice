Ext.define('AM.view.dashboard.noDataPanel' ,{
    extend: 'Ext.container.Container',
    alias: 'widget.nodatapanel',
	layout : {
		type : 'vbox',
	},
    loadMask: true,
    autoCreate: true,
    initComponent: function() {
	
		this.items =[{
			xtype: 'image',
			style: {
				margin:"20px 0 0 90px"
			},
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
		}]
		
		this.callParent(arguments);
    }
});