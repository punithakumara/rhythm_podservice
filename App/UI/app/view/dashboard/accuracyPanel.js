Ext.define('AM.view.dashboard.accuracyPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.accuracypanel',
    requires:['AM.view.chart.LineChart'],
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	minHeight:275,
    initComponent: function() {
		var me=this;
		this.activeItem  = 0;
		var topBar = "";
		var filterLbl = "";
		
    	Ext.apply(me, {
           store : Ext.create('AM.store.AccuracyChart')
        });
		me.store.getProxy().extraParams = {'groupvertical': me.groupverticalcombo}
		
		this.tbar = [{
     	  	xtype: 'fieldcontainer',
     	  	layout: 'hbox',
     	  	items : [{
				xtype: 'label',
				forId: "filterLabel",
				width: 398,
				text: ''
			},{
				xtype: 'button',
				name: 'noDataImg',
				tooltip:'Filter',
				cls:'buttonImg',
				listeners: {
					el: {
						click: function() {
							if(Ext.getCmp('GroupVertical_combo') != undefined)
							{
								me.groupvertical = Ext.getCmp('GroupVertical_combo').getValue();							
								var comboVal = (me.query('combobox')[0].getValue()!=null)? me.query('combobox')[0].getValue() : Ext.getCmp('GroupVertical_combo').getValue();
								me.query('combobox')[0].setValue(comboVal);
							}
							me.getLayout().setActiveItem(1);
							topBar = this;
							this.hide();
							
							filterLbl = me.getDockedItems()[0].items.items[0].items.items[0];
							filterLbl.hide();
						}
					}
				},
			}]
       }];
		
		// to here	
		this.items =[{
			store : me.store,
			xtype:'chartLineChart'
        },{
			layout: "vbox",
			width:400,
			height:258,
			autoScroll: true,
			items : [{
				xtype: 'boxselect',
				multiSelect: false,
				width:365,
				labelWidth:90,
				store: 'GroupVerticalCombo',
				queryMode: 'remote',
				minChars:0,
				displayField: 'Name', 
				valueField: 'VerticalID',
				fieldLabel: 'Department',		
				style:{
					margin:'25px 0 0 30px'
				},
				listeners: {
					'change': function(combo) {
						if(me.getEl() != undefined){
							me.query('combobox')[1].clearValue();
							me.query('combobox')[2].clearValue();
							AM.app.globals.dashboardChartClient['GradeChart'] = me.query('combobox')[0].getValue();
							AM.app.globals.dashboardChartProcess['GradeChart'] = "";
							
							Ext.getStore('LogInViewerClients').load({
								params:{'comboProcess':'1','hasNoLimit':"1",'VerticalID':combo.getValue()}
							});
							me.query('combobox')[1].bindStore("LogInViewerClients");
							if(me.query('combobox')[0].getValue()) me.query('combobox')[1].setDisabled(false);
							else me.query('combobox')[1].setDisabled(true);
						}
					}
				}
			},{
				xtype: 'boxselect',
				multiSelect: true,
				width:365,
				labelWidth:90,
				fieldLabel: 'Client',
				store: 'LogInViewerClients',
				displayField: 'Client_Name',
				valueField: 'ClientID',
				emptyText:"All Clients",
				style:{
					margin:'25px 0 0 30px'
				},
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						var string = me.query('combobox')[0].getRawValue();
						var pos = string.lastIndexOf("; ");
						string = string.substring(pos+1);
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'VerticalID': (string == 'TheoremIndia Ltd') ? '':me.query('combobox')[0].getValue(),
							'filterName' : queryEvent.combo.displayField
						}
					},
					'change': function(combo) {
						if(me.getEl() != undefined){
							me.query('combobox')[2].clearValue();
							AM.app.globals.dashboardChartClient['GradeChart'] = me.query('combobox')[0].getValue();
							AM.app.globals.dashboardChartProcess['GradeChart'] = "";
							var clientIds = me.query('combobox')[1].getValue();
							
							Ext.getStore('Process').load({
								params:{'comboProcess':'1','hasNoLimit':"1",'VerticalID': me.query('combobox')[0].getValue(),'ClientID' : (combo.getValue()!="") ? Ext.encode(combo.getValue()) : '', 'multigraph' :"1"}
							});
							me.query('combobox')[2].bindStore("Process");
							if(clientIds.length == 0) me.query('combobox')[2].setDisabled(true);
							else me.query('combobox')[2].setDisabled(false);
						}
					}
				}
			},{
				xtype: 'boxselect',
				multiSelect: true,
				width:365,
				labelWidth:90,
				fieldLabel: 'Process',
				displayField: 'Name',
				valueField: 'processID',
				emptyText:"All Process",
				style:{
					margin:'25px 0 0 30px'
				},
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						var string = me.query('combobox')[0].getRawValue();
						var pos = string.lastIndexOf("; ");
						string = string.substring(pos+1);
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'VerticalID': (string == 'TheoremIndia Ltd') ? '':me.query('combobox')[0].getValue(),
							'ClientID': (me.query('combobox')[1].getValue()!="") ? Ext.encode(me.query('combobox')[1].getValue()) : '',
							'filterName' : queryEvent.combo.displayField
						}
					},
				}
			},{
				layout: 'hbox',
				style:{
					margin:'0 0 0 108px'
				},
				items : [{
					xtype: 'button',
					text : 'Submit',
					style:{
						margin:'25px 0 0 30px'
					},
					handler: function(){						
						if(me.query('combobox')[2].getValue() == null){	
							me.store.load({
								params:{'hasNoLimit':"1",'ClientID': (me.query('combobox')[1].getValue()!="") ? Ext.encode(me.query('combobox')[1].getValue()) : '','groupvertical':me.query('combobox')[0].getValue()},
								callback:function(records, options, success){
									me.getEl().unmask("Loading...","x-mask-loading");
									if(success) {
										me.getLayout().setActiveItem(0);
									} else {
										me.getLayout().setActiveItem(2);
									}
								}
							});
						} else {
							me.store.load({
								params:{'hasNoLimit':"1",'ClientID': (me.query('combobox')[1].getValue()!="") ? Ext.encode(me.query('combobox')[1].getValue()) : '','processID' : (me.query('combobox')[2].getValue()!="") ? Ext.encode(me.query('combobox')[2].getValue()) : '','groupvertical':me.query('combobox')[0].getValue()},
								callback:function(records, options, success) {
										me.getEl().unmask("Loading...","x-mask-loading");
									if (success) {
										me.getLayout().setActiveItem(0);
									}else{
										me.getLayout().setActiveItem(2);
									}
								}
							});
						}
						topBar.show();
						
						var deptName = me.query('combobox')[0].getRawValue();
						var clients = me.query('combobox')[1].getValue();
						var clientNames = me.query('combobox')[1].getRawValue();
						var processes = me.query('combobox')[2].getValue();
						var processNames = me.query('combobox')[2].getRawValue();
						var stringValue = AM.app.getController('Dashboard').showGraphSelectionTitle(deptName,clients,clientNames,processes,processNames);							
						filterLbl.setText('<ext title="'+stringValue[1]+'">'+stringValue[0]+'</ext>', false);
						filterLbl.show();
					}
				},{
					xtype: 'button',
					text : 'Cancel',
					style:{
						margin:'25px 0 0 20px'
					},
					handler: function(){
						topBar.show();
						
						me.store.load({
							params:{'hasNoLimit':"1",'ClientID': (me.query('combobox')[1].getValue()!="") ? Ext.encode(me.query('combobox')[1].getValue()) : '','processID' : (me.query('combobox')[2].getValue()!="") ? Ext.encode(me.query('combobox')[2].getValue()) : '','groupvertical':me.query('combobox')[0].getValue()},
							callback:function(records, options, success) {
									me.getEl().unmask("Loading...","x-mask-loading");
								if (success) {
									me.getLayout().setActiveItem(0);
									var deptName = me.query('combobox')[0].getRawValue();
									var clients = me.query('combobox')[1].getValue();
									var clientNames = me.query('combobox')[1].getRawValue();
									var processes = me.query('combobox')[2].getValue();
									var processNames = me.query('combobox')[2].getRawValue();									
									var stringValue = AM.app.getController('Dashboard').showGraphSelectionTitle(deptName,clients,clientNames,processes,processNames);									
									filterLbl.setText('<ext title="'+stringValue[1]+'">'+stringValue[0]+'</ext>', false);
									filterLbl.show();
								}else{
									
									me.query('combobox')[1].setValue(null);
									me.query('combobox')[2].setValue(null);																
									var deptName = me.query('combobox')[0].getRawValue();
									var clients = me.query('combobox')[1].getValue();
									var clientNames = me.query('combobox')[1].getRawValue();
									var processes = me.query('combobox')[2].getValue();
									var processNames = me.query('combobox')[2].getRawValue();									
									var stringValue = AM.app.getController('Dashboard').showGraphSelectionTitle(deptName,clients,clientNames,processes,processNames);									
									filterLbl.setText('<ext title="'+stringValue[1]+'">'+stringValue[0]+'</ext>', false);
									filterLbl.show();
									
									me.store.load({
										params:{'hasNoLimit':"1",'ClientID': (me.query('combobox')[1].getValue()!="") ? Ext.encode(me.query('combobox')[1].getValue()) : '','processID' : (me.query('combobox')[2].getValue()!="") ? Ext.encode(me.query('combobox')[2].getValue()) : '','groupvertical':me.query('combobox')[0].getValue()},
										callback:function(records, options, success) {
											me.getEl().unmask("Loading...","x-mask-loading");
											if (success) {
												me.getLayout().setActiveItem(0);
											}else{
												me.getLayout().setActiveItem(2);
											}
										}
									});
									
								}
							}
						});															
					}
				}]
			}]
        },{
            xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        }];
        this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
				{
					obj.down('toolbar').setVisible(false);
					obj.getLayout().setActiveItem(2);
				}
				else
					obj.getLayout().setActiveItem(0);
			}); 
			var  accuracypanel = obj.items.items[1].items.items[0];
			accuracypanel.setValue(obj.groupverticalcombo);
		}
	}
	
});