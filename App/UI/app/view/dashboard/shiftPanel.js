

/**
 * This class provides a dataview-based chart legend.
 */
Ext.define('Ext.chart.legend.Legend', {
    extend: 'Ext.chart.legend.LegendBase',
    alternateClassName: 'Ext.chart.Legend',
    xtype: 'legend',
    alias: 'legend.dom',
    type: 'dom',
    isLegend: true,
    isDomLegend: true,
 
    config: {
        /**
         * @cfg {Array} 
         * The rect of the legend relative to its container.
         */
        rect: null,
 
        /**
         * @cfg {Boolean} toggleable
         * `true` to allow series items to have their visibility
         * toggled by interaction with the legend items.
         */
        toggleable: true
 
        /**
         * @cfg {Ext.chart.legend.store.Store} store
         * The {@link Ext.chart.legend.store.Store} to bind this legend to.
         * @private
         */
    },
 
    baseCls: Ext.baseCSSPrefix + 'legend',
 
    horizontalCls: Ext.baseCSSPrefix + 'legend-horizontal',
    verticalCls: Ext.baseCSSPrefix + 'legend-vertical',
 
    toggleItem: function (index) {
        if (!this.getToggleable()) {
            return;
        }
        var store = this.getStore(),
            disabledCount = 0, disabled,
            canToggle = true,
            i, count, record;
 
        if (store) {
            count = store.getCount();
            for (i = 0; i < count; i++) {
                record = store.getAt(i);
                if (record.get('disabled')) {
                    disabledCount++;
                }
            }
            canToggle = count - disabledCount > 1;
 
            record = store.getAt(index);
            if (record) {
                disabled = record.get('disabled');
                if (disabled || canToggle) {
                    // This will trigger AbstractChart.onLegendStoreUpdate.
                    record.set('disabled', !disabled);
                }
            }
        }
    },
 
    onResize: function (width, height, oldWidth, oldHeight) {
        var me = this,
            chart = me.chart;
 
        if (!me.isConfiguring) {
            if (chart) {
                chart.scheduleLayout();
            }
        }
    }
 
});


Ext.define('Ext.chart.theme.SeCustomCharts', {
    extend: 'Ext.chart.theme.Base',

    config: {
        axis: {
            stroke: '#7F8C8D'
        },
        colors: [ '#1ABC9C', '#F1C40F', '#3498DB', '#C0392B', '#9B59B6', '#f8e3c4', '#fe6dbc', '#009468', '#ed1c24', '#ff9232', '#c7d7c0', '#44002b', '#d3e9fe', '#e46d69','#deccad','#FFFF00','#006400','#7FFF00','#00FA9A','#C71585' ]
    },

    constructor: function(config) {
        var titleLabel = {
                font: 'bold 18px Helvetica'
            },
            axisLabel = {
                fill: '#7F8C8D',
                font: '12px Helvetica',
                spacing: 2,
                padding: 5
            };

        this.callParent([Ext.apply(this.config, config,  {
            axisLabelLeft: axisLabel,
            axisLabelBottom: axisLabel,
            axisTitleLeft: titleLabel,
            axisTitleBottom: titleLabel
        })]);
    }
});

Ext.define('AM.view.dashboard.shiftPanel' ,{
	extend			: 'Ext.Panel',
    alias			: 'widget.dashboardshiftpanel',
    requires		: ['AM.view.chart.PieChart'],
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask		: true,
    autoCreate		: true,
	minHeight		: 275,
    initComponent	: function() {
    	var me				= this;
    	this.activeItem 	= 0;
    	var grouppod	= '';
    	
    	Ext.apply(me, { store : Ext.create('AM.store.ShiftChart') } );
    	me.store.getProxy().extraParams = {'grouppod': me.grouppodcombo}

    	this.tbar	= [ {
    		xtype	: 'fieldcontainer',
			layout	: 'hbox',
			items 	: [ {
				xtype: 'label',
				forId: "filterLabel",
				width: 398,
				text: ''
			},{
				xtype: 'button',
				name: 'noDataImg',
				tooltip:'Select Pod',
				cls:'buttonImg',
				listeners	: {
					el	: {
						click	: function() {
							if(Ext.getCmp('GroupPod_combo') != undefined)
							{
								me.grouppod = Ext.getCmp('GroupPod_combo').getValue();							
								var comboVal = (me.query('combobox')[0].getValue()!=null)? me.query('combobox')[0].getValue() : Ext.getCmp('GroupPod_combo').getValue();
								me.query('combobox')[0].setValue(comboVal);
							}
							me.getLayout().setActiveItem(1);
							topBar = this;
							this.hide();
							filterLbl = me.getDockedItems()[0].items.items[0].items.items[0];
							filterLbl.hide();
						}
					}
				},
			} ]
		} ],

        this.items	= [ {
			theme: 'SeCustomCharts',
       		store	: me.store,
       		xtype	:'chartPieChart'
       	}, {
       		layout	: "vbox",
			width:400,
			height:258,
			autoScroll: true,
			items	: [{
				xtype		: 'boxselect',
				multiSelect	: true,
				width		: 365,
				labelWidth	: 90,
				id:'shiftPodId',
				name: 'pod',				
				fieldLabel: 'Pod',
				store: 'Pod',
				minChars:0,
				displayField: 'name',
				valueField: 'pod_id',
				emptyText:"All PODS",
				style		: { margin	: '25px 0 0 30px' },
				listeners	: {
					beforequery	: function(queryEvent, eOpts) {
						var string = me.query('combobox')[0].getRawValue();
						var pos = string.lastIndexOf("; ");
						string = string.substring(pos+1);
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'PodID': (string == 'TheoremIndia Ltd') ? '':me.query('combobox')[0].getValue(),
							'filterName' : queryEvent.combo.displayField
						}
					},
				}
			}, {
				layout	: 'hbox',
				style	: { margin	: '0 0 0 108px' },
				items	: [ {
					xtype	: 'button',
					text 	: 'Submit',
					style	: { margin	:'25px 0 0 30px' },
					handler	: function() {
						me.store.load( {
							params	: {
								'hasNoLimit'	: "1", 
								'PodId': (me.query('combobox')[0].getValue()!="") ? Ext.encode(me.query('combobox')[0].getValue()) : '',
							},
							callback	: function(records, options, success) {
								me.getEl().unmask("Loading...","x-mask-loading");
								if(success) 
								{
									this.activeItem = 0;
									me.getLayout().setActiveItem(0);
								} 
								else 
								{
									this.activeItem = 2;
									me.getLayout().setActiveItem(2);
								}
							}
						} );
						topBar.show();
						
						var pods = me.query('combobox')[0].getValue();
						var clientNames = me.query('combobox')[0].getRawValue();
						var stringValue = AM.app.getController('Dashboard').showGraphSelectionTitle(clientNames, null, null);							
						filterLbl.setText('<ext title="'+stringValue[1]+'">'+stringValue[0]+'</ext>', false);
						filterLbl.show();
					}
				}, {
					xtype	: 'button',
					text 	: 'Cancel',
					style	: { margin	: '25px 0 0 20px' },
					handler	: function() {
						topBar.show();
						me.getLayout().setActiveItem(this.activeItem);
						filterLbl.show();
					}
				},{
					xtype:'button',
					//id: "ClearFilter",
					anchor: '5%',
					text:'Clear Filters',
					style	: { margin	: '25px 0 0 20px' },
					icon: AM.app.globals.uiPath+'resources/images/reset.png',
					handler : function() {
						 AM.app.getController('Dashboard').resetShiftAllFilter();
					}
				} ]
			} ]
		}, {
			xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
        } ]
        this.callParent(arguments);
    },
    listeners	: {
    	render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
				{
					obj.down('toolbar').setVisible(false);
					obj.getLayout().setActiveItem(2);
				}
				else
					obj.getLayout().setActiveItem(0);
			} ); 
			var chartshiftwisemp = obj.items.items[1].items.items[0];
			chartshiftwisemp.setValue(obj.grouppodcombo);
		}
    }
} );