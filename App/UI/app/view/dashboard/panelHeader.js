Ext.define('AM.view.dashboard.panelHeader' ,{
    extend: 'Ext.Panel',
    alias: 'widget.dashboardpanelheader',
    layout : 'fit',
	id: 'dyngridpanel',
    loadMask: true,
    autoCreate: true,
	// requires:['Ext.data.*', 'Ext.dd.*'],
    disableSelection: false,
	stripeRows : true,
	margins    : 10,
	border : false,
	style : {
		border : '1px solid #CCCCCC',
		'border-bottom' : 'none',
		'border-top' : 'none',
    	'-moz-border-radius': '3px',
    	'-webkit-border-radius': '3px',
    	'-o-border-radius': '3px',
    	'-ms-border-radius': '3px',
    	'-khtml-border-radius': '3px',
    	'border-radius': '3px'
	},
	cls : 'dashHeader',
	initComponent: function() {
		var config = {
            columns:[],
            rowNumberer: false
        };
		
		var clsData = '';
		
		this.items =[{
			xtype: 'panel',
			cls: 'panelDash', 
			items: [{
				xtype: 'flowCharts',
				items: [{
					xtype: 'grid',
					columns: [],
					id: 'dynamicgridID1',
					columnLines: true, 
					hideHeaders: true,
					forceFit:true,
					store: Ext.getStore('DashboardPanelHeader'),
					//margins: {top:0, left:0, right: 0, bottom:40},
				}]
			}]
		}]
		
		Ext.apply(this, config);
        // apply to the initialConfig
        Ext.apply(this.initialConfig, config);
		
        this.callParent(arguments);
    },
	
	storeLoad1: function()
    {
        /**
        * JSON data returned from server has the column definitions
        */
        if(typeof(this.store.proxy.reader.jsonData.columns) === 'object') {
            var columns = [];
            /**
            * adding RowNumberer as we need to add them
            * before other columns to display first
            */
            if(this.rowNumberer) { columns.push(Ext.create('Ext.grid.RowNumberer')); }
            /**
            * assign new columns from the json data columns
            */
            Ext.each(this.store.proxy.reader.jsonData.columns, function(column){
                columns.push(column);
            });
            /**
            *  reconfigure the column model of the grid
            */
            Ext.getCmp('dynamicgridID1').reconfigure(this.store, columns);
        }
    },
	
	listeners: {
		render: function(panelGrid, position){
			var grids = panelGrid.items.items[0].initialConfig.items[0].items;
			
            if(!grids[0].store.getCount()) 
			{
				grids[0].store.on('load', this.storeLoad1, grids[0]);
            }
            else 
			{
				grids[0].store.reload();
            }
		}
	}
});