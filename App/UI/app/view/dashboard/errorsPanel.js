Ext.define('AM.view.dashboard.errorsPanel' ,{
	extend: 'Ext.Panel',
	alias: 'widget.errorspanel',
	//requires:['AM.view.chart.PieChart'],
	layout : {
		type: 'card',
		deferredRender: true
	},
	loadMask: true,
	autoCreate: true,
	height:275,
	initComponent: function() {
		var me = this;
		
		Ext.apply(me, {
		   store : Ext.create('AM.store.ErrorsChart')
		});
		me.store.getProxy().extraParams = {'groupvertical': me.groupverticalcombo}
		
		
		this.items =[{
			xtype: 'grid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Client', 
				dataIndex: 'Name', 
				flex: 1.5,
				menuDisabled:true
			},{
				header: 'Appreciation', 
				dataIndex: 'Value2', 
				flex: 1,
				sortable: false,
				menuDisabled:true
			},{
				header: 'Escalation', 
				dataIndex: 'Value3', 
				flex: 1,
				sortable: false,
				menuDisabled:true
			},{
				header: 'Errors', 
				dataIndex: 'Value1', 
				flex: 1,
				sortable: false,
				menuDisabled:true
			}],
			tbar: [{
				xtype : 'trigger',
				style : {
					'margin-left' : '10px'
				},
				triggerCls : 'x-form-clear-trigger',
				emptyText : 'Client Name',
				itemId : "clientnameErrGrid",
				minChars : 2,
				enableKeyEvents : true,
				listeners : {
					specialkey	: function(f,e) {
						var txtVal = Ext.util.Format.trim(f.getValue());
						if (e.getKey() == e.ENTER) {
							if(txtVal != '') {
								me.store.proxy.extraParams = {searchKey: txtVal};
								me.store.load();
							}
						}
					},
					keyup : function(f,e) {
						Ext.Ajax.abortAll();
						var txtVal = Ext.util.Format.trim(f.getValue());
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
				},
				onTriggerClick	: function() {
					this.reset();
					var store = me.store;
					store.proxy.extraParams = {searchKey: ""};
					store.load();
					store.clearFilter();
				}
			}, {
				text: 'Search',
				handler : function(obj) {
					var txtVal = Ext.util.Format.trim(obj.prev().getValue());
					if(txtVal != '')
					{
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
					else
					{
						me.store.proxy.extraParams = {searchKey: ""};
						me.store.load();
					}
				}
			} ]
		},{
			xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
		}]
		
		this.callParent(arguments);
	},

	
});