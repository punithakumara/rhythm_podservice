Ext.define('AM.view.dashboard.panelMenu' ,{
    extend: 'Ext.view.View',
    alias: 'widget.dashboardpanelmenu',    
    mixins: {
        dragSelector: 'Ext.ux.DataView.DragSelector',
        draggable   : 'Ext.ux.DataView.Draggable'
    },
    
    tpl: [
		'<tpl for=".">',
		'<tpl if="thumb==\'employee_Details.png\'">',
		'<div style="border-left: 1px solid #aaaaaa; float: left; height: 19px; margin: 11px 3px 8px 6px;"></div>',
		'</tpl>',
		'<tpl if="thumb==\'locationwise_client.png\'">',
		'<div style="border-left: 1px solid #aaaaaa; float: left; height: 19px; margin: 11px 3px 8px 6px;"></div>',
		'</tpl>',
		'<tpl if="thumb==\'err_app_esc.png\'">',
		'<div style="border-left: 1px solid #aaaaaa; float: left; height: 19px; margin: 11px 3px 8px 6px;"></div>',
		'</tpl>',
			'<div class="thumb-wrap">',
				'<div class="thumb" data-qtip="{tool_tip}">',
					(!Ext.isIE6? '<tpl if="thumb!=\'err_app_esc.png\'"><img src="resources/images/dashboard/{thumb}" width="30" height="30" /><tpl elseif="thumb==\'err_app_esc.png\'"><img src="resources/images/dashboard/{thumb}" width="70" height="30" /></tpl>' : 
					'<div style="width:76px;height:76px;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'resources/images/dashboard/{thumb}\')"></div>'),
				'</div>',
			'</div>',
		'</tpl>'
    ],
    itemSelector: 'div.thumb-wrap',
    overItemCls: 'drag-image-over',
    selectedItemClass: 'drag-image-selected',
    multiSelect: false,
    singleSelect: true,
    cls: 'x-image-view',
    autoScroll: false,
    trackOver: true,
    
	initComponent: function() {
    	var me = this;    	
    	var initialTools = AM.app.globals.DashboardToolsIcons;
    	
    	if(initialTools === null || initialTools == "" || initialTools == undefined)
			this.store = Ext.create('AM.store.DashboardTool');

		if(initialTools != "" && me.store == undefined && initialTools != "null")
		{
			initialTools = JSON.parse(initialTools);    		
			if(initialTools.Icons == undefined || initialTools.Icons == "")
				this.store = Ext.create('AM.store.DashboardTool');
			else
			{
				var storeInitial = Ext.getStore('DashboardTool');
				storeInitial.removeAll();
				var data = initialTools.Icons;
				storeInitial.sync();
				storeInitial.loadData(data,true);		   		
				this.store = storeInitial;
			}
    	}
    	else
    		this.store = me.store;

        this.mixins.dragSelector.init(this);
       
        this.mixins.draggable.init(this, {
        	ddConfig: {
				ddGroup: 'myDDGroup',
				containerScroll : true,
				scroll: false,
        	}
        });
       
        this.callParent(arguments);
        if(Ext.util.Cookies.get('grade') < 4) 
		{   
        	var storeItems = this.store.getRange(),
            i = 0;
        	filterItemIds =  ['dashboardRepBillable'];       	
			for(; i<storeItems.length; i++)
			{     
			    if(Ext.Array.contains(filterItemIds, storeItems[i].get('idVal')))
				{ 
					var removeRec = this.store.findRecord('idVal', storeItems[i].get('idVal'));
					this.store.remove(removeRec);
				}
			}
        }
    },
    
    listeners: {
    	itemmousedown: function (view, rec, item, idx, event) {
    		var startX = event.getX();
            var startY = event.getY();

            var grid = view.up('container');
            var div = grid.getEl();
            var body = Ext.getBody();
            
            div.on('mousemove', function(e){
            	x = e.getX(), y = e.getY()
                startX = x
                startY = y
    		});
            
            body.on('mouseup', function(e){
                div.removeAllListeners();
                body.removeAllListeners();
            });

            body.on('mouseleave', function(e, tgt){
                div.removeAllListeners();
                body.removeAllListeners();
            });
        }
    }
});