 Ext.define('AM.view.transition.flowCharts', {
	extend: 'Ext.container.Container', 
	alias: 'widget.flowCharts', 
	layout: {
		type: 'hbox', 
		align: 'stretch'
	}, 
	defaults: { flex: 1 }, 
	initComponent: function () {
		var me = this;
		me.callParent(arguments);
	}, 
	syncSizeToOwner: function () {
		var me = this;
		if (me.ownerCt) {
		  me.setSize(me.ownerCt.el.getWidth() * me.items.items.length, 75);
		}
	}, 
	showChild: function (item) {
		var me = this, left = item.el.getLeft() - me.el.getLeft();
		me.el.first().move('l', '', true);
		me.currentItem = item;
	}, 
	nextChild: function () {
		var me = this;
		var next = me.currentItem.nextSibling();
		me.showChild(next || me.items.items[0]);
		me.items.items[1].show();
		me.items.items[0].hide();
	}, 
	previousChild: function () {
		var me = this;
		var next = me.currentItem.previousSibling();
		me.showChild(next || me.items.items[me.items.items.length - 1]);
		me.items.items[1].hide();
		me.items.items[0].show();
	},
	listeners: {
		render: function(){
			var me = this;
			me.currentItem = me.items.items[0];
			if (me.ownerCt) {
				me.on({
					ownerresize: me.syncSizeToOwner
				});
			}
		},
	}
});