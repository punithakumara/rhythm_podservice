Ext.define('AM.view.dashboard.treePanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.dashboardtreepanel',
    layout : 'fit',
    loadMask: true,
    autoCreate: true,
	requires:['Ext.tree.*','Ext.data.*'],
	margins : 5,
	height:275,
	scroll:true,
    initComponent: function() {
    	var me = this;
    	Ext.apply(me, {
			store : Ext.create('AM.store.Hierarchy')
        });
    	me.items =[{
			xtype: 'treepanel',
			listeners: {
				itemclick: {

					fn: function (view, record, item, index, e) {
						var numberPattern = /\d+/g;
						var parentid = "";
						var flag = true;
						
						try{
							parentid = record.data.parentId.match( numberPattern )[0];    							
						}catch(e){
							
						}
						
						if(typeof(parentid) == "string")
						{    							
							parentid = parseInt(parentid);
							if(isNaN(parentid))
							   flag = false;      							
						}

						if(record.data.id.indexOf("NewJoinees-process") <= 0){
							if (record.data.id != '223-process' && record.data.id != '221-process' ) //disable history popup for '223-process' and '221-process'
							{
								var id = record.data.id;
								EmployID = id.match( numberPattern ).toString();
								AM.app.getController('Dashboard').popupContents(EmployID, 'BillableEmployeeHistory&popup');
							}
						}
					}
				}
			},
			store: me.store.load(),
			animate:true, 
			useArrows: true,
			autoScroll: true,
			rootVisible: false	
		}];
		
    	me.callParent(arguments);
		
		me.store.on({
            beforeload: function() {
            	me.getEl().mask("Loading", 'x-mask-loading');
            },
            load : function() {
            	(me.getEl() != undefined ) ? me.getEl().unmask(): ""
            }
        });
    }
});