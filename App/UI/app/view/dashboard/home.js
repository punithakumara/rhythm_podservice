Ext.define('AM.view.dashboard.home',{
	extend: 'Ext.container.Container',
	alias : 'widget.dashboardPage',
	// requires:['AM.view.dashboard.locationPanel','AM.view.dashboard.panelHeader','AM.view.dashboard.panelMenu'],
	requires:['AM.view.dashboard.panelMenu','AM.view.dashboard.panelHeader'],
	layout : {
		type: 'vbox',
		pack: 'end',
        align: 'stretch'		
	},
	id:"mydis",
	width:'100%',
	scroll:true,
	
	rendergraphtoPanel: function(userdashboarditems,combo)
	{
		userdashboarditems = Ext.decode(userdashboarditems);
		for (var key in userdashboarditems) 
		{
			var arrkey = [];
			for(var subkey in userdashboarditems[key])
			{
				var arr = [];
				for (var prop in userdashboarditems[key]) 
				{
					arr.push(userdashboarditems[key][prop]);
				}			 
			}
			arrkey.push(key);
			if(Ext.getCmp(arrkey[0]) != undefined)
			{
				Ext.getCmp(arrkey[0]).removeAll();
				Ext.getCmp(arrkey[0]).add({
					xtype : Ext.create('AM.view.'+arr[0],{groupverticalcombo:combo.getValue()} )
				});
				Ext.getCmp(arrkey[0]).doLayout();
			}
		}
	},
	
	
	initComponent: function() 
	{
		var me = this;
		if(!AM.app.globals.UserDashboard)
		{
			AM.app.globals.UserDashboard = Ext.encode({
				'panelId1': {'view_name': 'dashboard.barPanel', 'title': 'Billable and Non Billable Employees'}, 
				'panelId2': {'view_name': 'dashboard.locationPanel', 'title': 'Locationwise Employees'}, 
				'panelId3': {'view_name': 'dashboard.shiftPanel', 'title': 'Shiftwise Employees'}, 
				'panelId4': {'view_name': 'dashboard.gradePanel', 'title': 'Gradewise Employees'}, 
				'panelId5': {'view_name': 'dashboard.nrrRdrPanel', 'title': 'Work Order Request'}, 
				'panelId6': {'view_name': 'dashboard.treePanel', 'title': 'Employee Hierarchy'}
			});
		}
		
		var UserDashboard = Ext.decode(AM.app.globals.UserDashboard);
		
		this.items = [{
			xtype : 'dashboardpanelheader',
			minHeight: 100
		}, {
			xtype : 'panel',
			id : 'panelContainer1',
			style: {
				'margin-top':'-35px',
			},		
			items : [{
				xtype:'container',
				id : 'cot12',
				layout: {
					type: 'hbox',
					width:'100%'
				},
				items: [{
					xtype:'container',
					id:'PanelMenuIcons',
					width:780,
					layout: {
						type: 'hbox',
						align: "left",
						width:'100%',
					},
					items: [{
						xtype : 'dashboardpanelmenu',						
						style: {
							'margin-right':'10px',
							'padding-right':'4px',
							"align": "left",
						}
					}, Ext.create('Ext.Img', {
						src: AM.app.globals.uiPath+'resources/images/settingsIcon1.png',
						id: 'Setingsicons',
						listeners:{
							el: {
								click: function() {
									AM.app.getController("AM.controller.Dashboard").settingIcon();
								}
							},
							afterrender: function(c) {
								Ext.create('Ext.tip.ToolTip', {
									target: c.getEl(),
									html: 'Settings'
								});
							}
						}
					})] 
				}]
			}]
		}, {
			xtype : 'panel',
			id : 'panelContainer',
			items : [{
				xtype:'container',
				id : 'cot1',
				layout: {
					type: 'hbox',
					pack: 'center',
					align: "center",
					width:'100%',	
				},
				margin:5,
				items: [{
					xtype:'panel',
					id : 'panelId1',
					cls : 'subPanels',
					title : '<div style="text-align:center;">'+UserDashboard.panelId1.title+'</div>',
					style:{
						border:'1px solid gray'
					},	
					margins : 5,
					height:'auto',
					items : [{
						xtype : Ext.create('AM.view.'+UserDashboard.panelId1.view_name)
					}],
					flex:1
				},{
					xtype:'panel',
					id : 'panelId2',
					cls : 'subPanels',
					title : '<div style="text-align:center;">'+UserDashboard.panelId2.title+'</div>',
					style:{
						border:'1px solid gray'
					},	
					margins : 5,
					height:'auto',
					items : [{
						xtype : Ext.create('AM.view.'+UserDashboard.panelId2.view_name)
					}],
					flex:1
				},{
					xtype:'panel',
					id : 'panelId3',
					cls : 'subPanels',
					title : '<div style="text-align:center;">'+UserDashboard.panelId3.title+'</div>',
					style:{
						border:'1px solid gray'
					},	
					margins : 5,
					height:'auto',
					items : [{
						xtype : Ext.create('AM.view.'+UserDashboard.panelId3.view_name)
					}],
					flex:1
				}]
			},{
				xtype:'container',
				id : 'cot2',
				layout: {
					type: 'hbox',
					pack: 'center',
					align: "center",
					width:'100%'	
				},
				margin:5,
				items: [{
					xtype:'panel',
					id : 'panelId4',
					cls : 'subPanels',
					title : '<div style="text-align:center;">'+UserDashboard.panelId4.title+'</div>',
					style:{
						border:'1px solid gray'
					},	
					margins : 5,
					height:'auto',
					items : [{
						xtype : Ext.create('AM.view.'+UserDashboard.panelId4.view_name)
					}],
					flex:1
				},{
					xtype:'panel',
					id : 'panelId5',
					cls : 'subPanels',
					title : '<div style="text-align:center;">'+UserDashboard.panelId5.title+'</div>',
					style:{
						border:'1px solid gray'
					},	
					margins : 5,
					height:'auto',
					items : [{
						xtype : Ext.create('AM.view.'+UserDashboard.panelId5.view_name)
					}],
					flex:1
				},{
					xtype:'panel',
					id : 'panelId6',
					cls : 'subPanels',
					title : '<div style="text-align:center;">'+UserDashboard.panelId6.title+'</div>',
					style:{
						border:'1px solid gray'
					},	
					margins : 5,
					height:'auto',
					items : [{
						xtype : Ext.create('AM.view.'+UserDashboard.panelId6.view_name)
					}],
					flex:1
				}]
			}]
		}];

		this.callParent(arguments);

	}
	
});	
	
