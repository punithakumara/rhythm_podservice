Ext.define('AM.view.dashboard.ClientBillableResourcePanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.ClientBillableResourcePanel',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me=this;
		this.activeItem  = 0;
		Ext.apply(me, {
			store : Ext.create('AM.store.ClientBillableResource',{
				remoteSort: true,
				autoLoad: true,
			})
        });
		me.store.getProxy().extraParams = {'hasNoLimit':'1'};
		
		this.items =[{
			xtype: 'grid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Client', 
				dataIndex: 'client_name', 
				flex: 1.7,
				menuDisabled:true
			}, {
				header: 'Approved FTE', 
				dataIndex: 'model',
				sortable: false, 
				flex: 1,
				menuDisabled:true
			},{
				header: 'Billable', 
				dataIndex: 'Billable', 
				flex: 0.7,
				sortable: false,
				menuDisabled:true
			},{
				header: 'Non-Billable', 
				dataIndex: 'NonBillable', 
				flex: 1,
				sortable: false,
				menuDisabled:true
			}],
			tbar: [{
				xtype			: 'trigger',
				triggerCls		: 'x-form-clear-trigger',
				emptyText		: 'Client Name',
				itemId			: "clientname",
				minChars		: 2,
				enableKeyEvents : true,
				listeners		: {
					specialkey	: function(f,e) {
						var txtVal = Ext.util.Format.trim(f.getValue());
						if (e.getKey() == e.ENTER) {
							if(txtVal != '') {
								me.store.proxy.extraParams = {searchKey: txtVal};
								me.store.load();
							}
						}
					},
					keyup : function(f,e) {
						Ext.Ajax.abortAll();
						var txtVal = Ext.util.Format.trim(f.getValue());
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
				},
				onTriggerClick	: function() {
					this.reset();
					var store = Ext.getStore('ClientBillableResource');
					store.proxy.extraParams = {searchKey: ""};
					store.load();
        			store.clearFilter();
				}
			}, {
				text: 'Search',
				handler : function(obj) {
					var txtVal = Ext.util.Format.trim(obj.prev().getValue());
					if(txtVal != '')
					{
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
					else
					{
						me.store.proxy.extraParams = {searchKey: ""};
						me.store.load();
					}
				}
			}]
        },{
			xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
		}]
		
        this.callParent(arguments);
    },
	
});