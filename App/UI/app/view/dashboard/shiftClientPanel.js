Ext.define('AM.view.dashboard.shiftClientPanel' ,{
    extend: 'Ext.Panel',
    alias: 'widget.dashboardshiftclientpanel',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
	autoScroll: true,
    initComponent: function() {

		var me=this;
		this.activeItem = 0;
		Ext.apply(me, {
			store : Ext.create('AM.store.ShiftClientChart',{
				remoteSort: true,
				autoLoad: true,
			})
        });
		me.store.getProxy().extraParams = {'hasNoLimit':'1'};
		
		this.items =[{
			xtype: 'grid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Client', 
				dataIndex: 'Name', 
				width:100
			},{
				header: 'EST', 
				dataIndex: 'Value1', 
				width:70
			},{
				header: 'EMEA', 
				dataIndex: 'Value2', 
				width:70,
				sortable: false
			},{
				header: 'IST', 
				dataIndex: 'Value3', 
				width:70,
				sortable: false
			},{
				header: 'Late EMEA', 
				dataIndex: 'Value4', 
				width:70,
				sortable: false
			},{
				header: 'CST', 
				dataIndex: 'Value5', 
				width:70,
				sortable: false
			},{
				header: 'PST', 
				dataIndex: 'Value6', 
				width:70,
				sortable: false
			},{
				header: 'APAC', 
				dataIndex: 'Value7', 
				width:70,
				sortable: false
			}],
			tbar: [{
				xtype			: 'trigger',
				triggerCls		: 'x-form-clear-trigger',
				emptyText		: 'Client Name',
				itemId			: "clientname",
				minChars		: 2,
				enableKeyEvents : true,
				listeners		: {
					specialkey	: function(f,e) {
						var txtVal = Ext.util.Format.trim(f.getValue());
						if (e.getKey() == e.ENTER) {
							if(txtVal != '') {
								me.store.proxy.extraParams = {searchKey: txtVal};
								me.store.load();
							}
						}
					},
					keyup : function(f,e) {
						Ext.Ajax.abortAll();
						var txtVal = Ext.util.Format.trim(f.getValue());
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
				},
				onTriggerClick	: function() {
					this.reset();
					var store = Ext.getStore('ShiftClientChart');
					store.proxy.extraParams = {searchKey: ""};
					store.load();
        			store.clearFilter();
				}
			}, {
				text: 'Search',
				handler : function(obj) {
					var txtVal = Ext.util.Format.trim(obj.prev().getValue());
					if(txtVal != '')
					{
						me.store.proxy.extraParams = {searchKey: txtVal};
						me.store.load();
					}
					else
					{
						me.store.proxy.extraParams = {searchKey: ""};
						me.store.load();
					}
				}
			}]
        },{
			xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
		}]
		
        this.callParent(arguments);
    },
	
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
					obj.getLayout().setActiveItem(2);
				else
					obj.getLayout().setActiveItem(0);
			}); 
		}
	}
});