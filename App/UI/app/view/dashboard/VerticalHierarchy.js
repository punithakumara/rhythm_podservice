Ext.define('AM.view.dashboard.VerticalHierarchy', {
	extend		: 'Ext.tree.Panel',
	alias		: 'widget.verticalTreePanel',
	layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask	: true,
    autoCreate	: true,
	height		: 275,
	useArrows	: true,
	rootVisible	: false,
	viewConfig: {
		listeners: {
			cellclick : function(view, cell, cellIndex, record,row, rowIndex, e) {
			 var clickedColumnName = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
				var vertNamesplit = 'VerticalHierarchy' +"&"+clickedColumnName;
				
				if((cellIndex != 0) && (record.get(clickedColumnName) != 0))				
					AM.app.getController('Dashboard').popupContents(record,vertNamesplit);
				}			
		},
		getRowClass: function(record, index) {
			return 'cursor_hand';
        }
	},
	
	initComponent:function(){
		var me = this;
		Ext.apply(me, { store : Ext.create('AM.store.VerticalHierarchy') });
		
		this.columns = [{
			xtype		: 'treecolumn', //this is so we know which column will show the tree
			text		: 'Vertical',
			flex		: 2,
			sortable	: true,
			dataIndex	: 'Name'
		},{
			text		: '',
			flex		: 1,
			dataIndex	: 'ParentVerticalID',
			hidden:true
		},{
			text		: '',
			flex		: 1,
			dataIndex	: 'VerticalID',
			hidden:true
		},{
			text		: 'Billable',
			flex		: 1,
			dataIndex	: 'Billable',
			hidden:false
		},{
			text		: 'Non-Billable',
			flex		: 1,
			dataIndex	: 'NonBillable',
			hidden:false
		},{
			text		: 'Leadership',
			flex		: 1,
			dataIndex	: 'Leadership',
			hidden:false
		}];
		
		this.callParent(arguments);
	}
	
} );
