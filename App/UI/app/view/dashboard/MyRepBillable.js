Ext.define('AM.view.dashboard.MyRepBillable' ,{
    extend: 'Ext.Panel',
    alias: 'widget.dashboardMyRepBillable',
    layout : {
		type: 'card',
		deferredRender: true
	},	
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {
		 
		 var me=this;
		 this.activeItem  = 0;		
		 Ext.apply(me, {
	           store : Ext.create('AM.store.ReporteesBillable'),
	           remoteSort: true
	        });

		 var dataStore = Ext.create('Ext.data.Store', {
			 fields: ['monthValueBillable', 'monthRangeBillable'],
			        data: [
			          {monthValueBillable: 'Current Month', monthRangeBillable: '0'},
			          {monthValueBillable: 'Last 1 Months', monthRangeBillable: '1'},
			          {monthValueBillable: 'Last 2 Months', monthRangeBillable: '2'},
			          {monthValueBillable: 'Last 3 Months', monthRangeBillable: '3'},
			          {monthValueBillable: 'Last 6 Months', monthRangeBillable: '6'},
			          {monthValueBillable: 'Last 12 Months', monthRangeBillable: '12'},
			        ]
			  });
		this.items =[{
			xtype: 'grid',
			viewConfig: {
				listeners: {
					cellclick : function(view, cell, cellIndex, record,row, rowIndex, e) {
					
				
					if(cellIndex ==1 ){					
					  var clickedColumnName = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
						var myreporteebillsplit = 'ReporteesBillable' +"&"+clickedColumnName;
						record.set('Range', me.getDockedItems()[0].items.items[0].getValue());
						if(record.raw.Value1 != 0)
						AM.app.getController('Dashboard').popupContents(record,myreporteebillsplit);
					}
					if(cellIndex ==2 ){
						  var clickedColumnName = view.panel.headerCt.getHeaderAtIndex(cellIndex).text;
							var myreporteebillsplit = 'ReporteesBuffer' +"&"+clickedColumnName;
							record.set('Range', me.getDockedItems()[0].items.items[0].getValue());
							if(record.raw.Value3 != 0)
							AM.app.getController('Dashboard').popupContents(record,myreporteebillsplit);
						}
					
					/*if(cellIndex ==3 ){
						 
						  var Range = me.getDockedItems()[0].items.items[0].getValue();
							AM.app.getController('treeGridUtilController').viewContent(record.data.EmployID, record.data.Name, record.data.grades,Range);
				
						}*/
						/*if((me.store.getCount() === (rowIndex-1)) || (cellIndex == 1))
							return false;
						else*/
							
						
					}
					
				},
				getRowClass: function(record, index) {
						return 'cursor_hand';
                }
                
			},
			store : me.store,
            // xtype: 'chartPieChart',
			autoScroll: true,
			columns: [{
				header: 'Name',
				sortable: true,
				dataIndex: 'Name', 
				 renderer:function(value,metaData,record,colIndex,store,view) {					
					metaData.tdAttr = 'data-qtip="' + record.get("Name") + '"';
					return value;
				},
				flex: 2
			},{
				header: 'Billable %', 
				sortable: true,
				dataIndex: 'Value1', 
				flex: 1.2
			},
			/*{
				header: 'LeaderShip', 
				sortable: true,
				dataIndex: 'Value2', 
				flex: 1.4
			},*/
			{
				header: 'Non-Billable %', 
				sortable: true,
				dataIndex: 'Value3', 
				flex: 1.5
			},
			{
				header: 'Buffer %', 
				sortable: true,
				dataIndex: 'Value5', 
				flex: 0.9
			},
			/*{
				header: 'Utilization %', 
				sortable: true,
				dataIndex: 'Value4', 
				flex: 1.3
			}*/
			]
        },{
			xtype: 'image',
			name: 'noDataImg',
			cls:'noDataPanel',
			alt : 'no_data_available',
			src: AM.app.globals.uiPath+'resources/images/dashboard/no_data_available.png'
		}];
		this.tbar = [{
			xtype: 'combobox',
			multiSelect: false,
			store:dataStore,
			mode: 'local',		
			//labelSeparator:"",
			itemId:'myrepbillrange',
			name: 'monthRangeBillable',
			fieldLabel: 'Select Range ',
			//readOnly: true,
			displayField: 'monthValueBillable',
			valueField: 'monthRangeBillable',
			style: {'margin-right': '2px'},
			value:'0',
			width : 300,
			autoSelect:true,
			forceSelection:true,
			listeners: {
				change: function(obj){
				me.store.load({
					params:{'hasNoLimit':"1",'Range':obj.value}
				,callback:function(records, options, success) {
					me.getEl().unmask("Loading...","x-mask-loading");
					if (success)
						me.getLayout().setActiveItem(0);
					else
						me.getLayout().setActiveItem(1);
				}});
				},
				beforequery : function(queryEvent) {
            		Ext.Ajax.abortAll();
            		queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1', 'comboVertical':'1', 'filterName' : queryEvent.combo.displayField};
            	}
			}
		}];
        this.callParent(arguments);
    },
	listeners: {
		render: function(obj){
			obj.store.on( 'load', function( store, records, options ) {
				if(store.getCount() == 0)
					obj.getLayout().setActiveItem(2);
				else
					obj.getLayout().setActiveItem(0);
			}); 
		}
	}
});