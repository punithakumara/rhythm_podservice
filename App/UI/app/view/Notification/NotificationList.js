Ext.define("AM.view.Notification.NotificationList", {
    extend: "Ext.grid.Panel",
    alias: "widget.notificationList",
    title: "Notification List",
    id: "notificationlistId",
    layout: "fit",
    columnLines: true,
    cls: "custom-grid",
    emptyText: '<div align="center">No Data To Display</div>',
    viewConfig: {
        deferEmptyText: false,
    },
    width: "99%",
    overflowY: true,
    loadMask: true,
    border: true,
    store : 'Notifications',
    initComponent: function() {
        this.columns = [ {
            header: "Module Name",
            dataIndex: "Title",
            flex: 1,
            renderer: function(getTitle, item) {
                try {
                    var obj = Ext.decode(getTitle);
                    return obj.Title;
                } catch(e) {
                    return getTitle;
                }
            }
        }, {
            header: "Notification Sent On",
            dataIndex: "NotificationDate",
            flex: 1
        }, {
            header: "Sender Name",
            dataIndex: "FullName",
            flex: 1
        }, {
            dataIndex: "Type",
            hidden: true,
        }, {
            header: "Action",
            flex: 1,
            xtype: "actioncolumn",
            items: [ {
                icon: AM.app.globals.uiPath + "resources/images/view.png",
                tooltip: "View",
                handler: this.sendType,
                getClass: function(v, meta, rec) {                	
                    if((rec.data.Type == 8) ){                                                                      
                        return 'x-hide-display';
                    }
                }
            } ]
        } ];
        this.bbar = Ext.create("Ext.toolbar.Paging", {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: "Displaying {0} - {1} of {2}",
            emptyMsg: "No Notifications To Display",
            plugins: [ Ext.create("Ext.ux.grid.plugin.PagingToolbarResizer") ]
        });
        this.callParent(arguments);
    },
    sendType: function(grid, row, col) {
        var rec   = grid.getStore().getAt(row);
        var title = rec.get('Title');
        var nid   = rec.get('NotificationID');
        var type  = rec.get('Type');
        AM.app.getController("Notification").viewNotificationType(title, nid, type);
        AM.app.getController("Notification").updateNotification(nid);
    }
});