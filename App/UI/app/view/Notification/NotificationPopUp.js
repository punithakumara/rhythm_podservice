Ext.define("AM.view.Notification.NotificationPopUp", {
    extend: "Ext.Window",
    alias: "widget.notificationpopupx",
    id: "notifypopupcc",
    cls: "notifypopupcc",
    title: "<b>Notifications</b>",
    autoShow: true,
    width: "25%",
    minHeight: 300,
    Height: 100,
    border: false,
    layout: "fit",
    frame: false,
    closable: false,
    moveable: false,
    draggable: false,
    resizable: false,
    tbar: false,
    modal: false,
    footer: true,
    x: 950,
    y: 49,
    overflowY: "hidden",
    style: {
        display: "block"
    },
    initComponent: function() {
        var me      = this;
        var mystore = Ext.getStore("Notifications").load();
        var data    = { rec : [ {} ] };
        if (0 != mystore.getCount()) 
		{
            Ext.each(mystore.data.items, function(value, i) {
                data.rec[i] = value.data;
            } );
            var tpl = new Ext.XTemplate(
                '<tpl for=".">',
                    '<div>',
                        '<div class="mainnotfic">',
                            '<ul class="NotificationList">',
                                '<tpl for="rec">',
                                   '<li class="box" custom_nid = {NotificationID} custom_title = \'{Title}\' custom_type = {Type}>',
                                       '<div class="{[this.getNoteClass(values.ReadFlag)]}" id="content">{[this.addBreaks(values.Description, values.Type)]}</div>',
                                       '<div class="date">{[this.timeSince(values.NotificationDate)]}</div>',
                                    '</li>',
                                '</tpl>',
                            '</ul>',
                        '</div>',
                    '</div>',
                    '<div class="showallcls" id="showallcls">',
                        '<div class="clearallnotif" id="clearallnotif">',
                            '<a href="#" id="clearallnotfic">Clear All</a>',
                        '</div>',
                        '<div class="showallnotif" id="showallnotif">',
                            '<a href="#" id="seeallnotfic">See All Notifications</a>',
                        '</div>',
                    '</div>',
                '</tpl>',
                {
                    timeSince: function(date) {
                        return AM.app.getController("Notification").timeSince(date);
                    },
                    addBreaks: function(len, type) {
                        if(type == 4) {
                            return "Invitation to participate in a Theorem survey";
                        } else {
                            return len.replace(/(.{45})/g, "$1<br/>");
                        }
                    },
                    getNoteClass: function(ReadFlag) {
                        var cls = "";
                        if(ReadFlag == 0) {
                            cls = "unreadcontents";
                        } else {
                            cls = "readcontents";
                        }
                        return cls;
                    }
                }
            );
        } 
		else 
		{
            var tpl = new Ext.XTemplate(
                "<tpl>",
                    '<div id="watermark">',
                        '<p>No New Notifications.</p>',
                    '</div>',
                    '<div class="nullnotific">',
                        '<div class="clearallnotif" id="clearallnotif">',
                            '<a href="#" id="clearallnotfic">Clear All</a>',
                        '</div>',
                        '<div class="showallnotif" id="showallnotif">',
                            '<a href="#" id="seeallnotfic">See All Notifications</a>',
                        '</div>',
                    '</div>', 
                "</tpl>"
                );
        }
		this.items = [ {
			xtype: "panel",
			tpl: tpl,
			data: data,
			id: "notficationtpl",
			listeners: {
				afterrender: {
					fn: function(obj) {
						Ext.select(".box").on("click", function(attr) {
							var nid   = this.getAttribute("custom_nid");
							var title = this.getAttribute("custom_title");
							var type  = this.getAttribute("custom_type");
							AM.app.getController("Notification").viewNotificationType(title, nid, type);
							AM.app.getController("Notification").updateNotification(nid);
						});
					}
				}
			}
		} ];
		this.callParent(arguments);
	},
	listeners: {
		render: function(obj) {
            Ext.select(".notifypopupcc").on("mouseleave", function() {
                Ext.getCmp("notifypopupcc").destroy();
                var notifypopup = Ext.getCmp('NotifyToolbar');
                notifypopup.flag = 0;
            });
            Ext.select(".showallnotif").on("click", function() {
                AM.app.getController("AM.controller.Notification").viewNotifications();
            });
            Ext.select(".clearallnotif").on("click", function() {
                AM.app.getController("AM.controller.Notification").updateNotification();
            });
        }
    }
});