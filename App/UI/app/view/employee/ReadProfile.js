Ext.define('AM.view.employee.ReadProfile' ,{
    extend : 'Ext.window.Window',
    alias: 'widget.ReadProfile',
	//layout : 'fit',
	width: '93%',
	height: '90%',
    autoShow : true,
    modal : true,
	autoScroll : true,
	draggable : false,
	
    initComponent: function() {
		var me = this;
		
		this.items = [{
			xtype:'panel',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
			},
			style: {
				height : '50px !important',
			},
			// margin:5,
			items: [{
				xtype:'panel',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Skills</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				items : [{
					xtype : Ext.create('AM.view.employee.Skillsets')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId2',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Work Experience</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				items : [{
					xtype : Ext.create('AM.view.employee.WorkExperience')
				}],
				flex:1
			}]
		},{
			xtype:'panel',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'	
			},
			// margin:5,
			style:{
				height : '50px !important',
				'margin-top': '0px'
			},
			items: [{
				xtype:'panel',
				// id : 'panelId4',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Certifications</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				items : [{
					xtype : Ext.create('AM.view.employee.Certifications')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId5',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Language Proficiency</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				items : [{
					xtype : Ext.create('AM.view.employee.Language')
				}],
				flex:1
			}]
		},{
			xtype:'panel',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'	
			},
			// margin:5,
			style:{
				height : '50px !important',
				'margin-top': '0px'
			},
			items: [{
				xtype:'panel',
				// id : 'panelId5',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Other Achievements</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				items : [{
					xtype : Ext.create('AM.view.employee.OtherAchievements')
				}],
				flex:1
			}]
		}]

		this.callParent(arguments);
    }
	
}); 