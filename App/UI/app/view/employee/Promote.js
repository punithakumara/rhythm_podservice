Ext.define('AM.view.employee.Promote',{
	extend : 'Ext.window.Window',
	alias : 'widget.Promote',
	title: 'Employee - Promote',
	constrain:true,	
	layout: 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    maxHeight:600,
    autoScroll: true,
    modal: true,
	
    initComponent : function() {
		
    	this.items = [{
			xtype : 'form',
			id : 'EmpPromote',
			fieldDefaults: {
				labelAlign: 'right',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			autoScroll : true,
			autoHeight : true,
			items : [{
				xtype : 'combobox',
				fieldLabel: 'New Grade <span style="color:red">*</span>',
				name: 'new_grade',
				forceSelection: true,
				store: 'Grade',
				queryMode: 'remote',
				displayField: 'hcm_grade', 
				minChars:0,
				width:'100%',
				valueField: 'hcm_grade',
				id: 'new_hcm_grade',
				emptyText: 'Select New Grade',
				allowBlank: false,
				listeners:{
					beforequery: function(queryEvent, eOpts){
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'PromoteToGrades':Ext.getCmp('viewGradeLevel').getValue(),
							'noToGrades':Ext.getCmp('viewDesignationId').getValue()
						}								 
					},
					select: function() {
						Ext.getCmp('new_designation_id').setDisabled(false);
						Ext.getCmp('new_designation_id').setValue('');
						Ext.getStore('Designations').load({
							params:{
								'hasNoLimit':'1',
								'Grade':this.getValue(),
								'DesignNameCombo':Ext.getCmp('viewDesignationId').getValue(),
								'PromoteToGrades':Ext.getCmp('viewGradeLevel').getValue(),
								'noToGrades':Ext.getCmp('viewDesignationId').getValue()
							}
						});	
						Ext.getCmp('new_designation_id').bindStore("Designations");
					}
				}
			}, {
				width:'100%',
				xtype : 'combobox',
				fieldLabel: 'New Designation <span style="color:red">*</span>',
				disabled: true,
				id: 'new_designation_id',
				name: 'new_designation',
				queryMode: 'remote',
				displayField: 'name', 
				minChars:0,
				valueField: 'designation_id',
				emptyText: 'Select New Designation',
				allowBlank: false,
				anchor: '100%',
				flex:1,
				listeners:{
					beforequery: function(queryEvent, eOpts){
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'Grade': Ext.getCmp('new_hcm_grade').getValue(),
							'noToGrades':Ext.getCmp('viewDesignationId').getValue(),
							'PromoteToGrades':Ext.getCmp('viewGradeLevel').getValue()
						}								 
					}
				}
			}, {
				xtype : 'datefield',
				format:  AM.app.globals.CommonDateControl,
				width:'100%',
				fieldLabel: 'Date of Promotion <span style="color:red">*</span>',
				name: 'promote_date',
				id: 'promote_date',
				minValue: Ext.getCmp('view_doj').getValue(),
				maxValue: new Date(),
				allowBlank: false,
			}, {
				xtype : 'displayfield',
				value : 'There are few employees reporting to this person.',
				id:'warningfield',
				flex: 1,
				hidden:true,
			}, {
				width:'100%',
				xtype : 'boxselect',
				multiSelect: false,
				fieldLabel: 'Assign Reportees To',
				name: 'DeligateTo',
				id: 'promoteDeligate',
				displayField: 'Name', 
				valueField: 'employee_id',
				margin: '5 0 5 0',
				typeAhead: false,
				queryMode: 'local',
				minChar: 0,
				emptyText: 'Select Employee',
				anchor: '100%',
				flex:1,
				hidden:true,
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];

		this.callParent(arguments);
	},
	
	onCancel : function() {
		this.up('window').close();
	},
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();
		
		if (form.isValid()) 
		{
        	AM.app.getController('Employees').onSavePromotion(form, win);
        }
	},
});