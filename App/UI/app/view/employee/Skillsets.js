Ext.define('AM.view.employee.Skillsets' ,{
	extend: 'Ext.Panel',
	alias: 'widget.Skillsets',
	layout : {
		type: 'card',
		deferredRender: true
	},
	loadMask: true,
	autoCreate: true,
	height:275,
	initComponent: function() {

		var me = this;
		Ext.apply(me, {
			store : Ext.create('AM.store.SkillSets',{
				remoteSort: true,
				autoLoad: true,
			})
		});
		me.store.getProxy().extraParams = {'employee_id': AM.app.globals.viewIndEmp}
		
		this.items =[{
			xtype: 'grid',
			id:'skillGrid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Skills', 
				dataIndex: 'skill_name',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 0.6
			}, {
				header: 'Version', 
				dataIndex: 'version', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 0.6,
			},{
				text: "Last Used",
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 4,
				columns : [{
					header: 'Month', 
					dataIndex: 'last_used_month', 
					sortable: false,
					menuDisabled:true,
					groupable: false,
					draggable: false,
					width: 60,
				},{
					header: 'Year', 
					dataIndex: 'last_used_year', 
					sortable: false,
					menuDisabled:true,
					groupable: false,
					draggable: false,
					width: 60,
				}]
			},{
				header: 'Experience', 
				dataIndex: 'experience', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 0.6,
				renderer: function(value) {
					if(value==0)
						return '<1 Year';
					if(value==1)
						return '1-2 Years';
					if(value==2)
						return '2-3 Years';
					if(value==3)
						return '3-4 Years';
					if(value==4)
						return '4-5 Years';
					if(value==5)
						return '5+ Years';
					
				},
			},{
				header: 'Self Rating', 
				dataIndex: 'self_rating', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 0.6,
				renderer: function(value) {
					if(value==1)
						return 'Beginner';
					if(value==2)
						return 'Intermediate';
					if(value==3)
						return 'Proficient';
					if(value==4)
						return 'Advanced';
					if(value==5)
						return 'Champion/Expert';
					
				},
			},{
				header: 'Action', 
				xtype : 'actioncolumn',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 0.6,
				items: [{
					icon : AM.app.globals.uiPath+'/resources/images/edit.png',
					tooltip : 'Edit',
					handler : this.onEdit
				}, {
					xtype: 'tbspacer'
				}, {
					icon : AM.app.globals.uiPath+'/resources/images/delete.png',
					tooltip : 'Delete',
					handler : this.onDelete
				}]
			}],
		}]
		this.buttons = ['', {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			id : 'AddSkill',
			text : 'Add Skill',
			style : { 'margin' : '5px'},
			handler: function(){
				AM.app.getController('Employees').onCreateSkillset();
			}
		}];
		
		this.callParent(arguments);
	},
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onDeleteSkillset(grid, rowIndex, colIndex);
	},	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onEditSkillset(grid, rowIndex, colIndex);
	}
});