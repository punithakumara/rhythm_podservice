Ext.define('AM.view.employee.View',{
	extend : 'Ext.form.Panel',
	alias : 'widget.employeeview',
	title: 'View Employee details',
	layout: 'fit',
	width: '99%',
	id: 'employ_view_layout',
	border: true,
    height:'auto',
    
    initComponent : function() {
    	var Av = Ext.getStore('Vertical').load({params: {hasNoLimit: '1'}});
    	this.items = [{
			xtype : 'form',
			layout: 'vbox',
			itemId : 'addEmployeeFrm',
			bodyStyle : {
				padding : '15px'
			},
			fieldDefaults: {
				labelAlign: 'right',
				labelWidth: 140,
			},
			items:[{
				layout: 'hbox',
				width: '100%',
				items:[{
					xtype: 'fieldset',
					title: 'Official Details',
					margin: '0 10 10 0',
					width: '50%',
					items: [{
						xtype : 'hiddenfield',
						id: 'viewAssignedVertical',
						allowBlank: true,
					},{
						xtype : 'hiddenfield',
						id: 'viewEmployID',
						allowBlank: true,
					},{
						xtype : 'hiddenfield',
						id: 'viewEmployGrade',
						allowBlank: true,
					},{
						xtype : 'hiddenfield',
						id: 'viewDesignationId',
						allowBlank: true,
					},,{
						xtype : 'hiddenfield',
						id: 'viewGradeLevel',
						allowBlank: true,
					},{
						xtype: 'displayfield',
						fieldLabel: 'Employee ID <span style="color:red">*</span>',
						name : 'company_employ_id',
						anchor: '100%'
					}, {
						xtype: 'displayfield',
						fieldLabel: 'Employee Name <span style="color:red">*</span>',
						name: 'full_name',
						width:'50%',
					}, {
						xtype: 'displayfield',
						name : 'doj',
						id : 'view_doj',
						fieldLabel: 'Joining Date <span style="color:red">*</span>',
						anchor: '100%'
					}, {
						xtype: 'displayfield',
						name : 'email',
						fieldLabel: 'Email <span style="color:red">*</span>',
						anchor: '100%',
					}, {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						combineErrors: true,
						defaults: {
							anchor: '100%'
						},
						items:[{
							xtype: 'displayfield',
							fieldLabel: 'Grade <span style="color:red">*</span>',
							name: 'Grade',
							id:'viewGrade',
							width:'48%',
						}, {
							xtype: 'displayfield',
							fieldLabel: 'Designation <span style="color:red">*</span>',
							name: 'Designation_name',
							id:'viewDesignation',
							margins: '0 0 0 6',
							width:'52%',
						}]
					}, {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						combineErrors: true,
						defaults: {
							anchor: '100%'
						},
						items:[{
							xtype: 'displayfield',
							fieldLabel: 'Vertical <span style="color:red">*</span>',
							name: 'vertical_name',
							width:'48%',
						}, {
							xtype: 'displayfield',
							fieldLabel: 'Supervisor <span style="color:red">*</span>',
							name: 'lead_name',
							margins: '0 0 0 6',
							width:'52%',
						}]
					}, {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						combineErrors: true,
						defaults: {
							anchor: '100%'
						},
						items:[{
							xtype: 'displayfield',
							fieldLabel: 'Shift <span style="color:red">*</span>',
							name: 'shift_code',
							width:'48%',
						}, {
							xtype: 'displayfield',
							fieldLabel: 'Location <span style="color:red">*</span>',
							name: 'location_name',
							margins: '0 0 0 6',
							width:'52%',
						}]
					}, {
						xtype: 'displayfield',
						name : 'service_name',
						fieldLabel: 'Service <span style="color:red">*</span>',
						anchor: '100%',
					}]
				}, {
					xtype: 'fieldset',
					title: 'Present Address',
					width: '49%',
					items: [{
						xtype: 'displayfield',
						name : 'present_address',
						fieldLabel: 'Address <span style="color:red">*</span>',
						anchor: '100%'
					},{
						xtype: 'displayfield',
						name: 'present_city',
						fieldLabel: 'City <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
					},{
						xtype: 'displayfield',
						name: 'present_state',
						fieldLabel: 'State <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
					},{
						xtype: 'displayfield',
						name: 'present_zipcode',
						fieldLabel: 'Zip Code <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
					}]
				}]
			}, {
				layout: 'hbox',
				width: '100%',
				items:[{
					xtype: 'fieldset',
					title: 'Personal Details',
					margin: '0 10 0 0',
					width: '50%',
					items: [{
						xtype: 'displayfield',
						name : 'dob',
						fieldLabel: 'Birth Date <span style="color:red">*</span>',
						anchor: '100%'
					},{
						xtype      : 'displayfield',
						fieldLabel : 'Gender <span style="color:red">*</span>',
						width: '50%',
						name: 'gender'
					}, {
						xtype: 'displayfield',
						name : 'personal_email',
						fieldLabel: 'Personal Email <span style="color:red">*</span>',
						anchor: '100%',
					}, {
						xtype: 'fieldcontainer',
						layout: 'hbox',
						defaults: {
							anchor: '100%'
						},
						items:[{
							xtype: 'displayfield',
							fieldLabel: 'Contact Number <span style="color:red">*</span>',
							name: 'mobile',
							width:'48%',
						}, {
							xtype: 'displayfield',
							fieldLabel: 'Alt. Contact Number <span style="color:red">*</span>',
							name: 'alternate_number',
							margins: '0 0 0 6',
							width:'52%',
						}]
					}, {
						xtype : 'displayfield',
						fieldLabel: 'Qualification <span style="color:red">*</span>',
						name: 'qualification_name',
						anchor: '100%'
					}]
				}, {
					xtype: 'fieldset',
					title: 'Permanent Address',
					width: '49%',
					items: [{
						xtype: 'displayfield',
						name : 'permanent_address',
						fieldLabel: 'Address <span style="color:red">*</span>',
						anchor: '100%'
					},{
						xtype: 'displayfield',
						name: 'permanent_city',
						fieldLabel: 'City <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
					},{
						xtype: 'displayfield',
						name: 'permanent_state',
						fieldLabel: 'State <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
					},{
						xtype: 'displayfield',
						name: 'permanent_zipcode',
						fieldLabel: 'Zip Code <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
					}]
				}]
			}]
		}]

		this.tools = [{
			xtype: 'button',
			text: 'Promote',
			id: 'empPromote',
			icon : AM.app.globals.uiPath+'resources/images/menu/Promotion.png',
			listeners: {
				click: function() {
					AM.app.getController("Employees").onPromoteEmployee();
				}
			}
		},{
			xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
		},{
			xtype: 'button',
			text:'Resign',
			id: 'empResign',
			icon : AM.app.globals.uiPath+'resources/images/menu/Resign.png',
			listeners: {
				click: function() {
					AM.app.getController("Employees").onResignEmployee();
				}
			}
		},{
			xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
		},{
			xtype: 'button',
			text:'Relieve',
			id: 'empRelieve',
			icon : AM.app.globals.uiPath+'resources/images/menu/Relieve.png',
			listeners: {
				click: function() {
					AM.app.getController("Employees").onRelieveEmployee();
				}
			}
		}];
		
		this.buttons = ['->', {
		   text : 'Back',
		   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
		   handler : this.onCancel
		}]

		this.callParent(arguments);
	},
	
	onCancel : function() {
		AM.app.getController('Employees').viewContent();
	},
});