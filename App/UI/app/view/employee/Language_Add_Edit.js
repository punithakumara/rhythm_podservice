Ext.define('AM.view.employee.Language_Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.languageAddEdit',
	layout : 'fit',
	width : '40%',
	autoShow : true,
	autoSave: false,
	autoHeight : true,
	modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'addLanguageFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'id',
				id : 'id',
			},
			{
				xtype		:'combobox',
				multiSelect	: false,
				store		: Ext.getStore('LanguagesList'),								
				fieldLabel	: 'Language <span style="color:red">*</span>',
				allowBlank	: false,	
				name		: 'language',
				queryMode	: 'remote',
				forceSelection: true,
				minChars	: 0,
				displayField: 'language', 
				valueField	: 'language_id',
				id			: 'language_id',
				emptyText	: 'Select Language',
				margins		: 2,
				flex		: 1,
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					}
				}
			}
			, {
				xtype : 'combobox',
				multiSelect: false,
				name : 'proficiency',
				itemId : 'proficiency',
				allowBlank: false,
				fieldLabel : 'Select Proficiency <span style="color:red">*</span>',
				anchor: '100%',
				queryMode	: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: ['level', 'value'],
					data: [
					{'level': 'Beginner', 'value': 'Beginner'},
					{'level': 'Intermediate', 'value': 'Intermediate'},
					{'level': 'Expert', 'value': 'Expert'},					
					]
				}),
				displayField: 'level', 
				valueField: 'value',
				/*listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}*/
			}, {
				xtype : 'combobox',
				multiSelect: false,
				name : 'read',
				itemId : 'read',
				allowBlank: false,
				fieldLabel : 'Read <span style="color:red">*</span>',
				anchor: '100%',
				queryMode	: 'local',
					forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: ['label', 'value'],
					data: [
					{'label': 'Yes', 'value': '1'},
					{'label': 'No', 'value': '0'}				
					]
				}),
				displayField: 'label', 
				valueField: 'value',
				/*listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}*/
			}, {
				xtype : 'combobox',
				multiSelect: false,
				name : 'write',
				itemId : 'write',
				allowBlank: false,
				fieldLabel : 'Write <span style="color:red">*</span>',
				anchor: '100%',
				queryMode	: 'local',
					forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: ['label', 'value'],
					data: [
					{'label': 'Yes', 'value': '1'},
					{'label': 'No', 'value': '0'}				
					]
				}),
				displayField: 'label', 
				valueField: 'value',
				/*listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}*/
			}, {
				xtype : 'combobox',
				multiSelect: false,
				name : 'speak',
				itemId : 'speak',
				allowBlank: false,
				fieldLabel : 'Speak <span style="color:red">*</span>',
				anchor: '100%',
				queryMode	: 'local',
					forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: ['label', 'value'],
					data: [
					{'label': 'Yes', 'value': '1'},
					{'label': 'No', 'value': '0'}				
					]
				}),
				displayField: 'label', 
				valueField: 'value',
				/*listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}*/
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()) {
			win.fireEvent('saveLanguage', form, win);
		}
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});