Ext.define('AM.view.employee.OtherAchievements' ,{
    extend: 'Ext.Panel',
    alias: 'widget.OtherAchievements',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me=this;
		Ext.apply(me, {
			store : Ext.create('AM.store.EmployeeAchievement',{
				remoteSort: true,
				autoLoad: true,
			})
        }); 
		me.store.getProxy().extraParams = {'employee_id': AM.app.globals.viewIndEmp} 
		
		this.items =[{
			xtype: 'grid',
			id: 'otherAchiGrid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Name of the Award', 
				dataIndex: 'award_name',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1
			}, {
				header: 'Year', 
				dataIndex: 'year', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			}, {
				header: 'Expiry Month', 
				dataIndex: 'expiry_month', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			}, {
				header: 'Expiry Year', 
				dataIndex: 'expiry_year', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			},{
				header: 'Link', 
				dataIndex: 'link', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1,
			},{
				header: 'Action', 
				xtype : 'actioncolumn',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				items: [{
					icon : AM.app.globals.uiPath+'/resources/images/edit.png',
					tooltip : 'Edit',
					handler : this.onEdit
				}, {
					xtype: 'tbspacer'
				}, {
					icon : AM.app.globals.uiPath+'/resources/images/delete.png',
					tooltip : 'Delete',
					handler : this.onDelete
				}]
			}],
        }]
		this.buttons = ['', {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'resources/images/Add.png',
		text : 'Add Achievement',
		id : 'AddAchievement',
		style : { 'margin' : '5px'},
		handler: function(){
			AM.app.getController('Employees').onCreateAchievement();
		}
	}];
		
        this.callParent(arguments);
    },
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onDeleteAchievement(grid, rowIndex, colIndex);
	},	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onEditAchievement(grid, rowIndex, colIndex);
	}
});