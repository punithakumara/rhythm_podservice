Ext.define('AM.view.employee.WorkExperience' ,{
    extend: 'Ext.Panel',
    alias: 'widget.WorkExperience',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me=this;
		Ext.apply(me, {
			store : Ext.create('AM.store.EmployeeProfile',{
				remoteSort: true,
				autoLoad: true,
			})
        });
		me.store.getProxy().extraParams = {'employee_id': AM.app.globals.viewIndEmp}
		
		this.items =[{
			xtype: 'grid',
			id: 'experGrid',
			store : me.store,
			autoScroll: true,
			columns : [{
				header: 'Organization Name', 
				dataIndex: 'org_name',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5
			}, {
				header: 'Work Period', 
				dataIndex: 'work_period', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			},{
				header: 'Designation', 
				dataIndex: 'designation', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			},{
				header: 'Description', 
				dataIndex: 'job_profile', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1,
			},{
				header: 'Action', 
				xtype : 'actioncolumn',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				items: [{
					icon : AM.app.globals.uiPath+'/resources/images/edit.png',
					tooltip : 'Edit',
					handler: function(grid, rowIndex, colIndex){
						AM.app.getController('Employees').onEditExperience(grid, rowIndex, colIndex);
					},
				}, {
					xtype: 'tbspacer'
				}, {
					icon : AM.app.globals.uiPath+'/resources/images/delete.png',
					tooltip : 'Delete',
					handler: function(grid, rowIndex, colIndex) {
						AM.app.getController('Employees').deleteExperience(grid, rowIndex, colIndex);
					},
				}]
			}],
        }]
		
		this.buttons = ['', {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'resources/images/Add.png',
		text : 'Add Experience',
		id : 'AddExperience',
		style : { 'margin' : '5px'},
		handler: function(){
			AM.app.getController('Employees').onCreateWorkExperience();
		},
	}];
		
        this.callParent(arguments);
    },
});