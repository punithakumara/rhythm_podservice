Ext.define('AM.view.employee.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.employeeList',
    title: 'Employee List',
    store : 'Employees',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},	
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
    frame: true,
    width: "99%",
    height: 'auto',
    style: {
        border: "0px solid #157fcc",
        borderRadius:'0px'
    },
    initComponent: function() {
        this.columns = [{	
			header: 'Employee ID', 
			dataIndex: 'company_employ_id',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 0.7
		}, {
			header: 'Employee Name',  
			dataIndex: 'full_name',
			menuDisabled:true,
			groupable: false,
			draggable: false,  
			flex: 1.4
		}, {
			header : 'Designation', 
			dataIndex : 'Designation_name',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 1.3
		}, {
			header: 'Email', 
			dataIndex: 'email',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 1.5
		}, {
			header: 'Vertical', 
			dataIndex: 'vertical_name',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 1
		},{
			header: 'Shift', 
			dataIndex: 'shift_code',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 0.7
		},{
			header: 'Location', 
			dataIndex: 'location_name',
			menuDisabled:true,
			groupable: false,
			draggable: false, 
			flex: 0.6
		},{
			header : 'Action', 
			width : '6.5%',
			xtype : 'actioncolumn',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			
			items : [{
				id : 'vewid',
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit Employee Profile',
				handler : this.onEdit,
				getClass: function(v, meta, record)
				{	if(Ext.util.Cookies.get('grade')==2 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==206)
					{
						return 'rowVisible';
					}
					else if(Ext.util.Cookies.get('grade')==2 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==207)
					{
						return 'rowVisible';
					}				
					else if(Ext.util.Cookies.get('grade')>4 && record.data.status!=6 || Ext.util.Cookies.get('VertId')=='10')
					{
						return 'rowVisible';
					}
					else
					{ 
						return 'x-hide-display';
					}
				},
			}, '-' ,{
				icon : AM.app.globals.uiPath+'resources/images/view.png',
				tooltip : 'View Employee',
				handler : this.onView
			}]
		}];		
      
        this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Employees to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });

        this.callParent(arguments);
      
    },
    
    tools : [{
		xtype: 'combobox',
		itemId : 'empStatusCombo',
		store: Ext.create('Ext.data.Store', {
			fields: ['status', 'empStatus'],
			data : [{'status': '0', 'empStatus': 'Active'}, {'status': '1', 'empStatus': 'InActive'},{'status': '2', 'empStatus': 'Resigned'}]
		}),
		displayField: 'empStatus', 
		valueField: 'status',
		value: '0',
		listeners: {
			select: function(combo){
				this.fireEvent('changeStatusCombo', combo);
			}
		}
	},{ 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'resources/images/Add.png',
		text : 'Add Employee',
		style : { 'margin-right' : '10px'},
		handler: function(){
			AM.app.getController('Employees').onCreate('');
		},
		afterRender: function() {
			if(Ext.util.Cookies.get('grade')==2 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==206)
			{
				this.show();
			}
			else if(Ext.util.Cookies.get('grade')==2 && Ext.util.Cookies.get('is_onshore')==1 && Ext.util.Cookies.get('Designation')==207)
			{
				this.show();
			}		
			else if(Ext.util.Cookies.get('grade')>4 || Ext.util.Cookies.get('VertId')=='10')
			{
				this.show();
			}
			else
			{
				this.hide();
			}
		}
	}],
    
	onDelete : function(grid, rowIndex, colIndex) {
        this.up('grid').fireEvent('deleteEmployee', grid, rowIndex, colIndex);
	},
	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController("Employees").onEditEmployee(grid, rowIndex, colIndex);
	},
	
	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController("Employees").onViewEmployee(grid, rowIndex, colIndex);
	},
	
});