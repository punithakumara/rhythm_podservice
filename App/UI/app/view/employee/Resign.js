Ext.define('AM.view.employee.Resign',{
	extend : 'Ext.window.Window',
	alias: 'widget.Resign',
	title: 'Employee - Resign',
	constrain:  true,
	layout: 'fit',
	width: '40%',
    autoShow:  true,
    autoSave:  false,
    maxHeight:  700,
    autoScroll:  true,
    modal:  true,
    initComponent 	: function() {
    	this.items = [ {
    		xtype: 'form',
    		id: 'EmpResignation',
    		fieldDefaults: { 
				labelAlign	: 'right',
				labelWidth: 140, 
				anchor  : '100%'
			},
    		bodyStyle: { 
				border : 'none', 
				display : 'block', 
				float : 'left',
				padding : '10px'
			},
    		autoScroll: true,
    		autoHeight: true,
    		items: [ {
				xtype: 'datefield',
				format:  AM.app.globals.CommonDateControl,
				name: 'resigned_date',
				id: 'resigned_date',
				allowBlank:  false,
				fieldLabel: 'Resignation Date <span style="color:red">*</span>',
				minValue: Ext.getCmp('view_doj').getValue(),
				maxValue: new Date(),
				value: new Date(),
				anchor: '100%'
			}, {
				xtype: 'textareafield',
				grow: true,
				fieldLabel: 'Resignation Reason <span style="color:red">*</span>',									 
				id: 'resignation_reason',							
				name: 'resignation_reason',
				allowBlank: false,
				anchor: '100%',
				width: 260,
				enableKeyEvents	: true,
				listeners: {
					keypress: function() {
						Ext.getCmp('savebtn').setDisabled(false);
					}
				}
			}]
		} ];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	onCancel : function() {
		this.up('window').close();
	},
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();
		
		if (form.isValid()) 
		{
        	AM.app.getController('Employees').onResignation(form, win);
        }
	},
} );