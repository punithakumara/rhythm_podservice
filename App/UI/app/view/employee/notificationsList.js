Ext.define('AM.view.employee.notificationsList123' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.EmpNotificationsList123',
	store: 'Notifications',
	
    layout : 'fit',
    loadMask: true,
    autoCreate: true,
    border : true,
	title: 'List of Notifications',
	minHeight:300,
	autoScroll: true,
	width:'99%',
	height:300,
	
    initComponent: function() {
	var me = this;
		/*this.selModel = Ext.create('Ext.selection.CheckboxModel', {
			//allowDeselect: false,
			checkOnly: true,
			mode:'MULTI',
			listeners: {
                beforeselect: function(selModel, record, index){
                    // console.log('sm::beforeselect');   
                },
                select: function(){
                    // console.log('sm::select');                    
                }
            }
		});*/
        this.columns = [
			{
				header: 'NotificationID',  
				dataIndex: 'NotificationID',  
				flex: 3
			},
            {
            	header: 'Title',  
            	dataIndex: 'Title',  
            	flex: 3
            },
            {
            	header: 'Description',  
            	dataIndex: 'Description',  
            	flex: 3
            },{
            	header: 'From', 
        		dataIndex: 'SenderEmployee', 
        		flex: 3
            }
			
        ];
       
        /*this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
			id:'peopleTransPagTool',
			cls:'peopleTool',
			prependButtons: false,
            // pageSize: 38,
            displayMsg: 'Total {2} Employees',
            emptyMsg: "No employees to display"
			
        });
        
        
            this.tools = [{
	        	 xtype : 'combobox',
				 multiSelect: false,
				 id: 'trainingStatusToFliter',
				 name: 'training_status',
				 minChars: 0,
				 //store: Ext.getStore('TrStatus'),
				 store: 'TrStatus',
				 queryMode: 'local',
				 displayField: 'name',
				 valueField: 'id',
				 width : 300,
				 labelWidth : 50,
				 labelCls : 'searchLabel',
				 style: {
					'margin-right' : '5px'
					},
				 fieldLabel: 'Training Status',
				 labelWidth : '60%',
				 emptyText:"Select Status",
				 anchor: '100%',
				 //value: 'All',
				 
				 listeners: {
						
						select:function(){
						//console.log(Ext.getCmp('trPrimaryForm'));
								var prifrm = Ext.getCmp('trPrimaryForm');
								AM.app.getController('Transition').filterGridForTrStatus(prifrm, me);
								Ext.getCmp('transtionFirstgrid').getPlugin('pagingSelectionPersistence').clearPersistedSelection();
							
							},
						change:function(){
								if(me.getEl() != undefined){
								
									var prifrm = Ext.getCmp('trPrimaryForm');
									//var trstatus = Ext.getCmp('trainingStatusToFliter').getValue();
									var trstatus = me.query('combobox')[0].getValue();
									if(trstatus == null){
										AM.app.getController('Transition').filterGridForTrStatus(prifrm, me);
									}
								}	
							}
							
					}
       	  	},
			{ 
			xtype : 'tbseparator',
			style : {
				'margin-right' : '10px'
			}
			}]*/
        
        
        this.callParent(arguments);
		//this.plugins
    }
});