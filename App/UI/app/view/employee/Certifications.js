Ext.define('AM.view.employee.Certifications' ,{
    extend: 'Ext.Panel',
    alias: 'widget.Certifications',
    layout : {
		type: 'card',
		deferredRender: true
	},
    loadMask: true,
    autoCreate: true,
	height:275,
    initComponent: function() {

		var me=this;
		Ext.apply(me, {
			store : Ext.create('AM.store.Certifications',{
				remoteSort: true,
				autoLoad: true,
			})
        });
		me.store.getProxy().extraParams = {'employee_id': AM.app.globals.viewIndEmp}
		
		this.items =[{
			xtype: 'grid',
			id: 'certGrid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Certification Name', 
				dataIndex: 'certificate_name',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1
			}, {
				header: 'Certification Body', 
				dataIndex: 'content', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			},{
				header: 'Year', 
				dataIndex: 'year', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			},{
				header: 'Action', 
				xtype : 'actioncolumn',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				items: [{
					icon : AM.app.globals.uiPath+'/resources/images/edit.png',
					tooltip : 'Edit',
					handler : this.onEdit
				}, {
					xtype: 'tbspacer'
				}, {
					icon : AM.app.globals.uiPath+'/resources/images/delete.png',
					tooltip : 'Delete',
					handler : this.onDelete,
				}]
			}],
        }]
		this.buttons = ['', {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'resources/images/Add.png',
		text : 'Add Certificate',
		id : 'AddCertificate',
		style : { 'margin' : '5px'},
		handler: function(){
			AM.app.getController('Employees').onCreateCertificate();
		}
	}];
		
        this.callParent(arguments);
    },
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onDeleteCertificate(grid, rowIndex, colIndex);
	},	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onEditCertificate(grid, rowIndex, colIndex);
	}
});