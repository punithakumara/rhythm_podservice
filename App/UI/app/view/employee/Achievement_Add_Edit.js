Ext.define('AM.view.employee.Achievement_Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.achievementAddEdit',
	layout : 'fit',
	width : '40%',
	autoShow : true,
	autoSave: false,
	autoHeight : true,
	modal : true,
	
	initComponent : function() {
		
		this.items = [{
			xtype : 'form',
			itemId : 'addAchievementFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'id',
				id : 'id',
			},{
				xtype : 'textfield',
				name : 'award_name',
				itemId : 'award_name',
				allowBlank: false,
				fieldLabel : 'Name of the Award <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 50, 
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
				xtype: 'combobox',
				name : 'year',
				//allowBlank: false,
				fieldLabel: 'Year',
				margins	  : '0 0 0 6',
				width:'49.5%',
				queryMode	: 'local',
				forceSelection: true,
				store: Ext.create('Ext.data.Store', {
					fields: ['Value'],
					data: [
					{'Value': '2005'},
					{'Value': '2006'},
					{'Value': '2007'},
					{'Value': '2008'},
					{'Value': '2009'},
					{'Value': '2010'},
					{'Value': '2011'},
					{'Value': '2012'},
					{'Value': '2013'},
					{'Value': '2014'},
					{'Value': '2015'},
					{'Value': '2016'},
					{'Value': '2017'},
					{'Value': '2018'},
					{'Value': '2019'},
					{'Value': '2020'},
					{'Value': '2021'}
					]
				}),
				displayField: 'Value', 
				valueField: 'Value'
			}, {
				xtype : 'textfield',
				name : 'link',
				itemId : 'link',
				//allowBlank: false,
				fieldLabel : 'Link',
				anchor: '100%'			
			},{				
				xtype : 'checkboxfield',
				name : 'checkExpiryDate',
				id : 'checkExpiryDate',
				boxLabel : 'This Certificate does not expire',
				labelWidth: 200,
				checked:true,
				handler: function(obj) {
					if(obj.checked)
					{
						Ext.getCmp('expiry_year').setDisabled(true);
						Ext.getCmp('expiry_month').setDisabled(true);
					}
					else
					{
						Ext.getCmp('expiry_month').reset();
						Ext.getCmp('expiry_year').reset();
						Ext.getCmp('expiry_year').setDisabled(false);
						Ext.getCmp('expiry_month').setDisabled(false);
					}
				}
			},{
				xtype: 'fieldcontainer',
				itemId: 'expiry_date',
				fieldLabel: 'Expiry Date',
				id:"expiry_date",
				hidden:false,
				layout: 'hbox',
				combineErrors: true,
				defaultType: 'combobox',
				defaults: {
					hideLabel: 'true',
					anchor: '100%'
				},
				items:[{
					name : 'expiry_month',
					id : 'expiry_month',
					disabled: true,
					emptyText : 'Month',
					width:'50%',
					queryMode	: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: ['Value'],
						data: [
						{'Value': 'Jan'},
						{'Value': 'Feb'},
						{'Value': 'Mar'},
						{'Value': 'Apr'},
						{'Value': 'May'},
						{'Value': 'Jun'},
						{'Value': 'Jul'},
						{'Value': 'Aug'},
						{'Value': 'Sep'},
						{'Value': 'Oct'},
						{'Value': 'Nov'},
						{'Value': 'Dec'}
						]
					}),
					displayField: 'Value', 
					valueField: 'Value'
				}, {
					xtype: 'combobox',
					name : 'expiry_year',
					disabled: true,
					id:"expiry_year",
					emptyText : 'Year',
					margins	  : '0 0 0 6',
					width:'49.5%',
					queryMode	: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: ['Value'],
						data: [
						{'Value': '2005'},
						{'Value': '2006'},
						{'Value': '2007'},
						{'Value': '2008'},
						{'Value': '2009'},
						{'Value': '2010'},
						{'Value': '2011'},
						{'Value': '2012'},
						{'Value': '2013'},
						{'Value': '2014'},
						{'Value': '2015'},
						{'Value': '2016'},
						{'Value': '2017'},
						{'Value': '2018'},
						{'Value': '2019'},
						{'Value': '2020'}
						]
					}),
					displayField: 'Value', 
					valueField: 'Value'
				}]
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields is mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()) {
				win.fireEvent('saveAchievement', form, win);		
		}
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});