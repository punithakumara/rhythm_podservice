Ext.define('AM.view.employee.Edit',{
	extend : 'Ext.form.Panel',
	title: 'Employee details',
	layout: 'fit',
	title: 'Edit Employee Details',
    viewConfig: {
		forceFit: true
	},
	width: '99%',
	id: 'employ_edit_layout',
	border: true,
    height: 'auto',
    
    initComponent : function() {
    	var Av = Ext.getStore('Vertical').load({params: {hasNoLimit: '1',ComboNoVerticalID:'20'}});
    	this.items = [{
			xtype : 'form',
			layout: 'vbox',
			itemId : 'addEmployeeFrm',
			bodyStyle : {
				padding : '10px'
			},
			fieldDefaults: {
				labelAlign: 'right',
				labelWidth: 140,
			},
			method: 'POST',
			items:[{
				layout: 'hbox',
				width: '100%',
				items:[{
					xtype: 'fieldset',
					title: 'Official Details',
					margin: '0 10 10 0',
					width: '50%',
					items: [{
						xtype: 'hiddenfield',
						name : 'employee_id',
					}, {
						xtype: 'textfield',
						name : 'company_employ_id',
						id: 'EmpID',
						allowBlank: false,
						regex: /[0-9|A-Z|a-z][a-zA-Z0-9]?[-]?[0-9]/,
						maxLength : 8,
						enforceMaxLength: true,
						invalidText : 'Employ ID can be Number Or Alphanumeric',
						fieldLabel: 'Employee ID <span style="color:red">*</span>',
						emptyText: 'Enter Employee ID',
						anchor: '100%',
						listeners: {
                                keyup: function(field) {
                                    var value = field.getValue();
                                    if (value.length && !value.match(field.config.regex)) {
                                        field.setStyle('border: 1px solid red;');
                                    } else {
                                        field.setStyle('border: 0;');
                                    }
                                }
                            }
					}, {
						xtype: 'fieldcontainer',
						fieldLabel: 'Employee Name <span style="color:red">*</span>',
						layout: 'hbox',
						combineErrors: true,
						defaultType: 'textfield',
						defaults: {
							hideLabel: 'true',
							anchor: '100%'
						},
						items:[{
							name: 'first_name',
							id: 'first_name',
							maskRe: /[A-Za-z ]/,
							emptyText: 'Enter First Name',
							maxLength: 20,
							width:'50%',
							listeners:{
								scope: this,
								'blur': function(combo){
									var val = combo.getValue().trim();
									val = val.replace(/[^a-zA-Z 0-9]+/g,'');

									combo.setValue(val);
								}
							},
							allowBlank: false
						}, {
							name: 'last_name',
							id: 'LName',
							maskRe: /[A-Za-z ]/,
							margins: '0 0 0 6',
							emptyText: 'Enter Last Name',
							maxLength: 20,
							width:'49.5%',
							listeners:{
								scope: this,
								'blur': function(combo){
									var val = combo.getValue().trim();
									val = val.replace(/[^a-zA-Z 0-9]+/g,'');

									combo.setValue(val);
								}
							},
							allowBlank: false
						}]
					}, {
						xtype: 'datefield',
						format: AM.app.globals.CommonDateControl,
						name : 'doj',
						maxValue: new Date(),
						id : 'doj',
						allowBlank: false,
						readOnly: true,
						emptyText: 'Select Joining Date',
						fieldLabel: 'Joining Date <span style="color:red">*</span>',
						anchor: '100%'
					}, {
						xtype: 'textfield',
						name : 'email',
						id : 'email',
						vtype: 'email',
						emptyText : 'email@theoreminc.net',
						mask: { text: '@theoreminc.net', placeholder: '#', includeInValue: true },
						allowBlank: false,
						fieldLabel: 'Email <span style="color:red">*</span>',
						anchor: '100%',
						enableKeyEvents: true,
						listeners : {
							'blur' : this.changeEmail
						}
					}, {
						xtype: 'fieldcontainer',
						fieldLabel: 'Grade & Designation <span style="color:red">*</span>',
						layout: 'hbox',
						combineErrors: true,
						defaultType: 'displayfield',
						defaults: {
							hideLabel: 'true',
							anchor: '100%'
						},
						items:[{
							name: 'Grade',
							width:'50%',
						}, {
							name: 'Designation_name',
							margins: '0 0 0 6',
							width:'49.5%',
						}]
					}, {
						xtype: 'fieldcontainer',
						fieldLabel: 'Vertical & Supervisor <span style="color:red">*</span>',
						layout: 'hbox',
						combineErrors: true,
						defaultType: 'displayfield',
						defaults: {
							hideLabel: 'true',
							anchor: '100%'
						},
						items:[{
							name: 'vertical_name',
							width:'50%',
							value: 'Product Engineering Group'
						}, {
							name: 'lead_name',
							margins: '0 0 0 6',
							width:'49.5%',
						}]
					}, {
						xtype: 'fieldcontainer',
						fieldLabel: 'Shift & Location <span style="color:red">*</span>',
						layout: 'hbox',
						combineErrors: true,
						defaults: {
							hideLabel: 'true',
							anchor: '100%'
						},
						items:[{
							xtype: 'displayfield',
							name: 'shift_code',
							width:'50%',
						}, {
							xtype: 'combobox',
							id: 'OfficeLocation',
							name: 'location_id',
							store: 'Locations',
							queryMode: 'remote',
							minChars:0,
							displayField: 'name', 
							valueField: 'location_id',
							emptyText : 'Select location',
							allowBlank: false,
							margins: '0 0 0 6',
							width:'49.5%',
							listeners:{
								scope: this,
								'blur': function(combo){
									combo.setValue(combo.getValue().trim());
								}
							},
						}]
					}, {
						xtype : 'combobox',
						fieldLabel: 'Service <span style="color:red">*</span>',
						id: 'service',
						name: 'service_id',
						store: 'Services',
						queryMode: 'remote',
						minChars:0,
						displayField: 'name', 
						valueField: 'service_id',
						emptyText : 'Select Service',
						allowBlank: false,
						//forceSelection: true,
						listeners:{
							scope: this,
							'blur': function(combo){
								combo.setValue(combo.getValue().trim());
							}
						},
						anchor: '100%'
					}]
				}, {
					xtype: 'fieldset',
					title: 'Present Address',
					width: '49%',
					items: [{
						xtype: 'textareafield',
						name : 'present_address',
						allowBlank: false,
						//maskRe: /[A-Za-z0-9- ]/,
						maskRe: /^[a-zA-Z0-9\s\.\/]+$/,
						fieldLabel: 'Address <span style="color:red">*</span>',
						labelWidth: 100,
						anchor: '100%',
						listeners: {
							scope: this,
							change: this.onPermanentAddrFieldChange,
							blur: function(combo){
								combo.setValue(combo.getValue().trim());
							}
						},
						addrFieldName: 'permanent_address'
					},{
						xtype: 'textfield',
						name: 'present_city',
						maskRe: /[A-Za-z ]/,
						fieldLabel: 'City <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
						emptyText: 'Enter City Name',
						allowBlank: false,
						labelWidth: 100,
						listeners: {
							scope: this,
							change: this.onPermanentAddrFieldChange,
							blur: function(combo){
								combo.setValue(combo.getValue().trim());
							}
						},
						addrFieldName: 'permanent_city'
					},{
						xtype: 'textfield',
						name: 'present_state',
						maskRe: /[A-Za-z ]/,
						fieldLabel: 'State <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
						emptyText: 'Enter State Name',
						allowBlank: false,
						labelWidth: 100,
						listeners: {
							scope: this,
							change: this.onPermanentAddrFieldChange,
							blur: function(combo){
								combo.setValue(combo.getValue().trim());
							}
						},
						addrFieldName: 'permanent_state'
					},{
						xtype: 'textfield',
						name: 'present_zipcode',
						maskRe: /[0-9 ]/,
						maxLength : 6,
						enforceMaxLength: true,
						fieldLabel: 'Zip Code <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
						emptyText: 'Enter Zip Code',
						allowBlank: false,
						labelWidth: 100,
						listeners: {
							scope: this,
							change: this.onPermanentAddrFieldChange,
							blur: function(combo){
								combo.setValue(combo.getValue().trim());
							}
						},
						addrFieldName: 'permanent_zipcode'
					},{
						xtype: 'checkboxfield',
						boxLabel: 'Permanent Address same as Present Address',
						name: 'same_as',
						inputValue: '1',
						scope: this,
						handler: this.onSameAddressChange,
					}]
				}]
			}, {
				layout: 'hbox',
				width: '100%',
				items:[{
					xtype: 'fieldset',
					title: 'Personal Details',
					margin: '0 10 0 0',
					width: '50%',
					items: [{
						xtype: 'datefield',
						format: AM.app.globals.CommonDateControl,
						name : 'dob',
						maxValue: Ext.Date.add (new Date(),Ext.Date.YEAR,-15),
						allowBlank: false,
						emptyText: 'Select Birth Date',
						fieldLabel: 'Birth Date <span style="color:red">*</span>',
						anchor: '100%'
					},{
						xtype:'boxselect',
						multiSelect : false,
						allowBlank: false,
						fieldLabel: 'Gender <span style="color:red">*</span>',
						store: Ext.create('Ext.data.Store', {
							fields: ['val'],
							data: [
								{'val': 'Male'},
								{'val': 'Female'},
							]
						}),
						name:'gender',
						displayField: 'val',
						valueField: 'val',
						value:'',
						emptyText : 'Select Gender',
						anchor: '100%'
					}, {
						xtype: 'textfield',
						name : 'personal_email',
						// id : 'email',
						vtype: 'email',
						emptyText : 'Enter Personal Email',
						allowBlank: false,
						fieldLabel: 'Personal Email <span style="color:red">*</span>',
						anchor: '100%',
						enableKeyEvents: true,
					}, {
						xtype: 'fieldcontainer',
						fieldLabel: 'Contact Number <span style="color:red">*</span>',
						layout: 'hbox',
						combineErrors: true,
						defaultType: 'textfield',
						defaults: {
							hideLabel: 'true',
							anchor: '100%'
						},
						items:[{
							maskRe: /[0-9.]/,
							regex: /^\d{10}?$/,
							name : 'mobile',
							id : 'Mobile',
							maxLength : 10,
							enforceMaxLength: true,
							allowBlank: false,
							emptyText : 'Enter Primary Number',
							width:'50%',
						}, {
							xtype: 'textfield',
							maskRe: /[0-9.]/,
							regex: /^\d{10}?$/,
							name : 'alternate_number',
							// id : 'Mobile',
							maxLength : 10,
							enforceMaxLength: true,
							allowBlank: false,
							emptyText : 'Enter Alternate Number',
							margins	  : '0 0 0 6',
							width:'49.5%',
						}]
					}, {
						xtype : 'combobox',
						fieldLabel: 'Qualification <span style="color:red">*</span>',
						id: 'Qualification',
						name: 'qualification_id',
						store: 'Qualifications',
						queryMode: 'remote',
						minChars:0,
						displayField: 'name', 
						valueField: 'qualification_id',
						emptyText : 'Select Qualification',
						allowBlank: false,
						forceSelection: true,
						listeners:{
							scope: this,
							'blur': function(combo){
								combo.setValue(combo.getValue().trim());
							}
						},
						anchor: '100%'
					}, 
				{						
				xtype : 'checkboxfield',
				name : 'is_onshore',
				id : 'isOnshore',
				inputValue: '1',
				boxLabel : 'Onshore Team Member',
				labelWidth: 200,
				checked:false
				}
				]
				}, {
					xtype: 'fieldset',
					title: 'Permanent Address',
					width: '49%',
					items: [{
						xtype: 'textareafield',
						name : 'permanent_address',
						allowBlank: false,
						//maskRe: /[A-Za-z0-9- ]/,
						maskRe: /^[a-zA-Z0-9\s\.\/]+$/,
						fieldLabel: 'Address <span style="color:red">*</span>',
						anchor: '100%',
						labelWidth: 100,
					},{
						xtype: 'textfield',
						name: 'permanent_city',
						maskRe: /[A-Za-z ]/,
						fieldLabel: 'City <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
						emptyText: 'Enter City Name',
						allowBlank: false,
						labelWidth: 100,
					},{
						xtype: 'textfield',
						name: 'permanent_state',
						maskRe: /[A-Za-z ]/,
						fieldLabel: 'State <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
						emptyText: 'Enter State Name',
						allowBlank: false,
						labelWidth: 100,
					},{
						xtype: 'textfield',
						name: 'permanent_zipcode',
						maskRe: /[0-9 ]/,
						maxLength : 6,
						enforceMaxLength: true,
						fieldLabel: 'Zip Code <span style="color:red">*</span>',
						anchor: '100%',
						margins: '0 0 0 6',
						emptyText: 'Enter Zip Code',
						allowBlank: false,
						labelWidth: 100,
					}]
				}]
			}]
		}]

		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
		   text : 'Cancel',
		   id:'cancelbtn',
		   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
		   handler : this.onCancel
		}, {
		   text : 'Save',
		   id:'savebtn',
		   icon : AM.app.globals.uiPath+'resources/images/save.png',
		   handler : this.onSave
		}]

		this.callParent(arguments);
	},

	onSave :  function() {
		var form = this.up('form').getForm();
		if (form.isValid()) {
			AM.app.getController('Employees').onSaveEmployee( form)
		}
	},
	
	onCancel : function() {
		AM.app.getController('Employees').viewContent();
	},

    changeEmail : function(field) {
		var email = field.getValue();
		var email_array=email.split("@");
		var femail = email_array[0];
		femail = femail.replace(/[^a-zA-Z 0-9.]+/g,'');

		if(femail !='')
		{
			field.setValue(femail+'@theoreminc.net');
		}
		else
		{
			field.setValue('');
		}
	},
	
	onPermanentAddrFieldChange: function(field){
        var copyToAddr = this.down('[name=same_as]').getValue(),
            copyField = this.down('[name=' + field.addrFieldName + ']');
			
        if (copyToAddr) 
		{
            copyField.setValue(field.getValue());
        } 
		else 
		{
            copyField.clearInvalid();
        }
		copyField.setDisabled(copyToAddr);
		if (!Ext.isIE6) 
		{
			field.el.animate({opacity: copyToAddr ? 1 : 1});
		}
    },
	
	onSameAddressChange : function(box, checked){
        var fieldset = box.ownerCt;
		
        Ext.Array.forEach(fieldset.query('textfield'), this.onPermanentAddrFieldChange, this);
	},
});