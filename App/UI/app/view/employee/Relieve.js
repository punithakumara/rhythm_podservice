Ext.define('AM.view.employee.Relieve',{
	extend : 'Ext.window.Window',
	alias : 'widget.Relieve',
	title: 'Employee - Relieve',
	constrain:true,
	layout: 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    maxHeight:600,
    autoScroll: true,
    modal: true,
	
    initComponent : function() {
    	var RelieveType = new Ext.data.SimpleStore({
    	    fields: ['Flag', 'RelieveType'],
    	    data: [
    	        ['Resignation', 'Resignation'],
     	        ['Absconding', 'Absconding'],
     	        ['Termination', 'Termination'],
     	        ['Others', 'Others']
    	    ]
    	});
  	
    	this.items = [{
			xtype : 'form',
			id : 'EmpRelieve',
			fieldDefaults: {
				labelAlign: 'right',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			autoScroll : true,
			autoHeight : true,
			items : [{
				xtype: 'fieldset',
				id: 'EmpDeligacyFrame',
				layout: 'vbox',
				width:'100%',
				hidden: true,
				items : [{
					xtype : 'displayfield',
					value: 'The exiting employee has people reporting to him/her',
					labelWidth: 550,
				},{
					xtype : 'displayfield',
					value: 'Please select new reporting supervisor to whom these employees will report',
					labelWidth: 550,
				},{
					width:'100%',
					xtype : 'boxselect',
					multiSelect: false,
					name: 'DeligateTo',
					id: 'DeligateTo',
					displayField: 'Name', 
					valueField: 'employee_id',
					margin: '5 0 5 0',
					typeAhead: false,
					queryMode: 'local',
					minChar: 0,
					emptyText: 'Select Supervisor',
					anchor: '100%',
					flex:1,
				}]
			},{
				xtype: 'fieldset',
				id: 'EmpRelStatusFrame',
				title: 'Exit Status',
				style : {
					width : '100%'
				},
				items : [{
					xtype: 'datefield',
					format: AM.app.globals.CommonDateControl,
					name : 'RelieveDate',
					id : 'RelieveDate',
					displayField: 'RelieveDate',
					allowBlank: false,
					fieldLabel: 'Relieve Date <span style="color:red">*</span>',
					anchor: '100%',
					minValue: Ext.getCmp('view_doj').getValue(),
					maxValue: new Date(),
					value: new Date(),
				},{
					xtype : 'boxselect',
					multiSelect: false,
					store: RelieveType,
					fieldLabel: 'Relieve Type <span style="color:red">*</span>',
					name: 'RelieveType',
					queryMode: 'local',
					minChars:1,
					displayField: 'RelieveType', 
					valueField: 'Flag',
					value:'',
					allowBlank: false,
					anchor: '100%',
				},{
					xtype : 'textareafield',
					fieldLabel: 'Relieve Notes <span style="color:red">*</span>',
					name: 'RelieveNotes',
					allowBlank: false,
					anchor: '100%',
				}]
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		},{
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	onCancel : function() {
		this.up('window').close();
	},
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();
		
		if (form.isValid()) 
		{
        	AM.app.getController('Employees').onRelieve(form, win);
        }
	},
	
});