Ext.define('AM.view.employee.Skill_Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.skillAddEdit',
	layout : 'fit',
	width : '40%',
	autoShow : true,
	autoSave: false,
	autoHeight : true,
	modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'addSkillFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'id',
				id : 'id',
			},{
				xtype		:'combobox',
				multiSelect	: false,
				store		: Ext.getStore('Skills_Tools_Tech'),
				allowBlank	: false,				
				fieldLabel	: 'Skill <span style="color:red">*</span>',
				name		: 'skill_name',
				emptyText 	: 'Select Skill',
				queryMode	: 'remote',
				minChars	: 0,
				displayField: 'skill_name', 
				valueField	: 'skill_id',
				id			: 'skill_id',
				margins		: 2,
				flex		: 1,
				forceSelection: true,
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.store.proxy.extraParams = {
							'hasNoLimit':'1',
							'comboClient':'1',
							'filterName' : queryEvent.combo.displayField
						}
					}
				}
			}, {
				xtype : 'textfield',
				name : 'version',
				itemId : 'version',
				allowBlank: true,
				fieldLabel : 'Version',
				anchor: '100%',
				maxLength: 50, 
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
				xtype: 'fieldcontainer',
				fieldLabel: 'Last Used <span style="color:red">*</span>',
				layout: 'hbox',
				combineErrors: true,
				defaultType: 'combobox',
				defaults: {
					hideLabel: 'true',
					anchor: '100%'
				},
				items:[{
					name : 'last_used_month',
					id : 'last_used_month',
					allowBlank: false,
					emptyText : 'Month',
					width:'50%',
					queryMode	: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: ['Value'],
						data: [
						{'Value': 'Jan'},
						{'Value': 'Feb'},
						{'Value': 'Mar'},
						{'Value': 'Apr'},
						{'Value': 'May'},
						{'Value': 'Jun'},
						{'Value': 'Jul'},
						{'Value': 'Aug'},
						{'Value': 'Sep'},
						{'Value': 'Oct'},
						{'Value': 'Nov'},
						{'Value': 'Dec'}
						]
					}),
					displayField: 'Value', 
					valueField: 'Value'
				}, {
					xtype: 'combobox',
					name : 'last_used_year',
					id : 'last_used_year',
					allowBlank: false,
					emptyText : 'Year',
					margins	  : '0 0 0 6',
					width:'49.5%',
					queryMode	: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: ['Value'],
						data: [
						{'Value': '2005'},
						{'Value': '2006'},
						{'Value': '2007'},
						{'Value': '2008'},
						{'Value': '2009'},
						{'Value': '2010'},
						{'Value': '2011'},
						{'Value': '2012'},
						{'Value': '2013'},
						{'Value': '2014'},
						{'Value': '2015'},
						{'Value': '2016'},
						{'Value': '2017'},
						{'Value': '2018'},
						{'Value': '2019'},
						{'Value': '2020'},
						{'Value': '2021'},
						]
					}),
					displayField: 'Value', 
					valueField: 'Value'
				}]
			}, {
				xtype : 'combobox',
				multiSelect: false,
				name : 'experience',
				itemId : 'experience',
				emptyText : 'Select Experience',
				forceSelection: true,
				allowBlank: false,
				queryMode	: 'local',
				fieldLabel : 'Experience <span style="color:red">*</span>',
				anchor: '100%',
				store: Ext.create('Ext.data.Store', {
					fields: ['Flag', 'Level'],
					data: [
					{'Flag': '0', 'Level': '<1 Year'},
					{'Flag': '1', 'Level': '1-2 Years'},
					{'Flag': '2', 'Level': '2-3 Years'},
					{'Flag': '3', 'Level': '3-4 Years'},
					{'Flag': '4', 'Level': '4-5 Years'},
					{'Flag': '5', 'Level': '5+ Years'}
					]
				}),
				displayField: 'Level', 
				valueField: 'Flag',

			}, {
				xtype : 'combobox',
				multiSelect: false,
				emptyText : 'Select Self Rating',
				name : 'self_rating',
				itemId : 'self_rating',
				allowBlank: false,
				forceSelection: true,
				queryMode	: 'local',
				fieldLabel : 'Self Rating <span style="color:red">*</span>',
				anchor: '100%',
				store: Ext.create('Ext.data.Store', {
					fields: ['Rating', 'value'],
					data: [
					{'Rating': 'Beginner', 'value': '1'},
					{'Rating': 'Intermediate', 'value': '2'},
					{'Rating': 'Proficient', 'value': '3'},
					{'Rating': 'Advanced', 'value': '4'},
					{'Rating': 'Champion/Expert', 'value': '5'},
					]
				}),
				displayField: 'Rating', 
				valueField: 'value',
				/*listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}*/
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()) 
		{
			var last_used_month = Ext.getCmp('last_used_month').getValue();
			var last_used_year = Ext.getCmp('last_used_year').getValue();

			var date = new Date();
			var last_used_date = last_used_year + '-' + last_used_month + '-01';
			var end_date =	new Date(date.getFullYear(), date.getMonth() + 1, 0);
			
			if(Date.parse(end_date) < Date.parse(last_used_date))
			{
				Ext.MessageBox.show({
					title: 'Warning',
					msg: "Future dates not accepted",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING,
				});	
			}
			else
			{
				win.fireEvent('saveSkillset', form, win);
			}
		}
	},

	onReset : function() {
		this.up('form').getForm().reset();
	},

	onCancel : function() {
		this.up('window').close();
	}

});