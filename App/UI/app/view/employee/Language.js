Ext.define('AM.view.employee.Language' ,{
	extend: 'Ext.Panel',
	alias: 'widget.Language',
	layout : {
		type: 'card',
		deferredRender: true
	},
	loadMask: true,
	autoCreate: true,
	height:275,
	initComponent: function() {

		var me=this;
		Ext.apply(me, {
			store : Ext.create('AM.store.Languages',{
				remoteSort: true,
				autoLoad: true,
			})
		});
		me.store.getProxy().extraParams = {'employee_id': AM.app.globals.viewIndEmp}
		
		this.items =[{
			xtype: 'grid',
			id: 'langGrid',
			autoScroll: true,
			store : me.store,
			columns : [{
				header: 'Language', 
				dataIndex: 'language',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1
			}, {
				header: 'Proficiency', 
				dataIndex: 'proficiency', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
			},{
				header: 'Read', 
				dataIndex: 'read', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1.5,
				renderer: function(value) {
					if(value==0)
						return 'No';
					if(value==1)
						return 'Yes';	
				},
			},{
				header: 'Write', 
				dataIndex: 'write', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1,
				renderer: function(value) {
					if(value==0)
						return 'No';
					if(value==1)
						return 'Yes';	
				},
			},{
				header: 'Speak', 
				dataIndex: 'speak', 
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 1,
				renderer: function(value) {
					if(value==0)
						return 'No';
					if(value==1)
						return 'Yes';	
				},
			},{
				header: 'Action', 
				xtype : 'actioncolumn',
				sortable: false,
				menuDisabled:true,
				groupable: false,
				draggable: false,
				items: [{
					icon : AM.app.globals.uiPath+'/resources/images/edit.png',
					tooltip : 'Edit',
					handler : this.onEdit
				}, {
					xtype: 'tbspacer'
				}, {
					icon : AM.app.globals.uiPath+'/resources/images/delete.png',
					tooltip : 'Delete',
					handler : this.onDelete
				}]
			}],
		}]
		this.buttons = ['', {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Add Language',
			id : 'AddLanguage',
			style : { 'margin' : '5px'},
			handler: function(){
				AM.app.getController('Employees').onCreateLanguage();
			}
		}];
		
		this.callParent(arguments);
	},
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onDeleteLanguage(grid, rowIndex, colIndex);
	},	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Employees').onEditLanguage(grid, rowIndex, colIndex);
	}
});