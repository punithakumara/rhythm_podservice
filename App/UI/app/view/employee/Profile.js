Ext.define('AM.view.employee.Profile' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.Profile',
	layout : {
		type: 'vbox',
		pack: 'center',
        align: 'stretch'		
	},
	scroll:true,
	width: '99%',
	
    initComponent: function() {
		var me = this;
		
		this.items = [{
			xtype:'hiddenfield',
			id:'viewIndEmp',
			value:''
		},{
			xtype:'panel',
			id : 'selfPanel',
			title: 'My Profile',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'	
			},
			margin:5,
		},{
			xtype:'panel',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'
			},
			// margin:5,
			items: [{
				xtype:'panel',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Skills</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.employee.Skillsets')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId2',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Work Experience</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.employee.WorkExperience')
				}],
				flex:1
			}]
		},{
			xtype:'panel',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'	
			},
			// margin:5,
			style:{
				'margin-top': '20px'
			},
			items: [{
				xtype:'panel',
				// id : 'panelId4',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Certifications</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.employee.Certifications')
				}],
				flex:1
			},{
				xtype:'panel',
				// id : 'panelId5',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Language Proficiency</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.employee.Language')
				}],
				flex:1
			}]
		},{
			xtype:'panel',
			layout: {
				type: 'hbox',
				pack: 'center',
				align: "center",
				width:'100%'	
			},
			// margin:5,
			style:{
				'margin-top': '20px'
			},
			items: [{
				xtype:'panel',
				// id : 'panelId5',
				cls : 'subPanels',
				title : '<div style="text-align:center;">Other Achievements</div>',
				style:{
					border:'1px solid gray'
				},	
				margins : 5,
				height:'auto',
				items : [{
					xtype : Ext.create('AM.view.employee.OtherAchievements')
				}],
				flex:1
			}]
		}]

		this.callParent(arguments);
    }
	
}); 