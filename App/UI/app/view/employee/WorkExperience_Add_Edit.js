Ext.define('AM.view.employee.WorkExperience_Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.workExperienceAddEdit',
	layout : 'fit',
	width : '40%',
	autoShow : true,
	autoSave: false,
	autoHeight : true,
	modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'addexperienceFrm',
			fieldDefaults: {
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'id',
				id : 'workExp_id',
			},{
				xtype : 'textfield',
				name : 'org_name',
				itemId : 'org_name',
				labelAlign: 'top',
				allowBlank: false,
				fieldLabel : 'Company Name <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 50, 
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
				xtype : 'textfield',
				name : 'designation',
				itemId : 'designation',
				labelAlign: 'top',
				allowBlank: false,
				fieldLabel : 'Designation <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 50, 
			}, {
				xtype : 'checkboxfield',
				name : 'cuurentWork',
				id : 'cuurentWork',
				boxLabel : 'I am currently working on this role',
				labelWidth: 200,
				checked:true,
				handler: function(obj) {
					if(obj.checked)
					{
						Ext.getCmp('endDateExp').setDisabled(true);
						Ext.getCmp('end_month').setValue('');

				        Ext.getStore('Month').load({
				            params: {
				                'end_month': '',
				            },
				            callback: function (records, options, success) {
				                if (success) {
				                }
				            }
				        });
				        Ext.getCmp('end_year').setValue('');

				        Ext.getStore('Year').load({
				            params: {
				                'end_year': '',
				            },
				            callback: function (records, options, success) {
				                if (success) {
				                }
				            }
				        });
					}
					else
					{
						Ext.getCmp('endDateExp').setDisabled(false);
					}
				}
			}, {
				layout: 'hbox',
				items:[{
					items: [{
						fieldLabel : 'Start Date <span style="color:red">*</span>',
						xtype : 'combobox',
						emptyText : 'Month',
						multiSelect: false,
						name: 'start_month',
						id: 'start_month',
						labelAlign: 'top',
						allowBlank: false,
						width     : 250,
						store : Ext.getStore('Month'),
						queryMode	: 'remote',
						displayField: 'month_name', 
						valueField: 'month_id',
						minChars:0,
						value:'',
						listeners : {
							beforequery : function(queryEvent) {
								Ext.Ajax.abortAll();
								queryEvent.combo.getStore().getProxy().extraParams = {};
							}
						}
					}, {
						xtype : 'combobox',
						emptyText : 'Year',
						name:'start_year',
						id:'start_year',
						allowBlank: false,
						multiSelect: false,
						width     : 250,
						store : Ext.getStore('Year'),
						queryMode	: 'remote',
						displayField: 'year_name', 
						valueField: 'year_name',
						minChars:0,
						value:'',
						listeners : {
							beforequery : function(queryEvent) {
								Ext.Ajax.abortAll();
								queryEvent.combo.getStore().getProxy().extraParams = {};
							}
						}
					}]
				},{
					style : {
						"margin-left": '10px'
					},
					id:"endDateExp",
					disabled:true,
					items: [{
						fieldLabel : 'End Date <span style="color:red">*</span>',
						xtype : 'combobox',
						multiSelect: false,
						emptyText : 'Month',
						name: 'end_month',
						id: 'end_month',
						labelAlign: 'top',
						allowBlank: false,
						width     : 250,
						store : Ext.getStore('Month'),
						queryMode	: 'remote',
						displayField: 'month_name', 
						valueField: 'month_id',
						minChars:0,
						value:'',
						listeners : {
							beforequery : function(queryEvent) {
								Ext.Ajax.abortAll();
								queryEvent.combo.getStore().getProxy().extraParams = {};
							}
						}
					}, {
						xtype : 'combobox',
						emptyText : 'Year',
						name: 'end_year',
						id: 'end_year',
						multiSelect: false,
						allowBlank: false,
						width     : 250,
						store : Ext.getStore('Year'),
						queryMode	: 'remote',
						displayField: 'year_name', 
						valueField: 'year_name',
						minChars:0,
						value:'',
						listeners : {
							beforequery : function(queryEvent) {
								Ext.Ajax.abortAll();
								queryEvent.combo.getStore().getProxy().extraParams = {};
							}
						}
					}]
				}]
			}, {
				xtype     : 'textareafield',
				grow: false,
				name : 'job_profile',
				labelAlign: 'top',
				fieldLabel : 'Description ',
				anchor: '100%',
				maxLength: 1000,
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();
		
		if (form.isValid())
		{
			var start_month = Ext.getCmp('start_month').getValue();
			var start_year = Ext.getCmp('start_year').getRawValue();
			var end_month = Ext.getCmp('end_month').getValue();
			var end_year = Ext.getCmp('end_year').getRawValue();

			var date = new Date();
			var month_end_date =	new Date(date.getFullYear(), date.getMonth() + 1, 0);

			var start_date = start_year + '-' + start_month + '-01';
			var end_date = end_year + '-' + end_month + '-28';
			
			if(Date.parse(month_end_date) < Date.parse(start_date))
			{
				Ext.MessageBox.show({
					title: 'Warning',
					msg: "Future start date not accepted",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING,
				});	
			}
			else if(Date.parse(month_end_date) < Date.parse(end_date))
			{
				Ext.MessageBox.show({
					title: 'Warning',
					msg: "Future end date not accepted",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING,
				});	
			}
			else if(Date.parse(end_date) < Date.parse(start_date))
			{
				Ext.MessageBox.show({
					title: 'Warning',
					msg: "End date should be greater than Start date",
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING,
				});	
			}
			else
			{
				AM.app.getController('Employees').onSaveExperience(form, win);
			}
		}
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});