Ext.define('AM.view.employee.Certificate_Add_Edit',{
	extend : 'Ext.window.Window',
	alias : 'widget.certificateAddEdit',
	layout : 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'addCertificateFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 120,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'hiddenfield',
				name : 'id',
				id : 'id',
			},{
				xtype : 'textfield',
				name : 'certificate_name',
				itemId : 'c_id',
				allowBlank: false,
				fieldLabel : 'Certificate Name <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 50, 
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
				xtype     : 'textareafield',
				grow: false,
				name : 'content',
				fieldLabel : 'Certificate Body <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 1000,
				allowBlank: false,
				listeners:{
					scope: this,
					'blur': function(text){
						text.setValue(text.getValue().trim());
					}
				}
			}, {
			
					xtype: 'combobox',
					name : 'year',
					allowBlank: false,
					itemId : 'last_used_year',
					emptyText : 'Select Year',
					fieldLabel : 'Year <span style="color:red">*</span>',
					margins	  : '0 0 0 6',
					//width:'49.5%',
					queryMode	: 'local',
					forceSelection: true,
					store: Ext.create('Ext.data.Store', {
						fields: ['Value'],
						data: [
						{'Value': '2005'},
						{'Value': '2006'},
						{'Value': '2007'},
						{'Value': '2008'},
						{'Value': '2009'},
						{'Value': '2010'},
						{'Value': '2011'},
						{'Value': '2012'},
						{'Value': '2013'},
						{'Value': '2014'},
						{'Value': '2015'},
						{'Value': '2016'},
						{'Value': '2017'},
						{'Value': '2018'},
						{'Value': '2019'},
						{'Value': '2020'},
						{'Value': '2021'},

						]
					}),
					displayField: 'Value', 
					valueField: 'Value'
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	defaultFocus:'nameItemId',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form').getForm();

		if (form.isValid()) {
        	win.fireEvent('saveCertificate', form, win);
        }
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});