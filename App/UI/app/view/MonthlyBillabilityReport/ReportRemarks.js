Ext.define('AM.view.MonthlyBillabilityReport.ReportRemarks',{
	extend : 'Ext.window.Window',
	alias : 'widget.ReportRemarks',
	layout: 'fit',
	//width : '99%',
	layout: {
        type: 'vbox',
    },
	store: 'Remarks',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '5px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
    	this.items = [{
			xtype:'gridpanel',
			border:true,
			margin:'0',
			minHeight: 100,
			width:350,
			id: 'remarks_grid',
			columns : [{
				header: 'Change Effective Date',  
				dataIndex: 'change_date',  
				tooltip: 'Change Effective Date',
				flex: 3,
				sortable: false,
				menuDisabled:true,
				//renderer : Ext.util.Format.dateRenderer('d-M-Y'),
				draggable: false
			}, {
				header: 'Shift',  
				dataIndex: 'shift',  
				tooltip: 'Shift',
				flex: 2,
				sortable: false,
				menuDisabled:true,
				draggable: false
			}, {
				header: 'Count',  
				tooltip: 'Count',  
				dataIndex: 'count',  
				flex: 1.5,
				sortable: false,
				menuDisabled:true,
				draggable: false,
				renderer: function(value, meta, record, row, col) {
									
					var val = value;
					
					if(record.data.change_type == 2 && val > 0)
					{
						val = '-' + value;
					}else{
						val = '+' + value;
					}
										
					return val;
				},
			}],
		}];
    	
		this.callParent(arguments);
	},		
	
});