Ext.define("AM.view.MonthlyBillabilityReport.ProjectReport", {
    extend: "Ext.grid.Panel",
    alias: "widget.ProjectReport",
    title: "Monthly Billability Report - Project",
    loadMask: true,
    id: "ProjectReport",
    frame: true,
    width: "99%",
    height: 530,
    viewConfig: {
		forceFit: true,
        getRowClass: function(record) {
            return record.get('shiftHighlight') ? 'highlightMBR' : '';
        },
    },
	features: [{
		id: 'group',
		ftype: 'groupingsummary',
		groupHeaderTpl: '{name}',
		hideGroupedHeader: false,
		enableGroupingMenu: false
	}],
    resizable: false,
    style: {
        border: "1px solid #157fcc",
    },
    initComponent: function() {
    	var grid = this;
        var me = this;
		
		var firstdayOfmonth = new Date();
        firstdayOfmonth.setDate(1);
        firstdayOfmonth.setMonth(firstdayOfmonth.getMonth());

		this.store  = Ext.getStore("MonthlyBillabilityProjectReport").load();

		this.columns=[{
			text: "Client",
			tooltip: 'Client',
			flex:1,
			width: 220,
			hidden: false,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex : 'client_name',
			style: {
				'font-size': '11px'
			},
			summaryRenderer: function(value, summaryData, dataIndex) {
				return "<b>Total<b>"
			}
		},{
			header:'Work Order Name',
			dataIndex : 'wor_name',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:2
		},{
			header:'Type',
			dataIndex : 'wor_type',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:1.5
		},{
			header:'Project',
			dataIndex : 'project_type',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:1
		},{
			header:'Project Name',
			dataIndex : 'project_name',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:1
		},{
			header:'Role',
			flex:1,
			dataIndex : 'role_type',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
		},{
			header: 'Start Date',
			dataIndex : 'start_duration',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:1
		},{
			header: 'End Date',
			dataIndex : 'end_duration',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:1
		},{
			header: 'Hours Entered',
			dataIndex : 'hours_entered',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			flex:1,
			summaryType: 'sum',
			summaryRenderer: function(v, params, data){
				var sec_num = parseInt(v, 10); 
				var hours   = Math.floor(sec_num / 3600);
				var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
				var seconds = sec_num - (hours * 3600) - (minutes * 60);

				if (hours   < 10) {hours   = "0"+hours;}
				if (minutes < 10) {minutes = "0"+minutes;}
				if (seconds < 10) {seconds = "0"+seconds;}
				var t=hours+':'+minutes;
				var arr = t.split(':');
				var dec = parseInt((arr[1]/6)*10, 10);

				return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
			}, 
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return '0';
				}
				else
				{
					var sec_num = parseInt(value, 10);
					var hours   = Math.floor(sec_num / 3600);
					var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
					var seconds = sec_num - (hours * 3600) - (minutes * 60);

					if (hours   < 10) {hours   = "0"+hours;}
					if (minutes < 10) {minutes = "0"+minutes;}
					if (seconds < 10) {seconds = "0"+seconds;}
					var t=hours+':'+minutes;
					var arr = t.split(':');
					var dec = parseInt((arr[1]/6)*10, 10);

					return parseFloat(parseInt(arr[0], 10) + '.' + (dec<10?'0':'') + dec);
				}
			},
		}];

        this.bbar = Ext.create("Ext.toolbar.Paging", {
            store: this.store,
            displayInfo: true,
            cls: "peopleTool",
            prependButtons: false,
            displayMsg: "Total {2} Records",
            emptyMsg: "No Items to display"
        });
		
        this.tools = [{
			xtype: 'monthfield',
			submitFormat: 'Y-m-d',
			name: 'mbrmonth',
			fieldLabel: 'Month',
			id: 'mbrmonth',
			value:firstdayOfmonth,
			format: 'F, Y',
			labelCls : 'searchLabel',
			width: 250,
			listeners: {
				'focus': function(me) {
					this.onTriggerClick();
				},
				select:function (v){
					AM.app.getController('MonthlyBillabilityReport').mbrProjectFun(me,Ext.getCmp('mbrmonth').getValue());
				}
			}
		}, {
			xtype: "tbseparator",
			style: {
				"margin-right": "10px"
			}
		}, {
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 40,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search Client',
			width : 200,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
   	 	}, {
            xtype: "tbseparator",
            style: {
                "margin-right": "10px",
            }
        }, {
        	xtype: "button",
        	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
    		text : 'Export To Excel',
    		style: {
                "margin-right": "8px"
            },
			handler: function(obj){
			if(me.store.getCount() == 0)
				Ext.Msg.alert('Alert', 'No Data!!!');
			else
        		AM.app.getController('MonthlyBillabilityReport').downloadCSV("project");
    		}
    	}];
		
        this.callParent(arguments);
    },
});
