Ext.define("AM.view.MonthlyBillabilityReport.FteReport", {
	extend: "Ext.grid.Panel",
	alias: "widget.FteReport",
	title: "Monthly Billability Report - FTE",
	loadMask: true,
	id: "FteReport",
	frame: true,
	width: "99%",
	height: 530,
	viewConfig: {
		forceFit: true,
		getRowClass: function(record) {
			return record.get('shiftHighlight') ? 'highlightMBR' : '';
		},
	},
	features: [{
		id: 'group',
		ftype: 'groupingsummary',
		groupHeaderTpl: '{name}',
		hideGroupedHeader: false,
		enableGroupingMenu: false
	}, {
		ftype: 'summary',
		dock: 'bottom',
	}],
	resizable: false,
	style: {
		border: "1px solid #157fcc",
	},
	initComponent: function() {
		var grid = this;
		var me = this;
		
		var firstdayOfmonth = new Date();
		firstdayOfmonth.setDate(1);
		firstdayOfmonth.setMonth(firstdayOfmonth.getMonth());

		this.store  = Ext.getStore("MonthlyBillabilityFteReport").load();
		
		this.columns = [{
			text: "Client",
			tooltip: 'Client',
			width: 220,
			hidden: false,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex : 'client_name',
			style: {
				'font-size': '11px'
			},
			summaryRenderer: function(value, summaryData, dataIndex) {
				return "<b>Total<b>"
			}
		},{
			header:'Work Order Name',
			dataIndex : "wor_name",
			flex:1.3,
			sortable: false,
			menuDisabled:true,
			draggable: false,
		},{
			header:'Type',
			dataIndex : "wor_type",
			flex:1.3,
			sortable: false,
			menuDisabled:true,
			draggable: false,
		},{
			header:'Project',
			dataIndex : "project_type",
			flex:1.3,
			sortable: false,
			menuDisabled:true,
			draggable: false,
		},{
			header:'Role',
			dataIndex : "role_type",
			flex:1.3,
			sortable: false,
			menuDisabled:true,
			draggable: false,
		},{
			header: "Approved FTE",
			tooltip: "Approved FTE",
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "fte_total_hc",
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		}, {
			header: "Approved Buffer",
			tooltip: "Approved Buffer",
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "fte_total_buffer",
			value: 0,         
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'APAC',
			tooltip : 'APAC',
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'fte_apac',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'LEMEA',
			tooltip : 'LEMEA',
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'fte_lemea',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		}, {
			header : 'India <br>(IST, EMEA)',
			tooltip : 'India <br>(IST, EMEA)',
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'india',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'USA<br>(PST, CST, EST)',
			tooltip : 'USA<br>(PST, CST, EST)',
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'usa',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header: "Full Month FTE",
			tooltip: "Full Month FTE",
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "full_month_fte",
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		}, {
			header: "Prorated FTE",
			tooltip: "Prorated FTE",
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "prorated_fte",
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'Remarks',
			tooltip : 'Remarks',
			flex:1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			xtype : 'actioncolumn',
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'View',
				handler: function(grid, rowIndex, colIndex){
					AM.app.getController('MonthlyBillabilityReport').viewRemarks(grid, rowIndex, colIndex, Ext.getCmp('mbrmonth').getValue());
				} 
				// handler : this.addREm
				
			}]
		}];
		
		this.bbar = Ext.create("Ext.toolbar.Paging", {
			store: this.store,
			displayInfo: true,
			cls: "peopleTool",
			prependButtons: false,
			displayMsg: "Total {2} Records",
			emptyMsg: "No Items to display"
		});
		
		this.tools = [{
			xtype: 'monthfield',
			submitFormat: 'Y-m-d',
			name: 'mbrmonth',
			fieldLabel: 'Month',
			id: 'mbrmonth',
			value:firstdayOfmonth,
			format: 'F, Y',
			labelCls : 'searchLabel',
			width: 250,
			listeners: {
				'focus': function(me) {
					this.onTriggerClick();
				},
				select:function (v){
					AM.app.getController('MonthlyBillabilityReport').mbrFteFun(me,Ext.getCmp('mbrmonth').getValue(),v.getValue(),false);
				}
			}
		}, {
			xtype: "tbseparator",
			style: {
				"margin-right": "10px"
			}
		}, {
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 40,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search Client',
			width : 200,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		}, {
			xtype: "tbseparator",
			style: {
				"margin-right": "10px",
			}
		}, {
			xtype: "button",
			icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			text : 'Export To Excel',
			style: {
				"margin-right": "8px"
			},
			handler: function(obj){
				if(me.store.getCount() == 0)
					Ext.Msg.alert('Alert', 'No Data!!!');
				else
					AM.app.getController('MonthlyBillabilityReport').downloadCSV("fte");
			}
		}];
		
		this.callParent(arguments);
	},
});
