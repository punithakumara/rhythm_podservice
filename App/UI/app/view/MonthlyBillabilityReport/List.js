Ext.define('AM.view.MonthlyBillabilityReport.List',{
	extend : 'Ext.container.Container',
	alias : 'widget.MonthlyBillabilityReportList',
	height : 575,
	layout: {
		type: "vbox",
		align: "stretch",
		pack: "start"
	},
	width : '99%',
	border : true,
	bodyStyle: 'padding:0 0 50px 0',
	initComponent : function() {
		var me = this;
		me.items = [{
			xtype: Ext.create('AM.view.MonthlyBillabilityReport.BillabilityGroupGrid'),
			flex : 1
		}]

		me.callParent(arguments);
	}
});