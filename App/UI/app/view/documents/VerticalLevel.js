Ext.define('AM.view.documents.VerticalLevel',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.VerticalLevel',
	width: '99%',
	layout: 'fit',
	frame: true,
	
	initComponent : function() {
		var me =this;
		
		this.items = [{
			xtype: 'tabpanel',
			id: 'verticalTabs',
			activeTab: 0,
			items:[{
				xtype:AM.app.getView('documents.VerticalDocuments').create()
			},{
				xtype:AM.app.getView('documents.VerticalTemplate').create()
			}],
        }];	
	
		this.callParent(arguments);
	},
    
});