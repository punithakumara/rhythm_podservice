Ext.define('AM.view.documents.VerticalTemplate',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.VerticalTemplate',
	require : ['Ext.ux.grid.FiltersFeature'],
	title: 'Vertical Templates',
    store : 'DocumentsVerticalTemplates',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
	width : '80%',
    minHeight : 100,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	height: 'auto',	
    initComponent : function() {
		
       	Ext.getStore('DocumentsVerticalTemplates').load({
			params : { template_type : '3'}
 		});
		
    	this.columns = [{
			header : 'Vertical', 
			dataIndex : 'VerticalName', 
			flex: 1.5,
			draggable: false,
			groupable: false,
			menuDisabled:true,
		},{
			header : 'Template', 
			flex: 3,
			dataIndex : 'uploaded_default_name',
			draggable: false,
			groupable: false,
			menuDisabled:true,
		},{
			header : 'Uploaded By', 
			dataIndex : 'FirstName', 
			flex: 1.5,
			draggable: false,
			groupable: false,
			menuDisabled:true,
		},
		 {
			header : 'Uploaded On', 
			dataIndex : 'TemplateDate', 
			draggable: false,
			groupable: false,
			menuDisabled:true,
			flex: 1
		}, {			
			header : 'Action', 
			width : '10%',
			xtype : 'actioncolumn',
			sortable: false,
			draggable: false,
			groupable: false,
			menuDisabled:true,
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/download.png',
				tooltip : 'Download',
				handler : this.onDownload
			},'-',{
				icon : AM.app.globals.uiPath+'resources/images/delete.png',
				tooltip : 'Delete',												
				handler : this.onDelete,
				getClass: function(v, meta, record) {
					if(Ext.util.Cookies.get('grade')>=5)
					{
						return 'rowVisible';
					}
					else
					{
						return 'x-hide-display';
					}
				}
			}]
		}];
		
		this.tbar = [{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/upload.png',
			text : 'Upload Template',
			id : 'vertUpTemp',
			listeners :{
				'click':function(){
					AM.app.getController('DocumentsTemplates').onCreateTemplate();
					Ext.getCmp('verticalDocsFormat').setValue('<span style="color:blue">Acceptable Formats : .pdf, .gif, .jpg, .png, .jpeg, .doc, .docx, .xlsx, .xls, .csv, .ppt, .pptx, .txt</span>');
				}
			},
		}],
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No documents to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
			listeners: {
				beforechange: function(){
					this.store.getProxy().extraParams = {template_type : '3'};
				}
            }
        }];
		
		this.callParent(arguments);
	},
	
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('DocumentsTemplates').onDeleteTemplate(grid, rowIndex, colIndex,'3');
	},
	
	onDownload : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		AM.app.getController('DocumentsTemplates').onDownloadTemplate(grid, rowIndex, colIndex,'3');
	},	
	
	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('DocumentsTemplates').onViewTemplate(grid, rowIndex, colIndex,'3');
	}
});