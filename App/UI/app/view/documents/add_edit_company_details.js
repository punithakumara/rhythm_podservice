Ext.define('AM.view.documents.add_edit_company_details',{
	extend : 'Ext.window.Window',
	alias : 'widget.docAddEdit',
	title: 'Upload Company Details',
    constrain: true,	
	layout: 'fit',
	width : '45%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal: true,
    y : 100,
	
    initComponent : function() {
    	
		var me=this;
		
		this.items = [{
			xtype : 'form',
			itemId : 'addClientFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			fileUpload:true,
			isUpload: true,
			enctype:'multipart/form-data',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			
			items : [{
				xtype : 'hiddenfield',
				name : 'Access',
				value:'All'
			},{
				xtype : 'hiddenfield',
				name : 'template_type',
				value:'1'
			},{
				xtype : 'filefield',
				emptyText : 'Select File to Upload',
				name : 'NewDocument',
				id : 'NewCompDocument',
				fieldLabel : 'Upload File <span style="color:red">*</span><br>(Max file size 20 MB)',
				anchor: '100%',
				allowBlank: false,
				buttonText: 'Browse File',
				listeners: {
                        change: function(fld, value) {
                            var newValue = value.replace(/C:\\fakepath\\/g, '');
                            fld.setRawValue(newValue);
                        }
                    }
			},{
				xtype : 'displayfield',
				id: "companyDocsFormat",
				value : '',
			}]
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];

		this.callParent(arguments);

	},
	
	onSave :  function() {			
		var win = this.up('window');
		var fname = win.down('form').query('filefield')[0].getValue();					
		
		// validation
		
		var activeTab = Ext.getCmp('companyTabs').getActiveTab().tab.text;
		if(activeTab=="Company Documents")
		{
			var exttype = fname.substr((Math.max(0, fname.lastIndexOf(".")) || Infinity) + 1);
		var extTypeList = new Array("pdf", "PDF", "gif", "GIF", "jpg", "JPG", "png", "PNG","jpeg", "JPEG");
		}
		else
		{
			var exttype = fname.substr((Math.max(0, fname.lastIndexOf(".")) || Infinity) + 1);
		var extTypeList = new Array("pdf", "PDF", "gif", "GIF", "jpg", "JPG", "png", "PNG", "jpeg", "JPEG","xls", "XLS","xlsx","XLSX","doc", "DOC","docx","DOCX","ppt","PPT","pptx","PPTX",'csv','CSV');
		}
		if(Ext.getCmp('NewCompDocument').getValue() == ''){
			Ext.MessageBox.alert('Alert', 'Select file to upload');
			return false;
		}
		var form = win.down('form').getForm();
		if (form.isValid()) 
		{
			if(extTypeList.indexOf(exttype) != -1 || win.down('form').query('filefield')[0].disabled == true)
			{
				var tempPath = Ext.getCmp('NewCompDocument').getValue(); //C:\fakepath\sample.pdf
				var filename = tempPath.replace("C:\\fakepath\\","");
				var fileExists = "";

				Ext.Ajax.request({
					url : AM.app.globals.appPath+'index.php/DocumentsTemplates/checkfileExists/',
					method: 'POST',
					params: {
						verticalID : "",
						filename : filename,
						activeTab : activeTab,
					},
					success: function (batch, operations) {
				    	var jsonResp = Ext.decode(batch.responseText); 

				    	if(jsonResp.count>0)
				    	{
				    		Ext.MessageBox.confirm('Delete', 'File Already exists! Do you want to replace?', function(btn){
							   	if(btn === 'yes')
							   	{
							       	if(activeTab=="Company Documents")
									{
										AM.app.getController('DocumentsTemplates').onSaveDocument(form, win, 'All');
									}
									else
									{
										AM.app.getController('DocumentsTemplates').onSaveTemplate(form, win, '1');
									}
							   	}
							});
				    	}
				    	else
				    	{
				    		if(activeTab=="Company Documents")
							{
								AM.app.getController('DocumentsTemplates').onSaveDocument(form, win, 'All');
							}
							else
							{
								AM.app.getController('DocumentsTemplates').onSaveTemplate(form, win, '1');
							}
				    	}
				    }
				});

			}
			else if(win.down('form').query('filefield')[0].disabled != true)
			{
				Ext.MessageBox.alert('Alert', 'Invalid file format');
			}		
		}

	},
	
	onCancel : function() 
	{
		this.up('window').destroy();
	}
});