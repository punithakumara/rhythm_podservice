Ext.define('AM.view.documents.CompanyLevel',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.CompanyLevel',
	width: '99%',
	layout: 'fit',
	frame: true,
	
	initComponent : function() {
		var me = this;
		
		this.items = [{
			xtype: 'tabpanel',
			id: 'companyTabs',
			activeTab: 0,
			items:[{
				xtype:AM.app.getView('documents.CompanyDocuments').create()
			}, {
				xtype:AM.app.getView('documents.CompanyTemplate').create()
			}]
        }];	
	
		this.callParent(arguments);
	},
    
});