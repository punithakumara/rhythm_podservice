Ext.define('AM.view.documents.add_edit_vertical_details',{
	extend : 'Ext.window.Window',
	alias : 'widget.tempAddEdit',
	title: 'Upload Vertical Details',
    constrain: true,	
	layout: 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal: true,
    y : 100,
	
    initComponent : function() {
    	var me=this;
		this.items = [{
			xtype : 'form',
			itemId : 'addTpl',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			fileUpload:true,
			isUpload: true,
			enctype:'multipart/form-data',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items: [{
				xtype : 'hiddenfield',
				name : 'Access',
				value:'Vertical Specific'
			},{
				xtype : 'hiddenfield',
				name : 'template_type',
				value:'3'
			},{
				xtype : 'hiddenfield',
				name : 'template_id'
			},{
				xtype : 'boxselect',
				multiSelect: false,
				allowBlank: false,
				fieldLabel: 'Vertical <span style="color:red">*</span>',
				name: 'VerticalID',
				id: 'docVerticalID',
				store: 'Vertical',
				minChars:0,
				displayField: 'name', 
				valueField: 'vertical_id',
				anchor: '100%',
				emptyText: 'Select Vertical',
				value:'',
				listeners: {
					beforequery: function(queryEvent, eOpts) {
						queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','comboVertical':'1','filterName' : queryEvent.combo.displayField};
					},
				}
			},{
				xtype : 'filefield',
				emptyText : 'Select File to Upload',
				name : 'NewDocument',
				fieldLabel : 'Upload File <span style="color:red">*</span><br>(Max file size 20 MB)',
				anchor: '100%',
				id:'NewDocument',
				//allowBlank: false,
				buttonText: 'Browse File',
				listeners: {
                        change: function(fld, value) {
                            var newValue = value.replace(/C:\\fakepath\\/g, '');
                            fld.setRawValue(newValue);
                        }
                    }
			},{
				xtype: 'container',
				style : {
					'margin-left' :  '105px'
				},
				autoHeight: true,
				maxWidth: 500,
				maxHeight: 200,
				autoScroll: true,
				items: [{
					xtype: 'image',
					name : 'imgLogo',
					id : 'imgLogo'
				}]
			},{
				xtype : 'displayfield',
				id: "verticalDocsFormat",
				value : '',
			}]
		}];
		
    	this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];

		this.callParent(arguments);
	},
	
	onSave :  function() {			
		var win = this.up('window');
		var fname = win.down('form').query('filefield')[0].getValue();					
		var url = AM.app.globals.uiPath+'resources/uploads/verticalDocs/';
		// validation
		var activeTab = Ext.getCmp('verticalTabs').getActiveTab().tab.text;
		if(activeTab=="Vertical Documents")
		{
			var exttype = fname.substr((Math.max(0, fname.lastIndexOf(".")) || Infinity) + 1);
		var extTypeList = new Array("pdf", "PDF","gif", "GIF", "jpg", "JPG", "png", "PNG","jpeg", "JPEG");
		}
		else
		{
		var exttype = fname.substr((Math.max(0, fname.lastIndexOf(".")) || Infinity) + 1);
		var extTypeList = new Array("pdf", "PDF", "xls", "XLS","xlsx","XLSX","doc", "DOC","docx","DOCX","ppt","PPT","pptx","PPTX",'csv','CSV',"gif", "GIF", "jpg", "JPG", "png", "PNG", "jpeg", "JPEG","txt","TXT");
		}

		var form = win.down('form').getForm();
		if (form.isValid()) 
		{
			if(Ext.getCmp('NewDocument').getValue() == ''){
				Ext.MessageBox.alert('Alert', 'Select file to upload');
				return false;
			}
			if(extTypeList.indexOf(exttype) != -1 || win.down('form').query('filefield')[0].disabled == true)
			{
				var docVerticalID = Ext.getCmp('docVerticalID').getValue();

				var tempPath = Ext.getCmp('NewDocument').getValue(); //C:\fakepath\sample.pdf
				var filename = tempPath.replace("C:\\fakepath\\","");
				var fileExists = "";

				Ext.Ajax.request({
					url : AM.app.globals.appPath+'index.php/DocumentsTemplates/checkfileExists/',
					method: 'POST',
					params: {
						verticalID : docVerticalID,
						filename : filename,
						activeTab : activeTab,
					},
					success: function (batch, operations) {
				    	var jsonResp = Ext.decode(batch.responseText); 

				    	if(jsonResp.count>0)
				    	{
				    		Ext.MessageBox.confirm('Delete', 'File Already exists! Do you want to replace?', function(btn){
							   	if(btn === 'yes')
							   	{
							       	if(activeTab=="Vertical Documents")
									{
										AM.app.getController('DocumentsTemplates').onSaveDocument(form, win, 'Vertical Specific');
									}
									else
									{
										AM.app.getController('DocumentsTemplates').onSaveTemplate(form, win, '3');
									}
							   	}
							});
				    	}
				    	else
				    	{
				    		if(activeTab=="Vertical Documents")
							{
								AM.app.getController('DocumentsTemplates').onSaveDocument(form, win, 'Vertical Specific');
							}
							else
							{
								AM.app.getController('DocumentsTemplates').onSaveTemplate(form, win, '3');
							}
				    	}
				    }
				});

			}
			else if(win.down('form').query('filefield')[0].disabled != true)
			{
				Ext.MessageBox.alert('Alert', 'Invalid file format');
			}		
		}
	},
	
	onCancel : function() {
		this.up('window').destroy();
	},
	
	
});