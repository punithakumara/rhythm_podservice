Ext.define('AM.view.documents.CompanyDocuments',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.comdocList',
	require : ['Ext.ux.grid.FiltersFeature'],
	title: 'Company Documents',
    store : 'CompanyDocs',
    layout : 'fit',
    viewConfig: {
		forceFit: true
	},
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
	height: 'auto',	
    initComponent : function() {
		
		this.columns = [{
			header : 'Document', 
			dataIndex : 'UploadName',
			draggable: false,
			groupable: false,
			menuDisabled:true,
			flex: 3,
		}, {
			header : 'Uploaded By', 
			dataIndex : 'FirstName', 
			draggable: false,
			groupable: false,
			menuDisabled:true,
			flex: 1.5
		}, {
			header : 'Uploaded On', 
			dataIndex : 'DocumentDate', 
			draggable: false,
			groupable: false,
			menuDisabled:true,
			flex: 1
		}, {
			header : 'Action',
			sortable: false,
			draggable: false,
			groupable: false,
			menuDisabled:true,
			width : '7%',
			xtype : 'actioncolumn',
			id:'compdocview',
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/view.png',
				tooltip : 'View',												
				handler : this.onView,						
			},
			'-',
			{
				icon : AM.app.globals.uiPath+'resources/images/delete.png',
				tooltip : 'Delete',												
				handler : this.onDelete,
				getClass: function(v, meta, record) {
					if(Ext.util.Cookies.get('grade')>=5)
					{
						return 'rowVisible';
					}
					else
					{
						return 'x-hide-display';
					}
				}
			}]
		}];
		
		this.tbar = [{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/upload.png',
			text : 'Upload Document',
			id : 'compUpDoc',
			listeners :{
				'click':function(){
					AM.app.getController('DocumentsTemplates').onCreate();
					Ext.getCmp('companyDocsFormat').setValue('<span style="color:blue">Acceptable Formats : .pdf, .gif, .jpg, .png,.jpeg</span>');
				}
			},
		}],
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No documents to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        }];
		
		this.callParent(arguments);
	},
	
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('DocumentsTemplates').onDeleteDocuments(grid, rowIndex, colIndex);
	},
	
	onEdit : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		AM.app.getController('ProDocuments').onEditDocument('compDocID',grid, rowIndex, colIndex);
	},
	
	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('DocumentsTemplates').onViewDocuments(grid, rowIndex, colIndex);
	}
});