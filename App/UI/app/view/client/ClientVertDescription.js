Ext.define('AM.view.client.ClientVertDescription' ,{
    extend: 'Ext.panel.Panel',
	//id:'myClientPanel',
    alias: 'widget.clientvertdesc',
	autoScroll: true,
    layout: {
        type: 'table',
        // The total column count must be specified here
        columns: 4,
        tableAttrs: {
            style: {
                width: '100%'
            }
        },
        tdAttrs:{
            style: {border:'1px solid #e5e5e5',width:400}
        }
    },
	height: 200,
    defaults: {
        // applied to each contained panel
        bodyStyle: 'padding:5px',
        border:false
    },
	initComponent: function() {
		var me = this;
    	var vertId = this.vertId;
		//console.log(vertId); 
			var StoreData = Ext.getStore('VerticalDescription').load({
				params:{'ClientID':AM.app.globals.dashboardClient,'VerticalID':vertId}
			,callback:function(records, options, success) {
				me.onCallBack(me,records,vertId);
				//console.log('Testing...'+records);
			}});
		
        this.callParent(arguments);
    },
    
    onCallBack:function(param,records,vertId){
    	
		param.add(
			
			{ html: '<b>Total Resources</b>' },{ html: records[0].data["Total_ResourcesCount"]},
			{ html: '<b>Total # Approved Resource</b>'},{html: records[0].data["Resources_HC"]},
			
			{ html: '<b>Buffer Details</b>' },{ html: records[0].data["buffer_items"]},
			{ html: '<b>Billable Details</b>' },{ html: records[0].data["billable_items"]},
			
            { html: '<b>In Mysore</b>' },{ html: records[0].data["Mysore"]},            
            { html: '<b>In Bangalore</b>' },{ html: records[0].data["Bangalore"]},            
            
            { html: '<b>Shifts</b>' },{ html: records[0].data["Shifts"] ,colspan:3},
			
            { html: '<b>Tools & Technologies</b>' },{ html:records[0].data["Technologies"] },
            { html: '<b>Process & Tasks</b>' },{ html: records[0].data["Tasks"]}
			
            );
		
		param.doLayout();
    	
    	
    },	
	
});