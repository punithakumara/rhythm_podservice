Ext.define('AM.view.client.ClientRelVert',{
	extend : 'Ext.panel.Panel',
	alias : 'widget.clientrelvert',
	 width: 500,
    height: 200,
	defaults: {
            collapsible: true,
            split: true,
            animFloat: false,
            autoHide: false,
            useSplitTips: true,
            bodyStyle: 'padding:15px'
        },
	
	// defaults: {
        // bodyPadding: 10
    // },
	region:'west',
	//floatable: false,
	margins: '5 0 5 5',
	cmargins: '5 0 5 5',
	layout:'accordion',
	layoutConfig: {
                //titleCollapse: true,
                animate: true,
                fill:true
            },
	bodyStyle: 'padding:0px',
	autoScroll: true,
	initComponent : function() {
		var me =this;
		
		var StoreData = Ext.getStore('ClientVertDescription').load({
			params:{'ClientID':AM.app.globals.dashboardClient}
		,callback:function(records, options, success) {
			
			 me.height = (records.length*30)+270;
			 
			 for(i=0;i<records.length;i++){
					me.onCallBack(me,records[i]);
			 }
		
		}});
		
	
	this.callParent(arguments);
	
	},
	onCallBack : function(myparam,record){
		
	
		myparam.add({title:record.data["Name"], items:[{xtype:AM.app.getView('client.ClientVertDescription').create({vertId : record.data["VerticalID"]})}]});
		
		myparam.doLayout();
		
	}
	
    
});