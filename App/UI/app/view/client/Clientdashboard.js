Ext.define('AM.view.client.Clientdashboard',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.clientdashboardList',
	requires : ['Ext.ux.grid.plugin.PagingSelectionPersistence'],
	
	title: 'Client Dashboard',
    
    store : 'Clients',
    plugins : [{ ptype : 'pagingselectpersist' }],
    id : 'clientGridId',
    layout : 'fit',
    viewConfig : {
	  forceFit : true,
	  listeners: {
			cellclick : function(view, cell, cellIndex, record,row, rowIndex, e) {
							if(cellIndex == 1){
								if(cell.childNodes[0].firstChild.classList.contains('icon-follow')){
									cell.childNodes[0].firstChild.classList.remove('icon-follow');
									cell.childNodes[0].firstChild.classList.add('icon-followed');
									var followValue = 1;
								}
								else
								{
									cell.childNodes[0].firstChild.classList.remove('icon-followed');
									cell.childNodes[0].firstChild.classList.add('icon-follow');
									var followValue = 0;
								}
									AM.app.getController('Clientdashboard').followAccount(record, followValue);
							}
						}
				}
	},
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
	
    initComponent : function() {
    	  var me = this;
     	  if(me.status=='inactive'){
    		  me.status=0;
    		 
    		var oldstatus=me.status;
    	  }
    	 if(typeof me.status != 'undefined')
    		 var value = (me.status=='') ? '1' : ''+me.status;
    	 else
    		 var value = '1';
    //	console.log(typeof me.status);
    			
  		this.selModel = Ext.create('Ext.selection.CheckboxModel', {
  			//allowDeselect: false,
  			checkOnly: true,
  			mode:'MULTI',
  			listeners: {
                  beforeselect: function(selModel, record, index){
                      // console.log('sm::beforeselect');   
                  },
                  select: function(model, record, index){
                  	//console.log(record.data.ClientID);
                       //console.log('sm::select');                    
                  }
              }
  		});
  		
  		this.tools = [{
  			xtype : 'button',
  			icon: AM.app.globals.uiPath+'resources/images/claimTier.png',  			
  			tooltip : 'Claim Tier',
   				listeners : {
  					click : function() {
  				 	   //var comboIDval =	Ext.getCmp('clientStatusComboBox').getValue();
  				 	   var checkboxval=this.up('grid');
  						//this.up('grid').fireEvent('clientExport',this.up('grid'),comboIDval);
  						AM.app.getController('Clientdashboard').ClientTier(this.up('grid'));
  						 
  					}
  				}
  		},{
  			xtype : 'button',
  			icon: AM.app.globals.uiPath+'resources/images/auditTrail.png',  			
  			tooltip : 'Audit Trail',
   				listeners : {
  					click : function() {
  				 	   var comboIDval =	Ext.getCmp('clientStatusComboBox').getValue();
  				 	   var checkboxval=this.up('grid');
  						//this.up('grid').fireEvent('clientExport',this.up('grid'),comboIDval);
  						AM.app.getController('Clientdashboard').ClientAuditTrail(this.up('grid'),comboIDval);
  						 
  					}
  				}
  		},{
  			xtype : 'button',
  			icon: AM.app.globals.uiPath+'resources/images/claimAccount.png',  			
  			tooltip : 'Claim Account?',
   				listeners : {
  					click : function() {
  				 	   var comboIDval =	Ext.getCmp('clientStatusComboBox').getValue();
  				 	   var checkboxval=this.up('grid');
  						//this.up('grid').fireEvent('clientExport',this.up('grid'),comboIDval);
  						AM.app.getController('Clientdashboard').ClaimAccount(this.up('grid'),comboIDval);
  						 
  					}
  				}
  		},{
  			xtype : 'button',
  			icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
  			action : 'exportToExcel',
  			tooltip : 'Export To Excel',
  			//disabled : true,
  				listeners : {
  					click : function() {
  				 	   var comboIDval =	Ext.getCmp('clientStatusComboBox').getValue();
  				 	   var checkboxval=this.up('grid');
  						//this.up('grid').fireEvent('clientExport',this.up('grid'),comboIDval);
  						AM.app.getController('Clientdashboard').ClientExcelExport(this.up('grid'),comboIDval);
  						 
  					}
  				}
  			}, 
  			{ 
  		        xtype : 'tbseparator',
  		        style : {
  		        	'margin-right' : '10px'
  		        }
  		    },{ 
  	        xtype : 'tbseparator',
  	        style : {
  	        	'margin-right' : '10px'
  	        }
  	    },{
  	        xtype : 'trigger',
  	        itemId : 'gridTrigger',
  	        fieldLabel : '',
  	        labelWidth : 60,
  	        labelCls : 'searchLabel',
  	        triggerCls : 'x-form-clear-trigger',
  	        emptyText : 'Search',
  	        width : 250,
  	        minChars : 1,
  	        enableKeyEvents : true,
  	        onTriggerClick : function(){
  	            this.reset();
  	            this.fireEvent('triggerClear');
  	        }
  	    }, { 
  	        xtype : 'tbseparator',
  	        style : {
  	        	'margin-right' : '10px'
  	        }
  	    }/*, {
  			xtype : 'button',
  			icon: AM.app.globals.uiPath+'resources/images/add.png',
  			action : 'createClient',
  			text : 'Add Client'
  		}*/];
    	this.columns = [ 
    	                {
    	            header : '',
    	            dataIndex : 'followAccount',
    	            xtype  :'actioncolumn',
    	            //hidden : hiddenFollowAccount,
    	            //icon   : AM.app.globals.uiPath+'resources/images/Star_Grey.png',
    	            items:
    	                [{
    	                    getClass: function(v, meta, rec) {
    	                		if(rec.get('status') != 'In Active')
    	                		{
    	                			if (rec.get('followAccount') != 0) {
    	    	                          
        	                            return 'icon-followed';
        	                        }
    	                			else
    	                				return 'icon-follow';
    	                		}
    	                    },
    	                    getTip: function (v, meta, record, rowIndex, colIndex, store) {
    	                    	if(record.get('status') != 'In Active')
    	                    	{
	    	                    	 if (record.get('followAccount') == 0) 
	    	                    		 return 'Follow';
	    	                    	 if (record.get('followAccount') == 1) 
	        	                    		 return 'Unfollow';
    	                    	 }
    	                    },
    	                }],
    	            flex   :0.2,
    	            
    	        },{
					header : 'Name', 
					id : 'Name',
					flex: 1,
					dataIndex : 'Client_Name',
				}, {
					header : 'Client Type', 
					id : 'Web',
					dataIndex : 'industryType', 
					flex: 1
				}, {
					header : 'Country', 
					id : 'Country',
					dataIndex : 'Country', 
					flex: 1
				}, {
					header : 'Approved FTE',
					id : 'resourceCount',
					dataIndex : 'resourceCount', 
					flex: 1
				}, {
					header : 'Forecast NRR',
					id : 'ForcastNRR',
					dataIndex : 'ForcastNRR', 
					flex: 0.8
				}, {
					header : 'Forecast RDR',
					id : 'ForcastRDR',
					dataIndex : 'ForcastRDR', 
					flex: 0.8
				}, {
					header : 'Shifts Covered',
					flex: 1,
					dataIndex : 'shifts',
					hidden : true
				}, {
					header : 'Tools & Technologies', 
					flex: 1,
					dataIndex : 'technologies',
				},{
					header : 'Client Partner', 
					flex: 1,
					dataIndex : 'clientPartner',
				},{
					header : 'Engagement Manager', 
					flex: 1,
					dataIndex : 'FullName',
				},{
					header : 'Created Date', 
					flex: 1,
					dataIndex : 'Created_Date',
				},{
					header : 'View', 
					width : '5%',
					xtype : 'actioncolumn',
					
					items : [{
						icon : AM.app.globals.uiPath+'resources/images/view.png',
						tooltip : 'View',
						handler : function(grid, rowIndex, colIndex) {
              var statuscomboIDval =  Ext.getCmp('clientStatusComboBox').getValue();
              AM.app.getController('Clientdashboard').onViewClient(grid, rowIndex, colIndex,statuscomboIDval);
              //Commented to fix carousel for client issue
              //this.onView(me.cstatus)
            }
					}/*, '-', {
						icon : AM.app.globals.uiPath+'resources/images/delete.png',
						tooltip : 'Delete',
						handler : this.onDelete
					}*/]
				}
		];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No clients to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
	
	
	
	onDelete : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('deleteClient', grid, rowIndex, colIndex);
	},
	ClaimAccount : function(grid, rowIndex, colIndex) {
		//console.log(oldstatus);
		var statuscomboIDval =	Ext.getCmp('clientStatusComboBox').getValue();
		//console.log(statuscomboIDval);
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('viewClient', grid, rowIndex, colIndex,statuscomboIDval);
	},
  	
	onView : function(grid, rowIndex, colIndex, cstatus) {
		//console.log(oldstatus);
		var statuscomboIDval =	Ext.getCmp('clientStatusComboBox').getValue();
		//console.log(statuscomboIDval);
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
    AM.app.getController('Clientdashboard').onViewClient(grid, rowIndex, colIndex,statuscomboIDval);
		//gridPanel.fireEvent('viewClient', grid, rowIndex, colIndex,statuscomboIDval, me.cstatus);
	}
} );