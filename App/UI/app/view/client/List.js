Ext.define('AM.view.client.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.clientList',
	title: 'Client List',
    closeAction: 'destroy',
    store : 'Clients',
    itemId : 'clientGridId',
    layout : 'fit',
    viewConfig : {
		forceFit : true,
	},
	frame: true,
	width: "99%",
	minHeight: 495,
	height: 'auto',
	style: {
		border: "0px solid #157fcc",
		borderRadius:'0px'
	},
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    border : true,
	
    initComponent : function() {

    	this.columns = [{
			header : 'Name', 
			id : 'Name',
			flex: 1,
			dataIndex : 'client_name',
			menuDisabled:true,
			groupable: false,
			draggable: false,
		// },{
		// 	header : 'Verticals', 
		// 	id : 'vertical_name',
		// 	flex: 1,
		// 	dataIndex : 'vertical_name',
		// 	menuDisabled:true,
		// 	groupable: false,
		// 	draggable: false,
		}, {
			header : 'Web', 
			id : 'Web',
			dataIndex : 'web', 
			menuDisabled:true,
			groupable: false,
			draggable: false,
			flex: 1
		// }, {
		// 	header : 'City',
		// 	id : 'City',
		// 	dataIndex : 'city',
		// 	menuDisabled:true,
		// 	groupable: false,
		// 	draggable: false, 
		// 	flex: 1
		}, {
			header : 'State',
			flex: 1,
			dataIndex : 'state',
			menuDisabled:true,
			groupable: false,
			draggable: false,
		}, {
			header : 'Country', 
			flex: 1,
			dataIndex : 'country',
			menuDisabled:true,
			groupable: false,
			draggable: false,
		}, {
			header : 'Action', 
			width : '5%',
			xtype : 'actioncolumn',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,			
			items : [{
				icon : AM.app.globals.uiPath+'resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit
			}]
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No clients to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
	
	tools : [{
		xtype: 'combobox',
		itemId : 'clientStatusCombo',
		store: Ext.create('Ext.data.Store', {
			fields: ['status', 'clientStatus'],
			data : [{'status': '1', 'clientStatus': 'Active'}, {'status': '0', 'clientStatus': 'InActive'}]
		}),
		displayField: 'clientStatus', 
		valueField: 'status',
		value: '1',
		listeners: {
			select: function(combo){
				this.fireEvent('clientChangeStatusCombo', combo);
				Ext.getCmp('clientgridTrigger').reset();
			}
		}
	},{ 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        id : 'clientgridTrigger',
        fieldLabel : '',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }, {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'resources/images/Add.png',
		handler : function() {
			AM.app.globals.mainClient = "";
			AM.app.getController('Clients').onCreate();
		},
		listeners : {
			afterRender: function() {
				if(Ext.util.Cookies.get('grade')>=5 || Ext.util.Cookies.get('is_onshore')==1)
				{
					this.show();
				}
				else
				{
					this.hide();
				}
			}
		},
		text : 'Add Client'
	}],
	
	onDelete : function(grid, rowIndex, colIndex) {
		AM.app.getController('Clients').onDeleteClient(grid, rowIndex, colIndex);
	},
	
	onEdit : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		
		AM.app.getController('Clients').onEditClient(grid, rowIndex, colIndex);
	},
	
	onAddClientUser : function(grid, rowIndex, colIndex) {
		AM.app.getController('Clients').onAddClientUser(grid, rowIndex, colIndex);
	},
	
});