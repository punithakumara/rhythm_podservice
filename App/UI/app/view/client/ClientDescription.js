Ext.define('AM.view.client.ClientDescription' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.clientdescription',
    layout: {
        type: 'table',
        // The total column count must be specified here
        columns: 4,
        tableAttrs: {
            style: {
                width: '100%'
            }
     }

    },
    defaults: {
        // applied to each contained panel
        bodyStyle: 'padding:5px',
        border:false
    },
	initComponent: function() {
		var me = this;
    	var StoreData = Ext.getStore('ClientDescription').load({
			params:{'client_id':AM.app.globals.dashboardClient}
		,callback:function(records, options, success) {
			me.onCallBack(me,records);
			//console.log(records);
		}});
        this.callParent(arguments);
    },
    
    onCallBack:function(param,records){    	    	
    	param.add({
            html: '<b>Client name:</b>'
          
        }, 
		{
            html: '<font size="4px"><b>'+records[0].data["ClientName"]+'<b/></font>'
        }, 
		{
            html: '<b>Client since:</b>'
        },
        {
            html: records[0].data["ClientSince"] 
        },
        {
            html: '<b>Vertical(s):</b>'
        }, 
		{
            html: records[0].data["Verticals"]
        }, 
		{
        	html: (records[0].data["inactiveDate"] == null)?'<b>Last update on:</b>':'<b>Inactive Date:</b>'
        },
        {
        	 html: (records[0].data["inactiveDate"] == null)?records[0].data["LastUpdatedDate"]:records[0].data["inactiveDate"]
        },
        {
            html: '<b>Total resources:</b>'
        }, 
		{
            html: records[0].data["TotalResources"],			
        }, 
		{
            html: '<b>Shift(s) covered:</b>'
        },
        {
            html: records[0].data["ShiftsCovered"]
        });
		
		param.doLayout();
    	
    	
    },	
	totalResourcescount: function(value) {
		if(value == 0){
			Ext.MessageBox.alert('Alert', 'No Records to Show');
		}else{
			var client_id = AM.app.globals.dashboardClient;		
			AM.app.getView('client.dataPopups').create({client_id:client_id});
		}		
	}	
});