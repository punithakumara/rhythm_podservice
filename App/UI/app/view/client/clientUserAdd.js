Ext.define('AM.view.client.clientUserAdd',{
	extend : 'Ext.window.Window',
	alias : 'widget.clientUserAdd',
	layout : 'fit',
	width : '60%',
    autoShow : true,
    modal: true,
    initComponent : function() {
	
	this.items = [
	       	   		{
	       	   		  xtype : 'form',
	       	   		  id: 'clientUserFrm',
	       	   		  name : 'clientUserFrm',
	       	   		  fieldDefaults: {
		    		    labelAlign: 'left',
		    		    labelWidth: 150,
		    	        anchor: '100%'
	       	   			},
	       	   			bodyStyle : {
	    	    			border : 'none',
	    	    			display : 'block',
	    	    			float : 'left',
	    	    			padding : '10px'
	    	    		},
	    	    		items : [
									{
										 
										 xtype : 'displayfield',
										 name : 'ClientName',
										 id: 'ClientNme',
										 fieldLabel : 'Client',
										 anchor: '100%',
										 
									},
									{
										 
										 xtype : 'hiddenfield',
										 name : 'ClientID',
										 anchor: '100%',
										 value: AM.app.globals.mainClient
									},
									{
										 
										 xtype : 'hiddenfield',
										 name : 'UserID',
										 id: 'UserID',
										 anchor: '100%',
										
									},
									{
										 
										 xtype : 'textfield',
										 name : 'FullName',
										 allowBlank: false,
										 fieldLabel : 'Full Name',
										 id: 'FullName',
										 anchor: '100%'
									},
									{
										 
										 xtype : 'textfield',
										 name : 'Username',
										 allowBlank: false,
										 fieldLabel : 'User Name',
										 anchor: '100%',
										 id: 'Username',
										 minLength : '5',
										 maskRe: /[a-zA-Z0-9.]/
																      	
									},
									{
										 
										 xtype : 'textfield',
										 name : 'Email',
										 allowBlank: false,
										 fieldLabel : 'Email',
										 id: 'Email',
										 anchor: '100%',
										 vtype : 'email'
									},
									{
										 xtype : 'displayfield',
										 value: 'Password will be triggered to your mail once the user is created.',
										 labelWidth: 250,
						    		},
						    		{
										xtype : AM.app.getView('client.Client_Users').create({'ClientID':AM.app.globals.mainClient})
									}
	 							],
	 							
 							buttons : ['->', {
 				        	   text : 'Save',
 				        	   icon : AM.app.globals.uiPath+'resources/images/save.png',
 				        	   handler : this.onSave
 				        	   
 				           },{
	 		        	   text : 'Cancel',
	 		        	   icon : AM.app.globals.uiPath+'resources/images/cancel.png',
	 		        	   handler : this.onCancel
 				           }
 				     	]
	       	   		}
	       	   	];
	       			
	   		this.callParent(arguments);
	},
	
	onSave :  function() {
	
        var win = this.up('window');
		var form = win.down('form').getForm();

        if (form.isValid()) {
        	win.fireEvent('saveClientUsers', form, win);
        }
	},
	onCancel : function() {
		this.up('window').close();
	}

});