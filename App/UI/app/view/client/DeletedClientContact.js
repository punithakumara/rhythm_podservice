Ext.define('AM.view.client.DeletedClientContact',{
	extend				: 'Ext.grid.Panel',
	alias				: 'widget.deletedclientcontact',
	title				: 'Previous Client Contacts',
    id					: 'deletedclientgrid',
    layout				: 'fit',
    viewConfig			: { forceFit	: true },
    width				: '99%',
	minHeight			: 250,
	height 				: 200,
    loadMask			: true,
    disableSelection	: false,
    stripeRows			: false,
    remoteFilter		: true,
    border				: true,
    //store               : 'Client_Contacts',
    initComponent		: function() {
        var me = this;
        Ext.apply(me, {
            store: Ext.create('AM.store.DeleteClientContact').load({params:{'client_id':AM.app.globals.dashboardClient, 'status' : 'deleted'}})
        } );
    	this.columns	= [ {
    		header		: 'Client Contact Person',
    		flex		:  1,
    		dataIndex	: 'contact_name',
    	}, {
			header		: 'Designation', 
			dataIndex	: 'contact_title', 
			flex		:  1
		}, {
            header      : 'Role',
            dataIndex   : 'role',
            flex        :  1,
            renderer    : function(value,metaData,record) {          
                switch (value) {
                    case '1':
                        return "Day to Day in charge";
                    break;
                    case '2':
                        return "Day to Day in charge (UK)";
                    break;
                    case '3':
                        return "Day to Day in charge (US)";
                    break;
                    case '4':
                        return "Main POC";
                    break;
                    default:
                        return "Main POC";
                    break;                           
                }
            }
        }, {
			header		: 'Contact Email',
			dataIndex	: 'contact_email', 
			flex		: 1
		}, {
            header          : 'Service ',
            dataIndex       : 'ServiceName',
            width           : 200,
            renderer        : function(val) {
                var rec     = Ext.getStore('ClientContactVertical').findRecord('ServiceID', val);
                return rec == null ? val : rec.get('ServiceName');
            }
        }, {
			header		: 'Contact Phone No.',
			flex		:  1,
			dataIndex	: 'contact_phone',
		}, {
			header		: 'Contact Mobile No.', 
			flex		:  1,
			dataIndex	: 'contact_mobile',
		}, {
            header  : 'Action',
            flex    : 1,
            xtype   : 'actioncolumn',
            itemId  : 'clientaction',
            items   : [ {
                icon    : AM.app.globals.uiPath+'resources/images/undo.png',
                tooltip : 'Rollback Contact',
                handler : this.RollBack
            } ],
            renderer    : this.renderTip
        } ];
		
		/* this.bbar	= Ext.create('Ext.toolbar.Paging', {
			store		: this.store,
            displayInfo	: true,
            pageSize	: AM.app.globals.itemsPerPage,
            displayMsg	: 'Displaying {0} - {1} of {2}',
            emptyMsg	: "No records to display",
            plugins		: [ Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer') ]
        } ); */
        this.callParent(arguments);
	},
    RollBack  : function(grid, rowIndex, colIndex) {
        rowId = rowIndex;
        colId = colIndex;
        Ext.MessageBox.confirm('Confirm', 'Are you sure to rollback this contact?', 
            function(response) {
                if (response == 'yes') {
                    AM.app.getController('Clients').RollBackContact(grid, rowId, colId);
                }
            }       
        );
    },
	tools	: [ {
		xtype			: 'trigger',
        itemId			: 'gridTrigger',
        fieldLabel		: '',
        labelWidth		: 60,
        labelCls		: 'searchLabel',
        triggerCls		: 'x-form-clear-trigger',
        emptyText		: 'Search',
        width			: 250,
        minChars		: 1,
        enableKeyEvents	: true,
        onTriggerClick	: function() {
        	this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype			: 'tbseparator',
        style			: { 'margin-right' : '10px' }
    } ],
} );