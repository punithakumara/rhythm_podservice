Ext.define('AM.view.client.ClaimAccount',{
	extend : 'Ext.window.Window',
	alias : 'widget.ClaimAccount',
	autoShow : true,
    autoSave: false,
    width : '50%',
    autoHeight : true, 
    layout: 'fit',
    minHeight : 100,   
    modal : true,
    title: 'Claim Account',
    closeAction : 'close',
	initComponent : function() {
		var me = this;
		var SelectedRecords = this.selectionData;	
		var clientDashboardStore = this.gridStre;
		var my_store = Ext.create('Ext.data.Store', {
			fields : [{name : 'ClientID', dataType : 'int'}, 'Client_Name','FullName','AccntMngrID'],
			data:this.selectionData,
		});
		var acctmngrIds = [];
		var clientIDs = [];
		var EmpStore = AM.app.getStore('Employees').load({	params: {'hasNoLimit'	: "1", 'empTypes':  'grade5AndAbove', 'filterName'	: 'first_name'}});
		this.tools =  [{
			xtype : 'button',
			handler: function(grid){
	    		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
				
					if(clientIDs.toString() != ''){
						myMask.show();
				    Ext.Ajax.request({
						url : AM.app.globals.appPath+'index.php/clients/ClaimClientDetails/',
						method: 'POST',
						params: {
							clientID: clientIDs.toString(),
							MngrIds : acctmngrIds.toString()
							},
						success: function (batch, operations) {
					    	var jsonResp = Ext.decode(batch.responseText); 
					    	if(jsonResp.success){
					    		myMask.hide();
					    		Ext.MessageBox.show({
			                        title: jsonResp.title,
			                        msg: jsonResp.msg,
			                        buttons: Ext.Msg.OK,
			                        fn: function(buttonId) {
			                            if (buttonId === "ok") {
			                            	clientDashboardStore.store.load();
			                            	console.log(clientDashboardStore);
			                            	me.close();
			                     
			                            }
			                        }
			                    });						    	
					    	}
					    }
			   	 });
				}else{
					Ext.MessageBox.show({
		                title: 'Warning',
		                msg: 'No records modified',
		                buttons: Ext.Msg.OK,
		                icon: Ext.MessageBox.WARNING
		            });
				}
	    	
			},
			text : 'Claim Account?'
		}];
	

	
		this.items = [
					  {
					xtype:'grid',
					id:'cliamaccountGrid',
					height : 350,					
					autoScroll:true,
					layout: 'fit',
					border : false,
					loadMask: true,
					plugins:[ 
					          Ext.create('Ext.grid.plugin.RowEditing', {
					          clicksToEdit: 1,
					          autoCancel: false,
					          listeners: {
					        	  
					        	  validateedit:function(editor, e, eOpts){},
					        	  beforeedit :  function(editor, e) {				
									},
					        	  edit: function(editor, e, eOpts){
					        		/*selectArrayClientIds = [];
					        		
					        		  for(var i = 0;i < SelectedRecords.length;i++) {
					          			selectArrayClientIds.push(SelectedRecords[i].get('ClientID'));
					          			
					          		}*/
					        		  acctmngrIds.push(e.record.get("FullName"));
					        		  clientIDs.push(e.record.get("ClientID"))
					      			//myParams = selectArrayClientIds.toString();	
					      			e.record.set('AccntMngrID',e.record.get("FullName"));
					        	  },
					        	  canceledit: function(grid,obj){}
					          }
					          })
					      ],
					columns:[{
						header: 'Client ID',
						dataIndex: 'ClientID',
						flex: 1,
						hidden: true
					},{
						header: 'Client Name',
						dataIndex: 'Client_Name',
						flex: 1
					},
					{
						header: '',
						dataIndex: 'AccntMngrID',
						flex: 1,
						hidden : true
					}
					,{
						header: 'Engagement Manager',
						dataIndex: 'FullName',
						flex: 1,
						renderer		: function(value, metaData, record, row, col, store, gridView) {
							var field = isNaN(value) ? 'FullName' : 'EmployID';
							var rec		= EmpStore.findRecord(field, value);						
							return rec == null ? value : rec.get('FullName');
			
						},
						editor: {
					        xtype:'combobox',
							name: 'FullName',
							store		: EmpStore,
							queryMode	: 'remote',
							displayField: 'FullName',
							valueField	: 'EmployID',
							emptyText	: 'Select Employee',						
							multiSelect: false,
							anchor:'20%',
							listeners	: {								
								beforequery	: function(queryEvent) {
									queryEvent.combo.store.getProxy().extraParams = {
										'hasNoLimit'	: "1", 
										'empTypes'		:  'grade5AndAbove',
										'filterName'	: 'first_name'
									};						
								}
							}
							
					    }
					}],
					store: my_store,
					}
		              ];
		this.callParent(arguments);
	 
	},
	listeners:{
        close:function(){
		Ext.getCmp('clientGridId').store.load();	
	},
        scope:this
    }
});