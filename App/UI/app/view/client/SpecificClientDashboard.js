Ext.define('AM.view.client.SpecificClientDashboard',{
	extend : 'Ext.container.Container',
	alias : 'widget.specificlientdashbrd',
	width: '100%',
    loadMask:true,
	margin:5,
	title: 'Specific Dashboard',
    initComponent : function() {
		var me = this;
		this.items=[
			{
				xtype : 'button',
				style: {'visibility': 'hidden'},
			},
			{
				xtype : 'button',
				style: {'visibility': 'hidden'},
				text : 'Back',
			},
			{
				xtype : 'button',
				style: {float:'right','margin-right':'3px'},
				text : 'Back',
				id : 'addTrainingBtnId',
				listeners : {
				click : function() {
						if(AM.app.globals.fromCarsole == 'inactive') {
							AM.app.getController('Clientdashboard').viewContent(AM.app.globals.fromCarsole);
						} else {
							if(me.status == 'InActive') {
								me.status = 0;
							}
							if(me.status == 'Active') {
								me.status = 1;
							} 
							AM.app.getController('Clientdashboard').viewContent(me.status);
						}
					}
				}
			},
			{
				xtype : 'button',
				style: {float:'right','margin-right':'3px','visibility': 'hidden'},
				text : 'MBR / QBR',
				listeners : {
				click : function() {
						AM.app.getController("AM.controller.ClientBr").viewContent(AM.app.globals.dashboardClient);
					}
				}
			},
			{
				xtype : 'button',
				style: {float:'right','margin-right':'3px','visibility': 'hidden'},
				text : 'RDR',
				listeners : {
				click : function() {
						AM.app.getController("AM.controller.ClientRdr").viewContent(AM.app.globals.dashboardClient);
					}
				}
			},
			{
				xtype : 'button',
				style: {float:'right','margin-right':'3px','visibility': 'hidden'},
				text : 'NRR',
				listeners : {
				click : function() {
						AM.app.getController("AM.controller.ClientNrr").viewContent(AM.app.globals.dashboardClient);
					}
				}
			},
			
			{
				xtype : 'button',
				style: {float:'right','margin-right':'-130px'},
				icon: AM.app.globals.uiPath+'resources/images/add.png',
				text : 'Export CDD',
				listeners : {
				click : function() {
					//AM.app.getController('Clientdashboard').downloadclientviewPDF(AM.app.globals.dashboardClient);
					AM.app.getController('Clientdashboard').downloadclientCDD(AM.app.globals.dashboardClient);
					}
				}
			},
			{
				xtype : 'button',
				style: {float:'right','margin-right':'3px'},
				icon: AM.app.globals.uiPath+'resources/images/add.png',
				text : 'Export To PDF',
				listeners : {
				click : function() {
					AM.app.getController('Clientdashboard').downloadclientviewPDF(AM.app.globals.dashboardClient);
									}
				}
			},
			// Client Logo
			 {
				 type: 'image',
				 style: {float:'right','margin-right':'400px','width':'100px'},
				 html: '<img src="'+AM.app.globals.uiPath+'resources/uploads/client/'+AM.app.globals.clientLogo+'" width="100" height="25">',
			 },

			// End Client Logo
			
			
			{
				xtype:'tabpanel',
				margin:'5 0 0 0',
				plain: true,
				items:[
					{
						title: 'Client Description',
						html: 'Client Details',
						border:true,
						xtype:'clientdescription'
					
					}
				]
			},{
				xtype:'tabpanel',
				margin:'5 0 0 0',
				plain: true,
				items:[
					{
						title: 'Vertical Description',
						html: 'Vertical Details',
						border:true,
						xtype:'clientrelvert'
					
					}
				]
			},{
				xtype:'tabpanel',
				margin:'5 0 0 0',
				plain: true,
				items:[
					{
						title: 'Internal process contacts',
						html: 'Contact Details',
						border:true,
						xtype:'clientdetails'
					
					}
				]
			},{
				xtype:'tabpanel',
				margin:'5 0 0 0',
				plain: true,
				items:[
					{
						title: 'Client contacts',
						html: 'Client Contact Details',
						border:true,
						xtype:'clientCList'
					
					}
				]
			}
		]
	
	this.callParent(arguments);
	
	}
    
});