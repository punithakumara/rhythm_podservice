Ext.define('AM.view.client.WizardForm', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.wizardClientsDashboard',
	width : '99%',
	minHeight : 500,
	border : false,
	layout  : 'fit',
	itemId  : 'ClientsDashboardWizard',
	
	//requires:['client.GridPanel'],
	initComponent : function() {
		var me	= this;
		this.items= [{
			xtype:'container',
			"layout":
			{
			   "type": "vbox",
			   "align": "stretch"
			},
			"width": "100%",
			"height":"100%",
			items:[{
				xtype: 'TopPanelClientDashboard',
				margin:'0 0 10 0'
			},{
				xtype:AM.app.getView('client.Clientdashboard').create()	
			} ]
		} ];
		
		this.callParent(arguments);
	}
});
