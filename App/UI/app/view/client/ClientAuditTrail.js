Ext.define('AM.view.client.ClientAuditTrail', {
    extend          : 'Ext.grid.Panel',
    xtype           : "grouped-grid",
    alias           : 'widget.ClientAuditTrail',
    title           : 'Client Audit Trail',
    layout          : 'fit',
    store           : 'ClientAuditTrail',
    width           : '99%',
    minHeight       :  200,
    loadMask        : true,
    stripeRows      : false,
    autoScroll 		: true,
    remoteFilter    : true,
    border          : true,
    selType         : "rowmodel",
    requires        : [ "Ext.grid.feature.Grouping"],
    features        : [ {ftype  : "grouping", groupHeaderTpl  : "{name}", collapsible  : false} ],

    initComponent   : function() {
        this.columns    = [ {
            header      : "Client_Name",
            flex        : 1,
            sortable	: false
        }, {
            header      : "Process name",
            dataIndex   : "Process_name",
            flex        : 1,
            sortable	: false
        }, {
            header      : "Effective Date",
            dataIndex   : "Production_Date",
            flex        : 1,
            sortable	: false
        }, {
            header      : "NRR",
            dataIndex   : "No_of_Resources_nrr",
            flex        : 0.3,
            sortable	: false
            //renderer    : this.reliveInPercent
        }, {
            header      : "RDR",
            dataIndex   : "No_of_Resources_rdr",
            flex        : 0.3,
            sortable	: false
            //renderer    : this.reliveInPercent
        }, {
            header      : "Head Count",
            dataIndex   : "HeadCount",
            flex        : 0.3,
            sortable	: false
            //renderer    : this.reliveInPercent
        }  ];
        this.tools          = [{
    		xtype : 'button',
    		//id : 'nrrBackButtonID',
    		text : 'Back',				
    		listeners : {
    			click : function() {
    				AM.app.getController('Clientdashboard').viewContent();
    			}
    		}
    	} ],
        this.callParent(arguments);
    },
    getEmployeeCntLink: function(rec) {
        return '<a href="#">'+rec+'</a>';
    },
    getMonthName: function(month) {
        var mName  = new Array();
        mName[1]   = 'January';
        mName[2]   = 'February';
        mName[3]   = 'March';
        mName[4]   = 'April';
        mName[5]   = 'May';
        mName[6]   = 'June';
        mName[7]   = 'July';
        mName[8]   = 'August';
        mName[9]   = 'September';
        mName[10]  = 'October';
        mName[11]  = 'November';
        mName[12]  = 'December';
        return mName[month];
    },
    reliveInPercent: function(getnumber) {
        return parseFloat(getnumber).toFixed(2) +'%';
    }

});