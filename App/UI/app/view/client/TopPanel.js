Ext.define('AM.view.client.TopPanel', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.TopPanelClientDashboard',
	layout  : 'fit',
	collapsible: true,
	collapsed: true,
	floatable: true,
	width : '99%',
	id:'TopPanelClientDashboard',
	title   : 'Filter',
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
  
	initComponent : function() {
  		var me = this;
		
		var dataStore = new Ext.data.SimpleStore({
			fields: ['monthValue', 'monthRange'],
			data: [['0', ' Select Client Partner'],['1', 'Engagement manager']]
		});
		
		var dataStore2 = new Ext.data.SimpleStore({
			fields: ['monthValue', 'monthRange'],
			data: [["5","Vinay Nanjundaswamy"],['1', ' Select Engagement manager']]
		});

		var labelWidth = 300;
		var enddatewidth = 300;
		if(Ext.util.Cookies.get("grade") > 4){
			labelWidth = 250;
			enddatewidth = 350;
		}
		this.items = [{
			xtype:'container',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},		
			width: 800,
			heigth:500,
			style:{
				'margin-left':'20px'
			},
			items:[{
				xtype:'container',
				layout: {
					type: 'hbox',
				},
				defaults: {
					labelWidth: 80,
					style: 
					{
						'padding': '20px',
					}
				},
				width: 800,
				heigth:500,
				items: [{
					xtype		:'boxselect',
					multiSelect	: false,
					fieldLabel	: 'Engagement Manager ',
					name		: 'accManager',
					id			: 'Engagment_Managercomboid',
					store		: Ext.getStore('Employees'),
					queryMode	: 'remote',
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText	: 'Select Manager',
					minChars	: 0,
					width       : 340,
					style		:{
						'position':'absolute',
						'margin-left':'40px',
						'margin-top':'12px'
					},
					labelWidth: 140,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						},
						afterrender: function() {
							Ext.getStore('Employees').getProxy().extraParams = {
								'hasNoLimit'	: "1", 
								'empTypes'		:  'grade5AndAbove',
								'filterName'	: 'FirstName'
							};
						}
					}
				},{
					xtype		: 'boxselect',
					fieldLabel	: 'Client Partner ',
					multiSelect	: false,
					width		: 320,
					store		: Ext.getStore('Employees'),
					name		: 'clientPartner',
					queryMode	: 'remote',
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText	: 'Select Client Partner',
					//margins		: '0 0 0 20',
					id			: 'ClientPartnercomboid',
					minChars	: 0,
					style		:{
						'position':'absolute',
						'margin-left':'15px',
						'margin-top':'12px',
						'margin-right':'11px'
					},
					labelWidth: 90,
					listeners	: {
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							AM.app.getStore('Employees').getProxy().extraParams = {
								'hasNoLimit':'1', 
								'empTypes'	: empTypes,
								'filterName': 'FirstName'
							};
						},
						afterrender: function() {
							Ext.getStore('Employees').getProxy().extraParams = {
								'hasNoLimit'	: '1', 
								'empTypes'		: 'grade5AndAbove',
								'filterName'	: 'FirstName'
							};
						}
					}

				},{
					xtype: 'combobox',
					style:{
						'position':'absolute',
						'margin-right':'40px',
						'margin-top':'12px',
						'margin-bottom':'10px',
						'margin-left':'15px',
					}, 
					itemId		: 'clientStatusComboBox',
					fieldLabel	: 'Status ',
					id			:'clientStatusComboBox',
					store		: Ext.create('Ext.data.Store', {
						fields	: ['status', 'clientStatus'],
						data	: [ {
							'status'		: '1',
							'clientStatus'	: 'Active'
						}, {
							'status'		: '2', 
							'clientStatus'	: 'MyAccount'
						}, {
							'status'		: '3',
							'clientStatus'	: 'Followed Account'
						}, {
							'status'		: '0',
							'clientStatus'	: 'InActive'
						} ]
					} ),
					displayField: 'clientStatus', 
					valueField: 'status',
					labelWidth: 60,
					value: "1",
					listeners: {
						select: function(combo) {
							if(combo.getValue() == 0) {
								if(me.columns != undefined) {
									me.columns[0].hide();
								}
							} else {
								if(me.columns != undefined) {
									me.columns[0].show();
								}
							}
						}
					}
				}]
			}]	              
		}];
		
		this.buttons = [{
			text: 'Submit',
			name: 'ShowUtilization',
			handler: function() 
			{
				AM.app.globals.fromCarsole = "";
				AM.app.getController('Clientdashboard').finalresultsubmit();
			}
		},{
			text: 'Reset',
			name: 'ResetClient',
			handler: function() 
			{
				this.up('TopPanelClientDashboard').getForm().reset();
			}
		}];
		
		this.callParent(arguments);
	}
});
