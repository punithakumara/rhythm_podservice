Ext.define('AM.view.client.Clientdetails',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.clientdetails',
	
	title: 'Client Details Page',
  
    id : 'processContact',
    layout : 'fit',
    viewConfig : {
	  forceFit : true,
	},
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    stripeRows: false,
    remoteFilter:true,
    border : true,
    store: 'InternalProcessContacts',
	
    initComponent : function() {
  			var myStore = Ext.getStore('InternalProcessContacts');//{params:{ClientID:AM.app.globals.dashboardClient}}
  			myStore.getProxy().extraParams = {ClientID:AM.app.globals.dashboardClient}
  			myStore.load();
  			
  	this.columns = [ {
					header : 'Employee ID', 
					id : 'EmployID',
					dataIndex : 'EmployID', 
					flex: 1,
					hidden:true,
				},{
					header : 'Contact Name', 
					id : 'name',
					dataIndex : 'FirstName', 
					flex: 1.5
				}, {
					header : 'Designation',
					id : 'designation',
					dataIndex : 'Designation', 
					flex: 1
				},{
					header : 'Vertical',
					id : 'vertical',
					dataIndex : 'Vertical', 
					flex: 1
				},
				{
					header : 'Process',
					id : 'process',
					dataIndex : 'Name',					
					flex: 1.3
				},{
					header : 'Email',
					id : 'email',
					dataIndex : 'email', 
					flex: 1
				},{
					header : 'Contact Number',
					id : 'contact',
					dataIndex : 'mobile', 
					flex: 0.7
				}
				
				]
				
			 
    	this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: 15,
            ClientID:AM.app.globals.dashboardClient,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No clients to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		
		this.callParent(arguments);
	},
//This is to pass a parameter when sorting - starts
	listeners: {
        sortchange: function(){
        	Ext.getStore('InternalProcessContacts').load({params:{ClientID:AM.app.globals.dashboardClient}});
        }
    },
  //This is to pass a parameter when sorting - Ends
	tools : [{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        fieldLabel : '',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }],
	
	onDelete : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('deleteClient', grid, rowIndex, colIndex);
	},
	
	onView : function(grid, rowIndex, colIndex) {
		var viewport = Ext.ComponentQuery.query('viewport')[0];
		var gridPanel = viewport.down('grid');
		gridPanel.fireEvent('viewClient', grid, rowIndex, colIndex);
	}
});