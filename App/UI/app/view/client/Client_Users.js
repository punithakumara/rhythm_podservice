Ext.define('AM.view.client.Client_Users',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.ClientUserList',
	//store : 'Client_Contacts',	
	title: 'Client Users List',    
    id : 'Client_UsersGridId',
    layout : 'fit',
    selType: 'rowmodel',
    height: 100,
    autoScroll : true,
    emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
	
    width : '99%',
    minHeight : 225,
    loadMask: true, 
    border : true,
    store : 'ClientUser',
    initComponent : function() {
		var me = this;
		var myStore = Ext.getStore('ClientUser');
		myStore.getProxy().extraParams = {'ClientID':this.ClientID}
		myStore.sorters.clear();
		myStore.load();
			//**** List View Coloumns ****//
    	this.columns = [
				{
					header : 'Full Name', 
					dataIndex : 'FullName', 
					flex: 1
				},{
					header : 'Username', 
					dataIndex : 'Username', 
					flex: 1
				},{
					header : 'Email', 
					dataIndex : 'Email', 
					flex: 1
				},
				{
					header : 'Edit', 
					width : '20%',
					xtype : 'actioncolumn',
					sortable: false,
					items : [{
						icon : 'resources/images/edit.png',
						tooltip : 'Edit',
						handler : this.onEdit
					}]
				}
		];
		//**** End List View Coloumns ****//
		
		//**** Pagination ****//
		/*this.bbar = Ext.create('Ext.toolbar.Paging', {			
            store: this.store,
            displayInfo: true,
            pageSize: 5,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Items to display"
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });*/
		//**** Pagination ****//
		
		//**** Search Box And Add Button ****//
		this.tools = [
				
					//**** Separator ****//
					{ 
					xtype : 'tbseparator',
					style : {
								'margin-right' : '10px'
							}
					},
					
					
		];
		//**** End Search Box And Add Button ****//
		
		this.callParent(arguments);
	},
	onEdit: function(grid, rowIndex, colIndex) {
		var win = this.up('panel').up('window');
		//AM.app.getController("Clients").oneditClientUser(grid, rowIndex, colIndex);
    	win.fireEvent('editClientUsers',  grid, rowIndex, colIndex);
	
	}
});