Ext.define('AM.view.client.ClientContact',{
	extend				: 'Ext.grid.Panel',
	alias				: 'widget.clientCList',
	title				: 'Client Contact',
    itemId				: 'clientGridId',
    layout				: 'fit',
    viewConfig			: { forceFit	: true },
    width				: '99%',
    minHeight			: 200,
    loadMask			: true,
    disableSelection	: false,
    stripeRows			: false,
    remoteFilter		: true,
    border				: true,
    store               : 'Client_Contacts',
    initComponent		: function() {
        var myStore = Ext.getStore('Client_Contacts');
            myStore.getProxy().extraParams = {'client_id'    : AM.app.globals.dashboardClient}
            myStore.load();

    	this.columns	= [ {
    		header		: 'Client Contact Person',
    		id			: 'Contact_Name',
    		flex		: 1,
    		dataIndex	: 'contact_name',
    	}, {
    		header 		: 'Designaiton', 
			id			: 'Contact_Title',
			dataIndex 	: 'contact_title', 
			flex		: 1
		}, {
    		header 		: 'Service', 
			id			: 'Contact_Service',
			dataIndex 	: 'ServiceName', 
			flex		: 1
		}, /*{
    		header 		: 'Process', 
			id			: 'Contact_Process',
			dataIndex 	: 'ProcessName', 
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("ProcessName") + '"';
				return value;
			},
			flex		: 1
		},*/ {
			header 		: 'Contact Email',
			id 			: 'Contact_Email',
			dataIndex 	: 'contact_email', 
			flex		: 1
		}, {
			header 		: 'Contact Phone No.',
			flex		: 1,
			dataIndex 	: 'contact_phone',
		}, {
			header 		: 'Contact Mobile No.',
			flex		: 0.8,
			dataIndex 	: 'contact_mobile',
		},{
			header 		: 'Role',
			flex		: 0.8,
			dataIndex 	: 'role',
			renderer:function(value,metaData,record){			
			    switch (value) {
		        case '1':
		            return "Day to Day in charge";
		            break;
		        case '2':
		            return "Day to Day in charge (UK)";
		            break;
		        case '3':
		            return "Day to Day in charge (US)";
		            break;
		        case '4':
		            return "Main POC";
		            break;
		        default:
		            return "Main POC";
		            break;                           
			    }
			}	
		} ];
		
		this.bbar		= Ext.create('Ext.toolbar.Paging', {
			store		: this.store,
            displayInfo	: true,
            pageSize	: AM.app.globals.itemsPerPage,
            displayMsg	: 'Displaying {0} - {1} of {2}',
            clientID    : AM.app.globals.dashboardClient,
            emptyMsg	: "No clients to display",
            plugins 	: [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        } );
        this.callParent(arguments);
	},
	tools	: [ {
		xtype			: 'trigger',
        itemId			: 'gridTrigger',
        fieldLabel		: '',
        labelWidth		: 60,
        labelCls		: 'searchLabel',
        triggerCls 		: 'x-form-clear-trigger',
        emptyText 		: 'Search',
        width 			: 250,
        minChars 		: 1,
        enableKeyEvents : true,
        onTriggerClick 	: function() {
        	this.reset();
            this.fireEvent('triggerClear');
        }
    }, {
    	xtype : 'tbseparator',
        style : { 'margin-right'	: '10px' }
    } ]
} );