Ext.define('AM.view.client.dataPopups',{
	extend : 'Ext.window.Window',
	alias : 'widget.dataPopups',
	layout: 'fit',
	width : '50%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal: true,
    id : 'clientpopup',
    y : 100,
	title : 'Employee Details',
	
    initComponent : function() {
	
		var CleintID = this.clientId;
		var verticalID = this.verticalID;
		var type = this.type
		var locationID = this.locationID;
		var shiftcode = this.shiftcode;
	
		this.items = [
    	   {
	    		xtype : 'grid',
	    		itemId : 'clientgridID',
				width : '99%',
				height : 400,
				border : true,
	            store : Ext.getStore('clientPopup').load({params:{clientID:CleintID,verticalID:verticalID,type:type,shiftcode:shiftcode,locationID:locationID}}),
	            loadMask: true,
	            columns : [/*{
					header : 'SI No', 
					flex: 1,
					dataIndex : 'SlNo',
				},*/{
					header : 'Company ID', 
					flex: 1,
					dataIndex : 'CompanyEmployID',
				},{
					header : 'Name', 
					flex: 1,
					dataIndex : 'FirstName',
				},{
					header : 'Designation', 
					flex: 1,
					dataIndex : 'Name',
				},
				{
					header : 'Billable Percentage', 
					flex: 1,
					dataIndex : 'BillablePercentage',
				}
				]
    	   }];    			
		this.callParent(arguments);
	},	
});