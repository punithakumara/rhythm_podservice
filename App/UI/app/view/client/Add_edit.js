Ext.define('AM.view.client.Add_edit',{
	extend			: 'Ext.window.Window',
	alias			: 'widget.clientAddEdit',	
	title			: 'Client details',    	
	layout			: 'fit',
	width			: '90%',
	height 			: 600,
	autoShow		: true,
	autoSave		: false,
	id				: 'clientAddEdit',
	modal			: true,
	closeAction		: 'destroy',
	closable:false,
	initComponent	: function() {
		var typeOfIndustries = new Ext.data.SimpleStore({
			fields	: ['industryType', 'typeOfIndustries'],
			data	: [ 
			['Agency', 'Agency'],
			['Network', 'Network'],
			['Technical Provider', 'Technical Provider'],
			['Publisher', 'Publisher'],
			['Advertiser', 'Advertiser'] 
			]
		});
		
		//************* local store for Status *******************//
		var status = new Ext.data.SimpleStore({
			fields	: ['status', 'statusName'],
			data	: [ ['1', 'Active'],['0', 'Inactive'] ]
		});
		
		var Client_tier_store = new Ext.data.SimpleStore({
			fields	: ['Tier', 'Value'],
			data	: [ ['1', 'Tier 1'],['2', 'Tier 2'],['3', 'Tier 3'] ]
		});
		
		Ext.tip.QuickTipManager.init();  // enable tooltips
		var clientDescription = new Ext.FormPanel({
			title: 'Description',
			bodyStyle:'padding:5px 5px 0',
			layout: 'fit',
			items: [{
				xtype:'textareafield',
				name: 'client_description',
				border: false,
				height: 190,
				width: 370,
				autoScroll: true,
				margin: '10 10 10 10',
				anchor: '100%',
				grow: true,
				maxLength:65535,
				enforceMaxLength: true,       
			}]
		});
		
		var rEditing = Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			autoCancel: false,
			saveBtnText: 'Save',
			listeners: 
			{			
				validateedit:function(editor, e, eOpts){
					
					var Validity	= true;
					console.log(Validity);
					var validate 	=  AM.app.getController('Client_Contacts').validateattributeedit(editor,e);

					if(validate == false) 
					{
						Validity = false;
					} 
					else 
					{
						Validity = true;
					}
					
					if(Validity) 
					{
						return true;
					} 
					else 
					{
						Ext.MessageBox.show({
							title:'Alert',
							msg:"Add Attribute Names",
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.WARNING,
						});
						
						return false;
					}
				},
				edit: function(grid, obj, eOpts){

					var data = obj.view.store.data;										
					var dataArray = [];			
					Ext.each(data.items, function(value, i) {
						dataArray.push(value.data.name);
					});
					/* added to order  Attributes*/
					dataArray = JSON.stringify(dataArray);
					var form = obj.grid.up('window').down('form').getForm();
					form.findField("Attribute_Names").setValue(dataArray.toString());
				},
				canceledit: function(grid,obj){
					if(obj.store.data.items[0].data['name'] == "")
						obj.grid.getStore().remove(obj.record);
				}
			}
		});
		
		var attrStore = Ext.create('Ext.data.Store', {
			fields : [{name : 'attributes_id', dataType : 'int'}, 'name','OrderofAttributes'],
			data: [],
		});
		
		this.items	= [ {
			xtype		: 'form',
			bodyStyle		: {
				border		: 'none',
				display 	: 'block',
				float 		: 'left',
				padding 	: '5px'
			},
			fieldDefaults		: {
				labelAlign	: 'left',
				labelWidth	: 85,
				anchor		: '100%',
			},
			items	: [ {
				xtype	:'container',
				layout	: {
					type	: 'hbox',
					pack	: 'center',
					align	: "center",
				},
				margin	: 1,
				items	: [ {
					xtype	: 'hiddenfield',
					name	: 'client_id'
				}, {
					xtype		: 'textfield',
					itemId		: 'nameItemId',
					name		: 'client_name',
					allowBlank	: false,
					emptyText	: 'Enter Client Name',
					labelWidth	: 110,
					fieldLabel	: 'Name <span style="color:red">*</span>',
					margins 	: 2,
					flex		: 1,
					maskRe		: /[a-zA-Z0-9. &-]/,
					listeners	: {
						scope	: this,
						'blur'	: function(text) {
							text.setValue(text.getValue().trim());
						}
					}
				}, {
					xtype		: 'textfield',
					name		: 'web',
					fieldLabel	: 'Web',
					regex:/^(www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?$/,
					regexText:'The Feild should be Url  Example : www.theoreminc.net',
					margins		: '0 0 0 20', 
					flex		: 1,
				}]
			}, {
				xtype	:'container',
				layout	: {
					type	: 'hbox',
					pack	: 'center',
					align	: "center",
				},
				margin		: 1,
				items		: [{
					xtype 		: 'textfield',
					name		: 'parent_company',
					allowBlank	: false,
					emptyText	: 'Enter Parent Company Name',
					labelWidth	: 110,
					fieldLabel	: 'Parent Company <span style="color:red">*</span>',
					margins 	: 2,
					flex		: 1,
					maskRe		: /[a-zA-Z0-9. &-]/,
					listeners	: {
						scope	: this,
						'blur'	: function(text) {
							text.setValue(text.getValue().trim());
						}
					}
				},{
					xtype			:'boxselect',
					multiSelect		: true,
					allowBlank		: false,
					fieldLabel		: 'Country <span style="color:red">*</span>',
					itemId			: 'CountryComBox',
					name			: 'country',
					triggerOnClick	: true,
					filterPickList	: true,
					store			: 'Country',
					queryMode		: 'local',
					emptyText		: 'Select Country',
					displayField	: 'country_name',
					valueField		: 'country_name',
					minChars		: 1 ,
					margins			: '0 0 0 20',
					//margins 		: 2,
					flex			: 1,
					//value           : '',
					listeners		: {
						scope	: this,
						'blur'	: function(text) {
							text.setValue(text.getValue());
						}
					}
				}]
			}, {
				xtype	: 'container',
				layout	: {
					type	: 'hbox',
					pack	: 'center',
					align	: "center",
				},
				style	: {
					'position'	: 'relative',
				},
				items	: [ {
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel	: 'Acc Manager',
					store: Ext.getStore('Engagementmanagers'),
					queryMode: 'remote',
					name: 'acc_manager',
					minChars:0,
					labelWidth	: 111,
					margins : 2,
					flex		: 1,
					displayField: 'full_name',
					valueField	: 'employee_id',
					emptyText: 'Select Account Manager',
					width: 630,
					renderer: function(value) {
						var rec	= Ext.getStore('Engagementmanagers').findRecord('employee_id', value);
						return rec == null ? value : rec.get('full_name');
					}
				},{
					xtype			:'textfield',
					name			: 'state',
					fieldLabel		: 'State',
					margins			: '0 0 0 20',
					flex			: 1,
					listeners		: {
						scope		: this,
						'blur'		: function(text) {
							text.setValue(text.getValue().trim());
						}
					}
				}]
			}, {
				xtype		: 'container',
				layout		: {
					type	: 'hbox',
					pack	: 'center',
					align	: "center",
				},
				margin		: 1,
				items		: [{
					xtype		: 'boxselect',
					fieldLabel	: 'Client Partner',
					multiSelect	: false,
					width		: '48.35%',
					labelWidth	: 110,
					store : Ext.getStore('Clientpartner'),
					name		: 'client_partner',
					queryMode	: 'remote',
					displayField: 'full_name',
					valueField	: 'employee_id',
					emptyText	: 'Select Client Partner',
					margins		: 2,
					flex		: 1,
					listeners	: {
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							AM.app.getStore('Employees').getProxy().extraParams = {
								'hasNoLimit':'1', 
								'empTypes'	: empTypes, 
								'filterName': 'first_name'
							};
						},
						afterrender: function() {
							Ext.getStore('Employees').getProxy().extraParams = {
								'hasNoLimit'	: '1', 
								'empTypes'		: 'grade5AndAbove',
								'filterName'	: 'first_name'
							};
						}
					}
				}, {
					xtype		:'boxselect',
					multiSelect	: false,
					store		: Client_tier_store,								
					fieldLabel	: 'Client Tier',
					width		: '48.35%',
					name		: 'client_tier',
					queryMode	: 'local',
					minChars	: 0,
					displayField: 'Value', 
					valueField	: 'Tier',
					emptyText	: 'Select Tier',
					margins			: '0 0 0 20',
					flex		: 1,
				} ]
			}, {
				xtype	: 'container',
				layout		: {
					type	: 'hbox',
					pack	: 'center',
					align	: "center",
				},
				margin	: 1,
				items	: [ {
					xtype		:'boxselect',
					multiSelect	: false,
					store		: typeOfIndustries,								
					fieldLabel	: 'Client Type',
					name		: 'industry_type',
					queryMode	: 'local',
					labelWidth	: 110,
					minChars	: 0,
					displayField: 'typeOfIndustries', 
					valueField	: 'industryType',
					emptyText	: 'Select Client Type',
					margins		: 2,
					flex		: 1,
				}, {
					xtype		: 'hiddenfield',
					name		: 'logo',
					id			: 'Logo'
				}, {
					xtype		: 'filefield',
					margins 	: '0 0 0 20',
					emptyText 	: 'Select Logo',
					name 		: 'NewLogo',
					fieldLabel 	: 'Logo',
					buttonText	: 'Select Logo...',
					flex		: 1,
					listeners: {
						change: function(fld, value) {
							var newValue = value.replace(/C:\\fakepath\\/g, '');
							var file_ext = newValue.split('.').pop();
							var stringArray = ["jpg", "jpeg", "png"];

							if (stringArray.indexOf(file_ext) >= 0) 
							{
								fld.setRawValue(newValue);
							}
							else
							{
								Ext.Msg.alert("Alert", "Logo Should be either jpg ,jpeg , png Types ");
								fld.setRawValue("");
							}
						}
					}
				}]
			}, {
				xtype	: 'container',
				layout	: 'hbox',
				margin	: 1,
				items	: [ {
					xtype  		: 'datefield',
					width		: '50%',
					fieldLabel	: 'Client Since',
					margins		: 2,
					labelWidth	: 110,
					value : new Date(),
					valueField	: 'status',
					style  		: {
						margins 	: '5px 0 0 0'
					},
					format  	: AM.app.globals.CommonDateControl,
					name  		: 'created_on',
					id   		: 'clientSince',
					flex		: 1,
					listeners: {
						change : function(combo) {
							Ext.getCmp('inactivedate').setMinValue(combo.getValue());
						}
					}
				}, {
					xtype		: 'container',
					layout		: { type	: 'hbox', pack	: 'end'},
					items		: [ {
						xtype	: 'image',
						name 	: 'logo',
						id 		: 'imgLogo',
					} ],
					margins			: '0 0 0 20',
					flex		: 1
				} ]
			}, {
				xtype	: 'container',
				layout	: 'hbox',
				margin	: 1,
				items	: [ {
					xtype 		: 'boxselect',
					multiSelect	: false,
					itemId		: 'client_Status_combo',
					store		: status,								
					fieldLabel	: 'Status',
					name		: 'status',
					labelWidth	: 110,
					queryMode	: 'local',
					minChars	: 0,
					displayField: 'statusName', 
					valueField	: 'status',
					emptyText	: 'Status',
					allowBlank	: false,
					value		: '1',
					margins 	: 2,
					width		: '50%',
					listeners: {
						select : function(combo) {
							if(combo.getValue()==0)
							{
								Ext.getCmp('inactivedate').setVisible(true);
								Ext.getCmp('inactiveReason').setVisible(true);
							}
							else
							{
								Ext.getCmp('inactivedate').setVisible(false);
								Ext.getCmp('inactiveReason').setVisible(false);
							}
						},
						change : function(combo) {
							if(combo.getValue()==0)
							{
								Ext.getCmp('inactivedate').setVisible(true);
								Ext.getCmp('inactiveReason').setVisible(true);
							}
							else
							{
								Ext.getCmp('inactivedate').setVisible(false);
								Ext.getCmp('inactiveReason').setVisible(false);
							}
						}
					}
				},{
					xtype:'datefield',
					fieldLabel	: 'Inactivation Date <span style="color:red">*</span>',
					name  		: 'inactive_date',
					id   		: 'inactivedate',
					//allowBlank  : false,
					hidden 		: true,
					valueField	: 'inactive_date',
					margins		: '0 0 0 20',
					flex		: 1,
					format  	: AM.app.globals.CommonDateControl,
				}]
			},{
				xtype	: 'container',
				pack	: 'center',
				align	: "center",
				margin 	: 1,
				items	: [ {
					xtype			:'textareafield',
					name			: 'reason',
					fieldLabel		: 'Reason for Inactivation <span style="color:red">*</span>',
					margins			: '0 0 0 20',
					//allowBlank		: false,
					labelWidth		: 110,
					margins 		: 2,
					flex			: 1,
					id 				: 'inactiveReason',
					name 			: 'reason',
					valueField		: 'reason',
					width 			: 843,
					hidden: true,
				}]
			}, {
				xtype 		: 'hidden',
				itemId		: 'when_add_contact',
				name		: 'when_add_contact',
				allowBlank	: true,
				value		:'',
			}, {
				xtype 		: 'hidden',
				itemId		: 'service_array',
				name		: 'service_array',
				allowBlank	: true,
				value		:'',
			}, {
				xtype 		: 'hidden',
				itemId		: 'country_array',
				name		: 'country_array',
				allowBlank	: true,
				value		:'',
			}, {
				xtype: 'hidden',
				name: 'Attribute_Names',
			}, {
				xtype		: 'tabpanel',
				itemId		: 'Client_contact_grid',
				margin		: '5 0 0 0',
				plain		: true,
				items		: [{
					xtype	: AM.app.getView('Client_Contacts.List').create(),
					store	: Ext.getStore('Client_Contacts').load( {
						params	: {
							'client_id'	: AM.app.globals.mainClient,
						}
					} ),
				},{
					xtype	: AM.app.getView('client.DeletedClientContact'),
				}, {
					title	: 'Client Description',
					border	: true,
					items: [clientDescription],
				}],
				listeners	: {
					'tabchange'	: function(tabPanel, tab) {
						var myStore = tabPanel.items.items[1].store;
						if(tab.id == 'deletedclientgrid') {
							myStore.getProxy().extraParams = {
								'client_id'	: AM.app.globals.mainClient,
								'status'	: 'deleted'
							}
							myStore.load();
						}
					}
				}
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		},{
			text 	: 'Cancel',
			itemId:'ClientCancelBtn',
			id:'ClientCancelBtn',
			icon 	: AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}, {
			text 	: 'Submit',
			itemId:'ClientSaveBtn',
			id:'ClientSaveBtn',
			icon 	: AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];
		
		this.callParent(arguments);
	},
	
	//defaultFocus	: 'nameItemId',
	
	onSave 			:  function() {
		var win		= this.up('window');
		var form 	= win.down('form').getForm();
		if (form.isValid()) 
		{
			values = form.getValues();
			var vertical_array = [];
			// var cont_name	= Ext.getCmp('cont_na');
			// var cont = cont_name.getValue();
			// console.log(cont);
			
			form.findField("service_array").setValue(values.service_name);
			form.findField("country_array").setValue(values.country);
			AM.app.getController('Clients').onSaveClient( form, win)
		}
	},
	
	onCancel	: function() {
		this.up('window').destroy();
	},
	
	onNewAttributes : function() {
		var form = this.up('form').getForm();
		var win = this.up('window');
		AM.app.getController('Clients').dispNewProcessAttrs(form);
	},
	
	renderTip : function(val, meta, rec, rowIndex, colIndex, store) {
		meta.tdAttr = 'data-qtip="Drag and drop to re-order  attributes"';
		return val;
	}
	
} );