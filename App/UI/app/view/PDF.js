Ext.define('AM.view.PDF',{
	extend : 'Ext.window.Window',
	alias : 'widget.DocumentsPDF',
	
	title: 'Document Viewer',
    constrain:true,	
	layout: 'fit',
	width:'80%',
	height:600,
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    resizable: false,
    //autoScroll: true,
    //overflow: 'scroll',
    modal: true,
    //y : 100,

	
    initComponent : function() {	

		//alert(this.doctype);
		
		var docpath = this.doctype;

		if(Ext.util.Cookies.get('grade')<5)
		{
			docpath += "#toolbar=0";
		}
		
		this.items = [
		{
			xtype:'panel',
				items: { 
					xtype: 'box',					
					autoEl: { 
						tag: 'iframe', 
						style: 'height: 100%; width: 100%', 
						// src: AM.app.globals.uiPath+'resources/uploads/'+docpath,
						src: docpath,
					} 
					,listeners: {
				        el: {
				            load: function() {
				                var iframe = this.dom,
				                    iframeWindow = (iframe.contentWindow) ? iframe.contentWindow : (iframe.contentDocument.document) ? iframe.contentDocument.document : iframe.contentDocument,
				                    docEl = Ext.fly(iframeWindow.document),
				                    body = docEl.down('body'); // instance of Ext.dom.Element
				                    iframeWindow.document.oncontextmenu =new Function("return false;");
				                    console.log(iframeWindow);
				                    iframeWindow.location.oncontextmenu =new Function("return false;");
				            }
				        }
				    }
				}		
		}];
		
		this.callParent(arguments);
	},

	
	
});