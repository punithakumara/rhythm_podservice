Ext.define('AM.view.NotifyMenu', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.Notifymenu',
    id: 'ExtNotifyMenuToolbar',
    style: {
        'padding-bottom': '5',
        background: '#276193'
    },
    transitionType: 'slide',
    delay: 0.9,
    animate: true,
    initComponent: function() {
        var me = this;
        this.items = [];
        this.nIds = [];

        var AccMgtSubmenu = Ext.create('Ext.menu.Menu', {
            bodyStyle: 'background-color:#f1f1f1 !important;',
            id: 'accmgtsubmenu',
            style: {
                border: 'solid 3px #4499EE',
                borderRadius: '1px',
                height: '350px'
            },
        });

       /* var AllViewSubmenu = Ext.create('Ext.menu.Menu', {
            bodyStyle: 'background-color:#f1f1f1 !important;',
            style: {
                border: 'solid 3px #4499EE',
                borderRadius: '1px',
                height: '350px'
            },
        });*/

        me.items.push({
            xtype: 'button',
            cls: 'notifyIcon',
            iconAlign: 'bottom',
            listeners: {
                dbclick: function() {
                    var Desc;
                    var indx;
                    var rFlag;
                    var bgClr;
                    myNotiStore = [];
                    var myNotiStore = Ext.getStore('Notifications').load({params:{'limitoten':'limitoten'},
                        callback: function(records, options, success) {
                            AccMgtSubmenu.removeAll(true);
                            if (records.length != 0) {
                                myNotiStore.each(function(record, i) {
                                    indx = me.nIds.indexOf(myNotiStore.data.items[i].data.NotificationID);
                                    rFlag = parseInt(myNotiStore.data.items[i].data.ReadFlag);
                                    if (rFlag == 0) {
                                        var bg = "#e5e5e5";
                                        var fw = 'bold';
                                    }
                                    if (myNotiStore.data.items[i].data.Type == 4) {

                                        Desc = myNotiStore.data.items[i].data.Title;
                                    } else {
                                        Desc = myNotiStore.data.items[i].data.Description;
                                    }

                                    if (indx == -1) {
                                        AccMgtSubmenu.add({
                                            text: Desc,
                                            style: {
                                                width: '350px',
                                                padding: '6px',
                                                height: '30px',
                                                border: '#A2BFD5',
                                                'background-color': bg,
                                                'font-weight': fw
                                            },

                                            handler: function(obj) {
                                                var res  = myNotiStore.data.items[i].data.Title;
                                                var nid  = myNotiStore.data.items[i].data.NotificationID;
                                                var type = myNotiStore.data.items[i].data.Type;
                                                AM.app.getController("Notification").viewNotificationType(res, nid, type);
                                                AM.app.getController("Notification").updateNotification(nid);
                                            },
                                        });
                                    }
                                    AccMgtSubmenu.add(AccMgtSubmenu.items.items);
                                });
                                    AccMgtSubmenu.add({
                                        xtype: 'button',
                                        align: 'right',
                                        text: 'See All',
                                        id: 'notify_view',
                                        handler: function() {
                                            AM.app.getController("AM.controller.Notification")
                                                .viewNotifications();
                                        }
                                    });
                            }
                        }
                    });
                    this.showMenu();
                },
            },
            style: {
                'width': '32px',
                'height': '32px',
                'background': '#276193',
                'background-repeat': 'no-repeat',
                'border': 'none'
            },
            menu: AccMgtSubmenu,
        });
        this.callParent(arguments);
    },


});