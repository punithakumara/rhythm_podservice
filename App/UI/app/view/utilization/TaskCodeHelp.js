Ext.define('AM.view.utilization.TaskCodeHelp',{
	extend : 'Ext.window.Window',
	alias : 'widget.TaskCodeHelp',
	layout: 'fit',
	//width : '99%',
	layout: {
        type: 'vbox',
    },
	store: 'taskCode',
	width : '80%',
	height: '84%',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	title: 'View Resources',
	border : 'true',
	remoteFilter:true,
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '5px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
    	this.items = [{
	        xtype : 'trigger',
	        itemId : 'gridTrigger',
	        id : 'gridTrigger',
	        fieldLabel : '',
	        labelWidth : 60,
	        labelCls : 'searchLabel',
	        triggerCls : 'x-form-clear-trigger',
	        emptyText : 'Search Task Code, Task Name, Request Type, Complexity, Priority',
	        width : 450,
	        minChars : 1,
	        enableKeyEvents : true,
           // style:{'float':'right'},
	        onTriggerClick : function(){
	            this.reset();
	            this.fireEvent('triggerClear');
	        }
	    },{
			xtype:'gridpanel',
			border:true,
			margin:'0',
			height:450,
			width:'100%',
			id:'task_code_grid',			
			columns: [{
			header: 'Task Code', 
			dataIndex: 'task_code', 
			flex: 1.5,
			sortable: false,
            menuDisabled:true,
            draggable: false	
		},
		/*{
			header: 'Task Category',  
			dataIndex: 'task_category',  
			flex: 2.5
		},*/ {
			header: 'Task Name',  
			dataIndex: 'task_name',  
			flex: 1.5,
			sortable: false,
            menuDisabled:true,
            draggable: false	
		}, {
			header: 'Request Type', 
			dataIndex: 'request_type', 
			flex: 1,
			sortable: false,
            menuDisabled:true,
            draggable: false	
		}, {
			header : 'Complexity', 
			dataIndex : 'complexity', 
			flex: 1,
			sortable: false,
            menuDisabled:true,
            draggable: false	
		}, {
			header: 'Priority', 
			dataIndex: 'priority', 
			flex: 1.3,
			sortable: false,
            menuDisabled:true,
            draggable: false	
		}],
			bbar: Ext.create('Ext.PagingToolbar', {
				displayInfo: true,				
				id:'task_code_grid_bb',
				pageSize: AM.app.globals.itemsPerPage,
				displayMsg: 'Displaying {0} - {1} of {2}',
				emptyMsg: "No Data To Display",
				store   : 'taskCode',
				 plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
			}),
			
		}];
		
    	
		this.callParent(arguments);
	},		
	
});