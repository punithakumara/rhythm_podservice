Ext.define('AM.view.utilization.UtilizationGrid',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.UtilizationGrid',
    store : 'Utilization',
    id : 'UtilizationGridID',
    layout : 'fit',
	title: 'Utilization',
	border:true,
	selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
    width : '99.9%',
    // minHeight : '280',
	// autoHeight: true,
	// autoScroll: true,
    
    initComponent: function(){
        /**
        * set the config we want
        */
		var me = this;
 
		this.viewConfig = { 
			deferEmptyText: false,
			listeners: {
				refresh: function(view) {
					Ext.select('.delUtil').on('click', function(){		
						var RowIndex = this.parentNode.parentNode.parentNode.getAttribute("data-recordindex");
						var rec = view.store.getAt(RowIndex);

						if(rec.data.utilization_id != undefined)
						{
							AM.app.getController('Utilization').deleteUtilization(rec,Ext.getCmp('utilization_client').getValue(), me);
						}
						// THe else condition added sinc eht Utilization Id is missisng while Adding Team Member utilization sometimes..
						else
						{
							AM.app.getController('Utilization').utilzationFun(me,me.query('combobox')[0].getValue(),me.query('combobox')[2].getValue(),AM.app.globals.empid,false);
						}
					});
					if(this.store.getProxy().reader.jsonData != undefined)
						Ext.getCmp('TotalHours').setValue(this.store.getProxy().reader.jsonData.TotalHours);
					//	Ext.getCmp('Totalmins').setValue(this.store.getProxy().reader.jsonData.Totalmins);
						Ext.getCmp('TotalRecords').setValue("Total Records = "+this.store.data.length);
				}
			}
		};

		ProcessClientId ="";
		
		var addUtil = true;	
		var clientRender =false;
		
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{clicksToMoveEditor: 1,pluginId: 'rowEditing',autoCancel: false,saveBtnText: 'Save',draggable:true,listeners:{
			'beforeedit' :  function(editor, e) {
				
				/* This code added to Restrict the User to add the Records befor 15 days */ 
				var dat = me.query('datefield')[0].rawValue;
				var date1 = new Date;
				var date2 = new Date(dat);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				if(diffDays > 8)
				{	
					Ext.MessageBox.alert('Alert', 'Record Can\'t be Edited ');
					return false;
				}
			
				/* This code added to Restrict the User to add the Records befor 15 days CODE ENDS*/
				var record = me.getStore().getAt(e.rowIdx);
				
				// console.log(record.data.total_hrs);

				if (e.colIdx === editor.grid.columnManager.columns.length - 1 && e.column.header == undefined)
				{					     
					return false;
				}
				AM.app.globals.utilization = [];
				var i = 0;
				editor.editor.form.getFields().items.forEach(function(entry) 
				{
					entry.width = editor.grid.headerCt.columnManager.columns[i].getWidth() - 3;
					i++;
					entry.removeCls('x-form-my-field');
					if(entry.tdCls != undefined && entry.tdCls == "externaltime")
					{	
						if(!Ext.Array.contains(AM.app.globals.utilization,entry.name))
							AM.app.globals.utilization.push(entry.name);
					}
				});
			},	
			'validateedit':function(editor,e)
			{	
				
				var d = new Date();
				var todaysDate = Ext.Date.format(d, 'm/d/Y');	
				var comboDate = Ext.Date.format(me.query('datefield')[0].getValue(), 'm/d/Y');
				var date1 = new Date(todaysDate);
				var date2 = new Date(comboDate);
				var timeDiff = Math.abs(date2.getTime() - date1.getTime());
				var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
				var Validity = true;
				
				/*For Copy the previous row to current row comparison */
				var previousval	= e.record.copy(); //copy the old model
				var currentrow	= e.newValues;
				
				delete currentrow.total_hrs;
				if(currentrow.admin_hrs != ''){
					currentrow.admin_hrs = 	Ext.Date.format(currentrow.admin_hrs, 'H:i');
				}	
				if(currentrow.admin_hrs == ''){
					delete currentrow.admin_hrs;
				}
				if(currentrow.billable_hrs != ''){
					currentrow.billable_hrs = 	Ext.Date.format(currentrow.billable_hrs, 'H:i');
				}
				if(currentrow.billable_hrs == ''){
					delete currentrow.billable_hrs;
				}
				if(currentrow.non_billable_hrs != ''){
					currentrow.non_billable_hrs = 	Ext.Date.format(currentrow.non_billable_hrs, 'H:i');
				}
				if(currentrow.non_billable_hrs == ''){
					delete currentrow.non_billable_hrs;
				}
				/*if(currentrow.total_hrs!= ''){
					currentrow.total_hrs = 	Ext.Date.format(currentrow.total_hrs, 'H:i');
				}
				if(currentrow.total_hrs == ''){
					delete currentrow.total_hrs;
				}*/
				if(currentrow.request_completion_date != ''){
					currentrow.request_completion_date = 	Ext.Date.format(currentrow.request_completion_date, 'd-M-Y');
				}
				if(currentrow.request_completion_date == ''){
					delete currentrow.request_completion_date;
				}
				if(currentrow.request_date != ''){
					currentrow.request_date = 	Ext.Date.format(currentrow.request_date, 'd-M-Y');
				}
				if(currentrow.request_date == ''){
					delete currentrow.request_date;
				}
				if(currentrow.request_start_date != ''){
					currentrow.request_start_date = 	Ext.Date.format(currentrow.request_start_date, 'd-M-Y');
				}
				if(currentrow.request_start_date == ''){
					delete currentrow.request_start_date;
				}
				if(currentrow.campaign_start_date != ''){
					currentrow.campaign_start_date = 	Ext.Date.format(currentrow.campaign_start_date, 'd-M-Y');
				}
				if(currentrow.campaign_start_date == ''){
					delete currentrow.campaign_start_date;
				}
				
				/*For Checking Utilization for to be entered with in 24hrs /////////*			
				var row_mins = currentrow.hours * 60;
				var total_mins = currentrow.min + row_mins;
				var time_consumed = Ext.getCmp('Totalmins').getValue();
				var total_time = +total_mins + +time_consumed;
				
				if(total_time > 1440){
					Ext.MessageBox.alert('Alert', 'Utilization can not exceed 24Hrs for a Day');
					return false;
				}
				
				/*Code Ends For Checking Utilization for to be entered with in 24hrs */
				
				var count = 0;
				for(var c in currentrow){
					
					currentrow[c] = (typeof currentrow[c] == "number")?currentrow[c].toString():currentrow[c];
				}
			
				if(currentrow){				
					var key = ''+c+'';
					delete currentrow[key];
				}
				
				if(previousval)
				{
				
					if(previousval.data['utilization_id']=='utilization_id')
						var key = "utilization_id";
						var key2 = "FullName";
						var key3 = "TaskDate"; 
						var key4 = "theorem_hrs"; 
					 
						delete previousval.data[key];
						delete previousval.data[key2];
						delete previousval.data[key3];
						delete previousval.data[key4];
						
						 
					var a1=currentrow ;
					var a2= previousval.data ;
					
					var i="";
					for(var k in a1){
						a1[k] = (a1[k] === null)? ''+i+'' :a1[k];
					}
					
					if(Ext.Object.equals(a1, a2)){console.log('DUPLICATE');
						Ext.MessageBox.alert('Alert', 'Duplicate Entry');
						return false
					}
				 
				}
				
				/* end of copy row */				
				// checking the past 15days
				if(diffDays <=15)
				{
					var hoursRecord =""; 
					var hoursEntry =""; 
					var minEntry =""; 
					var minutesRecord =""; 
					var hoursMin = false; 
					var externalTime = false; 
					var externalTimeH = ""; 
					var record = me.getStore().getAt(e.rowIdx);
					
						var tot_hrs = '';
						var tot_mins = '';
					
					editor.editor.form.getFields().items.forEach(function(entry) {
						
					
						if(entry.name =="hours"){
							hoursRecord = entry.value;
							hoursEntry = entry;
						}
						
						if(entry.name =="admin_hrs"){
							
							tot_hrs = Math.floor(Ext.Date.format(entry.value, 'H'));
							tot_mins = Math.floor(Ext.Date.format(entry.value, 'i'));
							
							
							
						}if(entry.name =="billable_hrs"){
								
							tot_hrs += Math.floor(Ext.Date.format(entry.value, 'H'));
							tot_mins += Math.floor(Ext.Date.format(entry.value, 'i'));
							
						}
						if(entry.name =="non_billable_hrs"){
								
							tot_hrs += Math.floor(Ext.Date.format(entry.value, 'H'));
							tot_mins += Math.floor(Ext.Date.format(entry.value, 'i'));
							
						}
						if(entry.name =="theorem_hrs"){
							
							if(tot_hrs != '' || tot_mins !='' ){
								tottime = (tot_hrs * 60) + tot_mins;
								total_hours = Math.floor(tottime/60);
								total_mins = tottime%60;
							
								var totaltime    = total_hours+':'+total_mins;
								// entry.setRawValue(totaltime);
								entry.setValue(totaltime);
							}else{
								 var totaltime = entry.getRawValue();
								 entry.setRawValue(totaltime);
								 // entry.setValue('');
							}
						}
						if(entry.name =="min"){
							minutesRecord = entry.value;
							minEntry = entry;
							hoursMin = true;
						}

						if((AM.app.globals.utilization.length == 0)&&((hoursRecord == null || (hoursRecord == "" && minutesRecord == "") || minutesRecord == null) && hoursMin)){
							hoursEntry.addCls('x-form-my-field');
							minEntry.addCls('x-form-my-field');
							Validity = false;
						}
						
						if(entry.allowBlank == "false" && (entry.value == "" || entry.value == null)){
							entry.addCls('x-form-my-field');
							Validity = false;
						}
						
						editor.editor.form.findField(entry.name).enable();
					});
					if(Validity)
						return true;
					else			
						return false;
				}
				else
				{
					editor.editor.form.getFields().items.forEach(function(entry) {
						editor.editor.form.findField(entry.name).disable();
					});
					return false;
				}
			},
			cancelEdit: function(grid,obj){
				AM.app.getController('Utilization').viewContent(me.query('combobox')[0], me.query('datefield')[0]);
	        }
		}
		});

		this.editing.on('afteredit', function(editor, e, eOpts) {
			var record = me.getStore().getAt(e.rowIdx);
			
			var externalHours = 0; 
			var externalMinutes = 0; 
			var externalTime = 0; 
			var extHour = ""; 
			var extMinute = ""; 
			
			if(AM.app.globals.utilization.length > 0)
			{
				Ext.Array.each(AM.app.globals.utilization, function(rec) {
					externalHours += Math.floor(Ext.Date.format(record.get(rec), 'H'));
					externalMinutes += Math.floor(Ext.Date.format(record.get(rec), 'i'));
				});
			
				externalTime = (externalHours * 60) + externalMinutes;
				
				extHour = Math.floor(externalTime/60);
				extMinute = externalTime%60;
			
				record.set("hours", extHour);
				record.set("min",extMinute);
			}
			
			
			
			if(me.query('combobox')[0] != undefined && me.query('combobox')[0].getValue() != null) {
				WeekendSupport = 0;
				ClientHoliday = 0;
				
				AM.app.getController('Utilization').saveUtilization(me,e,me.query('combobox')[0].getValue(),me.query('datefield')[0].getValue(),AM.app.globals.empid,extHour,extMinute, WeekendSupport, ClientHoliday);
			}
		
		});
		
		Ext.apply(this, {
			plugins: [this.editing]
		});

		this.tbar = [{
            text: 'Add My Utilization',
            icon: AM.app.globals.uiPath+'resources/images/add.png',
			id:'AddUtil',
			disabled: addUtil,
			disabledCls: "disabledComboTestCls",
            handler : function() {
            	if(Ext.util.Cookies.get('employee_id')!=AM.app.globals.utilEmployIDs){
            		Ext.getCmp('UtilizationGridID').store.reload({ params :{
							'client_id':Ext.getCmp('utilization_client').getValue(),
							'utilization_date':Ext.getCmp('utilization_date').getValue(),
							'employee_id':Ext.util.Cookies.get('employee_id'),
            		 	}
            		});
            	}
            	AM.app.globals.utilEmployIDs = Ext.util.Cookies.get('employee_id');
            	Ext.getCmp('utilization_employee').setVisible(false);
            	var r2 = Ext.create('AM.store.Utilization');
				if(me.getStore().first()!=undefined){
					var UD= me.getStore().first();
					var key = "utilization_id";
					delete UD.data[key];
					me.getStore().insert(0, me.getStore().first());
				}
				else
				{
					me.getStore().insert(0,r2);
				}
			    me.editing.cancelEdit();
                me.editing.startEdit(0, 0);
				
				me.editing.addnew = true;
            }
        },
        // Made it Hidden as per the Requiremnets the functionalitiles will work if Uncommented
		{
            xtype:'button',
            isFormField: true,
            id: "DownloadLink",
            disabledCls: "disabledComboTestCls",
            anchor: '5%',
            text:'Export To Excel',
            icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			style:{
				float:'left',
				'margin-left':'10px'
			},
			//action : 'downloadTemplate',
			handler : function() {
			if(me.store.getCount() == 0)
				Ext.Msg.alert('Alert', 'No Data!!!');
			else
				AM.app.getController('Utilization').onDownload(me.query('combobox')[0].getValue());
			
			}
		}, {
            text: 'Add Team Member Utilization',
            icon: AM.app.globals.uiPath+'resources/images/add.png',
			id:'AddTeamMemberUtil',
			disabledCls: "disabledComboTestCls",
            handler : function() {
				var empStore = Ext.getStore('LogInViewerEmployees');
				empStore.load({ 
					params :{
						'client_id':Ext.getCmp('utilization_client').getValue(),
						'date':Ext.getCmp('utilization_date').getValue(),								  
					},
				
					callback: function(records, operation, success) { 
						if(empStore.data.items[0] != undefined){
							Ext.get('utilization_employee').setStyle('display', '');
							if(Ext.util.Cookies.get('employee_id')!=AM.app.globals.utilEmployIDs)
							{
								Ext.getCmp('utilization_employee').setValue(AM.app.globals.utilEmployIDs);
							}
							else
							{
								Ext.getCmp('utilization_employee').setValue(empStore.data.items[0].data.employee_id);
							}
							Ext.getCmp('utilization_employee').setValue(AM.app.globals.utilEmployIDs);
							
							var r = Ext.create('AM.store.Utilization');
							
							if(me.getStore().first()!=undefined){
								var UD= me.getStore().first();
								var key = "utilization_id";
								delete UD.data[key];
								me.getStore().insert(0, me.getStore().first());
							}
							else
							{
								me.getStore().insert(0,r);
							}
							me.editing.cancelEdit();
							me.editing.startEdit(0, 0);
							me.editing.addnew = true;
						}
						else
						{
							Ext.MessageBox.alert('Alert', 'No Billable Employees to Add Utilization.');
							Ext.getCmp('utilization_employee').setVisible(false);
						}
					}
				});
            }
        }, {
			xtype : 'combobox',
			multiSelect: false,
			id: 'utilization_employee',
			name: 'utilization_employee',
			minChars: 0,
			store: 'LogInViewerEmployees',
			queryMode: 'local',
			displayField: 'first_name',
			valueField: 'employee_id',							
			width : 300,
			forceSelection:true,
			labelWidth : 50,
			allowBlank: false,
			maskRe: /[A-Za-z ]/,
			labelCls : 'searchLabel',
			style: {
				'margin-right' : '5px',
				'display' : 'none'
			},
			fieldLabel: 'Client',
			anchor: '100%',
			listeners: {
				beforequery: function(queryEvent, eOpts) {
					queryEvent.combo.store.proxy.extraParams = {
						'client_id':Ext.getCmp('utilization_client').getValue(),
						'date':Ext.getCmp('utilization_date').getValue(),								  
					}
				},
				change: function(combo) {
					AM.app.globals.utilEmployIDs = combo.getValue();
				  
					/*Tis Condition added to check if the Employe combo values avalues availbale based on that the Add my Utilization button is enables  & disabled else the Empty values of Employee_id inserted*/
					var emp=  AM.app.globals.utilEmployIDs;
					if(emp == '' || emp == null  ){
						Ext.getCmp('AddTeamMemberUtil').setDisabled(true);
					}
					else
					{
						Ext.getCmp('AddTeamMemberUtil').setDisabled(false);
					}
				  
					Ext.getCmp('UtilizationGridID').store.reload({ 
						params :{
							'client_id':Ext.getCmp('utilization_client').getValue(),
							'utilization_date':Ext.getCmp('utilization_date').getValue(),
							'employee_id':combo.getValue(),
						},
						
						callback: function(records, operation, success){
							var r = Ext.create('AM.store.Utilization');
							if(me.getStore().first()!=undefined){
								var UD= me.getStore().first();
								var key = "utilization_id";
							}
						},
					});  
				} 
			} 
		}];

		
		var config = {
            columns:[],
            rowNumberer: false
        };

        // apply to this config
        Ext.apply(this, config);

        // apply to the initialConfig
        Ext.apply(this.initialConfig, config);

		this.tools = [{
			xtype : 'combobox',
			multiSelect: false,
			id: 'utilization_client',
			name: 'utilization_client',
			minChars: 0,
			store: Ext.getStore('LogInViewerClients'),
			queryMode: 'remote',
			displayField: 'client_name',
			valueField: 'client_id',
			width : 300,
			labelWidth : 50,
			allowBlank: false,
			labelCls : 'searchLabel',
			style: {
				'margin-right' : '5px'
			},
			fieldLabel: 'Client',
			emptyText:"Select Client",
			anchor: '100%',
			listeners: {
				beforequery: function(queryEvent, eOpts) {
					queryEvent.combo.store.proxy.extraParams = {
						'hasNoLimit':'1',
						'comboClient':'1',
						'isUtilzation':'1',
						'filterName' : queryEvent.combo.displayField
					}								
				},
				'select': function(combo) 
				{
					me.client_id = combo.getValue();
					
					Ext.getCmp('utilization_employee').setVisible(false);
					if(combo.getValue() == null && me.query('combobox')[0] != undefined)
					{
						me.query('combobox')[0].clearValue();
					}
					else if(me.query('combobox')[0] != undefined)
					{	
						me.query('datefield')[0].enable();
						
						// AM.app.globals.utilEmployIDs =  Ext.util.Cookies.get('employee_id');
						
						if(AM.app.globals.utilEmployIDs!='')
						{
							var empid=AM.app.globals.utilEmployIDs
						}
						
						
						AM.app.getController('Utilization').utilzationFun(me,me.query('combobox')[0].getValue(),me.query('datefield')[0].getValue(),AM.app.globals.empid,true);
					}
				},
			}
		}, {
			xtype: 'datefield',
			format: AM.app.globals.CommonDateControl,
			name : 'utilization_date',
			maxValue: new Date(),
			anchor: '100%',
			id: 'utilization_date',
			displayField: 'abbr',
			valueField: 'Date',
			fieldLabel: 'Date',
			disabled:true,
			labelWidth : 50,
			labelCls : 'searchLabel',
			width : 200,
			style: {
				'margin-right' : '5px'
			},
			value:new Date(),
			listeners: {
				select : function(combo) {
					if(me.query('combobox')[0].getValue())
					{
						me.utilDate = combo.getValue();
						
						AM.app.getController('Utilization').utilzationFun(me,me.query('combobox')[0].getValue(),me.query('datefield')[0].getValue(),AM.app.globals.empid,false);
					}
				}
			}
		}, { 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    }, {
			xtype: 'button',
			icon: AM.app.globals.uiPath+'/resources/images/checklist.png',
			style: {
				'margin-right' : '5px'
			},
			tooltip: "Project Code Help",
			handler: function(){
				
				var clientId = Ext.getCmp('utilization_client').getValue()
				if( clientId != null)
				{
					AM.app.getController('Utilization').roleCodeHelp(clientId);
				}
				else
				{
					Ext.MessageBox.show({
							title: 'Warning',
							msg: 'Please Select Client',
							icon: Ext.MessageBox.Warning,
							buttons: Ext.Msg.OK,
					});
					
					return false;
				}
			}
		},
		{
			xtype: 'button',
			icon: AM.app.globals.uiPath+'/resources/images/clipboard.png',
			style: {
				'margin-right' : '5px'
			},
			tooltip: "Task Code Help",
			handler: function(){
				
				var clientId = Ext.getCmp('utilization_client').getValue()
				
				if( clientId != null)
				{
					AM.app.getController('Utilization').taskCodeHelp();
				}
				else
				{
					Ext.MessageBox.show({
							title: 'Warning',
							msg: 'Please Select Client',
							icon: Ext.MessageBox.Warning,
							buttons: Ext.Msg.OK,
					});
					
					return false;
				}
			}
		}],	 

	    this.bbar    = [{
			xtype: 'displayfield',
			id: "TotalRecords",
			value:'',
			style : {
				'margin-left' : '10px'
			}
		}, {
			xtype: 'displayfield',
			id: "TotalHours",
			value:'',
			style : {
				'margin-left' : '800px'
			}
		}, {
			xtype    : 'hidden',
			id: "Totalmins",
			value:'',
		}]
		 
        this.callParent(arguments);
    },
 
    /**
    * When the store is loading then reconfigure the column model of the grid
    */
    storeLoad: function(store,me)
    {
        /**
        * JSON data returned from server has the column definitions
        */
		if(store.proxy.reader.jsonData != undefined){
			if(typeof(store.proxy.reader.jsonData.columns) === 'object') {
				var columns = [];
				/**
				* assign new columns from the json data columns
				*/
				Ext.each(store.proxy.reader.jsonData.columns, function(column){
					if(column.dataIndex != 'TaskDate')
						columns.push(column);
				});
				/**
				* reconfigure the column model of the grid
				*/		

// console.log(columns)				
				me.reconfigure(store, columns);
				
				if(store.proxy.reader.jsonData.total >= 7){
				Ext.getCmp('UtilizationGridID').setHeight(500);
				}else{
					Ext.getCmp('UtilizationGridID').setHeight(300);
				}
			}
		}
    },
	
	listeners: {
		afterrender: function(obj)
		{
			var dickItems = obj.getDockedItems();
			var ClientCombo = dickItems[0].items.items[1];
			// var ProcessCombo = dickItems[0].items.items[2];
			var dteObj = dickItems[0].items.items[2];
			ClientCombo.store.load({
				callback: function(records)
				{
					if(records.length > 0)
					{
						ClientCombo.setValue(obj.client_id ? obj.client_id : records[0].get('client_id'));
						
						var parr_client_id= obj.client_id ? obj.client_id : ClientCombo.getValue();
						
						
						if(AM.app.globals.utilEmployIDs!=''){
							var empidnew=AM.app.globals.utilEmployIDs
						}
					
						dteObj.setValue(obj.dateObj ? obj.dateObj : dteObj.getValue());
						
						dteObj.enable();
						dickItems[1].items.items[0].enable();
						dickItems[1].items.items[1].enable();
						obj.view.store.load({
						 params:{ 'client_id': parr_client_id ,'utilization_date':dteObj.getValue(),'employee_id':empidnew},
							callback: function(){
								obj.storeLoad(obj.view.store, obj);
							}
						});
						
					}
				}
			});
		}
	}
});