Ext.define('AM.view.utilization.Add',{
	extend : 'Ext.window.Window',
	alias : 'widget.utilizationAdd',
	id: 'utilization_add_edit',
	layout: 'fit',
	title: 'Add Utilization',
	border : 'true',
	width : 635,
	height: 405,
	autoShow : true,
	modal: true,
	autoScroll : true,
	resizable:false,
	
	initComponent : function() {
		
		var date = new Date(), 
		startMinDate = Ext.Date.add(date, Ext.Date.MONTH, -2); 
		today = Ext.Date.add(date);
		
		var attrStore = new Ext.data.SimpleStore({
			fields: ['attr_id', 'attr_name'],
			data: [
			['1', 'Static Page'],
			['2', 'Template'],
			]
		});

		this.items = [{
			xtype : 'form',
			layout: 'vbox',
			id: 'utilizationForm',
			fieldDefaults: {
				labelAlign: 'left',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px 10px 6px 10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				title: "Standard Fields",
				margin: '0 10 0 0',
				padding: '8',
				fieldDefaults: {
					labelWidth: 180,
					labelAlign: 'top'
				},
				width: 600,
				items:[{
					xtype : 'hidden',
					id:'utilization_id',
					name : 'utilization_id',
				},{
					xtype : 'hidden',
					id:'client_id',
					name : 'client_id',
				},{
					xtype : 'hidden',
					id:'wor_type_id',
					name : 'wor_type_id',
				},{
					xtype : 'hidden',
					id:'wor_id',
					name : 'wor_id',
				},{
					layout: 'hbox',
					items:[{
						xtype : 'datefield',
						format : AM.app.globals.CommonDateControl,
						fieldLabel : 'Task Date <span style="color:red">*</span>',
						name : 'task_date',
						maskRe: /[0-9\/]/,
						id : 'task_date',
						allowBlank : false,
						width: 280,
					}, {
						xtype:'textfield',
						fieldLabel: 'Request Name/ID <span style="color:red">*</span>',
						name: 'request_name',
						id: 'request_name',
						maskRe: /[a-zA-Z0-9 -]/,
						allowBlank: false,
						width: 280,
						margin:'0 0 0 17'
					}]
				},{
					layout: 'hbox',
					margin: '10 0 0 0',
					items:[{
						xtype : 'combobox',
						multiSelect: false,
						allowBlank: false,
						fieldLabel:'Project<span style="color:red">*</span>',
						name: 'project_type_id',
						id: 'project_type_id',
						store: Ext.getStore('ProjectTypes'),
						forceSelection: true,
						queryMode: 'remote',
						minChars:0,
						displayField: 'project_type',
						valueField: 'project_type_id',
						submitValue : true,
						value: '',
						width: 280,
						listeners:{
							beforequery: function(queryEvent){
								Ext.Ajax.abortAll();
								queryEvent.combo.getStore().getProxy().extraParams = {'wor_id':Ext.getCmp('wor_id').getValue()};
							}
						}
					},{
						xtype : 'combobox',
						multiSelect: false,
						allowBlank: false,
						fieldLabel:'Role Code <span style="color:red">*</span>',
						name: 'project_code',
						id: 'project_code',
						store: Ext.getStore('roleCode'),
						queryMode: 'remote',
						minChars:0,
						displayField: 'project_code',
						forceSelection: true,
						valueField: 'si_no',
						value: '',
						width: 280,
						margin:'0 0 0 17',
						listeners		: {
							beforequery	: function(queryEvent) {
								AM.app.getStore('roleCode').getProxy().extraParams = {
									'clientId' 	:Ext.getCmp('utilization_client').getValue(),
									'wor_id' 	:Ext.getCmp('utilization_wor_id').getValue(),
									'project_type_id' : Ext.getCmp('project_type_id').getValue(),
									'hasNoLimit':'1', 
								};
							},
							select: function(obj) {
								var addtionalItem = Ext.getCmp('additionalElements');
								for(var i = 0; i < addtionalItem.items.length; i++)
								{
									addtionalItem.removeAll(addtionalItem.items.items[i], true);
								}
								var formObj = [];

								var arttribstore = records = "";
								var countElem = rec_length = 0;
								
								Ext.Ajax.request({
									url: AM.app.globals.appPath+'index.php/utilization/ClientAttributes/',
									method: 'GET',
									params: {'client_id':Ext.getCmp('utilization_client').getValue(),'wor_id':Ext.getCmp('utilization_wor_id').getValue(),'role_id':Ext.getCmp('project_code').getValue()},
									success: function(responce, opts){
										var jsonResp = Ext.decode(responce.responseText);
										records = jsonResp.data;
										 console.log(records);
										rec_length = records.length;
										
										for(var i = 0;  i < rec_length; i++) 
										{									
											countElem++;
											var formElem = new Array();
											
											formElem['xtype'] = records[i].field_type;
											if(records[i].mandatory==1)
											{
												formElem['allowBlank'] = false;
												formElem['fieldLabel'] =  records[i].label_name+' <span style="color:red">*</span>';
											}
											else
											{
												formElem['fieldLabel'] =  records[i].label_name;
											}
											formElem['name'] =  'addtional_'+records[i].attributes_id;
											formElem['width'] = 280;
											if(records[i].field_name == "dollar_value")
											{
												formElem['maskRe'] = /[0-9.]/;
												formElem['regex'] = /^\d+(\.\d{1,2})?$/;
											}
											console.log(formElem["fieldLabel"]);
											//Completed Date/Submitted On
											//Received Date
											if(records[i].field_type == "datefield")
											{
													
													formElem['format'] = AM.app.globals.CommonDateControl;      
													formElem['minValue'] = startMinDate;    
											}
											if(records[i].field_type == "numberfield")
											{
												formElem['minValue'] = 0;      
												formElem['maxValue'] = 999999999; 
											}
											if(records[i].field_type == "timefield")
											{
												formElem['format'] = 'H:i';
												formElem['maxValue'] = '12:00'; 
											}
											if(records[i].field_name == "week")
											{
												var locStore = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "W1"},
													{"name": "W2"},
													{"name": "W3"},
													{"name": "W4"},
													{"name": "W5"},
													]
												});
												formElem['store'] = locStore;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";
												formElem['value'] = "";
												formElem['multiSelect'] = false;        
											}
											if(records[i].field_name == "build_project_complexity")
											{ 
												var locStore2 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Standard Static"},
													{"name": "Moderate Static"},
													{"name": "Standard Responsive"},
													{"name": "Moderate Responsive"},
													{"name": "Standard Fluid"},
													{"name": "Moderate Fluid"},
													{"name": "Complex Static"},
													{"name": "Complex Fluid"},
													{"name": "Complex Responsive"},
													]
												});
												formElem['store'] = locStore2;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name"; 
												formElem['value'] = "";      
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "managed_by")
											{
												var locStore3 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Theorem"},
													{"name": "Mogo"},
													]
												});
												formElem['store'] = locStore3;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name"; 
												formElem['value'] = "";      
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "channel")
											{
												var locStore4 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Display"},
													{"name": "AdsManager"},
													{"name": "Search"},
													{"name": "Social"},
													{"name": "Email"},
													{"name": "Programmatic"},
													]
												});
												formElem['store'] = locStore4;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";  
												formElem['value'] = "";     
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "category")
											{
												var locStore5 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Report"},
													{"name": "Dashboard"},
													{"name": "Implementation"},
													{"name": "Screenshot"},
													{"name": "Trafficking"},
													{"name": "Site QA"},
													{"name": "Content Monitoring"},
													{"name": "Email Deployment"},
													]
												});
												formElem['store'] = locStore5;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";  
												formElem['value'] = "";     
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "patch_news_corp")
											{
												var locStore6 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "NSW 1"},
													{"name": "NSW 2"},
													{"name": "NSW 3"},
													{"name": "NSW 4"},
													//{"name": "NSW 5"},
													{"name": "NSW 6"},
													{"name": "NSW 7"},
													{"name": "NSW 8"},
													{"name": "SMB"},
													{"name": "VIC 1"},
													{"name": "VIC 2"},
													{"name": "VIC 3"},
													{"name": "VIC 4"},
													{"name": "NAT QLD"},
													{"name": "State QLD"},
													{"name": "SA"},
													{"name": "WA"},
													//{"name": "VIC"},
													{"name": "Rich Media"},
													]
												});
												formElem['store'] = locStore6;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";  
												formElem['value'] = "";     
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "request_region")
											{
												var locStore7 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
														{"name": "APAC"},
														{"name": "EMEA"},
														{"name": "AMER"},
														{"name": "East"},
														{"name": "West"},
														{"name": "Central"},
														{"name": "SMB"},
														{"name": "Programmatic"},
														{"name": "Media Plan"},
														{"name": "ISQTC - East"},
														{"name": "ISQTC - West"},
														{"name": "OSQTC - East"},
														{"name": "OSQTC - West"},
														{"name": "OSQTC - Central"},
													]
												});
												formElem['store'] = locStore7;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";  
												formElem['value'] = "";     
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "service_line")
											{
												var locStore8 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Reporting"},
													{"name": "AdOps"},
													{"name": "Search"},
													{"name": "Email"},
													]
												});
												formElem['store'] = locStore8;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";  
												formElem['value'] = "";     
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "time_zone")
											{
												var locStore9 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "APAC"},
													{"name": "EMEA"},
													{"name": "LATE EMEA"},
													{"name": "EST"},
													{"name": "CST"},
													{"name": "PST"},
													]
												});
												formElem['store'] = locStore9;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";  
												formElem['value'] = "";     
												formElem['multiSelect'] = false;       
											}
											if(records[i].field_name == "clarification")
											{
												var locStore10 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Yes"},
													{"name": "No"},
													]
												});
												formElem['store'] = locStore10;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";
												formElem['value'] = "";
												formElem['multiSelect'] = false;        
											}
											if(records[i].field_name == "type_of_clarification")
											{
												var locStore11 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Approvals"},
													{"name": "Asset Missing"},
													{"name": "Asset Not Working"},
													{"name": "Clarification"},
													{"name": "Incomplete Information"},
													{"name": "NA"},
													]
												});
												formElem['store'] = locStore11;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";
												formElem['value'] = "";
												formElem['multiSelect'] = false;        
											}
											if(records[i].field_name == "platforms")
											{
												var locStore21 = Ext.create('Ext.data.Store', {
													fields: ['name'],
													data : [
													{"name": "Google Ad Manager 360"},
													{"name": "Display Video 360"},
													{"name": "Optimization"},
													{"name": "Whiteglove"},
													{"name": "Midnight/Weekend Adreview"}
													]
												});
												formElem['store'] = locStore21;      
												formElem['displayField'] = "name";      
												formElem['valueField'] = "name";
												formElem['value'] = "";
												formElem['multiSelect'] = false;        
											}

											if(i%2!=0)
											{
												formElem['margin'] = '0 0 0 17';
											}
											
											formObj.push(formElem);
											
											if(formObj.length==2)
											{
												var formItem = "";
												formItem = {
													layout : 'hbox',
													margin : '0 0 10 0',
													items:formObj
												};
												formObj = [];
												addtionalItem.add({items:formItem});
											}
											else if(formObj.length==1 && rec_length == countElem)
											{
												var formItem = "";
												formItem = {
													layout : 'hbox',
													items:formObj
												};
												formObj = [];
												addtionalItem.add({items:formItem});
											}
										}

										if(Ext.getCmp('additionalElements').items.length > 0)
										{
											Ext.getCmp('additionalElements').show();
											Ext.getCmp('utilization_add_edit').setHeight(530);
										}
										else
										{
											Ext.getCmp('additionalElements').hide();
											Ext.getCmp('utilization_add_edit').setHeight(405);
										}
										Ext.getCmp('utilization_add_edit').center()
									}
								})
}
}
}]
}, {
	layout: 'hbox',
	margin: '10 0 0 0',
	items:[{
		xtype : 'combobox',
		fieldLabel : 'Task Code <span style="color:red">*</span>',
		store: Ext.getStore('taskCode'),
		multiSelect: false,
		name : 'task_code',
		id:'task_code',
		queryMode: 'remote',
		minChars:0,
		displayField: 'task_code',
		forceSelection: true,
		valueField: 'task_id',
		allowBlank: false,
		value: '',
		width: 280,
		listeners		: {
			beforequery	: function(queryEvent) {
				AM.app.getStore('taskCode').getProxy().extraParams = {
					'hasNoLimit' 	:'1',
				};
			},
			select : function(obj) {
				Ext.getStore('subTaskCode').load({
					params:{ 'task_code': obj.getValue()}
				});

				Ext.getCmp('sub_task').setDisabled(false);
				Ext.getCmp('sub_task').enable();	
				Ext.getCmp('sub_task').setValue('');

			}
		}
	},{
		xtype : 'combobox',
		multiSelect: true,
		fieldLabel : 'Sub Task',
		store: Ext.getStore('subTaskCode'),
		name : 'sub_task',
		id:'sub_task',
		queryMode: 'remote',
		displayField: 'task_name',
		valueField: 'sub_task_id',
		forceSelection: true,
		value: '',
		width: 280,
		margin:'0 0 0 17',
		listeners:{						
			beforequery: function(queryEvent){
				var task_code =   Ext.getCmp('task_code').getValue();
				queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'task_code':task_code};
			}
		}
	}]
}, {
	layout: 'hbox',
	margin: '10 0 0 0',
	items:[{
		xtype:'timefield',
		fieldLabel: 'Client Hours <span style="color:red">*</span>',
		name: 'client_hrs',
		id: 'client_hrs',
		minValue:'0:01',
		maxValue:'12:00',
		format: 'H:i',
		increment:'15',
		allowBlank: false,
		width: 280,
	}, {
		xtype:'timefield',
		fieldLabel: 'Theorem Hours <span style="color:red">*</span>',
		name: 'theorem_hrs',
		id: 'theorem_hrs',
		minValue:'0:01',
		maxValue:'12:00',
		format: 'H:i',
		increment:'15',
		allowBlank: false,
		width: 280,
		margin:'0 0 0 17'
	}]
}, {
	xtype : 'textfield',
	fieldLabel : 'Remarks',
	name: 'remarks',
	maskRe: /[a-zA-Z0-9 -]/,
	id: 'remarks',
	margin: '10 0 5 0',
}]
}, {
	xtype : 'fieldset',
	title: 'Additional Fields',
	padding:'8',
	id: 'additionalElements',
	fieldDefaults: {
		labelWidth: 180,
		labelAlign: 'top'
	},
	width: 600,
	hidden: true,
	margin: '20 0 0 0',
}]
}];

this.buttons = ['->', {
	xtype: 'displayfield',
	value : '<span style="color:red">* marked fields are mandatory</span>',
},{
	text : 'Duplicate',
	id:'cloneButtonID',
	icon : AM.app.globals.uiPath+'resources/images/save.png',
	handler : function(obj) 
	{	
		var win  = this.up('window');
		var form  = win.down('form').getForm();

		var	values = form.getValues();
		var duplicate = "1";
		if(values.sub_task != '')
		{
			var sub = values.sub_task.join();
		}
		else
		{
			var sub = "";
		}			
		if (form.isValid())
		{
			obj.setDisabled(true);
			form.submit({
				url: AM.app.globals.appPath+'index.php/utilization/utilization_create/?v='+AM.app.globals.version,
				method: 'POST',
				params:{"subtaskval" : sub,"duplicate" : duplicate},
				success: function(form,o) 
				{
					var DupData = JSON.parse(o.response.responseText);
					Ext.MessageBox.show({
						title: "Success",
						msg: DupData.msg,
								// msg: "Record Duplicated Successfully.",
								buttons: Ext.Msg.OK
							});
					if(DupData.exceed != 1)
					{
						win.close();
						AM.app.getController('Utilization').viewContent(Ext.getCmp('utilization_client').getValue(),Ext.getCmp('utilization_wor_id').getValue(),Ext.getCmp('utilization_date').getValue());
					}
					else
					{
						obj.setDisabled(false);
					}
				},
				failure: function(response) {
					Ext.MessageBox.show({
						title: "Warnings",
						msg: "Error in adding Record",
						buttons: Ext.Msg.OK
					});
					win.close();
				}
			});
		}
	}
}, {
	text : 'Cancel',
	icon : AM.app.globals.uiPath+'resources/images/cancel.png',
	handler : this.onCancel
}, {
	text : 'Save',
	id:'saveButtonID',
	icon : AM.app.globals.uiPath+'resources/images/save.png',
	handler : function(obj) {
		var win  = this.up('window');
		var form  = win.down('form').getForm();

		var	values = form.getValues();
		if(values.sub_task != '')
		{
			var sub = values.sub_task.join();
		}
		else
		{
			var sub = "";
		}
		if(values.addtional_112 > values.addtional_82) {
			Ext.MessageBox.show({
				title: "Warnings",
				msg: "Completed Date/Submitted On should not be lesser than Recieved date",
				buttons: Ext.Msg.OK
			});
			return;
		}
		console.log(values.addtional_82);
		if (form.isValid())
		{
			obj.setDisabled(true);
			obj.setText('Saving...');
			form.submit({
				url: AM.app.globals.appPath+'index.php/utilization/utilization_create/?v='+AM.app.globals.version,
				method: 'POST',
				params:{"subtaskval" : sub,"roleName":Ext.getCmp('project_code').getRawValue()},
				success: function(form,o) {
					var retrData = JSON.parse(o.response.responseText);

					Ext.MessageBox.show({
						title: "Success",
						msg: retrData.msg,
						buttons: Ext.Msg.OK,

					});
					if(retrData.exceed != 1)
					{
						win.close();
						AM.app.getController('Utilization').viewContent(Ext.getCmp('utilization_client').getValue(),Ext.getCmp('utilization_wor_id').getValue(),Ext.getCmp('utilization_date').getValue());
					}
				},
				failure: function(response) {
					Ext.MessageBox.show({
						title: "Warnings",
						msg: "Error in adding Record",
						buttons: Ext.Msg.OK
					});
					win.close();
				}
			});
		}
	}
}];

this.callParent(arguments);
},	

onCancel : function() {
	this.up('window').close();
},


});