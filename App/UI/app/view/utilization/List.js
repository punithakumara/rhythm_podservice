Ext.define('AM.view.utilization.List',{
	extend : 'Ext.grid.Panel',
	title: 'Utilization',
    id : 'utilGridId',
    layout : 'fit',
    width : '99%',
    minHeight : 542,
    loadMask: true,
    disableSelection: false,
    border : true,
	store: 'Utilization',
	autoScroll : true,
	
    initComponent : function() {
		 var me = this;
		 this.viewConfig = { 
			deferEmptyText: false,
			 listeners: {
				refresh: function(view) {
					if(this.store.getProxy().reader.jsonData != undefined)
						Ext.getCmp('TotalHours').setValue(this.store.getProxy().reader.jsonData.TotalHours);
						Ext.getCmp('OverallTime').setValue(this.store.getProxy().reader.jsonData.OverallTime);
						//Ext.getCmp('TotalRecords').setValue("Total Records = "+this.store.data.length);
				}
			} 
		};
		 
    	this.columns = [{
			header : 'Task Date',
			dataIndex : 'task_date',
			width: 150,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Task Date',
			// renderer : Ext.util.Format.dateRenderer('d-M-Y')
		}, {
			header : 'Project',
			dataIndex : 'project_type',
			width: 220,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Project Code',
		},{
			header : 'Role Code',
			dataIndex : 'process_name',
			width: 220,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Role Code',
		}, {
			header : 'Task Code', 
			dataIndex : 'task_code_value', 
			width: 220,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Task Code',
		}, {
			header : 'Sub Task', 
			dataIndex : 'Sub_task_Values', 
			width: 220,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Sub Task',
		}, {
			header : 'Request Name/ID', 
			dataIndex : 'request_name', 
			width: 220,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Request Name/ID', 
		}, {
			header : 'Client Hrs', 
			dataIndex : 'client_hrs', 
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Client Hrs', 
		},{
			header : 'Theorem Hrs', 
			dataIndex : 'theorem_hrs', 
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Theorem Hrs', 
		},{
			header : 'Status', 
			dataIndex : 'status', 
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Status', 
		}, {
			header : 'Action', 
			width : 70,
			xtype : 'actioncolumn',
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
				getClass: function(v, meta, record) {
					
					if(record.data.Flag !=4)
					{
						return 'rowVisible';
					}
					else
					{ 
						return 'x-hide-display';
					}
				}
			} ,'-', {
				icon : AM.app.globals.uiPath+'/resources/images/delete.png',
				tooltip : 'Delete',
				handler : this.onDelete,
				getClass: function(v, meta, record) {
					
					if(record.data.approved != "Approved")
					{
						return 'rowVisible';
					}
					else
					{ 
						return 'x-hide-display';
					}
				}
			}]
		}];
		
		
		this.tbar = [{
            text: 'Add My Utilization',
            icon: AM.app.globals.uiPath+'resources/images/Add.png',
			id:'AddUtil',
			disabledCls: "disabledComboTestCls",
			handler : function(){
				AM.app.getController('Utilization').onUtilAdd(Ext.getCmp('utilization_client').getValue(),Ext.getCmp('utilization_wor_id').getValue());
			}
        }, {
            xtype:'button',
            isFormField: true,
            id: "DownloadLink",
            disabledCls: "disabledComboTestCls",
            anchor: '5%',
            text:'Export To Excel',
            icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			style:{
				float:'left',
				'margin-left':'10px'
			},
			//action : 'downloadTemplate',
			handler : function() {
			if(me.store.getCount() == 0)
				Ext.Msg.alert('Alert', 'No Data!!!');
			else
				AM.app.getController('Utilization').onDownload(me.query('combobox')[0].getValue());
			
			}
		}, {
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/upload.png',
			id:'UploadExcel',
			disabledCls: "disabledComboTestCls",
			hidden: true,
			text : 'Import',
			handler : function() {
				AM.app.getController('Utilization').onUpload(me.query('combobox')[0].getValue(), me.query('combobox')[1].getValue());
			
			}
		},{
			xtype : 'combobox',
			multiSelect: false,
			id: 'utilization_employee',
			name: 'utilization_employee',
			minChars: 0,
			queryMode: 'local',
			displayField: 'first_name',
			valueField: 'employee_id',							
			width : 300,
			forceSelection:true,
			labelWidth : 50,
			allowBlank: false,
			maskRe: /[A-Za-z ]/,
			labelCls : 'searchLabel',
			style: {
				'margin-right' : '5px',
				'display' : 'none'
			},
			fieldLabel: 'Client',
			anchor: '100%', 
		}];
		
		this.tools = [{
			xtype : 'combobox',
			multiSelect: false,
			id: 'utilization_client',
			name: 'utilization_client',
			minChars: 0,
			store: Ext.getStore('LogInViewerClients'),
			queryMode: 'remote',
			displayField: 'client_name',
			forceSelection:true,
			valueField: 'client_id',
			width : 300,
			labelWidth : 50,
			allowBlank: false,
			labelCls : 'searchLabel',
			style: {
				'margin-right' : '5px'
			},
			fieldLabel: 'Client <span style="color:red">*</span>',
			emptyText:"Select Client",
			value:'',
			anchor: '100%',
			listeners: {
				beforequery: function(queryEvent, eOpts) {
					queryEvent.combo.store.proxy.extraParams = {
						'hasNoLimit':'1',
						'comboClient':'1',
						'isUtilzation':'1',
						'filterName' : queryEvent.combo.displayField
					}
				},
				select: function(combo) 
				{
					me.ClientId = combo.getValue();

					if(combo.getValue() == null && me.query('combobox')[0] != undefined)
					{
						me.query('combobox')[1].clearValue();
					}
					else if(me.query('combobox')[1] != undefined)
					{	
						me.query('combobox')[1].clearValue();
						me.query('combobox')[1].store.removeAll();
						me.query('combobox')[1].bindStore("workOrder");
						Ext.getStore('workOrder').load({
							params:{'hasNoLimit':"1", 'utilCombo':'1','comboEmployee':1,'comboClient':combo.getValue()},
							callback:function(records, options, success)
							{
								if(records.length > 0)
								{
									me.query('combobox')[1].enable();
									AM.app.getController('Utilization').viewContent(Ext.getCmp('utilization_client').getValue(),records[0].data.wor_id,Ext.getCmp('utilization_date').getValue());
								}
							}
						});
					}

					
				},
			}
		}, {
			xtype : 'combobox',
			multiSelect: false, 
			fieldLabel: 'Work Order & Name <span style="color:red">*</span>',
			name: 'utilization_wor_id',
			id:'utilization_wor_id',
			store: 'workOrder',
			queryMode: 'remote',
			minChars:0,
			displayField: 'work_order_name',
			valueField: 'wor_id',
			emptyText: 'Select Work Order',
			allowBlank: false,
			labelCls : 'searchLabel',
			width: 450,
			labelWidth : 160,
			disabled: true,
			listeners:{
				beforequery: function(queryEvent){
					Ext.Ajax.abortAll();
					queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':Ext.getCmp('utilization_client').getValue(),'filterName' : queryEvent.combo.displayField};
				},
				select : function(combo) {
					if(Ext.getCmp('utilization_date').getValue())
					{
						AM.app.getController('Utilization').viewContent(Ext.getCmp('utilization_client').getValue(),combo.getValue(),Ext.getCmp('utilization_date').getValue());
					}
				}
			}
		}, { 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    }, {
			xtype : 'combobox',
			name: 'viewBy_name',
			id:'utilization_date',
			store: Ext.create('Ext.data.Store', {
				fields: ['fid', 'fname'],
				data: [
					{'fid': '0', 'fname': 'View last 7 days records'},
					{'fid': '1', 'fname': 'View last 15 days records'},
				]
			}),
			displayField: 'fname',
			valueField: 'fid',
			width: 200,
			value: '0',
			// disabled: true,
			labelCls : 'searchLabel',
			listeners:{
				select : function(combo) {
					if(me.query('combobox')[1].getValue())
					{
						AM.app.getController('Utilization').viewContent(Ext.getCmp('utilization_client').getValue(),Ext.getCmp('utilization_wor_id').getValue(),combo.getValue());
					}
				}
			}
		}, { 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    }, {
			xtype: 'button',
			icon: AM.app.globals.uiPath+'/resources/images/checklist.png',
			style: {
				'margin-right' : '5px'
			},
			tooltip: "Role Code Help",
			handler: function(){
				
				var clientId = Ext.getCmp('utilization_client').getValue();
				var wor_id = Ext.getCmp('utilization_wor_id').getValue();
				if( clientId != null)
				{
					AM.app.getController('Utilization').roleCodeHelp(clientId,wor_id);
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Please Select Client',
						icon: Ext.MessageBox.Warning,
						buttons: Ext.Msg.OK,
					});
					
					return false;
				}
			}
		}, {
			xtype: 'button',
			icon: AM.app.globals.uiPath+'/resources/images/clipboard.png',
			style: {
				'margin-right' : '5px'
			},
			tooltip: "Task Code Help",
			handler: function(){
				
				var clientId = Ext.getCmp('utilization_client').getValue()
				
				if( clientId != null)
				{
					AM.app.getController('Utilization').taskCodeHelp();
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: 'Please Select Client',
						icon: Ext.MessageBox.Warning,
						buttons: Ext.Msg.OK,
					});
					
					return false;
				}
			}
		}],
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Records to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
		},{
			xtype: 'displayfield',
			id: "TotalRecords",
			name: "TotalRecords",
			style : {
				'margin-left' : '10px'
			}
		}, {
			xtype: 'displayfield',
			id: "OverallTime",
			name: "OverallTime",
			// value:'Total = 34:05 ',
			style : {
				'margin-left' : '106px'
			}
		},{
			xtype: 'displayfield',
			id: "TotalHours",
			name: "TotalHours",
			// value:'Total = 34:05 ',
			style : {
				'margin-left' : '100px'
			}
		}]; 

		this.callParent(arguments);
	},
	
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('Utilization').onEditUtilization(grid, rowIndex, colIndex);
	},
	
	onDelete : function(grid, rowIndex, colIndex,me) {
		AM.app.getController('Utilization').deleteUtilization(grid, rowIndex, colIndex, me);
	},

	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientWOR').deleteUtilization(grid, rowIndex, colIndex);
	}
	
});