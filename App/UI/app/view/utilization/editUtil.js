Ext.define('AM.view.utilization.editUtil',{
	extend			: 'Ext.window.Window',
	alias			: 'widget.editUtil',
	title			: 'Edit Utilization Details',
	layout			: 'fit',
	width			: '50%',
    autoShow		: true,
    modal			: true,
    height			: 340,
	initComponent	: function() {
		this.items = [ {
			xtype			: 'form',
			itemId			: 'utilDashboardForm',
			autoScroll		: true,
	    	autoHeight		: true,
	    	bodyStyle		: {
	    		border		: 'none',
	    		display 	: 'block',
	    		float		: 'left',
	    		padding 	: '10px'
	    	},
	    	fieldDefaults	: {
	    		labelAlign	: 'left',
	    		labelWidth	: '40%',
	    	    anchor		: '100%'
	    	},
	    	items 			: [ {
	    		xtype		: 'displayfield',
				name      	: 'FullName',
				fieldLabel	: 'Employee Name',
				anchor		: '100%',
				id			: 'FullName',
			}, {
				xtype		: 'displayfield',
				format		: 'd-m-Y',
				name		: 'task_date',
				maxValue	:  new Date(),
				readOnly	:  true,
				anchor		: '100%',
				id			: 'TaskDate',
				displayField: 'Task Date',
				valueField	: 'Date',
				fieldLabel	: 'Task Date',
				format		: AM.app.globals.CommonDateControl,
			}, {
				itemId		:'task_id',
				xtype		: 'textfield',
				name		: 'task_id',
				allowBlank	: false,
				fieldLabel	: 'Task/Request/Ticket ID',
				anchor		: '100%'
			}, {
				xtype		: 'textfield',
				name		: 'description',
				fieldLabel	: 'Description',
				anchor		: '100%',
				allowBlank	: true
			} ],
			buttons 		: ['->', {
				text		: 'Save',
				id			:'utilDashSave',
				visible		: true,
				hidden		: false,
        	    icon		: AM.app.globals.uiPath+'resources/images/save.png',
        	    handler 	: this.onSave
        	}, {
        		text	: 'Cancel',
        	   	icon	: AM.app.globals.uiPath+'resources/images/cancel.png',
        	   	handler : this.onCancel
        	   	} 
        	]
        } ];
        this.callParent(arguments);
    },
    defaultFocus		: 'mngComBox',
    onSave 				:  function() {
    	var form 			= this.up('form').getForm();
    	if (!(Ext.getCmp('hourUtilID').getValue() == 0 && Ext.getCmp('minUtilID').getValue() == 0)) {
    		var win			= 	this.up('window');
        	var extHour		=	"", extMinute	=	"";
        	if(AM.app.globals.extUtilTime != 0) {
				extHour 	=  Math.floor(AM.app.globals.extUtilTime/60);
				extMinute 	= (AM.app.globals.extUtilTime)%60;
			}
			AM.app.getController('UtilizationDashboard').saveUtilization(form,extHour,extMinute,AM.app.globals.editUtilData.data.utilization_id,win)
        }
    },
    onAddHoursField		:	function(me, displayHour) {
    	var formutil	= me.items.items[0],approve =	"";
    	if(AM.app.globals.editUtilData.data.approved == "Pending") {
    		approve		= 0;
    	} else if(AM.app.globals.editUtilData.data.approved == "Approved") {
    		approve		= 1;
    	}
		else
		{
			approve		= 2;	
		}
    	if(displayHour) {
    		formutil.add( {
    			xtype		: 'displayfield',
				name		: 'hours',
				id			:'hourUtilID',
				fieldLabel	: 'Hours',
				value		:  AM.app.globals.editUtilData.data.hours,
				anchor		: '100%'
			}, {
				xtype		: 'displayfield',
				name		: 'minutes',
				fieldLabel	: 'Minutes',
				id			:'minUtilID',
				value		: AM.app.globals.editUtilData.data.minutes,
				anchor		: '100%'
			}, {
				xtype		: 'boxselect',
				multiSelect	: false,
				name		: "approved",
				fieldLabel	: "Approval Status",
				queryMode	: 'local',
				displayField: 'Name',
				anchor		: '100%',
				valueField	: 'id',
				store		: Ext.create('Ext.data.Store', {
					fields	: ['id', 'Name'],
					data	: [ {	id	:	0,	Name	: 'Pending' },
								{	id	:	1,	Name	: 'Approved'}, 
								{	id	:	2,	Name	: 'Rejected'}
								]
							} ),
				value		:approve
			} );
    	} else {
    		formutil.add( {
    			xtype		: 'numberfield',
				name		: 'hours',
				fieldLabel	: 'Hours',
				id			:'hourUtilID',
				anchor		: '100%',
				maskRe		:'/[0-9.]/',
				maxValue	: 23,
				minValue	: 0,
				value		: AM.app.globals.editUtilData.data.hours,
				allowBlank	: false
			}, {
				xtype		: 'numberfield',
			 	name		: 'minutes',
			 	fieldLabel	: 'Minutes',
			 	id			:'minUtilID',
			 	anchor		: '100%',
			 	maskRe		:'/[0-9.]/',
			 	maxValue	: 59,
			 	minValue	: 0,
			 	value		: AM.app.globals.editUtilData.data.minutes,
			 	allowBlank	: false
			}, {
			 	xtype		: 'boxselect',
				multiSelect	: false,
				name		: "approved",
				fieldLabel	: "Approval Status",
				queryMode	: 'local',
				displayField: 'Name',
				anchor		: '100%',
				valueField	: 'id',
				store		: Ext.create('Ext.data.Store', {
					fields	: ['id', 'Name'],
					data 	: [ {	id	:	0,	Name	: 'Pending' },
								{	id	:	1,	Name	: 'Approved'}, 
								{	id	:	2,	Name	: 'Rejected'}
								]
							} ),
				value:approve
			} );
    	}
    	formutil.doLayout();
	},
	onAddNewUtilField		: function(me, item) {
		var form = me.down('form'); // this is a better approach
		switch (item.Type) {
			case "list":
			var index	= 0;
			var utilmul	= [];
			item.List.split(",").forEach(function(entry) {
				var util		= {};
				util["id"]		= entry;
				util["Name"]	= entry;
				utilmul[index]	= util;
				index++;
			} );
			var primaryUtil		= Ext.create('Ext.data.Store', {
				fields		: [ 'id', 'Name' ],
				data		: utilmul
			} );
			form.add( {
				xtype		: 'boxselect',
				multiSelect	: false,
				name		: item.AttributesID,
				fieldLabel	: item.Name,
				queryMode	: 'local',
				displayField: 'Name',
				anchor		: '100%',
				valueField	: 'id',
				store		: primaryUtil,
				value		: item.Value
			} );
			break;
			case "integer"	:
			form.add( {
				xtype 		: 'numberfield',
			 	name		: item.AttributesID,
			 	fieldLabel	: item.Name,
			 	anchor		: '100%',
			 	maskRe		:'/[0-9.]/',
			 	minValue	: 0,
			 	allowBlank	: true,
			 	value		: item.Value
			 } );
			break;
			case "text"		:
			form.add( {
				xtype		: 'textfield',
				name		: item.AttributesID,
				fieldLabel	: item.Name,
				value		: item.Value,
				anchor		: '100%',
				allowBlank	: true
			} );
			break;
			case "internaltime":
			form.add( {
				xtype		: 'timefield',
				name		: item.AttributesID,
				fieldLabel	: item.Name,
				value		: item.Value,
				cls			: 'intTimeUtil',
				anchor		: '100%',
				allowBlank	: true,
				minValue	:'00:15',
				maxValue	:'12:00',
				increment	:'15',
				format		:'H:i'
			} );
			break;
			case "externaltime":
			form.add( {
				xtype		: 'timefield',
				name		: item.AttributesID,
				fieldLabel	: item.Name,
				value		: item.Value,
				cls			:'extTimeUtil',
				anchor		: '100%',
				allowBlank	: false,
				minValue	:'00:15',
				maxValue	:'12:00',
				listeners	: {
					change	: function (cb, newValue, oldValue, options) {
						var externaloldTime 		= 0;
						externalnewHours			= Math.floor(Ext.Date.format(newValue, 'H'));
						externalnewMinutes			= Math.floor(Ext.Date.format(newValue, 'i'));
						if(oldValue != null) {
							externaloldHours		= Math.floor(Ext.Date.format(oldValue, 'H'));
							externaloldMinutes		= Math.floor(Ext.Date.format(oldValue, 'i'));
							externaloldTime			= (externaloldHours * 60) + externaloldMinutes;
						}
						externalnewTime				= (externalnewHours * 60) + externalnewMinutes;
						externalboolTime			= (externalnewTime - externaloldTime);
						AM.app.globals.extUtilTime	= AM.app.globals.extUtilTime + externalboolTime;
						extHour 					= Math.floor(AM.app.globals.extUtilTime/60);
						extMinute					= (AM.app.globals.extUtilTime)%60;
						Ext.getCmp('hourUtilID').setValue(extHour);
						Ext.getCmp('minUtilID').setValue(extMinute);
					}
				},
				increment	:'15',
				format		:'H:i'
			} );
			break;
		} 
	},
	listeners				: {
		render				: function(obj) {
			var displayHour = false, externalHours = 0, externalMinutes = 0, externalTime = 0;
			Ext.Ajax.request( {
				url		: AM.app.globals.appPath+'index.php/utilization/utilization_dashboardedit',
				method	: 'GET',
				params	: {
					'utilID'	: AM.app.globals.editUtilData.data.utilization_id,
					'clientID'	: Ext.getCmp('utilization_client').getValue(),
				},
				scope	: this,
				success	: function(response) {
					var myObject 			= Ext.JSON.decode(response.responseText); 
					if(myObject.success) {
						var data 			= myObject.data; 
						var externalHours	= 0, externalMinutes=0;
						Ext.each(data, function(value) {
							obj.onAddNewUtilField(obj,value);
							if(value.Type == "externaltime" && value.Value != "") {
								externalHours	+= Math.floor(value.Value.split(":")[0]);
								externalMinutes	+= Math.floor(value.Value.split(":")[1]);
								displayHour		 = true;
							}
						} );
						AM.app.globals.extUtilTime = (externalHours * 60) + externalMinutes;
						obj.onAddHoursField(obj, displayHour);
					} else {
						obj.onAddHoursField(obj, displayHour);
						Ext.getCmp('utilDashSave').setVisible(true);
					}
					Ext.getCmp('utilDashSave').setVisible(true);												
				}
			});
		}
	},
	controlverTriggered:function(combo, value){
		combo.allowBlank = false;
	
	},
	
	onReset : function() {
		this.up('form').getForm().reset();
	},
	onCancel : function() {
		this.up('window').close();
	}
});