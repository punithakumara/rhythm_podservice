Ext.define('AM.view.utilization.importXls',{
	extend : 'Ext.window.Window',
	alias : 'widget.empimportXls',
	title: 'Import Utilization Details',
	layout: 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal: true,
	
    initComponent : function() {
	
		Ext.apply(Ext.form.VTypes, {
			fileUpload: function(val, field) {                              
				var format = val.split('.', 2);
				if(format[1] != 'csv'){
					Ext.MessageBox.alert('Invalid Format', 'File must be in .csv format');
					return false;
				}
				else return true;
			},                 
			fileUploadText: 'File must be in .csv format',
		});
	
    	this.items = [{
			xtype : 'form',
			itemId : 'importEmployeeForm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 90,
				anchor: '100%'
			},
			fileUpload:true,
			isUpload: true,
			enctype:'multipart/form-data',
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			
			items : [{
				xtype : 'hiddenfield',
				name : 'ClientID',
				id : 'hiddenClientID',
				anchor: '80%',
				style:{
					float:'left'
				}
			}, {
				xtype : 'hiddenfield',
				name : 'Work_id',
				id : 'hiddenWorID',
				anchor: '80%',
				style:{
					float:'left'
				}
			}, {
				xtype : 'hiddenfield',
				name : 'Dashboard',
				id : 'hiddenDashboard',
				anchor: '80%',
				style:{
					float:'left'
				}
			}, {
				xtype : 'filefield',
				name : 'UploadXls',
				itemId : 'UploadXls',
				fieldLabel : 'Import',
				anchor: '80%',
				style:{
					float:'left',
				},
				allowBlank: false,
				buttonText: 'Upload',
				vtype:'fileUpload'
			}, {
				xtype:'button',
				id: "DownloadLink1",
				//anchor: '7%',
				//text:'Download',
				tooltip: 'Download Template',
				icon: AM.app.globals.uiPath+'resources/images/download.png',
				style:{
					float:'left',
					'margin-left':'10px'
				},
				handler : function() {
					if(Ext.getCmp('hiddenDashboard').getValue()==1)
						AM.app.getController('UtilizationDashboard').onDownload(Ext.getCmp('utilization_client').getValue(),"Template");
					else
						AM.app.getController('Utilization').onDownload(Ext.getCmp('utilization_client').getValue(),"Template");
				}
			}, {
				xtype:'displayfield',
				isFormField: true,
				anchor: '80%',
				fieldStyle:{
					clear: 'both',
					color: 'grey',
					'font-size': '11px'
				},
				value: "Instructions:"
			}, {
				xtype:'displayfield',
				isFormField: true,
				anchor: '80%',
				fieldStyle:{
					clear: 'both',
					color: 'grey',
					margin: '0px',
					'font-size': '11px'
				},
				value: "1. Download the template"
			}, {
				xtype:'displayfield',
				isFormField: true,
				anchor: '80%',
				fieldStyle:{
					clear: 'both',
					color: 'grey',
					margin: '0px',
					'font-size': '11px'
				},
				value: "2. Fill the data & Save it as .csv file"
			}, {
				xtype:'displayfield',
				isFormField: true,
				anchor: '80%',
				fieldStyle:{
					clear: 'both',
					color: 'grey',
					margin: '0px',
					'font-size': '11px'
				},
				value: "3. Upload the .csv file"
			}, {
				xtype:'displayfield',
				isFormField: true,
				anchor: '80%',
				fieldStyle:{
					clear: 'both',
					color: 'grey',
					margin: '0px',
					'font-size': '11px'
				},
				value: "4. Hours and Mins are in the format of (hh:mm) and date should be in the format (dd-mm-yyyy)." 
			}, {
				xtype:'displayfield',
				isFormField: true,
				anchor: '80%',
				fieldStyle:{
					clear: 'both',
					color: 'grey',
					margin: '0px',
					'font-size': '11px'
				},
				value: "5. All fields are mandatory"
			}]
		}];
		
    	this.buttons = ['->', {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
        }];

		this.callParent(arguments);
	},
	
	defaultFocus:'UploadXls',
	
	onSave :  function() {
		var win = this.up('window');
		var form = win.down('form');
		var dashboard = Ext.getCmp("hiddenDashboard").getValue();
		if (form.isValid()) 
		{
			if(dashboard==1)
				AM.app.getController("UtilizationDashboard").onImportxls(form, win);
			else
				AM.app.getController("Utilization").onImportxls(form, win);
		}
	},
	
	onCancel : function() {
		this.up('window').close();
	}
});