Ext.define('AM.view.utilization.UtilizationDashboardGrid',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.UtilizationDashboardGrid',
	requires : ['Ext.ux.grid.plugin.PagingSelectionPersistence'],
    store : 'UtilizationDashboard',
    id : 'UtilizationDashboardGridID',
    layout : 'fit',
	title: 'Insight',
	border:true,
	selType: 'rowmodel',
    height:550,
	emptyText: '<div align="center">No Data To Display</div>',
	plugins : [{ ptype : 'pagingselectpersist' }],
	viewConfig: { 
		deferEmptyText: false,
		getRowClass: function(record, rowIndex, rowParams, store) {
		  
		  console.log(record.get('approved'));
		  if (record.get('approved')=="Approved") return 'approved';
		  //else if (record.get('approved')=="Pending") return 'rowBezahlt';
		  else if (record.get('approved')=="Rejected") return 'rejected';
		 },
		
        listeners: {
            refresh: function(view) {
			
				Ext.select('.EditInsight').on('click', function(){ 
					var RowIndex = this.parentNode.parentNode.parentNode.getAttribute("data-recordindex");
					var rec = Ext.getCmp('UtilizationDashboardGridID').getStore().getAt(RowIndex);
					AM.app.getController('UtilizationDashboard').onEdit(rec);
				});
				
				if(this.store.getProxy().reader.jsonData != undefined) {
					Ext.getCmp('TotalHours').setValue(this.store.getProxy().reader.jsonData.TotalHours);
					Ext.getCmp('OverallTime').setValue(this.store.getProxy().reader.jsonData.OverallTime);
				}
            }
        }
    
	},
    width : '99%',
	
	
    /**
     * initialising the components
     */
    initComponent: function(){
    
		/**
		* set the config we want
		*/
		var me = this;
		var clientRender = false;
		var date = new Date(), 
		startMinDate = Ext.Date.add(date, Ext.Date.YEAR, -12), startMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 1), 
		endMaxDate = Ext.Date.add(date, Ext.Date.YEAR, 5);
    	
		
		var sm = Ext.create('Ext.selection.CheckboxModel',{
			checkOnly: true,
		});

		this.tbar = [{
			xtype : 'button',
			icon: AM.app.globals.uiPath+'resources/images/upload.png',
			id:'UploadExcel',
			visible: true,
			hidden:true,
			disabled: true,
			disabledCls: "disabledComboTestCls",
			text : 'Import',
			handler : function() {
				AM.app.getController('UtilizationDashboard').onUpload(me.query('combobox')[0].getValue());			
			}
		},{
            xtype:'button',
            isFormField: true,
            id: "DownloadLink",
			visible: true,
			hidden:false,
            disabled: true,
            disabledCls: "disabledComboTestCls",
            anchor: '5%',
            text:'Export To Excel',
            icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
			style:{
				float:'left',
				'margin-left':'10px'
			},
			handler : function() {
				AM.app.getController('UtilizationDashboard').onDownload(me.query('combobox')[0].getValue());
			}
        },{
            xtype:'button',
            isFormField: true,
            id: "HistoryLink",
			visible: true,
			hidden:true,
            disabled: true,
            disabledCls: "disabledComboTestCls",
            anchor: '5%',
            text:'History',
            icon: AM.app.globals.uiPath+'resources/images/history.png',
			style:{
				float:'left',
				'margin-left':'10px'
			},
			handler : function() {			
				AM.app.globals.histrydbrdProcess = "";
				AM.app.getController('UtilizationDashboard').onHistory(me.query('combobox')[1].getValue(), me.query('combobox')[1].getRawValue(), me.query('combobox')[0].getRawValue(), me.query('combobox')[1].getValue());
			}
        }];
		
		var config = {
			selModel : sm,
            columns:[],
            rowNumberer: false
        };
	
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: 'UtilizationDashboard',
			dock: 'bottom',
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",						
			style:{
				border: 'none'
			},
			displayInfo: true,
			listeners: {
				beforechange: function(item1,item2,item3){
				
					if(this.store.currentPage < item2){
						start = ((this.store.currentPage)*Ext.getCmp('pagination_records').getValue());	
					}else{
						start = ((this.store.currentPage - 2)*Ext.getCmp('pagination_records').getValue());	
					}
					
					total = this.getPageData().pageCount;
					if(total < this.store.currentPage){
						this.store.currentPage = 2;
						this.store.getProxy().extraParams = {'client_id':me.query('combobox')[0].getValue(),'utilization_start_date':me.query('datefield')[0].getValue(),'utilization_end_date':me.query('datefield')[1].getValue(),'EmployID':me.query('combobox')[1].getValue(),'limit':Ext.getCmp('pagination_records').getValue(),'page' : 1,'start':0};
					}else{
						this.store.getProxy().extraParams = {'client_id':me.query('combobox')[0].getValue(),'utilization_start_date':me.query('datefield')[0].getValue(),'utilization_end_date':me.query('datefield')[1].getValue(),'EmployID':me.query('combobox')[1].getValue(),'limit':Ext.getCmp('pagination_records').getValue(),'start':start};
					}
				}
			}		   
		},'->',{
			xtype : 'combobox',
			multiSelect: false,
			id: 'pagination_records',
			name: 'pagination_records',
			minChars: 0,
			store: [15,25,50,100],
			queryMode: 'local',
			displayField: 'Records',
			valueField: 'rec',
			width : 200,
			value : 15,
			labelWidth : 110,
			style: {
				'margin-left':'60px'
			},
			fieldLabel: 'No Of Records',
			listeners: {				 
				select: function(combo) {															
					//Ext.getCmp('numberfield-1127-inputEl').setValue(1);					
					Ext.getCmp('UtilizationDashboardGridID').getStore().pageSize = combo.getValue();
					Ext.getCmp('UtilizationDashboardGridID').getStore().currentPage = 1;
					Ext.getCmp('UtilizationDashboardGridID').getStore().proxy.extraParams = { limit: combo.getValue(),EmployID:me.query('combobox')[1].getValue(),client_id:me.query('combobox')[0].getValue(),utilization_start_date:me.query('datefield')[0].getValue(),utilization_end_date:me.query('datefield')[1].getValue(),start:0,page:1};
					Ext.getCmp('UtilizationDashboardGridID').getStore().reload();
				}
			}
		},'->',{
			xtype    : 'displayfield',
			id: "OverallTime",
			visible:false,
			value:''
		},'->',,{
			xtype    : 'displayfield',
			id: "TotalHours",
			visible:false,
			value:''
		},'->',{
			text    : 'Approve',
			disabled: true,
			hidden:true,
			visible:false,
			disabledCls: "disabledComboTestCls",
			id: "approveButton",
			handler : function() {
				var clientID =  me.query('combobox')[0].getValue();
				if(clientID)
				{
					AM.app.getController('UtilizationDashboard').onApprove(me, this.up('grid'), 1);
				}
				else
					alert("Select Client to Approve the utilization !!!");
			}
		},{
			text    : 'Reject',
			disabled: true,
			hidden:true,
			visible:false,
			disabledCls: "disabledComboTestCls",
			id: "rejectButton",
			handler : function() {
				var clientID =  me.query('combobox')[0].getValue();
				if(clientID)
				{
					AM.app.getController('UtilizationDashboard').onApprove(me, this.up('grid'), 2);
				}
				else
					alert("Select Client to Reject the utilization !!!");
			}
		}];
		
        // appy to this config
        Ext.apply(this, config);
        // apply to the initialConfig
        Ext.apply(this.initialConfig, config);

		this.tools = [{
			xtype: 'datefield',
			format: AM.app.globals.CommonDateControl,
			name : 'utilization_start_date',
			anchor: '100%',
			id: 'utilization_start_date',
			displayField: 'abbr',
			valueField: 'Start Date',
			fieldLabel: 'Start Date',
			labelWidth : 80,
			labelCls : 'searchLabel',
			minValue: startMinDate,
			maxValue: startMaxDate,					
			width : 200,
			style: {
				'margin-right' : '5px'
			},
			//value:new Date(),
			listeners: {
				select : function(combo) {
					me.query('datefield')[1].setMinValue(me.query('datefield')[0].getValue());
					var clientID =  me.query('combobox')[0].getValue();
					if(clientID)
					{
						me.utilDate = combo.getValue();
						AM.app.getController('UtilizationDashboard').utilzationDashdoardFun(me,me.query('datefield')[0].getValue(),me.query('datefield')[1].getValue(), me.query('combobox')[1].getValue(),'',clientID);
					}
				}
			}
		}, {
			xtype: 'datefield',
			format: AM.app.globals.CommonDateControl,
			name : 'utilization_end_date',
			maxValue: endMaxDate,
			anchor: '100%',
			id: 'utilization_end_date',
			displayField: 'abbr',
			valueField: 'End Date',
			fieldLabel: 'End Date',
			labelWidth : 80,
			labelCls : 'searchLabel',
			width : 200,
			style: {
				'margin-right' : '5px'
			},
			//value:new Date(),
			listeners: {
				select : function(combo) {
					me.query('datefield')[0].setMaxValue(me.query('datefield')[1].getValue());
					var clientID =  me.query('combobox')[0].getValue();
					if(clientID)
					{	
						me.utilDate = combo.getValue();
						AM.app.getController('UtilizationDashboard').utilzationDashdoardFun(me,me.query('datefield')[0].getValue(),me.query('datefield')[1].getValue(), me.query('combobox')[1].getValue(),'',clientID);
					}
				}
			}
		}, {
			xtype : 'combobox',
			multiSelect: false,
			id: 'utilization_client',
			name: 'utilization_client',
			typeAhead: true,
			minChars: 0,
			store: Ext.getStore('LogInViewerClients'),
			queryMode: 'remote',
			displayField: 'client_name',
			valueField: 'client_id',
			width : 200,
			labelWidth : 50,
			labelCls : 'searchLabel',
			style: {
				'margin-right' : '5px'
			},
			fieldLabel: 'Client',
			// labelWidth : 70,
			emptyText:"Select Client",
			anchor: '100%',
			listeners: {
				beforequery: function(queryEvent, eOpts) {
					queryEvent.combo.store.proxy.extraParams = {
						'hasNoLimit':'1',
						'comboClient':'1',
						'filterName' : queryEvent.combo.displayField
					}
				},
				select: function(combo) {					
					    me.ClientId = combo.getValue();											
						me.query('combobox')[1].clearValue();
						me.query('combobox')[2].clearValue();
						var selectedRecord = "";
						
						me.StartDate = me.query('datefield')[0].getValue();
						me.EndDate = me.query('datefield')[1].getValue();
						AM.app.getController('UtilizationDashboard').utilzationDashdoardFun(me,me.StartDate,me.EndDate, 0, me.query('combobox')[1].getValue(), me.ClientId);
						Ext.getStore('Employees').load({
							params:{'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':combo.getValue()}
						});
						me.query('combobox')[1].enable();
						//me.query('combobox')[2].clearValue();	
						me.query('combobox')[1].bindStore("Employees");
						me.query('combobox')[0].bindStore("LogInViewerClients");

				},				
			}
		},{
			xtype : 'combobox',
			fieldLabel: 'Employee Name',
			id: 'IL_EmployID',
			name: 'EmployID',
			store: 'Employees',
			labelCls : 'searchLabel',
			mode: 'local',
			labelWidth : 150,
			disabled:true,
			minChars: 1,
			displayField: 'full_name', 
			valueField: 'employee_id',
			allowBlank: false,
			anchor: '100%',
			emptyText: 'Select Employee',
			listeners:{
				beforequery: function(queryEvent){
					var ClntID =  me.query('combobox')[0].getValue();
					queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':ClntID,'filterName'	: 'employees.first_name'};
				},
				select :function (combo){
					var ClntID =  me.query('combobox')[0].getValue();
					AM.app.getController('UtilizationDashboard').utilzationDashdoardFun(me, me.query('datefield')[0].getValue(),me.query('datefield')[1].getValue(), me.query('combobox')[1].getValue(),  me.query('combobox')[0].getValue(), ClntID);								
				}
			}
		}],	 
		 
        this.callParent(arguments);
    },
	
    /**
    * When the store is loading then reconfigure the column model of the grid
    */
    storeLoad: function(store,me)
    {
        /**
        * JSON data returned from server has the column definitions
        */
        if(typeof(store.proxy.reader.jsonData.columns) === 'object') 
		{
            var columns = [];
            /**
            * Assign new columns from the json data columns
            */
            Ext.each(store.proxy.reader.jsonData.columns, function(column){
                columns.push(column);
            });
			
            /**
            * Reconfigure the column model of the grid
            */			
			me.reconfigure(store, columns);
		}
    }
});