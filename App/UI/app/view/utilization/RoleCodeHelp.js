Ext.define('AM.view.utilization.RoleCodeHelp',{
	extend : 'Ext.window.Window',
	alias : 'widget.RoleCodeHelp',
	layout: 'fit',
	//width : '99%',
	layout: {
        type: 'vbox',
    },
	store: 'roleCode',
	width : '80%',
	height: '84%',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '5px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
    	this.items = [{
	        xtype : 'trigger',
	        itemId : 'gridTrigger',
	        id : 'gridTrigger',
	        fieldLabel : '',
	        labelWidth : 60,
	        labelCls : 'searchLabel',
	        triggerCls : 'x-form-clear-trigger',
	        emptyText : 'Search Role Code, Work Model, Shift Code, Support Type, Roles',
	        width : 550,
	        minChars : 1,
	        enableKeyEvents : true,
	        onTriggerClick : function(){
	            this.reset();
	            this.fireEvent('triggerClear');
	        }
	    },{
			xtype:'gridpanel',
			border:true,
			margin:'0',
			height:450,
			width:'100%',
			id:'role_code_grid',			
			columns : [{
				header: 'SL',
				xtype: 'rownumberer',
				flex: 0.25,
				// width: 50,
			}, {
				header: 'Role Code',  
				dataIndex: 'project_code',  
				flex: 2.5,
				sortable: false,
				menuDisabled:true,
				draggable: false
			}, {
				header: 'Work Model', 
				dataIndex: 'model', 
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false
			}, {
				header : 'Shift', 
				dataIndex : 'shift_name', 
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false
			},{
				header: 'Roles', 
				dataIndex: 'responsibilities', 
				flex: 2,
				sortable: false,
				menuDisabled:true,
				draggable: false	
			},{
				header: 'Support Coverage', 
				dataIndex: 'support_coverage', 
				flex: 1.3,
				sortable: false,
				menuDisabled:true,
				draggable: false
			}],
			
			bbar: Ext.create('Ext.PagingToolbar', {
				displayInfo: true,				
				id:'role_code_grid_bb',
				pageSize: AM.app.globals.itemsPerPage,
				displayMsg: 'Displaying {0} - {1} of {2}',
				emptyMsg: "No Data To Display",
				store   : 'roleCode'
			}),
		}];

		this.callParent(arguments);
	},		
	
});