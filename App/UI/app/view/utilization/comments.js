Ext.define('AM.view.utilization.comments',{
	extend : 'Ext.window.Window',
	alias : 'widget.utilizationComments',
	id: 'utilization_comments_add_edit',
	layout: 'fit',
	title: 'Add Comments',
	border : 'true',
	width : 400,
	height: 400,
	autoShow : true,
    modal: true,
	autoScroll : true,
	resizable:false,
	
    initComponent : function() {
		
    	this.items = [{
			xtype : 'form',
			id: 'utilizationForm',
			fieldDefaults: {
				labelAlign: 'left',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px 10px 6px 10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype : 'hidden',
				id:'UtilizationID',
				name : 'UtilizationID',
			},{
				xtype : 'hidden',
				id:'timeWeekRange',
				name : 'ClientName',
			},{
				xtype : 'displayfield',
				fieldLabel : 'Client Name <span style="color:red">*</span>',
				name : 'ClientName',
			},
			{
				xtype : 'displayfield',
				fieldLabel : 'Work Order Name <span style="color:red">*</span>',
				name : 'WorkOrderName',
			},
			{
				xtype : 'displayfield',
				fieldLabel : 'Project Name <span style="color:red">*</span>',
				name : 'ProjectType',
			},{
				xtype : 'displayfield',
				fieldLabel : 'Task Name <span style="color:red">*</span>',
				name : 'Task',
			}, {
				xtype : 'displayfield',
				fieldLabel : 'Sub Task',
				name : 'SubTask',
			}, {
				xtype:'textfield',
				fieldLabel: 'Mon',
				allowBlank: true,
				id: 'Mon',
				name : 'comment1',
			}, {
				xtype:'textfield',
				fieldLabel: 'Tue',
				allowBlank: true,
				id: 'Tue',
				name : 'comment2',
			}, {
				xtype:'textfield',
				fieldLabel: 'Wed',
				allowBlank: true,
				id: 'Wed',
				name : 'comment3',
			}, {
				xtype:'textfield',
				fieldLabel: 'Thu',
				allowBlank: true,
				id: 'Thu',
				name : 'comment4',
			}, {
				xtype:'textfield',
				fieldLabel: 'Fri',
				allowBlank: true,
				id: 'Fri',
				name : 'comment5',
			}, {
				xtype:'textfield',
				fieldLabel: 'Sat',
				allowBlank: true,
				id: 'Sat',
				name : 'comment6',
			}, {
				xtype:'textfield',
				fieldLabel: 'Sun',
				allowBlank: true,
				id: 'Sun',
				name : 'comment7',
			}]
    	}];
		
    	this.buttons = ['->', {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : function(obj) {
				var win  = this.up('window');
				var form  = win.down('form').getForm();
				console.log(Ext.getCmp('timeWeekRange').getValue());
				
				var	values = form.getValues();
				
				obj.setDisabled(true);
				form.submit({
					url: AM.app.globals.appPath+'index.php/Utilization/utilizationSaveComments/?v='+AM.app.globals.version,
					method: 'POST',
					params: {
						'weekrange': Ext.getCmp('timeWeekRange').getValue(),
					},
					success: function(form,o) {
						var retrData = JSON.parse(o.response.responseText);
						
						Ext.MessageBox.show({
							title: "Success",
							msg: retrData.message,
							buttons: Ext.Msg.OK,
							closable:false,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									win.close();
									AM.app.getController('UtilizationTest').listRecords(Ext.getCmp('UtilizationAddGridID'), Ext.getCmp('timeWeekRange').getValue(), Ext.getCmp('teamMember').getValue());
								}
							}
						});
					},
					failure: function(response) {
						Ext.MessageBox.show({
							title: "Warnings",
							icon: Ext.MessageBox.WARNING,
							msg: "Error while adding Record",
							buttons: Ext.Msg.OK
						});
						win.close();
					}
				});
			}
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},

	
});