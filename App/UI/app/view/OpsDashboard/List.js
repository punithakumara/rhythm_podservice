Ext.define('AM.view.OpsDashboard.List',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.OpsDashboardgrid',
	title: 'Employee View',
    store : 'OpsDashboard',    
    layout : 'fit',
    viewConfig: {
		forceFit: true,
		getRowClass: function(record) {
			if(record.get('NoticePeriod')=='Yes'){
				return 'resigned';
			}
		}
	},
    width : '99%',
	height : 509,
    loadMask: true,   
    remoteFilter:true,
    border : true,
    style:{
  		border: "0px solid #157fcc",
        borderRadius:'0px'
	},
	frame: true,
    initComponent : function() {
    	
		this.columns = [{
			header : 'Employee ID', 					
			dataIndex : 'EmployeeID',
			width: 100,
			menuDisabled:true,
			groupable: false,
			draggable: false,
		},{
			header : 'Employee Full Name', 
			dataIndex : 'EmployeeFullName',
			width: 200,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("EmployeeFullName") + '"';
				return value;
			}
		},{
			header : 'Location',
			dataIndex: 'Location',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 100,
		},{
			header : 'Work Shift', 
			dataIndex: 'WorkShift',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 100,
		},{
			header : 'Joining Date', 
			dataIndex: 'JoiningDate',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 100,
		},{
			header : 'Pod', 
			dataIndex : 'Pod',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 200,
		},{
			header : 'Service', 
			dataIndex : 'Service',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 200,
		},{
			header : 'Client', 
			dataIndex: 'ClientName',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 170,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("ClientName") + '"';
				return value;
			}
		},{
			header : 'Grade', 
			dataIndex: 'Grade',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 60,
		},{
			header : 'Designation', 
			dataIndex : 'Designation',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 180,
		},{
			header : 'Supervisor', 
			dataIndex : 'Supervisor',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 180,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("Supervisor") + '"';
				return value;
			}
		},{
			header : 'Reporting Manager', 
			dataIndex : 'ReportingManager',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 180,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("ReportingManager") + '"';
				return value;
			}
		},{
			header : 'Pod Manager', 
			dataIndex : 'PodManager',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 180,
			renderer:function(value,metaData,record,colIndex,store,view) {					
				metaData.tdAttr = 'data-qtip="' + record.get("VerticalManager") + '"';
				return value;
			}
		},{
			header : 'Billable / Non-Billable', 
			dataIndex: 'Billable',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 170,
		},{
			header : 'Notice Period',
			dataIndex: 'NoticePeriod',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 120
		},{
			header : 'Resignation Date',
			dataIndex: 'ResignationDate',
			menuDisabled:true,
			groupable: false,
			draggable: false,
			width: 130
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            displayInfo: true,
            pageSize: AM.app.globals.itemsPerPage,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Attributes to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });		
		
		this.tools =  [{
	    	xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 60,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search',
			width : 250,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
		},{ 
	        xtype : 'tbseparator',
	        style : {
	        	'margin-right' : '10px'
	        }
	    },{
			xtype: "button",
	    	icon: AM.app.globals.uiPath+'resources/images/Add.png',
			text : 'Export To Excel',
			style: {
	            "margin-right": "8px"
	        },
			handler: function(obj){
	    		AM.app.getController('OpsDashboard').downloadhierarchyCSV();
			}
		}]
		
		this.callParent(arguments);
		
	},
});