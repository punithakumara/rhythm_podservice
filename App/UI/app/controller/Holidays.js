Ext.define('AM.controller.Holidays',{
	extend : 'Ext.app.Controller',
	stores: ["Holidays"],
	views: ['Holidays.List', 'Holidays.AddEdit'],
	
	viewHolidaysList : function() {
		delete this.getStore('Holidays').getProxy().extraParams;
		var store = this.getStore('Holidays');
        store.clearFilter();
		
		var store = this.getStore('Vertical');
		store.getProxy().extraParams = {
			'hasNoLimit'				: '1'
		};
        store.clearFilter();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var holidaysList = this.getView('Holidays.List').create();
		mainContent.removeAll();
		mainContent.add(holidaysList);
		mainContent.doLayout();		
	},
	
	onCreateHoliday : function() {
		var view = Ext.widget('holidayAddEdit');
	},
	
	onSaveHoliday : function(form, win) {
		var myStore = this.getStore('Holidays');
		var record = form.getRecord(),
		values = form.getValues();
		
		if(values.holiday_id=="") 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
		    success: function (form, o) 
			{
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{
					win.close();
					
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
					
					myStore.load();
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    }, 
		    failure: function (batch) 
			{
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditHoliday : function(grid, row, col) 
	{
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('holidayAddEdit');
		view.setTitle('Edit Client Holiday');
        view.down('form').loadRecord(rec);
	},
	
	onDeleteHoliday : function(grid, row, col) 
	{
		var rec = grid.getStore().getAt(row);
		var holName = rec.get('name');
		Ext.Msg.confirm('Delete', "Are you sure to delete '"+holName+"' ?", function (button) 
		{
            if (button == 'yes') 
			{
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) 
					{	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) 
						{
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else 
						{
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) 
					{
						grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	
});