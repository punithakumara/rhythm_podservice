Ext.define('AM.controller.Tech',{
	extend : 'Ext.app.Controller',
	stores : ['Vertical','Tech','TechReport'],
    models : ['Tech'],
	views: ['Skills.TechList','Skills.Tech_Add_edit'],
	
	init : function() {
		this.control({
			'techList' : {
				'deleteTech' : this.onDeleteTech,
				'editTech' : this.onEditTech
			},
			'techList #gridTrigger' : {
                keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
            'tooltechList #gridTrigger' : {
                keyup : this.onTriggerKeyUpRpt,
                triggerClear : this.onTriggerClearRpt
            },
			'techList button[action=createTech]' : {
				click : this.onCreate
			},
			'techAddEdit' : {
				'saveTech' : this.onSaveTech
			}
		});
	},
	
	viewContent : function() {
		delete this.getStore('Tech').getProxy().extraParams;
		var store = this.getStore('Tech');
        store.clearFilter();
		
		var store = this.getStore('Vertical');
		store.getProxy().extraParams = {'hasNoLimit' : '1'};
        store.clearFilter();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var tech = this.getView('tech.List').create();
		mainContent.removeAll();
		mainContent.add( tech );
		mainContent.doLayout();
	},
	
	newTechnology : function(verticalId) {
		var view = Ext.widget('techAddEdit');
		Ext.getCmp('vertIS').setValue(verticalId);
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Tech');
		store.filters.clear();
        store.filter({
            property: 'tech.Name, tech.Description, vert.Name',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Tech');
        store.clearFilter();
    },
	
    onTriggerKeyUpRpt : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('TechReport');
		store.filters.clear();
        store.filter({
            property: 'tech.Name,Client_Name',
            value: t.getValue()
        });
    },
    
    onTriggerClearRpt : function() {
        var store = this.getStore('TechReport');
        store.clearFilter();
    },
	
	onCreate : function() {
		var view = Ext.widget('techAddEdit');
		view.setTitle('Add Technology Details');
	},
	
	onSaveTech : function(form, win) {
		
		var myStore = this.getStore('Tech');
		var record = form.getRecord(),
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
		    success: function (form, o) {	    	
		    	var VertID =  Ext.get('VerticalNameCombo') ? Ext.getCmp('VerticalNameCombo').getValue() : "";
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{
					if(jsonResp.title=="Updated") 
					{
						win.close();
					}
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    		if(jsonResp.title!="Existing") 
					{
		    			win.close();
					}
		    		if(jsonResp.title=="Existing") 
					{
						if(titleText != "Added"){myStore.load();win.close();}
						myStore.load();
					}	
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    	
		    	if(VertID != "")
				{
	    			Ext.getStore('Tech').removeAll();
	    	    	var myTechStore = Ext.getStore('Tech').load({
	    	    		params:{ VerticalID: VertID, hasNoLimit:1},
	    	    		callback : function(records){
	    	    			if(records.length)
							{
	    	    		    	Ext.getCmp('Process_technologies').bindStore('Tech');
	    	    			}
	    	    		}
	    	        });
	    		}
				else
				{	
	    			myStore.load();
	    		}
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving technology. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditTech : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('techAddEdit');
		// console.log(rec);
		view.setTitle('Edit Technology Details');
        view.down('form').loadRecord(rec);
	},
	
	onDeleteTech : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var techName = rec.get('Name');
		Ext.Msg.confirm('Delete Tech', 'Are you sure to delete '+techName+' ?', function (button) {
            if (button == 'yes') 
			{
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) 
						{
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else 
						{
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting tech. Please try again.");
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	
	ToolTechnologyReport: function()
	{
		delete this.getStore('TechReport').getProxy().extraParams;
		AM.util.Constants.ToolsTech = "SlNo";
		var store = this.getStore('TechReport');
		store.proxy.extraParams = {'tooltechReport':1};
        store.clearFilter();
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var tech = this.getView('tool&techReport.Reportview').create();
		mainContent.removeAll();
		mainContent.add( tech );
		mainContent.doLayout();
	},
	
	downloadtooltechCSV:function()
	{
		AM.util.Constants.ToolsTech = "SlNo";
		var ProcessID	 	= Ext.ComponentQuery.query('#ToolUtilProcessID')[0].value;
		var ClientID  	    = Ext.ComponentQuery.query('#ToolUtilClientID')[0].value;
		var VerticalID 	= Ext.ComponentQuery.query('#ToolUtilVerticalID')[0].value;
		var managerID 	    = Ext.ComponentQuery.query('#ToolUtilmanagerID')[0].value;
		var AMID 			= Ext.ComponentQuery.query('#ToolUtilAMID')[0].value;
		var TLId 			= Ext.ComponentQuery.query('#ToolUtilTLId')[0].value;
		var myStore 		= this.getStore('TechReport');
		
		if( myStore.getCount() == 0 ) 
		{
			Ext.Msg.alert('Alert', 'No Data To Export!!!');
		} 
		else 
		{
			var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
   			myMask.show();
   	    	Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/technology/DownloadtooltechCsv/',
				method: 'GET',
				params: {'Report':"reportflag",'tooltechReport':1,'ClientID':ClientID, 'VerticalID': VerticalID,'ProcessID':ProcessID, 'managerID':managerID, 'AMID':AMID,'TLId':TLId},
				success: function(responce, opts)
				{
					var jsonResp = Ext.decode(responce.responseText);
					if(jsonResp.success) 
					{
						myMask.hide();
						window.location = AM.app.globals.appPath+'download/ToolTechnology.csv?v='+AM.app.globals.version;
					}
				}
			});
		}
	}
});