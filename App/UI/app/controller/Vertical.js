Ext.define('AM.controller.Vertical',{
	extend : 'Ext.app.Controller',
	stores : ['Vertical','AllManagers','Shifts', 'ParentVertical'],
    models : ['Vertical'],
	views: ['vertical.List','vertical.Add_edit'],
	
	init : function() {
		this.control({
			'verticalList' : {
				'deleteVertical' : this.onDeleteVertical,
				'editVertical' : this.onEditVertical
			},
			'verticalList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
			'verticalList button[action=createVertical]' : {
				click : this.onCreate
			},
			'verticalAddEdit' : {
				'saveVertical' : this.onSaveVertical,
			}
		});
	},
	
	viewContent : function(fromdashboard) {
		delete this.getStore('Vertical').getProxy().extraParams;
		var store = this.getStore('Vertical');
		if(fromdashboard!=undefined)
			store.getProxy().extraParams = {'dashboard':fromdashboard};
		
        store.clearFilter();
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var vertical = this.getView('vertical.List').create();
		mainContent.removeAll();
		mainContent.add( vertical );
		mainContent.doLayout();
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Vertical');
		store.filters.clear();
        store.filter({
            property: 'temp.name, temp.description, temp.folder, emp.first_name, emp.last_name,sft.shift_code',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Vertical');
        store.clearFilter();
    },
	
	onCreate : function() {
		var view = Ext.widget('verticalAddEdit');
		view.setTitle('Add Vertical Details');
	},
	
	onSaveVertical : function(form, win) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
        });
		var myStore = this.getStore('Vertical');
		var record = form.getRecord(),
		values = form.getValues();
		if(!record) 
		{
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			record.set(values);
			var titleText = 'Updated';
		}		
		myStore.sync({ 
		    success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;				
		    	if(jsonResp.success) {				
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });			
		    		if(jsonResp.title!="Existing") {
		    			win.close();
					}
					if(jsonResp.title=="Existing") {
						if(titleText != "Added"){myStore.load();win.close();}
					myStore.load();
					myStore.clearFilter();
					}
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving vertical. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditVertical : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('verticalAddEdit');
		view.setTitle('Edit Vertical Details');
        view.down('form').loadRecord(rec);

	},
	
	onDeleteVertical : function(grid, row, col) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
         });
		var rec = grid.getStore().getAt(row);
		var verticalName = rec.get('Name');
		Ext.Msg.confirm('Delete Vertical', 'Are you sure to delete '+verticalName+' ?', function (button) {
            if (button == 'yes') {
            	Ext.MessageBox.show({
                    msg: 'Saving your data, please wait...',
                    progressText: 'Saving...',
                    width:300,
                    wait:true,
                    waitConfig: {interval:200},
                    animateTarget: 'waitButton'
                 });
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) {
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else {
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting client. Please try again.");
        		    	Ext.MessageBox.hide();
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	}
});