Ext.define('AM.controller.WorTypes',{
	extend : 'Ext.app.Controller',
	stores: ["WorTypes"],
	views: ['WorTypes.List'],

	init : function(){
		this.control({
			'WorTypes #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
		});
	},
	
	viewWorTypes : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var WorTypes = this.getView('WorTypes.List').create();
		mainContent.removeAll();
		mainContent.add(WorTypes);
		mainContent.doLayout();		
	},

	onTriggerClear : function() {
        var store = this.getStore('WorTypes');
        store.clearFilter();
    },
    
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('WorTypes');
		store.filters.clear();
        store.filter({
            property: "wor_type,wor_description",
            value: t.getValue()
        });
    }
	
});