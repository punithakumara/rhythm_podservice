Ext.define('AM.controller.Services',{
	extend : 'Ext.app.Controller',
	stores : ['Services'],
    models : ['Services'],
	views: ['services.List','services.Add_edit'],
	
	init : function() {
		this.control({
			'servicesList' : {
				'deleteServices' : this.onDeleteServices,
				'editServices' : this.onEditServices
			},
			'servicesList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
            'servicesList #servicesStatusCombo' : {
                servicesChangeStatusCombo : this.changeServicesStatus
            },
			'servicesList button[action=createServices]' : {
				click : this.onCreate
			},
			'servicesAddEdit' : {
				'saveServices' : this.onSaveServices,
			}
		});
	},
	
	viewContent : function(fromdashboard) {
		delete this.getStore('Services').getProxy().extraParams;
		var store = this.getStore('Services');
		store.getProxy().extraParams = {'servicesStatusCombo': 1};
		if(fromdashboard!=undefined)
			store.getProxy().extraParams = {'dashboard':fromdashboard};
		
        store.clearFilter();
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var service = this.getView('services.List').create();
		mainContent.removeAll();
		mainContent.add( service );
		mainContent.doLayout();
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Services');
		store.filters.clear();
        store.filter({
            property: 'temp.name, temp.description, temp.folder',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Services');
        store.clearFilter();
    },
	
	onCreate : function() {
		var view = Ext.widget('servicesAddEdit');
		view.setTitle('Add Services Details');
		Ext.ComponentQuery.query("#service_Status_combo")[0].hide();
	},
	
	onSaveServices : function(form, win) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
        });
		var myStore = this.getStore('Services');
		var record = form.getRecord(),
		values = form.getValues();
		if(!record) 
		{
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			record.set(values);
			var titleText = 'Updated';
		}		
		myStore.sync({ 
		    success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;				
		    	if(jsonResp.success) {				
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });			
		    		if(jsonResp.title!="Existing") {
		    			win.close();
					}
					if(jsonResp.title=="Existing") {
						if(titleText != "Added"){myStore.load();win.close();}
					myStore.load();
					myStore.clearFilter();
					}
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving services. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditServices : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('servicesAddEdit');
		view.setTitle('Edit Services Details');
		Ext.ComponentQuery.query("#service_Status_combo")[0].show();
        view.down('form').loadRecord(rec);

	},

	changeServicesStatus: function(combo){
		var store = this.getStore('Services');
		store.getProxy().extraParams = {'servicesStatusCombo': combo.getValue()};
		store.load();
		if(combo.getValue())
			store.clearFilter();
	},
	
	onDeleteServices : function(grid, row, col) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
         });
		var rec = grid.getStore().getAt(row);
		var servicesName = rec.get('Name');
		Ext.Msg.confirm('Delete Service', 'Are you sure to delete this service ?', function (button) {
            if (button == 'yes') {
            	Ext.MessageBox.show({
                    msg: 'Saving your data, please wait...',
                    progressText: 'Saving...',
                    width:300,
                    wait:true,
                    waitConfig: {interval:200},
                    animateTarget: 'waitButton'
                 });
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) {
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else {
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting client. Please try again.");
        		    	Ext.MessageBox.hide();
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	}
});