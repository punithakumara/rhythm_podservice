Ext.define('AM.controller.insight', {
	extend: 'Ext.app.Controller',
	stores: ['LogInViewerClients', 'Employees', 'filterRoleCode', 'UtilEmployees', 'roleCode', 'workOrder', 'UtilizationDashboard', 'WeekendCoverage', 'HolidayCoverage', 'WorTypes', 'ProjectTypes', 'Supervisor'],
	views: ['insight.List', 'insight.GeneralList', 'insight.WeekendList', 'insight.ClientHolidaysList', 'insight.TopPanel'],

	viewContent: function (clientID, from) {

		delete this.getStore('UtilizationDashboard').getProxy().extraParams;
		delete this.getStore('WeekendCoverage').getProxy().extraParams;
		delete this.getStore('HolidayCoverage').getProxy().extraParams;


		Ext.getStore('LogInViewerClients').load({
			params: {
				'hasNoLimit': '1',
				'comboClient': '1',
			},
			callback: function (records, options, success) {
				if (success) {
					Ext.getCmp('utilization_client').setValue(records[0].data.client_id);

					Ext.getStore('workOrder').load({
						params: {
							'comboClient': records[0].data.client_id,
							'utilClientWor': 1
						},
						callback: function (worrecords, options, success) {
							if (success) {
								Ext.getCmp('utilization_wor_id').setValue(worrecords[0].data.wor_id).enable();

								Ext.getStore('WorTypes').load({
									params: {
										'wor_id': worrecords[0].data.wor_id,
										'comboClient': records[0].data.client_id
									},
									callback: function (wortyperecords, options, success) {
										if (success) {
											Ext.getCmp('wor_type').setValue(wortyperecords[0].data.wor_type_id).enable();

											Ext.getStore('ProjectTypes').load({
												params: {
													'hasNoLimit': "1",
													'wor_id': worrecords[0].data.wor_id,
													'comboClient': records[0].data.client_id
												},
												callback: function (projtyperecords, options, success) {
													if (success) {
														Ext.getCmp('project_type').setValue(projtyperecords[0].data.project_type_id).enable();

														Ext.getStore('filterRoleCode').load({
															params: {
																'comboClient': records[0].data.client_id,
																'wor_id': worrecords[0].data.wor_id,
																'wor_type_id': wortyperecords[0].data.wor_type_id,
																'project_type_id': projtyperecords[0].data.project_type_id,
																'filter_rolecode': '1'
															},
															callback: function (rolecoderecords, options, success) {
																if (success) {
																	// console.log(rolecoderecords);
																	Ext.getCmp('util_project_code').setValue(rolecoderecords[0].data.process_id).enable();
																	Ext.getCmp('supervisor').enable();
																	Ext.getCmp('IL_EmployID').enable();

																	AM.app.getController('insight').listAllDataUtilRecords(Ext.getCmp('InsightGridID'), Ext.getCmp('utilization_start_date').getValue(), Ext.getCmp('utilization_end_date').getValue(), records[0].data.client_id, worrecords[0].data.wor_id, rolecoderecords[0].data.process_id, '', 0, 'all_data', wortyperecords[0].data.wor_type_id, projtyperecords[0].data.project_type_id, '');
																	AM.app.getController('insight').listHolidayUtilRecords(Ext.getCmp('InsightGridID'), Ext.getCmp('utilization_start_date').getValue(), Ext.getCmp('utilization_end_date').getValue(), records[0].data.client_id, worrecords[0].data.wor_id, rolecoderecords[0].data.process_id, '', 0, 'holiday_coverage', wortyperecords[0].data.wor_type_id, projtyperecords[0].data.project_type_id, '');
																	AM.app.getController('insight').listWeekendRecords(Ext.getCmp('InsightGridID'), Ext.getCmp('utilization_start_date').getValue(), Ext.getCmp('utilization_end_date').getValue(), records[0].data.client_id, worrecords[0].data.wor_id, rolecoderecords[0].data.process_id, '', 0, 'weekend_coverage', wortyperecords[0].data.wor_type_id, projtyperecords[0].data.project_type_id, '');
																}
															}

														});
													}
												}
											});
										}
									}

								});
							}
						}
					});
				}
			}
		});

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var insList = this.getView('insight.List').create();
		mainContent.removeAll();

		mainContent.add(insList);
		mainContent.doLayout();

		Ext.getCmp('InsightGridID').getStore().removeAll();
		Ext.getCmp('InsightGridID').getStore().sync();
	},

	listAllDataUtilRecords: function (me, startDate, endDate, clientID, workOrder, projectCode, EmployeeID, UtilStatus, tab_type, wor_type_id, project_type_id, supervisor) {
		Ext.getStore('UtilizationDashboard').removeAll();
		Ext.getStore('UtilizationDashboard').currentPage = 1;

		Ext.getStore('UtilizationDashboard').load({
			params: {
				'utilization_start_date': startDate,
				'utilization_end_date': endDate,
				'client_id': clientID,
				'wor_id': workOrder,
				'project_code': projectCode,
				'EmployID': EmployeeID,
				'utilstatus': UtilStatus,
				'tab_type': tab_type,
				'wor_type_id': wor_type_id,
				'project_type_id': project_type_id,
				'supervisor': supervisor
			},
			callback: function (records, options, success) {
				if(records[0].data != ""){
					console.log(records[0].data);
				}
				if (success) {
					if (Ext.util.Cookies.get("grade") >= 3) {
						Ext.getCmp('download_all').setDisabled(false);
						Ext.getCmp('download_all').addClass('enabledComboTestCls');
					}
					else {
						Ext.getCmp('download_all').setDisabled(true);
					}

					var Obj = eval('(' + options.response.responseText + ')');

					if (records.length != 0) {
						Ext.getCmp('approveButton').show();
						Ext.getCmp('rejectButton').show();
						Ext.getCmp('currentList').show();
						Ext.getCmp('currentList').setValue(Obj.TotalHours);
						Ext.getCmp('currentList').setValue(Obj.OverallTime);

						Ext.getCmp('approveButton').setDisabled(false);
						Ext.getCmp('approveButton').addClass('enabledComboTestCls');
						Ext.getCmp('rejectButton').setDisabled(false);
						Ext.getCmp('rejectButton').addClass('enabledComboTestCls');
					}
					//console.log(me.store);

					me.storeLoad(me.store, me);
					me.doLayout(false, true);
				}
			}
		});
	},

	listWeekendRecords: function (me, startDate, endDate, clientID, workOrder, projectCode, EmployeeID, UtilStatus, tab_type, wor_type_id, project_type_id, supervisor) {
		Ext.getStore('WeekendCoverage').removeAll();
		Ext.getStore('WeekendCoverage').currentPage = 1;

		Ext.getStore('WeekendCoverage').load({
			params: {
				'utilization_start_date': startDate,
				'utilization_end_date': endDate,
				'client_id': clientID,
				'wor_id': workOrder,
				'project_code': projectCode,
				'EmployID': EmployeeID,
				'utilstatus': UtilStatus,
				'tab_type': tab_type,
				'wor_type_id': wor_type_id,
				'project_type_id': project_type_id,
				'supervisor': supervisor
			},
			callback: function (records, options, success) {
				if (success) {
					if (Ext.util.Cookies.get("grade") >= 3) {
						Ext.getCmp('download_all').setDisabled(false);
						Ext.getCmp('download_all').addClass('enabledComboTestCls');
					}
					else {
						Ext.getCmp('download_all').setDisabled(true);
					}

					var Obj = eval('(' + options.response.responseText + ')');
					if (records.length != 0) {
						Ext.getCmp('weekendApproveButton').show();
						Ext.getCmp('weekendRejectButton').show();
						Ext.getCmp('wecurrentList').show();
						Ext.getCmp('wecurrentList').setValue(Obj.TotalHours);
						Ext.getCmp('wecurrentList').setValue(Obj.OverallTime);

						Ext.getCmp('weekendApproveButton').setDisabled(false);
						Ext.getCmp('weekendApproveButton').addClass('enabledComboTestCls');
						Ext.getCmp('weekendRejectButton').setDisabled(false);
						Ext.getCmp('weekendRejectButton').addClass('enabledComboTestCls');
					}

					me.storeLoad(me.store, me);
					me.doLayout(false, true);
				}
			}
		});
	},



	listHolidayUtilRecords: function (me, startDate, endDate, clientID, workOrder, projectCode, EmployeeID, UtilStatus, tab_type, wor_type_id, project_type_id, supervisor) {
		Ext.getStore('HolidayCoverage').removeAll();
		Ext.getStore('HolidayCoverage').currentPage = 1;

		Ext.getStore('HolidayCoverage').load({
			params: {
				'utilization_start_date': startDate,
				'utilization_end_date': endDate,
				'client_id': clientID,
				'wor_id': workOrder,
				'project_code': projectCode,
				'EmployID': EmployeeID,
				'utilstatus': UtilStatus,
				'tab_type': tab_type,
				'wor_type_id': wor_type_id,
				'project_type_id': project_type_id,
				'supervisor': supervisor
			},
			callback: function (records, options, success) {
				if (success) {
					if (Ext.util.Cookies.get("grade") >= 3) {
						Ext.getCmp('download_all').setDisabled(false);
						Ext.getCmp('download_all').addClass('enabledComboTestCls');
					}
					else {
						Ext.getCmp('download_all').setDisabled(true);
					}

					var Obj = eval('(' + options.response.responseText + ')');
					if (records.length != 0) {
						Ext.getCmp('holidayApproveButton').show();
						Ext.getCmp('holidayRejectButton').show();
						Ext.getCmp('chcurrentList').show();
						Ext.getCmp('chcurrentList').setValue(Obj.TotalHours);
						Ext.getCmp('chcurrentList').setValue(Obj.OverallTime);

						Ext.getCmp('holidayApproveButton').setDisabled(false);
						Ext.getCmp('holidayApproveButton').addClass('enabledComboTestCls');
						Ext.getCmp('holidayRejectButton').setDisabled(false);
						Ext.getCmp('holidayRejectButton').addClass('enabledComboTestCls');

					}

					me.storeLoad(me.store, me);
					me.doLayout(false, true);
				}
			}
		});

	},

	onApprove: function (obj, gridPanel, data) {
		var selection = gridPanel.getSelectionModel().selected;
		var select = Ext.getCmp('weekendlistGridID').getView('grid');
		var select2 = select.up('grid');

		var holiday = Ext.getCmp('holidaylistGridID').getView('grid');
		var holidaylist = holiday.up('grid');
		console.log(holidaylist);

		var status = [];
		var recordData = selection.items;

		for (var i = 0; i < recordData.length; i++) {
			status.push(recordData[i].data.status);
		}
		
		if (selection.items.length < 1) {
			var textMsg = "Reject";
			if (data == 1)
				textMsg = "Approve";
			Ext.MessageBox.show({
				title: 'Warning',
				msg: "Select a record before " + textMsg + ". !!!",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else if (data == 1 && status.indexOf("Approved") >= 0) {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: "One or more time entries are already approved. Please deselect those entries.",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else if (data == 2 && status.indexOf("Rejected") >= 0) {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: "One or more time entries are already rejected. Please deselect those entries.",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else {
			var selectArrayIds = [];
			var recordData = selection.items;

			for (var i = 0; i < recordData.length; i++) {
				selectArrayIds[i] = recordData[i].data.utilization_id;
			}
			Ext.Ajax.request({
				url: AM.app.globals.appPath + 'index.php/utilization/utilization_approve/',
				method: 'POST',
				params: {
					UtilizationArray: JSON.stringify(selectArrayIds),
					value: data
				},
				success: function (batch, operations) {
					var jsonResp = Ext.decode(batch.responseText);
					if (jsonResp.success) {
						Ext.MessageBox.show({
							title: jsonResp.title,
							msg: jsonResp.msg,
							buttons: Ext.Msg.OK
						});

						/* Reload All Data Grid */
						Ext.getCmp('InsightGridID').getStore().proxy.extraParams = { EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'all_data' };
						Ext.getCmp('InsightGridID').getStore().reload();

						/* Reload Weekend Coverage Grid */
						Ext.getCmp('weekendlistGridID').getStore().proxy.extraParams = { EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'weekend_coverage' };
						Ext.getCmp('weekendlistGridID').getStore().reload();

						/* Reload Client Holiday Grid */
						Ext.getCmp('holidaylistGridID').getStore().proxy.extraParams = { EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'holiday_coverage' };
						Ext.getCmp('holidaylistGridID').getStore().reload();


					}
					else {
						Ext.MessageBox.show({
							title: 'Error',
							msg: jsonResp.msg,
							icon: Ext.MessageBox.ERROR,
							buttons: Ext.Msg.OK
						});
					}

					gridPanel.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
					select2.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
					holidaylist.getPlugin('pagingSelectionPersistence').clearPersistedSelection();

					/* Reload All Data Grid */
					Ext.getCmp('InsightGridID').getStore().proxy.extraParams = { EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'all_data' };
					Ext.getCmp('InsightGridID').getStore().reload();

					/* Reload Weekend Coverage Grid */
					Ext.getCmp('weekendlistGridID').getStore().proxy.extraParams = { EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'weekend_coverage' };
					Ext.getCmp('weekendlistGridID').getStore().reload();

					/* Reload Client Holiday Grid */
					Ext.getCmp('holidaylistGridID').getStore().proxy.extraParams = { EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'holiday_coverage' };
					Ext.getCmp('holidaylistGridID').getStore().reload();


				},
				failure: function (batch) {
					console.log("Error occured while saving. Please try again.");
					myStore.rejectChanges();
				}
			});
		}
	},


	onBillableApprove: function (obj, gridPanel, data, tab_type) {
		var selection = gridPanel.getSelectionModel().selected;
		var select = Ext.getCmp('InsightGridID').getView('grid');
		var select2 = select.up('grid');

		var holiday = Ext.getCmp('holidaylistGridID').getView('grid');
		var holidaylist = holiday.up('grid');

		var weekend = Ext.getCmp('weekendlistGridID').getView('grid');
		var weekendcoverage = weekend.up('grid');

		var status = [];
		var recordData = selection.items;

		for (var i = 0; i < recordData.length; i++) {
			status.push(recordData[i].data.status);
		}				
		if(selection.items.length < 1) 
		{
			var textMsg = "Exclude";
			if(data==3 || data==4)
				textMsg = "Approve";
			Ext.MessageBox.show({
				title: 'Warning',
				msg: "Select a record before " + textMsg + ". !!!",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else if (data == 1 && status.indexOf("Weekend Hours") >= 0) {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: "One or more time entries are already approved. Please deselect those entries.",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else if (data == 2 && status.indexOf("Approved") >= 0) {
			Ext.MessageBox.show({
				title: 'Warning',
				msg: "One or more time entries are already rejected. Please deselect those entries.",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
		else {
			var selectArrayIds = [];
			var recordData = selection.items;
			for (var i = 0; i < recordData.length; i++) {
				selectArrayIds[i] = recordData[i].data.utilization_id;
			}
			Ext.Ajax.request({
				url: AM.app.globals.appPath + 'index.php/utilization/insight_billble_approve/',
				method: 'POST',
				params: {
					UtilizationArray: JSON.stringify(selectArrayIds),
					value: data,
					tab_type: tab_type,
				},
				success: function (batch, operations) {
					var jsonResp = Ext.decode(batch.responseText);
					if (jsonResp.success) {
						Ext.MessageBox.show({
							title: jsonResp.title,
							msg: jsonResp.msg,
							buttons: Ext.Msg.OK
						});


						/* Reload All Data Grid */
						Ext.getCmp('InsightGridID').getStore().proxy.extraParams = { limit: 15, EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'all_data' };
						Ext.getCmp('InsightGridID').getStore().reload();

						/* Reload Weekend Coverage Grid */
						Ext.getCmp('weekendlistGridID').getStore().proxy.extraParams = { limit: 15, EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'weekend_coverage' };
						Ext.getCmp('weekendlistGridID').getStore().reload();

						/* Reload Client Holiday Grid */
						Ext.getCmp('holidaylistGridID').getStore().proxy.extraParams = { limit: 15, EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'holiday_coverage' };
						Ext.getCmp('holidaylistGridID').getStore().reload();
					}
					else {
						Ext.MessageBox.show({
							title: 'Error',
							msg: jsonResp.msg,
							icon: Ext.MessageBox.ERROR,
							buttons: Ext.Msg.OK
						});
					}
					gridPanel.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
					select2.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
					holidaylist.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
					weekendcoverage.getPlugin('pagingSelectionPersistence').clearPersistedSelection();

					/* Reload All Data Grid */
					Ext.getCmp('InsightGridID').getStore().proxy.extraParams = { limit: 15, EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'all_data' };
					Ext.getCmp('InsightGridID').getStore().reload();

					/* Reload Weekend Coverage Grid */
					Ext.getCmp('weekendlistGridID').getStore().proxy.extraParams = { limit: 15, EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'weekend_coverage' };
					Ext.getCmp('weekendlistGridID').getStore().reload();

					/* Reload Client Holiday Grid */
					Ext.getCmp('holidaylistGridID').getStore().proxy.extraParams = { limit: 15, EmployID: Ext.getCmp('IL_EmployID').getValue(), client_id: Ext.getCmp('utilization_client').getValue(), utilization_start_date: Ext.getCmp('utilization_start_date').getValue(), utilization_end_date: Ext.getCmp('utilization_end_date').getValue(), project_code: Ext.getCmp('util_project_code').getValue(), tab_type: 'holiday_coverage' };
					Ext.getCmp('holidaylistGridID').getStore().reload();

				},
				failure: function (batch) {
					console.log("Error occured while saving. Please try again.");
					myStore.rejectChanges();
				}
			});
		}
	},


});
