Ext.define('AM.controller.ClientCOR',{
	extend : 'Ext.app.Controller',
	stores : ['Clients','WorProjects','WorkTypeFTE','WorkTypeHourly','WorkTypeVolume','WorkTypeProject','Responsibilities','SupportCoverage','ClientCOR','ClientWOR','Employees','Shifts','ChangeType','ChangeModel','corWorkTypeFTE','corWorkTypeHourly', 'corWorkTypeVolume','DurationCoverage','corWorkTypeProject', 'existingModel','newModel','Deliverymanagers','Engagementmanagers','Clientpartner','Stakeholders','TeamLeader','AssociateManager','WorTypes'],
	views: ['wor_cor.ListCor','wor_cor.AddCor','wor_cor.EditCor','wor_cor.ViewCor','wor_cor.ViewPopup','wor_cor.corFTEList','wor_cor.corHourlyList','wor_cor.corVolumeBasedList','wor_cor.corProjectList', 'wor_cor.existFTEList','wor_cor.existHourlyList','wor_cor.existVolumeBasedList','wor_cor.existProjectList', 'wor_cor.viewPopup','wor_cor.ViewCorFromwor'],

	init : function() {
		this.control({
			'corList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
		});
	},
	
	initCOR : function(cor_status) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		
		AM.app.globals.wor_cor_combostatus = '';
		
		if (cor_status != undefined || cor_status == ''){
			var status = cor_status;
		}
		else{
			var status = 'All';
		}
		
		var myStore = this.getStore('ClientCOR');
		myStore.clearFilter();
		myStore.getProxy().extraParams = {
			'status'	: status, 
		};
		myStore.load();
		var scStore = this.getStore('SupportCoverage');
		scStore.load();
		var clients = this.getView('wor_cor.ListCor').create();

		mainContent.removeAll();
		mainContent.add( clients );
		mainContent.doLayout();
		
		Ext.getCmp('CORStatusComboBox').setValue(status);
	},
	
	onCreateCOR : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var store = this.getStore('Clients');
		var wor = this.getView('wor_cor.AddCor').create();
		
		Ext.getCmp('cor_fte_list').store.removeAll();
		Ext.getCmp('cor_hourly_list').store.removeAll();
		Ext.getCmp('cor_volume_list').store.removeAll(); 
		Ext.getCmp('cor_project_list').store.removeAll();
		
		Ext.getCmp('exist_fte_list').store.removeAll();
		Ext.getCmp('exist_hourly_list').store.removeAll();
		Ext.getCmp('exist_volume_list').store.removeAll();
		Ext.getCmp('exist_project_list').store.removeAll(); 

		var work_order_id = Ext.getCmp('work_order_id').getValue();
		Ext.getCmp('previous_work_order_id').setValue(work_order_id);

		//------------------------------------START----------------------------------//

		var projectTypeStore = this.getStore('WorProjects');
		projectTypeStore.getProxy().extraParams = {'wor_type_id':Ext.getCmp('view_wor_type_id').getValue()};
		projectTypeStore.load();

		AM.app.getStore('TeamLeader').getProxy().extraParams = {
			'hasNoLimit'	:'1',
			'employee_type':'team_leader'
		};
		AM.app.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit'	:'1',
		};

		AM.app.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit'	:	"1", 
			'empTypes'		:   "grade5AndAbove",  
			'filterName'	: 	'first_name'
		};
		var myPanel = Ext.getCmp('ExistCorTabPanel');
		myPanel.items.getAt(0).setDisabled(false);
		myPanel.items.getAt(1).setDisabled(false);
		myPanel.items.getAt(2).setDisabled(false);
		myPanel.items.getAt(3).setDisabled(false);
		Ext.getCmp('addSave').setDisabled(false);
		Ext.getCmp('addSubmit').setDisabled(false);
		Ext.getCmp('cor_client').setDisabled(false);
		// Ext.getCmp('change_in_model').setValue('');
		// Ext.getCmp('addNewModel').hide();
		// Ext.getCmp('addExistingModel').hide();

		var wor_id = Ext.getCmp('wor_id').getValue(); 
		var count_history = Ext.getCmp('count_history').getValue();

		var wor_type = Ext.getCmp('wor_type').getValue();
		Ext.getCmp('previous_wor_type').setValue(wor_type);
		var wor_name = Ext.getCmp('wor_name').getValue();
		Ext.getCmp('previous_wor_name').setValue(wor_name);

		AM.app.getStore('WorkTypeFTE').load({params: {'wor_id':wor_id,'level': count_history} ,
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(0).setDisabled(true);
				}else
				{
					myPanel.setActiveTab(0);
				}

			}
		});
		AM.app.getStore('WorkTypeHourly').load({params: {'wor_id':wor_id,'level': count_history},
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(1).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(1);
				}

			}
		});	
		AM.app.getStore('WorkTypeVolume').load({params: {'wor_id':wor_id,'level': count_history}, 
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(2).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(2);
				}
			}
		});
		AM.app.getStore('WorkTypeProject').load({params: {'wor_id':wor_id,'level': count_history},
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(3).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(3);
				}
			}
		});

		Ext.getCmp('cor_client').setValue(Ext.getCmp('Vclient_name').getValue());
		Ext.getCmp('wor_start_date').setValue(Ext.getCmp('view_start_date').getValue());
		Ext.getCmp('wor_end_date').setValue(Ext.getCmp('view_end_date').getValue());
		Ext.getCmp('cor_cp').setValue(Ext.getCmp('cp_name').getValue());
		Ext.getCmp('cor_em').setValue(Ext.getCmp('em_name').getValue());
		Ext.getCmp('cor_dm').setValue(Ext.getCmp('dm_name').getValue());
		Ext.getCmp('cor_sh').setValue(Ext.getCmp('sh_name').getValue());
		Ext.getCmp('cor_tl').setValue(Ext.getCmp('tl_name').getValue());
		Ext.getCmp('cor_am').setValue(Ext.getCmp('am_name').getValue());

		Ext.getCmp('work_id').setValue(wor_id);
		Ext.getCmp('cor_wor_type_id').setValue(Ext.getCmp('view_wor_type_id').getValue());
		Ext.getCmp('previous_service_type').setValue(Ext.getCmp('view_service').getValue());
		Ext.getCmp('cor_client_id').setValue(Ext.getCmp('client_id').getValue());
		Ext.getCmp('cor_client_partner').setValue(Ext.getCmp('client_partner').getValue());
		Ext.getCmp('cor_engagement_manager').setValue(Ext.getCmp('engagement_manager').getValue());
		Ext.getCmp('cor_delivery_manager').setValue(Ext.getCmp('delivery_manager').getValue());
		Ext.getCmp('cor_stake_holder').setValue(Ext.getCmp('stake_holder').getValue());
		Ext.getCmp('cor_associate_manager').setValue(Ext.getCmp('associate_manager').getValue());
		Ext.getCmp('cor_team_lead').setValue(Ext.getCmp('team_lead').getValue());

		AM.app.globals.cormindate = Ext.getCmp('view_start_date').getValue();
		Ext.getCmp('change_request_date').setMinValue(AM.app.globals.cormindate);

		// Ext.getCmp('addExistingModel').reset();
		// Ext.getCmp('addNewModel').reset();

		Ext.getStore('existingModel').load({
			params:{ 'work_order_id' : Ext.getCmp('previous_work_order_id').getValue()}
		});
		// Ext.getCmp('addExistingModel').bindStore('existingModel');

		Ext.getStore('newModel').load({
			params:{ 'work_order_id' : Ext.getCmp('previous_work_order_id').getValue()}
		});
		// Ext.getCmp('addNewModel').bindStore('newModel');
//------------------------------------END------------------------------------//


AM.app.globals.redir='';		
mainContent.removeAll();
mainContent.add( wor );
mainContent.doLayout();
},

onEditCOR : function(grid, row, col) {

	var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
	Ext.getStore('ClientCOR').load();
	var rec = grid.getStore().getAt(row);
		//console.log(rec);
		AM.app.globals.cormindate = '';
		
		AM.app.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit'	:'1', 
			//'filterName' 	: queryEvent.combo.displayField,
			//'clientStatusCombo':'1',
		};

		AM.app.globals.wor_cor_combostatus = Ext.getCmp('CORStatusComboBox').getValue();
		AM.app.globals.redir='';
		
		this.getStore('corWorkTypeFTE').load({params: {'cor_id':rec.get('cor_id')}});		
		this.getStore('corWorkTypeHourly').load({params: {'cor_id':rec.get('cor_id')}});	
		this.getStore('corWorkTypeVolume').load({params: {'cor_id':rec.get('cor_id')}});
		this.getStore('corWorkTypeProject').load({params: {'cor_id':rec.get('cor_id')}});
		
		rec.data.status = "In-Progress";		
		var wor = Ext.widget('EditCor');	
		
		var myPanel = Ext.getCmp('ExistCorTabPanel');
		this.getStore('WorkTypeFTE').load({params: {'wor_id':rec.get('wor_id'),'level': rec.get('count_history')},
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(0).setDisabled(true);
				}else
				{
					myPanel.setActiveTab(0);
				}
				
			}
		});
		this.getStore('WorkTypeHourly').load({params: {'wor_id':rec.get('wor_id'),'level': rec.get('count_history')},
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(1).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(1);
				}
				
			}
		});	
		this.getStore('WorkTypeVolume').load({params: {'wor_id':rec.get('wor_id'),'level': rec.get('count_history')}, 
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(2).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(2);
				}
			}
		});
		this.getStore('WorkTypeProject').load({params: {'wor_id':rec.get('wor_id'),'level': rec.get('count_history')},
			callback : function(records)
			{
				if(records.length == 0)
				{
					myPanel.items.getAt(3).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(3);
				}
				
			}
		});
		AM.app.getStore('TeamLeader').getProxy().extraParams = {
			'hasNoLimit'	:'1', 
			'employee_type':'team_leader'
		};	

		if((rec.get('associate_manager')!="") && (rec.get('associate_manager')!=null))
		{
			rec.set('associate_manager', rec.get('associate_manager').split(','));
		}
		if((rec.get('team_lead')!="") && (rec.get('team_lead')!=null))
		{
			rec.set('team_lead', rec.get('team_lead').split(','));
		}
		
		Ext.getCmp('editStatus').bindStore(Ext.create('Ext.data.Store', {
			fields: ['Flag', 'Status'],
			data: [
			{'Flag': 'In-Progress', 'Status': 'In-Progress'},
			{'Flag': 'Delete', 'Status': 'Delete'},
			]
		}));
		
		wor.down('form').loadRecord(rec);
		AM.app.globals.cormindate = rec.get('wor_start_date');
		Ext.getCmp('change_request_date').setMinValue(AM.app.globals.cormindate);
		
		grid.getStore().load();
		
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();	
		
		// Hide & show of change model
		if(rec.data.change_in_model == '1') 
		{
			Ext.getCmp('editExistingModel').setVisible(true);    
			Ext.getCmp('editNewModel').setVisible(true);
		}
		else
		{
			Ext.getCmp('editExistingModel').allowBlank = true;
			Ext.getCmp('editNewModel').allowBlank = true;
		}
	},
	
	onViewCOR : function(grid, row, col) {
		
		var rec = grid.getStore().getAt(row);
		
		if(rec.get('stake_holder')!=null)
		{
			rec.set('stake_holder', rec.get('stake_holder').split(','));
		}
		// var statuscombo = Ext.getCmp('CORStatusComboBox').getValue();

		AM.app.globals.wor_cor_combostatus = Ext.getCmp('CORStatusComboBox').getValue();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var view = this.getView('wor_cor.ViewCor').create();
		mainContent.removeAll();
		mainContent.add( view );
		mainContent.doLayout();

		// Hide & show of change model
		/*if(rec.data.change_in_model == '2') 
		{
			rec.data.change_in_model = "No"; 
			Ext.getCmp('viewExistingModel').setVisible(false);    
			Ext.getCmp('viewNewModel').setVisible(false);
		}
		else
		{
			rec.data.change_in_model = "Yes";
			Ext.getCmp('viewExistingModel').setVisible(true);    
			Ext.getCmp('viewNewModel').setVisible(true);
		}*/

		// Hide & show of Save button and Description field
		if(rec.data.status=="In-Progress")
		{   
			Ext.getCmp('viewSaveButton').setVisible(false);    
			Ext.getCmp('ViewCORDescription').setVisible(false); 
			Ext.getCmp('inCorStatus').setVisible(true);		   
		}
		else if(rec.data.status=="Open")
		{
			Ext.getCmp('corOpenStatus').setVisible(true);
			Ext.getCmp('viewSaveButton').setVisible(false);
		}
		else if(rec.data.status=="Closed" || rec.data.status=="Deleted")
		{
			Ext.getCmp('inCorStatus').setVisible(true);
			Ext.getCmp('viewSaveButton').setVisible(false);
			Ext.getCmp('ViewCORDescription').setVisible(true);
			Ext.getCmp('ViewCORDescription').setReadOnly(true);
		}

		if(Ext.util.Cookies.get("grade") < 4)
		{
			Ext.getCmp('corOpenStatus').setDisabled(true);
		}

		view.setTitle('View '+rec.data.change_order_id+' Details');

		var myPanel = Ext.getCmp('myCorTabPanel');

		myPanel.add({
			title: 'FTE',
			items : [{        
				xtype:AM.app.getView('wor_cor.corFTEList').create(),
				hidden:false
			}]
		});
		Ext.getCmp('corAddFTE').setVisible(false);

		myPanel.add({
			title: 'Hourly',
			items : [{        
				xtype:AM.app.getView('wor_cor.corHourlyList').create(),
				hidden:false
			}]
		});

		myPanel.add({
			title: 'Volume Based',
			items : [{        
				xtype:AM.app.getView('wor_cor.corVolumeBasedList').create(),
				hidden:false
			}]
		});

		myPanel.add({
			title: 'Project',
			items : [{        
				xtype:AM.app.getView('wor_cor.corProjectList').create(),
				hidden:false
			}]
		});
		
		view.down('form').loadRecord(rec);

		this.getStore('corWorkTypeFTE').load({
			params: {'cor_id':rec.get('cor_id')},
			callback : function(records){
				if(records.length == 0)
				{
					myPanel.items.getAt(0).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(0);
				}
			}
		});		
		this.getStore('corWorkTypeHourly').load({
			params: {'cor_id':rec.get('cor_id')},
			callback : function(records){
				if(records.length == 0)
				{
					myPanel.items.getAt(1).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(1);
				}
			}
		});	
		this.getStore('corWorkTypeVolume').load({
			params: {'cor_id':rec.get('cor_id')},
			callback : function(records){
				if(records.length == 0)
				{
					myPanel.items.getAt(2).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(2);
				}
			}
		});
		this.getStore('corWorkTypeProject').load({
			params: {'cor_id':rec.get('cor_id')},
			callback : function(records){
				if(records.length == 0)
				{
					myPanel.items.getAt(3).setDisabled(true);
				}
				else
				{
					myPanel.setActiveTab(3);
				}
			}
		});

		/* disable Add Request Button in COR view */
		Ext.getCmp('cor_fte_list').tools['0']="";
		Ext.getCmp('cor_hourly_list').tools['0']="";
		Ext.getCmp('cor_project_list').tools['0']=""
		Ext.getCmp('cor_volume_list').tools['0']="" 
	},
	
	onViewCORFromWor : function(corid) {
		delete this.getStore('ClientCOR').getProxy().extraParams;
		var rec1 = [];
		
		var myStore = this.getStore('ClientCOR');
		myStore.getProxy().extraParams = {
			'cor_id'	: corid,
			'hasNoLimit' : 1
		};
		myStore.load();
		
		myStore.each(function(rec) {
			var recData = rec.getData();
			
			if (recData['cor_id'] === corid) {
				rec1.push(recData);
			}
		});
		
		if(rec1.length != 0)
		{
			var view = Ext.widget('ViewCorFromwor');
			
			view.setTitle('View '+rec1[0].change_order_id+' Details');
			Ext.getCmp('change_order_id').setValue(rec1[0].change_order_id);
			Ext.getCmp('previous_work_order_id').setValue(rec1[0].previous_work_order_id);
			Ext.getCmp('change_request_date').setValue(rec1[0].change_request_date);
			Ext.getCmp('client_name').setValue(rec1[0].client_name);
			Ext.getCmp('view_cor_service_type').setValue(rec1[0].name);
			Ext.getCmp('view_cor_cp_name').setValue(rec1[0].cp_name);
			Ext.getCmp('view_cor_em_name').setValue(rec1[0].em_name);
			Ext.getCmp('view_cor_dm_name').setValue(rec1[0].dm_name);
			Ext.getCmp('view_cor_am_name').setValue(rec1[0].am_name);
			Ext.getCmp('view_cor_tl_name').setValue(rec1[0].tl_name);
			Ext.getCmp('view_cor_sh_name').setValue(rec1[0].sh_name);
			Ext.getCmp('inCorStatus').setValue(rec1[0].status);

			Ext.getCmp('view_cor_wor_type').setValue(rec1[0].wor_type);
			Ext.getCmp('view_cor_wor_name').setValue(rec1[0].wor_name);
			
			var myPanel = Ext.getCmp('myCorTabPanel123');

			myPanel.add({
				title: 'FTE',
				items : [{        
					xtype:AM.app.getView('wor_cor.corFTEList').create(),
					hidden:false
				}]
			});
			Ext.getCmp('corAddFTE').setVisible(false);

			myPanel.add({
				title: 'Hourly',
				items : [{        
					xtype:AM.app.getView('wor_cor.corHourlyList').create(),
					hidden:false
				}]
			});

			myPanel.add({
				title: 'Volume Based',
				items : [{        
					xtype:AM.app.getView('wor_cor.corVolumeBasedList').create(),
					hidden:false
				}]
			});

			myPanel.add({
				title: 'Project',
				items : [{        
					xtype:AM.app.getView('wor_cor.corProjectList').create(),
					hidden:false
				}]
			});
			
			var activeTab = "";
			this.getStore('corWorkTypeFTE').load({
				params: {'cor_id':corid},
				callback : function(records){
					if(records.length == 0)
					{
						myPanel.items.getAt(0).setDisabled(true);
					}
					else
					{
						myPanel.setActiveTab(0);
					}
				}
			});		
			this.getStore('corWorkTypeHourly').load({
				params: {'cor_id':corid},
				callback : function(records){
					if(records.length == 0)
					{
						myPanel.items.getAt(1).setDisabled(true);
					}
					else
					{
						myPanel.setActiveTab(1);
					}
				}
			});
			this.getStore('corWorkTypeVolume').load({
				params: {'cor_id':corid},
				callback : function(records){
					if(records.length == 0)
					{
						myPanel.items.getAt(2).setDisabled(true);
					}
					else
					{
						myPanel.setActiveTab(2);
					}
				}
			});
			this.getStore('corWorkTypeProject').load({
				params: {'cor_id':corid},
				callback : function(records){
					if(records.length == 0)
					{
						myPanel.items.getAt(3).setDisabled(true);
					}
					else
					{
						myPanel.setActiveTab(3);
					}
				}
			});


			/* disable Add Request Button in COR pop up view */
			Ext.getCmp('cor_fte_list').tools['0']="";
			Ext.getCmp('cor_hourly_list').tools['0']="";
			Ext.getCmp('cor_project_list').tools['0']=""
			Ext.getCmp('cor_volume_list').tools['0']=""  

		}
		else
		{
			return false;
		}
	},
	
	onViewPopup : function() {
		var view = Ext.widget('ViewPopup');
		view.setTitle('Add Client NRR Details');
	},
	
	onSaveCOR : function(form, status) 
	{
		var myStore = this.getStore('ClientCOR');
		var record = form.getRecord(), values = form.getValues();
		var cor_id = values['cor_id'];
		values['status'] = status;
		console.log(values);
		
		//var index = Ext.StoreMgr.lookup("Clients").findExact('id',values['client_name']);
		//var reca = Ext.StoreMgr.lookup("Clients").getAt(index);
		
		if(Ext.getCmp('cor_fte_list').store.data.items.length != 0 || Ext.getCmp('cor_hourly_list').store.data.items.length != 0 || Ext.getCmp('cor_volume_list').store.data.items.length != 0 || Ext.getCmp('cor_project_list').store.data.items.length != 0)
		{			
			var fte_list = Ext.encode(Ext.pluck(Ext.getCmp('cor_fte_list').store.data.items, 'data'));
			var hourly_list = Ext.encode(Ext.pluck(Ext.getCmp('cor_hourly_list').store.data.items, 'data'));
			var volume_list = Ext.encode(Ext.pluck(Ext.getCmp('cor_volume_list').store.data.items, 'data'));
			var project_list = Ext.encode(Ext.pluck(Ext.getCmp('cor_project_list').store.data.items, 'data'));
			myStore.getProxy().extraParams = {'fte_details':fte_list, 'hourly_details':hourly_list, 'volume_list' : volume_list, 'project_list' : project_list,'client_name' :values['client'] };
		}
		else
		{
			Ext.Msg.alert("Alert", "Please Select Atleast one Model");
			return false;
		}

		if(cor_id != null && cor_id != "")
		{
			form.url = myStore.getProxy().api.create;
			var titleText = 'Updated';
			myStore.add(values);
		}
		else
		{
			form.url = myStore.getProxy().api.create;
			var titleText = 'Added';
			myStore.add(values);
		}

		myStore.sync({
			success: function (batch, operations) {
				var jsonResp = batch.proxy.getReader().jsonData;
				if(jsonResp.success) {

					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						cloasable: false,
						fn: function(buttonId) {
							if(jsonResp.exceed != 1) 
							{
								if (buttonId === "ok") 
								{
									Ext.getCmp('cor_hourly_list').store.removeAll();
									Ext.getCmp('cor_fte_list').store.removeAll();
									Ext.getCmp('cor_volume_list').store.removeAll();
									Ext.getCmp('cor_project_list').store.removeAll();   
									if(AM.app.globals.redir == '')   {                    	
										AM.app.getController('ClientCOR').initCOR();
									}
									else
									{
										AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus);
									}
								}
							}
							else
							{
								if(cor_id != null && cor_id != "")
								{
									Ext.getCmp('editSave').setDisabled(false);
									Ext.getCmp('editSubmit').setDisabled(false);
								}
								else
								{
									Ext.getCmp('addSubmit').setDisabled(false);
									Ext.getCmp('addSave').setDisabled(false);
								}
								return false;
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								Ext.getCmp('cor_hourly_list').store.removeAll();
								Ext.getCmp('cor_fte_list').store.removeAll();
								Ext.getCmp('cor_volume_list').store.removeAll();
								Ext.getCmp('cor_project_list').store.removeAll();  
								if(AM.app.globals.redir=='corredir')   {                    	
									AM.app.getController('ClientCOR').initCOR();
								}
								else
								{
									AM.app.getController('ClientWOR').initWOR(AM.app.globals.wor_cor_combostatus);
								}
							}
						}
					});
				}
			}, 
			failure: function (batch) {
				console.log("Error occured while Processing data. Please try again.");
				myStore.rejectChanges();
			}
		});		
	},
	
	onStatusChange: function(combo){
		var store = this.getStore('ClientCOR');
		store.getProxy().extraParams = {'status': combo.getValue()};
		store.load();
	},
	
	onDeleteCOR: function(form)
	{
		var values = form.getValues();
		if(values['ViewCORDescription']=="")
		{
			Ext.Msg.alert("Alert", "Please enter description");
			return false;
		}
		else
		{
			var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
			myMask.show();
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/Clientcor/saveStatus/',
				method: 'POST',
				params: {'cor_id':values['cor_id'], 'status':'Deleted', 'description':values['description']},
				success: function(responce, opts){
					var jsonResp = Ext.decode(responce.responseText);
					if(jsonResp.success)
					{ 
						myMask.hide();
						Ext.MessageBox.show({
							title: jsonResp.title,
							msg: jsonResp.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									AM.app.getController('ClientCOR').initCOR();
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: 'Error',
							msg: jsonResp.msg,
							icon: Ext.MessageBox.ERROR,
							buttons: Ext.Msg.OK,
						});
					}
				}
			});
		}
	},

	saveStatus: function(form,status)
	{
		var values = form.getValues();
		if(values['ViewCORDescription']=="")
		{
			Ext.Msg.alert("Alert", "Please enter description");
			return false;
		}
		else
		{
			var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
			myMask.show();
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/Clientcor/saveStatus/',
				method: 'POST',
				params: {'cor_id':values['cor_id'], 'status':'Closed', 'description':values['description']},
				success: function(responce, opts){
					var jsonResp = Ext.decode(responce.responseText);
					if(jsonResp.success)
					{ 
						myMask.hide();
						Ext.MessageBox.show({
							title: jsonResp.title,
							msg: jsonResp.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									AM.app.getController('ClientCOR').initCOR();
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: 'Error',
							msg: jsonResp.msg,
							icon: Ext.MessageBox.ERROR,
							buttons: Ext.Msg.OK,
						});
					}
				}
			});
		}
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var myStore = this.getStore('ClientCOR'); 

		myStore.filters.clear();
		myStore.filter({
			property: 'cor.change_order_id,wr.wor_name,c.`client_name`, cor.change_request_date,cor.created_by',
			value: t.getValue()
		});
		myStore.load();				
	},

	onTriggerClear : function() {
		var myStore = this.getStore('ClientCOR');
		myStore.getProxy().extraParams = {'CORStatusComboBox': Ext.getCmp('CORStatusComboBox').getValue()};
		myStore.clearFilter();
		myStore.load();
	},
});
