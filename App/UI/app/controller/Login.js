Ext.define('AM.controller.Login', {
	extend : 'Ext.app.Controller',
	stores: ['Login'],
    views:[
        'Login', 'Viewport', 'NotifyMenu'
    ],
    requires: [
        'AM.util.MD5',
		'AM.util.Base64'
	],
    refs:[{
		ref:'loginwindow',
		selector:'loginwindow'
	}],
	
	init:function () {
        this.control({
            'button[action=login]':{
                click:this.loginmozFunc
            },
            '#loginwindow textfield':{
                specialkey:this.keyenter
            },
            'button[action=logout]':{
                click:this.logout
            }
        });
    },
    
    checkLogin : function() 
	{
		if(Ext.util.Cookies.get('username') != '' && Ext.util.Cookies.get('username') != null && Ext.util.Cookies.get('Grade') != null && Ext.util.Cookies.get('employee_id') != null && Ext.util.Cookies.get('is_onshore') != 1)
		{	
			this.userSppecificDashboard();
			return true;
		}
		else 
		{
			return false;
		}
    },
	
	checkSession : function() 
	{
		window.setInterval(function() {
			if(Ext.util.Cookies.get('employee_id')==null)
			{
				Ext.MessageBox.show({
					title: 'Logged Out',
					closeAction:'hide',
					icon: Ext.MessageBox.INFO,
					msg: 'Your session has expired. Please login again.',
					buttons: Ext.Msg.OK,
					closable : false,
					fn: function(buttonId) {
						if (buttonId === "ok") 
						{
							Ext.util.Cookies.clear('username');
							Ext.util.Cookies.clear('token');
							Ext.util.Cookies.clear('Grade');
							Ext.util.Cookies.clear('grade');
							Ext.util.Cookies.clear('employee_id');
							Ext.util.Cookies.clear('gender');
							Ext.util.Cookies.clear('DsnName');
							Ext.util.Cookies.clear('VertName');
							Ext.util.Cookies.clear('VertId');
							Ext.util.Cookies.clear('VertIDs');
							Ext.util.Cookies.clear('ClientId');
							Ext.util.Cookies.clear('WorId');
							Ext.util.Cookies.clear('loginType');
							Ext.util.Cookies.clear('Uemail');
							Ext.util.Cookies.clear('LoginId');
							Ext.util.Cookies.clear('Notifications');
							Ext.util.Cookies.clear('view_popup');	
							Ext.util.Cookies.clear('docAllow');	
							
							var viewport = Ext.ComponentQuery.query('viewport')[0];
							var lay = viewport.getLayout();
							lay.setActiveItem(0);
							
							Ext.get('card-0').hide();
							
							location.reload(true);
						}
					}
				});
			}
        }, 30000);
    },
	
    index:function () 
	{
		var allow = true ;
		if((window.location.href.indexOf("localhost") > 0) ||(window.location.href.indexOf(AM.app.globals.qaUrl) > 0) || (window.location.href.indexOf(AM.app.globals.devUrl) > 0)){
			allow = false ;
		}
		
		if (Ext.isGecko || allow) 
		{
			Ext.get('card-0').hide();
			var loginWin = Ext.create('AM.view.Login');
			loginWin.show();
		} 
		else 
		{
			Ext.MessageBox.prompt('Login', 'Please enter username:', this.loginFunc);
		}
	},

    loginFunc:function (btn, txt) {
		if(btn=="ok")
		{
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/Authenticate/authentication_details',
				method: 'GET',
				scope: this,
				async:false,
				params: {username: txt},
				success: function(response) {
					var loginData = Ext.JSON.decode(response.responseText);
					var Verticals = '';
					var  vertical = '';
					if(loginData.success) 
					{
						console.log(loginData);
						AM.app.globals.UserDashboard = loginData.UserDashboard;
						// this.getDashboardtoolIcon();
						AM.app.globals.DashboardToolsIcons = loginData.DashboardTools;
						
						var UserName 	= loginData.UserName;
						var Grade 		= loginData.Grade;
						var EmployID 	= loginData.employee_id;
						var DsnName 	= loginData.DesgnName;
						var Designation = loginData.Designation;
						var ClientId	= loginData.ClientId;
						var gender	= loginData.gender;
						var WorId		= loginData.WorId;
						var approver		= loginData.approver;
						var vertName 	= (loginData.VertName == '' || loginData.VertName == null) ? '' : loginData.VertName;	
						var VertId 		= loginData.VertId;
						var PodName 		= loginData.pod_name;
						var Notifications = loginData.Notifications;
						var docAllow = loginData.docAllow;
						var is_onshore = loginData.is_onshore;
						
						Ext.util.Cookies.set('username',UserName);
						Ext.util.Cookies.set('Grade',Grade);
						Ext.util.Cookies.set('DsnName',DsnName);
						Ext.util.Cookies.set('Designation',Designation);
						Ext.util.Cookies.set('VertName',vertName);
						Ext.util.Cookies.set('PodName',PodName);
						Ext.util.Cookies.set('gender',gender);
						Ext.util.Cookies.set('VertId',VertId);
						Ext.util.Cookies.set('ClientId',ClientId);
						Ext.util.Cookies.set('WorId',WorId);
						Ext.util.Cookies.set('approver',approver);
						Ext.util.Cookies.set('Notifications',Notifications);
						Ext.util.Cookies.set('docAllow',docAllow);
						Ext.util.Cookies.set('is_onshore',is_onshore);
						
						AM.app.globals.empid = EmployID;
						
						
						titleName=DsnName;
						if(DsnName.length > 25)
							DsnName = DsnName.substring(0, 22)+'...';
						
						//Ext.getCmp("logInUserName").update(" <b>" + '<span style="float: left">' + UserName +'</span></b>');
						Ext.getCmp("logInCont").update('<span class="notiCnt">' + Ext.util.Cookies.get("Notifications") + '</span>');
						
						AM.app.getController('Menu').viewMenu();
						AM.app.getController('Menu').viewNotifyMenu();
						//AM.app.getController('Notification').viewPopNotify();
						AM.app.getController("Login").checkSession();
						
						var viewport = Ext.ComponentQuery.query('viewport')[0];
						var lay = viewport.getLayout();
						lay.setActiveItem(1);
						
						if(Ext.util.Cookies.get('Grade')>2)
						{
							AM.app.getController('Dashboard').viewContent();
						}
						else if(Ext.util.Cookies.get('Grade')<3 && Ext.util.Cookies.get('is_onshore')==1)
						{
							AM.app.getController('TimesheetOnshore').viewContent();
						}
						else
						{
							AM.app.getController('Timesheet').viewContent();
						}
					} 
					else 
					{
						Ext.Msg.show({
							title:'Login Message',
							id:'alr_id',
							msg: loginData["msg"],
							buttons: Ext.Msg.OK,
							animEl: 'elId'
						});
					}
				},
				failure: function(response) {
					Ext.Msg.alert('Problem in connecting to server.');
				}
			});
		}
		else 
		{
			Ext.MessageBox.prompt('Login', 'Please enter username:', this.loginFunc);
		}
    },
    
    loginmozFunc:function() 
	{
		var myMozUsername,myMozSuccess; 
		var win = Ext.getCmp('loginwindow');
		var form = Ext.getCmp('loginform');
		var values = form.getValues();
		var encodPasswrd  = AM.util.Base64.encode(values['password']);
		var encodUsername = AM.util.Base64.encode(values['userName']);
		
		if(values['userName'] !="" && values['password'] !="") 
		{
			Ext.getCmp('loginButton').setIconCls('my-loader');
			Ext.getCmp('loginButton').disable();
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/Authenticate/authentication_details',
				method: 'POST',
				params: {
					'username': values['userName'],
					'password':encodPasswrd
				},
				scope: this,
				success: function(response) 
				{
					AM.app.globals.upwd = encodPasswrd;
					AM.app.globals.uname = values['userName'];
					var Verticals = '';
					var  vertical = '';

					Ext.getCmp('loginButton').enable();
					
					var myObject = Ext.JSON.decode(response.responseText);
					AM.app.globals.UserDashboard = myObject["UserDashboard"];
					AM.app.globals.DashboardToolsIcons = myObject["DashboardTools"];
					
					if(typeof myObject["FullName"] == "undefined" || myObject["FullName"] == null)
						myMozFullname =  myObject["UserName"];
					else
						myMozFullname =  myObject["FullName"];
					if(myObject["VertName"]!=undefined)
					{
						if (myObject["VertName"].indexOf(",") > 0)
						{
							Verticals = myObject["VertName"];
							var Verticaldata = new Array();
							Verticaldata = myObject["VertName"].split(",");
							vertical = Verticaldata[0];
							myObject["VertName"] = vertical;
						}
						else
							Verticals = myObject["VertName"];
					}
					myMozLogo	  = myObject["Logo"];
					myMozDsnName  = myObject["DesgnName"];
					myMozVertName = (myObject["VertName"] == '' || myObject["VertName"] == null) ? '' : myObject["VertName"];
					myMozLoginId  = myObject["LoginId"];
					myMozPodName  = myObject["pod_name"];
					myMozEmail    = myObject["Email"];
					myMozgender    = myObject["gender"];
					myMozEmpId    = myObject["employee_id"];
					myMozSuccess  = myObject["success"];
					myMozGrade    = myObject["Grade"];
					myMozVert     = myObject["VertId"];
					myMozClientId = myObject["ClientId"];
					myMozWorId    = myObject["WorId"];
					myMozapprover    = myObject["approver"];
					docAllow    = myObject["docAllow"];
					is_onshore    = myObject["is_onshore"];
					
					myMozNotifications = myObject["Notifications"];
					AM.app.globals.uemail = myMozEmpId;
					AM.app.globals.empid = myMozEmpId;
					
					if(myMozLoginId != undefined)
						AM.app.globals.uname = AM.util.Base64.encode(myMozLoginId);
					
					if (myMozSuccess) 
					{
						Ext.util.Cookies.set('username', myMozFullname);
						Ext.util.Cookies.set('LoginId', myMozLoginId);
						Ext.util.Cookies.set('Uemail', myMozEmail);
						Ext.util.Cookies.set('gender', myMozgender);
						Ext.util.Cookies.set('Grade', myMozGrade);
						Ext.util.Cookies.set('DsnName', myMozDsnName);
						Ext.util.Cookies.set('Podname', myMozPodName);
						Ext.util.Cookies.set('VertName', myMozVertName);
						Ext.util.Cookies.set('VertId', myMozVert);
						Ext.util.Cookies.set('ClientId', myMozClientId);
						Ext.util.Cookies.set('WorId', myMozWorId);
						Ext.util.Cookies.set('approver', myMozapprover);
						Ext.util.Cookies.set('Notifications', myMozNotifications);
						Ext.util.Cookies.set('docAllow', docAllow);
						Ext.util.Cookies.set('is_onshore', is_onshore);
						AM.app.globals.uemail = myMozEmail;	
						AM.app.globals.empid = myMozEmpId;															
						AM.app.globals.uname = AM.util.Base64.encode(myMozLoginId);
						titleName=myMozDsnName;
						if(myMozDsnName.length > 25)
							myMozDsnName = myMozDsnName.substring(0, 22)+'...';
						//Ext.getCmp("logInUserName").update(" <b>" + '<span style="float: left">'+ myMozFullname +'</span></b>');
						Ext.getCmp("logInCont").update('<span class="notiCnt">' + Ext.util.Cookies.get("Notifications") + '</span>');
						
						AM.app.getController('Menu').viewMenu();
						AM.app.getController('Menu').viewNotifyMenu();
						AM.app.getController("Login").checkSession();
			
						var viewport = Ext.ComponentQuery.query('viewport')[0];
						var lay = viewport.getLayout();
						lay.setActiveItem(1);
						win.hide();
						
						if(Ext.util.Cookies.get('Grade')>2)
						{
							AM.app.getController('Dashboard').viewContent();
						}
						else
						{
							AM.app.getController('Timesheet').viewContent();
						}
					}
					else
					{
						Ext.getCmp('loginButton').setIconCls('form-login-icon-login');
						Ext.Msg.show({
						   title:'Login Message',
						   id:'alr_id',
						   msg: myObject["msg"],
						   buttons: Ext.Msg.OK,
						   animEl: 'elId'
						});
					}
						
					//console.log('email='+AM.app.globals.uemail);
					AM.app.globals.tickSystemArgs = "?eid="+AM.app.globals.uemail+"&pwd="+AM.app.globals.upwd;
					AM.app.globals.TMSSystemArgs = "/"+AM.app.globals.uname+"/"+AM.app.globals.upwd;
					AM.app.globals.TaxdecArgs= "/"+AM.app.globals.uname+"/"+AM.app.globals.upwd;
					AM.app.globals.tickSystem += AM.app.globals.tickSystemArgs ;
					AM.app.globals.tickSystemSCP += AM.app.globals.tickSystemArgs ;
					AM.app.globals.tmsUrl+= AM.app.globals.TMSSystemArgs ;
					AM.app.globals.taxdecUrl+= AM.app.globals.TaxdecArgs ;
				},
				failure: function(response) 
				{
					Ext.getCmp('loginButton').setIconCls('form-login-icon-login');
					Ext.Msg.alert('INVALID USERNAME OR PASSWORD');
				}
			});
			
		}
		else
		{
			win.down('form').query("field{isValid()==false}");
		}
	},
	
    keyenter:function (item, event) {
        if (event.getKey() == event.ENTER) {
            this.loginmozFunc();
        }

    },
    
    logout:function (button) {
        Ext.log('Logout user')
        var viewport = Ext.ComponentQuery.query('viewport')[0];
        var lay = viewport.getLayout();
        lay.setActiveItem(0);
		
		if(Ext.util.Cookies.get('loginType') == 'Client')
		{
			Ext.get('card-0Client').hide();
		}
		else
		{
			Ext.get('card-0').hide();
		}
		
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Authenticate/logout',
			method: 'GET',
			scope: this,
			success: function(response) {
				Ext.MessageBox.show({
					title: 'Logged Out',
					closeAction:'hide',
					icon: Ext.MessageBox.INFO,
					msg: 'Logging you out..,',
				});
			
				Ext.util.Cookies.clear('username');
				Ext.util.Cookies.clear('token');
				Ext.util.Cookies.clear('Grade');
				Ext.util.Cookies.clear('grade');
				Ext.util.Cookies.clear('employee_id');
				Ext.util.Cookies.clear('DsnName');
				Ext.util.Cookies.clear('VertName');
				Ext.util.Cookies.clear('gender');
				Ext.util.Cookies.clear('VertId');
				Ext.util.Cookies.clear('PodName');
				Ext.util.Cookies.clear('VertIDs');
				Ext.util.Cookies.clear('ClientId');
				Ext.util.Cookies.clear('WorId');
				Ext.util.Cookies.clear('loginType');
				Ext.util.Cookies.clear('Uemail');
				Ext.util.Cookies.clear('LoginId');
				Ext.util.Cookies.clear('Notifications');
				Ext.util.Cookies.clear('approver');
				Ext.util.Cookies.clear('view_popup');
				Ext.util.Cookies.clear('docAllow');
				Ext.util.Cookies.clear('isOnshore');
				
				
				
				window.setInterval(function() {
					var viewport = Ext.ComponentQuery.query('viewport')[0];
					var lay = viewport.getLayout();
					lay.setActiveItem(0);
					
					Ext.get('card-0').hide();
					
					location.reload(true);
				}, 2000);
			}
		});
    },
	
	myCallbackFunction : function() 
	{
		window.close();
		location.reload(true);
	},
	
	/* Loading user dashboard */
	userSppecificDashboard: function()
	{
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Employees/loggedinUserDashboard/?v='+AM.app.globals.version,
			scope: this,
			async: false,
			success: function(response) {
				var myObject = response.responseText;
				AM.app.globals.UserDashboard = myObject;
			}
		});	
		this.getDashboardtoolIcon();
	},
	
	/* Loading user dashboard icon */
	getDashboardtoolIcon: function()
	{
    	Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Employees/loggedinUserDataIcons/?v='+AM.app.globals.version,
			scope: this,
			async: false,
			success: function(response) 
			{
				var myObject = response.responseText;
				AM.app.globals.DashboardToolsIcons = myObject;
			}
		});
    },
	
	clientLogo:function(logo){
		
		if(Ext.util.Cookies.get('ClientLogo') == '' || Ext.util.Cookies.get('ClientLogo') == 'null')
		{
			Ext.getCmp('logImgClient').update('<div class = "tooltip"><img height="53"  width="100" src="'+AM.app.globals.uiPath+'resources/images/nologo.png"><span class = "callout">'+Ext.util.Cookies.get('Client_Name')+'</span></div>');
		}
		else
		{
			Ext.getCmp('logImgClient').update('<img alt="" height="53" width="100" src="'+AM.app.globals.uiPath+'resources/uploads/client/'+logo+'">');
		}
		
	},
	
});
