Ext.define('AM.controller.Utilization',{
	extend : 'Ext.app.Controller',
	stores : ['Utilization','LogInViewerClients','LogInViewerEmployees','Employees','roleCode','taskCode','subTaskCode','workOrder','ProjectTypes','clientattributes','requestRegion'],
	views: ['utilization.UtilizationGrid', 'utilization.RoleCodeHelp','utilization.TaskCodeHelp','utilization.List','utilization.Add','utilization.importXls'],
	
	init : function() {
		this.control({
			'opsDataList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
			'utilizationList' : {
				'saveUtilization' : this.saveUtilization,
				'chgDisplayContent' : this.onChgSearch,
				'extParams' : this.sendExtParams
			},
			'EmpprocessAddEditEffort' : {
				'addEmppecificEffort' : this.onSaveEmpProcessEffort,
				'chgSetValues' : this.setEmpProsFields
			},
			'TaskCodeHelp #gridTrigger' : {
				keyup : this.taskcodeonTriggerKeyUp,
				triggerClear : this.taskcodeonTriggerClear
			},
			'RoleCodeHelp #gridTrigger' : {
				keyup : this.projcodeonTriggerKeyUp,
				triggerClear : this.projcodeonTriggerClear
			}
		});
	},
	
	taskcodeonTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('taskCode');
		store.filters.clear();
		store.filter({
			property: 'task_code, task_name, request_type, complexity, priority',
			value: t.getValue()
		});
	},
	
	taskcodeonTriggerClear : function() {
		var store = this.getStore('taskCode');
		store.clearFilter();
	},
	
	projcodeonTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('roleCode');
		store.filters.clear();
		store.filter({
			property: 'p.name, client_name, shift_code, support_type, jr.role_type, p.model',
			value: t.getValue()
		});
	},
	
	projcodeonTriggerClear : function() {
		var store = this.getStore('roleCode');
		store.clearFilter();
	},
	
	onUtilAdd : function(client_id,wor_id) {
		if(client_id == '' || client_id == null)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Client",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			return false;
		}
		if(wor_id == '' || wor_id == null)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Work Order",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			return false;
		}

		var view = Ext.widget('utilizationAdd');

		if(client_id == 221)
		{
			Ext.getCmp('client_hrs').setDisabled(true);
		}
		
		Ext.getCmp('client_id').setValue(client_id);
		Ext.getCmp('utilization_id').setValue('');
		Ext.getCmp('wor_id').setValue(wor_id);
		
		Ext.getCmp('cloneButtonID').setVisible(false); 
		
		var index = Ext.StoreMgr.lookup("workOrder").findExact('wor_id',wor_id);
		var reca = Ext.StoreMgr.lookup("workOrder").getAt(index);
		Ext.getCmp('task_date').setMinValue(reca.data.start_date);
	},
	
	viewContent : function(clientID,wor_id,date) {
		//to auto populate first value
		if(clientID==undefined)
		{
			Ext.getStore('workOrder').load();
			
			Ext.getStore('LogInViewerClients').load({
				params:{
					'hasNoLimit':'1',
					'comboClient':'1',
					'isUtilzation':'1',
				},
				callback:function(records, options, success) {
					if (success) 
					{
						var clientID = records[0].data.client_id;
						Ext.getCmp('utilization_client').setValue(clientID);

						Ext.getStore('workOrder').load({
							params:{'hasNoLimit':"1", 'utilCombo':'1','comboEmployee':1,'comboClient':clientID},
							callback:function(worrecords, options, success)
							{
								if(worrecords.length > 0)
								{
									wor_id = worrecords[0].data.wor_id;
									Ext.getCmp('utilization_wor_id').setValue(wor_id).setDisabled(false);
									AM.app.getController('Utilization').viewContent(clientID,wor_id,Ext.getCmp('utilization_date').getValue());
								}
							}
						});
					}
				}
			});
		}
		// console.log(clientID,wor_id,date);
		AM.app.globals.empid = Ext.util.Cookies.get('employee_id');// Added since the Global varible not assigned in Menu level.
		
		this.getStore('Utilization').rejectChanges();
		
		delete this.getStore('Utilization').getProxy().extraParams;
		var store = this.getStore('Utilization');
		store.getProxy().extraParams = {'client_id': clientID,'date': date,'wor_id': wor_id};
		store.clearFilter();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var utilList = this.getView('utilization.List').create({client_id: clientID,  date: date,'wor_id': wor_id});
		mainContent.removeAll();
		mainContent.add( utilList );
		mainContent.doLayout();
		
		if(clientID != '' || clientID != null || clientID != undefined )
		{
			Ext.getCmp('utilization_client').setValue(clientID);
			if(wor_id == '' || wor_id == null  || wor_id == undefined )
			{
				Ext.getCmp('utilization_wor_id').setDisabled(true);
				Ext.getCmp('utilization_wor_id').setValue('');
			}
			else
			{
				Ext.getCmp('utilization_wor_id').setDisabled(false);
				Ext.getCmp('utilization_wor_id').setValue(wor_id);
				Ext.getCmp('utilization_date').setValue(date);
			}
		}

		var importUtilClients = AM.app.globals.importUtilClients;

		var showImportButton = importUtilClients.includes(clientID);

		if(showImportButton) 
		{
			Ext.getCmp('UploadExcel').show();
		} 
		else 
		{
			Ext.getCmp('UploadExcel').hide();
		}
	},

	
	onDownload : function(client_id,dashboard) {
		
		if(dashboard==="Template")
			paramess="Template";
		else var paramess="";
		
		var clientComboId = client_id;
		var utilization_date = Ext.getCmp('utilization_date').getValue();
		
		if( AM.app.globals.utilEmployIDs!="" && AM.app.globals.utilEmployIDs !=Ext.util.Cookies.get('employee_id'))
		{
			var EmployID = AM.app.globals.utilEmployIDs;
		}
		else
		{
			var EmployID = '';
		}
		
		if(isNaN(client_id))
			clientComboId = AM.app.globals.utilProcess;
		
		Ext.Ajax.request({
			url : AM.app.globals.appPath+'index.php/utilization/downloadCsvTemplate/',
			method: 'POST',
			params: {
				Export:paramess,
				client_id: clientComboId,
				utilization_date: utilization_date,
				TeamMemeber :EmployID,
			},
			success: function (batch, operations) {
				var jsonResp = Ext.decode(batch.responseText); 
				if(jsonResp.success)
				{
					if(paramess=="Template")
					{
						window.location = AM.app.globals.appPath+'download/UtilizationTemplate.csv';
					}
					else
					{
						window.location = AM.app.globals.appPath+'download/Utilization.csv';
					}
				}
			}
		});
	},
	
	
	//function to edit employee details
	onEditUtilization : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);

		 console.log(rec);
		
		var formObj = [];
		
		var view = Ext.widget('utilizationAdd');
		view.setTitle("Edit/Clone Utilization");

		var obj = rec.data.AdditionalAttributes;
		var countElem = rec_length = 0;
		
		if(rec.data.fk_client_id == 221)
		{
			Ext.getCmp('client_hrs').setValue('');
			Ext.getCmp('client_hrs').setDisabled(true);
		}

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/utilization/ClientAttributes/',
			method: 'GET',
			params: {'client_id':rec.data.fk_client_id,'wor_id':rec.data.fk_wor_id,'role_id':rec.data.fk_role_id},
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);
				records = jsonResp.data;
				rec_length = records.length; 

				var date = new Date(); 
				startMinDate = Ext.Date.add(date, Ext.Date.MONTH, -2); 
				today = Ext.Date.add(date);
				
				for(var i = 0;  i < rec_length; i++) 
				{
					countElem++;
					var formElem = new Array();

					feildvalue = 'addtional_'+records[i].attributes_id;

					formElem['xtype'] = records[i].field_type;
					if(records[i].mandatory==1)
					{
						formElem['allowBlank'] = false;
						formElem['fieldLabel'] =  records[i].label_name+' <span style="color:red">*</span>';
					}
					else
					{
						formElem['fieldLabel'] =  records[i].label_name;
					}
					formElem['name'] =  'addtional_'+records[i].attributes_id;
					formElem['id'] =  'addtional_'+records[i].attributes_id;
					formElem['value'] = obj[feildvalue];
					formElem['width'] = 280;
					if(records[i].field_name == "dollar_value")
					{
						formElem['maskRe'] = /[0-9.]/;
						formElem['regex'] = /^\d+(\.\d{1,2})?$/;
					}
					if(records[i].field_type == "datefield")
					{
						formElem['format'] =AM.app.globals.CommonDateControl;   
						formElem['minValue'] = startMinDate;         
					}
					if(records[i].field_type == "numberfield")
					{
						formElem['minValue'] = 0;      
						formElem['maxValue'] = 999999999;      
					}
					if(records[i].field_type == "timefield")
					{
						formElem['format'] = 'H:i';
						formElem['maxValue'] = '12:00';      
					}
					if(records[i].field_name == "week")
					{
						var locStore = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "W1"},
							{"name": "W2"},
							{"name": "W3"},
							{"name": "W4"},
							{"name": "W5"},
							]
						});
						formElem['store'] = locStore;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;        
					}
					if(records[i].field_name == "build_project_complexity")
					{ 
						var locStore2 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Standard Static"},
							{"name": "Moderate Static"},
							{"name": "Standard Responsive"},
							{"name": "Moderate Responsive"},
							{"name": "Standard Fluid"},
							{"name": "Moderate Fluid"},
							{"name": "Complex Static"},
							{"name": "Complex Fluid"},
							{"name": "Complex Responsive"},
							]
						});
						formElem['store'] = locStore2;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";      
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "managed_by")
					{
						var locStore3 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Theorem"},
							{"name": "Mogo"},
							]
						});
						formElem['store'] = locStore3;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name"; 
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "channel")
					{
						var locStore4 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Display"},
							{"name": "AdsManager"},
							{"name": "Search"},
							{"name": "Social"},
							{"name": "Email"},
							{"name": "Programmatic"},
							]
						});
						formElem['store'] = locStore4;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name"; 
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "category")
					{
						var locStore5 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Report"},
							{"name": "Dashboard"},
							{"name": "Implementation"},
							{"name": "Screenshot"},
							{"name": "Trafficking"},
							{"name": "Site QA"},
							{"name": "Content Monitoring"},
							{"name": "Email Deployment"},
							]
						});
						formElem['store'] = locStore5;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "patch_news_corp")
					{
						var locStore6 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "NSW 1"},
							{"name": "NSW 2"},
							{"name": "NSW 3"},
							{"name": "NSW 4"},
							{"name": "NSW 5"},
							{"name": "NSW 6"},
							{"name": "NSW 7"},
							{"name": "NSW 8"},
							{"name": "VIC 1"},
							{"name": "VIC 2"},
							{"name": "VIC 3"},
							{"name": "VIC 4"},
							{"name": "WA"},
							{"name": "QLD"},
							{"name": "SA & Taus"},
							{"name": "VIC"},
							{"name": "Rich Media"},
							]
						});
						formElem['store'] = locStore6;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "request_region")
					{
						var locStore7 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "APAC"},
							{"name": "EMEA"},
							{"name": "Amer"},
							{"name": "East"},
							{"name": "West"},
							{"name": "Central"},
							{"name": "SMB"},
							{"name": "Programmatic"},
							{"name": "Media Plan"},
							{"name": "ISQTC - East"},
							{"name": "ISQTC - West"},
							{"name": "OSQTC - East"},
							{"name": "OSQTC - West"},
							{"name": "OSQTC - Central"},
							]
						});
						formElem['store'] = locStore7;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "service_line")
					{
						var locStore8 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Reporting"},
							{"name": "AdOps"},
							{"name": "Search"},
							{"name": "Email"},
							]
						});
						formElem['store'] = locStore8;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "time_zone")
					{
						var locStore9 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "APAC"},
							{"name": "EMEA"},
							{"name": "LATE EMEA"},
							{"name": "EST"},
							{"name": "CST"},
							{"name": "PST"},
							]
						});
						formElem['store'] = locStore9;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;       
					}
					if(records[i].field_name == "clarification")
					{
						var locStore10 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Yes"},
							{"name": "No"},
							]
						});
						formElem['store'] = locStore10;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;        
					}
					if(records[i].field_name == "type_of_clarification")
					{
						var locStore11 = Ext.create('Ext.data.Store', {
							fields: ['name'],
							data : [
							{"name": "Approvals"},
							{"name": "Asset Missing"},
							{"name": "Asset Not Working"},
							{"name": "Clarification"},
							{"name": "Incomplete Information"},
							]
						});
						formElem['store'] = locStore11;      
						formElem['displayField'] = "name";      
						formElem['valueField'] = "name";
						formElem['multiSelect'] = false;        
					}

					if(i%2!=0)
					{
						formElem['margin'] = '0 0 0 17';
					}

					formObj.push(formElem); 

					if(formObj.length==2)
					{
						var formItem = "";
						formItem = {
							layout : 'hbox',
							margin : '0 0 10 0',
							items:formObj
						};
						formObj = [];
						Ext.getCmp('additionalElements').add({items:formItem});
					}
					else if(formObj.length==1 && rec_length == countElem)
					{
						var formItem = "";
						formItem = {
							layout : 'hbox',
							items:formObj
						};
						formObj = [];
						Ext.getCmp('additionalElements').add({items:formItem});
					}
				}
				if(Ext.getCmp('additionalElements').items.length > 0)
				{
					Ext.getCmp('additionalElements').show();
					Ext.getCmp('utilization_add_edit').setHeight(530);
				}
				else
				{
					Ext.getCmp('additionalElements').hide();
					Ext.getCmp('utilization_add_edit').setHeight(405);
				}
				Ext.getCmp('utilization_add_edit').center()
			}
		});

Ext.getCmp('client_id').setValue(rec.data.fk_client_id);
Ext.getCmp('saveButtonID').setText ('Update');


if(Ext.getCmp('additionalElements').items.length > 0)
{
	Ext.getCmp('additionalElements').show();
	Ext.getCmp('utilization_add_edit').setHeight(530);
	Ext.getCmp('utilization_add_edit').center()
}
else
{
	Ext.getCmp('additionalElements').hide();
	Ext.getCmp('utilization_add_edit').setHeight(405);
	Ext.getCmp('utilization_add_edit').setWidth(635);
	Ext.getCmp('utilization_add_edit').center()
}

Ext.getStore('roleCode').load({params: {'clientId':rec.data.fk_client_id,'wor_id':rec.data.fk_wor_id,'hasNoLimit':'1', }});
Ext.getStore('ProjectTypes').load({params: {'wor_id':rec.data.fk_wor_id,'hasNoLimit':'1', }});
Ext.getStore('taskCode').load({params: {'hasNoLimit':'1'}});
Ext.getStore('subTaskCode').load({params: {'hasNoLimit':'1','task_code':rec.data.fk_task_id}});

view.down('form').loadRecord(rec);

if(rec.data.status == "Approved")
{
	Ext.getCmp('saveButtonID').hide();
}

Ext.getCmp('project_type_id').setValue(rec.data.fk_project_id);
Ext.getCmp('project_code').setValue(parseInt(rec.data.fk_role_id));
Ext.getCmp('task_code').setValue(parseInt(rec.data.fk_task_id));
Ext.getCmp('sub_task').setValue(JSON.parse("[" + rec.data.fk_sub_task_id + "]"));
},


utilzationFun: function(me,cliId,dteObj,empID,proc,rawValue){

	if(Ext.util.Cookies.get('employee_id') == AM.app.globals.utilEmployIDs)
	{
		var empID = empID;
	}
	else if(AM.app.globals.utilEmployIDs!=""){
		var empID = AM.app.globals.utilEmployIDs;
	}

	var client_id = cliId; 

	if(isNaN(cliId))
		client_id = AM.app.globals.utilProcess;
		// console.log(Ext.getStore('Utilization').load());
		Ext.getStore('Utilization').load({
			params:{'client_id':client_id,'hasNoLimit':"1",'utilization_date':dteObj,'employee_id':empID},
			callback:function(records, options, success) {
				
				if (success) 
				{
					var d = new Date();
					var todaysDate = Ext.Date.format(d, 'm/d/Y');	
					var comboDate = Ext.Date.format(dteObj, 'm/d/Y');
					
					var date1 = new Date(todaysDate);
					var date2 = new Date(comboDate);
					var timeDiff = Math.abs(date2.getTime() - date1.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
					// checking the past 15days
					if(diffDays <=7)
					{
						Ext.getCmp('AddUtil').setDisabled(false);
						Ext.getCmp('AddUtil').addClass('enabledComboTestCls');
						Ext.getCmp('AddTeamMemberUtil').setDisabled(false);
					}
					else
					{
						Ext.getCmp('AddUtil').setDisabled(true);
						Ext.getCmp('AddUtil').addClass('disabledComboTestCls');
						Ext.getCmp('AddTeamMemberUtil').setDisabled(true);
					}
					
					if(proc)
					{
						me.storeLoad(me.store,me);
						me.doLayout(false,true);
					}
					var tskDate = Ext.Date.format(d, 'd-m-Y');
					
					if(records.length!=0)
					{
						me.storeLoad(me.store,me);
						me.doLayout(false,true);
					}
				}
			}
		});
		
		AM.app.globals.utilization = [];
	},
	
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Utilization');
		store.filters.clear();

		var utilization_date = Ext.getCmp('utilization_date').getValue();
		var utilization_process = Ext.getCmp('utilization_process').getValue();
		
		store.getProxy().extraParams = this.customParamers(utilization_process, utilization_date);
		store.filter({
			property: 'emp.FirstName, temp.Errors',
			value: t.getValue()
		});
	},
	
	
	onTriggerClear : function() {
		var store = this.getStore('Opsdata');
		store.clearFilter();
	},
	
	
	deleteUtilization : function(grid, row, col,me){
		
		var rec = grid.getStore().getAt(row);
		
		empID = Ext.util.Cookies.get('employee_id');
		dteObj = Ext.getCmp('utilization_date').getValue();
		Ext.Msg.confirm('Confirm', 'Are you sure you want to Delete?', function (button) 
		{
			if (button == 'yes') 
			{
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/utilization/utilization_dele',
					method: 'POST',
					// async: false,
					params: {
						'utilization_id': rec.data.utilization_id,
					},
					success: function(response) {
						var myObject = Ext.JSON.decode(response.responseText);
						
						if(myObject["success"]){
							Ext.MessageBox.show({
								title: "Message",
								msg: "Deleted Successfully",
								buttons: Ext.Msg.OK
							});
							
							rec.store.remove(rec);
							// clientID,wor_id,date
							AM.app.getController('Utilization').viewContent(Ext.getCmp('utilization_client').getValue(),Ext.getCmp('utilization_wor_id').getValue(),Ext.getCmp('utilization_date').getValue());
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: " Failed",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK
							});
						}
					},
				});
			}
		}, this);
	},
	
	
	roleCodeHelp : function(clientId,wor_id) {			
		var view = Ext.widget('RoleCodeHelp');
		view.setTitle('Role Code Details');
		var myStore = this.getStore('roleCode');
		
		myStore.getProxy().extraParams = {
			'clientId'	:clientId,
			'wor_id'	:wor_id,
		};
		myStore.filters.clear();
		myStore.filter({
			property: 'p.name',
			value: '1'
		});
		Ext.getCmp('role_code_grid').bindStore(myStore);
		Ext.getCmp('role_code_grid_bb').bindStore(myStore);	
		myStore.load();				
	},
	
	taskCodeHelp : function() {			
		var view = Ext.widget('TaskCodeHelp');
		view.setTitle('Task Code Details');
		var myStore = this.getStore('taskCode');
		myStore.filters.clear();
		/* myStore.filter({
            property: 'task_code',
            value: '1'
        }); */
        Ext.getCmp('task_code_grid').bindStore(myStore);
        Ext.getCmp('task_code_grid_bb').bindStore(myStore);			
    },
    
    onUpload : function(clientId, workId) {		
    	Ext.widget('empimportXls');	
		//var prcessID = processId;
		//if(isNaN(prcessID))
			//prcessID = AM.app.globals.utilProcess;
			
			Ext.getCmp('hiddenClientID').setValue(clientId);
			Ext.getCmp('hiddenWorID').setValue(workId);
		},
		
		onImportxls : function(form, window) {
			// alert();
			var myStore = this.getStore('Utilization');
			var record = form.getRecord(), values = form.getValues();
			
			var Dt = Ext.getCmp('utilization_date').getValue();
			var empid = Ext.util.Cookies.get('empid');
			
			form.submit({
				method: 'POST',
				url: myStore.getProxy().api.url,
				waitMsg : 'Saving your data, please wait...',  
				success: function(form,o) {
					var retrData = JSON.parse(o.response.responseText);
					
					Ext.MessageBox.show({
						title: "Success",
						icon: Ext.MessageBox.INFO,
						msg: retrData.msg,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") {
								window.close();
							}
						}
					});

					myStore.load({
						params: {
							
							client_id: Ext.getCmp('utilization_client').getValue(),
							wor_id: Ext.getCmp('utilization_wor_id').getValue(),						
							//ProcessID: prcessID,
	                		//utilization_date: Dt,
	                		//EmployID:empid
	                	}
	                });
				},
				failure: function(form, o)
				{
					var retrData = JSON.parse(o.response.responseText);
					Ext.MessageBox.show({
						msg: retrData.msg,
						title: "Error",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") {
								window.close();
							}
						}
					});
					myStore.rejectChanges();
					window.close();
				}
			});
			
		},
		
	});