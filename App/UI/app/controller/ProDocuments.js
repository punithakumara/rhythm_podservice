Ext.define('AM.controller.ProDocuments',{
	extend : 'Ext.app.Controller',
	stores : ['DocumentAccess','Vertical','Documents','CompanyDocs','ProcessDocs','VerticalDocs','DocumentsTemplates','DocumentsVerticalTemplates'],
	views: ['documents.CompanyDocuments','documents.VerticalDocuments','documents.DocumentsAcc','documents.Add_edit','PDF','documents.TemplateCompnyDocuments','documents.Add_edit_Template','documents.TemplateVerticalDocuments','documents.add_edit_template_vertical'],
	
	init : function() {
		this.control({
			'attributesList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            }
		});
	},
	
	viewContent : function() {
		delete this.getStore('Process').getProxy().extraParams;
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		var prodoc = this.getView('documents.DocumentsAcc').create();
		mainContent.add( prodoc );
		mainContent.doLayout();
	},

	onCreate : function() {
		var view = Ext.widget('docAddEdit');
	},

	onCreateTemplate : function() {
		var view = Ext.widget('tempAddEdit');
	},

	onSaveDocument : function(form, window) 
	{
	
		var myStore = this.getStore('Documents');
		var compStore = this.getStore('CompanyDocs');
		var vertStore = this.getStore('VerticalDocs');
		
		var record = form.getRecord(), values = form.getValues();
		
		if(!record) 
		{
			myStore.add(values);
			form.url = myStore.getProxy().api.create;
			var titleText = 'Added';
		}
		else 
		{
			var docEditPro="",docEditVert="",docEditClient="",PreviousUploadName="";
			
			var docModifiedPro="",docModifiedVert="",docModifiedClient="";
			
			PreviousUploadName = record.get('UploadName');
				
			if(record.raw.Access == "All managers" || record.raw.Access == "All")
			{
				docEditVert="";
				docEditPro="";				
				docEditClient="";
			}
			else if(record.raw.Access == "Vertical Specific")
			{
				docEditVert=record.raw.VerticalID;
				docEditPro="";				
				docEditClient="";
			}
			
			if(values.Access == "All managers" || values.Access == "All")
			{
				docModifiedPro = "";
				docModifiedVert = "";				
				docModifiedClient = "";
			}
			else if(values.Access == "Vertical Specific")
			{
				docModifiedVert = values.VerticalID;
				docModifiedPro = "";				
				docModifiedClient = "";
			}
			
			form.url = myStore.getProxy().api.update;
			form.baseParams = {'docEditPro':docEditPro,'docEditVert':docEditVert,'docEditClient':docEditClient,'docModifiedPro':docModifiedPro,'docModifiedVert':docModifiedVert,'docModifiedClient':docModifiedClient,'rawAccess':record.raw.Access,'modifiedAccess':values.Access,'PreviousUploadName':PreviousUploadName};
			
			// form.params = myStore.getProxy().extraParams;
			record.set(values);
			var titleText = 'Updated';
		}		
		
		//console.log('testing...');
		form.submit({
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
            success: function(form,o) {
                var retrData = JSON.parse(o.response.responseText);
                if(retrData.success) 
				{
					if(retrData.filesize=='max') 
					{
						window.destroy();
						AM.app.getController('ProDocuments').onCreate();
						Ext.MessageBox.show({
							title: 'Error',
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {						
								if (buttonId === "ok") {									
									window.destroy();
								}
							}
						});
					}
					else if(retrData.file=='notexist') 
					{
						window.destroy();
						AM.app.getController('ProDocuments').onCreate();
						Ext.MessageBox.show({
							title: 'Error',
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {						
								if (buttonId === "ok") {									
									window.destroy();
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: titleText,
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") {
								compStore.load();
								procStore.load();
								vertStore.load();							
								window.destroy();
								}
							}
						});
					}
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        //title: 'Error',
                        msg: retrData.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                            	window.destroy();
                            }
                        }
                    });
		    	}
                //myStore.load();				
				window.destroy();
            },
            failure: function(form, action)
            {
            	console.log("Error occured while saving Document. Please try again.");
                myStore.rejectChanges();
                window.close();
            }
        });
	},
	
	
	onEditDocument:function(docID,grid, row, col) 
	{
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('prodocAddEdit');
		
		view.down('form').query('filefield')[0].setDisabled(true);
		if(docID == "vertDocID")
		{
			view.setTitle('Edit Vertical Document Details');
			view.down('form').query('combobox')[3].setDisabled(false);
		}
		else
		{
			view.setTitle('Edit Company Document Details');
		}
		
		view.down('form').loadRecord(rec);
	},
	
	
	// ******* viewing uploaded file ******* //
	onViewDocuments:function(grid, row, col)
	{
		var rec = grid.getStore().getAt(row);

		var doctype = "";

		if(rec.get('Access') == "All managers" || rec.get('Access') == "All")
		{
			doctype ="companyDocs/"+rec.get('UploadName');
		}
		else
		{
			Folder = "temp";
			
			if(rec.get('Folder') != "")
				Folder = rec.get('Folder');
				
			doctype ="verticalDocs/"+Folder+"/"+rec.get('UploadName');
		}
		
		var view = this.getView('PDF').create({doctype:doctype,docs:'Documents'});
	},
	
	
	onDeleteDocuments:function(grid, row, col)
	{
		Ext.Msg.confirm('Confirm', 'Are you sure you want to Delete?', function (button) {
			if (button == 'yes') 
			{
				var compStore = this.getStore('CompanyDocs');
				var vertStore = this.getStore('VerticalDocs');
				Folder = "temp";
				var rec = grid.getStore().getAt(row);
				var doctyp = rec.get('Access');
				
				if(rec.get('Folder')!=undefined)
				{
					if(rec.get('Folder') != "")
						Folder = rec.get('Folder');
				}
				
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/documents/Documents_dele',
					method: 'POST',
					// async: false,
					params: {
						'doctype': doctyp,
						'DocumentID': rec.get('DocumentID'),
						'uploadName':rec.get('UploadName'),
						'getFolder':Folder
					},
					success: function(response) {
						var myObject = Ext.JSON.decode(response.responseText);
						
						if(myObject["success"])
						{
							Ext.MessageBox.show({
								title: "Message",
								msg: "Successfully deleted",
								buttons: Ext.Msg.OK
							});
							
							compStore.load();
							vertStore.load();
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: " Failed",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK
							});
						}
					},
				
				});
			}
		}, this);
	}
	
});