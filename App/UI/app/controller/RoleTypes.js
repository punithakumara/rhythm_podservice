Ext.define('AM.controller.RoleTypes',{
	extend : 'Ext.app.Controller',
	stores: ['RoleTypes'],
	views: ['RoleTypes.List'],

	init : function(){
		this.control({
			'RoleTypes #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
		});
	},
	
	viewRoleTypes : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var RoleTypes = this.getView('RoleTypes.List').create();
		mainContent.removeAll();
		mainContent.add(RoleTypes);
		mainContent.doLayout();		
	},

	onTriggerClear : function() {
		var store = this.getStore('RoleTypes');
		store.clearFilter();
	},

	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('RoleTypes');
		store.filters.clear();
		store.filter({
			property: "project_type,role_type,role_description",
			value: t.getValue()
		});
	}
	
});