Ext.define('AM.controller.Language',{
	extend : 'Ext.app.Controller',
	stores : ['Language'],
    models : ['Language'],
	views: ['Skills.LanguageList','Skills.Language_Add_Edit'],
	
	init : function() {
		this.control({
			'languageAddEdit' : {
				'saveLangauge' : this.onSaveLanguage
			}
		});
	},
   
	onCreate : function() {
		var view = Ext.widget('languageAddEdit');
		view.setTitle('Add Language');
	},
	
	onSaveLanguage : function(form, win) {
		
		var myStore = this.getStore('Language');
		var record = form.getRecord(),
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
		    success: function (form, o) {	    	
		    	//var VertID =  Ext.get('VerticalNameCombo') ? Ext.getCmp('VerticalNameCombo').getValue() : "";
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{
					if(jsonResp.title=="Updated") 
					{
						win.close();
					}
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    		if(jsonResp.title!="Existing") 
					{
		    			win.close();
					}
		    		if(jsonResp.title=="Existing") 
					{
						if(titleText != "Added"){myStore.load();win.close();}
						myStore.load();
					}	
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    /*	
		    	if(VertID != "")
				{
	    			Ext.getStore('Tech').removeAll();
	    	    	var myTechStore = Ext.getStore('Tech').load({
	    	    		params:{ VerticalID: VertID, hasNoLimit:1},
	    	    		callback : function(records){
	    	    			if(records.length)
							{
	    	    		    	Ext.getCmp('Process_technologies').bindStore('Tech');
	    	    			}
	    	    		}
	    	        });
	    		}
				else
				{	
	    			myStore.load();
	    		}*/
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving language. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditLanguage : function(grid, row, col) {
	/*	var rec = grid.getStore().getAt(row);
		var view = Ext.widget('languageAddEdit');
		// console.log(rec);
		view.setTitle('Edit language Details');
        view.down('form').loadRecord(rec);*/
	},
	
});