Ext.define('AM.controller.Client_Contacts',{
	extend	: 'Ext.app.Controller',
	stores	: ['Client_Contacts', 'DeleteClientContact','Attributes'],
	views	: ['Client_Contacts.List'],
	refs	: [{
		ref			: 'Client_ContactsGridId', 
		selector	: 'grid'
	}],
	
	init	: function() {
		this.control({
			'Client_ContactsList #gridTrigger1' : {
				keyup			: this.onTriggerKeyUp,
				triggerClear	: this.onTriggerClear
			},			
		});
	},
	
	//**** List View ****//
	viewContent : function() {
		delete this.getStore('Client_Contacts').getProxy().extraParams;	
		var store 						= this.getStore('Client_Contacts');		
		store.clearFilter();
		store.getProxy().extraParams	= {client_id:AM.app.globals.mainClient};
		var mainContent					= Ext.ComponentQuery.query('#mainContent')[0];
		var Client_Contacts				= this.getView('Client_Contacts.List').create();
		mainContent.removeAll();
		mainContent.add(Client_Contacts);
		mainContent.doLayout();		
	},	
	
	// Filteting And Sorting
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Client_Contacts');
		store.filters.clear();
		store.filter({            
			property: 'contact_name,contact_title',
			value: t.getValue()
		});
	},

	onTriggerClear : function() {
		var store = this.getStore('Client_Contacts');
		store.clearFilter();
	},
	
	validateedit	: function(editor,e) {


		var cont_name	= Ext.ComponentQuery.query("#cont_name")[0].getValue();
		var cont_desi	= Ext.ComponentQuery.query("#cont_desi")[0].getValue();
		var cont_email	= Ext.ComponentQuery.query("#cont_email")[0].getValue();
		var cont_servi	= Ext.ComponentQuery.query("#client_cont_serv")[0].getValue();
		// var cont_proces	= Ext.ComponentQuery.query("#client_cont_process")[0].getValue();
		var cont_role	= Ext.ComponentQuery.query("#client_cont_role")[0].getValue();

		is_fields_null = 1;
		
		var myStore = this.getStore('ClientContactVertical');
		var ContactStore = this.getStore('Client_Contacts');

		var removeRec = ContactStore.findRecord('contact_email', e.newValues.contact_email);

		// console.log('removeRec' + removeRec);
		if(removeRec != null)
		{
			var form_contact_id = e.record.data.contact_id;
			var store_contact_id = removeRec.data.contact_id;

			// console.log(form_contact_id);
			// console.log(store_contact_id);

			if(form_contact_id != store_contact_id)
			{
				return 2;	
			}
		}
		
		Ext.each(myStore.data.items, function(value, i) {      					
			if (value.data.ServiceName === cont_servi)
			{
				is_fields_null = 0;	
			}
		});

		if(is_fields_null == 1)
		{
			return false;	
		}	
		
		if(cont_name == "" || cont_name	== null || cont_desi == "" || cont_desi	== null || cont_email == "" || cont_email == null
			|| cont_role == "" || cont_role == null) {
			return false;
	} else {
		return true;
	}
	
},

validateattributeedit	: function(editor,e) {
	var attribute_name_list	= Ext.ComponentQuery.query("#attribute_name_list")[0].getValue();

	if(attribute_name_list == "" || attribute_name_list	== null	) {
		return false;
	} else {
		return true;
	}
}

});