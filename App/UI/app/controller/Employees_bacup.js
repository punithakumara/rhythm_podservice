Ext.define('AM.controller.Employees', {
	extend: 'Ext.app.Controller',
	stores : ['Employees','GetReportiee','Designations','Grade','ReportTo','Shifts','Locations','Gender','Status','Vertical','Qualifications', 'userProfile', 'GetApprover','EmployeeProfile','Certifications','SkillSets','EmployeeAchievement','Languages','LanguagesList','Skills_Tools_Tech'],

	views: ['employee.List', 'employee.Add', 'employee.Edit', 'employee.View','employee.Profile','employee.Relieve','employee.Promote','employee.Resign','employee.Certificate_Add_Edit','employee.Skill_Add_Edit','employee.Language_Add_Edit','employee.Achievement_Add_Edit','employee.WorkExperience_Add_Edit'],

	refs: [{
		ref: 'employGrid', 
		selector: 'grid'
	}],

	init: function() {
		this.control({
			'employeeList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
			'employeeList button[action=uploadEmployee]' : {
				click : this.onUpload
			},
			//code added to filter based on EmpStatus(Active/Inactive)
			'employeeList #empStatusCombo' : {
				changeStatusCombo : this.changeEmpStatus
			},
			'employeeAddEdit' : {
				'DisableVerCBox' : this.onDisableVerCBox,
				'EnableVerCBox' : this.onEnableVerCBox
			},
			'#DesignNameCombo' : {
				beforequery : function(queryEvent) {
					Ext.Ajax.abortAll();
					queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','filterName' : queryEvent.combo.displayField};
				}
			},
			'#OfficeLocation' : {
				beforequery : function(queryEvent) {
					Ext.Ajax.abortAll();
					queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','filterName' : queryEvent.combo.displayField};
				}
			},
			'certificateAddEdit' : {
				'saveCertificate' : this.onSaveCertificate
			},
			'languageAddEdit' : {
				'saveLanguage' : this.onSaveLanguage
			},
			'skillAddEdit' : {
				'saveSkillset' : this.onSaveSkillset
			},
			'achievementAddEdit' : {
				'saveAchievement' : this.onSaveAchievement
			}
		});
	},

	viewContent : function(){
		delete this.getStore('Employees').getProxy().extraParams;
		var store = this.getStore('Employees');
    	// if gridEmployees = 1 display only his employee details
    	// if gridEmployees = 0 display all the employee details
    	var gridEmployees = 1;
    	if(Ext.util.Cookies.get('Vert') == Ext.util.Cookies.get('HRVer'))
    		gridEmployees = 0;
    	if(Ext.util.Cookies.get('Vert')=="")
    		gridEmployees = 0;

    	store.getProxy().extraParams = {'gridEmployees': gridEmployees};
    	store.clearFilter();
    	var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
    	var employees = this.getView('employee.List').create();
    	mainContent.removeAll();
    	mainContent.add( employees );
    	mainContent.doLayout();
    },
    
    onTriggerKeyUp : function(t) {
    	Ext.Ajax.abortAll();
    	var store = this.getStore('Employees');
    	store.filters.clear();
    	store.filter({
    		property: 'employees.company_employ_id, employees.first_name, d.name, employees.email, v.name, s.shift_code, loc.name',
    		value: t.getValue()
    	});
    },
    
    onTriggerClear : function() {
    	var store = this.getStore('Employees');
    	store.clearFilter();
    },
    
    onCreate : function(rec) {
    	var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
    	var wor = this.getView('employee.Add').create();
    	mainContent.removeAll();
    	mainContent.add( wor );
    	mainContent.doLayout();

    	Ext.getStore('Designations').load({params:{'hasNoLimit':'1'}});

    	if(!rec) '';
    	else
    	{
    		var form = view.down('#addEmployeeFrm').getForm();
    		form.findField("newjoinee_id").setValue(rec.data.EmployID);
    		form.findField("FirstName").setValue(rec.data.FirstName);
    		form.findField("LastName").setValue(rec.data.LastName);
    		form.findField("Gender").setValue(rec.data.Gender);
    		form.findField("Status").setValue(rec.data.Status);
    		form.findField("DateOfBirth").setValue(rec.data.DateOfBirth);
    		form.findField("DateOfJoin").setValue(rec.data.DateOfJoin);
    		form.findField("DesignationID").setValue(rec.data.DesignationID);
    		form.findField("AssignedVertical").setValue(rec.data.VerticalID);
    		form.findField("Email").setValue(rec.data.Email);
    		form.findField("Mobile").setValue(rec.data.Mobile);
    		form.findField("AlternateEmail").setValue(rec.data.AlternateEmail);
    		form.findField("QualificationID").setValue(rec.data.QualificationID);
    		form.findField("LocationID").setValue(rec.data.LocationID);
    		form.findField("PersonalEmail").setValue(rec.data.PersonalEmail);
    		form.findField("AlternateNumber").setValue(rec.data.AlternateNumber);
    		form.findField("PermanentAddress").setValue(rec.data.PermanentAddress);
    		form.findField("PermanentCity").setValue(rec.data.PermanentCity);
    		form.findField("PermanentState").setValue(rec.data.PermanentState);
    		form.findField("PermanentZipcode").setValue(rec.data.PermanentZipcode);
    		form.findField("SameAs").setValue(rec.data.SameAs);
    		form.findField("PresentAddress").setValue(rec.data.PresentAddress);
    		form.findField("PresentCity").setValue(rec.data.PresentCity);
    		form.findField("PresentState").setValue(rec.data.PresentState);
    		form.findField("PresentZipcode").setValue(rec.data.PresentZipcode);

    	}
		//Ext.getCmp('EmpRelStatusFrame').hide();
	},
	
	onUpload : function() {
		Ext.widget('empimportXls');
	},
	
	// Function to save Employee on Add
	onSaveEmployee : function(form) {
		Ext.MessageBox.show({
			msg: 'Saving your data, please wait...',
			progressText: 'Saving...',
			width:300,
			wait:true,
			waitConfig: {interval:200},
			animateTarget: 'waitButton'
		});
		var myStore = this.getStore('Employees');
		var record = form.getRecord(), values = form.getValues();
		var email = values.email;
		// var email_array=email.split("@");
		// email = email_array[0]+'@theoreminc.net';
		form.setValues({'Email':email});
		values = form.getValues();		
		if(values['primary_lead_id'] == undefined || values['primary_lead_id'] == null  || values['primary_lead_id'] == '')
		{
			values['primary_lead_id']=null;
		}
		if(!record) 
		{
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			record.set(values);
			var titleText = 'Updated';
		}	
		myStore.sync({ 
			success: function (batch, operations) {
				var jsonResp = batch.proxy.getReader().jsonData;
				if(jsonResp.success) 
				{				
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						fn: function(buttonId) {
							if (buttonId === "ok" && jsonResp.title=="Success") 
							{                        	
								AM.app.getController('Employees').viewContent();
							}
						}
					});	
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
			}, 
			failure: function (batch) {
				Ext.MessageBox.hide();
				myStore.rejectChanges();
			}
		});
	},
	
	
	//function to edit employee details 
	onEditEmployee : function(grid, row, col) {
		Ext.getStore('Designations').load({params:{'hasNoLimit':'1'}});
		var rec = grid.getStore().getAt(row);
		
		var RelieveFlag=rec.get('RelieveFlag');
		var Resign_Flag=rec.get('Resign_Flag');
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var wor = this.getView('employee.Edit').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
		
		if(RelieveFlag==1 || Resign_Flag == 1)
		{
			Ext.getCmp('savebtn').hide();
		}
		
		wor.down('form').loadRecord(rec);
		
		var date1 = new Date(rec.data.doj);
		var date2 = new Date();
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
		
		if(diffDays >= 8 )
		{
			Ext.getCmp('doj').setDisabled(true);
			Ext.getCmp('doj').allowBlank = true;
		}
		else
		{
			Ext.getCmp('doj').setDisabled(false);
		}
	},
	
	
	onViewEmployee : function(grid, row, col) {
		Ext.getStore('Designations').load({params:{'hasNoLimit':'1'}});
		var rec = grid.getStore().getAt(row);
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var wor = this.getView('employee.View').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
		
		if(rec.data.same_as=="1")
		{
			rec.data.permanent_address = rec.data.present_address;
			rec.data.permanent_city = rec.data.present_city;
			rec.data.permanent_state = rec.data.present_state;
			rec.data.permanent_zipcode = rec.data.present_zipcode;
		}
		
		wor.down('form').loadRecord(rec);
		
		Ext.getCmp('viewAssignedVertical').setValue(rec.data.assigned_vertical);
		Ext.getCmp('viewEmployID').setValue(rec.data.employee_id);
		Ext.getCmp('viewEmployGrade').setValue(rec.data.grade_level);
		Ext.getCmp('viewDesignationId').setValue(rec.data.designation_id);
		Ext.getCmp('viewGradeLevel').setValue(rec.data.grade_level);
		
		if(rec.data.status==6 || Ext.util.Cookies.get('grade')<5)
		{
			Ext.getCmp('empRelieve').hide();
			Ext.getCmp('empResign').hide();
			Ext.getCmp('empPromote').hide();
		}
		if(Ext.util.Cookies.get('VertId')=='10')
		{
			Ext.getCmp('empRelieve').show();
			Ext.getCmp('empResign').show();
			Ext.getCmp('empPromote').show();
		}
		if(rec.data.status==5)
		{
			Ext.getCmp('empResign').hide();
			Ext.getCmp('empPromote').hide();
		}
	},

	onResignEmployee : function() {
		var view = Ext.widget('Resign');
	},  

	onRelieveEmployee : function() {
		var view = Ext.widget('Relieve');
		
		Ext.getStore('ReportTo').load({
			params:{'hasNoLimit':'1','VerticalId':Ext.getCmp('viewAssignedVertical').getValue(),'EmployId':Ext.getCmp('viewEmployID').getValue()}
		});
		Ext.getCmp('DeligateTo').bindStore("ReportTo");
		
		if(Ext.getCmp('viewEmployGrade').getValue()>2)
		{
			Ext.getCmp('EmpDeligacyFrame').show();
		}
	},  

	onPromoteEmployee : function() {
		var view = Ext.widget('Promote');
		
		Ext.getStore('Grade').load({
			params:{'hasNoLimit':'1','PromoteToGrades':Ext.getCmp('viewGradeLevel').getValue(),'noToGrades':Ext.getCmp('viewDesignationId').getValue()}
		});
		Ext.getCmp('new_hcm_grade').bindStore("Grade");
		
		Ext.getStore('ReportTo').load({
			params:{'hasNoLimit':'1','VerticalId':Ext.getCmp('viewAssignedVertical').getValue()}
		});
		Ext.getCmp('promoteDeligate').bindStore("ReportTo");
		
		if(Ext.getCmp('viewEmployGrade').getValue()>2)
		{
			Ext.getCmp('warningfield').show();
			Ext.getCmp('promoteDeligate').show();
		}
	},

	changeEmpStatus: function(combo){
		
		var store = this.getStore('Employees');
		
		store.getProxy().extraParams = {'empStatusCombo': combo.getValue(),'gridEmployees':'1'};
		store.load();
	},	

	onSavePromotion : function(form, window) 
	{
		var values = form.getValues();
		
		var newDesignation = Ext.getCmp('new_designation_id').getRawValue();
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Employees/savePromotion/',
			method: 'POST',
			params: {
				'employee_id':Ext.getCmp('viewEmployID').getValue(), 
				'hcm_grade':values['new_grade'], 
				'new_designation':values['new_designation'], 
				'promotion_date':values['promote_date'], 
				'grade':Ext.getCmp('viewEmployGrade').getValue(), 
				'deligate_to':values['DeligateTo'], 
			},
			success: function(response, opts){
				var jsonResp = Ext.decode(response.responseText);
				if(jsonResp.success)
				{ 
					myMask.hide();
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						closable: false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								Ext.getCmp('viewGrade').setValue(values['new_grade']);
								Ext.getCmp('viewDesignation').setValue(newDesignation);
								window.close();
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
					});
				}
			}
		});
	},
	
	onResignation:function(form, window) {
		var values = form.getValues();
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Employees/updateResignation/',
			method: 'POST',
			params: {
				'employee_id':Ext.getCmp('viewEmployID').getValue(), 
				'resigned_date':values['resigned_date'], 
				'resignation_reason':values['resignation_reason'],
			},
			success: function(response, opts){
				var jsonResp = Ext.decode(response.responseText);
				if(jsonResp.success)
				{ 
					myMask.hide();
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						closable: false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								Ext.getCmp('empResign').hide();
								Ext.getCmp('empPromote').hide();
								window.close();
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
					});
				}
			}
		});
	},
	
	onRelieve:function(form, window) {
		var values = form.getValues();
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Employees/updateRelieve/',
			method: 'POST',
			params: {
				'employee_id':Ext.getCmp('viewEmployID').getValue(), 
				'relieve_date':values['RelieveDate'], 
				'relieve_type':values['RelieveType'],
				'relieve_notes':values['RelieveNotes'],
				'deligate_to':values['DeligateTo'], 
			},
			success: function(response, opts){
				var jsonResp = Ext.decode(response.responseText);
				if(jsonResp.success)
				{ 
					myMask.hide();
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						closable: false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								Ext.getCmp('empRelieve').hide();
								Ext.getCmp('empResign').hide();
								Ext.getCmp('empPromote').hide();
								window.close();
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
					});
				}
			}
		});
	},

	onViewProfile : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var wor = this.getView('employee.Profile').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
	},
	
	onCreateWorkExperience : function() {
		var view = Ext.widget('workExperienceAddEdit');
		view.setTitle('Add experience');
	},

	onSaveExperience : function(form, win) {
		
		var myStore = this.getStore('EmployeeProfile');
		var record = form.getRecord(), values = form.getValues();
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
		}	
		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
		    success: function (batch, o) {
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{				
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
                        fn: function(buttonId) {
							if (buttonId === "ok" && jsonResp.title=="Success") 
							{                        	
								AM.app.getController('Employees').onViewProfile();
								
								win.close();
							}
						}
                    });	
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    	}
		    }, 
		    failure: function (batch) {
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	
	//function to edit experience details 
	onEditExperience : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		
		var view = Ext.widget('workExperienceAddEdit');
		view.setTitle('Edit experience');
		
		view.down('form').loadRecord(rec);
	},

	
	deleteExperience : function(grid, row, col) {
		
		var rec = grid.getStore().getAt(row);
		empID = Ext.util.Cookies.get('employee_id');

		Ext.Msg.confirm('Confirm', 'Are you sure you want to Delete?', function (button) 
		{
			if (button == 'yes') 
			{
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/EmployeeProfile/experience_dele',
					method: 'POST',
					// async: false,
					params: {
						'experience_id': rec.data.id,
					},
					success: function(response) {
						var myObject = Ext.JSON.decode(response.responseText);
						
						if(myObject["success"])
						{
							Ext.MessageBox.show({
								title: "Message",
								msg: "Deleted Successfully",
								buttons: Ext.Msg.OK
							});
							AM.app.getController('Employees').onViewProfile();
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: " Failed",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK
							});
						}
					},
				});
			}
		}, this);
	},

	onCreateCertificate : function() {
		var view = Ext.widget('certificateAddEdit');
		view.setTitle('Add Certificate');
	},
	onSaveCertificate : function(form, win) {
		var myStore = this.getStore('Certifications');
		var record = form.getRecord(),
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
		    success: function (form, o) {	    	
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{					
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
						AM.app.getController('Employees').onViewProfile();
		    			 win.close();
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}		    
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving technology. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	onEditCertificate : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('certificateAddEdit');
		view.setTitle('Edit Certificate');
        view.down('form').loadRecord(rec);
	},
	onDeleteCertificate : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var certName = rec.get('certificate_name');
		Ext.Msg.confirm('Delete Certificate', 'Are you sure to delete '+certName+' ?', function (button) {
            if (button == 'yes') 
			{
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) 
						{
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else 
						{
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting tech. Please try again.");
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	
	onCreateSkillset : function() {
		var view = Ext.widget('skillAddEdit');
		view.setTitle('Add Skill/Tool/Technology');
	},
	onSaveSkillset : function(form, win) {
	//console.log("here");	
		var myStore = this.getStore('SkillSets');
		var record = form.getRecord(),
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
			success: function (form, o) {	    	
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{					
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
						AM.app.getController('Employees').onViewProfile();
		    			 win.close();
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}		    
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving technology. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	onEditSkillset : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('skillAddEdit');
		// console.log(rec);
		view.setTitle('Edit Skill');
        view.down('form').loadRecord(rec);
	},
	onDeleteSkillset : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var skillName = rec.get('skill_name');
		Ext.Msg.confirm('Delete Skill', 'Are you sure to delete '+skillName+' ?', function (button) {
            if (button == 'yes') 
			{
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) 
						{
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else 
						{
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting skill. Please try again.");
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	onCreateLanguage : function() {
		var view = Ext.widget('languageAddEdit');
		view.setTitle('Add Language');
	},
	onSaveLanguage : function(form, win) {
	//console.log("here");	
		var myStore = this.getStore('Languages');
		var record = form.getRecord(),
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
			success: function (form, o) {	    	
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{					
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
						AM.app.getController('Employees').onViewProfile();
		    			 win.close();
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}		    
		    },  
		    failure: function (batch) {
		    	console.log("Error occured while saving language. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	onEditLanguage : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('languageAddEdit');
		// console.log(rec);
		view.setTitle('Edit Language');
        view.down('form').loadRecord(rec);
	},
	onDeleteLanguage : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var languageName = rec.get('language');
		Ext.Msg.confirm('Delete Language', 'Are you sure to delete this Language Proficiency?', function (button) {
            if (button == 'yes') 
			{
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) 
						{
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else 
						{
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting language. Please try again.");
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	onCreateAchievement : function() {
		var view = Ext.widget('achievementAddEdit');
		view.setTitle('Add Achievement');
	},
	onSaveAchievement : function(form, win) {	
		var myStore = this.getStore('EmployeeAchievement');
		var record = form.getRecord(),
		values = form.getValues();

		if(!record) 
		{
			form.url = myStore.getProxy().api.create;
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			form.url = myStore.getProxy().api.update;
			record.set(values);
			var titleText = 'Updated';
		}		
		form.submit({ 
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
		    success: function (form, o) {	    	
		    	var jsonResp = JSON.parse(o.response.responseText);
		    	if(jsonResp.success) 
				{					
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
						AM.app.getController('Employees').onViewProfile();
		    			 win.close();
		    	}
		    	else 
				{
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}		    
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving Achievement. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	onEditAchievement : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('achievementAddEdit');
		// console.log(rec);
		view.setTitle('Edit Achievement');
        view.down('form').loadRecord(rec);
	},
	onDeleteAchievement : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var achievementName = rec.get('award_name');
		Ext.Msg.confirm('Delete Achievement', 'Are you sure to delete '+achievementName+' ?', function (button) {
            if (button == 'yes') 
			{
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) 
						{
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else 
						{
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting achievement. Please try again.");
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},	

	sendEmailNotification:function(category)
	{

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/EmployeeProfile/sendEmailNotification',
			method: 'POST',
			params: {
				'category': category,
			},
			success: function(response) {
				var myObject = Ext.JSON.decode(response.responseText);

				if(myObject["success"])
				{
					Ext.MessageBox.show({
						title: "Message",
						msg: "Email Sent",
						buttons: Ext.Msg.OK
					});
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: "Email not sent",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
			},
		});
	}
	
});