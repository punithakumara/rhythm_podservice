Ext.define('AM.controller.newIncidentLog',{
	extend : 'Ext.app.Controller',	
	stores : ['Clients','Employees','newRootCause','newErrorLog','apprIncidentLog','newEscLog','Managers','workOrder','TeamLeaderIncident','AssociateManagerIncident','Errorcategory','Errorsubcategory','ResponsiblePerson'],
	models : ['newIncidentLog','newEscLog'],
	views: ['newIncidentLog.apprList','newIncidentLog.appr_AddEdit','newIncidentLog.Multiupload','newIncidentLog.escList','newIncidentLog.esc_AddEdit','newIncidentLog.errorList','newIncidentLog.error_AddEdit','newIncidentLog.errorView','newIncidentLog.CurrencyField','newIncidentLog.escView','newIncidentLog.apprView'],
	
	init : function() {

		this.control({
			'errorListGrid #gridTrigger' : {
				keyup : this.erroronTriggerKeyUp,
				triggerClear : this.erroronTriggerClear
			},
			'logList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
			'apprList #gridTrigger' : {
				keyup : this.appronTriggerKeyUp,
				triggerClear : this.appronTriggerClear
			}
		});
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('newEscLog');
		store.filters.clear();
		store.filter({
			property: 'error_id, client_name,first_name,last_name',
			value: t.getValue()
		});
	},

	onTriggerClear : function() {
		var store = this.getStore('newEscLog');
		store.clearFilter();
	},
	
	appronTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('apprIncidentLog');
		store.filters.clear();
		store.filter({
			property: 'appr_id, client_name,employees.first_name,employees.last_name',
			value: t.getValue()
		});
	},

	appronTriggerClear : function() {
		var store = this.getStore('apprIncidentLog');
		store.clearFilter();
	},
	
	erroronTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('newErrorLog');
		store.filters.clear();
		store.filter({
			property: 'error_id, client_name ,employees.first_name,employees.last_name',
			value: t.getValue()
		});
	},

	erroronTriggerClear : function() {
		var store = this.getStore('newErrorLog');
		store.clearFilter();
	},
	
	viewContent : function(cat) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var Log = "";
		if(cat=="appr")
		{
			var store = this.getStore('apprIncidentLog');
			store.clearFilter();
			Log = this.getView('newIncidentLog.apprList').create();
		}
		if(cat=="esc")
		{
			var store = this.getStore('newEscLog');
			store.clearFilter();
			Log = this.getView('newIncidentLog.escList').create();
		}
		if(cat=="error")
		{
			var store = this.getStore('newErrorLog');
			store.clearFilter();
			Log = this.getView('newIncidentLog.errorList').create();
		}
		mainContent.removeAll();
		mainContent.add( Log );
		mainContent.doLayout();
	},
	
	onCreate : function(cat) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var Log = "";
		if(cat=="appr")
		{
			Log = this.getView('newIncidentLog.appr_AddEdit').create();
			Ext.getCmp('attachApprFile').hide();
		   // Ext.getCmp('download_apr').hide();
		   Ext.getCmp('responsible_member').setDisabled(true);
		   Ext.getCmp('responsible_member').reset();
		}
		if(cat=="esc")
		{
			Log = this.getView('newIncidentLog.esc_AddEdit').create();
			Ext.getCmp('manager').setDisabled(true);
			Ext.getCmp('production_member').setDisabled(true);
			Ext.getCmp('attachEscFile').hide();
		    //Ext.getCmp('download_esc').hide();
		}
		if(cat=="error")
		{
			Log = this.getView('newIncidentLog.error_AddEdit').create();
			Ext.getCmp('attachErrFile').hide();
		  //  Ext.getCmp('download_err').hide();
		}
		
		mainContent.removeAll();
		mainContent.add( Log );
		mainContent.doLayout();
	},
	
	onEditErrorLog: function(grid, row, col) {
		var rec = grid.getStore().getAt(row);

		var view = this.getView('newIncidentLog.error_AddEdit').create();

		Ext.getCmp('production_member').setDisabled(false);
		Ext.getCmp('qa_member').setDisabled(false);
		Ext.getCmp('wor_id').setDisabled(false);
		Ext.getCmp('ErrSubCat').setDisabled(false);	
		Ext.getCmp('team_leader').setDisabled(false);
		Ext.getCmp('associate_manager').setDisabled(false);
		Ext.getCmp('error_notified_date').setMinValue(rec.get('error_notified_date'));
		Ext.getCmp('correction_date').setMinValue(rec.get('error_notified_date'));
		Ext.getCmp('corrective_action_date').setMinValue(rec.get('error_notified_date'));
		
		var addedFilePanel = Ext.create('Ext.form.Panel', {
			frame: false,
			padding: 2,
			margin: '0 10 0 0',
			layout: {
				type: 'hbox',
				align: 'middle'
			},
			items: [{
				xtype: 'image',
				src: AM.app.globals.uiPath+'resources/images/attach.png'
			}]
		});
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		//mainContent.add(addedFilePanel);
		mainContent.add(view);
		mainContent.doLayout();

		view.setTitle('Edit External Error Details');
		rec.set('Client', rec.get('client_id'));
		rec.set('wor_id', rec.get('wor_id'));
		rec.set('team_leader', rec.get('team_leader'));
		rec.set('associate_manager', rec.get('associate_manager'));
		
		rec.set('production_member', rec.get('production_member').split(','));
		rec.set('qa_member', rec.get('qa_member').split(','));

		if(rec.get('attachment'))
		{
			var str=rec.get('attachment');
			var str1='_'+rec.get('error_id');
			var res = str.replace(str1, "");
			res = res.substring(0,60)+'...';
			rec.set('errattach', res);
			rec.set('errattach_ori', rec.get('attachment'));
		}
		else
		{
			Ext.getCmp('attachErrFile').hide();
		}

		Ext.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit': "1",'VerticalID': rec.get('vertical_id'), 'comboClient':'1'
		};
		
		Ext.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit': "1",'comboEmployee':1,'utilCombo':1,'comboClient':rec.get('client_id')
		};
		
		Ext.getStore('workOrder').getProxy().extraParams = {
			'comboClient':rec.get('client_id')
		};

		Ext.getStore('Errorcategory').getProxy().extraParams = {
			'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':rec.get('client_id')
		};

		Ext.getStore('Errorsubcategory').getProxy().extraParams = {
			'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':rec.get('client_id'), 'parent_id':rec.get('error_category')
		};
		
		Ext.getStore('TeamLeaderIncident').getProxy().extraParams = {
			'client_id':rec.get('client_id'),
			'wor_id' : rec.get('wor_id'),
			'employee_type':'team_leader'
		};
		
		Ext.getStore('AssociateManagerIncident').getProxy().extraParams = {
			'client_id':rec.get('client_id'),
			'wor_id' : rec.get('wor_id'),
			'employee_type':'associate_manager'
		};

		view.down('form').loadRecord(rec);
		
		if(rec.data.impact_relationship=="Yes")
		{
			Ext.getCmp('errimpact_relationship').items.items[0].setValue(true);
		}
		if(rec.data.impact_business=="Yes")
		{
			Ext.getCmp('errimpact_business').items.items[0].setValue(true);
		}
	},
	
	OnCopyError : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.error_AddEdit').create();
		
		Ext.getCmp('production_member').setDisabled(false);
		Ext.getCmp('qa_member').setDisabled(false);
		Ext.getCmp('wor_id').setDisabled(false);
		Ext.getCmp('ErrSubCat').setDisabled(false);
		Ext.getCmp('team_leader').setDisabled(false);
		Ext.getCmp('associate_manager').setDisabled(false);
		Ext.getCmp('correction_date').setMinValue(rec.get('error_notified_date'));
		Ext.getCmp('corrective_action_date').setMinValue(rec.get('error_notified_date'));
		Ext.getCmp('attachErrFile').hide();
		//Ext.getCmp('download_err').hide();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		rec.set('id', '');
		rec.set('log_date', Ext.Date.format(new Date(), AM.app.globals.CommonDateControl));
		rec.set('error_id', 'ERR'+Ext.Date.format(new Date(), 'yhis'));
		rec.set('Client', rec.get('client_id'));
		rec.set('wor_id', rec.get('wor_id'));
		rec.set('team_leader', rec.get('team_leader'));
		rec.set('associate_manager', rec.get('associate_manager'));
		rec.set('attachment', '');
		
		rec.set('production_member', rec.get('production_member').split(','));
		rec.set('qa_member', rec.get('qa_member').split(','));
		//rec.set('domain_expert', rec.get('domain_expert').split(','));
		rec.set('client_partner', rec.get('client_partner').split(','));
		
		Ext.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit': "1", 'comboClient':'1'
		};
		
		Ext.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit': "1",'comboEmployee':1,'utilCombo':1,'comboClient':rec.get('client_id'),
		};
		
		Ext.getStore('workOrder').getProxy().extraParams = {
			'comboClient':rec.get('client_id')
		};
		Ext.getStore('Errorcategory').getProxy().extraParams = {
			'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':rec.get('client_id')
		};

		Ext.getStore('Errorsubcategory').getProxy().extraParams = {
			'hasNoLimit':"1",'utilCombo':1,'comboEmployee':1,'comboClient':rec.get('client_id'), 'parent_id':rec.get('error_category')
		};
		
		Ext.getStore('TeamLeaderIncident').getProxy().extraParams = {
			'client_id':rec.get('client_id'),
			'wor_id' : rec.get('wor_id'),
			'employee_type':'team_leader'
		};
		
		Ext.getStore('AssociateManagerIncident').getProxy().extraParams = {
			'client_id':rec.get('client_id'),
			'wor_id' : rec.get('wor_id'),
			'employee_type':'associate_manager'
		};

		view.down('form').loadRecord(rec);
		
		if(rec.data.impact_relationship=="Yes")
		{
			Ext.getCmp('errimpact_relationship').items.items[0].setValue(true);
		}
		if(rec.data.impact_business=="Yes")
		{
			Ext.getCmp('errimpact_business').items.items[0].setValue(true);
		}
	},
	
	OnViewError : function(grid, row, col) {

		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.errorView').create();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();	

		if(rec.get('attachment'))
		{
			var str=rec.get('attachment');
			var str1='_'+rec.get('error_id');
			var res = str.replace(str1, "");
			res = res.substring(0,60)+'...';
			rec.set('errattach', res);
			rec.set('errattach_ori', rec.get('attachment'));
		}
		else
		{
			Ext.getCmp('attachErrFile').hide();

		}

		view.down('form').loadRecord(rec);
		
		if(Ext.util.Cookies.get('grade') < 4)
		{
			Ext.getCmp('review').hide();
		}
		
		if (rec.get('reviewed_by') != null && rec.get('reviewed_by') != "" && rec.get('reviewed_by') != " ")
		{
			Ext.getCmp('review').hide();		
			if (rec.get('approved_by') == null || rec.get('approved_by') == "" || rec.get('approved_by') == " ")
			{							
				if (Ext.util.Cookies.get('grade') < 4 )
				{
					Ext.getCmp('approve').hide();
				}
				else
				{
					Ext.getCmp('approve').show();
				}	
			}
			else
			{
				Ext.getCmp('approve').hide();
			}
		}
		else
		{			
			Ext.getCmp('approve').hide();			
		}								
	},
	
	onSaveErrorLog : function(form) {
		var myStore = this.getStore('newErrorLog');
		var record = form.getRecord(), values = form.getValues();
		
		if(!record) 
		{
			myStore.add(values);
		}
		else
		{
			record.set(values);
		}
		myStore.sync({
			success: function (batch, operations) {
				var jsonResp = batch.proxy.getReader().jsonData;

				if(jsonResp.success) 
				{
					Ext.MessageBox.show({
						title: "Success",
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK
					});
					AM.app.getController('newIncidentLog').viewContent('error');
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
					win.close();
				}
			}, 
			failure: function (batch) {
				console.log("Error occured while saving accuracy. Please try again.");
				Ext.MessageBox.hide();
				myStore.rejectChanges();
			}
		});
	},
	
	
	OnDelete : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var myStore = this.getStore('newErrorLog');
		
		Ext.MessageBox.show({
			title: "Delete",
			msg: "Are you sure want to delete this log?",
			icon: Ext.MessageBox.WARNING,
			buttons: Ext.MessageBox.YESNO,
			fn: function(buttonId) {
				if (buttonId === "yes") {
					myStore.destroy(rec.data);

					Ext.MessageBox.show({
						title: "Success",
						msg: "Error log Deleted Successfully.",
						buttons: Ext.Msg.OK
					});
					
					myStore.load();
				}
			}
		});
	},

	onEditAppr: function(grid, row, col) {
		var rec = grid.getStore().getAt(row);

		var view = this.getView('newIncidentLog.appr_AddEdit').create();
		Ext.getCmp('wor_id').setDisabled(false);
		if(rec.get('attachment'))
		{
			var str=rec.get('attachment');
			var str1='_'+rec.get('appr_id');
			var res = str.replace(str1, "");
			res = res.substring(0,60)+'...';
			rec.set('aprattach', res);
			rec.set('aprattach_ori', rec.get('attachment'));
		}
		else
		{
			Ext.getCmp('attachApprFile').hide();
		}
		
		var addedFilePanel = Ext.create('Ext.form.Panel', {
			frame: false,
			padding: 2,
			margin: '0 10 0 0',
			layout: {
				type: 'hbox',
				align: 'middle'
			},
			items: [{
				xtype: 'image',
				src: AM.app.globals.uiPath+'resources/images/attach.png'
			}]
		});
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		view.setTitle('Edit Appreciation Details');
		
		rec.set('Client', rec.get('client_id'));
		rec.set('wor_id', rec.get('wor_id'));
		rec.set('responsible_member', rec.get('responsible_member').split(','));
		rec.set('team_leader', rec.get('team_leader'));
		
		Ext.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit': "1", 'comboClient':'1'
		};
		
		Ext.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit': "1",'comboEmployee':1,'utilCombo':1,'comboClient':rec.get('client_id')
		};
		
		Ext.getStore('workOrder').getProxy().extraParams = {
			'comboClient':rec.get('client_id')
		};
		Ext.getStore('TeamLeaderIncident').getProxy().extraParams = {
			'client_id':rec.get('client_id'),
			'wor_id' : rec.get('wor_id'),
			'employee_type':'team_leader'
		};
		
		view.down('form').loadRecord(rec);
	},
	
	
	OnDeleteAppr : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var myStore = this.getStore('apprIncidentLog');
		
		Ext.MessageBox.show({
			title: "Delete",
			msg: "Are you sure want to delete this appreciation?",
			icon: Ext.MessageBox.WARNING,
			buttons: Ext.MessageBox.YESNO,
			fn: function(buttonId) {
				if (buttonId === "yes") 
				{
					myStore.destroy(rec.data);

					Ext.MessageBox.show({
						title: "Success",
						msg: "Appreciation Deleted Successfully.",
						buttons: Ext.Msg.OK
					});
					
					myStore.load();
				}
			}
		});
	},
	
	onEditEscalationLog: function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.esc_AddEdit').create();
		Ext.getCmp('wor_id').setDisabled(false);
		
		var addedFilePanel = Ext.create('Ext.form.Panel', {
			frame: false,
			padding: 2,
			margin: '0 10 0 0',
			layout: {
				type: 'hbox',
				align: 'middle'
			},
			items: [{
				xtype: 'image',
				src: AM.app.globals.uiPath+'resources/images/attach.png'
			}]
		});
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		view.setTitle('Edit Escalation Details');
		rec.set('client', rec.get('client_id'));
		rec.set('wor_id', rec.get('wor_id'));
		rec.set('production_member', rec.get('production_member').split(','));

		if(rec.get('attachment'))
		{
			var str=rec.get('attachment');
			var str1='_'+rec.get('error_id');
			var res = str.replace(str1, "");
			res = res.substring(0,60)+'...';
			rec.set('escattach', res);
			rec.set('escattach_ori', rec.get('attachment'));
		}
		else
		{
			Ext.getCmp('attachEscFile').hide();
		}
		Ext.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit': "1",'comboClient':'1'
		};
		Ext.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit': "1",'utilCombo':1,'comboClient':rec.get('client_id')
		};

		Ext.getStore('workOrder').getProxy().extraParams = {
			'comboClient':rec.get('client_id')
		};
		
		view.down('form').loadRecord(rec);
		
		if(rec.data.impact_relationship=="Yes")
		{
			Ext.getCmp('impact_relationship').items.items[0].setValue(true);
		}
		if(rec.data.impact_business=="Yes")
		{
			Ext.getCmp('impact_business').items.items[0].setValue(true);
		}
	},
	
	OnCopyEscalation : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.esc_AddEdit').create();
		Ext.getCmp('wor_id').setDisabled(false);
		
		Ext.getCmp('production_member').setDisabled(false);
		Ext.getCmp('manager').setDisabled(false);
		Ext.getCmp('attachEscFile').hide();
		//Ext.getCmp('download_esc').hide();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		rec.set('id', '');
		rec.set('log_date', Ext.Date.format(new Date(), AM.app.globals.CommonDateControl));
		rec.set('error_id', 'ESC'+Ext.Date.format(new Date(), 'yhis'));
		rec.set('client', rec.get('client_id'));
		rec.set('wor_id', rec.get('wor_id'));
		rec.set('attachment', '');
		
		rec.set('production_member', rec.get('production_member').split(','));

		Ext.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit': "1", 'comboClient':'1'
		};
		
		Ext.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit': "1",'comboEmployee':1,'utilCombo':1,'comboClient':rec.get('client_id'),
		};
		
		Ext.getStore('workOrder').getProxy().extraParams = {
			'comboClient':rec.get('client_id')
		};
		
		view.down('form').loadRecord(rec);
		
		if(rec.data.impact_relationship=="Yes")
		{
			Ext.getCmp('impact_relationship').items.items[0].setValue(true);
		}
		if(rec.data.impact_business=="Yes")
		{
			Ext.getCmp('impact_business').items.items[0].setValue(true);
		}
	},
	
	OnCopyAppr : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.appr_AddEdit').create();
		
		Ext.getCmp('wor_id').setDisabled(false);
		Ext.getCmp('attachApprFile').hide();
		//Ext.getCmp('download_apr').hide();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		rec.set('id', '');
		rec.set('log_date', Ext.Date.format(new Date(), AM.app.globals.CommonDateControl));
		rec.set('appr_id', 'APPR'+Ext.Date.format(new Date(), 'yhis'));
		rec.set('client', rec.get('client_id'));
		rec.set('wor_id', rec.get('wor_id'));
		rec.set('attachment', '');
		
		rec.set('responsible_member', rec.get('responsible_member').split(','));
		rec.set('team_leader', rec.get('team_leader'));

		Ext.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit': "1", 'comboClient':'1'
		};
		
		Ext.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit': "1",'comboEmployee':1,'utilCombo':1,'comboClient':rec.get('client_id'),
		};
		
		Ext.getStore('workOrder').getProxy().extraParams = {
			'comboClient':rec.get('client_id')
		};
		Ext.getStore('TeamLeaderIncident').getProxy().extraParams = {
			'client_id':rec.get('client_id'),
			'wor_id' : rec.get('wor_id'),
			'employee_type':'team_leader'
		};
		
		view.down('form').loadRecord(rec);
	},
	
	OnViewEscalation : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.escView').create();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		if(rec.get('attachment'))
		{
			var str=rec.get('attachment');
			var str1='_'+rec.get('error_id');
			var res = str.replace(str1, "");
			res = res.substring(0,60)+'...';
			rec.set('escattach', res);
			rec.set('escattach_ori', rec.get('attachment'));
		}
		else
		{
			Ext.getCmp('attachEscFile').hide();

		}
		
		view.down('form').loadRecord(rec);
		
		if(Ext.util.Cookies.get('grade') < 4)
		{
			Ext.getCmp('review').hide();
		}
		
		if (rec.get('reviewed_by') != null && rec.get('reviewed_by') != "" && rec.get('reviewed_by') != " ")
		{
			Ext.getCmp('review').hide();		
			if (rec.get('approved_by') == null || rec.get('approved_by') == "" || rec.get('approved_by') == " ")
			{								
				if (Ext.util.Cookies.get('grade') > 4)
				{
					Ext.getCmp('approve').show();
				}
				else
				{
					Ext.getCmp('approve').hide();
				}	
			}
			else
			{
				Ext.getCmp('approve').hide();
			}
		}
		else
		{			
			Ext.getCmp('approve').hide();			
		}
	},
	
	OnViewAppr : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = this.getView('newIncidentLog.apprView').create();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		mainContent.removeAll();
		mainContent.add(view);
		mainContent.doLayout();

		if(rec.get('attachment'))
		{
			var str=rec.get('attachment');
			var str1='_'+rec.get('appr_id');
			var res = str.replace(str1, "");
			res = res.substring(0,60)+'...';
			rec.set('aprattach', res);
			rec.set('aprattach_ori', rec.get('attachment'));
		}
		else
		{
			Ext.getCmp('attachApprFile').hide();


		}
		
		view.down('form').loadRecord(rec);
	},
	
	OnDeleteEscalation : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var myStore = this.getStore('newEscLog');
		
		Ext.MessageBox.show({
			title: "Delete",
			msg: "Are you sure want to delete this log?",
			icon: Ext.MessageBox.WARNING,
			buttons: Ext.MessageBox.YESNO,
			fn: function(buttonId) {
				if (buttonId === "yes") {
					myStore.destroy(rec.data);

					Ext.MessageBox.show({
						title: "Success",
						msg: "Escalation log Deleted Successfully.",
						buttons: Ext.Msg.OK
					});
					
					myStore.load();
				}
			}
		});
	},
	
	/* Review/Approve for External Errors */
	reviewApproveError: function(error_id, type, client_id)
	{			
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/newIncidentLog/reviewApproveError/',
			method: 'POST',
			timeout: 1800000,
			params: {'id':error_id, 'type':type, 'client_id':client_id},
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);
				if(jsonResp.success)
				{ 
					myMask.hide();
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								AM.app.getController('newIncidentLog').viewContent('error');
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
					});
				}
			}
		});
	},
	
	/* Review/Approve for Escalations */
	reviewApprove: function(esc_id, type, client_id)
	{			
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/newIncidentLog/reviewApprove/',
			method: 'POST',
			timeout: 1800000,
			params: {'id':esc_id, 'type':type, 'client_id':client_id},
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);
				if(jsonResp.success)
				{ 
					myMask.hide();
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								AM.app.getController('newIncidentLog').viewContent('esc');
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Escalation',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
					});
				}
			}
		});
	},
	
	errfileDownload: function(attach)
	{			
		window.open(AM.app.globals.uiPath+'resources/uploads/incidentLogDocs/errorDocs/'+attach);
	},
	
	escfileDownload: function(attach)
	{			
		window.open(AM.app.globals.uiPath+'resources/uploads/incidentLogDocs/EscalationDocs/'+attach);
	},
	
	aprfileDownload: function(attach)
	{			
		window.open(AM.app.globals.uiPath+'resources/uploads/incidentLogDocs/ApprDocs/'+attach);
	},
	aprfileDelete: function(attach_id)
	{	Ext.MessageBox.show({
		title: "Delete",
		msg: "Are you sure want to delete this file?",
		icon: Ext.MessageBox.WARNING,
		buttons: Ext.MessageBox.YESNO,
		fn: function(buttonId) {
			if (buttonId === "yes") {
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/newIncidentLog/deletefile/',
					method: 'POST',
					params: {'id':attach_id,'type':'appr'},
					success: function(responce, opts){
						var jsonResp = Ext.decode(responce.responseText);
						console.log(jsonResp);
						if(jsonResp.success)
						{ 

							Ext.MessageBox.show({
								title: jsonResp.title,
								msg: jsonResp.msg,
								buttons: Ext.Msg.OK,
								fn: function(buttonId) {
									if (buttonId === "ok") 
									{
										AM.app.getController('newIncidentLog').viewContent('appr');
									}
								}
							});
						}
						else 
						{
							Ext.MessageBox.show({
								title: 'Delete file',
								msg: jsonResp.msg,
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK,
							});
						}
					}
				});
			}
		}
	});		

},
escfileDelete: function(attach_id)
{	Ext.MessageBox.show({
	title: "Delete",
	msg: "Are you sure want to delete this file?",
	icon: Ext.MessageBox.WARNING,
	buttons: Ext.MessageBox.YESNO,
	fn: function(buttonId) {
		if (buttonId === "yes") {
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/newIncidentLog/deletefile/',
				method: 'POST',
				params: {'id':attach_id,'type':'esc'},
				success: function(responce, opts){
					var jsonResp = Ext.decode(responce.responseText);
					console.log(jsonResp);
					if(jsonResp.success)
					{ 

						Ext.MessageBox.show({
							title: jsonResp.title,
							msg: jsonResp.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									AM.app.getController('newIncidentLog').viewContent('esc');
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: 'Delete file',
							msg: jsonResp.msg,
							icon: Ext.MessageBox.ERROR,
							buttons: Ext.Msg.OK,
						});
					}
				}
			});
		}
	}
});		

},
errfileDelete: function(attach_id)
{	Ext.MessageBox.show({
	title: "Delete",
	msg: "Are you sure want to delete this file?",
	icon: Ext.MessageBox.WARNING,
	buttons: Ext.MessageBox.YESNO,
	fn: function(buttonId) {
		if (buttonId === "yes") {
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/newIncidentLog/deletefile/',
				method: 'POST',
				params: {'id':attach_id,'type':'error'},
				success: function(responce, opts){
					var jsonResp = Ext.decode(responce.responseText);
					console.log(jsonResp);
					if(jsonResp.success)
					{ 

						Ext.MessageBox.show({
							title: jsonResp.title,
							msg: jsonResp.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									AM.app.getController('newIncidentLog').viewContent('error');
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: 'Delete file',
							msg: jsonResp.msg,
							icon: Ext.MessageBox.ERROR,
							buttons: Ext.Msg.OK,
						});
					}
				}
			});
		}
	}
});		

},

exportExternalError:function()
{
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
	myMask.show();
	Ext.Ajax.request( {
		url: AM.app.globals.appPath+'index.php/newIncidentLog/downloadExternalErrorCsv/',
		method: 'GET',
		params: {'downloadCsv':1},
		success: function(responce, opts){
			var jsonResp = Ext.decode(responce.responseText);
			if(jsonResp.success) {	
				myMask.hide();
				window.location = AM.app.globals.appPath+'download/ExternalError.csv';
			}
		}
	});
},

exportEscalation:function()
{
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
	myMask.show();
	Ext.Ajax.request( {
		url: AM.app.globals.appPath+'index.php/newIncidentLog/downloadEscalationCsv/',
		method: 'GET',
		params: {'downloadCsv':1},
		success: function(responce, opts){
			var jsonResp = Ext.decode(responce.responseText);
			if(jsonResp.success) {	
				myMask.hide();
				window.location = AM.app.globals.appPath+'download/Escalation.csv';
			}
		}
	});
}
});