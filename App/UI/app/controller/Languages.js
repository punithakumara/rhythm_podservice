Ext.define('AM.controller.Languages',{
	extend : 'Ext.app.Controller',
	//store : 'Tool',
	views: ['Skills.LanguageList','Skills.Language_Add_Edit'],
	
	viewContent : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var tool = this.getView('Skills.LanguageList').create();
		mainContent.removeAll();
		mainContent.add( tool );
		mainContent.doLayout();
	},
	
	newTechnology : function(verticalId) {
		var view = Ext.widget('LanguageAddEdit');
	},
	
	// onTriggerKeyUp : function(t) {
		// Ext.Ajax.abortAll();
		//var store = this.getStore('Tool');
		//store.filters.clear();
        //store.filter({
           //property: 'tool.Name, tool.Description',
            // value: t.getValue()
        // });
    // },
    
    // onTriggerClear : function() {
        // var store = this.getStore('Tool');
        // store.clearFilter();
    // },
	
	onCreate : function() {
		var view = Ext.widget('LanguageAddEdit');
		view.setTitle('Add Language');
	},
	
	// onSaveTool : function(form, win) {
		
		// var myStore = this.getStore('Tool');
		// var record = form.getRecord(),
		// values = form.getValues();

		// if(!record) 
		// {
			// form.url = myStore.getProxy().api.create;
			// myStore.add(values);
			// var titleText = 'Added';
		// }
		// else 
		// {
			// form.url = myStore.getProxy().api.update;
			// record.set(values);
			// var titleText = 'Updated';
		// }		
		// form.submit({ 
			// method: 'POST',
            // waitMsg : 'Saving your data, please wait...',
		    // success: function (form, o) {
		    	// var jsonResp = JSON.parse(o.response.responseText);
		    	// if(jsonResp.success) 
				// {
					// if(jsonResp.title=="Updated") 
					// {
						// win.close();
					// }
		    		// Ext.MessageBox.show({
                        // title: jsonResp.title,
                        // msg: jsonResp.msg,
                        // buttons: Ext.Msg.OK
                    // });
		    		// if(jsonResp.title!="Existing") 
					// {
		    			// win.close();
					// }
		    		// if(jsonResp.title=="Existing") 
					// {
						// if(titleText != "Added"){myStore.load();win.close();}
						// myStore.load();
					// }	
		    	// }
		    	// else 
				// {
		    		// Ext.MessageBox.show({
                        // title: 'Error',
                        // msg: jsonResp.msg,
                        // icon: Ext.MessageBox.ERROR,
                        // buttons: Ext.Msg.OK
                    // });
		    		// win.close();
		    	// }
				
				// myStore.load();
		    // }, 
		    // failure: function (batch) {
		    	// console.log("Error occured while saving technology. Please try again.");
		    	// Ext.MessageBox.hide();
		    	// myStore.rejectChanges();
		    // }
		// });
	// },
	
	// onEditTool : function(grid, row, col) {
		// var rec = grid.getStore().getAt(row);
		// var view = Ext.widget('toolAddEdit');
		//console.log(rec);
		// view.setTitle('Edit Tool Details');
        // view.down('form').loadRecord(rec);
	// },
	
	// onDeleteTool : function(grid, row, col) {
		// var rec = grid.getStore().getAt(row);
		// var techName = rec.get('Name');
		// Ext.Msg.confirm('Delete Tool', 'Are you sure to delete '+techName+' ?', function (button) {
            // if (button == 'yes') 
			// {
        		// grid.store.remove(rec);
        		// grid.store.sync({ 
        		    // success: function (batch, operations) {
        		    	
        		    	// var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	// if(jsonResp.success) 
						// {
        		    		// Ext.MessageBox.show({
                                // title: 'Deleted',
                                // msg: jsonResp.msg,
                                // buttons: Ext.Msg.OK
                            // });
        		    	// }
        		    	// else 
						// {
        		    		// Ext.MessageBox.show({
                                // title: 'Error',
                                // msg: jsonResp.msg,
                                // icon: Ext.MessageBox.ERROR,
                                // buttons: Ext.Msg.OK
                            // });
        		    	// }
        		    	// grid.store.load();
        		    // }, 
        		    // failure: function (batch) {
        		    	// console.log("Error occured while deleting tool. Please try again.");
        		    	// grid.store.rejectChanges();
        		    // }
        		// });
            // }
        // }, this);
	// },

});