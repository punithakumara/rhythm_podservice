Ext.define('AM.controller.Attributes',{
	extend : 'Ext.app.Controller',
	
	stores : ['Attributes','attributesType'],
	views: ['attributes.List','attributes.Add_edit'],
	
	init : function() {
		this.control({
			'attributesList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            }
		});
	},
	
	viewContent : function() {
		delete this.getStore('Attributes').getProxy().extraParams;
		var store = this.getStore('Attributes');
        store.clearFilter();
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var attribute = this.getView('attributes.List').create();
		mainContent.removeAll();
		mainContent.add( attribute );
		mainContent.doLayout();
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Attributes');
		store.filters.clear();
        store.filter({
            property: 'label_name, field_type, description',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Attributes');
        store.clearFilter();
    },
	
	onCreate : function() {
		var view = Ext.widget('attributesAddEdit');
		view.setTitle('Add Attribute Details');
	},
	
	onSaveAttributes : function(form, win) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
        });
		var myStore = this.getStore('Attributes');
		var record = form.getRecord(),
		values = form.getValues();
		if(!record) {
			myStore.add(values);
			var titleText = 'Added';
		}
		else {
			record.set(values);
			var titleText = 'Updated';
		}		
		myStore.sync({ 
		    success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;
				if(jsonResp.title=="Updated") {
					win.close();
				}		    	
		    	if(jsonResp.success) {
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });					
		    		if(jsonResp.title!="Existing") {
		    			win.close();
					}
					if(jsonResp.title=="Existing") {
					store.clearFilter();
					}	
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving attribute. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditAttributes : function(grid, row, col) {

		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('attributesAddEdit');
		var ListValues = rec.get('ListValues');		
		var ValuesStatus = rec.get('ValuesStatus');
		view.setTitle('Edit Attributes Details');

		if(rec.get('type') == 'list' && rec.get('ListValues').length > 0 && rec.get('ValuesStatus').length > 0){		
			this.onAddRow(view.down('form'), ListValues, ValuesStatus);
			
		}
		if(rec.get('type') == 'list'){
			view.down('form').query('fieldcontainer')[0].show();
			
		}
		
        view.down('form').loadRecord(rec);
		view.down('form').query('fieldcontainer')[0].items.items[0].setValue(ListValues[0]);
		view.down('form').query('fieldcontainer')[1].items.items[1].setValue(ValuesStatus[0]);
		Ext.getCmp('attrfirst_text').setValue("");
		
	
	},
	
	onDeleteAttributes : function(grid, row, col) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
         });
		var rec = grid.getStore().getAt(row);
		var attributeName = rec.get('name');
		Ext.Msg.confirm('Delete Attributes', 'Are you sure to delete '+attributeName+' ?', function (button) {
            if (button == 'yes') {
            	Ext.MessageBox.show({
                    msg: 'Saving your data, please wait...',
                    progressText: 'Saving...',
                    width:300,
                    wait:true,
                    waitConfig: {interval:200},
                    animateTarget: 'waitButton'
                 });
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) {
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else {
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting attribute. Please try again.");
        		    	Ext.MessageBox.hide();
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	onAddRow: function(form, listValues, ValuesStatus) {

		var formObj = [];
		for(var i = 0; i < listValues.length; i++) {
					
			if(listValues == "" || ValuesStatus == "") {
				Ext.Msg.alert('Alert', 'Please Enter Desired Value');
				return false;
			} else {	
				
				var statusStore = new Ext.data.SimpleStore({
					fields: ['opvalue', 'ValuesStatus'],
					data: [
					       ['0', 'Disabled'],
					       ['1', 'Active'],			       
					      ]
				});
				
				
				Ext.getCmp('attrfirst_text').setValue("");
				var formElem = {
					xtype		: 'fieldcontainer',
					layout		: 'hbox',
					hideLabel	: true,
					items		: [{
						xtype			: 'textfield',
						fieldLabel		: ' ',
						labelSeparator	: "",
						name			: 'ListValues',
						style			: { 'margin-right' : '10px', 'float': 'right' },
						anchor			: '100%',
						flex			: 5,
						value   		: listValues[i],
						allowBlank		: false
					},
					
					{
						 xtype : 'boxselect',
						 multiSelect: false,
						 name: 'ValuesStatus',
						 style: { 'margin-right' : '10px', 'float': 'right' },
						 store: statusStore,
						 queryMode: 'local',
						 minChars:0,
						 displayField: 'ValuesStatus', 						
						 valueField: 'opvalue',
						 anchor: '100%',
						 flex : 3,
						 value : ValuesStatus[i],
						 allowBlank		: false
					 },
					
										
					 {
						xtype			: 'button',
						text 			: 'Delete',
						flex 			: 1,
						handler 		: function(btn) {
							var form = btn.up('window').down('form');
							form.remove(this.up());
							form.doLayout();
						}
					}]
				};
				formObj.push(formElem);
			}	
	    }			
		form.add(formObj);
		form.doLayout();	
	
	},
});