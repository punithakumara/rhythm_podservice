Ext.define('AM.controller.ClientWOR',{
	extend : 'Ext.app.Controller',
	stores : ['Clients','Services','Responsibilities','WorProjects','SupportCoverage','ClientCOR','ClientWOR','Employees','Shifts','ChangeType','WorTypes','Roles','summaryStore','WorkTypeFTE','WorkTypeHourly', 'WorkTypeVolume','DurationCoverage','WorkTypeProject','WorList','ChangeModel','corWorkTypeFTE','corWorkTypeHourly', 'corWorkTypeVolume','corWorkTypeProject', 'existingModel','newModel','Transition','ProjectTypes','Stakeholders','TimeEntryApprovers','Deliverymanagers','Engagementmanagers','Clientpartner','AllReportees','AllReportees1','AllocatedResources','AllocatedRes','TeamLeader','AssociateManager','clientHolidayDetails','roleCode','Holidays','Attributes','Categorization','YesNo','billableType','Employees','fteDetailsStore','clientattributes','ClientAddAttr'],
	views: ['wor_cor.ListWor','wor_cor.AddWor','wor_cor.EditWor','wor_cor.ViewWor','wor_cor.SummaryList','wor_cor.FTEList','wor_cor.HourlyList','wor_cor.VolumeBasedList','wor_cor.AddCor','wor_cor.ProjectList','wor_cor.viewPopup','wor_cor.AssignClientHolidays','wor_cor.ExtendWor','wor_cor.AssignResources','wor_cor.ViewRes','wor_cor.ConfigureAttributes','wor_cor.people_billability'],
	
	init : function() {
		this.control({
			'worList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
		});
		this.control({
			'AssignResources #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
		});
		this.control({
			'ViewRes #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
		});
	},

	initWOR : function(wor_status) {
		AM.app.globals.transEmployRes = [];
		//delete this.getStore('ClientWOR').getProxy().extraParams;
		
		var myStore = this.getStore('ClientWOR');
		myStore.clearFilter();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var status='All';
		if (wor_status != undefined || wor_status == '')
		{
			var status = wor_status;
		}
		
		myStore.getProxy().extraParams = {
			'status' : status,
		};				
		myStore.load();
		var scStore = this.getStore('SupportCoverage');
		scStore.load();
		var clients = this.getView('wor_cor.ListWor').create();
		mainContent.removeAll();
		mainContent.add( clients );
		mainContent.doLayout();	
		if(Ext.util.Cookies.get('grade') <4)
		{
			Ext.getCmp('addwor').setVisible(false);
		}
		
		Ext.getCmp('WORStatusComboBox').setValue(status);
	},
	
	onCreateWOR : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('wor_cor.AddWor').create();
		
		Ext.getCmp('hourly_list').store.removeAll();
		Ext.getCmp('fte_list').store.removeAll();
		Ext.getCmp('volume_list').store.removeAll();
		Ext.getCmp('project_list').store.removeAll();  
		
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
	},

	onEditWOR : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var rec = grid.getStore().getAt(row);
		
		AM.app.getStore('Clients').getProxy().extraParams = {
			'hasNoLimit'	:'1',
		};
		
		AM.app.getStore('WorTypes').getProxy().extraParams = {
			'hasNoLimit'	:'1',
		};
		
		AM.app.getStore('Employees').getProxy().extraParams = {
			'hasNoLimit'	:	"1", 
			'empTypes'		:   "grade5AndAbove",  
			'filterName'	: 	'first_name'
		};			
		
		AM.app.getStore('TeamLeader').getProxy().extraParams = {
			'hasNoLimit'	:'1', 
			'employee_type':'team_leader'
		};	
		
		AM.app.globals.wor_cor_combostatus = Ext.getCmp('WORStatusComboBox').getValue();
		
		this.getStore('WorkTypeFTE').load({params: {'wor_id':rec.get('wor_id')}});		
		this.getStore('WorkTypeHourly').load({params: {'wor_id':rec.get('wor_id')}});	
		this.getStore('WorkTypeVolume').load({params: {'wor_id':rec.get('wor_id')}});
		this.getStore('WorkTypeProject').load({params: {'wor_id':rec.get('wor_id')}});
		
		rec.data.status = "In-Progress";
		
		var wor = this.getView('wor_cor.EditWor').create();
		
		if((rec.get('associate_manager')!="") && (rec.get('associate_manager')!=null))
		{
			rec.set('associate_manager', rec.get('associate_manager').split(','));
		}
		if((rec.get('team_lead')!="") && (rec.get('team_lead')!=null))
		{
			rec.set('team_lead', rec.get('team_lead').split(','));
		}
		if((rec.get('stake_holder')!="") && (rec.get('stake_holder')!=null))
		{
			rec.set('stake_holder', rec.get('stake_holder').split(','));
		}
		if((rec.get('wor_type_id')!="") && (rec.get('wor_type_id')!=null))
		{
			rec.set('wor_type_id', rec.get('wor_type_id').split(','));
		}
		
		Ext.getCmp('editStatus').bindStore(Ext.create('Ext.data.Store', {
			fields: ['Flag', 'Status'],
			data: [
			{'Flag': 'In-Progress', 'Status': 'In-Progress'},
			{'Flag': 'Deleted', 'Status': 'Delete'},
			]
		}));

		wor.down('form').loadRecord(rec);	

		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
		
		Ext.getCmp('actionColumn').setVisible(false);
	},
	
	onViewWOR : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		
		AM.app.getStore('TimeEntryApprovers').getProxy().extraParams = {
			'wor_id'	:rec.data.wor_id, 
		};
		
		AM.app.globals.wor_cor_combostatus = Ext.getCmp('WORStatusComboBox').getValue();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var view = this.getView('wor_cor.ViewWor').create();
		mainContent.removeAll();
		mainContent.add( view );
		mainContent.doLayout();

		// Hide & show of Save button and Description field
		if(rec.data.status=="In-Progress")
		{   
			Ext.getCmp('saveButtonID').setVisible(false);    
			Ext.getCmp('ViewWORDescription').setVisible(false); 
			Ext.getCmp('inprogressStatus').setVisible(true);		   
		}
		else if(rec.data.status=="Open")
		{
			Ext.getCmp('addResources').setVisible(true);
			Ext.getCmp('viewResources').setVisible(true);
			Ext.getCmp('peopleBillability').setVisible(true);
			if(Ext.util.Cookies.get('grade')>=3)
			{
				Ext.getCmp('configureFields').setVisible(true);
				Ext.getCmp('assignHolidays').setVisible(true);
			}

			Ext.getCmp('openStatus').setVisible(true);
			Ext.getCmp('saveButtonID').setVisible(false);
			if(Ext.util.Cookies.get("grade") >= 4)
			{
				Ext.getCmp('addCor').setVisible(true);
				Ext.getCmp('extendWOR').setVisible(true);

				Ext.getCmp('cp_edit').setVisible(true);
				Ext.getCmp('em_edit').setVisible(true);
				Ext.getCmp('dm_edit').setVisible(true);
				Ext.getCmp('am_edit').setVisible(true);
				Ext.getCmp('tl_edit').setVisible(true);
				Ext.getCmp('sh_edit').setVisible(true);
				Ext.getCmp('ta_edit').setVisible(true);
			}
		}
		else if(rec.data.status=="Closed" || rec.data.status=="Deleted")
		{
			Ext.getCmp('inprogressStatus').setVisible(true);
			Ext.getCmp('saveButtonID').setVisible(false);
			Ext.getCmp('ViewWORDescription').setVisible(true);
			Ext.getCmp('ViewWORDescription').setReadOnly(true);
			Ext.getCmp('saveButtonID').setVisible(false);
			Ext.getCmp('addResources').setVisible(false);
			Ext.getCmp('viewResources').setVisible(false);
			Ext.getCmp('peopleBillability').setVisible(false);
			Ext.getCmp('configureFields').setVisible(false);
			Ext.getCmp('assignHolidays').setVisible(false);
			Ext.getCmp('addCor').setVisible(false);
		}
		if(rec.data.corids==null)
		{
			Ext.getCmp('corids').setVisible(false);
		}

		if(Ext.util.Cookies.get("grade") < 4)
		{
			Ext.getCmp('openStatus').setDisabled(true);
		}
		
		view.setTitle('View '+rec.data.work_order_id+' Details');

		var myPanel = Ext.getCmp('myTabPanel');
		
		myPanel.add({
			title: 'Summary',
			items : [{								
				xtype:AM.app.getView('wor_cor.SummaryList').create(),
				hidden:false
			}]
		});
		
		myPanel.add({
			title: 'FTE',
			items : [{        
				xtype:AM.app.getView('wor_cor.FTEList').create(),
				hidden:false
			}]
		});
		
		myPanel.add({
			title: 'Hourly',
			items : [{        
				xtype:AM.app.getView('wor_cor.HourlyList').create(),
				hidden:false
			}]
		});

		myPanel.add({
			title: 'Volume Based',
			items : [{        
				xtype:AM.app.getView('wor_cor.VolumeBasedList').create(),
				hidden:false
			}]
		});

		myPanel.add({
			title: 'Project',
			items : [{        
				xtype:AM.app.getView('wor_cor.ProjectList').create(),
				hidden:false
			}]
		});

		view.down('form').loadRecord(rec);

		myPanel.setActiveTab(0);// to display Summary tab as Default Tab
		
		/**
			Pass  'level': rec.data.count_history as argument in the params to make the Previous Next option to be enabled
			**/
			this.getStore('WorkTypeFTE').load({params: {'wor_id':rec.data.wor_id},
				callback : function(records)
				{
					if(records.length == 0)
					{
						myPanel.items.getAt(1).setDisabled(true);
					}

				}
			});

			this.getStore('WorkTypeHourly').load({params: {'wor_id':rec.data.wor_id},
				callback : function(records)
				{
					if(records.length == 0)
					{
						myPanel.items.getAt(2).setDisabled(true);
					}

				}
			});

			this.getStore('WorkTypeProject').load({params: {'wor_id':rec.data.wor_id},
				callback : function(records)
				{
					if(records.length == 0)
					{
						myPanel.items.getAt(4).setDisabled(true);
					}

				}
			});

			this.getStore('WorkTypeVolume').load({params: {'wor_id':rec.data.wor_id},
				callback : function(records)
				{
					if(records.length == 0)
					{
						myPanel.items.getAt(3).setDisabled(true);
					}

				}
			});

			this.getStore('summaryStore').load({params: {'wor_id':rec.data.wor_id,'level': rec.data.count_history,'work_order_id': rec.data.id},});


			/* disable Add Request Button in WOR view */
			Ext.getCmp('fte_list').tools['0']="";
			Ext.getCmp('hourly_list').tools['0']="";
			Ext.getCmp('project_list').tools['0']="";
			Ext.getCmp('volume_list').tools['0']=""; 

			var fteGrid = Ext.getCmp('fte_list');
			fteGrid.columns[6].setText('Anticipated Impact Date');
			fteGrid.columns[6].tooltip = 'Anticipated Impact Date';

			var hourGrid = Ext.getCmp('hourly_list');
			hourGrid.columns[7].setText('Anticipated Impact Date');
			hourGrid.columns[7].tooltip = 'Anticipated Impact Date';

			var volumeGrid = Ext.getCmp('volume_list');
			volumeGrid.columns[7].setText('Anticipated Impact Date');
			volumeGrid.columns[7].tooltip = 'Anticipated Impact Date';

			var projectGrid = Ext.getCmp('project_list');
			projectGrid.columns[8].setText('Anticipated Impact Date');
			projectGrid.columns[8].tooltip = 'Anticipated Impact Date';
		},	

		onStatusChange: function(combo){
			var store = this.getStore('ClientWOR');
			store.getProxy().extraParams = {'status': combo.getValue()};
			store.load();
		},

		onSaveWOR : function(form, status) {		
			var myStore = this.getStore('ClientWOR');
			var record = form.getRecord(), values = form.getValues();
			var wor_id = values['wor_id'];
			var is_client_exists = 0;
			var is_date_validation_exists = 0;
			var is_fields_null = 0;
			values['status'] = status;

			var start_date = values['start_date'];
			var end_date = values['end_date']; 
		// start_date = Ext.Date.format(start_date, 'd-M-Y');
		// end_date = Ext.Date.format(end_date, 'd-M-Y');
		
		var index = Ext.StoreMgr.lookup("Clients").findExact('id',values['client']);
		var reca = Ext.StoreMgr.lookup("Clients").getAt(index);
		
		if(Ext.getCmp('fte_list').store.data.items.length != 0 || Ext.getCmp('hourly_list').store.data.items.length != 0 || Ext.getCmp('volume_list').store.data.items.length != 0 || Ext.getCmp('project_list').store.data.items.length != 0)
		{			
			var fte_list = Ext.encode(Ext.pluck(Ext.getCmp('fte_list').store.data.items, 'data'));
			if (Ext.getCmp('fte_list').store.data.items.length != 0 )
			{
				Ext.each(Ext.getCmp('fte_list').store.data.items, function(value, i) { 
					if((isNaN(Number(value.data.support_coverage))) || (isNaN(Number(value.data.responsibilities))) || (isNaN(Number(value.data.shift))))
					{
						is_fields_null = 1;							
					}											
					var asd = new Date(value.data.anticipated_date);
					//var asd = Ext.Date.format(asd, 'd-M-Y');
					if(asd < start_date || asd > end_date )
					{
						if(asd < start_date || asd > end_date )
						{
							is_date_validation_exists = 1;
						}
					}
				});
			}
			
			if (Ext.getCmp('hourly_list').store.data.items.length != 0 )
			{
				Ext.each(Ext.getCmp('hourly_list').store.data.items, function(value, i) {
					if((isNaN(Number(value.data.support_coverage))) || (isNaN(Number(value.data.responsibilities))) || (isNaN(Number(value.data.shift))))
					{
						is_fields_null = 1;							
					}
					var asd = new Date(value.data.anticipated_date);
					// var asd = Ext.Date.format(asd, 'd-M-Y');
					if(asd < start_date || asd > end_date )
					{
						if(asd < start_date || asd > end_date )
						{
							is_date_validation_exists = 1;
						}	
					}
				});
			}
			
			if (Ext.getCmp('volume_list').store.data.items.length != 0 )
			{
				Ext.each(Ext.getCmp('volume_list').store.data.items, function(value, i) {      
					if((isNaN(Number(value.data.support_coverage))) || (isNaN(Number(value.data.responsibilities))) || (isNaN(Number(value.data.shift))))
					{
						is_fields_null = 1;							
					}
					var asd = new Date(value.data.anticipated_date);
					// var asd = Ext.Date.format(asd, 'd-M-Y');
					if(asd < start_date || asd > end_date )
					{
						if(asd < start_date || asd > end_date )
						{
							is_date_validation_exists = 1;
						}	
					}
				});
			}
			
			if (Ext.getCmp('project_list').store.data.items.length != 0 )
			{
				Ext.each(Ext.getCmp('project_list').store.data.items, function(value, i) {      

					if((isNaN(Number(value.data.support_coverage))) || (isNaN(Number(value.data.responsibilities))) || (isNaN(Number(value.data.shift))))
					{
						is_fields_null = 1;							
					}
					var asd = new Date(value.data.anticipated_date);
					// var asd = Ext.Date.format(asd, 'd-M-Y');
					if(asd < start_date || asd > end_date )
					{
						if(asd < start_date || asd > end_date )
						{
							is_date_validation_exists = 1;
						}	
					}
				});
			}
			//console.log(is_date_validation_exists);
			var hourly_list = Ext.encode(Ext.pluck(Ext.getCmp('hourly_list').store.data.items, 'data'));
			var volume_list = Ext.encode(Ext.pluck(Ext.getCmp('volume_list').store.data.items, 'data'));
			var project_list = Ext.encode(Ext.pluck(Ext.getCmp('project_list').store.data.items, 'data'));
			myStore.getProxy().extraParams = {'fte_details':fte_list, 'hourly_details':hourly_list, 'volume_list' : volume_list, 'project_list' : project_list,'client_name' :reca.data.client_name  };
		}
		else
		{
			Ext.Msg.alert("Alert", "Please Select Atleast one Model");
			Ext.getCmp('addSave').setDisabled(false);
			Ext.getCmp('addSubmit').setDisabled(false);
			return false;
		}
		
		if(is_fields_null == 1)
		{
			Ext.Msg.alert("Alert", "Please Choose values from drop-down for Shift, Job Responsibilities and Support Coverage");
			Ext.getCmp('editSave').setDisabled(false);
			Ext.getCmp('editSubmit').setDisabled(false);
			return false;	
		}
		else
		{	
			if(is_date_validation_exists == 1)
			{
				Ext.Msg.alert("Alert", "Anticipated Start date should within WOR Start Date and WOR End Date");
				Ext.getCmp('editSave').setDisabled(false);
				Ext.getCmp('editSubmit').setDisabled(false);
				return false;				
			}
			else
			{

				/* START code to check work order duplication for client */
				
				// console.log("Form Values "+values['wor_name']);
				is_wor_name_exists = 0;
				Ext.each(myStore.data.items, function(value, i) {      					
					// console.log("DB Values "+value.data.wor_name);

					if (value.data.wor_name == values['wor_name'])
					{
						is_wor_name_exists = 1;
					}
				});
				// return false;
				/*
				if (is_wor_name_exists == 1)
				{
					Ext.Msg.alert("Warning", "Work Order Name Already Exists");
					Ext.getCmp('addSave').setDisabled(false);
					Ext.getCmp('addSubmit').setDisabled(false);
					return false;			
				}		
				END */

				if(wor_id != null && wor_id != "")
				{
					form.url = myStore.getProxy().api.create;
					var titleText = 'Updated';
					myStore.add(values);
				}
				else
				{
					/* START code to check work order duplication for client */
					Ext.each(myStore.data.items, function(value, i) {      					
						if (value.data.client == values['client'])
						{
							// is_client_exists = 1;
						}
					});

					if (is_client_exists == 1)
					{
						Ext.Msg.alert("Warning", "Work Order Already Exists for this Client");
						Ext.getCmp('addSave').setDisabled(false);
						Ext.getCmp('addSubmit').setDisabled(false);
						return false;			
					}		
					/* END */
					
					form.url = myStore.getProxy().api.create;
					var titleText = 'Added';
					myStore.add(values);
				}
				
				myStore.sync({
					success: function (batch, operations) {
						var jsonResp = batch.proxy.getReader().jsonData;
						
						titleText = jsonResp.title;
						
						if(jsonResp.success) 
						{
							Ext.MessageBox.show({
								title: titleText,
								msg: jsonResp.msg,
								buttons: Ext.Msg.OK,
								closable: false,
								fn: function(buttonId) {
									if (buttonId === "ok") 
									{
										Ext.getCmp('hourly_list').store.removeAll();
										Ext.getCmp('fte_list').store.removeAll();
										Ext.getCmp('volume_list').store.removeAll();
										Ext.getCmp('project_list').store.removeAll();                        	
										AM.app.getController('ClientWOR').initWOR();
									}
								}
							});
						}
						else 
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: jsonResp.msg,
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK,
								fn: function(buttonId) {
									if (buttonId === "ok") {
										Ext.getCmp('hourly_list').store.removeAll();
										Ext.getCmp('fte_list').store.removeAll();
										Ext.getCmp('volume_list').store.removeAll();
										Ext.getCmp('project_list').store.removeAll();
										AM.app.getController('ClientWOR').initWOR();
									}
								}
							});
						}
					}, 
					failure: function (batch) {
						myStore.rejectChanges();
					}
				});	
			}
		}
	},
	
	saveStatus: function(form,fte_count)
	{
		var values = form.getValues();		
		if(values['ViewWORDescription']=="")
		{
			Ext.Msg.alert("Alert", "Please add Description");
			return false;
		}
		else
		{
			var warnMsg="";
			if(Ext.getCmp("resourceCount").getValue()>0)
			{
				warnMsg = "There are some resources linked to WOR which will be removed automatically. Still you want to close this WOR?";
			}
			else
			{
				warnMsg = "Are you sure want to close this WOR?";
			}
			Ext.MessageBox.show({
				title: "Remove Resources",
				msg: warnMsg,
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.YESNO,
				closable: false,
				fn: function(buttonId) {
					if (buttonId === "yes") 
					{
						var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
						myMask.show();
						Ext.Ajax.request({
							url: AM.app.globals.appPath+'index.php/Clientwor/saveStatus/',
							method: 'POST',
							params: {'wor_id':values['wor_id'], 'client_id':values['client'], 'status':'Closed', 'description':values['description'], 'fte_count':fte_count},
							success: function(responce, opts){
								var jsonResp = Ext.decode(responce.responseText);
								if(jsonResp.success)
								{ 
									myMask.hide();
									Ext.MessageBox.show({
										title: jsonResp.title,
										msg: jsonResp.msg,
										buttons: Ext.Msg.OK,
										closable: false,
										fn: function(buttonId) {
											if (buttonId === "ok") 
											{
												AM.app.getController('ClientWOR').initWOR();
											}
										}
									});
								}
								else 
								{
									Ext.MessageBox.show({
										title: 'Error',
										msg: jsonResp.msg,
										icon: Ext.MessageBox.ERROR,
										buttons: Ext.Msg.OK,
									});
								}
							}
						});	
					}
					else
					{
						Ext.getCmp('saveButtonID').setDisabled(false);
					}
				}
			});
		}
	},

	onViewPopup : function(work_order_id) 
	{
		var myStore = this.getStore('ClientWOR');
		myStore.getProxy().extraParams = {
			'work_order_id'	: work_order_id,
			'status'	:'All'
		};
		myStore.load();
		
		this.getStore('WorkTypeFTE').load({params: {'wor_id':myStore.data.items[0].data.wor_id}});		
		this.getStore('WorkTypeHourly').load({params: {'wor_id':myStore.data.items[0].data.wor_id}});	
		this.getStore('WorkTypeVolume').load({params: {'wor_id':myStore.data.items[0].data.wor_id}});
		this.getStore('WorkTypeProject').load({params: {'wor_id':myStore.data.items[0].data.wor_id}});

		var view = Ext.widget('worViewPopup');
		view.setTitle('View WOR');
	},
	
	worHistory	: function(status) {
		var component = Ext.getCmp('wor_id');
		var value = component.getValue();
		var orig_history = Ext.getCmp('orig_history');
		var orig_history_val = orig_history.getValue();

		var count_history = Ext.getCmp('count_history');
		var count_val = count_history.getValue();

		if(status == "prev") 
		{ 
			count_val--;
			var name = Ext.getCmp('count_history').setValue(count_val);
			Ext.ComponentQuery.query("#nextWOR")[0].enable();
			if (count_val == 0 )
			{
				Ext.ComponentQuery.query("#prevWOR")[0].disable();
			}
		}

		if(status == "next") 
		{
			count_val++;
			var name = Ext.getCmp('count_history').setValue(count_val);
			Ext.ComponentQuery.query("#prevWOR")[0].enable();
			if(count_val == orig_history_val) 
			{
				Ext.ComponentQuery.query("#nextWOR")[0].disable();
			}

		}
		var store = this.getStore('ClientWOR');
		
		var myPanel = Ext.getCmp('myTabPanel');
		myPanel.update('');
		
		myPanel.update({
			title: 'Summary',
			items : [{								
				xtype:AM.app.getView('wor_cor.SummaryList').create(),
				hidden:false
			}]
		});
		
		myPanel.update({
			title: 'FTE',
			items : [{        
				xtype:AM.app.getView('wor_cor.FTEList').create(),
				hidden:false
			}]
		});
		// Ext.getCmp('AddFTE').setVisible(false);

		myPanel.update({
			title: 'Hourly',
			items : [{        
				xtype:AM.app.getView('wor_cor.HourlyList').create(),
				hidden:false
			}]
		});

		myPanel.update({
			title: 'Volume Based',
			items : [{        
				xtype:AM.app.getView('wor_cor.VolumeBasedList').create(),
				hidden:false
			}]
		});  

		myPanel.update({
			title: 'Project',
			items : [{        
				xtype:AM.app.getView('wor_cor.ProjectList').create(),
				hidden:false
			}]
		});  

		var getcount = this.getStore('WorkTypeFTE').load({
			params: {'wor_id':value,'level': count_val},
			callback : function(records){
				if(records.length != 0)
				{
					myPanel.items.getAt(1).setDisabled(false);
					myPanel.setActiveTab(1);
				}
				else
				{
					myPanel.items.getAt(1).setDisabled(true);
				}
			}
		});

		this.getStore('WorkTypeHourly').load({params: {'wor_id':value ,'level': count_val }, 
			callback : function(records){
				if(records.length != 0)
				{
					myPanel.items.getAt(2).setDisabled(false);
					myPanel.setActiveTab(2);
				}
				else
				{
					myPanel.items.getAt(2).setDisabled(true);
				}
			}
		});
		
		this.getStore('WorkTypeProject').load({params: {'wor_id':value,'level': count_val},
			callback : function(records){
				if(records.length != 0)
				{
					myPanel.items.getAt(4).setDisabled(false);
					myPanel.setActiveTab(4);
				}
				else
				{
					myPanel.items.getAt(4).setDisabled(true);
				}
			}
		});
		
		this.getStore('WorkTypeVolume').load({params: {'wor_id':value,'level': count_val},
			callback : function(records){
				if(records.length != 0)
				{
					myPanel.items.getAt(3).setDisabled(false);
					myPanel.setActiveTab(3);
				}
				else
				{
					myPanel.items.getAt(3).setDisabled(true);
				}
			}
		});

		store.load();
	},
	
	// Adding COR from WOR LIST
	onAddCORcontroller : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var wor = this.getView('wor_cor.AddCor').create();
		
		
		AM.app.globals.wor_cor_combostatus = Ext.getCmp('WORStatusComboBox').getValue();
		AM.app.globals.redir='worredir';
		
		var rec = grid.getStore().getAt(row);
		
		var combo = Ext.getCmp('previous_work_order_id');
		Ext.getCmp('previous_work_order_id').setReadOnly(true);
		var toselect = rec.get('work_order_id');
		combo.select(toselect);
		var record = combo.getStore().findRecord('name', toselect);
		combo.fireEvent('select', combo,[rec] ); 

		var ftestore = this.getStore('corWorkTypeFTE');
		ftestore.clearFilter();
		ftestore.load();
		
		var hourlystore = this.getStore('corWorkTypeHourly');
		hourlystore.clearFilter();
		hourlystore.load();
		
		var volumestore = this.getStore('corWorkTypeVolume');
		volumestore.clearFilter();
		volumestore.load();
		
		var projectstore = this.getStore('corWorkTypeProject');
		projectstore.clearFilter();
		projectstore.load();

		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();		
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var myStore = this.getStore('ClientWOR'); 

		myStore.filters.clear();
		myStore.filter({
			property: 'wor.work_order_id, wor.wor_name, c.`client_name`, wt.wor_type, cp.first_name, em.first_name, dm.first_name, cp.last_name, em.last_name, dm.last_name',
			value: t.getValue()
		});
		myStore.load();				
	},

	onTriggerClear : function() {
		var myStore = this.getStore('ClientWOR');
		myStore.getProxy().extraParams = {'status': Ext.getCmp('WORStatusComboBox').getValue()};
		myStore.clearFilter();
		myStore.load();
	},


	//Assigning Resources from WOR List
	onAssignResources : function(client, client_id, work_order_id, wor_id,wor_name) {
		var view = Ext.widget('AssignResources');

		delete this.getStore('AllReportees1').getProxy().extraParams;
		var store = this.getStore('AllReportees1').load({params: {'manager' : Ext.util.Cookies.get('employee_id'),'client_id': client_id,'wor_id':Ext.getCmp('wor_id').getValue()}});
		var project_store = this.getStore('ProjectTypes').load({params: {'wor_id':Ext.getCmp('wor_id').getValue()}});
		Ext.getCmp('WORtransGridID').bindStore(store);
		Ext.getCmp('WORtransGridPage').bindStore(store);

		Ext.getCmp('res_client').setValue(client);
		Ext.getCmp('res_work_order_id').setValue(work_order_id);
		Ext.getCmp('res_work_order_name').setValue(wor_name);
		Ext.getCmp('transition_date').setMinValue(Ext.getCmp('view_start_date').getValue());
		Ext.getCmp('transition_date').setMaxValue(Ext.getCmp('view_end_date').getValue());
		Ext.getCmp('job_responsibilities').getValue();
	},
	
	onSaveResources : function(win){
		var myAry =  AM.app.globals.transEmployRes, store = [], empIds = "", Grade ="";
		
		var ClientId 	= Ext.getCmp('client_id').getValue();
		var ProjectID 	= Ext.getCmp('project_type_id').getValue();
		var WorID 		= Ext.getCmp('work_order_id').getValue();
		var RoleTypeID 	= Ext.getCmp('role_type_id').getValue(); 
		var TransitionDate = Ext.Date.format(Ext.getCmp('transition_date').getValue(), 'Y-m-d');
		
		var maxDate =  Ext.getCmp('view_end_date').getValue(); 
		var minDate1 = Ext.Date.add(new Date(), Ext.Date.MONTH, -3);
		var minDate = Ext.getCmp('view_start_date').getValue();
		
		if(ProjectID=="" || ProjectID == null)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"<center>Please Select Project<br><small>A member needs to be assigned to a project</small><center>",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('addSubmit').setDisabled(false);
			return false;
		}
		
		if(TransitionDate=="")
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Assign Date",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('addSubmit').setDisabled(false);
			return false;
		}
		
		if(myAry.length != 0 && !(myAry.length == 1 && myAry[0] == null))
		{
			myAry = myAry.filter(function(e){return e;});
			myAry = Ext.Array.unique(myAry);
			var empcount = [];
			for(i=0; i < myAry.length; i++)
			{
				if(!Ext.Array.contains(empcount, myAry[i].data.employee_id))
				{
					var user = myAry[i];
					empcount[i] = user.data.employee_id;
					empIds += (empIds != '')?';'+user.data.employee_id:user.data.employee_id;
					Grade += (Grade != '')?';'+parseFloat(user.data.grades):parseFloat(user.data.grades);
				}
			}
			
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/transition/empSaveEmpProTransition?v='+AM.app.globals.version,
				method: 'GET',
				params: {
					'EmployID':empIds,
					'Grade':Grade,
					'WorID':Ext.getCmp('wor_id').getValue(),
					'ClientID':ClientId,
					'ProjectID':ProjectID,
					'RoleTypeID':RoleTypeID,
					'TransitionDate':TransitionDate
				},
				scope: this,
				success: function(response) 
				{
					var myObject = Ext.JSON.decode(response.responseText);
					
					Ext.Msg.show({
						title:'Message',
						msg:"Employee has been assigned successfully!!!",
						buttons: Ext.Msg.OK,
						closable : false,
						icon: Ext.MessageBox.INFO,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								AM.app.globals.transEmployRes = [];
								Ext.getStore('summaryStore').load({params: {'wor_id': Ext.getCmp('wor_id').getValue(), 'work_order_id': WorID, 'level':Ext.getCmp('orig_history').getValue()}});
								win.close();
								
								var rescount = Ext.getCmp('resourceCount').getValue();									
								var addcount = empcount.length;
								
								var finalvalue = parseInt(rescount) + parseInt(addcount);
								Ext.getCmp('resourceCount').setValue(finalvalue);
							}
						}
					});					
				}
			});
		}
		
	},

	onSavePeopleBillability : function(win){
		var myAry =  AM.app.globals.transEmployRes, store = [], empIds = "", ProjectId ="", worIds = "", ec_pkIDs="";
		var ClientId 	= Ext.getCmp('client_id').getValue();
		var ProjectID 	= Ext.getCmp('project_type_id').getValue();
		var empBill = Ext.getCmp('radio_name').getValue();
		var BillType = Ext.getCmp('bill_type').getValue();
		var BillDate = Ext.getCmp('billable_date').getValue();

		// console.log(empBill.name);
		var radio_value = empBill.name;

		if(radio_value==undefined)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please select Billable Yes/No",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('save_bill').setDisabled(false);
			return false;
		}

		if(BillDate=="" || BillDate == null )
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Billable Date",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('save_bill').setDisabled(false);
			return false;
		}

		if(BillType=="" || BillType == null )
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Billable Type",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('save_bill').setDisabled(false);
			return false;
		}

		if(myAry.length != 0 && !(myAry.length == 1 && myAry[0] == null))
		{
			myAry = myAry.filter(function(e){return e;});
			store = Ext.Array.unique(myAry);
			var empcount = [];
			for(i=0; i < store.length; i++)
			{
				var user = store[i];
				empcount[i] = user.data.employee_id;
				empIds += (empIds != '')?';'+user.data.employee_id:user.data.employee_id;
				ProjectId += (ProjectId != '')?';'+parseFloat(user.data.project_type_id):parseFloat(user.data.project_type_id);
				worIds += (worIds != '')?';'+parseFloat(user.data.wor_id):parseFloat(user.data.wor_id);
				ec_pkIDs += (ec_pkIDs != '')?';'+parseFloat(user.data.id):parseFloat(user.data.id);
			}
			
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/empcategorize/empCategoreDetails?v='+AM.app.globals.version,
				method: 'GET',
				params: {
					'employee_id':empIds,
					'projectIds':ProjectId,
					'worIds':worIds,
					'tblIds':ec_pkIDs,
					'billable':empBill,
					'billable_type':BillType,
					'billable_date':BillDate,
					'client_id':ClientId,
				},
				scope: this,
				success: function(response) 
				{
					var myObject = Ext.JSON.decode(response.responseText);
					
					Ext.Msg.show({
						title:'Message',
						msg:"Employee Billable data updated successfully!!!",
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						closable : false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								AM.app.globals.transEmployRes = [];
								Ext.getStore('Categorization').load({params: {'project_type_id':Ext.getCmp('project_type_id').getValue(), 'wor_id':Ext.getCmp('wor_id').getValue(), 'client_id':Ext.getCmp('client_id').getValue(),'processID':AM.app.globals.categorPro,'ClientID':Ext.getCmp('client_id').getValue(),'Lead':AM.app.globals.categorLead}});
								Ext.getCmp('bill_type').reset();
								Ext.getCmp('radio_name').reset();
								Ext.getCmp('people_billability_grid').getSelectionModel().deselectAll();
								
								var rescount = Ext.getCmp('resourceCount').getValue();									
								var addcount = empcount.length;
								
								var finalvalue = parseInt(rescount) + parseInt(addcount);
								Ext.getCmp('resourceCount').setValue(finalvalue);
							}
						}
					});
				}
			});
		}

	},

	
	onSaveRemResources : function(win){
		var myAry =  AM.app.globals.transEmployRes, store = [], empIds = "", Grade ="";
		var ClientId = Ext.getCmp('res_client_id').getValue();
		var TransitionDate = Ext.Date.format(Ext.getCmp('transition_date').getValue(), 'Y-m-d');
		var WorID = Ext.getCmp('res_work_order_id').getValue();
		var ProjectID 	= Ext.getCmp('project_type_id').getValue();
		
		if(ProjectID=="" || ProjectID == null)
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Project",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('removeEmploySubmit').setDisabled(false);
			return false;
		}

		if(TransitionDate=="")
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Please Select Remove Date",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			Ext.getCmp('removeEmploySubmit').setDisabled(false);
			return false;
		}
		
		if(myAry.length != 0 && !(myAry.length == 1 && myAry[0] == null))
		{
			myAry = myAry.filter(function(e){return e;});
			store = Ext.Array.unique(myAry);
			
			for(i=0; i < store.length; i++)
			{
				var user = store[i];
				empIds += (empIds != '')?';'+user.data.employee_id:user.data.employee_id;
				Grade += (Grade != '')?';'+parseFloat(user.data.grades):parseFloat(user.data.grades);		
			}
			
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/transition/empRemoveEmpTransition?v='+AM.app.globals.version,
				method: 'GET',
				params: {
					'EmployID':empIds,
					'Grade':Grade,
					'WorID':WorID,
					'ClientID':ClientId,
					'ProjectID':ProjectID,
					'TransitionDate':TransitionDate
				},
				scope: this,
				success: function(response) 
				{
					var myObject = Ext.JSON.decode(response.responseText);
					
					Ext.Msg.show({
						title:'Transition Alert Message',
						msg:"Employee has been removed successfully!!!",
						buttons: Ext.Msg.OK,
						closable : false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{ 
								Ext.getCmp('view_resources_grid').getStore().load();
								AM.app.globals.transEmployRes = [];
								Ext.getStore('summaryStore').load({params: {'wor_id': Ext.getCmp('wor_id').getValue(), 'work_order_id': WorID, 'level':Ext.getCmp('orig_history').getValue()}});
								win.close();
								
								var rescount = Ext.getCmp('resourceCount').getValue();									
								var addcount = empcount.length;
								
								var finalvalue = parseInt(rescount) + parseInt(addcount);
								Ext.getCmp('resourceCount').setValue(finalvalue);
							}
						}
					});					
				}
			});
		}
	},


	// View Allocated resources of work order line item.
	onViewResources : function(type, client, client_id, work_order_id, wor_id,wor_name) {
		var view = Ext.widget('ViewRes');
		
		delete this.getStore('AllocatedRes').getProxy().extraParams;
		var store = this.getStore('AllocatedRes').load({params: {'client_id' : client_id, 'wor_id': wor_id, 'start': 0, 'page': 1 }});
		var project_store = this.getStore('ProjectTypes').load({params: {'wor_id':Ext.getCmp('wor_id').getValue()}});

		Ext.getCmp('view_resources_grid').bindStore(store);
		Ext.getCmp('view_resources_gridPage').bindStore(store);

		Ext.getCmp('res_client').setValue(client);
		Ext.getCmp('res_work_order_id').setValue(work_order_id);
		Ext.getCmp('res_work_order_name').setValue(wor_name);
		// Ext.getCmp('transition_date').setMinValue(Ext.getCmp('view_start_date').getValue());
		// Ext.getCmp('transition_date').setMaxValue(Ext.getCmp('view_end_date').getValue());
	},


	// People Billability of work order line item.
	onPeopleBillability : function(type, client, client_id, work_order_id, wor_id,wor_name) {
		var view = Ext.widget('people_billability');

		delete this.getStore('Categorization').getProxy().extraParams;
		var store = this.getStore('Categorization').load({params: {'client_id' : client_id, 'wor_id': wor_id, 'processID':AM.app.globals.categorPro,'ClientID':Ext.getCmp('client_id').getValue(),'Lead':AM.app.globals.categorLead, 'start': 0, 'page': 1 }});

		Ext.getCmp('people_billability_grid').bindStore(store);
		Ext.getCmp('people_billability_gridPage').bindStore(store);
		
		Ext.getCmp('res_client').setValue(client);
		Ext.getCmp('res_work_order_id').setValue(work_order_id);
		Ext.getCmp('res_work_order_name').setValue(wor_name);
	},


	//View configure attributes
	onConfigureAttributes : function(client, client_id, work_order_id, wor_id,wor_name) {
		this.getStore('Attributes').load({params: {'hasNoLimit':'1'}})
		var view = Ext.widget('ConfigureAttributes');

		var project_store = this.getStore('ProjectTypes').load({params: {'wor_id':Ext.getCmp('wor_id').getValue()}});

		Ext.getCmp('res_client').setValue(client);
		Ext.getCmp('res_client_id').setValue(client_id);
		Ext.getCmp('res_work_order_id').setValue(work_order_id);
		Ext.getCmp('res_work_order_name').setValue(wor_name);

		
		var myGrid = Ext.getCmp('attributesGridID');
		myGrid.setDisabled(true);
	},

	onSaveConfigAttributes : function(recordItems, win){
		var myAry =  recordItems, mandatory = "", visible = "";
		var ClientId 	= Ext.getCmp('client_id').getValue();
		var WorID 		= Ext.getCmp('wor_id').getValue();
		var project_type_id = Ext.getCmp('project_type_id').getValue();
		var role_type_id = Ext.getCmp('role_type_id').getValue();
		
		var RowData = [];
		for(i=0; i < myAry.length; i++)
		{
			var user = myAry[i];
			if(user.data.selected==true)
			{
				RowData.push({'attribute_id':user.data.attributes_id, 'mandatory':user.data.mandatory, 'visible':user.data.selected});
			}
		}

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/attributes/SaveClientAttributes?v='+AM.app.globals.version,
			method: 'GET',
			params: {
				'RowData' : JSON.stringify(RowData),
				'project_type_id': project_type_id,
				'role_type_id': role_type_id,
				'WorID':WorID,
				'ClientID':ClientId,
			},
			scope: this,
			success: function(response) 
			{
				var myObject = Ext.JSON.decode(response.responseText);
				
				Ext.Msg.show({
					title:'Attributes Alert Message',
					msg:"Attributes has been assigned successfully!!!",
					buttons: Ext.Msg.OK,
					closable : false,
					fn: function(buttonId) {
						if (buttonId === "ok") 
						{
							AM.app.globals.transEmployRes = [];
							var myGrid = Ext.getCmp('attributesGridID');
							myGrid.store.reload();
							win.close();
						}
					}
				});
			}
		});
	},

	//view assign holidays
	onAddClientHoliday : function(grid, row, col) {
		this.getStore('Holidays').load({params: {'fromWOR':'1','hasNoLimit':1}});
		this.getStore('roleCode').load({params: {'wor_id':Ext.getCmp('wor_id').getValue(),'clientId' : Ext.getCmp('client_id').getValue(),}});
		this.getStore('clientHolidayDetails').load({params: {'client_id' : Ext.getCmp('client_id').getValue(),'wor_id':Ext.getCmp('wor_id').getValue()}});
		
		var view = Ext.widget('AssignClientHolidays');
		view.setTitle("Assign Client Holidays");
		
		Ext.getCmp('holiday_wor_id').setValue(Ext.getCmp('wor_id').getValue());
		Ext.getCmp('holiday_client_id').setValue(Ext.getCmp('client_id').getValue());
	},
	
	onSubmit :  function(grid, client_id, wor_id, project_type_id) {										        					
		var store = [], selectArrayIds = [];
		var TransitionDate = Ext.Date.format(Ext.getCmp('transition_date').getValue(), 'Y-m-d'); 
		var maxDate =  Ext.Date.format(new Date(), 'Y-m-d'); 
		var minDate1 = Ext.Date.add(new Date(), Ext.Date.MONTH, -3);
		var minDate = Ext.Date.format(minDate1, 'Y-m-d');
		var myAry = new Array();
		for (var i = 0; i < AM.app.globals.transEmployRes.length; i++) {
			if (AM.app.globals.transEmployRes[i]) {
				myAry.push(AM.app.globals.transEmployRes[i]);
			}
		}
		if (TransitionDate >= minDate && TransitionDate <= maxDate )
		{	
			if(myAry.length > 0)
			{					
				Ext.Msg.confirm('Delete Resources', 'Are you sure you want to remove resources ', function (button) {
					if (button == 'yes') 
					{   											
						Ext.getCmp('removeEmploySubmit').setDisabled(true);

						myAry = myAry.filter(function(e){return e;});
						store = Ext.Array.unique(myAry);
						
						for(i=0; i < store.length; i++)
						{
							var user = store[i];
							selectArrayIds[i] = user.data.employee_id;
						}
						
						Ext.Ajax.request({
							url : AM.app.globals.appPath+"index.php/Clientwor/remove_resources/?v="+AM.app.globals.version,
							method: 'POST',
							params: {
								resource_ids : JSON.stringify(selectArrayIds),
								client_id  : client_id,
								process_id : project_type_id,
								wor_id : wor_id,
								TransitionDate : TransitionDate,
							},
							success: function (batch, operations) {
								var jsonResp = Ext.decode(batch.responseText); 
								if(jsonResp.success) 
								{
									Ext.MessageBox.show({
										title: jsonResp.title,
										msg: jsonResp.msg,
										buttons: Ext.Msg.OK,
										closable : false,
										fn: function(buttonId) {
											if (buttonId === "ok") 
											{ 																				
												Ext.getCmp('removeEmploySubmit').setDisabled(false);
												/* after removing resources reload store and come to first page */
												Ext.getCmp('view_resources_grid').getStore().removeAll();
												Ext.getCmp('view_resources_gridPage').moveFirst();
												
												Ext.getCmp('view_resources_grid').getStore().load({params: {'wor_id':Ext.getCmp('wor_id').getValue(),'client_id' : client_id, 'project_id': project_type_id }});
												Ext.getCmp('view_resources_gridPage').getStore().load({params: {'wor_id':Ext.getCmp('wor_id').getValue(),'client_id' : client_id, 'project_id': project_type_id }});	
												Ext.getCmp('summary_list').getStore().load({params: {'wor_id':Ext.getCmp('wor_id').getValue(),'level': Ext.getCmp('orig_history').getValue(),'work_order_id': Ext.getCmp('work_order_id').getValue()},});

												var rescount = Ext.getCmp('resourceCount').getValue();									
												var remcount = selectArrayIds.length;
												
												AM.app.globals.transEmployRes = [];

												Ext.getCmp('removeEmploySubmit').setVisible(false);
												Ext.getCmp('transition_date').setVisible(false);
												
												var newStore = Ext.getStore('ClientWOR').reload({
													callback: function(record, operation, success) {
														Ext.each(record, function(item){
															if(item.data.wor_id == Ext.getCmp('wor_id').getValue())
															{
																AM.app.getController('ClientWOR').onReplaceExist('taLayout', item.data);
																if(Ext.util.Cookies.get('grade') >= 3 && Ext.util.Cookies.get('grade') < 4) {
																	Ext.getCmp('ta_edit').setVisible(false);
																}
															}
														});
													}
												});
											}
										}
									});
								}
								else 
								{
									Ext.MessageBox.show({
										title: 'Error',
										msg: jsonResp.msg,
										icon: Ext.MessageBox.ERROR,
										buttons: Ext.Msg.OK
									});
								}			    	
							}, 
							failure: function (batch) {
								console.log("Error occured while saving. Please try again.");
								myStore.rejectChanges();
							}
						});
					}
				}, this);
			}
			else
			{
				Ext.Msg.show({
					title:'Alert',
					msg:"Please Select Employees to Remove",
					icon: Ext.MessageBox.WARNING,
					buttons: Ext.Msg.OK
				});
				return false;
			}

		}
		else
		{
			
			Ext.Msg.show({
				title:'Alert',
				msg:"Please select Remove Date",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			
			Ext.getCmp('removeEmploySubmit').setDisabled(false);
			return false;
		}
	},
	
	onCancel : function() {
		this.up('window').close();
	},
	
	onAddHoliday : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		
		Ext.getStore('roleCode').load({params: {'clientId':rec.data.client}});
		Ext.getStore('Holidays').load({params: {'fromWOR':'1','hasNoLimit':1}});
		Ext.getStore('clientHolidayDetails').load({params: {'client_id' : rec.data.client}});
		
		var view = Ext.widget('AssignClientHolidays');
		view.setTitle("Assign Client Holidays");
		
		Ext.getCmp('holiday_client_id').setValue(rec.data.client);
		Ext.getCmp('holiday_wor_id').setValue(rec.data.wor_id);
	},
	
	onDeleteHoliday : function(grid, row, clientId) 
	{
		var rec = grid.getStore().getAt(row);
		
		var holName = rec.get('holiday');
		Ext.Msg.confirm('Delete', "Are you sure want to delete '"+holName+"' ?", function (button) 
		{
			if (button == 'yes') 
			{
				Ext.Ajax.request({
					url : AM.app.globals.appPath+"index.php/Clientwor/remove_client_holidays/?v="+AM.app.globals.version,
					method: 'POST',
					params: {
						"id" : rec.get('id'),
					},
					success: function (batch, operations) {
						var jsonResp = Ext.decode(batch.responseText); 
						if(jsonResp.success) 
						{
							Ext.MessageBox.show({
								title: jsonResp.title,
								msg: jsonResp.msg,
								buttons: Ext.Msg.OK,
							});

							Ext.getCmp('holidays_list').getStore().load({params: {'client_id' : clientId, 'wor_id':Ext.getCmp('holiday_wor_id').getValue()}});
						}
						else 
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: jsonResp.msg,
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK
							});
						}			    	
					}, 
					failure: function (batch) {
						console.log("Error occurred while saving. Please try again.");
						myStore.rejectChanges();
					}
				});
			}
		}, this);
	},
	
	
	onExtendWor : function() {
		var view = Ext.widget('ExtendWor');
		view.setTitle('Extend Work Order Details');
	},
	
	
	onSaveExtendWor: function(form, win) {
		var values = form.getValues();
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Clientwor/saveExtension/',
			method: 'POST',
			params: {
				'wor_id':Ext.getCmp('wor_id').getValue(), 
				'log_date':values['log_date'], 
				'start_date':values['new_start_date'], 
				'end_date':values['new_end_date'], 
				'extenDescription':values['extenDescription'], 
			},
			success: function(response, opts){
				var jsonResp = Ext.decode(response.responseText);
				if(jsonResp.success)
				{ 
					myMask.hide();
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						closable: false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								Ext.getCmp('view_end_date').setValue(values['new_end_date']);
								win.close();
							}
						}
					});
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK,
					});
				}
			}
		});
	},
	
	
	onReplaceNew : function(fieldname) {
		var panel = Ext.getCmp(fieldname);
		panel.remove(panel.items.items[0],true);
		panel.remove(panel.items.items[0],true);
		var records = Ext.getStore('ClientWOR').findRecord('wor_id', Ext.getCmp('wor_id').getValue());
		var saveValue = rawValue = "";
		
		if(fieldname=="cpLayout")
		{
			panel.add({
				xtype:'boxselect',
				multiSelect : false,
				allowBlank : false,
				name: 'cp_name',
				fieldLabel : 'Client Partner <span style="color:red">*</span>',
				store: Ext.getStore('Clientpartner'),
				displayField: 'full_name',
				valueField	: 'employee_id',
				emptyText: 'Select Client Partner',
				width: 500,
				value: records.data.client_partner,
			});
		}
		else if(fieldname=="emLayout")
		{
			panel.add({
				xtype:'boxselect',
				multiSelect : false,
				allowBlank : false,
				name: 'em_name',
				fieldLabel : 'Engagement Manager <span style="color:red">*</span>',
				store: Ext.getStore('Engagementmanagers'),
				displayField: 'full_name',
				valueField	: 'employee_id',
				emptyText: 'Select Engagement Manager',
				width: 500,
				value: records.data.engagement_manager,
			});
		}
		else if(fieldname=="dmLayout")
		{
			panel.add({
				xtype:'boxselect',
				multiSelect : false,
				allowBlank : false,
				name: 'dm_name',
				fieldLabel : 'Delivery Manager <span style="color:red">*</span>',
				store: Ext.getStore('Deliverymanagers'),
				displayField: 'full_name',
				valueField	: 'employee_id',
				emptyText: 'Select Delivery Manager',
				width: 500,
				value: records.data.delivery_manager,
			});
		}
		else if(fieldname=="amLayout")
		{
			var amArr = records.data.associate_manager;
			if(amArr!="" && amArr!=null)
			{
				amArr = amArr.split(',');
			}
			
			panel.add({
				xtype:'boxselect',
				multiSelect : true,
				name: 'am_name',
				fieldLabel : 'Associate Manager',
				store: Ext.getStore('AssociateManager'),
				displayField: 'full_name',
				valueField	: 'employee_id',
				emptyText: 'Select Associate Manager',
				width: 500,
				value: amArr,
				listeners:{
					beforequery: function(queryEvent){
						Ext.Ajax.abortAll();
						queryEvent.combo.getStore().getProxy().extraParams = {'employee_type':'associate_manager'};
					}
				}
			});
		}
		else if(fieldname=="shLayout")
		{
			var shArr = records.data.stake_holder;
			if(shArr!="" && shArr!=null)
			{
				shArr = shArr.split(',');
			}
			
			panel.add({
				xtype:'boxselect',
				multiSelect : true,
				name: 'sh_name',
				fieldLabel : 'Stakeholder',
				store: Ext.getStore('Stakeholders'),
				displayField: 'full_name',
				valueField	: 'employee_id',
				emptyText: 'Select Stake Holder',
				width: 500,
				value: shArr,
			});
		}
		else if(fieldname=="tlLayout")
		{
			var tlStore = Ext.getStore('TeamLeader').load({params:{'employee_type':'team_leader'}})
			var tlArr = records.data.team_lead;
			if(tlArr!="" && tlArr!=null)
			{
				tlArr = tlArr.split(',');
			}
			
			panel.add({
				xtype:'boxselect',
				multiSelect : true,
				name: 'tl_name',
				fieldLabel : 'Team Lead',
				store: tlStore,
				displayField: 'full_name',
				valueField	: 'employee_id',
				emptyText: 'Select Team Lead',
				width: 500,
				value: tlArr,
				listeners:{
					beforequery: function(queryEvent){
						Ext.Ajax.abortAll();
						queryEvent.combo.getStore().getProxy().extraParams = {'employee_type':'team_leader'};
					}
				}
			});
		}
		else if(fieldname=="taLayout")
		{

			var taArr = records.data.time_entry_approver;
			if(taArr!="" && taArr!=null)
			{
				taArr = taArr.split(',');
			}
			
			panel.add({
				xtype:'boxselect',
				multiSelect : true,
				name: 'ta_name',
				fieldLabel : 'Time Entry Approver',
				store: Ext.getStore('AllocatedRes'),
				displayField: 'FullName',
				valueField	: 'employee_id',
				emptyText: 'Select Time Entry Approver',
				width: 500,
				value: taArr,
				listeners:{
					beforequery: function(queryEvent){
						Ext.Ajax.abortAll();
						queryEvent.combo.getStore().getProxy().extraParams = {'client_id':Ext.getCmp('client_id').getValue(), 'wor_id':Ext.getCmp('wor_id').getValue(),'hasNoLimit':1};
					}
				}
			});
		}
		
		panel.add({
			xtype:'button',
			text: 'Save',
			style   : { 
				"margin-left"  : "15px" 
			},
			handler: function(obj) {
				if(fieldname=="cpLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=cp_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=cp_name]')[0].value;
				}
				else if(fieldname=="emLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=em_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=em_name]')[0].value;
				}
				else if(fieldname=="dmLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=dm_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=dm_name]')[0].value;
				}
				else if(fieldname=="amLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=am_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=am_name]')[0].value;
				}
				else if(fieldname=="tlLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=tl_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=tl_name]')[0].value;
				}
				else if(fieldname=="shLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=sh_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=sh_name]')[0].value;
				}
				else if(fieldname=="taLayout")
				{
					rawValue = Ext.ComponentQuery.query('[name=ta_name]')[0].rawValue;
					saveValue = Ext.ComponentQuery.query('[name=ta_name]')[0].value;
				}
				
				/*var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Updating details....'});
				myMask.show();*/
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/Clientwor/worEmpsHistory/',
					method: 'POST',
					params: {
						'wor_id':Ext.getCmp('wor_id').getValue(), 
						'value':saveValue.toString(),
						'fieldname':fieldname
					},
					success: function(response, opts){
						var jsonResp = Ext.decode(response.responseText);
						if(jsonResp.success)
						{ 
							//myMask.hide();
							Ext.MessageBox.show({
								title: jsonResp.title,
								msg: jsonResp.msg,
								buttons: Ext.Msg.OK,
								closable: false,
								fn: function(buttonId) {
									if (buttonId === "ok") 
									{
										var records = Ext.getStore('ClientWOR').findRecord('wor_id', Ext.getCmp('wor_id').getValue());
										if(fieldname=="cpLayout")
										{
											records.data.client_partner = saveValue;
											records.data.cp_name = rawValue;
										}
										else if(fieldname=="emLayout")
										{
											records.data.engagement_manager = saveValue;
											records.data.em_name = rawValue;
										}
										else if(fieldname=="dmLayout")
										{
											records.data.delivery_manager = saveValue;
											records.data.dm_name = rawValue;
										}
										else if(fieldname=="amLayout")
										{
											records.data.associate_manager = saveValue.toString();
											records.data.am_name = rawValue;
										}
										else if(fieldname=="tlLayout")
										{
											records.data.team_lead = saveValue.toString();
											records.data.tl_name = rawValue;
										}
										else if(fieldname=="shLayout")
										{
											records.data.stake_holder = saveValue.toString();
											records.data.sh_name = rawValue;
										}
										else if(fieldname=="taLayout")
										{
											records.data.stake_holder = saveValue.toString();
											records.data.ta_name = rawValue;
										}
										AM.app.getController('ClientWOR').onReplaceExist(fieldname, records.data);
									}
								}
							});
						}
						else 
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: jsonResp.msg,
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK,
							});
						}
					}
				});
			}
		},{
			xtype:'button',
			text: 'Cancel',
			style   : { 
				"margin-left"  : "10px" 
			},
			handler : function() {
				var records = Ext.getStore('ClientWOR').findRecord('wor_id', Ext.getCmp('wor_id').getValue());
				AM.app.getController('ClientWOR').onReplaceExist(fieldname, records.data);
			},
		});
},

onReplaceExist : function(fieldname, data) {
	var panel = Ext.getCmp(fieldname);
	var fieldId = "";
	panel.remove(panel.items.items[0],true);
	panel.remove(panel.items.items[0],true);
	panel.remove(panel.items.items[0],true);

	if(fieldname=="cpLayout")
	{
		fieldId = "cp_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Client Partner',
			value: data.cp_name
		});
	}
	else if(fieldname=="emLayout")
	{
		fieldId = "em_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Engagement Manager',
			value: data.em_name
		});
	}
	else if(fieldname=="dmLayout")
	{
		fieldId = "dm_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Delivery Manager',
			value: data.dm_name
		});
	}
	else if(fieldname=="amLayout")
	{
		fieldId = "am_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Associate Manager',
			value: data.am_name,
			renderer: function(value, field) {
				if(value.length>40)
				{
					return value.substring(0,60)+'...';
				}
				else
				{
					return value;
				}
			}
		});
	}
	else if(fieldname=="tlLayout")
	{
		fieldId = "tl_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Team Lead',
			value: data.tl_name,
			renderer: function(value, field) {
				if(value.length>40)
				{
					return value.substring(0,60)+'...';
				}
				else
				{
					return value;
				}
			}
		});
	}
	else if(fieldname=="shLayout")
	{
		fieldId = "sh_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Stakeholder',
			value: data.sh_name,
			renderer: function(value, field) {
				if(value.length>40)
				{
					return value.substring(0,60)+'...';
				}
				else
				{
					return value;
				}
			}
		});
	}
	else if(fieldname=="taLayout")
	{
		fieldId = "ta_edit";
		panel.add({
			xtype:'displayfield',
			fieldLabel : 'Time Entry Approver',
			value: data.ta_name,
			renderer: function(value, field) {
				if(value.length>40)
				{
					return value.substring(0,60)+'...';
				}
				else
				{
					return value;
				}
			}
		});
	}

	panel.add({
		xtype:'button',
		icon : AM.app.globals.uiPath+'/resources/images/edit.png',
		value : 'Change',
		id : fieldId,
		style: {
			'margin-left':'10px',
		},
		handler: function() {
			AM.app.getController('ClientWOR').onReplaceNew(fieldname);
		}
	});
},
});
