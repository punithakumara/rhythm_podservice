Ext.define('AM.controller.Process', {
    extend: 'Ext.app.Controller',

    stores : ['Process', 'Clients', 'Vertical','Associate_manager', 'Leads', 'Approver', 'Tech', 'Attributes','ProcessFromNRR'],
    models : ['Process'],
    views: ['process.List', 'process.Add_Edit','process.Add_Nrr_Process'],
	gridProcessID: [],
	editRecord: [],
	techRecs: [],
    init: function() {
        this.control({
           
            'processEdit button[action=save]' : {
            	click : this.updateProcess
            },
            'processAddEdit' : {
            	'addProcess' : this.onSaveClick,
            	'selectVerticalCombo' : this.filterComboBoxes,
            },
			'processList #gridTrigger' : {
                keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
			'processList #processStatusCombo' : {
                changeStatusCombo : this.changeProcessStatus
            },
            '#ClientNameCombo' : {
            	beforequery : function(queryEvent) {
            		Ext.Ajax.abortAll();
            		queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','comboClientPro':'1','filterName' : queryEvent.combo.displayField};
            	}
            },
            '#VerticalNameCombo' : {
            	beforequery : function(queryEvent) {
            		Ext.Ajax.abortAll();
            		queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','comboVertical':'1','filterName' : queryEvent.combo.displayField};
            	}
            },
            '#Process_attributes' : {
            	beforequery : function(queryEvent) {
            		Ext.Ajax.abortAll();
            		queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','filterName' : queryEvent.combo.displayField};
            	}
            }
        });
    },
    
    viewContent : function(){
    	delete this.getStore('Process').getProxy().extraParams;
    	var store = this.getStore('Process');
    	
    	var gridProcess = 1;
		if(Ext.util.Cookies.get('Vert') == Ext.util.Cookies.get('HRVer'))
			gridProcess = 0;
		if(Ext.util.Cookies.get('Vert')=="")
			gridProcess = 0;
		if(Ext.util.Cookies.get('grade') > 5)
			gridProcess = 0;
		this.gridProcessID[0] = gridProcess
		store.proxy.extraParams = {'gridProcess': gridProcess, 'processStatusCombo': '0'};
        store.clearFilter();
        
        var techStore = this.getStore('Tech');
        techStore.filters.clear();
        
        var attrStore = this.getStore('Attributes');
        attrStore.filters.clear();
        
        var attrStore = this.getStore('Clients');
        attrStore.filters.clear();
        
        var attrStore = this.getStore('Vertical');
        attrStore.filters.clear();
        
        var attrStore = this.getStore('Leads');
        attrStore.filters.clear();
		
		var attrStore = this.getStore('Approver');
        attrStore.filters.clear();
        
        var attrStore = this.getStore('Associate_manager');
        attrStore.filters.clear();
		this.getStore('Attributes').load({
		params:{'hasNoLimit':'1'}
		});
    	var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
        var home = this.getView('process.List').create();
        mainContent.removeAll();
        mainContent.add( home );
        mainContent.doLayout();
    },
    
    onTriggerKeyUp : function(t) 
	{
    	Ext.Ajax.abortAll();
		var store = this.getStore('Process');
		store.filters.clear();
        store.filter({
            property: 'process.Name, client.Client_Name, vertical.Name, process.OnBoardingDate, process.ProductionStartDate',
            value: t.getValue()
        });
    },    
    onTriggerClear : function() 
	{
        var store = this.getStore('Process');
        store.clearFilter();
    },
    dispNewTech : function(form) 
	{
    	var values = form.getValues();
    	var verticalId = values.VerticalID;
    	this.getController('AM.controller.Tech').newTechnology(verticalId);
    },
    dispNewProcessAttrs : function(form) 
	{
    	this.getController('AM.controller.Attributes').onCreate();
		Ext.getCmp('ClientNameCombo').setReadOnly(false);
		Ext.getCmp('VerticalNameCombo').setReadOnly(false);
		Ext.getCmp('ProcessType').prev().setReadOnly(false);
    },
    filterComboBoxes : function(form, obj, win)
	{
    	var values = form.getValues();
    	var verticalId = values.VerticalID;
    	Ext.getCmp('verticalmanager').clearValue();
		Ext.getCmp('Process_leads').clearValue();
		this.specificFilteredCombo(verticalId);
    },
	specificFilteredCombo : function(verticalId){
		Ext.getStore('Associate_manager').removeAll();
		Ext.getStore('Associate_manager').load({params:{ VerticalID: verticalId}});
		Ext.getCmp('verticalmanager').bindStore('Associate_manager');
	},
	editProcess: function(grid, row, col) 
	{
		var rec = grid.getStore().getAt(row);
		this.editRecord[0] = rec;
		var techRecs = this.techRecs;
		var Attributes = this.getStore('Attributes');
		AM.app.globals.processID = rec.get('processID');
		
		if(rec.get('ProcessType') != 'Hourly') 
			rec.set('minHours', '');
		
		rec.set('ProcessStatus', rec.get('ProcessStatus') );
		this.getStore('Clients').getProxy().extraParams = {'hasNoLimit':'1','comboClientPro':'1'};
		this.getStore('Vertical').getProxy().extraParams = {'hasNoLimit':'1','comboVertical':'1'};
		Attributes.getProxy().extraParams = {'hasNoLimit':'1'};
		var view = Ext.widget('processAddEdit', {'action': 'edit', 'record': this.editRecord[0]});
	
		view.setTitle('Edit Process Details');
		var verticalId = rec.get('VerticalID');
		
		Ext.getCmp('Approver').show();
		
		Ext.getStore('Approver').load({

			params:{ 'ProcessID':rec.get('processID'),hasNoLimit:1},
		
		});
		Ext.getCmp('Approver').bindStore('Approver');
		
		this.specificFilteredCombo(verticalId);
		Ext.getStore('Leads').load({ params:{ EmplyID: rec.get('AM')[0] }});
		Ext.getCmp('Process_leads').bindStore('Leads');
		
		var mydata = [];
		var tabpanel = view.query('tabpanel')[0];
		var TechnologyID = rec.get('TechnologyID');
		Ext.getStore('Tech').load({
			params:{ VerticalID: verticalId,hasNoLimit:1},
			callback: function(r,options,success) {
				Ext.each(TechnologyID, function(value, i) {
					Ext.each(r, function(value1, ii) {
						if(value1.data.TechnologyID == value){
							mydata.push(value1.data);
							techRecs.push(value1.data);
						}
					});
				});
				tabpanel.items.items[0].store.removeAll();
				tabpanel.items.items[0].store.loadData(mydata, false);
			}
		});
		
		var attrData = [];
		var Attribute_Names = rec.get('Attribute_Names');
		//var OrderofAttr =  rec.get('OrderofAttributes');
		Ext.getStore('Attributes').load({
			params:{ hasNoLimit:1},
			callback: function(r,options,success) {
				Ext.each(Attribute_Names, function(value, i) {
					Ext.each(r, function(value1, ii) {
						console.log(Attribute_Names.length)
						if(value1.data.AttributesID == value){
							value1.data.OrderofAttributes = (i + 1);
							attrData.push(value1.data);
						}						
					});
				});
				
				tabpanel.items.items[1].store.removeAll();
				tabpanel.items.items[1].store.loadData(attrData, false);
			}
		});
		view.down('form').loadRecord(rec);
		var form = view.down('form').getForm();
		form.findField('ClientNameCombo').setReadOnly(true);
		form.findField('VerticalNameCombo').setReadOnly(true);
		form.findField('ProcessType').prev().setReadOnly(true);
		if(rec.get('ProcessType') == 'Hourly')
			form.findField('ClientNameCombo').next().enable();
		else
			form.findField('ClientNameCombo').next().disable();
	},
	ProcessViewForm: function()
	{
	   var view = Ext.widget('processAddEdit');
	   view.setTitle('Add Process Details');
	},
	onCreateProcessFromNrr : function(ClientID,VerticalID) 
	{
		var view = Ext.widget('nrrProcessAddEdit');
		view.setTitle('Add Process');
		Ext.getCmp('hiddenClientID').setValue(ClientID);
		Ext.getCmp('hiddenVerticalID').setValue(VerticalID);
	},
	onSaveNrrProcess : function(form, win){
		var myStore = this.getStore('ProcessFromNRR');
		var record = form.getRecord(), values = form.getValues();
		
		if(!record) {
			myStore.add(values);
			var titleText = 'Added';
		}
		else {
			record.set(values);
			var titleText = 'Updated';
		}	
		
		myStore.sync({
			success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;
		    	
		    	if(jsonResp.success) {
		    		Ext.MessageBox.show({
                        title: titleText,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK,
                    });
					Ext.getStore('Process').load();
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    	}
		    	win.close();
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving process. Please try again.");
		    	myStore.rejectChanges();
		    }
		});
	},
   
	onSaveClick: function(form)
	{
		var myStore = this.getStore('Process');
		var record  = form.getRecord(), values = form.getValues();
		if(!record) {
			myStore.add(values);
			var titleText = 'Added';
		}
		else {
			record.set(values);
			var titleText = 'Updated';
		}	
		
		myStore.sync({
			success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;
		    	
		    	if(jsonResp.success == 'added') {
		    		Ext.MessageBox.show({
                        title: titleText,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    	}else if(jsonResp.success == 'updated') {
		    		Ext.MessageBox.show({
                        title: titleText,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    	}else if(jsonResp.success == 'existed'){
					Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
				}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    	}
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving process. Please try again.");
		    	myStore.rejectChanges();
		    }
		});
   },
   changeProcessStatus: function(combo){
		combo.next().next().reset();
		var store = this.getStore('Process');
		store.getProxy().extraParams = {'processStatusCombo': combo.getValue(), 'gridProcess': this.gridProcessID[0]};
		if(combo.getValue())
			store.clearFilter();
   },
});