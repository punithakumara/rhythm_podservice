Ext.define('AM.controller.ResourceMapping',{
	extend : 'Ext.app.Controller',	
	stores : ['ResourceMapping'],
	views: ['ResourceMapping.List', 'ResourceMapping.MonthField'],
	init : function() {
		this.control({
			'ResourceMappinggrid #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear	
			}
		});
	},
	
	viewContent : function() {
		var myStore = this.getStore('ResourceMapping');
		myStore.clearFilter();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var ResourceMapping = this.getView('ResourceMapping.List').create();
		mainContent.removeAll();
		mainContent.add( ResourceMapping );
		mainContent.doLayout();		
	},

	downloadResourceMappingCSV : function() {
		var myStore = this.getStore('ResourceMapping');
		if( myStore.getCount() == 0) 
		{
		  	Ext.Msg.alert('Alert', 'No Data To Export!!!');
		} 
		else 
		{
	   		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
	   		myMask.show();
	   		Ext.Ajax.request( {
	   			url: AM.app.globals.appPath+'index.php/reports/DownloadResourceMappingCsv/',
	   			method: 'GET',
	   			params: {'Report':"reportflag"},
	   			success: function(responce, opts){
	   				var jsonResp = Ext.decode(responce.responseText);
	   				if(jsonResp.success) {	
	   					myMask.hide();
	   		    		window.location = AM.app.globals.appPath+'download/ResourceMapping.csv';
	   		    	}
				}
	   	    });
	   	} 
	},
	
	onTriggerKeyUp : function(t) {		
		Ext.Ajax.abortAll();
		var store = this.getStore('ResourceMapping');
		store.filters.clear();
		store.filter({
			property: 'EmployeeID,EmployeeName,ClientName,Vertical,BillableType,location',
			value: t.getValue()
		});
	},

	onTriggerClear : function() {
		var store = this.getStore('ResourceMapping');
		store.clearFilter();
	},

	rmrMonthFilter : function(me,mbrmonth,enddte){
		Ext.getStore('ResourceMapping').load({params:{'mbrmonth':mbrmonth}});
	}
	
});