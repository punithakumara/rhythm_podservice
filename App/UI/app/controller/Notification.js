Ext.define("AM.controller.Notification", {
    extend: "Ext.app.Controller",
    stores: [ "Notifications"],
    views: [ "Notification.NotificationList", "Notification.NotificationPopUp" ],
    init: function() {
        this.control({})
    },
    refs: [ {
        ref: "Notification.NotificationPopUp"
    } ],
    viewNotifications: function() {
        delete this.getStore('Notifications').getProxy().extraParams;
        var maincontent = Ext.ComponentQuery.query("#mainContent")[0];
        var notficview  = this.getView("Notification.NotificationList").create();
        maincontent.removeAll();
        maincontent.add(notficview);
        maincontent.doLayout();
    },

    viewNotificationType: function(res, nid, type) {
        try 
		{
            var obj = Ext.decode(res);
            if (1 == type) {
                this.destroy();
                AM.app.getController("AM.controller.ClientNrr").onNotificationNrr(obj);
            }
            if (2 == type) {
                this.destroy();
                AM.app.getController("AM.controller.ClientRdr").onNotificationRdr(obj);
            }
            if (6 == type) {
                this.destroy();
                AM.app.getController("AM.controller.newTransition").pushNotfications(obj.EmployId, obj.Grade);
            } 
            if (7 == type) {
                this.destroy();
                AM.app.getController("AM.controller.ClientRdr").onNotificationViewClientRdr(obj);
            }
        } 
		catch (e) 
		{
            if (3 == type) {
                this.destroy();
                AM.app.getController("AM.controller.Meeting_minutes").viewContent();
            }
            if (5 == type) {
                this.destroy();
                AM.app.getController("AM.controller.MonthlyBillabilityReport").viewContent();
            }
            if (6 == type) {
                this.destroy();
                AM.app.getController("AM.controller.newTransition").viewContent();
            } 
			else if (4 == type) 
				window.open("/onboarding/survey/survey.php?NotificationID=" + nid, "_blank");
        }
    },
	
	
    viewPopNotify: function() {
		var mystore = Ext.getStore('Notifications').load({ 
			params: { limitoten: "limitoten" },
			callback: function(records, options, success) {
				var view = Ext.widget("notificationpopupx");
			}
		});
    },

	
    updateNotification: function() {
		Ext.getCmp("notifypopupcc").destroy();
        Ext.Ajax.request({
            url: AM.app.globals.appPath+ "/index.php/Employees/upDateNotification/?v="+AM.app.globals.version,
            method: "POST",
            params: {
                empid: Ext.util.Cookies.get("employee_id"),
            }
        });
		
		var notiObj 	= Ext.getCmp("logInCont").getEl();
		notiObj.down('.notiCnt').update('0');
		Ext.util.Cookies.clear('Notifications');
		Ext.util.Cookies.set('Notifications', '0');
		
		var mystore = Ext.getStore('Notifications').load({ 
			params: { limitoten: "limitoten" },
			callback: function(records, options, success) {
				var view = Ext.widget("notificationpopupx");
			}
		});
    },
	
	
    timeSince: function(date) {
        var today = this.replaceAll("-", "/", date);
        var seconds = Math.floor((new Date() - new Date(today)) / 1e3);
        var interval = Math.floor(seconds / 31536e3);
        if (interval > 1) return interval + " years ago";
            interval = Math.floor(seconds / 2592e3);
        if (interval > 1) return interval + " months ago";
            interval = Math.floor(seconds / 86400);
        if (interval > 1) return interval + " days ago";
            interval = Math.floor(seconds / 3600);
        if (interval > 1) return interval + " hours ago";
            interval = Math.floor(seconds / 60);
        if (interval > 1) return interval + " minutes ago";
            return Math.floor(seconds) + " seconds ago";
    },
	
	
    replaceAll: function(find, replace, str) {
        return str.replace(new RegExp(find, "g"), replace);
    },

});