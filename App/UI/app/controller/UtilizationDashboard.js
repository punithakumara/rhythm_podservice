Ext.define('AM.controller.UtilizationDashboard',{
	extend : 'Ext.app.Controller',	
	stores : ['LogInViewerClients','Employees','UtilizationDashboard'],
	views: ['utilization.UtilizationDashboardGrid','utilization.editUtil','utilization.UtilizationGrid'],
	
	viewContent : function() {

		this.getStore('Utilization').rejectChanges();	
	
		AM.app.globals.editUtilData = {};
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var opsdata = this.getView('utilization.UtilizationGrid').create();
		
		mainContent.removeAll();
		mainContent.add( opsdata );
		mainContent.doLayout();
	},
	
	//Insight
	utilizationDashbordList : function(){
		//delete this.getStore('Process').getProxy().extraParams;
		//this.getStore('Process').sorters.clear();
		this.getStore('UtilizationDashboard').getProxy().extraParams;
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var UtilizationDashboard = this.getView('utilization.UtilizationDashboardGrid').create();
		mainContent.removeAll();
		mainContent.add(UtilizationDashboard);
		mainContent.doLayout();
		Ext.getCmp('id-docs-paging').store.removeAll();
		Ext.getCmp('id-docs-paging').store.currentPage=1;
		Ext.getCmp('id-docs-paging').store.load();
		Ext.getCmp('TotalHours').hide();
		Ext.getCmp('utilization_client').getEl().on('click', function(obj) {
			Ext.getCmp('utilization_client').clearValue();
		});
	},
	
	onUpload : function(clientId){		
		Ext.widget('empimportXls');		
		Ext.getCmp('hiddenClientID').setValue(clientId);
		Ext.getCmp('hiddenDashboard').setValue("1");
	},
	
	onDownload : function(dashboard){
				
		var paramess="insighExport";
		if(dashboard==="Template")
			paramess="Template";
		var empid = Ext.getCmp('IL_EmployID').getValue();
		var StartDate = Ext.getCmp('utilization_start_date').getValue();
		var EndDate = Ext.getCmp('utilization_end_date').getValue();
			StartDate = Ext.Date.format(StartDate, 'Y-m-d');
			EndDate = Ext.Date.format(EndDate, 'Y-m-d');
		var clientComboId = Ext.getCmp('utilization_client').getValue();
		var utilWorId = Ext.getCmp('utilization_wor_id').getValue();
		var utilStatus = Ext.getCmp('utilstatus').getValue();
		var project_code = Ext.getCmp('util_project_code').getValue();
		var worTypeId = Ext.getCmp('wor_type').getValue();
		var projectTypeId = Ext.getCmp('project_type').getValue();
		var supervisor = Ext.getCmp('supervisor').getValue();
		var myStore = this.getStore('UtilizationDashboard');
		
		Ext.Ajax.request({
			url : AM.app.globals.appPath+'index.php/utilization/downloadCsvTemplate/',
			method: 'POST',
			params: {
				StartDate				: StartDate,
				EndDate					: EndDate,
				clientId				: clientComboId,
				wor_id					: utilWorId,
				projectCode				: project_code,
				EmployID				: empid,
				utilstatus				: utilStatus,
				insightType				: dashboard,
				wor_type_id				: worTypeId,
				project_type_id			: projectTypeId,
				supervisor				: supervisor,
				Export					: paramess,
			},
			success: function (batch, operations) {
				var jsonResp = Ext.decode(batch.responseText); 
				if(jsonResp.success){
					
					if(jsonResp.data == 1)
					{
						if(paramess=="Template")
						{
							window.location = AM.app.globals.appPath+'download/UtilizationTemplate.csv';
						}else
						{
							window.location = AM.app.globals.appPath+'download/Utilization_insight.csv';
						}
					}
					else
					{
						Ext.Msg.alert("Alert", "No Data Available to export");					
					}					
				}
			}
		});
	},
	
	onEdit:function(data){
						
		AM.app.globals.editUtilData = data;
		var view1 = Ext.widget('editUtil');
		data.set('task_id', Ext.util.Format.htmlDecode(data.get('task_id')));
		data.set('description', Ext.util.Format.htmlDecode(data.get('description')));
		view1.down('form').loadRecord(data);
	
	},
	
	/*onHistory : function (ProcessID, ProcessName, ClientName, EmployID){
		AM.app.globals.histrydbrdProcess = ProcessID;
		AM.app.globals.histrydbrdEmployID = EmployID;
		
		var view1 = Ext.widget('historyUtilization');
		Ext.getCmp('HistoryProcessID').setValue(ProcessID);
		Ext.getCmp('HistoryProcessName').setValue('<b>'+ProcessName+'</b>');
		Ext.getCmp('HistoryClientName').setValue('<b>'+ClientName+'</b>');
	},*/
	
	onImportxls : function(form, window){
		
		var myStore = this.getStore('UtilizationDashboard');
		var record = form.getRecord(), values = form.getValues();
		var prcessID = Ext.getCmp('utilization_client').getValue();
		var StartDate = Ext.getCmp('utilization_start_date').getValue();
		var EndDate = Ext.getCmp('utilization_end_date').getValue();
		var empid = Ext.util.Cookies.get('empid');
		
		form.submit({
			method: 'POST',
			url: myStore.getProxy().api.url,
			waitMsg : 'Saving your data, please wait...',  
			success: function(form,o) {
                var retrData = JSON.parse(o.response.responseText);
                if(retrData.success) {
		    		Ext.MessageBox.show({
                        title: "Import Utilization",
                        msg: retrData.msg,
                        buttons: Ext.Msg.OK,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                            	window.close();
                            }
                        }
                    });
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        //title: 'Error',
                        msg: retrData.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                            	window.close();
                            }
                        }
                    });
		    	}
            },
            failure: function(form, action)
            {
            	console.log("Error occured while saving Document. Please try again.");
                myStore.rejectChanges();
                window.close();
            }
		});
	},
	
	sendExtParams : function(){		
		Ext.getStore('Utilization').getProxy().extraParams = this.customParamers('', '');
	},
	
	onApprove : function(obj, gridPanel, data){	
		var selection = gridPanel.getSelectionModel().selected;
		if(selection.items.length > 0) {
			var selectArrayIds = [];
			var recordData = selection.items;	
			for(var i = 0;i < recordData.length;i++) {
				selectArrayIds[i] = recordData[i].data.utilization_id;
			}
			Ext.Ajax.request({
				url : AM.app.globals.appPath+'index.php/utilization/utilization_approve/',
				method: 'POST',
				params: {
					UtilizationArray : JSON.stringify(selectArrayIds),
					value	: data
				},
				success: function (batch, operations) {
			    	var jsonResp = Ext.decode(batch.responseText); 
			    	if(jsonResp.success) {
			    		Ext.MessageBox.show({
	                        title: jsonResp.title,
	                        msg: jsonResp.msg,
	                        buttons: Ext.Msg.OK
	                    });
			    	}
			    	else {
			    		Ext.MessageBox.show({
	                        title: 'Error',
	                        msg: jsonResp.msg,
	                        icon: Ext.MessageBox.ERROR,
	                        buttons: Ext.Msg.OK
	                    });
			    	}
			    	gridPanel.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
					
					Ext.getCmp('UtilizationDashboardGridID').getStore().proxy.extraParams = { limit: Ext.getCmp('pagination_records').getValue(),EmployID:obj.query('combobox')[1].getValue(),client_id:obj.query('combobox')[0].getValue(),utilization_start_date:obj.query('datefield')[0].getValue(),utilization_end_date:obj.query('datefield')[1].getValue()};
					Ext.getCmp('UtilizationDashboardGridID').getStore().reload();
			    }, 
			    failure: function (batch) {
			    	console.log("Error occured while saving. Please try again.");
			    	myStore.rejectChanges();
			    }
	   	 	});
		}
		else {
			var textMsg = "Reject";
			if(data==1)
				textMsg = "Approve";
			Ext.MessageBox.show({
                title: 'Error',
                msg: "Select a record before "+textMsg+". !!!",
                icon: Ext.MessageBox.ERROR,
                buttons: Ext.Msg.OK
            });
		}
	},
	
	saveUtilization : function(form,ExtHours,ExtMinutes,UtilizationID,win){
		var myStore = this.getStore('UtilizationDashboard');
				
		startDate 	= Ext.getCmp('utilization_start_date').getValue();
		endDate 	= Ext.getCmp('utilization_end_date').getValue();
		ClientID 	= Ext.getCmp('utilization_client').getValue();
		EmployID 	= Ext.getCmp('IL_EmployID').getValue();
		
		var record = form.getRecord(),values = form.getValues();
		
		myStore.getProxy().extraParams = {
			client_id: ClientID,
			utilization_id: UtilizationID,
			ExtHours:ExtHours,
			ExtMinutes:ExtMinutes,
			dashboard:1
		};
		
		if(ExtHours !="" || ExtMinutes != ""){
			values['Hours'] = ExtHours;
			values['Minutes'] = ExtMinutes;
		}
		
		if(!record) myStore.add(values);
		else record.set(values);
		
		myStore.sync({
			success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;
		    	var title = (jsonResp.msg ==  'Record updated successfully.') ? "Updated" : "Added";
				
				if(jsonResp.msg == 'No Changes made to save.')
				{
					title = 'No Change';
				}
		    	
		    	if(jsonResp.success) {
		    		Ext.MessageBox.show({
                        title: title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    	}
				
				win.close();
				//alert(clientID);
				AM.app.getController('UtilizationDashboard').utilzationDashdoardFun(Ext.getCmp('UtilizationDashboardGridID'), startDate, endDate, EmployID, Ext.getCmp('utilization_client').getValue());
			}, 
		    failure: function (batch) {
		    	console.log("Error occured while saving process. Please try again.");
				myStore.rejectChanges();
				win.close();
		    }
		});
	},

	utilzationDashdoardFun: function(me,StartDate,EndDate,EmployeeID,SaveCall, clientID){		 
		
		Ext.getStore('UtilizationDashboard').removeAll();
		Ext.getStore('UtilizationDashboard').currentPage = 1;
		
		if (Ext.getCmp('pagination_records').getValue() == null)
		{
			Ext.getCmp('pagination_records').setValue(15);
		}
				
		Ext.getStore('UtilizationDashboard').load({
			params:{'client_id': Ext.getCmp('utilization_client').getValue(),'utilization_start_date':StartDate,'utilization_end_date':EndDate,'EmployID':EmployeeID},
			callback:function(records, options, success) {
				if (success) {
					Ext.getCmp('UploadExcel').setDisabled(false);
					Ext.getCmp('UploadExcel').addClass('enabledComboTestCls');
					if(Ext.util.Cookies.get("grade") >= 3){
						Ext.getCmp('DownloadLink').setDisabled(false);
						Ext.getCmp('DownloadLink').addClass('enabledComboTestCls');
					}
					else{
						Ext.getCmp('DownloadLink').setDisabled(true);
					}
					Ext.getCmp('HistoryLink').setDisabled(false);
					Ext.getCmp('HistoryLink').addClass('enabledComboTestCls');
					
					// console.log(options.response.responseText);
					var Obj = eval('(' +options.response.responseText+ ')');
					if(records.length != 0){
						Ext.getCmp('approveButton').show();
						Ext.getCmp('rejectButton').show();
						Ext.getCmp('TotalHours').show();
						Ext.getCmp('TotalHours').setValue(Obj.TotalHours);
					
						Ext.getCmp('approveButton').setDisabled(false);
						Ext.getCmp('approveButton').addClass('enabledComboTestCls');
						Ext.getCmp('rejectButton').setDisabled(false);
						Ext.getCmp('rejectButton').addClass('enabledComboTestCls');
						if(!SaveCall){
							me.storeLoad(me.store,me);								
							me.doLayout(false,true);
						}
					}
					else{
						Ext.getCmp('TotalHours').hide();
						Ext.getCmp('TotalHours').setValue(" ");
						Ext.getCmp('approveButton').setDisabled(true);
						Ext.getCmp('approveButton').addClass('disabledComboTestCls');
						Ext.getCmp('rejectButton').setDisabled(true);
						Ext.getCmp('rejectButton').addClass('disabledComboTestCls');
					}					
				}
				else
				{
					Ext.getCmp('UploadExcel').setDisabled(true);
					Ext.getCmp('UploadExcel').addClass('disabledComboTestCls');
					Ext.getCmp('approveButton').setDisabled(true);
					Ext.getCmp('approveButton').addClass('disabledComboTestCls');
					Ext.getCmp('rejectButton').setDisabled(true);
					Ext.getCmp('rejectButton').addClass('disabledComboTestCls');
					Ext.getCmp('HistoryLink').setDisabled(true);
					Ext.getCmp('HistoryLink').addClass('disabledComboTestCls');
					/*Ext.MessageBox.show({
						title: 'Insight Message',
						msg: 'You are not approver to this process.',
						buttons: Ext.Msg.OK
					}); */
				}
			}
		});
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Utilization');
		store.filters.clear();

		var utilization_date = Ext.getCmp('utilization_date').getValue();
        var utilization_process = Ext.getCmp('utilization_process').getValue();
        
		store.getProxy().extraParams = this.customParamers(utilization_process, utilization_date);
        store.filter({
            property: 'emp.FirstName, temp.Errors',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Opsdata');
        store.clearFilter();
    }
});