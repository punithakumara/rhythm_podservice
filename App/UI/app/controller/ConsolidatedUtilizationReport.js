Ext.define('AM.controller.ConsolidatedUtilizationReport', {
	extend: 'Ext.app.Controller',
	stores: ['Clients', 'Reports', 'Vertical','Employees','Shifts','Managers','ConsolidatedUtilizationReport','Leads','Associate_manager','Process_manager','TlEmployee'],
	views: [ 'ConsolidatedUtilizationReport.WizardForm','ConsolidatedUtilizationReport.TopPanel','ConsolidatedUtilizationReport.GridPanel'],
	init: function () {
		this.control({
			'TopPanel button[name=ShowUtilization]': {
				click: this.showGridPanel
			}
		});
	},
  
	viewContent : function() 
	{
		//To delete the existing Store values in grid 
	    delete this.getStore('ConsolidatedUtilizationReport').getProxy().extraParams;
		this.getStore('ConsolidatedUtilizationReport').getProxy().extraParams = {};
		var store = this.getStore('ConsolidatedUtilizationReport');
		store.clearFilter();  
		
		//End grid cleaner
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var utilReports = this.getView('ConsolidatedUtilizationReport.WizardForm').create();
		mainContent.removeAll();
		mainContent.add(utilReports);
		mainContent.doLayout();
		if(Ext.util.Cookies.get("grade") == 3 ||  Ext.util.Cookies.get("grade")==3.1){
			Ext.getCmp('ConUtilmanagerID').setVisible(false);
			Ext.getCmp('ConUtilAMID').setVisible(false);
			Ext.getCmp('ConUtilTLId').setVisible(false);
			Ext.getCmp('ConUtilVerticalID').setVisible(false);
			Ext.getCmp('Parentvrtclconsutili').setVisible(false);
			Ext.getCmp('ConUtilEmployID').setVisible(true);
		}
		else if(Ext.util.Cookies.get("grade") == 4){
			Ext.getCmp('ConUtilmanagerID').setVisible(false);
			Ext.getCmp('ConUtilAMID').setVisible(false);
			//Ext.getCmp('ConUtilVerticalID').setVisible(false);
			Ext.getCmp('ConUtilEmployID').setVisible(false);
			Ext.getCmp('Parentvrtclconsutili').setVisible(false);
			
		}
		else if(Ext.util.Cookies.get("grade") >=5){
			Ext.getCmp('ConUtilTLId').setVisible(false);
			Ext.getCmp('ConUtilEmployID').setVisible(false);
		}
		else{
			Ext.getCmp('ConUtilVerticalID').setVisible(false);
			Ext.getCmp('ConUtilmanagerID').setVisible(false);
			Ext.getCmp('ConUtilAMID').setVisible(false);
			Ext.getCmp('ConUtilTLId').setVisible(false);
			Ext.getCmp('ConUtilEmployID').setVisible(false);
			Ext.getCmp('Parentvrtclconsutili').setVisible(false);
		}
	},
  
	showGridPanel: function (btn) 
	{
		var pverticalCombo= Ext.getCmp('Parentvrtclconsutili').getValue();	
		
		var ClientID  	= Ext.getCmp('ConUtilClientID').getValue();
		var VerticalID  	= Ext.getCmp('ConUtilVerticalID').getValue();
		var ShiftID 		= Ext.getCmp('ConUtilShiftID').getValue();
		var managerID 	= Ext.getCmp('ConUtilmanagerID').getValue();
		var AMID 			= Ext.getCmp('ConUtilAMID').getValue();
		var TLId 			= Ext.getCmp('ConUtilTLId').getValue();
		var EmployID 		= Ext.getCmp('ConUtilEmployID').getValue();
		var startDate 	= Ext.getCmp('ConUtilreportSstartDate').getValue();
		var endDate 		= Ext.getCmp('ConUtilreportsEndDate').getValue();
		var ConUtilGroupBy 		= Ext.getCmp('ConUtilGroupBy').getValue();
		var WeekendSupport 		= Ext.getCmp('WeekendSupport').getValue();
		var ClientHoliday 		= Ext.getCmp('ClientHoliday').getValue();
		var VerticalID_strings= JSON.stringify(VerticalID);
		
		
		console.log("pverticalCombo");
		console.log(pverticalCombo);
		
		console.log("ClientID");
		console.log(ClientID);
		
		console.log("VerticalID");
		console.log(VerticalID);
		
		console.log("ShiftID");
		console.log(ShiftID);
		
		console.log("managerID");
		console.log(managerID);
		
		console.log("EmployID");
		console.log(EmployID);
		
		console.log("AMID");
		console.log(AMID);
		
		console.log("TLId");
		console.log(TLId);
		
		console.log("startDate");
		console.log(startDate);
		
		console.log("endDate");
		console.log(endDate);
		
		console.log("ConUtilGroupBy");
		console.log(ConUtilGroupBy);
		
		var data = this.getStore('ConsolidatedUtilizationReport').getProxy().extraParams = {pverticalCombo:pverticalCombo,ClientID:ClientID, VerticalID:VerticalID_strings , ConUtilGroupBy:ConUtilGroupBy,ShiftID:ShiftID,managerID:managerID,AMID:AMID,TLId:TLId,EmployID:EmployID,startDate:startDate,endDate:endDate,WeekendSupport:WeekendSupport,ClientHoliday:ClientHoliday};

		this.getStore('ConsolidatedUtilizationReport').load();
	},
 
	onChangeReportType: function(combo){
		if(combo.getValue() == 'Month')
		{
			Ext.getCmp('Reports_Month').show();
			Ext.getCmp('Reports_Week').hide();
		}
		else{
			Ext.getCmp('Reports_Week').show();
			Ext.getCmp('Reports_Month').hide();
		}
		this.getStore('Reports').getProxy().extraParams.reportType = combo.getValue();
		this.getStore('Reports').load();	
	},
  
	isEmpty: function(obj) {
		for(var key in obj) {
			if(obj.hasOwnProperty(key))
				return false;
		}
		return true;
	},
 
  
	onSelectVertical: function(obj){
		obj.next().reset();
		obj.next().store.load({
			params: {'hasNoLimit':'1', 'VerticalID': obj.getValue()}
		});
	},
  
	onSelectManager: function(obj){
		obj.next().reset();
		obj.next().store.load({
			params: {'hasNoLimit':'1', 'VerticalID': obj.getValue()}
		});
	},
  
 	downloadConUtiliaztionCSV : function(){
		var pverticalCombo= Ext.getCmp('Parentvrtclconsutili').getValue();
		// var ProcessID	 	= Ext.getCmp('ConUtilProcessID').getValue();
		var ClientID  	= Ext.getCmp('ConUtilClientID').getValue();
		var VerticalID 	= Ext.getCmp('ConUtilVerticalID').getValue();
		var ShiftID 		= Ext.getCmp('ConUtilShiftID').getValue();
		var managerID 	= Ext.getCmp('ConUtilmanagerID').getValue();
		var AMID 			= Ext.getCmp('ConUtilAMID').getValue();
		var TLId 			= Ext.getCmp('ConUtilTLId').getValue();
		var EmployID 		= Ext.getCmp('ConUtilEmployID').getValue();
		var startDate 	= Ext.getCmp('ConUtilreportSstartDate').getValue();
		var endDate 		= Ext.getCmp('ConUtilreportsEndDate').getValue();
		var ConUtilGroupBy 		= Ext.getCmp('ConUtilGroupBy').getValue();
		var WeekendSupport 		= Ext.getCmp('WeekendSupport').getValue();
		var ClientHoliday 		= Ext.getCmp('ClientHoliday').getValue();

		var myStore = this.getStore('ConsolidatedUtilizationReport');
		if(myStore.getCount()==0) {
			Ext.Msg.alert('Alert', 'No Data To Export!!!');
		}
		else
		{
			var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
			myMask.show();
			Ext.Ajax.request({
				url: AM.app.globals.appPath+'index.php/reports/csvConUtilizationReport/?v='+AM.app.globals.version,
				method: 'GET',
				params: {pverticalCombo:pverticalCombo,ClientID:ClientID, VerticalID: VerticalID, ConUtilGroupBy:ConUtilGroupBy,ShiftID:ShiftID,managerID:managerID,AMID:AMID,TLId:TLId,EmployID:EmployID,startDate:startDate,endDate:endDate,WeekendSupport:WeekendSupport,ClientHoliday:ClientHoliday},
				success: function(responce, opts){
					var jsonResp = Ext.decode(responce.responseText);
					if(jsonResp.success)
					{	
						myMask.hide();
						window.location = AM.app.globals.appPath+'download/ConUtilizationReports.csv?v='+AM.app.globals.version;
					}
				}
			});
		} 	
	} 
});