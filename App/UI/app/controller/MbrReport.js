Ext.define('AM.controller.MbrReport',{
	extend : 'Ext.app.Controller',	
	stores : ['MbrReport'],
	views: ['MbrReport.List'],
	init : function() {
		this.control({
			'MbrReportgrid #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear	
			}
		});
	},
	
	viewContent : function() {
		var myStore = this.getStore('ResourceMapping');
		myStore.clearFilter();
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var ResourceMapping = this.getView('MbrReport.List').create();
		mainContent.removeAll();
		mainContent.add( ResourceMapping );
		mainContent.doLayout();		
	},

	onSaveDocument : function(form, window) 
	{
		var myStore = this.getStore('MbrReport');
		
		var record = form.getRecord(), values = form.getValues();
		
		if(!record) 
		{
			myStore.add(values);
			form.url = myStore.getProxy().api.create;
			var titleText = 'Added';
		}	
		
		

		//console.log('testing...');
		var msg = Ext.MessageBox.wait('Exporting data, Please wait...');
		form.submit({
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
            success: function(form,o) {
                var retrData = JSON.parse(o.response.responseText); 
                //console.log(retrData);
    			if(retrData.success)
					{
						msg.hide();
						window.location = AM.app.globals.appPath+'download/MBRReport.csv';
						var a = document.createElement("a");
			            // safari doesn't support this yet
			            a.href = window.location;
			            document.body.appendChild(a);
			            a.click();
			            AM.app.getController('Dashboard').viewContent();
						//window.destroy();
					}		
				
            },
            failure: function(form, action)
            {
            	console.log("Error occured while saving Document. Please try again.");
                myStore.rejectChanges();
                window.close();
            }
        });
        //window.destroy();
	},

	
});