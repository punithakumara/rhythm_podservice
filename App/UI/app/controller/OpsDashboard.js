Ext.define('AM.controller.OpsDashboard',{
	extend : 'Ext.app.Controller',	
	stores : ['OpsDashboard'],
	views: ['OpsDashboard.List'],
	init : function() {
		this.control({
			'OpsDashboardgrid #gridTrigger' : {
                keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear	
            }
		});
	},
	
	OpsDashboard : function() {
		delete this.getStore('OpsDashboard').getProxy().extraParams;	
		var store = this.getStore('OpsDashboard');
        store.clearFilter();		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var OpsDashboardReport = this.getView('OpsDashboard.List').create();
		mainContent.removeAll();
		mainContent.add( OpsDashboardReport );
		mainContent.doLayout();		
	},
	
	downloadhierarchyCSV : function() {
		  var myStore = this.getStore('OpsDashboard');
		  if( myStore.getCount() == 0) {
		  	Ext.Msg.alert('Alert', 'No Data To Export!!!');
		  } else {
	   		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
	   		myMask.show();
	   		Ext.Ajax.request( {
	   			url: AM.app.globals.appPath+'index.php/reports/DownloadhierarchyCsv/',
	   			method: 'GET',
	   			params: {'Report':"reportflag"},
	   			success: function(responce, opts){
	   				var jsonResp = Ext.decode(responce.responseText);
	   				if(jsonResp.success) {	
	   					myMask.hide();
	   		    		window.location = AM.app.globals.appPath+'download/Employee.csv';
	   		    	}
				}
	   	    });
	   	} 
	},
	
	onTriggerKeyUp : function(t) {		
		Ext.Ajax.abortAll();
		var store = this.getStore('OpsDashboard');
		store.filters.clear();
        store.filter({
            property: 'company_employ_id,first_name,verticals.name,client.client_name,shift.shift_code,location.name,designation.hcm_grade,designation.name',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('OpsDashboard');
        store.clearFilter();
    }
	
});