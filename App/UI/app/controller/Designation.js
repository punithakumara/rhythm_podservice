Ext.define('AM.controller.Designation',{
	extend : 'Ext.app.Controller',

	stores : ['Designations'],
	views: ['Designation.List','Designation.Add_edit'],
	
	init : function(){
		this.control({
			'DesignationList' : {
				'deleteDesignation' : this.onDeleteDesignation,
				'editDesignation' : this.onEditDesignation
			},
			'DesignationList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
			'DesignationList button[action=createDesignation]' : {
				click : this.onCreate
			},
			'DesignationAdd_edit' : {
				'saveDesignation' : this.onSaveDesignation,
			}
		});
	},
	
	viewContent : function() {
		delete this.getStore('Designations').getProxy().extraParams;
		var store = this.getStore('Designations');
        store.clearFilter();
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var Designation = this.getView('Designation.List').create();
		mainContent.removeAll();
		mainContent.add( Designation );
		mainContent.doLayout();
	},
	
	onCreate : function() {
		var view = Ext.widget('DesignationAdd_edit');
		view.setTitle('Add Designation Details');
	},
	
	onSaveDesignation : function(form) {

		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
        });
		var myStore = this.getStore('Designations');
		var record = form.getRecord(),
		values = form.getValues();
		 if(values['PromoteToFlag'] == undefined || values['PromoteToFlag'] == null  || values['PromoteToFlag'] == ''){
			 values['PromoteToFlag']=0;
		 }
		 if(!record) {
			myStore.add(values);
			var titleText = 'Added';
		}
		else {
			record.set(values);
			var titleText = 'Updated';
		}		
		myStore.sync({ 
		    success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;
		    	
		    	if(jsonResp.success) {
		    		
		    		if(jsonResp.status == 3) { titleText = 'Warning'; }  //change alert title is status is 3  		
		    		Ext.MessageBox.show({
                        title: titleText,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    	}
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving attribute. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditDesignation : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('DesignationAdd_edit');
		view.setTitle('Edit Designations Details');
        view.down('form').loadRecord(rec);

	},
	
	onTriggerClear : function() {
        var store = this.getStore('Designations');
        store.clearFilter();
    },
    
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Designations');
		store.filters.clear();
        store.filter({
            property: 'Name, grades',
            value: t.getValue()
        });
    }
	
});