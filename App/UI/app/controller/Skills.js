Ext.define('AM.controller.Skills',{
	extend : 'Ext.app.Controller',	
	stores : ['EmployeeSkills','Tech','Skills','SkillSets','Tools','Technologies','Shifts','Designations','Languages','LanguagesList'],
	views: ['Skills.SearchSkills','Skills.EmpList','Skills.List','Skills.Skills_Add_edit','Skills.Configuration','Skills.TechList','Skills.Tech_Add_edit','Skills.SkillsList','Skills.ToolsList'],

	init : function(){
		this.control({
			'EmpList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
			'SkillsList #searchSkillsId' : {
				keyup : this.onTriggerKeyUpSearchSkills,
				triggerClear : this.onTriggerClearSearchSkills
			},
			'TechList #searchToolsId' : {
				keyup : this.onTriggerKeyUpSearchTech,
				triggerClear : this.onTriggerClearSearchTech
			},
			'skillsAddEdit' : {
				'saveSkill' : this.onSaveSkill
			},
			'techAddEdit' : {
				'saveTech' : this.onSaveTechnology
			},
			'ToolsList #gridToolTrigger' : {
				keyup : this.onTriggerKeyUpSearchTools,
				triggerClear : this.onTriggerClearSearchTools
			},
		});
	},

	viewContent : function() {
		var skillStore = this.getStore('Skills');
		skillStore.clearFilter();
		var techStore = this.getStore('Technologies');
		techStore.clearFilter();
		var toolStore = this.getStore('Tools');
		toolStore.clearFilter();

		var myStore = this.getStore('EmployeeSkills');
		myStore.clearFilter();

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var SearchSkills = this.getView('Skills.List').create();
		mainContent.removeAll();
		mainContent.add( SearchSkills );
		mainContent.doLayout();
	},

	listEmployees : function(skills,tools,technologies,shift,designation,experience,languages,certifications){
		Ext.getStore('EmployeeSkills').load({
			params: {
				'skills': skills,
				'tools': tools,
				'technologies': technologies,
				'shift': shift,
				'designation': designation,
				'experience': experience,
				// 'passport': passport,
				// 'visa': visa,
				// 'stepup': stepup,
				'languages': languages,
				'certifications': certifications
			},
			callback: function (records, options, success) {
				if (success) {
					// alert('djfdkfn');
				}
			}
		});
	},

	resetAllFilter : function(){
		Ext.getCmp('gridTrigger').setValue('');
		Ext.getCmp('skillset').setValue('');
		Ext.getCmp('tools').setValue('');
		Ext.getCmp('technologies').setValue('');
		Ext.getCmp('shift').setValue('');
		Ext.getCmp('designation').setValue('');
		Ext.getCmp('experience').setValue('');
		// Ext.getCmp('passport').setValue('');
		// Ext.getCmp('visa').setValue('');
		// Ext.getCmp('stepup').setValue('');
		Ext.getCmp('languages').setValue('');
		Ext.getCmp('certifications').setValue('');

		Ext.getStore('EmployeeSkills').load({
			params: {
				'filter' : '[{"property":"first_name,last_name","value":""}]',
				'skills': '',
				'tools': '',
				'technologies': '',
				'shift': '',
				'designation': '',
				'experience': '',
                // 'passport': '',
                // 'visa': '',
                // 'stepup': '',
                'languages': '',
                'certifications': '',
            },
            callback: function (records, options, success) {
            	if (success) {
					// alert('djfdkfn');
				}
			}
		});
	},

	downloadEmployeeSkillsCsv : function(search_param) {
		
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request( {
			url: AM.app.globals.appPath+'index.php/skills/downloadEmployeeSkillsCsv/',
			method: 'GET',
			params: {
				'Report':"reportflag",
				'filter' : '[{"property":"first_name,last_name","value":"'+search_param+'"}]',
				'skills': Ext.getCmp('skillset').getValue(),
				'tools': Ext.getCmp('tools').getValue(),
				'technologies': Ext.getCmp('technologies').getValue(),
				'shift': Ext.getCmp('shift').getValue(),
				'designation': Ext.getCmp('designation').getValue(),
				'experience': Ext.getCmp('experience').getValue(),
				// 'passport': Ext.getCmp('passport').getValue(),
				// 'visa': Ext.getCmp('visa').getValue(),
				// 'stepup': Ext.getCmp('stepup').getValue(),
				'languages': Ext.getCmp('languages').getValue(),
				'certifications': Ext.getCmp('certifications').getValue(),
			},
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);

				if(jsonResp.success) {	
					myMask.hide();
					if(jsonResp.data == 1)
					{
						window.location = AM.app.globals.appPath+'download/EmployeeSkills.csv';		
					}
					else
					{
						Ext.MessageBox.show({
							title: "Alert",
							msg: "No Data Available to export",
							icon: Ext.MessageBox.WARNING,
							buttons: Ext.Msg.OK
						});				
					}	
				}
			}
		});
	},
	
	onTriggerKeyUp : function(t) {		
		Ext.Ajax.abortAll();
		var store = this.getStore('EmployeeSkills');
		store.filters.clear();
		store.filter({
			// property: 'emp_name,emp_designation,emp_vertical,emp_skills,emp_tools,emp_technologies,emp_shift',
			property: 'first_name,last_name',
			value: t.getValue()
		});
	},

	onTriggerClear : function() {
		var store = this.getStore('EmployeeSkills');
		store.clearFilter();
	},

	onTriggerKeyUpSearchSkills : function(t) {		
		Ext.Ajax.abortAll();
		var store = this.getStore('Skills');
		store.filters.clear();
		store.filter({
			property: 'skill_name',
			value: t.getValue()
		});
	},

	onTriggerClearSearchSkills : function() {
		var store = this.getStore('Skills');
		store.clearFilter();
	},

	onTriggerKeyUpSearchTech : function(t) {		
		Ext.Ajax.abortAll();
		var store = this.getStore('Technologies');
		store.filters.clear();
		store.filter({
			property: 'skill_name',
			value: t.getValue()
		});
	},

	onTriggerClearSearchTech : function() {
		var store = this.getStore('Technologies');
		store.clearFilter();
	},

	onTriggerKeyUpSearchTools : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Tools');
		store.filters.clear();
		store.filter({
			property: 'skill_name',
			value: t.getValue()
		});
	},

	onTriggerClearSearchTools : function() {
		var store = this.getStore('Tools');
		store.clearFilter();
	},
	configurationList : function() {
		var skillStore = this.getStore('Skills');
		skillStore.clearFilter();

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var configSkills = this.getView('Skills.Configuration').create();
		mainContent.removeAll();
		mainContent.add( configSkills );
		mainContent.doLayout();
	},
	
	
	onCreateSkills : function() {
		var view = Ext.widget('skillsAddEdit');
		view.setTitle('Add Skills');
	},

	

	onEditSkills : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('skillsAddEdit');
		view.setTitle('Edit Skill');
		view.down('form').loadRecord(rec);
	},

	onSaveSkill : function(form, win) {
		
		var myStore = this.getStore('Skills');
		var record = form.getRecord(),
		values = form.getValues();

		form.url = myStore.getProxy().api.create;
		if(!record) 
		{
			var message = 'Skill Added Successfully';
		}
		else 
		{
			var message = 'Skill Updated Successfully';
		}	
		
		form.submit({ 
			method: 'POST',
			waitMsg : 'Saving your data, please wait...',
			success: function (form, o) {	    	
				var jsonResp = JSON.parse(o.response.responseText);
				console.log(jsonResp);
				if(jsonResp.success == 1) 
				{
					Ext.MessageBox.show({
						title: 'Success',
						msg: message,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						fn: function(buttonId) {
							if (buttonId === "ok" && jsonResp.title == "Added") 
							{
								Ext.getCmp('skillsListGridID').getStore().reload();
								win.close();
							}
						}
					});	
				}
				else 
				{
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK
					});
				}
			}, 
			failure: function (batch) {
				console.log("Error occured while saving Skill. Please try again.");
				Ext.MessageBox.hide();
				myStore.rejectChanges();
			}
		});
	},

	onCreateTechnology : function() {
		var skillStore = this.getStore('Technologies');
		skillStore.clearFilter();

		var view = Ext.widget('techAddEdit');
		view.setTitle('Add Technology');
	},

	onSaveTechnology : function(form, win) {
		
		var myStore = this.getStore('Technologies');
		var record = form.getRecord(),
		values = form.getValues();

		form.url = myStore.getProxy().api.create;
		if(!record) 
		{
			var message = 'Technology Added Successfully';
		}
		else 
		{
			var message = 'Technology Updated Successfully';
		}	

		form.submit({ 
			method: 'POST',
			waitMsg : 'Saving your data, please wait...',
			success: function (form, o) {	    	
				var jsonResp = JSON.parse(o.response.responseText);
				if(jsonResp.success == 1) 
				{
					Ext.MessageBox.show({
						title: 'Success',
						msg: message,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						fn: function(buttonId) {
							if (buttonId === "ok" && jsonResp.title == "Added") 
							{
								Ext.getCmp('techListGridID').getStore().reload();
								win.close();
							}
						}
					});	
				}
				else 
				{
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK
					});
				}
			}, 
			failure: function (batch) {
				console.log("Error occured while saving Skill. Please try again.");
				Ext.MessageBox.hide();
				myStore.rejectChanges();
			}
		});
	},

	onEditTechnology : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('techAddEdit');
		view.setTitle('Edit Technology');
		view.down('form').loadRecord(rec);
	},
	
});