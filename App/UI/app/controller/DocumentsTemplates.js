Ext.define('AM.controller.DocumentsTemplates',{
	extend : 'Ext.app.Controller',
	stores : ['Vertical','Documents','CompanyDocs','VerticalDocs','DocumentsTemplates','DocumentsVerticalTemplates'],
	views: ['documents.CompanyLevel','documents.CompanyDocuments','documents.CompanyTemplate','documents.VerticalLevel','documents.VerticalDocuments','documents.VerticalTemplate','documents.add_edit_company_details','documents.add_edit_vertical_details','PDF'],
	
	init : function() {
		this.control({
			'attributesList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
            
		});
	},
	
	viewCompanyDocuments : function() {
		
		if(Ext.util.Cookies.get('docAllow')=='no')
		{
			Ext.Msg.show({
				title:'Alert',
				msg:"Document cannot be accessed outside Theorem network",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
			
			return false;
		}
		else
		{
			var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
			mainContent.removeAll();
			var doc = this.getView('documents.CompanyLevel').create();
			mainContent.add( doc );
			mainContent.doLayout();
		}
		
		if(Ext.util.Cookies.get('grade')<4 && Ext.util.Cookies.get('empid')!=2341)
		{
			Ext.getCmp('compUpDoc').hide();
			Ext.getCmp('compUpTemp').hide();
		}
	},
	
	viewVerticalDocuments : function() {
		// if(Ext.util.Cookies.get('docAllow')=='no')
		// {
		// 	Ext.Msg.show({
		// 		title:'Alert',
		// 		msg:"Document cannot be accessed outside Theorem network",
		// 		icon: Ext.MessageBox.WARNING,
		// 		buttons: Ext.Msg.OK
		// 	});
			
		// 	return false;
		// }
		// else
		// {
			var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
			mainContent.removeAll();
			var doc = this.getView('documents.VerticalLevel').create();
			mainContent.add( doc );
			mainContent.doLayout();
		// }
		
		if(Ext.util.Cookies.get('grade')<4 && Ext.util.Cookies.get('empid')!=2341)
		{
			Ext.getCmp('vertUpDoc').hide();
			Ext.getCmp('vertUpTemp').hide();
		}
	},

	onCreate : function() {
		var view = Ext.widget('docAddEdit');
	},

	onCreateTemplate : function() {
		var view = Ext.widget('tempAddEdit');
		
		// if(Ext.util.Cookies.get('grade')==4)
		// {
		// 	Ext.getCmp('docVerticalID').setValue(Ext.util.Cookies.get('VertId'));
		// 	//Ext.getCmp('docVerticalID').setReadOnly(true);
		// }
	},

	onSaveDocument : function(form, window, access) 
	{
		var myStore = this.getStore('Documents');
		var compDocStore = this.getStore('CompanyDocs');
		var vertDocStore = this.getStore('VerticalDocs');
		var compTempStore = this.getStore('DocumentsTemplates');
		
		var record = form.getRecord(), values = form.getValues();
		
		if(!record) 
		{
			myStore.add(values);
			form.url = myStore.getProxy().api.create;
			var titleText = 'Added';
		}
		else 
		{
			var docEditVert="",PreviousUploadName="";
			var docModifiedVert="";
			PreviousUploadName = record.get('UploadName');

			if(access == "All managers" || access == "All")
			{
				docEditVert="";
			}
			else if(access == "Vertical Specific")
			{
				docEditVert=record.raw.VerticalID;
			}
			
			if(access == "All managers" || access == "All")
			{
				docModifiedVert = "";
			}
			else if(access == "Vertical Specific")
			{
				docModifiedVert = values.VerticalID;
			}
			
			form.url = myStore.getProxy().api.update;
			form.baseParams = {'docEditVert':docEditVert,'docModifiedVert':docModifiedVert,'rawAccess':access,'modifiedAccess':values.Access,'PreviousUploadName':PreviousUploadName};
			
			// form.params = myStore.getProxy().extraParams;
			record.set(values);
			var titleText = 'Updated';
		}		
		
		

		//console.log('testing...');
		form.submit({
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
            success: function(form,o) {
                var retrData = JSON.parse(o.response.responseText);
                if(retrData.success) 
				{
					if(retrData.filesize=='max') 
					{
						window.destroy();
						//AM.app.getController('ProDocuments').onCreate();
						Ext.MessageBox.show({
							title: 'Error',
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {						
								if (buttonId === "ok") {									
									window.destroy();
								}
							}
						});
					}
					else if(retrData.file=='notexist') 
					{
						window.destroy();
						//AM.app.getController('ProDocuments').onCreate();
						Ext.MessageBox.show({
							title: 'Error',
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {						
								if (buttonId === "ok") {									
									window.destroy();
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: titleText,
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									compDocStore.load();
									compTempStore.load();
									vertDocStore.load();
									window.destroy();
								}
							}
						});
					}
				}
		    	else 
				{
		    		Ext.MessageBox.show({
                        //title: 'Error',
                        msg: retrData.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                            	window.destroy();
                            }
                        }
                    });
		    	}
                //myStore.load();				
				window.destroy();
            },
            failure: function(form, action)
            {
            	console.log("Error occured while saving Document. Please try again.");
                myStore.rejectChanges();
                window.close();
            }
        });
	},

	onSaveTemplate : function(form, window, access) 
	{
		var myStore = this.getStore('DocumentsTemplates');
		var compDocStore = this.getStore('CompanyDocs');
		var vertDocTempStore = this.getStore('DocumentsVerticalTemplates');
		var compTempStore = this.getStore('DocumentsTemplates');
		
		var record = form.getRecord(), values = form.getValues();
		
		if(!record) 
		{
			myStore.add(values);
			form.url = myStore.getProxy().api.create;
			var titleText = 'Added';
		}
		else 
		{
			var docEditVert="",PreviousUploadName="";
			var docModifiedVert="";
			PreviousUploadName = record.get('UploadName');

			if(access == "All managers" || access == "All")
			{
				docEditVert="";
			}
			else if(access == "Vertical Specific")
			{
				docEditVert=record.raw.VerticalID;
			}
			
			if(access == "All managers" || access == "All")
			{
				docModifiedVert = "";
			}
			else if(access == "Vertical Specific")
			{
				docModifiedVert = values.VerticalID;
			}
			
			form.url = myStore.getProxy().api.update;
			form.baseParams = {'docEditVert':docEditVert,'docModifiedVert':docModifiedVert,'rawAccess':access,'modifiedAccess':values.Access,'PreviousUploadName':PreviousUploadName};
			
			// form.params = myStore.getProxy().extraParams;
			record.set(values);
			var titleText = 'Updated';
		}		
		
		//console.log('testing...');
		form.submit({
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
            success: function(form,o) {
                var retrData = JSON.parse(o.response.responseText);
                if(retrData.success) 
				{
					if(retrData.filesize=='max') 
					{
						window.destroy();
						//AM.app.getController('ProDocuments').onCreate();
						Ext.MessageBox.show({
							title: 'Error',
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {						
								if (buttonId === "ok") {									
									window.destroy();
								}
							}
						});
					}
					else if(retrData.file=='notexist') 
					{
						window.destroy();
						//AM.app.getController('ProDocuments').onCreate();
						Ext.MessageBox.show({
							title: 'Error',
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {						
								if (buttonId === "ok") {									
									window.destroy();
								}
							}
						});
					}
					else 
					{
						Ext.MessageBox.show({
							title: titleText,
							msg: retrData.msg,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									compDocStore.load();
									compTempStore.load();
									vertDocTempStore.load();
									window.destroy();
								}
							}
						});
					}
				}
		    	else 
				{
		    		Ext.MessageBox.show({
                        //title: 'Error',
                        msg: retrData.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK,
                        fn: function(buttonId) {
                            if (buttonId === "ok") {
                            	window.destroy();
                            }
                        }
                    });
		    	}
                //myStore.load();				
				window.destroy();
            },
            failure: function(form, action)
            {
            	console.log("Error occured while saving Document. Please try again.");
                myStore.rejectChanges();
                window.close();
            }
        });
	},
	
	
	// ******* viewing uploaded file ******* //
	onViewDocuments:function(grid, row, col)
	{
		var rec = grid.getStore().getAt(row);
		var doctype = "";
	
		if(rec.get('Access') == "All managers" || rec.get('Access') == "All")
		{
			doctype ="type=company&filename="+rec.get('UploadName');
		}
		else
		{
			Folder = "temp";
			
			if(rec.get('Folder') != "")
				Folder = rec.get('Folder');
				
			doctype ="type=vertical&folder="+Folder+"&filename="+rec.get('UploadName');
		}
		
		doctype = AM.app.globals.appPath+'index.php/documents/document_access/?'+doctype;
		
		var view = this.getView('PDF').create({doctype:doctype,docs:'Documents'});
		//var view = Ext.widget('DocumentsPDF');	
	},
	
	
	onDeleteDocuments:function(grid, row, col)
	{
		Ext.Msg.confirm('Confirm', 'Are you sure you want to Delete?', function (button) {
			if (button == 'yes') 
			{
				var compStore = this.getStore('CompanyDocs');
				var vertStore = this.getStore('VerticalDocs');
				var processId = '';
				Folder = "temp";
				var rec = grid.getStore().getAt(row);
				var doctyp = rec.get('Access');
				
				if(rec.get('Folder')!=undefined)
				{
					if(rec.get('Folder') != "")
						Folder = rec.get('Folder');
				}
				
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/documents/Documents_dele',
					method: 'POST',
					// async: false,
					params: {
						'doctype': doctyp,
						'DocumentID': rec.get('DocumentID'),
						'uploadName':rec.get('UploadName'),
						'getFolder':Folder
					},
					success: function(response) {
						var myObject = Ext.JSON.decode(response.responseText);
						
						if(myObject["success"])
						{
							Ext.MessageBox.show({
								title: "Success",
								msg: "Successfully deleted",
								buttons: Ext.Msg.OK
							});
							
							compStore.load();
							vertStore.load();
						}
						else
						{
							Ext.MessageBox.show({
								title: 'Error',
								msg: " Failed",
								icon: Ext.MessageBox.ERROR,
								buttons: Ext.Msg.OK
							});
						}
					},
				});
			}
		}, this);
	},
	
	
	onDownloadTemplate:function(grid, row, col,template_type)
	{
		var rec = grid.getStore().getAt(row);
		var temp_id = rec.get('template_id');
		var fname=rec.get('uploaded_default_name');
		var exttype = fname.substr((Math.max(0, fname.lastIndexOf("."))));  
		
		var doctype = "";
		if(template_type == "1")
		{
			doctype ="type=company&filename="+rec.get('uploaded_default_name');
		}
		else if(template_type == "3")
		{
			doctype ="type=vertical&filename="+rec.get('uploaded_default_name');
		}
		
		doctype = AM.app.globals.appPath+'index.php/documents/document_download/?'+doctype;
		
		Ext.DomHelper.append(document.body, {
			tag: 'iframe',
			id:'downloadIframe',
			frameBorder: 0,
			width: 0,
			height: 0,
			css: 'display:none;visibility:hidden;height: 0px;',
			src: doctype
		});
	},
	
	
	// ******* viewing uploaded Template ******* //
	onViewTemplate:function(grid, row, col,template_type){
		var rec = grid.getStore().getAt(row);
		var doctype = "";
			if(template_type == "1")
			{
				doctype ="companyDocs/templates/"+rec.get('uploaded_default_name');
			}
			else if(template_type == "3")
			{
				doctype ="verticalDocs/templates/"+rec.get('uploaded_default_name');
			}
		var view = this.getView('PDF').create({doctype:doctype,docs:'Documents'});
	},
	
	
	onDeleteTemplate:function(grid, row, col,temp_ids){
		if(temp_ids=='1')
			var myStore = this.getStore('DocumentsTemplates');
		else if(temp_ids=='3')
			var myStore= this.getStore('DocumentsVerticalTemplates');
		
		var DStore = this.getStore('DocumentsTemplates');

		Ext.Msg.confirm('Confirm', 'Are you sure you want to Delete?', function (button) {
			if (button == 'yes') 
			{
				Folder = "template";
				var rec = grid.getStore().getAt(row);
				var template_id = rec.get('template_id');
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/DocumentsTemplates/template_dele',
					method: 'POST',
					// async: false,
					params: {
						'temp_id': template_id,
						'uploaded_default_name':rec.get('uploaded_default_name'),
						'getFolder':Folder,
						'template_type':temp_ids
					},
					success: function(response) {
						var myObject = Ext.JSON.decode(response.responseText);
						
						if(myObject["success"]){
							Ext.MessageBox.show({
		                        title: "Alert",
		                        msg: "Deleted successfully",
		                        buttons: Ext.Msg.OK
		                    });
							
                        	DStore.load({
                        	    params: {template_type: '1'}
                        	})
							myStore.load({
                        	    params: {template_type: temp_ids}
                        	});
						}
						else
						{
							Ext.MessageBox.show({
			                    title: 'Error',
			                    msg: " Failed",
			                    icon: Ext.MessageBox.ERROR,
			                    buttons: Ext.Msg.OK
			                });
						}
					}
				
				});
			}
		}, this);
	},

	
});