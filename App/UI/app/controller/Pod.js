Ext.define('AM.controller.Pod',{
	extend : 'Ext.app.Controller',
	stores : ['Pod','AllManagers','Shifts', 'ParentPod'],
    models : ['Pod'],
	views: ['pod.List','pod.Add_edit'],
	
	init : function() {
		this.control({
			'podList' : {
				'deletePod' : this.onDeletePod,
				'editPod' : this.onEditPod
			},
			'podList #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
            'podList #podStatusCombo' : {
                podChangeStatusCombo : this.changePodStatus
            },
			'podList button[action=createPod]' : {
				click : this.onCreate
			},
			'podAddEdit' : {
				'savePod' : this.onSavePod,
			}
		});
	},
	
	viewContent : function(fromdashboard) {
		delete this.getStore('Pod').getProxy().extraParams;
		var store = this.getStore('Pod');
		store.getProxy().extraParams = {'podStatusCombo': 1};
		if(fromdashboard!=undefined)
			store.getProxy().extraParams = {'dashboard':fromdashboard};
		
        store.clearFilter();
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var vertical = this.getView('pod.List').create();
		mainContent.removeAll();
		mainContent.add( vertical );
		mainContent.doLayout();
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Pod');
		store.filters.clear();
        store.filter({
            property: 'temp.pod_name, temp.description, temp.folder, emp.first_name, emp.last_name,sft.shift_code',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Pod');
        store.clearFilter();
    },
	
	onCreate : function() {
		var view = Ext.widget('podAddEdit');
		view.setTitle('Add POD Details');
		Ext.ComponentQuery.query("#pod_Status_combo")[0].hide();
	},
	
	onSavePod : function(form, win) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
        });
		var myStore = this.getStore('Pod');
		var record = form.getRecord(),
		values = form.getValues();
		if(!record) 
		{
			myStore.add(values);
			var titleText = 'Added';
		}
		else 
		{
			record.set(values);
			var titleText = 'Updated';
		}		
		myStore.sync({ 
		    success: function (batch, operations) {
		    	var jsonResp = batch.proxy.getReader().jsonData;				
		    	if(jsonResp.success) {				
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });			
		    		if(jsonResp.title!="Existing") {
		    			win.close();
					}
					if(jsonResp.title=="Existing") {
						if(titleText != "Added"){myStore.load();win.close();}
					myStore.load();
					myStore.clearFilter();
					}
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    	myStore.load();
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while saving pod. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	onEditPod : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var view = Ext.widget('podAddEdit');
		view.setTitle('Edit POD Details');
		Ext.ComponentQuery.query("#pod_Status_combo")[0].show();
        view.down('form').loadRecord(rec);

	},

	changePodStatus: function(combo){
		var store = this.getStore('Pod');
		store.getProxy().extraParams = {'podStatusCombo': combo.getValue()};
		store.load();
		if(combo.getValue())
			store.clearFilter();
	},
	
	onDeletePod : function(grid, row, col) {
		Ext.MessageBox.show({
            msg: 'Saving your data, please wait...',
            progressText: 'Saving...',
            width:300,
            wait:true,
            waitConfig: {interval:200},
            animateTarget: 'waitButton'
         });
		var rec = grid.getStore().getAt(row);
		var podName = rec.get('Name');
		Ext.Msg.confirm('Delete Pod', 'Are you sure to delete this POD ?', function (button) {
            if (button == 'yes') {
            	Ext.MessageBox.show({
                    msg: 'Saving your data, please wait...',
                    progressText: 'Saving...',
                    width:300,
                    wait:true,
                    waitConfig: {interval:200},
                    animateTarget: 'waitButton'
                 });
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) {
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else {
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting client. Please try again.");
        		    	Ext.MessageBox.hide();
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	}
});