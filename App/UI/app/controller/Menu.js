Ext.define('AM.controller.Menu',{
	extend : 'Ext.app.Controller',
	views : ['Menu'],

    viewMenu : function() {
    	var mainTool = Ext.ComponentQuery.query('#menuToolbar')[0];
		mainTool.removeAll();
        var menu = this.getView('Menu').create();
        mainTool.add( menu );
    },

	
	/* Home stars here */
	
	viewDashboard : function()
	{
		this.getController("AM.controller.Dashboard").viewContent();
    },
	
	/* Home ends here */
	

	/* Account Management Starts here */
	
	worDashboardList : function(){
    	this.getController("AM.controller.ClientWOR").initWOR();
	},
	
	corDashboardList : function(){
    	this.getController("AM.controller.ClientCOR").initCOR();
	},
	

	
	/* Account Management ends here */

	
	/* Ops Data starts here */
	
	onBoardList : function(){
    	this.getController("AM.controller.OnBoarding").viewContent();
	},
	
	shiftTransPage : function(){
    	this.getController("AM.controller.Transition").viewShiftPage();
	},

	leadTransPage : function(){
    	this.getController("AM.controller.Transition").viewLeadPage();
	},

	// verticalTransPage : function(){
 //    	this.getController("AM.controller.Transition").viewLeadPage();
	// },

	releaseHrPage : function(){
    	this.getController("AM.controller.Transition").viewReleaseHrPage();
	},

	// traineePage : function(){
 //    	this.getController("AM.controller.Transition").viewTraineePage();
	// },

	pullHrPage : function(){
    	this.getController("AM.controller.Transition").viewPullHrPage();
	},	
	timesheetList : function(){
    	this.getController("AM.controller.Timesheet").viewContent();
	},
	onshoreTimesheetList : function(){
    	this.getController("AM.controller.TimesheetOnshore").viewContent();
	},	
	approveTimesheetList : function(){
    	this.getController("AM.controller.Timesheet").approveTimesheet();
	},
	viewOnshoreTimesheetList : function(){
    	this.getController("AM.controller.TimesheetOnshore").viewOnshoreTimesheet();
	},
	timesheetDashboard : function(){
    	this.getController("AM.controller.Timesheet").viewDashboard();
	},
	
	categorizationList : function(){
    	this.getController("AM.controller.Categorization").viewContent();
	},
	
	approveUtilizationList : function(){
    	this.getController("AM.controller.UtilizationTest").approveUtilization();
	},
	
	utilizationList : function(){
    	this.getController("AM.controller.UtilizationTest").viewContent();
	},
	
	accuracyList : function(){
    	this.getController("AM.controller.Accuracy").viewContent();
	},
	
	appreciationList : function(){
    	this.getController("AM.controller.newIncidentLog").viewContent('appr');
	},
	
	escalationList : function(){
    	this.getController("AM.controller.newIncidentLog").viewContent('esc');
	},
	
	errorList : function(){
    	this.getController("AM.controller.newIncidentLog").viewContent('error');
	},

	/* Ops Data ends here */
	
	
	/* Master Data starts here */
	
	verticalList : function() {
    	this.getController("AM.controller.Vertical").viewContent();
    },

    podList : function() {
    	this.getController("AM.controller.Pod").viewContent();
    },
    
    clientList : function() {
    	this.getController("AM.controller.Clients").viewContent();
    },

    servicesList : function() {
    	this.getController("AM.controller.Services").viewContent();
    },
	
	processList : function() {
    	this.getController("AM.controller.Process").viewContent();
    },
    
    designationList : function() {
    	this.getController("AM.controller.Designation").viewContent();
    },
    
    empList : function() {
    	this.getController("AM.controller.Employees").viewContent();
    },
	
	configurationList : function() {
    	this.getController("AM.controller.Skills").configurationList();
    },
	
	attributesList : function() {
    	this.getController("AM.controller.Attributes").viewContent();
    },
	
	clientHolidaysList : function(){
    	this.getController("AM.controller.Holidays").viewHolidaysList();
	},	
	
	/* Master Data ends here */
	
	/* Reports Data Starts here */
	
	OpsDashboard : function(){
    	this.getController("AM.controller.OpsDashboard").OpsDashboard();
    },

    ResourceMapping : function(){
    	this.getController("AM.controller.ResourceMapping").viewContent();
    },

    MbrReport : function(){
    	this.getController("AM.controller.MbrReport").viewContent();
    },
	
	ConsolidatedUtilizationReportList : function(){
    	this.getController("AM.controller.ConsolidatedUtilizationReport").viewContent();
    },
	
	monthlyBillabilityReportPage : function(){
    	this.getController("AM.controller.MonthlyBillabilityReport").viewContent();
    },
	
	monthlyBillabilityFteReportPage : function(){
    	this.getController("AM.controller.MonthlyBillabilityReport").viewFteContent();
    },
	
	monthlyBillabilityProjectReportPage : function(){
    	this.getController("AM.controller.MonthlyBillabilityReport").viewProjectContent();
    },
	
	/* Reports Data ends here */
	
	
	/* Other Apps Starts here */
	
	companyDocumentsPage : function(){
    	this.getController("AM.controller.DocumentsTemplates").viewCompanyDocuments();
    },
	verticalDocumentsPage : function(){
    	this.getController("AM.controller.DocumentsTemplates").viewVerticalDocuments();
    },
	
	/* Other Apps ends here */
	
	
	viewNotifyMenu : function() {
    	var mainTool = Ext.ComponentQuery.query('#NotifyToolbar')[0];
    	mainTool.removeAll();
        var menu = this.getView('NotifyMenu').create();
        mainTool.add( menu );
        //Interval function to get notifications count dynamically
        /* window.setInterval(function() {
        	Ext.Ajax.request({
            url		: AM.app.globals.appPath+'index.php/employees/notificationCount/?v='+AM.app.globals.version,
            timeout	: 4000,
            success	: function(response) 
			{
            	if(response.responseText !== "") 
				{
            		var countData	= Ext.JSON.decode(response.responseText);
            		var notiObj 	= Ext.getCmp("logInCont").getEl();
            		notiObj.down('.notiCnt').update(countData.count);
            		Ext.util.Cookies.clear('Notifications');
            		Ext.util.Cookies.set('Notifications', countData.count);
            	} 
            }
          })
        }, 10000); */
    },


    worTypes : function(){
    	this.getController("AM.controller.WorTypes").viewWorTypes();
	},	

	projectTypes : function(){
    	this.getController("AM.controller.projectTypes").viewProjectTypes();
	},	

	roleTypes : function(){
    	this.getController("AM.controller.RoleTypes").viewRoleTypes();
	},	

	searchSkills : function(){
    	this.getController("AM.controller.Skills").viewContent();
	},
	
});