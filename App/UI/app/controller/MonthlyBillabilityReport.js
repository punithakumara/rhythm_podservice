Ext.define('AM.controller.MonthlyBillabilityReport',{
	extend : 'Ext.app.Controller',
	stores : ['MonthlyBillabilityReport','MonthlyBillabilityFteReport','MonthlyBillabilityProjectReport','Remarks'],
	views: ['MonthlyBillabilityReport.HourlyVolumeReport', 'MonthlyBillabilityReport.FteReport','MonthlyBillabilityReport.ProjectReport', 'MonthlyBillabilityReport.ReportRemarks', 'MonthlyBillabilityReport.MonthField'],

	init : function() {
		this.control({
			'HourlyVolumeReport #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
			'FteReport #gridTrigger' : {
				keyup : this.onTriggerFteKeyUp,
				triggerClear : this.onTriggerFteClear
			},
			'ProjectReport #gridTrigger' : {
				keyup : this.onTriggerProjectKeyUp,
				triggerClear : this.onTriggerProjectClear
			}
		});
	},
	
	viewFteContent : function() 
	{
		delete this.getStore('MonthlyBillabilityFteReport').getProxy().extraParams;	
		var store = this.getStore('MonthlyBillabilityFteReport');
		store.clearFilter();		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var MonthlyBillabilityFteReport = this.getView('MonthlyBillabilityReport.FteReport').create();
		mainContent.removeAll();
		mainContent.add(MonthlyBillabilityFteReport);
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};
		store.load();
		mainContent.doLayout();	
	},
	
	viewProjectContent : function() {
		delete this.getStore('MonthlyBillabilityProjectReport').getProxy().extraParams;	
		var store = this.getStore('MonthlyBillabilityProjectReport');
		store.clearFilter();		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var MonthlyBillabilityProjectReport = this.getView('MonthlyBillabilityReport.ProjectReport').create();
		mainContent.removeAll();
		mainContent.add(MonthlyBillabilityProjectReport);
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};
		store.load();
		mainContent.doLayout();	
	},
	
	viewContent : function() {
		delete this.getStore('MonthlyBillabilityReport').getProxy().extraParams;	
		var store = this.getStore('MonthlyBillabilityReport');
		store.clearFilter();		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var MonthlyBillabilityReport = this.getView('MonthlyBillabilityReport.HourlyVolumeReport').create();
		mainContent.removeAll();
		mainContent.add(MonthlyBillabilityReport);
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};
		//store.load();
		mainContent.doLayout();	
	},
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('MonthlyBillabilityReport');
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};					
		store.filters.clear();
		store.filter({
			property: 'client.client_name',
			value: t.getValue()
		});
	},

	onTriggerClear : function() 
	{
		var store = this.getStore('MonthlyBillabilityReport');
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};
		store.clearFilter();
	},

	onTriggerFteKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('MonthlyBillabilityFteReport');
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};					
		store.filters.clear();
		store.filter({
			property: 'client.client_name',
			value: t.getValue()
		});
	},

	onTriggerFteClear : function() 
	{
		var store = this.getStore('MonthlyBillabilityFteReport');
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};
		store.clearFilter();
	},

	onTriggerProjectKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('MonthlyBillabilityProjectReport');
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};					
		store.filters.clear();
		store.filter({
			property: 'client.client_name',
			value: t.getValue()
		});
	},

	onTriggerProjectClear : function() 
	{
		var store = this.getStore('MonthlyBillabilityProjectReport');
		store.getProxy().extraParams	= {'mbrmonth':Ext.getCmp('mbrmonth').getValue()};
		store.clearFilter();
	},

	downloadCSV: function(model){
		var myStore = this.getStore('MonthlyBillabilityReport');
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/MonthlyBillabilityReport/DownloadMonthlyBillabilityReportCsv/?v='+AM.app.globals.version,
			method: 'GET',
			params: {'csvreport':"1",'model':model,'mbrmonth':Ext.getCmp('mbrmonth').getValue()},
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);
				if(jsonResp.success)
				{	
					myMask.hide();
					if(model == 'fte')
					{
						window.location = AM.app.globals.appPath+'download/MonthlyBillabilityFteReport.csv?v='+AM.app.globals.version;
					}
					else if(model == 'hourly_volume')
					{
						window.location = AM.app.globals.appPath+'download/MonthlyBillabilityHourlyVolumeReport.csv?v='+AM.app.globals.version;
					}
					else if(model == 'project')
					{
						window.location = AM.app.globals.appPath+'download/MonthlyBillabilityProjectReport.csv?v='+AM.app.globals.version;
					}
				}
			}
		});
	},

	mbrFun : function(me,mbrmonth,enddte){
		Ext.getStore('MonthlyBillabilityReport').load({params:{'mbrmonth':mbrmonth},
			callback:function(records, options, success) {
				if (success) 
				{
					// me.storeLoad(me.store,me);
					me.doLayout(false,true);
				}
			}
		});
	},
	
	mbrFteFun : function(me,mbrmonth,enddte){
		Ext.getStore('MonthlyBillabilityFteReport').load({params:{'mbrmonth':mbrmonth},
			callback:function(records, options, success) {
				if (success) 
				{
					// me.storeLoad(me.store,me);
					me.doLayout(false,true);
				}
			}
		});
	},

	mbrProjectFun : function(me,mbrmonth){
		Ext.getStore('MonthlyBillabilityProjectReport').load({params:{'mbrmonth':mbrmonth},
			callback:function(records, options, success) {
				if (success) 
				{
					// me.storeLoad(me.store,me);
					me.doLayout(false,true);
				}
			}
		});
	},
	
	viewRemarks : function(grid, row, col,mbrmonth) {
		var rec = grid.getStore().getAt(row);
		console.log(rec);
		if(rec.get('prorated_fte')==0)
		{
			Ext.Msg.alert('Alert', 'No Prorated FTE!!!');
			return false;
		}
		else
		{
			var view = Ext.widget('ReportRemarks');
			view.setTitle("Prorated FTE's");
			
			var myStore = this.getStore('Remarks').load({params: {'mbrmonth' : mbrmonth, 'responsibility_id':rec.get('responsibility_id'),'fte_id':rec.get('fte_id'),'client_id':rec.get('client_id')}});
			
			Ext.getCmp('remarks_grid').bindStore(myStore);
		}
	},
});