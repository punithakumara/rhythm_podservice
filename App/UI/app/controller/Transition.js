Ext.define('AM.controller.Transition', {
    extend: 'Ext.app.Controller',
    views: ['transition.transWizard','transition.transClientPage','transition.transPullHrPage','transition.transShiftPage','transition.transRelEmpThirdPage','transition.transShiftThirdPage','transition.transLeadThirdPage','transition.transPullHrEmpThirdPage','transition.transFourthPage','transition.transLeadPage'],
	stores:['AllReportees','Shifts','TeamLead_AM','PullFromHR','Pod','Services','ReportTo'],

	viewShiftPage : function() 
	{
    	AM.app.transitionTargetPage ="shiftTrans";
		AM.app.globals.transEmployRes = [];
		Ext.getStore('AllReportees').load({params: {'searchTxt': '','NoHrPoolEmployees': '1', 'manager' : Ext.util.Cookies.get('employee_id')}});

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.layout.setActiveItem(2);
		wizTrans.setTitle('Shift Transition');
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();

		Ext.getCmp('transShiftGridID').getStore().reload();
	},
	
	viewLeadPage : function() 
	{
    	AM.app.transitionTargetPage ="leadTrans";
		AM.app.globals.transEmployRes = [];

		Ext.getStore('AllReportees').load({params: {'searchTxt': '','NoHrPoolEmployees': '1', 'manager' : Ext.util.Cookies.get('employee_id')}});

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.layout.setActiveItem(6);
		wizTrans.setTitle('Supervisor Transition');
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();

		Ext.getCmp('transLeadGridID').getStore().reload();
	},
	
    viewReleaseHrPage : function()
	{
    	AM.app.transitionTargetPage ="releaseHr";
		AM.app.globals.transEmployRes = [];
		Ext.getStore('AllReportees').load({params: {'searchTxt': '','NoHrPoolEmployees': '1', 'manager' : Ext.util.Cookies.get('employee_id')}});
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.setTitle('Release to RMG');
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
		
		Ext.getCmp('transClntGridID').getStore().reload();
    },
	
	viewPullHrPage : function() 
	{
    	AM.app.transitionTargetPage ="pullHr";
		AM.app.globals.transEmployRes = [];
		Ext.getStore('PullFromHR').load({params: {'searchTxt': '', 'manager' : Ext.util.Cookies.get('employee_id')}});;
		Ext.getStore('Pod');
		Ext.getStore('Services');
		Ext.getStore('TeamLead_AM').load({params:{'employee_type':'team_leader'}});

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.layout.setActiveItem(1);
		wizTrans.setTitle('Pull from RMG');
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();

		Ext.getCmp('transHRGridID').getStore().reload();
	},
	
	showTransSecondPage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');	
		
		if(AM.app.transitionTargetPage=="shiftTrans")
		{
			wizard.getLayout().setActiveItem('transitionShiftPage');
		}
		else if(AM.app.transitionTargetPage=="leadTrans")
		{
			wizard.getLayout().setActiveItem('transitionLeadPage');
		}
		else if(AM.app.transitionTargetPage=="pullHr")
		{
			wizard.getLayout().setActiveItem('transitionHrPage');
		}
		else
		{
			wizard.getLayout().setActiveItem('transitionClientPage');
		}
	},
	
	
	showTransThirdPage :function(btn) 
	{               
		var desGrid = Ext.getCmp('transGridID');
		var desGrid = Ext.getCmp('transHrGridID');
		var selection =""; 
		var wizard = btn.up('#wizardForm');
		var pullHrPool = true, traningNeeded = false, compareVal ="";

		Ext.getCmp('hrPoolStatus').reset();
		Ext.getCmp('transRelComment').setValue('');
		
		var myAry =  AM.app.globals.transEmployRes;
		myAry = myAry.filter(function(e){return e;});
		var shiftAry = leadAry = [];
		var gAry = [];
		var gradeAry = 0;

		if(myAry.length != 0 && !(myAry.length == 1 && myAry[0] == null))
		{
			for (var i=0; i< myAry.length; i++)
			{
				shiftAry.push(myAry[i].data.current_shift);
				leadAry.push(myAry[i].data.supervisor_id);
				gAry.push(myAry[i].data.grades);

				if(gradeAry<myAry[i].data.grades)
				{
					gradeAry = myAry[i].data.grades;
				}
				
			}

			if(AM.app.transitionTargetPage=="shiftTrans")
			{
				shiftAry = (Ext.Array.unique(shiftAry)).toString();
				Ext.getStore('Shifts').load({params:{'shift_code':shiftAry}});

				Ext.getCmp('transShiftThirdGridID').getStore().reload();
				Ext.getCmp('transShiftThirdGridID').store.add(Ext.Array.unique(myAry));
				
				wizard.getLayout().setActiveItem('transShiftThirdPage');

				Ext.getCmp('selectedShift').setValue(shiftAry);
			}
			else if(AM.app.transitionTargetPage=="leadTrans")
			{
				Ext.getStore('TeamLead_AM').load({params:{'grades':gradeAry,'supervisor':leadAry}});

				Ext.getCmp('transLeadThirdGridID').getStore().reload();
				Ext.getCmp('transLeadThirdGridID').store.add(Ext.Array.unique(myAry));
				
				wizard.getLayout().setActiveItem('transLeadThirdPage');

				Ext.getCmp('selectedGrade').setValue(gradeAry);
				Ext.getCmp('selectedCurrentLead').setValue(leadAry);
			}
			else if(AM.app.transitionTargetPage=="pullHr")
			{
				//check array values are same or  different
			    for(var i = 0; i < gAry.length - 1; i++) 
			    {
			        if(gAry[i] != gAry[i+1]) 
			        {
			            pullHrPool = false;
			        }
			    }
				
				if(!pullHrPool)
				{
					Ext.Msg.show({
						title:'Transition Alert Message',
						msg:"Grade should be same for all selected Employees",
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK
					});
					return false;
				}
				else
				{
					/*if(gradeAry>2)
					{
						Ext.getCmp('assign_team_lead').show();
					}
					else
					{
						Ext.getCmp('assign_team_lead').hide();
					}*/
					Ext.getCmp('transPullHRThirdGridID').getStore().reload();
					Ext.getCmp('transPullHRThirdGridID').store.add(Ext.Array.unique(myAry));

					wizard.getLayout().setActiveItem('transPullHrEmpThirdPage');
				}
			}
			else
			{
				Ext.getCmp('transRelThirdGridID').getStore().reload();
				Ext.getCmp('transRelThirdGridID').store.add(Ext.Array.unique(myAry));

				wizard.getLayout().setActiveItem('transReleaseEmpThirdPage');
			}
		}
		else
		{
			Ext.Msg.show({
				title:'Warning',
				msg:"Please Select Employee(s)",
				icon: Ext.MessageBox.WARNING,
				buttons: Ext.Msg.OK
			});
		}
	},
	
	onShiftSavePage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');		
		var store = Ext.getCmp('transShiftThirdGridID').store;		
		var empIds = '', Grade = '', transitiongMsg = "", transitionShiftDate = "", transitionShift = '';; 
		var transitionShiftComment = "";
		
		store = Ext.getCmp('transShiftThirdGridID').store;
		transitionShiftDate = Ext.getCmp('dateShiftStatus').getValue();
		transitionShift = Ext.getCmp('shiftStatus').getValue();
		TransitionShiftComment = Ext.getCmp('transShiftComment').getValue();

		for(i=0; i < store.data.length; i++)
		{
			var user = store.getAt(i);
			empIds += (empIds != '')?';'+user.data.id:user.data.id;
			Grade += (Grade != '')?';'+parseFloat(user.data.grades):parseFloat(user.data.grades);		
		}
		
		// btn.setDisabled(true);
		transitiongMsg = "Employee has been moved successfully.";

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Transition/empSaveShiftTransition?v='+AM.app.globals.version,
			method: 'GET',
			params: {
				'EmployID':empIds,
				'Grade':Grade,
				'transitionShiftDate':transitionShiftDate,
				'transitionShift':transitionShift,
				'TransitionShiftComment':TransitionShiftComment,
			},
			scope: this,
			success: function(response) 
			{
				var myObject = Ext.JSON.decode(response.responseText); 
				
				wizard.setTitle('People Transition');				
				wizard.getLayout().setActiveItem('transitionFourthPage');				
				Ext.getCmp('TransDisplayMsg').setValue(transitiongMsg);					
			}
		});	
	},
	
	onSavePage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');		
		var store = Ext.getCmp('transRelThirdGridID').store;		
		var empIds = '', Grade = '', transitiongMsg = "", hrPoolStatus = ""; 
		var transitionComment = "";
		
		store = Ext.getCmp('transRelThirdGridID').store;
		transitionComment = Ext.getCmp('transRelComment').getValue();
		hrPoolStatus = Ext.getCmp('hrPoolStatus').getValue();
		release_date = Ext.getCmp('release_date').getValue();

		for(i=0; i < store.data.length; i++)
		{
			var user = store.getAt(i);
			empIds += (empIds != '')?';'+user.data.id:user.data.id;
			Grade += (Grade != '')?';'+parseFloat(user.data.grades):parseFloat(user.data.grades);		
		}
		
		// btn.setDisabled(true);
		transitiongMsg = "Employee has been moved successfully.";

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Transition/empSaveTransition?v='+AM.app.globals.version,
			method: 'GET',
			params: {
				'EmployID':empIds,
				'Grade':Grade,
				'TransitionComment':transitionComment,
				'hrPoolStatus':hrPoolStatus,
				'release_date': release_date,
			},
			scope: this,
			success: function(response) 
			{
				var myObject = Ext.JSON.decode(response.responseText); 
				
				wizard.setTitle('People Transition');				
				wizard.getLayout().setActiveItem('transitionFourthPage');				
				Ext.getCmp('TransDisplayMsg').setValue(transitiongMsg);					
			}
		});	
	},
	onLeadSavePage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');		
		var store = Ext.getCmp('transLeadThirdGridID').store;		
		var empIds = '', Grade = '', transitiongMsg = "", hrPoolStatus = ""; 
		var transitionComment = "";
		
		store = Ext.getCmp('transLeadThirdGridID').store;
		transitionDate = Ext.getCmp('dateStatus').getValue();
		transitionLead = Ext.getCmp('team_lead').getValue();
		transitionComment = Ext.getCmp('transLeadComment').getValue();

		for(i=0; i < store.data.length; i++)
		{
			var user = store.getAt(i);
			empIds += (empIds != '')?';'+user.data.id:user.data.id;
			Grade += (Grade != '')?';'+parseFloat(user.data.grades):parseFloat(user.data.grades);		
		}
		
		// btn.setDisabled(true);
		transitiongMsg = "Employee Supervisior has been changed successfully.";

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Transition/empSaveLeadTransition?v='+AM.app.globals.version,
			method: 'GET',
			params: {
				'EmployID':empIds,
				'Grade':Grade,
				'transitionDate':transitionDate,
				'transitionLead':transitionLead,
				'TransitionComment':transitionComment,
			},
			scope: this,
			success: function(response) 
			{
				var myObject = Ext.JSON.decode(response.responseText); 
				
				wizard.setTitle('Supervisior Transition');				
				wizard.getLayout().setActiveItem('transitionFourthPage');				
				Ext.getCmp('TransDisplayMsg').setValue(transitiongMsg);					
			}
		});	
	},

	onPullFromHRSavePage: function(btn)
	{
		var wizard = btn.up('#wizardForm');		
		var store = Ext.getCmp('transPullHRThirdGridID').store;		
		var empIds = '', Grade = '', transitiongMsg = "", hrPoolStatus = ""; 
		var transitionComment = "";
		
		store = Ext.getCmp('transPullHRThirdGridID').store;
		transitionDate = Ext.getCmp('PullfromHR_date').getValue();
		transitionPod = Ext.getCmp('new_pod').getValue();
		transitionService = Ext.getCmp('new_service').getValue();
		transitionSupervisor = Ext.getCmp('new_team_lead').getValue();
		// assignSupervisor = Ext.getCmp('assign_team_lead').getValue();
		transitionShift = Ext.getCmp('new_shift').getValue();
		transitionComment = Ext.getCmp('transPullHRComment').getValue();

		for(i=0; i < store.data.length; i++)
		{
			var user = store.getAt(i);
			empIds += (empIds != '')?';'+user.data.id:user.data.id;
			Grade += (Grade != '')?';'+parseFloat(user.data.grades):parseFloat(user.data.grades);		
		}

		transitiongMsg = "Employee has been moved successfully.";

		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Transition/SaveFromHRPoolTransition?v='+AM.app.globals.version,
			method: 'GET',
			params: {
				'EmployID':empIds,
				'Grade':Grade,
				'transitionDate':transitionDate,
				'transitionPod':transitionPod,
				'transitionService':transitionService,
				'transitionSupervisor':transitionSupervisor,
				'transitionShift':transitionShift,
				'TransitionComment':transitionComment,
				// 'assign_team_lead':assignSupervisor,
			},
			scope: this,
			success: function(response) 
			{
				var myObject = Ext.JSON.decode(response.responseText); 
				
				wizard.setTitle('Pull from HR Pool');				
				wizard.getLayout().setActiveItem('transitionFourthPage');				
				Ext.getCmp('TransDisplayMsg').setValue(transitiongMsg);					
			}
		});
	},
});