Ext.define('AM.controller.Dashboard', {
    extend: 'Ext.app.Controller',
    // stores : ['DashboardPanelHeader','LogInViewerClients','DashboardPanelHeaderSecond','TopicManager','GroupVerticalCombo','DashboardTool'],
    stores : ['DashboardTool','LogInViewerClients','GroupWorCombo','DashboardPanelHeader','Vertical','GroupVerticalCombo','Pod'],
	views: ['dashboard.home','dashboard.noDataPanel','dashboard.flowCharts','dashboard.panelHeader'],

    init: function() {
    	var me = this;
        this.control({
        	'#panelContainer' : {
        		'render' : this.initializeDropTarget
        	},
        	'#gradeClientIDGraph, #locClientIDGraph' : {
            	beforequery : function(queryEvent) {
            		Ext.Ajax.abortAll();
            		queryEvent.combo.getStore().getProxy().extraParams = {'hasNoLimit':'1','filterName' : queryEvent.combo.displayField};
            	}
            },
        });
    },
    
	
    viewContent : function()
	{
		AM.app.globals.dashboardChartClient =  [];
        var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var dashboard = this.getView('dashboard.home').create();
        mainContent.removeAll();
		mainContent.add( dashboard );
		mainContent.doLayout();
		
		if(!Ext.util.Cookies.get('view_popup') && ( Ext.util.Cookies.get('Notifications') > 0 ))
		{
			AM.app.getController('Notification').viewPopNotify();			
			Ext.util.Cookies.set('view_popup', true);
		}
    },
    
	
    popupContents: function(obj,chartName,ClientId,PodId,PodsID,grouppod)
	{
		var storeName = 'Emp_chart_details';
    	var cloumnNames = '';
        var WorID = '';
    	var HistoryCloumnNames  ='';
        var ShiftCloumnNames = '';

        var ClientCloumnNames = '';

        var PodCloumnNames  ='';
    	var url = AM.app.globals.appPath+'index.php/charts/';
    	var HistoryUrl = AM.app.globals.appPath+'index.php/charts/';
    	var ShiftUrl = AM.app.globals.appPath+'index.php/charts/';

    	var ClientUrl = AM.app.globals.appPath+'index.php/charts/';

        var PodUrl = AM.app.globals.appPath+'index.php/charts/';

    	var title = 'Employee Details';
    	var paramObj = {};
    	var flag = false;    
    	var myreportee = {};
		paramObj["clientID"] = "";

		paramObj["PodId"] = "";

    	
		var chartNamesplit = chartName.split("&",2);	
		var chartName = chartNamesplit[0];	
		var clickedName = chartNamesplit[1];
		
		if(chartName =='Barchart' || chartName =='ShiftChart' || chartName == 'GradeChart' || chartName =='ErrorsChart' || chartName == 'LocationChart')
		{
			ClientId = (ClientId!="") ? Ext.encode(ClientId) : '';
            PodId = (PodId!="") ? Ext.encode(PodId) : '';
            WorID = (grouppod!="") ? Ext.encode(grouppod) : '';	
        }
		
		if(ClientId != 'null')
		{    						
			paramObj["clientID"] = ClientId;
		}

		if(PodId != 'null')
		{    						
			paramObj["PodId"] = PodId;
		}

		if(WorID != 'null')
        {                           
            paramObj["WorID"] = WorID;
        }
		
    	paramObj['empId'] = Ext.util.Cookies.get('empid');
    	
		if(obj.storeItem != undefined)
		{
			if (obj.storeItem.data['Name'] != 'Leadership') 
			{
				if( chartName == 'GradeChart'){
					cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable: false,menuDisabled:true},{ header:"Employee ID",dataIndex:"company_employ_id",menuDisabled:true},{ header:"Employee Name",dataIndex:"FirstName",flex:1,menuDisabled:true},{ header:"Designation",dataIndex:"DesignationName",flex:1,menuDisabled:true},{header:"Client",dataIndex:"Client_Name",flex:1,menuDisabled:true},{header:"Grade",dataIndex:"Grade",flex:1,menuDisabled:true}]
				} else{   
					cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable: false,menuDisabled:true},{ header:"Employee ID",dataIndex:"company_employ_id",menuDisabled:true},{ header:"Employee Name",dataIndex:"FirstName",flex:1,menuDisabled:true},{ header:"Designation",dataIndex:"DesignationName",flex:1,menuDisabled:true},{header:"Client",dataIndex:"Client_Name",flex:1,menuDisabled:true}]
				}
			} 
            else {cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable: false,menuDisabled:true},{ header:"Employee ID",dataIndex:"company_employ_id",menuDisabled:true},{ header:"Employee Name",dataIndex:"FirstName",flex:1,menuDisabled:true},{ header:"Designation",dataIndex:"DesignationName",flex:1,menuDisabled:true}]}
        }
		else
		{
			cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable: false,menuDisabled:true},{ header:"Employee ID",dataIndex:"company_employ_id",menuDisabled:true},{ header:"Employee Name",dataIndex:"FirstName",flex:1,menuDisabled:true},{ header:"Designation",dataIndex:"DesignationName",flex:1,menuDisabled:true}];
		}
		
		// if(chartName == 'NrrRdr')
  // 		{
  // 			flag = true;
  // 			var type = 'rdr';
  // 			if(obj.yField =='Value1')
  // 				type = 'nrr';
  // 			paramObj['type']=type;
  // 			url += 'nrrRdrDetails/';
  // 			cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50},{ header:"Date Of Request",dataIndex:"DateOfRequest",flex:1},{ header:"Client Name",dataIndex:"Client_Name",flex:1 },{ header:"Process",dataIndex:"Process_Name",flex:1},{ header:"Resource",dataIndex:"TotalResource",flex:1}];
  // 			paramObj['empId']= Ext.util.Cookies.get('empid');
  // 			paramObj['month'] = obj.value[0];
		// 	param = paramObj;
  // 		}
  	  
 	    if(chartName=='GradeChart' || chartName=='ShiftChart')
    	{
 	    	flag = true;
    		if(chartName=='GradeChart') 
			{
				paramObj['grade']			= obj.storeItem.data['Name'];
                paramObj["PodId"]       = PodId;
				paramObj['billability']		= AM.app.globals.billability;
				paramObj['grouppod']	= grouppod;
    			url += 'gradePodEmployeeDetails/';
    			param = paramObj;
    		} 
			else 
			{
    			paramObj['shift'] 			= obj.storeItem.data['shift_id'];
    			paramObj["clientID"] 		= ClientId;
				paramObj["PodId"] 		= PodId;
    			paramObj['grouppod']	= grouppod;
    			url += 'shiftEmployeeDetails/';
    			param = paramObj;
    		}
    	}
    	else if(chartName == 'ShiftClientChart')
    	{
    		flag = true;
    		url += 'shiftClientDetails/';
    		cloumnNames = [ { header:"Sl No", dataIndex:"SlNo",width:50,menuDisabled:true},{ header:"Client Name",dataIndex:"Client_Name",flex:1,menuDisabled:true },{ header:"Website",dataIndex:"Web",flex:1,menuDisabled:true},{ header:"Country",dataIndex:"Country",flex:1,menuDisabled:true}];
    		param = { 
    			empId			: Ext.util.Cookies.get('empid'),
    			shift			: obj.storeItem.data['shift_id'], 
				grouppod	: grouppod, 
			};
    		var title = 'Client Details';
    	}
    	else if(chartName =='LocationChart')
    	{
    		flag = true;			
    		paramObj["location"] = obj.storeItem.data['location_id'];
    		paramObj["grouppod"] = grouppod; 
			paramObj["PodId"] 		= PodId; 
			paramObj["clientID"] 		= ClientId;
    		url +='locationEmployeeDetails';
    		param = paramObj;
    	}
    	else if(chartName =='Barchart')
		{		
			flag = true;
			if(typeof(obj) == "string")			
			{
				paramObj["billability"] = obj;
                paramObj["PodId"]  = PodId;
				var title = obj+' Employees';
			}
			else
			{
                paramObj["PodId"]  = PodId;
				paramObj["billability"] = obj.storeItem.data['Name'];
				var title = obj.storeItem.data['Name']+' Employees';
			}
    		url +='billableEmployeeDetails?v='+AM.app.globals.version;
    		param = paramObj;
		}
    	else if(chartName =='LocationClientChart')
    	{
    		//console.log(obj.storeItem);
            //paramObj["location"] = obj.storeItem.data['Name'];
    		flag = true;
    		url += 'locationClientDetails/';
    		cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable:false,menuDisabled:true },{ header:"Client Name",dataIndex:"Client_Name",flex:1,menuDisabled:true},{ header:"Website",dataIndex:"Web",flex:1,menuDisabled:true},{ header:"Country",dataIndex:"Country",flex:1,menuDisabled:true}];
    		param = { empId : Ext.util.Cookies.get('empid'),location: obj.storeItem.data['Name'],grouppod:grouppod };
    		var title = 'Client Details';
    	}
    	else if(chartName =='ErrorsChart')
    	{
    		flag = true;
    		url += 'ErrorEscAprDetails/';
    		
			cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50},{ header:"Date",dataIndex:"Date"},{ header:"Process Name",dataIndex:"Process_Name",flex:1},{ header:"Employee Name",dataIndex:"Employ_Name",flex:1.5},{ header:"Severity",dataIndex:"Severity",flex:1}];
 
			param = { empId : Ext.util.Cookies.get('empid'),logType: 'Error',ProcessID: obj.data['ProcessID'],ClientID: obj.data['ClientID'] };
    		var title = "Error Details";
    	}
    	else if(chartName =='EscalationsChart')
    	{
    		flag = true;
    		url += 'ErrorEscAprDetails/';
    		
    		cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50},{ header:"Date",dataIndex:"Date"},{ header:"Process Name",dataIndex:"Process_Name",flex:1},{ header:"Employee Name",dataIndex:"Employ_Name",flex:1.5}];

			param = { empId : Ext.util.Cookies.get('empid'),logType: 'Escalation',ProcessID: obj.data['ProcessID'],ClientID: obj.data['ClientID'] };
    		var title = "Escalation Details";
    	}
    	else if(chartName =='AppreciationChart')
    	{
    		flag = true;
    		url += 'ErrorEscAprDetails/';
    		
    		cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50},{ header:"Date",dataIndex:"Date"},{ header:"Process Name",dataIndex:"Process_Name",flex:1},{ header:"Employee Name",dataIndex:"Employ_Name",flex:1.5}];
    		
			param = { empId : Ext.util.Cookies.get('empid'),logType: 'Appreciation',ProcessID: obj.data['ProcessID'],ClientID: obj.data['ClientID'] };
    		var title = "Appreciation Details";
    	}
		else if(chartName == 'ClientWiseEmpChart')
		{
			flag = true;
			url += 'getClientWiseEmployees/';
    		cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50},{ header:"Employee ID",dataIndex:"company_employ_id",flex:1},{ header:"Employee",dataIndex:"Employ_Name",flex:1},{ header:"Designation",dataIndex:"DesignationName",flex:1},{ header:"Process",dataIndex:"Process_Name",flex:1},{ header:"Pod",dataIndex:"Pod_Name",flex:1}];
			param = {ClientID: obj.data.ClientID, logType: 'ClientWiseEmpChart'};
    		var title = "Clientwise Billable Employees -  ``"+obj.data.Name+"``";
		}
		else if(chartName == 'PodWiseEmpChart')
		{
			flag = true;
			url += 'getPodEmployees/?v='+AM.app.globals.version;
    		cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable: false},{ header:"Employee ID",dataIndex:"company_employ_id",flex:1},{ header:"Name",dataIndex:"FirstName",flex:1},{ header:"Designation",dataIndex:"Name",flex:1},{ header:"Client",dataIndex:"Client_Name",flex:1},{ header:"Process",dataIndex:"Process_Name",flex:1}];
    		
    		if(typeof(obj) == "string")
    		{
    			var dashboardParams = obj.split("&",2);
    			var title = "HR Pool Employees - ``HR Pool``";
    			//pass carouselHrPool param to differentiate query to display HR pool details
    			param = {PodsID: dashboardParams[0],Type: clickedName,ProcessID:dashboardParams[1],carouselHrPool:1 };
    		}
    		else
    		{
    			param = {PodsID: obj.data.PodsID,Type: clickedName,ProcessID:obj.data.ProcessID};
    			var title = clickedName.replace(/[^a-zA-Z ]/g, " ")+" Employees - ``"+obj.data.Name+"``";
    		}
		}
		else if(chartName == 'PodHierarchy')
		{
			flag = true;
			url += 'getVerticalEmployees/?v='+AM.app.globals.version;
    		cloumnNames = [{ header:"Sl No",dataIndex:"SlNo",width:50,sortable: false},{ header:"Employee ID",dataIndex:"company_employ_id",flex:1},{ header:"Name",dataIndex:"FirstName",flex:1},{ header:"Designation",dataIndex:"Name",flex:1},{ header:"Client",dataIndex:"Client_Name",flex:1}];
    		
    		if(typeof(obj) == "string")
    		{
    			
    			var dashboardParams = obj.split("&",2);
    			var title = "HR Pool Employees - ``HR Pool``";
    			//pass carouselHrPool param to differentiate query to display HR pool details
    			param = {PodsID: dashboardParams[0],Type: clickedName,ProcessID:dashboardParams[1],carouselHrPool:1,chart:'vHierar',processname:obj.data.Name};
    		}
    		else
    		{
    			if(obj.data.PodsID)
				{
					var podid=obj.data.PodsID;
    			}
				else
				{
					var podid=obj.data.PodID;
    			}
           
    			param = {PodsID: podid,Type: clickedName,ProcessID:obj.data.ProcessID,chart:'vHierar',processname:obj.data.Name};
    			var title = clickedName.replace(/[^a-zA-Z ]/g, " ")+" Employees - ``"+obj.data.Name+"``";
    		}
		}
		else if(chartName == 'EmployeeDetails')
		{
			flag = true;
			url += 'employDetailsPopup/';
    		cloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1},{ header:"Name",dataIndex:"Employ_Name",flex:1},{ header:"Designation",dataIndex:"DesignationName",flex:1},{ header:"Client",dataIndex:"Client_Name",flex:1},{ header:"Billable",dataIndex:"Billable",flex:1}, { header:"Reports To",dataIndex:"PrimaryLead",flex:1}];
			param = {EmployID: obj.data.EmployID};
    		var title = "Employee Details";
		}
		else if(chartName == 'BillableEmployeeHistory')
		{
			flag = true;
			url += 'BillableHistoryDetails/';
    		cloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"WOR ID",dataIndex:"wor_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Name",dataIndex:"Employ_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Client",dataIndex:"Client_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Role Code",dataIndex:"project_code",flex:1.3, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Billable Type",dataIndex:"billability_type",flex:1.3, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Start Date",dataIndex:"start_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"End Date",dataIndex:"end_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1}];
			param = {EmployID: obj};
			var title = "Employee History";

    		HistoryUrl += 'EmpHistoryDetails/';
    		HistoryCloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Name",dataIndex:"Employ_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false}, { header:"Previous Supervisor",dataIndex:"from_reporting", sortable: false, flex:2,menuDisabled:true,groupable: false,draggable: false}, { header:"New Supervisor",dataIndex:"to_reporting", sortable: false, flex:2,menuDisabled:true,groupable: false,draggable: false},{ header:"Comments",dataIndex:"comment", sortable: false, flex:2,menuDisabled:true,groupable: false,draggable: false},{ header:"Transition Date",dataIndex:"transition_date", sortable: false, flex:2,menuDisabled:true,groupable: false,draggable: false, flex:1}];
			param = {EmployID: obj};

            PodUrl += 'EmpVerticalDetails/';
            PodCloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Name",dataIndex:"Employ_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"POD",dataIndex:"Pod",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Start Date",dataIndex:"start_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"End Date",dataIndex:"end_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"Comment",dataIndex:"comment", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1}];
            param = {EmployID: obj};

    		ShiftUrl += 'EmpShiftDetails/';
    		// ShiftCloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Name",dataIndex:"Employ_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Client",dataIndex:"Client_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Shift",dataIndex:"Shift_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Start Date",dataIndex:"start_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"End Date",dataIndex:"end_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"Comment",dataIndex:"comment", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1}];
            ShiftCloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Name",dataIndex:"Employ_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Shift",dataIndex:"Shift_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Start Date",dataIndex:"start_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"End Date",dataIndex:"end_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"Comment",dataIndex:"comment", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1}];
			param = {EmployID: obj};
			
			ClientUrl += 'EmpClientDetails/';
    		ClientCloumnNames = [{ header:"Employee ID",dataIndex:"company_employ_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Name",dataIndex:"Employ_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Client",dataIndex:"Client_Name",flex:2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Work Order ID",dataIndex:"wor_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Role Code",dataIndex:"process_id",flex:1.2, sortable: false,menuDisabled:true,groupable: false,draggable: false},{ header:"Assigned Date",dataIndex:"start_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1},{ header:"Removed Date",dataIndex:"end_date", sortable: false,menuDisabled:true,groupable: false,draggable: false, flex:1}];
			param = {EmployID: obj};	
		}
		else if(chartName == 'ReporteesBillable')
		{
			myreportee   = {flag : true, group_var : 'Client_Name',grades : obj.data.grades };
			url += 'MyReporteeBillableDetials/';
			
    		cloumnNames = [{header:"Client",flex:0.7,sortable:false,summaryRenderer: function(value, summaryData, dataIndex) {
                return "<font color='black'><b>Total</b><font>"; 
            }},{ header:"Process",sortable:false,flex:0.8,dataIndex:"PROCESS"},
				               {header:"Billable",dataIndex:"Billable",flex:0.5,sortable:false,				            
				            		summaryType: 'sum',
				            		summaryRenderer: function(value, summaryData, dataIndex) {
				                return "<font color='black'><b>"+value+"</b><font>"; 
				            },
				            		
				            	}];
			param = {EmployID: obj.data.EmployID,grades:obj.data.grades,BillableDrill:1,Range:obj.data.Range};
    		var title = "My Reportee Billable(%)";
		}
		else if(chartName == 'ReporteesBuffer')
		{
			myreportee   = {flag : true, group_var : 'Client_Name',grades : obj.data.grades };
			url += 'MyReporteeBuffer/';
			
    		cloumnNames = [{ header:"Client", flex:0.7, sortable:false, 
					summaryRenderer: function(value, summaryData, dataIndex) {
						return "<font color='black'><b>Total</b><font>"; 
					}
				},
				{ header:"Process",sortable:false,flex:0.8,dataIndex:"PROCESS"},
				{ header:"Buffer",dataIndex:"Buffer",flex:0.5,sortable:false, summaryType: 'sum', 
					summaryRenderer: function(value, summaryData, dataIndex) {
				        return "<font color='black'><b>"+value+"</b><font>"; 
				    },
				}];
			param = { EmployID: obj.data.EmployID,grades:obj.data.grades,BillableDrill:1,Range:obj.data.Range};
    		var title = "My Reportee Buffer(%)";
		}
	
   
 	    if(flag)
 	    {

			this.DashboardDetails(chartName,cloumnNames,url,title,param,HistoryUrl,HistoryCloumnNames,ShiftUrl,ShiftCloumnNames,PodUrl,PodCloumnNames,ClientUrl,ClientCloumnNames);

 	    }
		if(myreportee.flag)
	    {
			if(myreportee.grades >= 3)
				this.DashboardMyReporteeDetails(chartName,cloumnNames,url,title,param,myreportee.group_var);
	    }
    },

    resetShiftAllFilter : function(){
         Ext.getCmp('shiftpodId').setValue('');

        Ext.getStore('Pod').load({
            params: {
                //'filter' : '[{"property":"first_name,last_name","value":""}]',
                'Pod': '',
            },
            callback: function (records, options, success) {
                if (success) {
                    // alert('djfdkfn');
                }
            }
        });
    },

    resetnrrRdrAllFilter : function(){
         Ext.getCmp('resbillablepodid').setValue('');

        Ext.getStore('Pod').load({
            params: {
                //'filter' : '[{"property":"first_name,last_name","value":""}]',
                'Pod': '',
            },
            callback: function (records, options, success) {
                if (success) {
                    // alert('djfdkfn');
                }
            }
        });
    },
    
    resetLocationAllFilter : function(){
         Ext.getCmp('locPodId').setValue('');

        Ext.getStore('Pod').load({
            params: {
                //'filter' : '[{"property":"first_name,last_name","value":""}]',
                'Pod': '',
            },
            callback: function (records, options, success) {
                if (success) {
                    // alert('djfdkfn');
                }
            }
        });
    },
    resetGradeAllFilter : function(){
         Ext.getCmp('PodId').setValue('');

        Ext.getStore('Pod').load({
            params: {
                //'filter' : '[{"property":"first_name,last_name","value":""}]',
                'Pod': '',
            },
            callback: function (records, options, success) {
                if (success) {
                    // alert('djfdkfn');
                }
            }
        });
    },
    resetLocationClientAllFilter : function(){
         Ext.getCmp('locClientPodId').setValue('');

        Ext.getStore('Pod').load({
            params: {
                //'filter' : '[{"property":"first_name,last_name","value":""}]',
                'Pod': '',
            },
            callback: function (records, options, success) {
                if (success) {
                    // alert('djfdkfn');
                }
            }
        });
    },


    DashboardMyReporteeDetails:function(chartName,cloumnNames,url,title,param,grpvar)
	{
		var my_store = Ext.create('Ext.data.Store', {
			fields: ['Client_Name', 'Name', 'Value1', 'ProcessID', {name:'Billable', type: 'float'},'EmployID','grades','PROCESS','ReporteeName', {name:'Buffer',type: 'float'}, 'EmployName', 'ProcessName'],
			autoLoad: true,
			remoteSort: true,
			pageSize: AM.app.globals.itemsPerPage,
			groupers: [{
				property : grpvar,
				direction: 'ASC'
			}],
			proxy: {
				type: 'rest',
				url : url,
				extraParams:param,
				reader : {
					type: 'json',
					root : 'rows',
					successProperty : 'success',
					totalProperty: 'totalCount'
				}
			}
		});
	    
		var _pagingToolbar = new Ext.PagingToolbar({
			displayInfo: true,
			cls: "peopleTool",
            prependButtons: false,
            displayMsg: "Total {2} Records",
            emptyMsg: "No Items to display",
			buttons : [{
				text : 'Cancel',
				icon : AM.app.globals.uiPath+'resources/images/cancel.png',
				handler : function(pagingtool){
					pagingtool.up().up().up().close()
				}
			}],
			emptyMsg: "No Data To Display",
			plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
			store   : my_store
		});
		
		var w = '75%';
	
		var _topToolbar ="";
		
		var my_grid = new Ext.window.Window({
			title : title,
			centered: true,
			constrain: true,
			layout: 'fit',
			width : w,
			autoShow : true,
			autoSave: false,
			autoHeight : true,
			modal: true,
			y : 100,
			requires: [ "Ext.grid.feature.Grouping"],    
			items:[{
				xtype:'grid',
				id:'myReproteeBillGrid',
				width : '99%',
				height : 440,
				border : true,
				loadMask: true,
				columns:cloumnNames,
				store: my_store,
				tbar:_topToolbar,	
				bbar : _pagingToolbar,
				cls: "custom-grid",
				features: [{
					ftype: "groupingsummary",
					groupHeaderTpl: "{name}",
					collapsible : true,
			 
				}],
			}]
		}); 
	
		my_grid.show();
	},
	

	DashboardDetails:function(chartName,cloumnNames,url,title,param,HistoryUrl,HistoryCloumnNames,ShiftUrl,ShiftCloumnNames,PodUrl,PodCloumnNames,ClientUrl,ClientCloumnNames)

	{
		var my_store = Ext.create('Ext.data.Store', {
			fields: ['SlNo','company_employ_id', 'FirstName', 'DesignationName','Client_Name','Web','Country','Count','Date', 'Process_Name', 'Employ_Name', 'Severity','TotalResource', 'DateOfRequest', 'Name', 'Value1', 'Pod_Name', 'ProcessID', 'Billable', 'PrimaryLead', 'From_Pod', 'To_Pod', 'from_reporting', 'to_reporting', 'transition_date', 'billability_type', 'billability_percentage', 'start_date', 'end_date','Billable','ClientName','project_code','wor_id','comment','Grade'],
			autoLoad: true,
			remoteSort: true,
			pageSize: AM.app.globals.itemsPerPage,
			proxy: {
				type: 'rest',
				url : url,
				extraParams:param,
				reader :  {
					type: 'json',
					root : 'rows',
					successProperty : 'success',
					totalProperty: 'totalCount'
				}
			}
		});

		var _pagingToolbar = new Ext.PagingToolbar({
			displayInfo: true,
			pageSize: AM.app.globals.itemsPerPage,
			displayMsg: 'Displaying {0} - {1} of {2}',
			emptyMsg: "No Data To Display",
			plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')],
			store   : my_store
		});
		
		var w = '60%';

		var currentTime = new Date();
		var now = currentTime.getFullYear();
		var nowMonth = currentTime.getMonth();
		var years = new Array();
		var i = 1;
		maxVal = 10;
		
		j = 2;
		for(y= now+maxVal;;y--)
		{
			temp = ""+y;
			years.push([temp,temp]);
		
			if(j > maxVal)
			{
				break;
			}
			j++;
		}
		
		for(y = now;;y--)
		{
			temp = ""+y;
			years.push([temp,temp]);
			
			if(i > maxVal)
			{
				break;
			}
			i++;
		}
	
		var storeThn = new Ext.data.SimpleStore({
			fields: ['Year','abbr'],        
			data: years
		});
	
		var _topToolbar ="";
		
		if(chartName == 'ErrorsChart' || chartName == 'EscalationsChart' || chartName == 'AppreciationChart')
		{
			_topToolbar = new Ext.toolbar.Toolbar({
				items: [{
					xtype: 'boxselect',
					name: 'Month',
					itemId:'moncombodshboard',
					multiSelect: false,
					fieldLabel: 'Month',
					selectOnFocus: true,
					labelWidth : 50,
					width : 200,
					style: {
						'margin-right' : '5px'
					},
					allowBlank: false,
					emptyText: 'Select Month',
					store: new Ext.data.ArrayStore({
						fields: ['Month','abbr'],
						data  : [
							[1,'January'], 
							[2,'Febraury'], 
							[3,'March'], 
							[4,'April'],
							[5,'May'],
							[6,'June'],
							[7,'July'],
							[8,'August'],
							[9,'September'],
							[10,'October'],
							[11,'November'],
							[12,'December'],
						]
					}),
					displayField: 'abbr',
					valueField: 'Month',
					value:nowMonth+1,
					forceSelection: true,
					listeners: {											
						'select': function(combo) {											
							param.Month = combo.getValue();
							combo.up('grid').store.load({extraParams:param,params:{'Month':combo.getValue()}});
						}
					}
				}, { 
					xtype : 'tbseparator',
					style : {
						'margin-right' : '10px'
					}
				}]
			});
		}
		
		//Club Employee Billable and Employee Transition in TapPanel
		if(chartName == 'BillableEmployeeHistory')
		{
			_pagingToolbar = '';
			w = '80%';

			var HistoryStore = Ext.create('Ext.data.Store', {
				fields: ['company_employ_id', 'Employ_Name', 'work_order_id', 'From_Pod','To_Pod','from_reporting','to_reporting','transition_date','From_client', 'To_client','comment'],
				autoLoad: true,
				remoteSort: true,
				pageSize: AM.app.globals.itemsPerPage,
				proxy: {
					type: 'rest',
					url : HistoryUrl,
					extraParams:param,
					reader :  {
						type: 'json',
						root : 'rows',
						successProperty : 'success',
						totalProperty: 'totalCount'
					}
				}
			});
			
			var ShiftStore = Ext.create('Ext.data.Store', {
				fields: ["company_employ_id","Employ_Name","Shift_Name","start_date","end_date","comment"],
				autoLoad: true,
				remoteSort: true,
				pageSize: AM.app.globals.itemsPerPage,
				proxy: {
					type: 'rest',
					url : ShiftUrl,
					extraParams:param,
					reader :  {
						type: 'json',
						root : 'rows',
						successProperty : 'success',
						totalProperty: 'totalCount'
					}
				}
			});
			
			var ClientStore = Ext.create('Ext.data.Store', {
				fields: ["company_employ_id","Employ_Name","Client_Name","wor_id","process_id","start_date","end_date"],
				autoLoad: true,
				remoteSort: true,
				pageSize: AM.app.globals.itemsPerPage,
				proxy: {
					type: 'rest',
					url : ClientUrl,
					extraParams:param,
					reader :  {
						type: 'json',
						root : 'rows',
						successProperty : 'success',
						totalProperty: 'totalCount'
					}
				}
			});

            var PodStore = Ext.create('Ext.data.Store', {
                fields: ["company_employ_id","Employ_Name","Pod","start_date","end_date","comment"],
                autoLoad: true,
                remoteSort: true,
                pageSize: AM.app.globals.itemsPerPage,
                proxy: {
                    type: 'rest',
                    url : PodUrl,
                    extraParams:param,
                    reader :  {
                        type: 'json',
                        root : 'rows',
                        successProperty : 'success',
                        totalProperty: 'totalCount'
                    }
                }
            });

			var my_grid = new Ext.window.Window({
				title : title,
				centered: true,
				constrain: true,
				layout: 'fit',
				width : w,
				autoShow : true,
				autoSave: false,
				autoHeight : true,
				modal: true,
				y : 100,
				items: [{                
					xtype:'tabpanel',
					activeTab: 0,
					deferredRender: false,
					layoutOnTabChange:true,
					defaults:{autoHeight:true},
					border:false,
					items: [{
						title: 'Billable History',
						xtype:'grid',
						id:'myGrid',
						width : '99%',
						height : 400,
						border : true,
						loadMask: true,
						columns:cloumnNames,
						store: my_store,
						tbar:_topToolbar,	
						bbar : _pagingToolbar
					}, {
						title: 'Supervisor History',
						xtype:'grid',
						id:'myGrid2',
						width : '99%',
						height : 400,
						border : true,
						loadMask: true,
						columns:HistoryCloumnNames,
						store: HistoryStore,
						tbar:_topToolbar,	
						bbar : _pagingToolbar
					}, {
						title: 'Shift History',
						xtype:'grid',
						id:'myGrid3',
						width : '99%',
						height : 400,
						border : true,
						loadMask: true,
						columns:ShiftCloumnNames,
						store: ShiftStore,
						tbar:_topToolbar,	
						bbar : _pagingToolbar

					},{
						title: 'Client History',
						xtype:'grid',
						id:'myGrid4',
						width : '99%',
						height : 400,
						border : true,
						loadMask: true,
						columns:ClientCloumnNames,
						store: ClientStore,
						tbar:_topToolbar,	
						bbar : _pagingToolbar
					
					}, {
                        title: 'POD History',
                        xtype:'grid',
                        id:'myGrid5',
                        width : '99%',
                        height : 400,
                        border : true,
                        loadMask: true,
                        columns:PodCloumnNames,
                        store: PodStore,
                        tbar:_topToolbar,   
                        bbar : _pagingToolbar

				}]
			}]
			});
		}
		else
		{
			var my_grid = new Ext.window.Window({
				title : title,
				centered: true,
				constrain: true,
				layout: 'fit',
				width : w,
				autoShow : true,
				autoSave: false,
				autoHeight : true,
				modal: true,
				y : 100,
				items:[{
					xtype:'grid',
					id:'myGrid',
					width : '99%',
					height : 449,
					border : true,
					loadMask: true,
					columns:cloumnNames,
					store: my_store,
					tbar:_topToolbar,	
					bbar : _pagingToolbar
				}]
			}); 
		}
		my_grid.show();
	},
	
    initializeDropTarget : function(targetPanel) 
	{
		if(AM.app.globals.UserDashboard == '')
		{
			var employDashboardView = {
				'panelId1': {'view_name': 'dashboard.barPanel', 'title': 'Billable & Non Billable Resource'}, 
				'panelId2': {'view_name': 'dashboard.locationPanel', 'title': 'Location wise Employees'}, 
				'panelId3': {'view_name': 'dashboard.shiftPanel', 'title': 'Shift wise Employees'}, 
				'panelId4': {'view_name': 'dashboard.gradePanel', 'title': 'Grade wise Employees'}, 
				'panelId5': {'view_name': 'dashboard.locationClientPanel', 'title': 'Location wise Clients'}, 
				'panelId6': {'view_name': 'dashboard.treePanel', 'title': 'Employee Hierarchy'}
			};
		}
		else
		{
			var employDashboardView = Ext.decode(AM.app.globals.UserDashboard);
		}
    	
	    var gridView = targetPanel,
	        grid = targetPanel;
	    	grid.dropZone = Ext.create('Ext.dd.DropZone', targetPanel.up('panel').el, {
	    	ddGroup: 'myDDGroup',
    		isTarget  : true,
    		containerScroll : true,
	        getTargetFromEvent: function(e) {
	            return e.getTarget('.subPanels');
	        },

	        onNodeEnter : function(target, dd, e, data)
			{
	        	var containerId = '';
	        	if((target.id == 'panelId1') || (target.id == 'panelId2') || (target.id == 'panelId3')) 
				{
	        		containerId = 'cot1';
	        	}
	        	else 
				{
	        		containerId = 'cot2';
	        	}
	        	targetPanel.down('#'+containerId).down('#'+target.id).getEl().mask("Loading", 'x-mask-loading');
	        	targetPanel.down('#'+containerId).down('#'+target.id).addCls('x-drop-target-active');
				return this.callParent(arguments);
	        },

	        onNodeOut : function(target, dd, e, data)
			{
				var containerId = '';
	        	if((target.id == 'panelId1') || (target.id == 'panelId2') || (target.id == 'panelId3')) 
				{
	        		containerId = 'cot1';
	        	}
	        	else 
				{
	        		containerId = 'cot2';
	        	}
	        	targetPanel.down('#'+containerId).down('#'+target.id).getEl().unmask();
	        	targetPanel.down('#'+containerId).down('#'+target.id).removeCls('x-drop-target-active');
				return this.callParent(arguments);
	        },

	        onNodeOver : function(target, dd, e, data)
			{
	            return Ext.dd.DropZone.prototype.dropAllowed;
	        },

	        onNodeDrop : function(target, dd, e, data)
			{
				Ext.require('AM.view.'+data.records[0].data.view_name);
				
				var My_store = Ext.create('AM.store.'+data.records[0].data.store_name);

		    	My_store.load({callback:function(records, options, success) {
					var my_records_count = 0;  
					
					var containerId = '';
					if(target.id != null) 
					{
						if((target.id == 'panelId1') || (target.id == 'panelId2') || (target.id == 'panelId3')) 
						{
							containerId = 'cot1';
						}
						else 
						{
							containerId = 'cot2';
						}
						targetPanel.down('#'+containerId).down('#'+target.id).removeAll();

						if(!records) 
						{
							var viewPanel = AM.app.getView(data.records[0].data.view_name).create();
							viewPanel.id = data.records[0].data.idVal+'_'+target.id;
						}
						else 
						{
							if(parseInt(records.length) > 0) 
							{
								
								var viewPanel = AM.app.getView(data.records[0].data.view_name).create();
								viewPanel.id = data.records[0].data.idVal+'_'+target.id;
								
							}
							else 
							{
								var viewPanel = AM.app.getView('dashboard.noDataPanel').create();
								if(data.records[0].data.store_name == 'EmployeeDetails' || data.records[0].data.store_name == 'EmployeeHRPool')
									var viewPanel = AM.app.getView(data.records[0].data.view_name).create();
							}
						}

						targetPanel.down('#'+containerId).down('#'+target.id).setTitle('<div style="text-align:center;">'+data.records[0].data.title+'</div>');
						targetPanel.down('#'+containerId).down('#'+target.id).add(viewPanel);
						
						var i=1;
						for(i=1;i<=6;i++)
						{
							if(i<4)
								targetPanel.down('#cot1').down('#panelId'+i).getEl().unmask();
							else
								targetPanel.down('#cot2').down('#panelId'+i).getEl().unmask();
						}

						targetPanel.down('#'+containerId).down('#'+target.id).getEl().unmask();
						
						switch(target.id)
						{
							case 'panelId1': 
								employDashboardView.panelId1.view_name = data.records[0].data.view_name;
								employDashboardView.panelId1.title = data.records[0].data.title;
								break;
							case 'panelId2': 
								employDashboardView.panelId2.view_name = data.records[0].data.view_name;
								employDashboardView.panelId2.title = data.records[0].data.title;
								break;
							case 'panelId3': 
								employDashboardView.panelId3.view_name = data.records[0].data.view_name; 
								employDashboardView.panelId3.title = data.records[0].data.title;
								break;
							case 'panelId4': 
								employDashboardView.panelId4.view_name = data.records[0].data.view_name; 
								employDashboardView.panelId4.title = data.records[0].data.title;
								break;
							case 'panelId5': 
								employDashboardView.panelId5.view_name = data.records[0].data.view_name; 
								employDashboardView.panelId5.title = data.records[0].data.title;
								break;
							case 'panelId6': 
								employDashboardView.panelId6.view_name = data.records[0].data.view_name;
								employDashboardView.panelId6.title = data.records[0].data.title;
								break;
						}

						Ext.Ajax.request({
							url: AM.app.globals.appPath+'index.php/employees/userDashboard/',
							method: 'POST',
							params: {userDashboard: Ext.encode(employDashboardView)}
						});
						AM.app.globals.UserDashboard = Ext.encode(employDashboardView);
					}
				}});

				return true;
	        }
	    });
    },
	
	
	downloadCSV: function()
	{
		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
			myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/Charts/downloadCsvTemplate/',
			method: 'GET',
			// params: myStore.getProxy().extraParams,
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);
				if(jsonResp.success)
				{	
					myMask.hide();
					window.location = AM.app.globals.appPath+'download/ClientResorces.csv';
				}
			}
		});
	},

	/*For Dashboard High & Low Utilization  */
    High_Low_Utilization:function(obj,chartName,ProcessId)
	{
    	var url = AM.app.globals.appPath+'index.php/charts/';
    	var title = obj;
    	var paramObj = {};

    	var myreportee = {};

		paramObj["processID"] = "";

		var chartNamesplit = chartName.split("&",2);
		var chartName = chartNamesplit[0];
		var clickedName = chartNamesplit[1];
		
    	if(  ProcessId!='null')
	 	{
			paramObj["processID"] = ProcessId;
		} 

		title= obj+" :   "+chartName
		url +='HighLowUtilization';
		param = paramObj;
		this.HighLowUtilizationDetails(chartName,url,title,param);
    },
	
	
	HighLowUtilizationDetails:function(chartName,url,title,param)
	{
		var chartDataStore = Ext.create('Ext.data.Store', {
			fields: [{ name: "year", type: "string" },  { name: "value1", type: "integer" },'processName'],
			autoLoad: true,
			remoteSort: true,
			proxy: {
				type: 'rest',
				url : url,
				extraParams:param,
				reader :  {
					type: 'json',
					root : 'rows',
					successProperty : 'success',
					totalProperty: 'totalCount'
				}
			}
		});
		var newst='';
		chartDataStore.load(function(){
			newst = this.getAt(0).get('processName');
		});
	  	var chart = Ext.create('Ext.chart.Chart', {  
		    style: 'background:#fff',  
		    animate: true,  
		    shadow: true,  
		    store: chartDataStore,  
		    axes: [{
                type: "Numeric",
                minimum: 0,
                position: "left",
                fields: ["value1"],
                title: "Utilization",
                minorTickSteps: 1,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: "#ddd",
                        stroke: "#bbb",
                        "stroke-width": 0.5
                    }
                }
            }, {
                type: "Category",
                position: "bottom",
                fields: ["year"],
                title: "Month"
            }],
            series: [{
                type: "line",
                highlight: {
                    size: 7,
                    radius: 7
                },
                axis: "left",
                smooth: true,
                xField: "year",
                yField: "value1",
                title: "USA",
                markerConfig: {
                    type: "circle",
                    size: 4,
                    radius: 4,
                    "stroke-width": 0
                },
                tips: {
    				trackMouse: true,
    				width: 80,
    				height: 40,
    				align:"center",
    				renderer: function(storeItem, item) {
						this.setTitle('<div style="text-align:center;font-size:24px;">'+item.value[1]+'</div>');
    				}
    			}
            }]  
		});  

		var win = Ext.create('Ext.window.Window', {  
		    width: 600,  
		    height: 400,  
		    minHeight: 350,  
		    minWidth: 550,  
		    hidden: false,  
		    maximizable: true,  
		    title:title,  
		    autoShow: true,  
		    layout: 'fit',  
		    items: chart ,
		    modal:true
		});
	},
	
	
	settingIcon: function()
	{	
		var iconItems = [{
			xtype: 'checkboxgroup',
			columns: 1,
			pod: true,
			fieldDefaults: {
				labelWidth: 245,
				'height': 50,
				labelSeparator : "",
				labelStyle : 'font-size:13px;',
				
			},
			id  : 'SettingIconGroup',
			cls:'reportsgroupCheckbox',
			items: [{
				//boxLabel: 'Utilization',
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px">Billable & Non Billable Resource </span>',
				name: 'BillableNonBillable',
				inputValue: '1',
				id:'billNonBill',
				checked: true,
				beforeLabelTextTpl: '<img title="Billable & Non Billable Resource" width="30px" src="'+AM.app.globals.uiPath+'resources/images/dashboard/billable_nonbillable.png"/> ',
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Client wise Billable Resource</span>',
				name: 'Clientwisebillableresource',
				id:'ClientBillableResourcePanel',
				checked: true,
				beforeLabelTextTpl: '<img title="Client wise Billable Resource" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/ClientResources.png"/>',
				inputValue: '1'
			},/*{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Vertical Hierarchy</span>',
				name: 'VerticalHierarchy',
				id:'vertical_hierarchy',
				checked: true,
				beforeLabelTextTpl: '<img title="Vertical Hierarchy" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/verticalhierarchy.png"/>',
				inputValue: '1'
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">My Reportees</span>',
				name: 'MyReportees',
				id:'dashboardRepBillable',
				checked: true,
				beforeLabelTextTpl: '<img title="My Reportees" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/rep_buffer_per.png"/>',
				inputValue: '1'
			},*/{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Employee Details</span>',
				name: 'EmployDetails',
				id:'EmployeeDetails',
				checked: true,
				beforeLabelTextTpl: '<img title="Employee Details" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/employee_Details.png"/>',
				inputValue: '1'
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Location wise Employees</span>',
				name: 'locEmployees',
				id:'location',
				checked: true,
				beforeLabelTextTpl: '<img title="Locationwise Employees" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/locationwise_employees.png"/>',
				inputValue: '1'
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Shift wise Employees</span>',
				name: 'shiftEmployees',
				id:'shift',
				checked: true,
				beforeLabelTextTpl: '<img title="Shiftwise Employees" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/shiftwise_employees.png"/>',
				inputValue: '1'
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Grade wise Employees</span>',
				name: 'gradeEmployees',
				id:'grade',
				checked: true,
				beforeLabelTextTpl: '<img title="Gradewise Employees" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/gradebased_employees.png"/>',
				inputValue: '1'
			},/*{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">How Long In HR Pool</span>',
				name: 'hrpool',
				id:'EmployeeHRPool',
				checked: true,
				beforeLabelTextTpl: '<img title="How Long In HR Pool" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/hrpool.png"/>',
				inputValue: '1'
			},*/{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Employee Hierarchy</span>',
				name: 'empHier',
				id:'hierarchy',
				checked: true,
				beforeLabelTextTpl: '<img title="Employee Hierarchy" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/hierarchy-emp.png"/>',
				inputValue: '1'
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Location wise Clients</span>',
				name: 'locClients',
				id:'locationClient',
				checked: true,
				beforeLabelTextTpl: '<img title="Shiftwise Clients" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/locationwise_client.png"/>',
				inputValue: '1'
			},{
                fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Incident Log</span>',
                name: 'errorEmp',
                id:'errors',
                checked: true,
                beforeLabelTextTpl: '<img title="Error" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/error.png"/>',
                inputValue: '1'
            },
            /*{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Shiftwise Clients</span>',
				name: 'shiftClients',
				id:'shiftClient',
				checked: true,
				beforeLabelTextTpl: '<img title="Shiftwise Clients" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/shiftwise_client.png"/>',
				inputValue: '1'
			},*/
   //          {
			// 	fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Work Order Request</span>',
			// 	name: 'NRRRDR',
			// 	id:'nrrRdr',
			// 	checked: true,
			// 	beforeLabelTextTpl: '<img title="Work Order Request" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/work_order_request.png"/>',
			// 	inputValue: '1'
			// },
   //          {
   //              fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Change Order Request</span>',
   //              name: 'COR',
   //              id:'cor',
   //              checked: true,
   //              beforeLabelTextTpl: '<img title="Change Order Request" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/change_order_request.png"/>',
   //              inputValue: '1'
   //          },
            /*{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Meeting Miniutes</span>',
				name: 'meetMin',
				id:'dashboardMeetMin',
				checked: true,
				beforeLabelTextTpl: '<img title="Meeting Miniutes" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/meetingTime.png"/>',
				inputValue: '1'
			},*/
            /*{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Utilization</span>',
				name: 'Utilizationeemp',
				id:'utilization',
				checked: true,
				beforeLabelTextTpl: '<img title="Utilization" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/utilisation.png"/>',
				inputValue: '1'
			},{
				fieldLabel: '<span style="position:relative; top:-8px; left: 7px;">Accuracy</span>',
				name: 'Accuracyemp',
				id:'accuracy',
				checked: true,
				beforeLabelTextTpl: '<img title="Accuracy" width="30px"src="'+AM.app.globals.uiPath+'resources/images/dashboard/accuracy.png"/>',
				inputValue: '1',
			}*/]
		}]

        // Ext.create('Ext.Button', {
        //     renderTo: Ext.getBody(),
        //     text: 'Check all',
        //     handler: function () {
        //         var checkboxes = Ext.getCmp('SettingIconGroup').query('[isCheckbox]');
        //         Ext.Array.each(checkboxes, function (checkbox) {
        //             checkbox.setValue(1);
        //         });
        //     }
        // });
		
		var settingwin = Ext.create('Ext.window.Window', {  
		    width: 340,  
		    height: 430,
		    hidden: false, 
		    resizable: false, 
		    title:'Settings',  
		    autoShow: true,  
		    layout: 'fit',  
		    items: iconItems ,
		    modal:true,
		    id:'settingIconsWin',
		    overflowY:'scroll',
		    closable:true,
		    closeAction:'destroy',
		    scope:this,		 
		    buttons: [{
				text:'Close',
				icon : AM.app.globals.uiPath+'resources/images/cancel.png',
				listeners:{
					click: function() {
						Ext.getCmp('settingIconsWin').destroy(); 
					}
				}
			},{ 
				text: 'Apply', 
				icon : AM.app.globals.uiPath+'resources/images/save.png',
				listeners:{
					click: function() {
						AM.app.getController('AM.controller.Dashboard').saveIconControl();
					}
				}
			},{
                text: 'Check All',
                handler: function () {
                var checkboxes = Ext.getCmp('SettingIconGroup').query('[isCheckbox]');
                Ext.Array.each(checkboxes, function (checkbox) {
                    checkbox.setValue(1);
                });
            }
            }]   
		}); 
		
		if(Ext.util.Cookies.get('grade') < 3) 
		{
			Ext.getCmp('dashboardRepBillable').hide();//setDisabled(true);
		}
		
		if(AM.app.globals.DashboardToolsIcons != null)
		{
			var initialTools = JSON.parse(AM.app.globals.DashboardToolsIcons);
			var selectedSuccessValues = Ext.getCmp('SettingIconGroup').getChecked()
			
			for(var j=0;j<selectedSuccessValues.length;j++)
			{
				Ext.getCmp(selectedSuccessValues[j].id).setValue(false)
			}
			for(var k = 0;k<initialTools.Icons.length;k++)
			{
				Ext.getCmp(initialTools.Icons[k].idVal).setValue(true)
			}
			
		}
	},
	
	
	saveIconControl: function()
	{
		var filterwise = [];
		var title = '';
		var userdata = {}; 
		var loadvalues	= {};
		var objects = new Array();
		var datarec = {};
	    var selectedSuccessValues = Ext.getCmp('SettingIconGroup').getChecked();
	    var storeRecords = Ext.create('Ext.data.Store', {
			autoLoad: true,
			fields: ['thumb',"tool_tip","view_name","idVal","title","store_name"],
			data : [
				{"thumb" : "billable_nonbillable.png",  "tool_tip" : "Billable & Non Billable Resource","view_name" : "dashboard.barPanel",                     "idVal" : "billNonBill",                "title" : "Billable & Non Billable Resource",      "store_name" : "Barchart"},
				{"thumb" : "ClientResources.png",       "tool_tip" : "Client wise billable resource",   "view_name" : "dashboard.ClientBillableResourcePanel",  "idVal" : "ClientBillableResourcePanel","title" : "Client wise Billable Resource",          "store_name" : "ClientBillableResource"},
				{"thumb" : "verticalhierarchy.png",      "tool_tip" : "Pod Hierarchy",             "view_name" : "dashboard.VerticalHierarchy",            "idVal" : "pod_hierarchy",         "title" : "Pod Hierarchy",                     "store_name" : "VerticalHierarchy"},
				{"thumb" : "rep_buffer_per.png",        "tool_tip" : "My Reportees Data",               "view_name" : "dashboard.MyRepBillable",                "idVal" : "dashboardRepBillable",       "title" : "My Reportees Data",       		         "store_name" : "ReporteesBillable"},
				{"thumb" : "employee_Details.png",      "tool_tip" : "Employee Details",                "view_name" : "dashboard.EmployDetailsPanel",           "idVal" : "EmployeeDetails",            "title" : "Employee Details",                       "store_name" : "EmployeeDetails"},
				{"thumb" : "locationwise_employees.png","tool_tip" : "Location wise Employees",          "view_name" : "dashboard.locationPanel",                "idVal" : "location",                   "title" : "Location wise Employees",                 "store_name" : "LocationChart"},
				{"thumb" : "shiftwise_employees.png",   "tool_tip" : "Shift wise Employees",             "view_name" : "dashboard.shiftPanel",                   "idVal" : "shift",                      "title" : "Shift wise Employees",                    "store_name" : "ShiftChart"},
				{"thumb" : "gradebased_employees.png",  "tool_tip" : "Grade wise Employees",             "view_name" : "dashboard.gradePanel",                   "idVal" : "grade",                      "title" : "Grade wise Employees",                    "store_name" : "GradeChart"},
				{"thumb" : "hrpool.png",                "tool_tip" : "How Long In HR Pool",             "view_name" : "dashboard.Employ_HRPool",                "idVal" : "EmployeeHRPool",             "title" : "How Long In HR Pool",                    "store_name" : "EmployeeHRPool"},
				{"thumb" : "hierarchy-emp.png",         "tool_tip" : "Employee Hierarchy",              "view_name" : "dashboard.treePanel",                    "idVal" : "hierarchy",                  "title" : "Employee Hierarchy",                     "store_name" : "Hierarchy"},
				{"thumb" : "locationwise_client.png",   "tool_tip" : "Location wise Clients",            "view_name" : "dashboard.locationClientPanel",          "idVal" : "locationClient",             "title" : "Location wise Clients",                   "store_name" : "LocationClientChart"},
                {"thumb" : "err_app_esc.png",           "tool_tip" : "Incident Log",                    "view_name" : "dashboard.errorsPanel",                  "idVal" : "errors",                     "title" : "Incident Log",                           "store_name" : "ErrorsChart"},
				//{"thumb" : "shiftwise_client.png",      "tool_tip" : "Shiftwise Clients",               "view_name" : "dashboard.shiftClientPanel",             "idVal" : "shiftClient",                "title" : "Shiftwise Clients",              "store_name" : "ShiftClientChart"},
				// {"thumb" : "work_order_request.png",               "tool_tip" : "Work Order Request",              "view_name" : "dashboard.nrrRdrPanel",                  "idVal" : "nrrRdr",                     "title" : "Work Order Request",             "store_name" : "NrrRdrChart"},
				// {"thumb" : "change_order_request.png",               "tool_tip" : "Change Order Request",            "view_name" : "dashboard.corPanel",                  	"idVal" : "cor",                     	"title" : "Change Order Request",           "store_name" : "CorChart"},
				//{"thumb" : "meetingTime.png",           "tool_tip" : "Meeting Minutes",                 "view_name" : "dashboard.Meet_Minutes",                 "idVal" : "dashboardMeetMin",           "title" : "Meeting Minutes",                "store_name" : "meetingMinDashboard"},
				//{"thumb" : "utilisation.png",           "tool_tip" : "Utilization",                     "view_name" : "dashboard.utilizationPanel",             "idVal" : "utilization",                "title" : "Utilization",                    "store_name" : "UtilizationChart"},
				//{"thumb" : "accuracy.png",              "tool_tip" : "Accuracy",                        "view_name" : "dashboard.accuracyPanel",                "idVal" : "accuracy",                   "title" : "Accuracy",                       "store_name" : "AccuracyChart"},
			]
		});
		
		for(var i=0;i<selectedSuccessValues.length;i++)
		{
			loadvalues[selectedSuccessValues[i].name] = selectedSuccessValues[i].name;
		}	
		console.log
		var json=[];
		for(var i=0;i<selectedSuccessValues.length;i++)
		{		      
			var recordFind = storeRecords.findRecord('idVal', selectedSuccessValues[i].id);
			datarec = recordFind.data;
			json.push(datarec);
		}
		
		userdata.Icons = json;
		
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/employees/userDashboard/?v='+AM.app.globals.version,
			method: 'POST',
			params: {userDashboard: Ext.encode(userdata),'icons':1},
			success : function(data){
				Ext.getCmp('settingIconsWin').destroy();  
			}
		});
		AM.app.globals.DashboardToolsIcons = Ext.encode(userdata);
		var store = Ext.getStore('DashboardTool');
		store.removeAll();
		
		var data = json;
		
		store.sync();
		store.loadData(data,true);
		
		Ext.getCmp('PanelMenuIcons').removeAll();
		
		Ext.getCmp('PanelMenuIcons').add({			
			xtype : Ext.create('AM.view.dashboard.panelMenu',{store:store} )
		}, Ext.create('Ext.Img', {
			src: AM.app.globals.uiPath+'resources/images/settingsIcon1.png',
			id: 'Setingsicons',			        
			listeners:{
				el: {
					click: function() {
						AM.app.getController("AM.controller.Dashboard").settingIcon();
					}
				},
				afterrender: function(c) {
					Ext.create('Ext.tip.ToolTip', {
						target: c.getEl(),
						html: 'Settings'
					});
				}
			}
		}));
		Ext.getCmp('PanelMenuIcons').doLayout();
	},
   
   
	//implemented this single function to show selected client, process values for all dashboard graphs
	showGraphSelectionTitle: function(clients, wor, worNames)
	{
       //console.log(wor);
		var flrVal = "", string="";
       if(wor==null){ var wor=""};
		
		if (clients.length > 0)
		{

			/* flrVal += "Clients : "+clients;
			string += "<b>Verticals</b> : "+clients; */

			flrVal += "Pod : "+clients;
			string += "<b>Pod</b> : "+clients;

		}
        // if (verticals.length > 0)
        // {
        //     flrVal += "Verticals : "+verticals;
        //     string += "<b>Verticals</b> : "+verticals;
        // }
        if (clients.length > 0){
            if(wor.length > 0)
        {
           flrVal += ",";
           string += ","; 
        }

        }
		if(wor.length > 0)
		{
			if (worNames != null)
			{ 
				flrVal += " WOR : "+worNames;
				string += " <b>WOR</b>: "+worNames;
			} 
		}
		
		/* if (verticals.length > 0)
		{
			flrVal += "verticals : "+verticals;
			string += "<b>Verticals</b> : "+verticals;
		} */
		return [string, flrVal];		
	}
  
});