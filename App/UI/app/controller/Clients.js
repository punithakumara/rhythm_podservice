Ext.define('AM.controller.Clients',{
	extend : 'Ext.app.Controller',
	
	stores : ['Clients','Country','ClientContactVertical','Shifts','Attributes','Vertical','Employees','DeleteClientContact','Client_Contacts','ParentVertical','Engagementmanagers','Clientpartner','Services'],
	
	views: ['client.List','client.Add_edit','client.ClientContact','Client_Contacts.List','client.clientUserAdd','client.Client_Users','client.DeletedClientContact'],
	
	editRecord: [],
	refs: [{
        ref: 'clientGrid', 
        selector: 'grid'
    }],
	
	init : function() {
		this.control({
			'clientList' : {
				'deleteClient' : this.onDeleteClient,
				'editClient' : this.onEditClient,
			},
			'clientList #gridTrigger' : {
                keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
			'clientList button[action=createClient]' : {
				click : this.onCreate
			},
			'clientAddEdit' : {
				'saveClient' : this.onSaveClient,
			},
			'clientList #clientStatusCombo' : {
                clientChangeStatusCombo : this.changeClientStatus
            },
			'clientAddEdit #client_Status_combo' : {
                checkProcess : this.checkProcessCall
            },
            'clientUserAdd': {
            	'saveClientUsers' : this.onSaveClientUser,
            	'editClientUsers' : this.oneditClientUser
            },
            'activeclientprocesList #gridTrigger' : {
                keyup : this.onTriggerKeyUpactiveclients,
                triggerClear : this.onTriggerClearactiveclients
            }
		});
	},
	
	viewContent : function() {
		delete this.getStore('Clients').getProxy().extraParams;
		var store = this.getStore('Clients');
		store.getProxy().extraParams = {'clientStatusCombo': 1};
        store.clearFilter();
		
		
		var attrStore = this.getStore('Attributes');
        attrStore.filters.clear();
		this.getStore('Attributes').load({
		params:{'hasNoLimit':'1'}
		});
		
		//AM.app.globals.mainClient = "";		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var clients = this.getView('client.List').create();
		mainContent.removeAll();
		mainContent.add( clients );
		mainContent.doLayout();
	},
	
	dispNewProcessAttrs : function(form) 
	{
    	this.getController('AM.controller.Attributes').onCreate();
	
    },
	
	onTriggerKeyUp : function(t) {		
		Ext.Ajax.abortAll();
		var store = this.getStore('Clients');
		store.filters.clear();
        store.filter({
            property: 'client_name, verticals.name, web, city, state, country',
            value: t.getValue()
        });
    },
    
    onTriggerClear : function() {
        var store = this.getStore('Clients');
        store.clearFilter();
    },
	
	onCreate : function() {
		delete this.getStore('Client_Contacts').getProxy().extraParams;
		var view = Ext.widget('clientAddEdit');
		view.setTitle('Add Client Details');
		Ext.ComponentQuery.query("#client_Status_combo")[0].hide();
	},
	
	onSaveClient : function(form, window) {
		var myStore = this.getStore('Clients');
		var record = form.getRecord(), values = form.getValues();
			// console.log(values)	;
		if(!record) {
			if(Ext.getCmp('ClientContactsList').store.data.items.length == 0){
				var contact_list = Ext.encode(Ext.pluck(Ext.getCmp('ClientContactsList').store.data.items, 'data'));
				if (Ext.getCmp('ClientContactsList').store.data.items.length == 0 ){
					Ext.MessageBox.show({
				        title: 'Alert',
				        msg: 'Please fill the mandatory contact fields',
				        icon: Ext.MessageBox.ERROR,
				        buttons: Ext.Msg.OK
				    });
				    return;
				}
			} else {

			form.url = myStore.getProxy().api.create;
			console.log(values);
			myStore.add(values);
			var titleText = 'Added';
			}
		}
		else {
			console.log(values.status);
			if(values.status == 0){
				if(values.inactive_date == ''){
					Ext.MessageBox.show({
				        title: 'Alert',
				        msg: 'Please fill the inactive date',
				        icon: Ext.MessageBox.ERROR,
				        buttons: Ext.Msg.OK
				    });
				    return;
				}
			}
			if(values.status == 0){
				if(values.reason == ''){
					Ext.MessageBox.show({
				        title: 'Alert',
				        msg: 'Please fill the inactive reason',
				        icon: Ext.MessageBox.ERROR,
				        buttons: Ext.Msg.OK
				    });
				    return;
				}
			}
			if(Ext.getCmp('ClientContactsList').store.data.items.length == 0){
				var contact_list = Ext.encode(Ext.pluck(Ext.getCmp('ClientContactsList').store.data.items, 'data'));
				if (Ext.getCmp('ClientContactsList').store.data.items.length == 0 ){
					Ext.MessageBox.show({
				        title: 'Alert',
				        msg: 'Please fill the mandatory contact fields',
				        icon: Ext.MessageBox.ERROR,
				        buttons: Ext.Msg.OK
				    });
				    return;
				}
			} else {
				form.url = myStore.getProxy().api.update;
				console.log(values);
				record.set(values);
				var titleText = 'Updated';
			}
		}	
		
		form.submit({
			method: 'POST',
            waitMsg : 'Saving your data, please wait...',
            success: function(form,o) {				
                var retrData = JSON.parse(o.response.responseText);
                console.log(retrData);

                if(retrData.success || retrData.totalCount>0) {
                	if(retrData.title=="Existing"){
						Ext.MessageBox.show({
						title: "Alert",
						msg: retrData.msg ,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") {
								// window.close();
								// window.destroy();
							}
						}
					});
					titleText = retrData.title
					}
		    		if(retrData.title!="Existing") 
					{
		    			var contactstore	= Ext.getStore('Client_Contacts');
						AM.app.globals.mainClient = retrData.lastIsertedID;
						contactstore.getProxy().extraParams = {'client_id':AM.app.globals.mainClient};
						// contactstore.add(values);
						contactstore.sync({
							success: function (batch, operations) {
								var jsonResp = batch.proxy.getReader().jsonData;
		    	
	    						if(!jsonResp.success)
	    						{
		    						Ext.MessageBox.show({
                				        title: 'Error',
                				        msg: jsonResp.msg,
                				        icon: Ext.MessageBox.ERROR,
                				        buttons: Ext.Msg.OK
                				    });
		    					}
		    					//myStore.load({params:{"MeetingMinID":AM.app.globals.MeetingMinID}});
								AM.app.globals.dashboardClient = AM.app.globals.mainClient;

								contactstore.load();
							}, 
							failure: function (batch) {
								console.log("Error occured while saving Client Contacts. Please try again.");
								Ext.MessageBox.hide();
								contactstore.rejectChanges();
							}	
						});
					
						Ext.MessageBox.show({
						title: "Success",
						msg: "Client details saved successfully.",
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") {
								window.close();
								window.destroy();
							}
						}
					});
					window.close();
					window.destroy();
					}
					
					
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        //title: 'Error',
                        msg: retrData.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK,
                    });               
		    		//window.close();
		    	}
				myStore.load();
            },
            failure: function(form, action) {
            	console.log("Error occured while saving client. Please try again.");
                myStore.rejectChanges();
                window.close();
            }
        });
	},
	
	onEditClient : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		
		this.editRecord[0] = rec;
		var Attributes = this.getStore('Attributes');
		Attributes.getProxy().extraParams = {'hasNoLimit':'1'};
		
		var Services = this.getStore('Services');
		Services.getProxy().extraParams = {'hasNoLimit':'1'};
		
		AM.app.globals.mainClient = rec.data.client_id;
		AM.app.getStore('Client_Contacts').getProxy().extraParams = {'client_id':AM.app.globals.mainClient};
		var view = Ext.widget('clientAddEdit');
		
		if(rec.get('logo')!="")
			Ext.get('imgLogo').dom.src = AM.app.globals.uiPath+'resources/uploads/client/'+rec.get('logo');
		view.setTitle('Edit Client Details');
		Ext.ComponentQuery.query("#client_Status_combo")[0].show();
		Ext.get('imgLogo').setStyle({'height':'40px'});
		
		var tabpanel = view.query('tabpanel')[0];
		
		var attrData = [];
		var Attribute_Names = rec.get('Attribute_Names');
		
		
		//var OrderofAttr =  rec.get('OrderofAttributes');

		if((rec.get('country')!="") && (rec.get('country')!=null))
		{
			
			rec.set('country', rec.get('country').split(','));
		}
		
		if((rec.get('service_id')!="") && (rec.get('service_id')!=null))
		{
			var ser_id =rec.get('service_id');
			rec.set('service_name', (ser_id.toString().split(',')));
		}
		
		
		 view.down('form').loadRecord(rec);
		 
		 grid.getStore().load();
	},


	// To remove Client Contact - Start
	onRemove_Contact : function(grid, row, col) {
		var rec		= grid.getStore().getAt(row);
		var myMask	= new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();

		Ext.Ajax.request( {
			url		: AM.app.globals.appPath+'index.php/clients/Remove_clientContact/?v='+AM.app.globals.version,
			method	: 'POST',
			params	: { clientContactId: rec.get('contact_id'), clientContactEmail: rec.get('contact_email'), role: rec.get('role'), status	: 'delete'},
			success	: function (batch, operations) {
				grid.store.remove(rec);
				var jsonResp = Ext.decode(batch.responseText); 
				if(jsonResp.success){
					myMask.hide();
					Ext.MessageBox.show({
						title: 'Contact Removed',
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK,
						fn: function(buttonId) {
							if (buttonId === "ok") {
								grid.store.load();
							}
						}
					} );						    	
				}
				grid.store.load();
			}
		} );
	},


	// To remove Client Contact - End
	RollBackContact	: function(grid, row, col) {
		var rec		= grid.getStore().getAt(row);
		var myMask	= new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
		myMask.show();
		Ext.Ajax.request( {
			url		: AM.app.globals.appPath+'index.php/clients/Remove_clientContact/?v='+AM.app.globals.version,
			method	: 'POST',
			params	: { clientContactId: rec.get('contact_id'), clientContactEmail	: rec.get('contact_email'), role: rec.get('role'), status	: 'rollback' },
			success	: function (batch, operations) {
				grid.store.remove(rec);
				var jsonResp = Ext.decode(batch.responseText);
				if(jsonResp.success) {
					myMask.hide();
					Ext.MessageBox.show( {
						title	: 'Contact Rolled back',
	                    msg		: jsonResp.msg,
	                    buttons	: Ext.Msg.OK,
	                    fn		: function(buttonId) {
	                    	if (buttonId === "ok") {
	                    		Ext.getCmp('clientAddEdit').destroy(); 
	                        	//grid.store.load();
	                        }
	                    }
	                } );
				}
				grid.store.load();
			}
	   	} );
	},
	
	onDeleteClient : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		var clientName = rec.get('Client_Name');
		Ext.Msg.confirm('Delete Client', 'Are you sure to delete '+clientName+' ?', function (button) {
            if (button == 'yes') {
        		grid.store.remove(rec);
        		grid.store.sync({ 
        		    success: function (batch, operations) {
        		    	
        		    	var jsonResp = batch.proxy.getReader().jsonData;
        		    	
        		    	if(jsonResp.success) {
        		    		Ext.MessageBox.show({
                                title: 'Deleted',
                                msg: jsonResp.msg,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	else {
        		    		Ext.MessageBox.show({
                                title: 'Error',
                                msg: jsonResp.msg,
                                icon: Ext.MessageBox.ERROR,
                                buttons: Ext.Msg.OK
                            });
        		    	}
        		    	grid.store.load();
        		    }, 
        		    failure: function (batch) {
        		    	console.log("Error occured while deleting client. Please try again.");
        		    	grid.store.rejectChanges();
        		    }
        		});
            }
        }, this);
	},
	
	changeClientStatus: function(combo){
		var store = this.getStore('Clients');
		store.getProxy().extraParams = {'clientStatusCombo': combo.getValue()};
		store.load();
		if(combo.getValue())
			store.clearFilter();
	},
	
	checkProcessCall: function(combo){
   		var res = Ext.ComponentQuery.query("#client_Status_combo")[0];
		var selectedStatus = combo.getValue();
		var ClientID = AM.app.globals.mainClient;
	
		if(selectedStatus == 0){
			Ext.Msg.confirm('Confirm', 'Are you sure you want to Inactive?', function (button) {
				if (button == 'yes') {
						Ext.Ajax.request({
						url: AM.app.globals.appPath+'index.php/process/checkProcessStatus/?v='+AM.app.globals.version,
						method: 'POST',
						params: {
						   'ClientID': ClientID,
						   },
					   scope: this,
					   success: function(response, opts) {
						  var myObject = Ext.JSON.decode(response.responseText);
						  if(myObject == 'dontAllow'){
							Ext.MessageBox.alert('Alert', 'Client can not be Inactivated because Process Under the Client are Active');
							Ext.ComponentQuery.query("#client_Status_combo")[0].setValue('1');
						  }
					   },
					   failure: function(response, opts) {
						  console.log('server-side failure with status code ' + response.status);
					   }
					});
			   }else{
			   		Ext.ComponentQuery.query("#client_Status_combo")[0].setValue('1');
			   }
		   }, this);
		}else if(selectedStatus == 1){
			Ext.Msg.confirm('Confirm', 'Are you sure you want to Active?', function (button) {
				if (button == 'no') {
					Ext.ComponentQuery.query("#client_Status_combo")[0].setValue('0');
				}
			}, this);
		}
	},
   
	onAddClientUser : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		AM.app.globals.mainClient = rec.data.ClientID;
		var view = Ext.widget('clientUserAdd');
		view.setTitle('Add Client User');
		Ext.getCmp('ClientNme').setValue(rec.data.Client_Name);
	   
	},
   
	onSaveClientUser : function(form,win){
	    var myStore = this.getStore('ClientUser');
		var record = form.getRecord();
		
		var values = form.getValues();	
		if(!record) {
			myStore.add(values);
			var titleText = 'Added';
		}else{
			record.set(values);
			var titleText = 'Updated';
		}
	
		myStore.sync({ 
		    success: function (batch, operations) {
		    	//var myContacts = this.getStore('ClientContactVertical');
		    	var jsonResp = batch.proxy.getReader().jsonData;
		    	if(jsonResp.success) {
		    		Ext.MessageBox.show({
                        title: jsonResp.title,
                        msg: jsonResp.msg,
                        buttons: Ext.Msg.OK
                    });
		    		
		    		if(jsonResp.title!="Existing"){
		    			Ext.getCmp('FullName').setValue('');
		    			Ext.getCmp('Username').setValue('');
		    			Ext.getCmp('Email').setValue('');
		    		}
		    		Ext.getStore('ClientUser').load();
		    	}
		    	else {
		    		Ext.MessageBox.show({
                        title: 'Error',
                        msg: jsonResp.msg,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
		    		win.close();
		    	}
		    	Ext.getStore('Clients').load();
				
		    }, 
		    failure: function (batch) {
		    	console.log("Error occured while adding User. Please try again.");
		    	Ext.MessageBox.hide();
		    	myStore.rejectChanges();
		    }
		});
	},
	
	oneditClientUser : function(grid,row,col){
		var rec = grid.getStore().getAt(row);
		var FullName = rec.get('FullName');
		Ext.getCmp('clientUserFrm').loadRecord(rec);
	},
   
	viewclientprocessList:function(){
	    delete this.getStore('ActiveClientProcess').getProxy().extraParams;
		var store = this.getStore('ActiveClientProcess');
        store.clearFilter();
      //  store.getProxy().extraParams = {'pverticalCombo':Ext.getCmp('clientProcessRept').getValue()};
	    var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var clients = this.getView('ActiveClientProcess.List').create();
		mainContent.removeAll();
		mainContent.add( clients );
		mainContent.doLayout();
	},
	
	onTriggerKeyUpactiveclients : function(t) {
		
		Ext.Ajax.abortAll();
		var store = this.getStore('ActiveClientProcess');
		store.filters.clear();
		store.filter({
			property: 'Client_Name,process.Name,vertical.Name',
			value: t.getValue()
		});
	},
   
	onTriggerClearactiveclients : function() {
		var store = this.getStore('ActiveClientProcess');
		store.clearFilter();
	},
	
	changeParentVertical: function(combo){
		var store = this.getStore('ActiveClientProcess');
		store.getProxy().extraParams = {'pverticalCombo': combo.getValue()};
		store.load();
		if(combo.getValue())
			store.clearFilter();
	},
	
	downloadactClientProcessCSV: function(){
	    var myStore = this.getStore('ActiveClientProcess');
   		var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Please Wait....'});
   		myMask.show();
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/clients/DownloadActiveClientProcessCsv/?v='+AM.app.globals.version,
			method: 'GET',
			params: {'Report':"reportflag",'pverticalCombo':Ext.getCmp('clientProcessRept').getValue()},
			success: function(responce, opts){
				var jsonResp = Ext.decode(responce.responseText);
				if(jsonResp.success)
				{	
					myMask.hide();
					window.location = AM.app.globals.appPath+'download/ActiveClientProcess.csv?v='+AM.app.globals.version;
				}
			}
		});
	},
	
	/*filterProcessCombo	: function() {
   		var myStore		= Ext.getStore('ClientContactProcess');
   		myStore.clearFilter();
   		// Ext.ComponentQuery.query('#client_cont_process')[0].setValue("");
   		var verticalId	= Ext.ComponentQuery.query('#client_cont_vert')[0].getValue();
		if(verticalId) {
			myStore.load( {
				params	: { 
					'VerticalID'		: verticalId,
				}
			} );
			
			// Ext.ComponentQuery.query('#client_cont_process')[0].bindStore('ClientContactProcess');
		}
    }*/
} );