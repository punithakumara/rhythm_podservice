Ext.define('AM.controller.Tools',{
	extend : 'Ext.app.Controller',
	store : 'Tools',
	views: ['Skills.ToolsList','Skills.Tools_Add_edit'],
	
	init : function(){
		this.control({
			'ToolsList #gridToolTrigger' : {
				keyup : this.onTriggerKeyUpSearchTools,
				triggerClear : this.onTriggerClearSearchTools
			},
			'toolsAddEdit' : {
				'saveTool' : this.onSaveTool
			}
		});
	},	

	viewContent : function() {
		var toolStore = this.getStore('Tools');
		toolStore.clearFilter();

		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var tool = this.getView('Skills.ToolsList').create();
		mainContent.removeAll();
		mainContent.add( tool );
		mainContent.doLayout();
	},
	
	newTechnology : function(verticalId) {
		var view = Ext.widget('toolsAddEdit');
	},
	
	onTriggerKeyUpSearchTools : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('Tools');
		store.filters.clear();
		store.filter({
			property: 'skill_name',
			value: t.getValue()
		});
	},

	onTriggerClearSearchTools : function() {
		var store = this.getStore('Tools');
		store.clearFilter();
	},
	
	onCreate : function() {
		var toolStore = this.getStore('Tools');
		toolStore.clearFilter();
		
		var view = Ext.widget('toolsAddEdit');
		view.setTitle('Add Tool');
	},
	
	onSaveTool : function(form, win) {
		
		var myStore = this.getStore('Tools');
		var record = form.getRecord(),
		values = form.getValues();

		form.url = myStore.getProxy().api.create;
		if(!record) 
		{
			var message = 'Tool Added Successfully';
		}
		else 
		{
			var message = 'Tool Updated Successfully';
		}
		
		form.submit({ 
			method: 'POST',
			waitMsg : 'Saving your data, please wait...',
			success: function (form, o) {	    	
				var jsonResp = JSON.parse(o.response.responseText);
				console.log(jsonResp);
				if(jsonResp.success == 1) 
				{
					Ext.MessageBox.show({
						title: 'Success',
						msg: message,
						buttons: Ext.Msg.OK,
						icon: Ext.MessageBox.INFO,
						fn: function(buttonId) {
							if (buttonId === "ok" && jsonResp.title == "Added") 
							{
								Ext.getCmp('toolsListGridID').getStore().reload();
								win.close();
							}
						}
					});	
				}
				else 
				{
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK
					});
				}
			}, 
			failure: function (batch) {
				console.log("Error occured while saving Tool. Please try again.");
				Ext.MessageBox.hide();
				myStore.rejectChanges();
			}
		});
	},
	
	onEditTool : function(grid, row, col) {
		var rec = grid.getStore().getAt(row);
		console.log(rec);
		var view = Ext.widget('toolsAddEdit');
		view.setTitle('Edit Tool');
		view.down('form').loadRecord(rec);
	},
	


});