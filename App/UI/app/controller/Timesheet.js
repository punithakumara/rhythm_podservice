Ext.define('AM.controller.Timesheet', {
	extend: 'Ext.app.Controller',
	stores:['Timesheet','TimesheetWeekrange','TimesheetFutWeekrange','timesheetTaskCode','timesheetSubTask','timesheetClients','ApproveTimesheet','Employees'],
	views: ['timesheet.list','timesheet.dashboard','timesheet.approvelist','timesheet.comments','timesheet.duplicate','timesheet.approveComments', 'timesheet.MonthField','timesheet.SelectDateRange'],
	
	init : function() {
		this.control({
			'approvelist #timeGridTrigger' : {
				keyup : this.onTriggerKeyUp,
				triggerClear : this.onTriggerClear
			},
		});
	},
	
	viewContent : function()
	{
		AM.app.globals.dupTimeRec = [];
		
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('timesheet.list').create();
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
		
		if(Ext.util.Cookies.get("grade") < 3)
		{
			Ext.getCmp('teamMember').hide();
		}
	},
	
	viewDashboard : function()
	{
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('timesheet.dashboard').create();
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
		
		if(Ext.util.Cookies.get("grade") < 3)
		{
			Ext.getCmp('teamPanel').hide();
		}
		if(Ext.util.Cookies.get("empid") == 1296)
		{
			Ext.getCmp('selfPanel').hide();
			Ext.getCmp('teamPanel').setTitle('Org data');
		}
	},

	slectDateRange : function()
	{
		Ext.widget('timesheetSelectDateRange');
	},
	
	approveTimesheet : function()
	{
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('timesheet.approvelist').create();
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
		
		if(Ext.util.Cookies.get("grade") == 4)
		{
			Ext.getCmp('chk_group2').hide();
		}
		else if(Ext.util.Cookies.get("grade") < 4)
		{
			Ext.getCmp('chk_group1').hide();
			Ext.getCmp('chk_group2').hide();
		}
	},
	
	listRecords: function(me, weekrange, teamMember)
	{
		AM.app.globals.dupTimeRec = [];

		Ext.getStore('Timesheet').load({
			params:{
				'week_range':weekrange,
				'teamMember':teamMember,
			},
			callback:function(records, options, success) {
				if (success) 
				{
					if(records.length != 0)
					{
						me.storeLoad(me.store,me, weekrange);
						me.doLayout(false,true);
					}
					else
					{
						me.storeLoad(me.store,me);
						me.doLayout(false,true);
					}					
				}
			}
		});
		Ext.ComponentQuery.query('#TimesheetGridID')[0].getView().refresh();
		Ext.getCmp('saveTimeRow').enable();
	},
	
	listApproveRecords: function(me, weekstatus, weekrange, level_members)
	{
		Ext.getStore('ApproveTimesheet').load({
			params:{
				'level_members':level_members,
				'week_status':weekstatus,
				'week_range':weekrange,
			},
			callback:function(records, options, success) {
				if (success) 
				{
					if(records.length != 0)
					{
						me.storeLoad(me.store,me);
						me.doLayout(false,true);
					}
					else
					{
						me.storeLoad(me.store,me);
						me.doLayout(false,true);
					}					
				}
			}
		});
		Ext.ComponentQuery.query('#TimesheetApproveGridID')[0].getView().refresh();
		Ext.getCmp('approveTime').enable();
		Ext.getCmp('rejectTime').enable();
	},
	
	deleteTimesheet : function(grid, row, col)
	{
		var rec = grid.getStore().getAt(row);
		
		Ext.Msg.confirm('Confirm', 'Are you sure you want to delete this line item? <br>You will not be able to retrieve the deleted data!', function (button) {
			if (button == 'yes') 
			{
				Ext.Ajax.request({
					url: AM.app.globals.appPath+'index.php/timesheet/timesheetDelete',
					method: 'POST',
					params: {
						'timesheetID': rec.data.timesheetID,
					},
					success: function(response) {
						var myObject = Ext.JSON.decode(response.responseText);
						
						Ext.MessageBox.show({
							title: "Success",
							msg: "Record deleted Successfully",
							icon: Ext.Msg.INFO,
							buttons: Ext.Msg.OK,
							fn: function(buttonId) {
								if (buttonId === "ok") 
								{
									Ext.getCmp('TimesheetGridID').getStore().reload();
								}
							}
						});
					},
				});
				rec.store.remove(rec);
			}
		}, this);
	},
	
	viewTimesheetComments : function(grid, row, col)
	{
		var rec = grid.getStore().getAt(row);

		var view = Ext.widget('timesheetComments');
		view.down('form').loadRecord(rec);
		
		if(rec.data.day1=="")
		{
			Ext.getCmp('Mon').setDisabled(true);
		}
		if(rec.data.day2=="")
		{
			Ext.getCmp('Tue').setDisabled(true);
		}
		if(rec.data.day3=="")
		{
			Ext.getCmp('Wed').setDisabled(true);
		}
		if(rec.data.day4=="")
		{
			Ext.getCmp('Thu').setDisabled(true);
		}
		if(rec.data.day5=="")
		{
			Ext.getCmp('Fri').setDisabled(true);
		}
		if(rec.data.day6=="")
		{
			Ext.getCmp('Sat').setDisabled(true);
		}
		if(rec.data.day7=="")
		{
			Ext.getCmp('Sun').setDisabled(true);
		}
	},
	
	duplicateTimesheet : function(idAry)
	{
		var view = Ext.widget('timesheetDuplicate');
		
		Ext.getCmp('duptimesheetID').setValue(idAry);
	},
	
	approveTimesheetComments : function(grid, row, col)
	{
		var rec = grid.getStore().getAt(row);

		var view = Ext.widget('approveComments');
		view.down('form').loadRecord(rec);
		
		if(rec.data.comment1=="")
		{
			Ext.getCmp('comment1').hide();
		}
		if(rec.data.comment2=="")
		{
			Ext.getCmp('comment2').hide();
		}
		if(rec.data.comment3=="")
		{
			Ext.getCmp('comment3').hide();
		}
		if(rec.data.comment4=="")
		{
			Ext.getCmp('comment4').hide();
		}
		if(rec.data.comment5=="")
		{
			Ext.getCmp('comment5').hide();
		}
		if(rec.data.comment6=="")
		{
			Ext.getCmp('comment6').hide();
		}
		if(rec.data.comment7=="")
		{
			Ext.getCmp('comment7').hide();
		}
	},
	
	saveTimesheet : function(rec, weekcode, teamMember, me)
	{
		Ext.Ajax.request({
			url: AM.app.globals.appPath+'index.php/timesheet/timesheetSave',
			method: 'POST',
			params: {
				'timesheetID': rec.timesheetID,
				'teamMember' : teamMember,
				'ClientName' : rec.ClientName,
				'TaskCode' : rec.Task,
				'SubTask'  : rec.SubTask,
				'weekcode' : weekcode,
				'day1' : rec.day1,
				'day2' : rec.day2,
				'day3' : rec.day3,
				'day4' : rec.day4,
				'day5' : rec.day5,
				'day6' : rec.day6,
				'day7' : rec.day7,
			},
			success: function(response) {
				var myObject = Ext.JSON.decode(response.responseText);
				
				if(myObject["success"])
				{
					Ext.MessageBox.show({
						title: "Success",
						msg: "Saved Successfully",
						buttons: Ext.Msg.OK,
						icon: 'success-icon',
						//iconCls: 'success-icon',
						closable:false,
						fn: function(buttonId) {
							if (buttonId === "ok") 
							{
								AM.app.getController('Timesheet').listRecords(Ext.getCmp('TimesheetGridID'), weekcode, teamMember);
							}
						}
					});
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: "Something went worng.",
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
			},
		});
	},
	
	
	onTriggerKeyUp : function(t) {
		var gridPanel = Ext.getCmp('TimesheetApproveGridID');
		gridPanel.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
		
		Ext.Ajax.abortAll();
		var myStore = this.getStore('ApproveTimesheet'); 

		myStore.filters.clear();
		myStore.getProxy().extraParams = {'week_status': Ext.getCmp('timesheetStaus').getValue(),'week_range': Ext.getCmp('timeWeekRange').getValue(), 'level_members':Ext.getCmp('chk_group').getValue()};
		myStore.filter({
			property: 'employees.first_name,employees.last_name,c.client_name,t.task_name,s.sub_task_name',
			value: t.getValue()
		});
		myStore.load();				
	},

	onTriggerClear : function() {
		var myStore = this.getStore('ApproveTimesheet');
		myStore.getProxy().extraParams = {'week_status': Ext.getCmp('timesheetStaus').getValue(),'week_range': Ext.getCmp('timeWeekRange').getValue(), 'level_members':Ext.getCmp('chk_group').getValue()};
		myStore.clearFilter();
		myStore.load();
	},
	
	onApprove : function(obj, gridPanel, data, reason)
	{
		var selection = gridPanel.getSelectionModel().selected;
		
		var selectArrayIds = [];
		var empIds = [];
		var recordData = selection.items;
		var empid = "";
		
		for(var i = 0;i < recordData.length;i++) 
		{
			if(!isNaN(recordData[i].data.timesheetID))
			{
				selectArrayIds[i] = recordData[i].data.timesheetID;
			}
			empid = recordData[i].data.emp_id;
			if(empIds.indexOf(empid)=="-1")
			{
				empIds.push(empid);
			}
		}
		Ext.Ajax.request({
			url : AM.app.globals.appPath+'index.php/timesheet/approve_records/',
			method: 'POST',
			params: {
				timesheetIDs : JSON.stringify(selectArrayIds),
				empIds 		 : JSON.stringify(empIds),
				value		 : data,
				reason 		 : reason
			},
			success: function (batch, operations) 
			{
				var jsonResp = Ext.decode(batch.responseText); 
				if(jsonResp.success) 
				{
					Ext.MessageBox.show({
						title: jsonResp.title,
						msg: jsonResp.msg,
						buttons: Ext.Msg.OK
					});
					
					/* Reload All Data Grid */
					Ext.getCmp('TimesheetApproveGridID').getStore().proxy.extraParams = { 'week_status': Ext.getCmp('timesheetStaus').getValue(),'week_range': Ext.getCmp('timeWeekRange').getValue()};
					Ext.getCmp('TimesheetApproveGridID').getStore().reload();
					
				}
				else 
				{
					Ext.MessageBox.show({
						title: 'Error',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.ERROR,
						buttons: Ext.Msg.OK
					});
				}
				gridPanel.getPlugin('pagingSelectionPersistence').clearPersistedSelection();
			}, 
			failure: function (batch) {
				console.log("Error occured while saving. Please try again.");
				myStore.rejectChanges();
			}
		});
	},
	
	onDownload : function(start_date, end_date, win) {
		var msg = Ext.MessageBox.wait('Exporting data, Please wait...');
		
		Ext.Ajax.request({
			url : AM.app.globals.appPath+'index.php/timesheet/downloadCsvTemplate/',
			method: 'POST',
			timeout: 1800000,
			params: {
				start: start_date, 
				end: end_date,
			},
			success: function (batch, operations) {
				var jsonResp = Ext.decode(batch.responseText); 
				if(jsonResp.success)
				{
					msg.hide();
					window.location = AM.app.globals.appPath+'download/Timesheet.csv';
					win.close();
				}
				else
				{
					Ext.MessageBox.show({
						title: 'Warning',
						msg: jsonResp.msg,
						icon: Ext.MessageBox.WARNING,
						buttons: Ext.Msg.OK
					});
				}

			}
		});
	},
	
});