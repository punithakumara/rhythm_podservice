Ext.define('AM.controller.projectTypes',{
	extend : 'Ext.app.Controller',
	stores: ["ProjectTypes"],
	views: ['ProjectTypes.List'],

	init : function(){
		this.control({
			'ProjectTypes #gridTrigger' : {
				keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            },
		});
	},
	
	viewProjectTypes : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var ProjectTypes = this.getView('ProjectTypes.List').create();
		mainContent.removeAll();
		mainContent.add(ProjectTypes);
		mainContent.doLayout();		
	},

	onTriggerClear : function() {
        var store = this.getStore('ProjectTypes');
        store.clearFilter();
    },
	
	onTriggerKeyUp : function(t) {
		Ext.Ajax.abortAll();
		var store = this.getStore('ProjectTypes');
		store.filters.clear();
        store.filter({
            property: "wor_type,project_type,project_description",
            value: t.getValue()
        });
    }
	
});