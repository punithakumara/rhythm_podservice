Ext.override(Ext.ux.chart.Legend, {

	createItems: function() {
		if (this.customLegend != null) {
			this.customLegend.remove();
		}

		this.customLegend = $('<div class="custom-legend"></div>');
		$(this.chart.el.dom).append(this.customLegend);
		this.callParent();
	},

	createLegendItem: function(series, yFieldIndex) {
		var colorItem = series.getLegendColor(yFieldIndex).match(/.+?\-(.+?)\-.+?/i)[1];
		var legendItem = $('<div class="custom-legendItem"><div class="custom-legendItemMarker" style="background-color: #'+colorItem+'"></div>'+series.yField[yFieldIndex]+'</div>');
		$(this.customLegend).append(legendItem);

		legendItem.on('mouseover', function() {
			series._index = yFieldIndex;
			series.highlightItem();
		});

		legendItem.on('mouseout', function() {
			series._index = yFieldIndex;
			series.unHighlightItem();
		});

		legendItem.on('mousedown', function() {
			var me = this,
				index = yFieldIndex;

			if (!me.hiddenSeries) {
				series.hideAll(index);
				legendItem.addClass('hide');
			} else {
				series.showAll(index);
				legendItem.removeClass('hide');
			}
			me.hiddenSeries = !me.hiddenSeries;
			me.legend.chart.redraw();
		});
	},
	updateItemDimensions: function() {
		return {
			totalWidth:  0,
			totalHeight: 0,
			maxWidth:    0,
			maxHeight:   0
		};
	},
	updatePosition: function() {

	},
	removeItems: function() {
	}
});