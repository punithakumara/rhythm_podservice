Ext.define('Ext.ux.chart.LegendUnclickable', {
    override: 'Ext.chart.LegendItem',

    onMouseDown: function() {
        if (this.legend.clickable !== false) {
            this.callParent(arguments);
        }
    },

    onMouseOver: function() {
        if (this.legend.clickable !== false) {
            this.callParent(arguments);
        }
    },

    onMouseOut: function() {
        if (this.legend.clickable !== false) {
            this.callParent(arguments);
        }
    }
});