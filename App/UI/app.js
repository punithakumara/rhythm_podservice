Ext.Loader.setPath('Ext.ux', 'extjs/src/ux'); 
Ext.application( {
    requires            : ["AM.view.Viewport"],
    name                : "AM",
    appFolder           : "app",
    autoCreateViewport  : false,
    launch              : function() {
        Ext.create("AM.view.Viewport", {
            controller  : this 
        } );

        var checkLogin = this.getController("AM.controller.Login").checkLogin();

        if (checkLogin) 
		{
			this.getController("AM.controller.Login").checkSession();
			
			// var DsnName = Ext.util.Cookies.get("DsnName");
			//if (DsnName.length > 25) 
				//DsnName = DsnName.substring(0, 22) + "...";

			//Ext.getCmp("logInUserName").update("<b>" + '<span style="float:left">' + Ext.util.Cookies.get("username") + '</span></b>');
			Ext.getCmp("logInCont").update('<span class="notiCnt">' + Ext.util.Cookies.get("Notifications") + '</span>');

			this.getController("Menu").viewMenu();
			this.getController("Menu").viewNotifyMenu();
			var viewport    = Ext.ComponentQuery.query("viewport")[0];
			var lay         = viewport.getLayout();
			lay.setActiveItem(1);
			//console.log(Ext.util.Cookies.get("grade"));
			//console.log(Ext.util.Cookies.get("grade"));
			if(Ext.util.Cookies.get("grade")<3 && Ext.util.Cookies.get("is_onshore")==1)
			{
				console.log(Ext.util.Cookies.get('employee_id'));
				AM.app.getController("AM.controller.TimesheetOnshore").viewContent();
			}
			else if(Ext.util.Cookies.get("grade")<3 && Ext.util.Cookies.get("is_onshore")!=1)
			{
				AM.app.getController("AM.controller.Timesheet").viewContent();
			}
			else
			{
				this.getController("AM.controller.Dashboard").viewContent();
			}
        }
		else 
		{ 
            this.getController("AM.controller.Login").index();
        } 
    },

    globals : {
        appPath             : "http://" + window.location.host + "/RhythmPOD/App/services/",
        onboardPath         : "http://" + window.location.host + "/RhythmPOD/App/onboarding/survey/",
        uiPath              : "http://" + window.location.host + "/RhythmPOD/App/UI/",
		qaUrl               : "172.19.19.229",
        devUrl              : "172.19.18.98:8100", 
        version             : "3.7.1",
        itemsPerPage        : 15,
        menuObj             : [],
		CommonDateControl   : "d-M-Y",
		workmodelsofclient  : [],
		modifiedRecord      : false,
		utilEmployIDs		:'',
		wor_cor_combostatus	:'',
        redir               :'',
        linerecord          :'',
		cormindate          :'',
		timesheetTask       :'',
		utilizationWOR      :'',
		utilizationProject  :'',
		utilizationTask       :'',
		timesheetClient     :'',
		utilizationClient   :'',
		wor_type_id       	:'',
		HRVert              :10,
		viewIndEmp			: '',
        importUtilClients	:['235', '371', '99', '313', '363', '430', '283', '474', '331', '456', '357', '260', '288', '365', '264', '442', '469', '347', '432','477','222','429','199']
        //importUtilClients	:['235']
    }
} );