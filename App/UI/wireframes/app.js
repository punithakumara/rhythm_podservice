Ext.Loader.setPath('Ext.ux', 'extjs/src/ux'); 
Ext.application( {
    requires            : ["AM.view.Viewport"],
    name                : "AM",
    appFolder           : "app",
    autoCreateViewport  : false,
    launch              : function() {
        Ext.create("AM.view.Viewport", {
            controller  : this
        } );

		this.getController("Menu").viewMenu();
		var viewport    = Ext.ComponentQuery.query("viewport")[0];
		var lay         = viewport.getLayout();
	
		lay.setActiveItem(1);     
    },

    globals : {
        appPath                 : "http://" + window.location.host + "/services/",
        onboardPath             : "http://" + window.location.host + "/onboarding/survey/",
        uiPath                  : "http://" + window.location.host + "/UI/",
        version                 : "1.2.7",
        itemsPerPage            : 15,
        menuObj                 : [],
		CommonDateControl       : "d-M-Y",
    }
} );