Ext.define('AM.store.Remarks', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'change_date', type: 'string'},
        {name: 'count', type: 'string'},
    ],
	data: [
		{change_date:"10-Jan-2018", count:"+2"},
		{change_date:"20-Jan-2018", count:"-4"},
	],
});