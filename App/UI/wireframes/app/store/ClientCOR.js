Ext.define('AM.store.ClientCOR', {
	extend : 'Ext.data.Store', 
    fields: ['change_order_id', 'client', 'start_date', 'end_date', 'status', 'raised_by'],
    data : [
        {'change_order_id': 'COR123', 'client': 'Fairfax Media', 'start_date' : '24-MAR-2017', 'end_date':'15-MAR-2018', 'status':'In-Progress', 'raised_by':'Vinay B N'},
    ]
});