// for static data

Ext.define('AM.store.ProjectTypes', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'wor_type', type: 'string'},
        {name: 'project_name', type: 'string'},
        {name: 'project_description', type: 'string'},
        {name: 'total_roles', type: 'string'},
    ],
	data: [
		{wor_type:"wortype1",project_name:"Test4", project_description:"Test4 Test4 Test4", total_roles:"17"},
		{wor_type:"wortype2",project_name:"Test3", project_description:"Test3 Test3 Test3", total_roles:"15"},
		{wor_type:"wortype3",project_name:"Test1", project_description:"Test1 Test1 Test1", total_roles:"23"},
		{wor_type:"wortype4",project_name:"Test2", project_description:"Test2 Test2 Test2", total_roles:"10"},
		{wor_type:"wortype5",project_name:"Test5", project_description:"Test5 Test5 Test5", total_roles:"14"},
	],
});


// for dynamic data

/*Ext.define('AM.store.ProjectTypes', {
	extend : 'Ext.data.Store',
	model : 'AM.model.ProjectTypes',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/projectTypes/project_types/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});*/