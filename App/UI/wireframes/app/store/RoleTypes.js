Ext.define('AM.store.RoleTypes', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'role_name', type: 'string'},
        {name: 'role_description', type: 'string'},
        {name: 'project_name', type: 'string'},
    ],
	data: [
		{role_name:"Test1", role_description:"Test1 Test1 Test1", project_name:"project1"},
		{role_name:"Test2", role_description:"Test2 Test2 Test2", project_name:"project2"},
		{role_name:"Test3", role_description:"Test3 Test3 Test3", project_name:"project3"},
		{role_name:"Test4", role_description:"Test4 Test4 Test4", project_name:"project4"},
		{role_name:"Test5", role_description:"Test5 Test5 Test5", project_name:"project5"},
	],
});