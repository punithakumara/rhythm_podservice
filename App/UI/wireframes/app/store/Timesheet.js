Ext.define('AM.store.Timesheet', {
	extend : 'Ext.data.Store', 
    fields: ['wor_name','client_name','task_name','sub_task_name','mon','tue','wed','thu','fir','sat','sun'],
    data : [{
		'wor_name'		: 	'EngagePOC_Non_Bilable',
		'client_name'	: 	'Engage Smart',
		'task_name'		: 	'Meeting',
		'sub_task_name'	: 	'Client Call',
		'mon'			: 	'',
		'tue'			: 	'01:55',
		'wed'			:	'',
		'thu'			: 	'00:10',
		'fri'			: 	'',
		'sat'			: 	'',
		'sun'			: 	'',
	},{
		'wor_name'		: 	'EngagePOC_Non_Bilable',
		'client_name'	: 	'Engage Smart',
		'task_name'		: 	'Production Work',
		'sub_task_name'	: 	'',
		'mon'			: 	'',
		'tue'			: 	'',
		'wed'			:	'',
		'thu'			: 	'',
		'fri'			: 	'',
		'sat'			: 	'',
		'sun'			: 	'',
	}]
});