Ext.define('AM.store.TimeEntryFields', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'field_name', type: 'string'},
        {name: 'field_description', type: 'string'},
        {name: 'field_property', type: 'string'},
        {name: 'field_type', type: 'string'},
    ],
	data: [
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
		{field_name:"Test1", field_description:"Test1 Test1 Test1", field_property:"test",field_type:"test"},
	],
});