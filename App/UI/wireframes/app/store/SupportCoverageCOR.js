Ext.define('AM.store.SupportCoverageCOR', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"1", "name":"M-F"},
        {"id":"2", "name":"T-Sa"},
        {"id":"3", "name":"W-Su"},
        {"id":"4", "name":"Th-M"},
        {"id":"5", "name":"F-Tu"},
        {"id":"6", "name":"Sa-W"},
        {"id":"7", "name":"Su-Th"},
        {"id":"8", "name":"M-S"},
    ]
});