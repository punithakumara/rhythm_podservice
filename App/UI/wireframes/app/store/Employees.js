Ext.define("AM.store.Employees", {
    extend: "Ext.data.Store",
    model: "AM.model.Employee",
    autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    proxy: {
        type: "rest",
        api: {
            read: AM.app.globals.appPath + "index.php/employee/employeesDetails/?v="+AM.app.globals.version,
            create: AM.app.globals.appPath + "index.php/employee/employee_add/?v="+AM.app.globals.version,
            update: AM.app.globals.appPath + "index.php/employee/employee_update/",
            destroy: AM.app.globals.appPath + "index.php/employee/employee_del/",
            url: AM.app.globals.appPath + "index.php/employee/employee_upload/"
        },
        reader: {
            type: "json",
            root: "data",
            successProperty: "success",
            totalProperty: "totalCount",
            idProperty: "EmployID"
        },
        writer: {
            type: "json",
            root: "employ",
            idProperty: "EmployID",
            writeAllFields: true,
            encode: true
        }
    },
    sortOnLoad: true,
    sorters: [ {
        property: "FirstName",
        direction: "ASC"
    } ]
});