Ext.define('AM.store.Responsibilities', {
	extend : 'Ext.data.Store', 
    fields: ['id', 'name'],
    data : [
        {"id":"1", "name":"Trafficker"},
        {"id":"2", "name":"Campaign Manager"},
        {"id":"3", "name":"Web Developer"},
        {"id":"4", "name":"Quality Analyst"},
    ]
});

