Ext.define('AM.store.WorkTypeVolume', {
	extend : 'Ext.data.Store',
	model : 'AM.model.WorkTypeVolume',
	autoLoad: false,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: true,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_view/?v='+AM.app.globals.version,
			create : AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_save/?v='+AM.app.globals.version,
			update: AM.app.globals.appPath+'index.php/Clientrdr/clientrdr_update/',
			//destroy : AM.app.globals.appPath+'index.php/clients/client_del/'
		},
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
			idProperty : 'RDR_ID'
		},
        writer: {
            type: 'json',
            root : 'clientrdr',
            idProperty : 'RDR_ID',
            writeAllFields: true,
            encode : true
        }
	},
	
	sortOnLoad:true,
	
	sorters: [{
        property: 'RDR_ID',
        direction: 'DESC'
    }]
});