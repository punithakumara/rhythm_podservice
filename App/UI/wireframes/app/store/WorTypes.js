// for static data

Ext.define('AM.store.WorTypes', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'name', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'total_projects', type: 'string'},
    ],
	data: [
		{name:"Test1", description:"Test1 Test1 Test1", total_projects:"13"},
		{name:"Test2", description:"Test2 Test2 Test2", total_projects:"14"},
		{name:"Test3", description:"Test3 Test3 Test3", total_projects:"5"},
		{name:"Test4", description:"Test4 Test4 Test4", total_projects:"7"},
		{name:"Test5", description:"Test5 Test5 Test5", total_projects:"4"},
	],
});

// for dynamic data

/*Ext.define('AM.store.WorTypes', {
	extend : 'Ext.data.Store',
	model : 'AM.model.WorTypes',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/worTypes/wor_types/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'rows',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});*/