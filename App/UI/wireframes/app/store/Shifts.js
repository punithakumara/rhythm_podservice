Ext.define('AM.store.Shifts', {
	extend : 'Ext.data.Store',
	model : 'AM.model.Shift',
	autoLoad: true,
    pageSize: AM.app.globals.itemsPerPage,
    remoteSort: true,
    remoteFilter: false,
    autoSync: false,
    
	proxy : {
		type : 'rest',
		api : {
			read : AM.app.globals.appPath+'index.php/shift/shifts_list/?v='+AM.app.globals.version,
		},
		
		reader : {
			type : 'json',
			root: 'shift',
			successProperty : 'success',
			totalProperty: 'totalCount',
		},
	},
});