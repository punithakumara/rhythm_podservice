Ext.define('AM.store.clientHolidays', {
	extend: 'Ext.data.Store',
    fields: [
        {name: 'leave_name', type: 'string'},
        {name: 'date', type: 'string'},
        {name: 'raised_by', type: 'string'},
    ],
	data: [
		{leave_name:"Valentines Day", date:"14-Feb-2018", raised_by:"Ilananal Arunachalam"},
	],
});