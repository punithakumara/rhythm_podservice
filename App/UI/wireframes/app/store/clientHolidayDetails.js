Ext.define('AM.store.clientHolidayDetails', {
	extend: 'Ext.data.Store',
	groupField: 'project_code',
    fields: [
		{name: 'id', type: 'int'},
        {name: 'project_code', type: 'string'},
        {name: 'holiday', type: 'string'},
        {name: 'date', type: 'string'},
    ],
	data: [
		{id:'1', project_code:"PANRA_H_EST_ADQM_MF", holiday:"Presidents Day", date:"19-Feb-2018"},
		{id:'2', project_code:"PANRA_H_EST_ADQM_MF", holiday:"Memorial Day", date:"28-May-2018"},
		{id:'3', project_code:"PANRA_H_EST_ADQM_MF", holiday:"Independence Day", date:"04-Jul-2018"},
		{id:'4', project_code:"PANRA_H_EST_ADQM_MF", holiday:"Labor Day", date:"03-Sep-2018"},
		{id:'5', project_code:"PANRA_H_LEMEA_ADCS_MF", holiday:"Memorial Day", date:"28-May-2018"},
		{id:'6', project_code:"PANRA_H_LEMEA_ADCS_MF", holiday:"Independence Day", date:"04-Jul-2018"},
		{id:'7', project_code:"PANRA_H_LEMEA_ADCS_MF", holiday:"Labor Day", date:"03-Sep-2018"},
		{id:'8', project_code:"PANRA_H_PST_ADCM_MF", holiday:"Memorial Day", date:"28-May-2018"},
		{id:'9', project_code:"PANRA_H_PST_ADCM_MF", holiday:"Independence Day", date:"04-Jul-2018"},
		{id:'10', project_code:"PANRA_H_PST_ADCM_MF", holiday:"Labor Day", date:"03-Sep-2018"},
	],
	idProperty: 'id',
});