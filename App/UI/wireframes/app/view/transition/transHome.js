Ext.define('AM.view.transition.transHome' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transhome',
    id: 'transtionHomePage',
	layout: {
		align: 'stretch',
		type: 'vbox'
	},
    loadMask: true,
    autoCreate: true,
    border : true,
	minHeight:300,
	autoScroll: true,
	width:'99%',
	title: 'People Transition',
	itemId  : 'transtionHomePage',
    initComponent: function() {
    	var me = this;
		this.items=[{
			layout: {
				align: 'stretch',
				type: 'hbox'
			},
			items : [{			
				xtype:'radiogroup',
				id:'radioTrans',
				columns: 1,
				items: [{
					xtype: 'radiofield',
					boxLabel: 'Shift Transition',
					name: 'framework',
					inputValue: '1'
				},{
					xtype: 'radiofield',
					boxLabel: 'Supervisor Transition',
					name: 'framework',
					inputValue: '2'
				},{
					xtype: 'radiofield',
					boxLabel: 'Release to HR Pool',
					name: 'framework',
					inputValue: '3'
				},{
					xtype: 'radiofield',
					boxLabel: 'Pull people from HR Pool',
					name: 'framework',
					inputValue: '4'
				}]
			}]
		}];
		
		this.buttons = [{text: 'Next >>',handler: this.showSecondPage}];
        
        this.callParent(arguments);
    },
	
	showSecondPage: function(btn){
		AM.app.getController('newTransition').showTransSecondPage(btn);
	}
});