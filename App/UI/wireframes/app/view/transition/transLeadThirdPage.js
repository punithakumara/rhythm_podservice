Ext.define('AM.view.transition.transLeadThirdPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.transleadpage',
    id: 'transLeadThirdPage',
	layout : 'anchor',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
    initComponent: function() {
		var me = this;
		this.items=[{
			layout: {
                align: 'stretch',
                type: 'vbox'
            },
			xtype:'radiogroup',
			columns: 1,
			items: [{
				xtype:'gridpanel',
				border:true,
				id:'transRelThirdGridID',
				minHeight:300,
				style: 'padding-right:10px',
				columns: [{
					header: 'Employee ID',  
					dataIndex: 'company_employ_id',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 2
				},{
					header: 'Name',  
					dataIndex: 'FullName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Designation',  
					dataIndex: 'designationName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Email', 
					dataIndex: 'Email', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Vertical', 
					dataIndex: 'vertName', 
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				}]	
			}]
		},{
            xtype: 'datefield',
			fieldLabel: "Date <span style='color:red'>*</span>",
        },{
            xtype: 'combobox',
			fieldLabel: "New Supervisor <span style='color:red'>*</span>",
        },{
			xtype:'textareafield',
			id		  :'transRelComment',
			grow      : true,
			hidden	  : false,
			allowBlank: false,
			name      : 'message',
			fieldLabel: "Comments <span style='color:red'>*</span>",
			anchor    : '99%',
			listeners: {
				afterrender: function(field) {
					field.focus(false, 200);
				}
			}
		}];
		
		this.buttons = [{
			text  : '<< Previous',
			name  : 'back',
			handler: this.showSecondPage
		},{
			text  : 'Confirm >>',
			name  : 'confirmvert',
			handler: this.showEmpFourthpage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },	
	
	showSecondPage: function(btn){
		AM.app.getController('Transition').showTransSecondPage(btn);
	},	
	
	showEmpFourthpage: function(btn){
		AM.app.getController('Transition').onSavePage(btn);
	}
});