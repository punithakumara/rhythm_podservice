Ext.define('AM.view.transition.transPullHrPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.tranpullhrpage',
    id: 'transPullHrPage',
	layout : 'anchor',
	store: 'AllReportees',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
	defaults     : { flex : 1 }, //auto stretch
	itemId  : 'transPullHrPage',
    initComponent: function() {
		var me = this;
		this.items=[{
			layout: {
                type: 'hbox'
            },
			xtype:'panel',
			width:'60%',
			margin:'0 0 10 0',
			items: [{
				xtype : 'textfield',
				format : AM.app.globals.CommonDateControl,
				fieldLabel : 'Employee ID/Name ',
				anchor : '100%',
				emptyText: 'Enter Employee ID/Name',
				width: 500,
				name : 'resourceTrans_id',
				id : 'resourceTrans_id',
				labelWidth : 150,
				listeners: {
					change: function(field, e) {
						//if(e.getKey() == e.ENTER) {
							me.onClick(me)
						//}
					}
				}
			}, {
				xtype:'tbspacer',
				width: 15
			}, {
				xtype:'button',
				text:'Search',
				width: 70,
				listeners:{
					'click':function(){
						me.onClick(me)
					}
				}
			}]
		
		},{
			xtype:'gridpanel',
			border:true,
			margin:'0 0 10 0',
			selType: 'checkboxmodel',
			selModel: {
				checkOnly: true,
				mode:'MULTI',
				listeners: {
					deselect: function(model, record, index) {
						var myAry = AM.app.globals.transEmployRes;						
						for(var i =0; i < myAry.length; i++)
						{
							if (myAry[i] != null && (myAry[i].data.employee_id === record.data.employee_id))
							{
								myAry[i] = null;
							}
						}
					},
					select: function(model, record, index) {
						if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
						{	
							AM.app.globals.transEmployRes.push(record);
						}
					}
				}
			} ,
			id:'transClntGridID',
			minHeight:300,
			columns: [{
				header: 'Employee ID',  
				dataIndex: 'company_employ_id',
				menuDisabled:true,
				groupable: false,
				draggable: false,
				flex: 2
			},{
            	header: 'Name',  
            	dataIndex: 'FullName',
				menuDisabled:true,
				groupable: false,
				draggable: false,  
            	flex: 3
            },{
            	header: 'Designation',  
            	dataIndex: 'designationName',
				menuDisabled:true,
				groupable: false,
				draggable: false,  
            	flex: 3
            },{
            	header: 'Email', 
        		dataIndex: 'Email',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'Vertical', 
        		dataIndex: 'vertName',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'Reporting To', 
        		dataIndex: 'supervisor',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            },{
            	header: 'Status',
				menuDisabled:true,
				groupable: false,
				draggable: false, 
        		flex: 3
            }],
			bbar: Ext.create('Ext.PagingToolbar', {
				id:'transClntGridPage',
				//store: this.store,
				displayInfo: true,
				displayMsg: 'Displaying employees {0} - {1} of {2}',
				emptyMsg: "No employees to display."
			}),
		}];
		
		this.buttons = [{
			text: 'Next >>',
			name:'clienttothirdpage',
			handler: this.showThirdPage
		}];
        
        this.callParent(arguments);
		//this.plugins
    },
	
	showHomePage: function(){
		AM.app.getController('Transition').viewContent();
	},
	
	showThirdPage: function(btn){
		AM.app.getController('Transition').showTransThirdPage(btn);
	},

}); 