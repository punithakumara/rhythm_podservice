Ext.define('AM.view.transition.transFourthPage' ,{
    extend: 'Ext.form.Panel',
    alias: 'widget.tranfourthpage',
    id: 'transitionFourthPage',
	layout : 'anchor',
    loadMask: true,
    autoCreate: true,
    border : false,
	minHeight:300,
	autoScroll: true,
	width:'99%',
	itemId  : 'transitionFourthPage',
    initComponent: function() {
		var me = this;
		this.items=[{
			layout: {
                align: 'stretch',
                type: 'vbox'
            },
			xtype:'radiogroup',
			columns: 1,
			items: [{
				xtype:'gridpanel',
				border:true,
				id:'transFourthGridID',
				minHeight:300,
				style: 'padding-right: 10px',
				columns: [{
					header: 'Employee ID',  
					dataIndex: 'company_employ_id',
					menuDisabled:true,
					groupable: false,
					draggable: false,
					flex: 3
				},{
					header: 'Name',  
					dataIndex: 'FullName',
					menuDisabled:true,
					groupable: false,
					draggable: false,			
					flex: 3
				},{
					header: 'Designation',  
					dataIndex: 'designationName',
					menuDisabled:true,
					groupable: false,
					draggable: false,  
					flex: 3
				},{
					header: 'Email', 
					dataIndex: 'Email',
					menuDisabled:true,
					groupable: false,
					draggable: false, 
					flex: 3
				}]
				
			},{
				layout: {
					type: 'vbox'
				},
				xtype:'panel',
				width:'99%',
				margin:'10 0 10 0',
				items: [{
					xtype: 'displayfield',
					name : 'FourthPage',
					anchor: '99%',
					width:'60%',
					fieldStyle:'font-size:20px',
					id : 'TransDisplayMsg',
					autoCapitalize : true,
					value : '',
				}]				
			}]		
		}];
		
		this.buttons = [{
			text  : 'Back to People Transition',
			name  : 'firstpage',
			handler: this.showHomepage
	    }];
			
        this.callParent(arguments);
		//this.plugins
    },
	
	showHomepage: function() {
		AM.app.getController('Transition').viewContent();
	}
	
});