Ext.define("AM.view.Viewport", {
    extend: "Ext.container.Viewport",
    layout : {
		type: 'card',
		deferredRender: true
	},
    id: "viewport",
    activeItem: 0,
    items: [ {
        id: "card-0",
        xtype: "container",
        region: "center",
        layout: {
            type: "vbox",
            align: "center"
        },
        style: {
            position: "fixed",
            top: "43%",
            left: "40%"
        },
        region: "center",
        html: '<h1 style="font-size:30px">Theorem Rhythm <br/><img src="./resources/images/loader-img.gif" alt="Loading...." style="margin-left:6%" /></h1>'
    }, {
        id: "card-1",
        xtype: "container",
        layout: "border",
        width: "100%",
        defaults: {
            split: false
        },
        items: [ {
            region: "north",
            bodyStyle: {
                "background-color": "#276193",
                color: "white",
                width: "100%",
                border: "none",
                "float": "left"
            },
            border: "none",
            height: 65,
            items: [ {
                xtype: "container",
                height: 65,
                width: "250px",
                style: {
                    "float": "left",
                    padding: "5px"
                },
                layout: {
                    type: "column",
                    align: "center",
                    pack: "center"
                },
                html: '<img src="./resources/images/rhythm_logo_new.png" alt="Theorem Rhythm"/>'
            }, {
                xtype: "container",
                height: 65,
                width: "75%",
                style: {
                    "float": "left"
                },
                layout: {
                    type: "vbox",
                    align: "left",
                    pack: "end"
                },
                id: "menuToolbar"
            }]
        }, {
            region: "center",
            layout: {
                type: "vbox",
                align: "center"
            },
            width: "104%",
            bodyStyle: {
                border: "none",
                padding: "10px 0"
            },
            style: {
                clear: "both"
            },
            autoScroll: true,
            id: "mainContent"
        }, {
			region : 'south',			
			layout: {
                type: 'vbox',
                align: 'stretch'
            },
			id:"feedback",
			cls:"ui-widget-content",
			// margins: "0 0 0 0",
          margins: "-40 428 -15 410",
		    collapsed: true,
            floatable: true,
			//titleAlign: "center",
            hideCollapseTool: true,
			icon: "./resources/images/feedback_Img.png",
           // scope: this,
			 
            //bodyPadding: 0,
			
		    collapsible: false,
		    preventHeader: true,
            flex: 1,
            margin: 5,                
		 items: [ {
                xtype: "form",
                id: "feedbckfrm",
                bodyStyle: {
                    display: "block",
                    padding: "8px",
					
					
                },
				style: {
                    margin: "0px 0 0px 0",
					background: "none"
                },
                fieldDefaults: {
                    labelAlign: "left",
                    labelWidth: 80,
                    //anchor: "100%"
                },
                items: [ {
                  // margin: "0 10 0 0",
                    xtype: "displayfield",
                    fieldLabel: "Page Name",
                    name: "CurrentPge",
                    flex: 1,
                    id: "CurrentPge"
                }, {
                    xtype: "displayfield",
                    fieldLabel: "User Name",
                    name: "logdUser",
                    flex: 1,
                    id: "logdUser"
                }, {
                    xtype: "textareafield",
                    name: "message",
                    fieldLabel: "Message",
                    anchor: "100%"
                }, {
                    xtype: "container",
                    layout: {
                        type: "vbox",
                        align: "right",
                        anchor: "100%"
                    },
                    width: "100%",
                    items: [ {
                        xtype: "button",
                        text: "Submit",
                        id: "feedBackSubmit",
                        handler: function(obj) {
                            var form   = obj.up("form");
                            formValues = form.getForm().getValues();
                            if ("" == formValues["message"]) Ext.MessageBox.alert("Alert", "Please Type Message"); else AM.app.getController("FeedBack").onCreate(form);
                        }
                    } ]
                } ]
            } ],
			
            listeners: {
            	afterrender: function(p){
                   p.placeholder.getEl().on('click', function(){ p.floatCollapsedPanel()            
                })                
              }
            }
        }]
    } ]
});