Ext.define('AM.view.clientCOR.Add',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	title: 'Add COR Details',
	border : 'true',
    initComponent : function() {

    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addCORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 630,
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'Date of Entry <span style="color:red">*</span>',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					name : 'log_date',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'COR ID <span style="color:red">*</span>',
					value : 'COR123',
					name : 'cor_id',
					submitValue: true
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					readOnly: true,
					fieldLabel: 'Status <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': '0', 'Status': 'Work In Progress'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: '0',
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Client <span style="color:red">*</span>',
					name: 'ClientID',
					store: Ext.getStore('Clients'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Client_Name',
					valueField: 'ClientID',
					emptyText: 'Select Client',
					width: 630,
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'COR Start Date <span style="color:red">*</span>',
					anchor : '100%',
					allowBlank : false,
					emptyText: 'Select Start Date',
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'COR End Date <span style="color:red">*</span>',
					allowBlank : false,
					emptyText: 'Select End Date',
				}]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				items:[{
					xtype : 'panel',
					layout : 'hbox',
					style : {
						'marginBottom' : '5px'
					},
					items : [{
						xtype:'boxselect',
						multiSelect : false,
						fieldLabel: 'WOR ID <span style="color:red">*</span>',
						store: Ext.create('Ext.data.Store', {
							fields: ['Flag', 'Status'],
							data: [
								{'Flag': '0', 'Status': 'WOR100'},
								{'Flag': '0', 'Status': 'WOR200'},
							]
						}),
						queryMode: 'remote',
						minChars:0,
						displayField: 'Status',
						valueField: 'Flag',
						width: 550,
						emptyText: 'Select WOR ID',
					}, {
						xtype:'button',
						text: 'View',
						width: 50,
						style : {
							'marginLeft' : '10px'
						},
					}]
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					store : Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Client Partner',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Engagement Manager',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Delivery Manager',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Change in Model',
					store: Ext.getStore('ChangeModel'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'value',
					valueField: 'id',
					emptyText: 'Select Type',
					width: 630,
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue==1)
							{
								Ext.getCmp('existingModel').setVisible(true);
								Ext.getCmp('newModel').setVisible(true);
							}
							else
							{
								Ext.getCmp('existingModel').setVisible(false);
								Ext.getCmp('newModel').setVisible(false);
							}
						}
					}
				}, {
					xtype : 'boxselect',
					fieldLabel : 'Existing Model <span style="color:red">*</span>',
					multiSelect: true,
					width : 630,
					store : Ext.create('Ext.data.Store', {
						fields: ['Flag', 'value'],
						data: [
							{'id': '1', 'value': 'FTE'},
							{'id': '2', 'value': 'Hourly'},
						]
					}),
					hidden : true,
					id : 'existingModel',
					displayField: 'value',
					valueField: 'id',
					emptyText: 'Select Existing Model',
				}, {
					xtype : 'boxselect',
					fieldLabel : 'New Model <span style="color:red">*</span>',
					multiSelect: true,
					width : 630,
					store : Ext.create('Ext.data.Store', {
						fields: ['Flag', 'value'],
						data: [
							{'id': '3', 'value': 'Volume Based'},
							{'id': '4', 'value': 'Project'},
						]
					}),
					hidden : true,
					id : 'newModel',
					displayField: 'value',
					valueField: 'id',
					emptyText: 'Select New Model',
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1300,
			height: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				items : [{								
					xtype:AM.app.getView('clientCOR.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('clientCOR.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('clientCOR.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('clientCOR.ProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
        }, {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		AM.app.getController('ClientCOR').initCOR();
	},
});