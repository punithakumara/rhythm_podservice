Ext.define('AM.view.clientCOR.ProjectList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.ProjectList',
	title: 'Project Details',
    layout : 'fit',
    selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		deferEmptyText: false
	},
    width : '99%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	listeners : {
		sortchange : function(grid, sortInfo){
			this.getStore().getProxy().extraParams = {'RDR_ID':AM.app.globals.mainClientRDR_ID};
		}
	},
	
	initComponent : function() {
		var me = this;
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true
		});
		
		Ext.apply(this, {
			plugins: [this.editing]
		});
		
		//**** End Editing Plugin ****//
		 
		//**** List View Coloumns ****//
    	this.columns = [{
			header: 'Project Name',
			flex: 1,
			editor: {
				xtype:'textfield'
			}
		}, {
			header: 'Project Details',
			flex: 1,
			editor: {
				xtype:'textfield',
			}
		}, {
			header : 'Shift',
			flex: 1,
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				displayField: 'ShiftCode',
				valueField: 'ShiftCode',
				multiSelect: false,
			}
		}, {
			header : 'Duration - Start',
			flex: 1,
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
			}
		}, {
			header : 'Duration - End',
			flex: 1,
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
			}
		}, {
			header : 'Experience', 
			dataIndex: 'experience',
			flex: 1,
			editor: {
				xtype:'textfield',
			}
		}, {
			header : 'Skills', 
			dataIndex: 'skills',
			flex: 1,
			editor: {
				xtype:'textfield',
			}
		}, {
			header: 'Production Start Date',
			dataIndex: 'start_date',
			flex: 1,
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
			}
		}, {
			header: 'Change Type',
			flex: 1,
			editor: {
				xtype:'combobox',
				name: 'ShiftCode',
				store:'ChangeType',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
			}
		}, {
			header: 'Support Coverage',
			dataIndex: 'support',
			flex: 1,
			editor: {
				xtype:'combobox',
				name: 'ShiftCode',
				store:'SupportCoverageCOR',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
			}
		}, {
			header: 'Job Responsibilities',
			dataIndex: 'responsibilities',
			flex: 1,
			editor: {
				xtype:'combobox',
				name: 'ShiftCode',
				store:'Responsibilities',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
			}
		}];

		//**** End List View Coloumns ****//
		
		//**** Pagination ****//
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            pageSize: 5,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Items to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		//**** Pagination ****//
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'AddProject',
			icon: AM.app.globals.uiPath+'resources/images/add.png',
			handler : function() {
				var r = Ext.create('AM.store.WorkTypeProject');
				me.editing.cancelEdit();
				me.store.insert(0, r);
				me.editing.startEdit(0, 0);
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});