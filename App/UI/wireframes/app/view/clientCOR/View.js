Ext.define('AM.view.clientCOR.View',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	title: 'View COR123 Details',
	border : 'true',
    initComponent : function() {

    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addCORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 630,
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'Date of Entry <span style="color:red">*</span>',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					name : 'log_date',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'COR ID <span style="color:red">*</span>',
					value : 'COR123',
					name : 'cor_id',
				}, {
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Status',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': '0', 'Status': 'Open'},
							{'Flag': '1', 'Status': 'Closed'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: '0',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue==1)
							{
								Ext.getCmp('ViewCORDescription').setVisible(true);
							}
							else
							{
								Ext.getCmp('ViewCORDescription').setVisible(false);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					width : 630,
					hidden : true,
					id : 'ViewCORDescription'
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Client <span style="color:red">*</span>',
					value : 'Fairfax Media',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'COR Start Date <span style="color:red">*</span>',
					value : '24-MAR-2017',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'COR End Date <span style="color:red">*</span>',
					value : '15-MAR-2018',
				}]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				items:[{
					xtype:'displayfield',
					fieldLabel: 'Previous WOR ID',
					value: '<a href="#">WOR100</a>',
					width: 630,
				}, {
					xtype:'displayfield',
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					value: 'Paula Kim',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					value: 'Ashok Srinivas',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					value: 'Ashok Srinivas',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Change in Model <span style="color:red">*</span>',
					value: 'Yes',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Existing Model <span style="color:red">*</span>',
					value: 'FTE',
				}, {
					xtype:'displayfield',
					fieldLabel: 'New Model <span style="color:red">*</span>',
					value: 'Volume Based',
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1300,
			height: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				items : [{								
					xtype:AM.app.getView('clientCOR.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('clientCOR.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('clientCOR.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('clientCOR.ProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
		this.tools = [{
            xtype   : "button",
            icon    : AM.app.globals.uiPath+'resources/images/previous.png',
            text    : 'Previous',
            style   : { 
				"margin-right"  : "8px" 
			},
        }, {
            xtype   : "button",
            text    : 'Next',
            disabled: true,
            icon    : AM.app.globals.uiPath+'resources/images/next.png',
            style   : { 
				"margin-right"  : "8px" 
			},
        }];
		
    	this.buttons = ['->', {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
        }, {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		AM.app.getController('ClientCOR').initCOR();
	},
});