Ext.define('AM.view.clientCOR.List',{
	extend : 'Ext.grid.Panel',
	title: 'Change Order List',
    store: 'ClientCOR',
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
    	this.columns = [ {
			header : 'Change Order ID',
			dataIndex : 'change_order_id',
			flex: 1
		}, {
			header : 'Client', 
			dataIndex : 'client', 
			flex: 1,
		}, {
			header : 'Start Date',
			dataIndex : 'start_date',
			flex: 1,
		}, {
			header : 'End Date',
			dataIndex : 'end_date',
			flex: 1,
		},  {
			header : 'Status', 
			dataIndex : 'status',
			flex: 1
		}, {
			header : 'Raised By', 
			dataIndex : 'raised_by', 
			flex: 1
		}, {
			header : 'Action', 
			width : '8%',
			xtype : 'actioncolumn',
			sortable: false,
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
				getClass: function(v, meta, record) {
					
					if(record.data.Flag !=4)
					{
						return 'rowVisible';
					}
					else
					{ 
						return 'x-hide-display';
					}
				}
			} ,'-', {
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'View',
				handler : this.onView
			}]
		}];
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: 'ClientCOR',
			dock: 'bottom',
			width:450,
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",
			style:{
				border: 'none'
			},
			displayInfo: true,
		}];

		this.callParent(arguments);
	},
	
	tools : [{
		xtype: 'combobox',
		id:'CORStatusComboBox',
		store: Ext.create('Ext.data.Store', {
			fields: ['Flag', 'Status'],
			data: [
				{'Flag': '0', 'Status': 'All'},
				{'Flag': '1', 'Status': 'In-Progress'},
				{'Flag': '2', 'Status': 'Open'},
				{'Flag': '3', 'Status': 'Closed'}
			]
		}),
		displayField: 'Status', 
		valueField: 'Flag',
		value:'1',
	}, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }, {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientCOR').onCreateCOR();
	    },
		text : 'Add COR'
	}],
		
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientCOR').onEditCOR(grid, rowIndex, colIndex);
	},

	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientCOR').onViewCOR(grid, rowIndex, colIndex);
	}
	
});