Ext.define('AM.view.clientCOR.FTEList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.FTEList',
	title: 'FTE Details',
    layout : 'fit',
    selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		forceFit : true,
	},
    width : '99%',
    minHeight : 225,
    loadMask: true, 
    border : true,
	
	initComponent : function() {
		var me = this;
		
		//**** Editing Plugin ****//
		this.editing = Ext.create('Ext.grid.plugin.RowEditing',{
			clicksToEdit: 1,
			pluginId: 'rowEditing',
			saveBtnText: 'Save',
			draggable:true
		});
		
		Ext.apply(this, {
			plugins: [this.editing]
		});
		
		//**** End Editing Plugin ****//
		 
		//**** List View Coloumns ****//
    	this.columns = [{
			header: '# of FTEs',
			flex: 1,
			editor: {
				xtype:'numberfield',
				minValue: 1,
				step: 1,
			}
		}, {
			header : 'Shift', 
			flex: 1,
			editor: {
				xtype:'combobox',
				store: Ext.getStore('Shifts'),
				displayField: 'ShiftCode',
				valueField: 'ShiftCode',
				multiSelect: false,
			}
		}, {
			header : 'Experience', 
			flex: 1,
			editor: {
				xtype:'numberfield',
				minValue: 1,
				step: 1,
			}
		}, {
			header : 'Skills',
			flex: 1,
			editor: {
				xtype:'textfield',
			}
		}, {
			header: 'Production Start Date',
			flex: 1,
			editor: {
				xtype:'datefield',
				format: 'd-M-Y',
			}
		}, {
			header: 'Change Type',
			flex: 1,
			editor: {
				xtype:'combobox',
				name: 'ShiftCode',
				store:'ChangeType',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
			}
		}, {
			header: 'Support Coverage',
			flex: 1,
			editor: {
				xtype:'combobox',
				name: 'ShiftCode',
				store:'SupportCoverageCOR',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
			}
		}, {
			header: 'Job Responsibilities',
			flex: 1,
			editor: {
				xtype:'combobox',
				name: 'ShiftCode',
				store:'Responsibilities',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'id',
				multiSelect: false,
				flex: 1,
			}
		}];

		//**** End List View Coloumns ****//
		
		//**** Pagination ****//
		this.bbar = Ext.create('Ext.toolbar.Paging', {
            displayInfo: true,
            pageSize: 5,
            displayMsg: 'Displaying {0} - {1} of {2}',
            emptyMsg: "No Items to display",
            plugins : [Ext.create('Ext.ux.grid.plugin.PagingToolbarResizer')]
        });
		//**** Pagination ****//
		
		//**** Add Button ****//
		this.tools = [{
			xtype : 'button',
			text: 'Add Request',
			id : 'AddFTE',
			icon: AM.app.globals.uiPath+'resources/images/add.png',
			handler : function() {
				var r = Ext.create('AM.store.WorkTypeFTE');
				me.editing.cancelEdit();
				me.store.insert(0, r);
				me.editing.startEdit(0, 0);
			}					
		}];
		//**** End Add Button ****//
		
		this.callParent(arguments);
	},		
});