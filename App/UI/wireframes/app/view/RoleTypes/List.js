Ext.define('AM.view.RoleTypes.List',{
	extend : 'Ext.grid.Panel',
	title: 'Roles',
	store: Ext.getStore('RoleTypes'),
	layout : 'fit',
	width : '99%',
	minHeight : 200,
	loadMask: true,
	disableSelection: false,
	border : true,
	
	initComponent : function() {
		
		this.store  = Ext.getStore("RoleTypes");
		
		this.columns = [ {
			header : 'Project Name', 
			dataIndex : 'project_name', 
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		},{
			header : 'Role Name',
			dataIndex : 'role_name',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Description',
			dataIndex : 'role_description',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		} ];
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: this.store,
			dock: 'bottom',
			width:450,
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",
			style:{
				border: 'none'
			},
			displayInfo: true,
		}];

		this.callParent(arguments);
	},

	tools : [{
		xtype : 'trigger',
		itemId : 'gridTrigger',
		labelWidth : 60,
		labelCls : 'searchLabel',
		triggerCls : 'x-form-clear-trigger',
		emptyText : 'Search',
		width : 250,
		minChars : 1,
		enableKeyEvents : true,
		onTriggerClick : function(){
			this.reset();
			this.fireEvent('triggerClear');
		}
	}]
	
	/*tools : [{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientHolidays').onCreateHoliday();
	    },
		text : 'Add Client Holiday'
	}],*/
	
});