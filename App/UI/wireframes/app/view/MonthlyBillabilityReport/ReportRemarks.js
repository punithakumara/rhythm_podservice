Ext.define('AM.view.MonthlyBillabilityReport.ReportRemarks',{
	extend : 'Ext.window.Window',
	alias : 'widget.ReportRemarks',
	layout: 'fit',
	//width : '99%',
	layout: {
        type: 'vbox',
    },
	store: 'Remarks',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '5px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
    	this.items = [{
			xtype:'gridpanel',
			border:true,
			margin:'0',
			minHeight: 100,
			width:250,
			id: 'remarks_grid',
			columns : [{
				header: 'Change Effective Date',  
				dataIndex: 'change_date',  
				flex: 2,
				sortable: false,
				menuDisabled:true,
				draggable: false
			}, {
				header: 'Count',  
				dataIndex: 'count',  
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false
			}],
		}];
    	
		this.callParent(arguments);
	},		
	
});