Ext.define("AM.view.MonthlyBillabilityReport.HourlyReport", {
    extend: "Ext.grid.Panel",
    alias: "widget.HourlyReport",
    title: "Monthly Billability Report - Hourly & Volume",
    layout: "fit",
    loadMask: true,
    store: Ext.getStore('HourlyReport'),
    id: "HourlyReport",
    frame: true,
    width: "99%",
    minheight: 540,
    emptyText: '<div align="center">No Data to display</div>',
    viewConfig: {
        deferEmptyText: false,
        getRowClass: function(record) {
            return record.get('shiftHighlight') ? 'highlightMBR' : '';
        },
    },
	features: [{
		id: 'group',
		ftype: 'groupingsummary',
		groupHeaderTpl: '{name}',
		hideGroupedHeader: true,
		enableGroupingMenu: false
	}],
    resizable: false,
    style: {
        border: "1px solid #157fcc",
    },
    initComponent: function() {
    	var grid = this;
        var me = this;

		this.store  = Ext.getStore("HourlyReport");
		this.columns = [{
            text: "Client",
            width:200,
            hidden: false,
            sortable: false,
            menuDisabled:true,
            draggable: false,
			locked: true,
            dataIndex : 'responsibilities',
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return "<b>Total<b>"
            }
        }, {
			header : 'Minimum Hours',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'hourly_min',
			tooltip : 'Minimum Hours',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return 0;
				}
				else
				{
					return value;
				}
			},
		}, {
			header : 'Maximum Hours',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'hourly_max',
			tooltip : 'Maximum Hours',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return 0;
				}
				else
				{
					return value;
				}
			},
		},{
			header : 'Minimum Vol',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'volume_min',
			tooltip : 'Minimum Vol',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return 0;
				}
				else
				{
					return value;
				}
			},
		}, {
			header : 'Maximum Vol',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'volume_max',
			tooltip : 'Maximum Vol',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return 0;
				}
				else
				{
					return value;
				}
			},
		},{
			header : 'Billable Hours',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'hourly_billable',
			tooltip : 'Billable Hours',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return '0';
				}
				else
				{
					return value;
				}
			},
		},{
			header : 'Weekend Coverage',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'weekend_coverage',
			tooltip : 'Weekend Coverage',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return '0';
				}
				else
				{
					return value;
				}
			},
		},{
			header : 'Holiday Coverage',
			width:164,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'holiday_coverage',
			tooltip : 'Holiday Coverage',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
			renderer: function(value, meta, record, row, col) {
				if(value == '' || value < 0 )
				{
					return '0';
				}
				else
				{
					return value;
				}
			},
		}];
		
        this.bbar = Ext.create("Ext.toolbar.Paging", {
            store: this.store,
            displayInfo: true,
            cls: "peopleTool",
            prependButtons: false,
            displayMsg: "Total {2} Records",
            emptyMsg: "No Items to display"
        });
		
        this.tools = [{
			xtype: 'monthfield',
			submitFormat: 'Y-m-d',
			name: 'mbrmonth',
			fieldLabel: 'Month',
			id: 'mbrmonth',
			format: 'F, Y',
			labelCls : 'searchLabel',
			width: 250,
			listeners: {
				'focus': function(me) {
					this.onTriggerClick();
				},
				select:function (v){
					AM.app.getController('MonthlyBillabilityReport').mbrFun(me,Ext.getCmp('mbrmonth').getValue(),v.getValue(),false);
				}
			}
		}, {
			xtype: "tbseparator",
			style: {
				"margin-right": "10px"
			}
		}, {
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 40,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search Client',
			width : 200,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
   	 	}, {
            xtype: "tbseparator",
            style: {
                "margin-right": "10px",
            }
        }, {
        	xtype: "button",
        	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
    		text : 'Export To Excel',
    		style: {
                "margin-right": "8px"
            },
			handler: function(obj){
        		AM.app.getController('MonthlyBillabilityReport').downloadCSV();
    		}
    	}];
		
        this.callParent(arguments);
    },
});
