Ext.define("AM.view.MonthlyBillabilityReport.FteReport", {
    extend: "Ext.grid.Panel",
    alias: "widget.FteReport",
    title: "Monthly Billability Report - FTE",
    layout: "fit",
    loadMask: true,
    store: Ext.getStore('FteReport'),
    id: "FteReport",
    frame: true,
    width: "99%",
    minheight: 540,
    emptyText: '<div align="center">No Data to display</div>',
    viewConfig: {
        deferEmptyText: false,
        getRowClass: function(record) {
            return record.get('shiftHighlight') ? 'highlightMBR' : '';
        },
    },
	features: [{
		id: 'group',
		ftype: 'groupingsummary',
		groupHeaderTpl: '{name}',
		hideGroupedHeader: false,
		enableGroupingMenu: false
	}],
    resizable: false,
    style: {
        border: "1px solid #157fcc",
    },
    initComponent: function() {
    	var grid = this;
        var me = this;

		this.store  = Ext.getStore("FteReport");
		this.columns = [{
            text: "Client",
            width: 230,
            hidden: false,
            sortable: false,
            menuDisabled:true,
            draggable: false,
			locked: true,
            dataIndex : 'responsibilities',
			style: {
				'font-size': '11px'
			},
			summaryRenderer: function(value, summaryData, dataIndex) {
				return "<b>Total<b>"
            }
        }, {
			header: "Approved FTE",
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "fte_total_hc",
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		}, {
			header: "Full Month FTE",
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "monthly_fte",
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		}, {
			header: "Prorated FTE",
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "prorated_fte",
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header: "Approved Buffer",
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			dataIndex: "fte_total_buffer",
			value: 0,         
			style: {
				'font-size': '11px'
			},
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'APAC',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'fte_apac',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'LEMEA',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'fte_lemea',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		}, {
			header : 'India <br>(IST, EMEA)',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'india',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'USA  <br> (PST, CST, EST) ',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			dataIndex : 'usa',
			summaryType: 'sum',
			summaryRenderer: function(value, summaryData, dataIndex) {
				return value;
			},
		},{
			header : 'Remarks',
			flex: 1,
			sortable: false,
			menuDisabled:true,
			draggable: false,
			hideable: false,
			xtype : 'actioncolumn',
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'View',
				handler: function(){
					AM.app.getController('MonthlyBillabilityReport').viewRemarks();
				}
			}]
		}];
		
        this.bbar = Ext.create("Ext.toolbar.Paging", {
            store: this.store,
            displayInfo: true,
            cls: "peopleTool",
            prependButtons: false,
            displayMsg: "Total {2} Records",
            emptyMsg: "No Items to display"
        });
		
        this.tools = [{
			xtype: 'monthfield',
			submitFormat: 'Y-m-d',
			name: 'mbrmonth',
			fieldLabel: 'Month',
			id: 'mbrmonth',
			format: 'F, Y',
			labelCls : 'searchLabel',
			width: 250,
			listeners: {
				'focus': function(me) {
					this.onTriggerClick();
				},
				select:function (v){
					AM.app.getController('MonthlyBillabilityReport').mbrFun(me,Ext.getCmp('mbrmonth').getValue(),v.getValue(),false);
				}
			}
		}, {
			xtype: "tbseparator",
			style: {
				"margin-right": "10px"
			}
		}, {
			xtype : 'trigger',
			itemId : 'gridTrigger',
			fieldLabel : '',
			labelWidth : 40,
			labelCls : 'searchLabel',
			triggerCls : 'x-form-clear-trigger',
			emptyText : 'Search Client',
			width : 200,
			minChars : 1,
			enableKeyEvents : true,
			onTriggerClick : function(){
				this.reset();
				this.fireEvent('triggerClear');
			}
   	 	}, {
            xtype: "tbseparator",
            style: {
                "margin-right": "10px",
            }
        }, {
        	xtype: "button",
        	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
    		text : 'Export To Excel',
    		style: {
                "margin-right": "8px"
            },
			handler: function(obj){
        		AM.app.getController('MonthlyBillabilityReport').downloadCSV();
    		}
    	}];
		
        this.callParent(arguments);
    },
});
