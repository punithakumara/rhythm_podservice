Ext.define('AM.view.Menu', { 
	extend : 'Ext.toolbar.Toolbar',
	alias : 'widget.menu',
	id: 'ExtheaderMenuToolbar',
	cls: 'appMenu',
   // margin: '0 0 10 0',
    style: {'padding-bottom':'0', background: '#276193'},
    transitionType    : 'slide',
	delay        : 0.9,
	animate: true,
	initComponent : function() {

		var me = this;
		this.items = [];
		
		
		/* Home Menu starts here */
		
		me.items.push({text: 'Home', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'homeIcon',
			listeners : {
				'click':function() {
					AM.app.getController('Login').userSppecificDashboard();
					AM.app.getController('Menu').viewDashboard();
				},
			},
		});
	
		/* Home Menu ends here */
	

		/* A/C Management Menu starts */
		var AccManagementSubmenu = Ext.create('Ext.menu.Menu');
		
		me.items.push({text : 'A/C Management', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'otherAppsIcon otherAppsIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu: AccManagementSubmenu
		});

		AccManagementSubmenu.add({text: 'Work Orders', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
		group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/menu/wor.png',
			listeners : {
				click: function(){
					AM.app.getController('Menu').worDashboardList();
				},
			}
		});
		
		AccManagementSubmenu.add({text: 'Change Orders', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, 
		group: 'theme', icon: AM.app.globals.uiPath+'/resources/images/cor.png',
			listeners : {
				click: function(){
					AM.app.getController('Menu').corDashboardList();
				},
			}
		});
		
		AccManagementSubmenu.add({text: 'People Billability', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon peplBillabilityIcon', 
			listeners: {
				click: function(){
					AM.app.getController('Menu').categorizationList();
				},
			}
		});
		
		
		var IncidentSubmenu = Ext.create('Ext.menu.Menu');
	
		IncidentSubmenu.add({text: 'Appreciation', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon appreciationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').appreciationList();
				},
			}
		});
		
		IncidentSubmenu.add({text: 'Escalation', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon escalationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').escalationList();
				}
			}
		});
		
		IncidentSubmenu.add({text: 'External Error', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon errorIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').errorList();
				}
			}
		});
		
		AccManagementSubmenu.add({text: 'Incident Log', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'opsIcon incidentlogIcon', group: 'theme',
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu:IncidentSubmenu
		});

		var ConfigurationSubmenu = Ext.create('Ext.menu.Menu');

		ConfigurationSubmenu.add({text: 'Time Entry Fields', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon appreciationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').timeEntryFields();
				},
			}
		});

		ConfigurationSubmenu.add({text: 'WOR Types', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon appreciationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').worTypes();
				},
			}
		});

		ConfigurationSubmenu.add({text: 'Projects', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon appreciationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').projectTypes();
				},
			}
		});

		ConfigurationSubmenu.add({text: 'Roles', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon appreciationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').roleTypes();
				},
			}
		});

		AccManagementSubmenu.add({text: 'Configurations', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'opsIcon incidentlogIcon', group: 'theme',
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu:ConfigurationSubmenu
		});
		
		/* A/C Management Menu ends */

		
		
		/* Operations sub menu starts */
		
		var OperationsSubmenu = Ext.create('Ext.menu.Menu');
		
		/*OperationsSubmenu.add({text: 'Onboarding', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon onboardingIcon', 
			listeners: {
				click: function(){
					AM.app.getController('Menu').onBoardList();
				}
			}
		});*/
		
		var TimesheetSubmenu = Ext.create('Ext.menu.Menu');
	
		TimesheetSubmenu.add({text: 'Add Time', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'addTimeIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').timesheetList();
				},
			}
		});		
		
		TimesheetSubmenu.add({text: 'Approve Timesheet', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'approveTimeIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').approveTimesheetList();
				},
			}
		});	
		
		TimesheetSubmenu.add({text: 'Dashboard', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'dashboardTimeIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').timesheetDashboard();
				},
			}
		});
		
		OperationsSubmenu.add({text: 'Theorem Timesheet', checked: false, style:{width:'168px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'timesheetIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu:TimesheetSubmenu
		});
		
		

		var MetricsSubmenu = Ext.create('Ext.menu.Menu');
		
		MetricsSubmenu.add({text: 'Add Utilization', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'opsIcon utilisationIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').utilizationList();
				}
			}
		});
		
		MetricsSubmenu.add({text: 'Approve Utilization', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'opsIcon metricDashboardIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').insightList();
				},
			}
		});	
		
		/* MetricsSubmenu.add({text: 'Accuracy', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'opsIcon accuracyIcon', 
			listeners:{
				click: function(){
					AM.app.getController('Menu').accuracyList();
				}
			}
		}); */
		
		OperationsSubmenu.add({text: 'Client Utilization', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, iconCls : 'opsIcon metricsIcon', group: 'theme',
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu:MetricsSubmenu
		});

		me.items.push({text : 'Time Management', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},
		iconCls : 'opsIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu: OperationsSubmenu
		});
		
		
		var ResourcesSubmenu = Ext.create('Ext.menu.Menu');
		
		
		var transSubmenu = Ext.create('Ext.menu.Menu');
		transSubmenu.add({text: 'Shift Transition', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'ShiftTransitionIcon',
			listeners: {
				click: function(){
					AM.app.getController('Menu').shiftTransPage();
				},
			}
		});
		transSubmenu.add({text: 'Supervisor Transition', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'SupervisorTransitionIcon',
			listeners: {
				click: function(){
					AM.app.getController('Menu').leadTransPage();
				},
			}
		});
		transSubmenu.add({text: 'Vertical Transition', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'SupervisorTransitionIcon',
			listeners: {
				click: function(){
					AM.app.getController('Menu').leadTransPage();
				},
			}
		});
		
		ResourcesSubmenu.add({text: 'Transitions', checked: false, style:{width:'140px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon peplTransitionIcon', 
			menu: transSubmenu
		});
		
		ResourcesSubmenu.add({text: 'RMG', checked: false, style:{width:'140px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon peplTransitionIcon', 
		});
		
		
		ResourcesSubmenu.add({text: 'Trainings', checked: false, style:{width:'140px', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'opsIcon peplTransitionIcon', 
		});
		
		me.items.push({text : 'Resource Management', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},
		iconCls : 'opsIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu: ResourcesSubmenu
		});
		
		/* Operations sub menu ends */		
		
		
		
		/* Master Data Menu starts */
		
		var MasterDataSubmenu = Ext.create('Ext.menu.Menu');

		MasterDataSubmenu.add({text: 'Verticals', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'mstrDataIcon verticalIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').verticalList();
				},
			}
		});	
	
		MasterDataSubmenu.add({text: 'Clients', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'mstrDataIcon clientIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').clientList();
				},
			}
		});	
	
		
		MasterDataSubmenu.add({text: 'Designations', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon designationIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').designationList();
				},
			}
		});	

		MasterDataSubmenu.add({text: 'Employees', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'mstrDataIcon employeeIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').empList();
				},
			}
		});	

		MasterDataSubmenu.add({text: 'Technologies', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon technologiesIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').techList();
				},
			}
		});	

		/*MasterDataSubmenu.add({text: 'Attributes', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon attributeIcon',
			listeners : {
				click: function(){
					AM.app.getController('Menu').attributesList();
				},
			}
		});	*/	

		MasterDataSubmenu.add({text: 'Client Holidays', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'mstrDataIcon clientholiday',
			listeners : {
				click: function(){
					AM.app.getController('Menu').clientHolidaysList();
				},
			}
		});		


		me.items.push({text: 'Master Data', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'mstrDataIcon',
			listeners : {
				mouseover : function() {
					this.showMenu();
				},

			},
			menu:MasterDataSubmenu
		});
		
		/* Master Data Menu ends */
		
			
		var reportsMenu = Ext.create('Ext.menu.Menu');
	
		/*reportsMenu.add({text: 'Consolidated Utilization Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'reportsMenuIcon consolidatedUtilization',
			listeners:{
				click: function(){
					AM.app.getController('Menu').ConsolidatedUtilizationReportList();
				}
			}
		});
		
		reportsMenu.add({text: 'Monthly Billability Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'reportsMenuIcon acMngFRIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').monthlyBillabilityReportPage();
				},
				afterRender: function() {
					if(Ext.util.Cookies.get('grade')>=4)
					{
						this.show();
					}
					else
					{
						this.hide();
					}
				}
			}
		});*/
		
		reportsMenu.add({text: 'Employee Dashboard', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', 
		iconCls : 'reportsMenuIcon hierarchyIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').OpsDashboard();
				},
			}
		});
		
		var billabilitySubmenu = Ext.create('Ext.menu.Menu');
	
		billabilitySubmenu.add({text: 'FTE Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon ftereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').monthlyBillabilityFteReportPage();
				},
			}
		});		
		
		billabilitySubmenu.add({text: 'Hourly & Volume Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'acMngIcon reportsMenuIcon volumereport',
			listeners:{
				click: function(){
					AM.app.getController('Menu').monthlyBillabilityReportPage();
				},
			}
		});
		
		reportsMenu.add({text: 'Monthly Billability Report', checked: false, style:{width:'auto', padding:'6px', height:'30px'}, group: 'theme', iconCls : 'reportsMenuIcon acMngFRIcon',
			menu:billabilitySubmenu,
		});
		
		me.items.push({text : 'Reports', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'reportsMenuIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				},
			},
			menu: reportsMenu
		});
		
		
		/* Other App sub menu starts */
		
		var documentsSubMenu = Ext.create('Ext.menu.Menu');
		documentsSubMenu.add({text : 'Company Documents', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, group: 'theme',iconCls : 'companyDocIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').companyDocumentsPage();
				},
			}
		});
		documentsSubMenu.add({text : 'Vertical Documents', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, group: 'theme',iconCls : 'verticalDocIcon',
			listeners:{
				click: function(){
					AM.app.getController('Menu').verticalDocumentsPage();
				},
			}
		});
		
		var OtherAppSubmenu = Ext.create('Ext.menu.Menu');

		OtherAppSubmenu.add({text : 'Documents', checked: false, style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'}, group: 'theme',iconCls : 'otherAppsIcon docmtIcon',
			menu: documentsSubMenu
		});
		
		me.items.push({text : 'Other Apps', xtype: 'button', style:{width:'auto', height:'30px', padding:'6px', 'border-radius':'0px'},iconCls : 'otherAppsIcon otherAppsIcon', 
			listeners : {
				mouseover : function() {
					this.showMenu();
				}
			},
			menu: OtherAppSubmenu
		});	
		
		/* Othere App sub Menu ends */
	
		this.callParent(arguments);
	
	}
 });