Ext.define('AM.view.clientWOR.AssignClientHolidays',{
	extend : 'Ext.window.Window',
	alias : 'widget.AssignClientHolidays',
	requires: ['Ext.ux.MultiSelect'],
	layout: 'fit',
	layout: {
        type: 'vbox',
    },
	// store: 'Remarks',
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	border : 'true',
	width: 520,
	height: 600,
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '5px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
		
		var projectCodeStore = Ext.create('Ext.data.Store', {
            fields: ['name'],
            data: [
				{'name': 'PANRA_H_EST_ADQM_MF'}, 
				{'name': 'PANRA_H_LEMEA_ADCS_MF'},
				{'name': 'PANRA_H_PST_ADCM_MF'},
				{'name': 'PANRA_H_CST_ADTT_MF'}
			]
        });
		
		var holidayStore = Ext.create('Ext.data.Store', {
            fields: ['name', 'id'],
            data: [
				{'name': 'Presidents Day - Feb 19, 2018', 'id': '1'},
				{'name': 'Memorial Day - May 28, 2018', 'id': '2'},
				{'name': 'Independence Day - Jul 04, 2018', 'id': '3'},
				{'name': 'Labor Day - Sep 03, 2018', 'id': '4'},
			]
        });
    	
    	this.items = [{
			xtype : 'form',
			itemId : 'assgnHolidayFrm',
			fieldDefaults: {
				labelWidth: 100,
				anchor: '100%',
				labelAlign: 'top'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			layout: 'hbox',
			items : [{
				xtype: 'multiselect',
                name: 'ComboMulti',
                store: projectCodeStore,
                displayField: 'name',
                valueField: 'name',
				fieldLabel : 'Project Code <span style="color:red">*</span>',
				labelStyle: 'padding:0 0 5px 0',
				anchor: '100%',
				margin: '0 0 5px 0',
				width: 230,
				height: 160,
			},{
				xtype: 'multiselect',
                name: 'ComboMulti',
                store: holidayStore,
                displayField: 'name',
                valueField: 'id',
				fieldLabel : 'Holiday List <span style="color:red">*</span>',
				labelStyle: 'padding:0 0 5px 0',
				anchor: '100%',
				margin: '0 0 5px 20px',
				width: 230,
				height: 160,
			}]
		},{
			layout: {
				type: 'hbox',
				pack: 'center',
			},
			width: 490,
			style:{
				margin: '0 0 15px 0',
			},
			items: [{
				xtype: 'tbfill'
			},{
				xtype: 'button',
				text : 'Cancel',
				icon : AM.app.globals.uiPath+'resources/images/cancel.png',
				handler : this.onCancel
			}, {
				xtype: 'button',
				text : 'Save',
				margin: '0 0 0 10px',
				icon : AM.app.globals.uiPath+'resources/images/save.png',
			}]
		},{
			xtype:'gridpanel',
			border:true,
			margin:'0',
			height:320,
			width:'99%',
			store: Ext.getStore('clientHolidayDetails'),
			id: 'holidays_list',
			features: [{
				id: 'group',
				ftype: 'groupingsummary',
				groupHeaderTpl: '{name}',
				hideGroupedHeader: true,
				enableGroupingMenu: false,
				startCollapsed: true,
			}],
			columns:[{
				header : 'Holiday',
				dataIndex : 'holiday',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
			}, {
				header : 'Project Code',
				dataIndex : 'project_code', 
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
			}, {
				header : 'Date',
				dataIndex : 'date',
				flex: 1,
				sortable: false,
				menuDisabled:true,
				draggable: false,
			}]
		}];
		
		this.callParent(arguments);
	},	
	
	onCancel : function() {
		this.up('window').close();
	}	
	
});