Ext.define('AM.view.clientWOR.Edit',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
        type: 'vbox',
        align: 'center'
    },
	title: 'Edit WOR Details',
	border : 'true',
    initComponent : function() {

    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addWORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 630,
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'Date of Entry <span style="color:red">*</span>',
					value : Ext.Date.format(new Date(), AM.app.globals.CommonDateControl),
					name : 'log_date',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel : 'WOR ID <span style="color:red">*</span>',
					value : 'WOR123',
					name : 'wor_id',
					submitValue: true
				}, {
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Status',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': '0', 'Status': 'Work In Progress'},
							{'Flag': '1', 'Status': 'Delete'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: '0',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue==1)
							{
								Ext.getCmp('WORDescription').setVisible(true);
							}
							else
							{
								Ext.getCmp('WORDescription').setVisible(false);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					width : 630,
					hidden : true,
					id : 'WORDescription'
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'WOR Start Date <span style="color:red">*</span>',
					anchor : '100%',
					allowBlank : false,
					value : '24-MAR-2017',
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'WOR End Date <span style="color:red">*</span>',
					allowBlank : false,
					value : '15-MAR-2018',
				}]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				items:[{
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Previous WOR ID',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
							{'Flag': '0', 'Status': 'WOR100'},
							{'Flag': '0', 'Status': 'WOR200'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: '0',
					width: 630,
				}, {
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'ClientID',
					store: Ext.getStore('Clients'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Client_Name',
					valueField: 'ClientID',
					emptyText: 'Select Client',
					width: 630,
					value : '236',
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					store : Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Client Partner',
					value: '1689',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Engagement Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					value: '2155',
					emptyText: 'Select Engagement Manager',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Delivery Manager',
					value: '2155',
					width: 630,
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}]
			}]
    	}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1300,
			height: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				items : [{								
					xtype:AM.app.getView('clientWOR.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('clientWOR.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('clientWOR.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('clientWOR.ProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
    	this.buttons = ['->', {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
        }, {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		AM.app.getController('ClientWOR').initWOR();
	},
});