Ext.define('AM.view.clientWOR.View',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
		type: 'vbox',
		align: 'center'
	},
	title: 'View WOR123 Details',
	border : 'true',
	initComponent : function() {

		this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addWORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '49%',
				},
				width: 630,
				title:"WOR Information",
				items:[{
					xtype:'panel',
					layout : 'hbox',
					margin: '0 0 5 0',
					/*items: [{
						xtype : 'displayfield',
						fieldLabel : 'WOR ID & Name <span style="color:red">*</span>',
						value : 'WOR'+Ext.Date.format(new Date(), 'yhis')+' & Work_Order_Name',
						name : 'wor_id',
						submitValue: true
					},{
						xtype : 'displayfield',
						margin: '0 0 0 10',
						width: 366,
					}]*/
				},{
					xtype : 'displayfield',
					fieldLabel : 'WOR ID <span style="color:red">*</span>',
					value : 'WOR'+Ext.Date.format(new Date(), 'yhis'),
					name : 'wor_id',
					submitValue: true
				},{
					xtype : 'displayfield',
					fieldLabel : 'WOR Name <span style="color:red">*</span>',
					value : 'Work_Order_Name',
					name : 'wor_id',
					submitValue: true
				}, {
					xtype : 'displayfield',
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					value : 'Advance Digital',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'WOR Type <span style="color:red">*</span>',
					value : 'Digital Ops',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'WOR Start Date <span style="color:red">*</span>',
					value : '24-MAR-2017',
				}, {
					xtype : 'displayfield',
					fieldLabel : 'WOR End Date <span style="color:red">*</span>',
					value : '15-MAR-2018',
				}, {
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Status',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
						{'Flag': '0', 'Status': 'Open'},
						{'Flag': '1', 'Status': 'Closed'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: '0',
					listeners	: {
						select : function(queryEvent) {
							if(queryEvent.lastValue==1)
							{
								Ext.getCmp('ViewWORDescription').setVisible(true);
							}
							else
							{
								Ext.getCmp('ViewWORDescription').setVisible(false);
							}
						}
					}
				}, {
					xtype : 'textareafield',
					fieldLabel : 'Description <span style="color:red">*</span>',
					width : 630,
					hidden : true,
					id : 'ViewWORDescription'
				}]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				width: 630,
				title:"People Information",
				items:[{
					xtype:'displayfield',
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					value: 'Paula Kim',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Eng. Manager <span style="color:red">*</span>',
					value: 'Vineet Singh',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					value: 'Ilananal Arunachalam',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Associate Manager',
					value: '',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Team Lead',
					value: '',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Stake Holder',
					value: '',
				}, {
					xtype:'displayfield',
					fieldLabel: 'Time Entry Approver',
					value: '',
				}]
			}]
		}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1300,
			height: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'Summary',
				items : [{								
					xtype:AM.app.getView('clientWOR.SummaryList').create(),
					hidden:false
				}]
			}, {
				title: 'FTE',
				items : [{								
					xtype:AM.app.getView('clientWOR.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('clientWOR.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('clientWOR.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('clientWOR.ProjectList').create(),
					hidden:false
				}]
			}]
		}];

		this.callParent(arguments);
	},
	
	tools : [{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientWOR').onAssignResources();
		},
		tooltip : 'Assign Employee',
	},/*{
		xtype: 'tbspacer',
		width: 10
	},{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/delete.png',
		handler: function () {
			AM.app.getController('ClientWOR').onAssignResources();
	    },
		tooltip : 'Remove Resources',
	},{
		xtype: 'tbspacer',
		width: 10
	},*/{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/view.png',
		handler: function () {
			AM.app.getController('ClientWOR').onViewResources();
		},
		tooltip : 'View Employee',
	},{
		xtype: 'tbspacer',
		width: 10
	},{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientWOR').onConfigureAttributes();
		},
		tooltip : 'Configure Time Entry Fields'
	},{
		xtype: 'tbspacer',
		width: 10
	},{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/booking.png',
		handler: function () {
			AM.app.getController('ClientWOR').onAddHoliday();
		},
		tooltip : 'Assign Client Holiday'
	},{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		text: "COR",
		tooltip : 'Add COR'
	},{
		xtype: 'tbspacer',
		width: 10
	}],

	onCancel : function() {
		AM.app.getController('ClientWOR').initWOR();
	},
});