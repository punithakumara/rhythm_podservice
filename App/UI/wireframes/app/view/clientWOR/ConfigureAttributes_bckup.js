Ext.define('AM.view.clientWOR.ConfigureAttributes',{
	extend : 'Ext.window.Window',
	alias : 'widget.ConfigureAttributes',
	layout: 'fit',
	width : '70%',
	height: 555,
	autoScroll: true,
    autoHeight : true,   
    autoShow : true,
    modal: true,
	title: 'Configure Time Entry Fields',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
    initComponent : function() {

    	var me = this, intialStore = "";
    	
		this.items = [{
			xtype:'gridpanel',
			border:true,
			margin:'0 0 5 0',
			height:415,
			store: Ext.create('Ext.data.Store', {
				fields: ['AttributeName','AttributesDescription','Property','Mandatory','Visible'],
				data: [
					{'AttributeName': 'Biilable Hrs', 'AttributesDescription':'asfasdf', 'Property':'Default','Mandatory':false,'Visible':true},
					{'AttributeName': '# of Lists', 'AttributesDescription':'asdfd', 'Property':'Additional','Mandatory':false,'Visible':false},
					{'AttributeName': 'Campaign Start Date', 'AttributesDescription':'sdfd',  'Property':'Additional','Mandatory':false,'Visible':false},
				]
			}),
			columns: [{
				header: 'Field Name',  
				dataIndex: 'AttributeName',  
				flex: 3,
			},{
				header: 'Field Description',  
				dataIndex: 'AttributesDescription',  
				flex: 3,
			},{
				header: 'Field Property', 
				dataIndex: 'Property', 
				flex: 3,
			},{
				xtype: 'checkcolumn',
                text: 'Mandatory',
                dataIndex:'Mandatory',
                listeners: {
                    checkChange: function(column, rowIndex, checked, eOpts) {
						return false;
					}
                }
			},{
				xtype: 'checkcolumn',
                text: 'Visible',
                dataIndex:'Visible',
                listeners: {
                    checkChange: function(column, rowIndex, checked, eOpts) {
						return false;
					}
                }
			}],
		}];
		
    	this.buttons = [{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			id:'addSubmit',
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},
	
});