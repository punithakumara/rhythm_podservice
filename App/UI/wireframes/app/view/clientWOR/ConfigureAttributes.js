Ext.define('AM.view.clientWOR.ConfigureAttributes',{
	extend : 'Ext.window.Window',
	alias : 'widget.ConfigureAttributes',
	layout: 'fit',
	// store: 'AllReportees',
	width : '90%',
	height: 555,
	autoScroll: true,
	autoHeight : true,   
	autoShow : true,
	modal: true,
	title: 'Configure Time Entry Fields',
	border : 'true',
	bodyStyle : {
		display : 'block',
		float : 'left',
		padding : '10px',
	},
	initComponent : function() {

		var me = this, intialStore = "";
		
		this.items = [{
			xtype:'container',
			layout: {
				type: 'border',
			},
			items:[{
				region:'west',
				// xtype: 'insightTopPanel',
				width: '20%',
				border: true,
				height: 400,
				margin : '0 5px 5px 0',
				layout: {
					type: 'vbox',
				},
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'Client <span style="color:red">*</span>',
					width: 220,
					name : 'resource_id',
					labelAlign: 'top',
					value : 'Advance Digital',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'displayfield',
					fieldLabel : 'WOR ID <span style="color:red">*</span>',
					width: 220,
					name : 'resource_id',
					labelAlign: 'top',
					value : 'WOR'+Ext.Date.format(new Date(), 'yhis'),
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				},{
					xtype : 'boxselect',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Project <span style="color:red">*</span>',
					anchor : '100%',
					width: 220,
					name : 'resource_id',
					labelAlign: 'top',
					labelWidth : 150,
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
						{'Flag': '0', 'Status': 'AdTech'},
						{'Flag': '0', 'Status': 'Martech'},
						{'Flag': '0', 'Status': 'PEG'},
						]
					}),
					displayField: 'Status',
					valueField: 'Flag',
				},{
					xtype : 'displayfield',
					fieldLabel : '# of Roles under this project',
					width: 220,
					name : 'resource_id',
					labelAlign: 'top',
					value : 'Internal Tools, QA, Developer',
					style:{
						'position':'absolute',
						'margin':'10px 0 0 5px'
					},
				}]
			}, {
				region:'center',
				width: '69%',
				items:[{
					xtype : 'form',
					layout: 'hbox',
					id : 'addResFrm',
					items:[{
						xtype : 'textfield',
						format : AM.app.globals.CommonDateControl,
						// fieldLabel : 'Employee ID/Name ',
						anchor : '100%',
						width: 500,
						name : 'resource_id',
						id : 'resource_id',
						labelWidth : 150,
						listeners: {
							change: function(field, e) {
								//if(e.getKey() == e.ENTER) {
									me.onClick(me)
								//}
							}
						}
					}, {
						xtype:'tbspacer',
						width: 15
					}, {
						xtype:'button',
						text:'Search',
						id:'transSecondSearch',
						width:60,
						listeners:{
							'click':function(){
								me.onClick(me)
							}
						}
					}]
				},{
					xtype:'gridpanel',
					border:true,
					margin:'10 0 5 0',
					height:415,
					// id:'WORtransGridID',
					selType: 'checkboxmodel',
					selModel: {
						checkOnly: true,
						mode:'MULTI',
						listeners: {
							deselect: function(model, record, index) {
								var myAry = AM.app.globals.transEmployRes;						
								for(var i =0; i < myAry.length; i++)
								{
									if (myAry[i] != null && (myAry[i].data.employee_id == record.data.employee_id))
									{
										myAry[i] = null;
									}
								}
							},
							select: function(model, record, index) {
								if(!(Ext.Array.contains(AM.app.globals.transEmployRes,record)))
								{							
									AM.app.globals.transEmployRes.push(record);
								}
							}
						}
					} ,
					store: Ext.create('Ext.data.Store', {
						fields: ['AttributeName','AttributesDescription','Property','Mandatory','Visible'],
						data: [
						{'AttributeName': 'Biilable Hrs', 'AttributesDescription':'asfasdf', 'Property':'Default','Mandatory':false,'Visible':true},
						{'AttributeName': '# of Lists', 'AttributesDescription':'asdfd', 'Property':'Additional','Mandatory':false,'Visible':false},
						{'AttributeName': 'Campaign Start Date', 'AttributesDescription':'sdfd',  'Property':'Additional','Mandatory':false,'Visible':false},
						]
					}),
					columns: [{
						header: 'Field Name',  
						dataIndex: 'AttributeName',  
						flex: 3,
					},{
						header: 'Field Description',  
						dataIndex: 'AttributesDescription',  
						flex: 3,
					},{
						header: 'Field Property', 
						dataIndex: 'Property', 
						flex: 3,
					},{
						xtype: 'checkcolumn',
						text: 'Mandatory',
						dataIndex:'Mandatory',
						listeners: {
							checkChange: function(column, rowIndex, checked, eOpts) {
								return false;
							}
						}
					},{
						xtype: 'checkcolumn',
						text: 'Visible',
						dataIndex:'Visible',
						listeners: {
							checkChange: function(column, rowIndex, checked, eOpts) {
								return false;
							}
						}
					}],
					
					bbar: Ext.create('Ext.PagingToolbar', {
						id:'WORtransGridPage',
						// store: store,
						displayInfo: true,
						listeners: {
							beforechange: function(item1,item2,item3){
								this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(), 'manager' : Ext.util.Cookies.get('employee_id'), 'ProjectID':Ext.getCmp('res_add_project_id').getValue(),'client_id':Ext.getCmp('res_add_client_id').getValue()}																		
							},
							change:function(){
								
								var myGrid = Ext.getCmp('WORtransGridID');
								
								for(i=0; i < myGrid.store.data.length; i++)
								{
									var user = myGrid.store.getAt(i);							
									var myAry = AM.app.globals.transEmployRes;
									
									for (var j=0; j< myAry.length; j++)
									{
										if(myAry[j] != null && user.data.employee_id === myAry[j].data.employee_id)
											myGrid.selModel.doMultiSelect(user,true);
									}	
								}
							}
						},
						displayMsg: 'Displaying employees {0} - {1} of {2}',
						emptyMsg: "No employees to display."
					}),
					listeners: {
						sortchange :  function(ct, column) {
							this.store.getProxy().extraParams = {'searchTxt': Ext.getCmp('resource_id').getValue(),'manager' : Ext.util.Cookies.get('employee_id'), 'ProjectID':Ext.getCmp('res_add_project_id').getValue(),'client_id':Ext.getCmp('res_add_client_id').getValue()}																		
							this.store.load();
						}
					}
				}]
			}]
		}];
		
		this.buttons = ['->', {
			xtype: 'displayfield',
			value : '<span style="color:red">* marked fields are mandatory</span>',
		}, {
			text : 'Remove Employee',
			id:'addSubmit',
			icon : AM.app.globals.uiPath+'resources/images/submit.png',
			handler : this.onSubmit
		},{
			text : 'Exit',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},
	
});