Ext.define('AM.view.clientWOR.List',{
	extend : 'Ext.grid.Panel',
	title: 'Work Order List',
    store: 'ClientWOR',
    id : 'ClientrdrGridId',
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
    	this.columns = [ {
			header : 'WOR ID',
			dataIndex : 'work_order_id',
			flex: 1
		}, {
			header : 'WOR Name', 
			dataIndex : 'wor_name', 
			flex: 1,
		}, {
			header : 'Client', 
			dataIndex : 'client', 
			flex: 1,
		}, {
			header : 'WOR Type', 
			dataIndex : 'wor_type', 
			flex: 1,
		}, {
			header : 'Start Date',
			dataIndex : 'start_date',
			flex: 1,
		}, {
			header : 'End Date',
			dataIndex : 'end_date',
			flex: 1,
		},  {
			header : 'Client Partner', 
			dataIndex : 'client_partner',
			flex: 1
		},  {
			header : 'Engagement Manager', 
			dataIndex : 'engagement_manager',
			flex: 1
		},  {
			header : 'Delivery Manager', 
			dataIndex : 'delivery_manager',
			flex: 1
		},  {
			header : 'Status', 
			dataIndex : 'status',
			flex: 1
		}, {
			header : 'Action', 
			width : '8%',
			xtype : 'actioncolumn',
			sortable: false,
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
				getClass: function(v, meta, record) {
					
					if(record.data.Flag !=4)
					{
						return 'rowVisible';
					}
					else
					{ 
						return 'x-hide-display';
					}
				}
			} ,'-', {
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'View',
				handler : this.onView
			}]
		}];
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: 'ClientWOR',
			dock: 'bottom',
			width:450,
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",
			style:{
				border: 'none'
			},
			displayInfo: true,
		}];

		this.callParent(arguments);
	},
	
	tools : [{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    },{ 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    },{
		xtype: 'combobox',
		id:'WORStatusComboBox',
		store: Ext.create('Ext.data.Store', {
			fields: ['Flag', 'Status'],
			data: [
				{'Flag': '0', 'Status': 'All'},
				{'Flag': '1', 'Status': 'In-Progress'},
				{'Flag': '2', 'Status': 'Open'},
				{'Flag': '3', 'Status': 'Closed'}
			]
		}),
		displayField: 'Status', 
		valueField: 'Flag',
		value:'1',
	}, { 
        xtype : 'tbseparator',
        style : {
        	'margin-right' : '10px'
        }
    }, {
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientWOR').onCreateWOR();
	    },
		text : 'Add WOR'
	}],
		
	onEdit : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientWOR').onEditWOR(grid, rowIndex, colIndex);
	},

	onView : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientWOR').onViewWOR(grid, rowIndex, colIndex);
	},

	onAssignHoliday : function(grid, rowIndex, colIndex) {
		AM.app.getController('ClientWOR').onAddHoliday(grid, rowIndex, colIndex);
	}
	
});