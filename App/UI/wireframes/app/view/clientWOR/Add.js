Ext.define('AM.view.clientWOR.Add',{
	extend : 'Ext.form.Panel',
	layout: 'fit',
	width : '99%',
	layout: {
		type: 'vbox',
		align: 'center'
	},
	title: 'Add WOR Details',
	border : 'true',
	initComponent : function() {

		this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id : 'addWORFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 140,
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '50%',
				},
				// width: 630,
				title:"WOR Information",
				items:[{
					xtype : 'displayfield',
					fieldLabel : 'WOR ID <span style="color:red">*</span>',
					value : 'WOR'+Ext.Date.format(new Date(), 'yhis'),
					name : 'wor_id',
					width: 630,
					submitValue: true
				},{
					xtype : 'textfield',
					fieldLabel: 'WOR Name <span style="color:red">*</span>',
					width: 630,
				},{
					xtype : 'boxselect',
					multiSelect: false,
					allowBlank: false,
					fieldLabel: 'Client Name <span style="color:red">*</span>',
					name: 'ClientID',
					store: Ext.getStore('Clients'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Client_Name',
					valueField: 'ClientID',
					emptyText: 'Select Client',
					value:'',
					width: 630,
				},{
					xtype : 'radiogroup',
					fieldLabel: 'Billable <span style="color:red">*</span>',
					items: [
						{ boxLabel: 'Yes', name: 'rb', inputValue: '1', width: '60px !important' },
						{ boxLabel: 'No', name: 'rb', inputValue: '2', width: '60px !important'  },
					]
				}, {
					xtype:'boxselect',
					multiSelect : false,
					fieldLabel: 'Service Type <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['id', 'name'],
						data: [
						{'id': '1', 'name': 'Adtech'},
						{'id': '2', 'name': 'Martech'},
						{'id': '3', 'name': 'Search'},
						{'id': '4', 'name': 'Media Review'},
						{'id': '5', 'name': 'Reporting'},
						{'id': '6', 'name': 'Tech Services'},
						{'id': '7', 'name': 'Product Development Services'},
						{'id': '8', 'name': 'Product Data Services'},
						{'id': '9', 'name': 'Product Development'},
						{'id': '10', 'name': 'Engagement'},
					]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'name',
					valueField: 'id',
					width: 630,
				},{
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'WOR Start Date <span style="color:red">*</span>',
					anchor : '100%',
					allowBlank : false,
					width: 630,
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'WOR End Date <span style="color:red">*</span>',
					allowBlank : false,
					width: 630,
				},{
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					readOnly: true,
					width: 630,
					fieldLabel: 'Status <span style="color:red">*</span>',
					store: Ext.create('Ext.data.Store', {
						fields: ['Flag', 'Status'],
						data: [
						{'Flag': '0', 'Status': 'Work In Progress'},
						]
					}),
					queryMode: 'remote',
					minChars:0,
					displayField: 'Status',
					valueField: 'Flag',
					value: '0',
				}]
			}, {
				xtype:'fieldset',
				padding: '8',
				style : {
					width : '50%',
				},
				width:630,
				title:"People Information",
				items:[{
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Client Partner <span style="color:red">*</span>',
					store : Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Client Partner',
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Eng. Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Engagement Manager',
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Delivery Manager <span style="color:red">*</span>',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Delivery Manager',
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				},{
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Associate Manager',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Associate Manager',
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Team Lead',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Team Lead',
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				}, {
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Stake Holder',
					store: Ext.getStore('Employees'),
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Stake Holder',
					listeners	: {
						scope		: this,
						beforequery	: function(queryEvent) {
							var empTypes = 'grade5AndAbove';
							queryEvent.combo.getStore().getProxy().extraParams = {
								'hasNoLimit'	:	"1", 
								'empTypes'		:   empTypes,  
								'filterName'	: 	'FirstName'
							};
						}
					}
				},{
					xtype:'boxselect',
					multiSelect : false,
					allowBlank: false,
					fieldLabel: 'Time Entry Approver(s)',
					queryMode: 'remote',
					minChars:0,
					displayField: 'FullName',
					valueField	: 'EmployID',
					emptyText: 'Select Time Entry Approver',
				}]
			}]
		}, {
			xtype: 'tabpanel',
			activeTab: 0,
			width: 1300,
			height: 300,
			padding: '0 0 10px 0',
			items:[{
				title: 'FTE',
				items : [{								
					xtype:AM.app.getView('clientWOR.FTEList').create(),
					hidden:false
				}]
			}, {
				title: 'Hourly',
				items : [{								
					xtype:AM.app.getView('clientWOR.HourlyList').create(),
					hidden:false
				}]
			}, {
				title: 'Volume Based',
				items : [{								
					xtype:AM.app.getView('clientWOR.VolumeBasedList').create(),
					hidden:false
				}]
			}, {
				title: 'Project',
				items : [{								
					xtype:AM.app.getView('clientWOR.ProjectList').create(),
					hidden:false
				}]
			}]
		}];
		
		this.buttons = ['->', {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
		}, {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
		}];

		this.callParent(arguments);
	},	

	onCancel : function() {
		AM.app.getController('ClientWOR').initWOR();
	},
});