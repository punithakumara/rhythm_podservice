Ext.define('AM.view.ClientHolidays.Add',{
	extend : 'Ext.window.Window',
	alias : 'widget.holidayAdd',
	title: 'Add Client Holiday',
	layout : 'fit',
	width : '40%',
    autoShow : true,
    autoSave: false,
    autoHeight : true,
    modal : true,
	
	initComponent : function() {		
		this.items = [{
			xtype : 'form',
			itemId : 'holidayFrm',
			fieldDefaults: {
				labelAlign: 'left',
				labelWidth: 100,
				anchor: '100%'
			},
			autoScroll : true,
			autoHeight : true,
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px'
			},
			items : [{
				xtype : 'textfield',
				name : 'name',
				allowBlank: false,
				fieldLabel : 'Holiday Name <span style="color:red">*</span>',
				anchor: '100%',
			}, {
				xtype : 'datefield',
				allowBlank: false,
				fieldLabel : 'Date <span style="color:red">*</span>',
				anchor: '100%',
				maxLength: 500,
			}]
		}];
		
		this.buttons = ['->', {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
			handler : this.onSave
		}];

		this.callParent(arguments);
	},
	
	onCancel : function() {
		this.up('window').close();
	}
	
});