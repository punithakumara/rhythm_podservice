Ext.define('AM.view.ClientHolidays.List',{
	extend : 'Ext.grid.Panel',
	title: 'Client Holidays List',
	store: Ext.getStore('clientHolidays'),
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
		
		this.store  = Ext.getStore("clientHolidays");
		
    	this.columns = [ {
			header : 'Name', 
			dataIndex : 'leave_name', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1,
		}, {
			header : 'Date',
			dataIndex : 'date',
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1,
		}, {
			header : 'Added By', 
			dataIndex : 'raised_by', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1
		}, {
			header : 'Action', 
			width : '8%',
			xtype : 'actioncolumn',
            sortable: false,
            menuDisabled:true,
            draggable: false,
			items : [{
				icon : AM.app.globals.uiPath+'/resources/images/edit.png',
				tooltip : 'Edit',
				handler : this.onEdit,
			} ,'-', {
				icon : AM.app.globals.uiPath+'/resources/images/delete.png',
				tooltip : 'Delete',
				handler : this.onDelete
			}]
		}];
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: this.store,
			dock: 'bottom',
			width:450,
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",
			style:{
				border: 'none'
			},
			displayInfo: true,
		}];

		this.callParent(arguments);
	},
	
	tools : [{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientHolidays').onCreateHoliday();
	    },
		text : 'Add Client Holiday'
	}],
	
});