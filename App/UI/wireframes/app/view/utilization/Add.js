Ext.define('AM.view.utilization.Add',{
	extend : 'Ext.window.Window',
	alias : 'widget.utilizationAdd',
	layout: 'fit',
	title: 'Add Utilization',
	border : 'true',
	width : '63%',
	minHeight: '50%',
	autoShow : true,
    modal: true,
	autoScroll : true,
	
    initComponent : function() {

		var attrStore = new Ext.data.SimpleStore({
			fields: ['attr_id', 'attr_name'],
			data: [
			   ['1', 'Static Page'],
			   ['2', 'Template'],
			]
		});

    	this.items = [{
			xtype : 'form',
			layout: 'hbox',
			id: 'utilizationForm',
			fieldDefaults: {
				labelAlign: 'left',
				anchor: '100%'
			},
			bodyStyle : {
				border : 'none',
				display : 'block',
				float : 'left',
				padding : '10px 10px 6px 10px',
			},
			autoScroll : true,
			autoHeight : true,
			
			items : [{
				xtype: 'fieldset',
				title: "Standard Fields",
				margin: '0 10 0 0',
				padding: '8',
				style : {
					width : '32%',
				},
				fieldDefaults: {
					labelWidth: 180,
				},
				width: 406,
				items:[{
					xtype : 'boxselect',
					fieldLabel : 'Project Code <span style="color:red">*</span>',
					name : 'project_code',
				}, {
					xtype : 'boxselect',
					fieldLabel : 'Task Code <span style="color:red">*</span>',
					name : 'task_code',
				}, {
					xtype : 'boxselect',
					fieldLabel : 'Sub Task',
					name : 'sub_task',
				}, {
					xtype:'textfield',
					fieldLabel: 'Brand <span style="color:red">*</span>',
					name: 'brand',
				}, {
					xtype:'textfield',
					fieldLabel: 'Request Region <span style="color:red">*</span>',
					name: 'region',
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Request Date <span style="color:red">*</span>',
					anchor : '100%',
					allowBlank : false,
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Request Start Date <span style="color:red">*</span>',
					allowBlank : false,
				}, {
					xtype : 'datefield',
					format : AM.app.globals.CommonDateControl,
					fieldLabel : 'Request Complete Date <span style="color:red">*</span>',
					allowBlank : false,
				}, {
					xtype:'textfield',
					fieldLabel: 'Request Name/ID <span style="color:red">*</span>',
					name: 'request_name',
				}, {
					xtype : 'numberfield',
					fieldLabel: '# of Campaigns <span style="color:red">*</span>',
					name: 'campaigns',
				}, {
					xtype:'numberfield',
					fieldLabel: '# of Placement / Line Items/ Mailers/ Versions <span style="color:red">*</span>',
					name: 'placement',
				}, {
					xtype:'numberfield',
					fieldLabel: '# of Creatives/ Tags/ Reports <span style="color:red">*</span>',
					name: 'creatives',
				}, {
					xtype:'boxselect',
					fieldLabel: 'Total Hrs <span style="color:red">*</span>',
					name: 'total_hrs',
				}, {
					xtype : 'textfield',
					fieldLabel : 'Remarks',
				}]
			}, {
				xtype : 'fieldset',
				title: 'Additional Fields',
				id: 'additionalFields',
				layout : 'hbox',
				padding:'8',
				margin: '0 10 20 10',
				style : {
					width : '32%'
				},
				fieldDefaults: {
					labelWidth: 130,
					width: 370
				},
			}]
    	}];
		
    	this.buttons = ['->', {
			text : 'Duplicate',
			id:'cloneButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
        }, {
			text : 'Cancel',
			icon : AM.app.globals.uiPath+'resources/images/cancel.png',
			handler : this.onCancel
        }, {
			text : 'Save',
			id:'saveButtonID',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
        }];

		this.callParent(arguments);
	},	

	onCancel : function() {
		this.up('window').close();
	},
});