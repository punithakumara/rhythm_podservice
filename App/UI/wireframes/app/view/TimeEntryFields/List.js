Ext.define('AM.view.TimeEntryFields.List',{
	extend : 'Ext.grid.Panel',
	title: 'Time Entry Fields',
	store: Ext.getStore('TimeEntryFields'),
    layout : 'fit',
    width : '99%',
    minHeight : 200,
    loadMask: true,
    disableSelection: false,
    border : true,
	
    initComponent : function() {
		
		this.store  = Ext.getStore("TimeEntryFields");
		
    	this.columns = [ {
			header : 'Field Name', 
			dataIndex : 'field_name', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1,
		}, {
			header : 'Field Description',
			dataIndex : 'field_description',
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1,
		}, {
			header : 'Filed Property', 
			dataIndex : 'field_property', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1
		}, {
			header : 'Filed Type', 
			dataIndex : 'field_type', 
            sortable: false,
            menuDisabled:true,
            draggable: false,
			flex: 1
		}];
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: this.store,
			dock: 'bottom',
			width:450,
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",
			style:{
				border: 'none'
			},
			displayInfo: true,
		}];

		this.callParent(arguments);
	},

	tools : [{
        xtype : 'trigger',
        itemId : 'gridTrigger',
        labelWidth : 60,
        labelCls : 'searchLabel',
        triggerCls : 'x-form-clear-trigger',
        emptyText : 'Search',
        width : 250,
        minChars : 1,
        enableKeyEvents : true,
        onTriggerClick : function(){
            this.reset();
            this.fireEvent('triggerClear');
        }
    }]
	
	/*tools : [{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientHolidays').onCreateHoliday();
	    },
		text : 'Add Client Holiday'
	}],*/
	
});