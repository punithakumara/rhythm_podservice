Ext.define('AM.view.insight.List',{
	extend : 'Ext.form.Panel',
	width : '99%',
	layout: 'fit',
	
    initComponent : function() {

    	this.items = [{
			xtype:'container',
			layout: {
				type: 'vbox',
			},
			items: [{
				xtype: 'insightTopPanel',
				margin : '0 0 10px 0'
			}, {
				xtype: 'tabpanel',
				activeTab: 0,
				width: '100%',
				height: 480,
				margin : '0 0 10px 0',
				frame: true,
				items:[{
					title: 'All Data',
					items : [{								
						xtype:AM.app.getView('insight.GeneralList').create(),
						hidden:false
					}]
				}, {
					title: 'Weekend Coverage',
					items : [{								
						xtype:AM.app.getView('insight.WeekendList').create(),
						hidden:false
					}]
				}, {
					title: 'Holiday Coverage',
					items : [{								
						xtype:AM.app.getView('insight.ClientHolidaysList').create(),
						hidden:false
					}]
				}]
			}]
		}];
		
		this.callParent(arguments);
	},
});