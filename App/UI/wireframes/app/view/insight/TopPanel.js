Ext.define('AM.view.insight.TopPanel', {
	extend  : 'Ext.form.Panel',
	alias   : 'widget.insightTopPanel',
	layout  : 'fit',
	collapsible: true,
	collapsed: false,
	floatable: true,
	width : '100%',
	title   : 'Insight',
	
	listeners: {
		afterrender: function(panel) {
			panel.header.el.on('click', function() {
				if (panel.collapsed) {panel.expand();}
				else {panel.collapse();}
			});
		}
    },
  
	initComponent : function() {
  		var me = this;

		var labelWidth = 300;
		var enddatewidth = 300;
		
		this.items = [{
			xtype:'container',
			layout: {
				type: 'hbox',
			},
			items: [{
				xtype : 'datefield',
				fieldLabel: 'Start Date',
				labelWidth: 70,
				emptyText: 'Select Start Date',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 15px'
				},
				width:220,
			},{
				xtype : 'datefield',
				fieldLabel: 'End Date',
				labelWidth: 60,
				emptyText: 'Select End Date',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 20px'
				},
				width:200,
			},{
				xtype : 'boxselect',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 20px'
				},
				width:250,
				fieldLabel: 'Client',
				labelWidth: 40,
				emptyText: 'Select Client',
			},{
				xtype : 'boxselect',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 20px'
				},
				width:250,
				fieldLabel: 'Project Code',
				labelWidth: 80,
				emptyText: 'Select Project Code',
			},{
				xtype : 'boxselect',
				style:{
					'position':'absolute',
					'margin':'10px 0 0 20px'
				},
				width:300,
				fieldLabel: 'Employee Name',
				labelWidth: 100,
				emptyText: 'Select Employee Name',
			}]
		}];

		this.callParent(arguments);
		
	}
});
