Ext.define('AM.view.insight.ClientHolidaysList',{
	extend : 'Ext.grid.Panel',
	alias : 'widget.ClientHolidaysList',
    layout : 'fit',
    selType: 'rowmodel',
	emptyText: '<div align="center">No Data To Display</div>',
	viewConfig: { 
		forceFit : true,
	},
    width : '100%',
    height : 435,
    loadMask: true, 
    border : true,
	
	initComponent : function() {
		var me = this;

		//**** List View Coloumns ****//
    	this.columns = [ {
			header : 'Project Code',
			dataIndex : 'project_code',
			width: 220,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Project Code',
		}, {
			header : 'Task Code', 
			dataIndex : 'task_code', 
			width: 100,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Task Code',
		}, {
			header : 'Sub Task', 
			dataIndex : 'sub_task', 
			width: 180,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Sub Task',
		}, {
			header : 'Brand',
			dataIndex : 'brand',
			width: 100,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Brand',
		}, {
			header : 'Request Region',
			dataIndex : 'request_region',
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Request Region',
		},  {
			header : 'Request Date', 
			dataIndex : 'request_date',
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Request Date', 
		}, {
			header : 'Request Start Date', 
			dataIndex : 'start_date', 
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Request Start Date', 
		}, {
			header : 'Request Completion Date', 
			dataIndex : 'completion_date', 
			width: 120,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Request Completion Date', 
		}, {
			header : 'Request Name/ID', 
			dataIndex : 'request_name', 
			width: 150,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Request Name/ID', 
		}, {
			header : '# of Campaigns', 
			dataIndex : 'campaign', 
			width: 100,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : '# of Campaigns', 
		}, {
			header : '# of Placement / Line Items/ Mailers/ Versions', 
			dataIndex : 'placement', 
			width: 100,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : '# of Placement / Line Items/ Mailers/ Versions', 
		}, {
			header : '# of Creatives/ Tags/ Reports', 
			dataIndex : 'tags', 
			width: 100,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : '# of Creatives/ Tags/ Reports', 
		}, {
			header : 'Total Hrs', 
			dataIndex : 'total_hrs', 
			width: 70,
			sortable: false,
			menuDisabled:true,
			groupable: false,
			draggable: false,
			tooltip : 'Total Hrs', 
		}];

		//**** End List View Coloumns ****//
		
		this.tbar = [{
			text : 'Reject Billable Hours',
			icon: AM.app.globals.uiPath+'resources/images/cancel.png',
		}, {
			text : 'Approve Billable Hours',
			icon: AM.app.globals.uiPath+'resources/images/assign.png',
		}, {
        	xtype: "button",
        	icon: AM.app.globals.uiPath+'resources/images/excel_Icon.png',
    		text : 'Export To Excel',
    		style: {
                "margin": "0 0 0 850px",
            }
    	}];
		
		//**** Pagination ****//
		this.bbar = [{
			xtype: 'pagingtoolbar',
			dock: 'bottom',
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",						
			style:{
				border: 'none'
			},
			displayInfo: true,	   
		},'->',{
			xtype : 'combobox',
			multiSelect: false,
			name: 'pagination_records',
			minChars: 0,
			store: [15,25,50,100],
			queryMode: 'local',
			displayField: 'Records',
			valueField: 'rec',
			width : 200,
			value : 15,
			labelWidth : 110,
			style: {
				'margin-left':'0px'
			},
			fieldLabel: 'No of Records',
		},'->',{
			xtype    : 'displayfield',
			value:'Overall List Total : 145 Hrs 90 Min'
		},'->',{
			xtype    : 'displayfield',
			value:'Current List Total : 45 Hrs 90 Min'
		}];
		//**** Pagination ****//
		
		this.callParent(arguments);
	},		
});