Ext.define('AM.view.ProjectTypes.List',{
	extend : 'Ext.grid.Panel',
	title: 'Projects',
	store: Ext.getStore('ProjectTypes'),
	layout : 'fit',
	width : '99%',
	minHeight : 200,
	loadMask: true,
	disableSelection: false,
	border : true,
	
	initComponent : function() {
		
		this.store  = Ext.getStore("ProjectTypes");
		
		this.columns = [ {
			header : 'WOR Type', 
			dataIndex : 'wor_type', 
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		},{
			header : 'Project Name', 
			dataIndex : 'project_name', 
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Description',
			dataIndex : 'project_description',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : '# of Roles', 
			dataIndex : 'total_roles', 
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1
		}];
		
		this.bbar = [{
			xtype: 'pagingtoolbar',
			id: 'id-docs-paging',
			store: this.store,
			dock: 'bottom',
			width:450,
			displayMsg: 'Total {2} Records',
			emptyMsg: "No Records to display",
			style:{
				border: 'none'
			},
			displayInfo: true,
		}];

		this.callParent(arguments);
	},

	tools : [{
		xtype : 'trigger',
		itemId : 'gridTrigger',
		labelWidth : 60,
		labelCls : 'searchLabel',
		triggerCls : 'x-form-clear-trigger',
		emptyText : 'Search',
		width : 250,
		minChars : 1,
		enableKeyEvents : true,
		onTriggerClick : function(){
			this.reset();
			this.fireEvent('triggerClear');
		}
	}]
	
	/*tools : [{
		xtype : 'button',
		icon: AM.app.globals.uiPath+'/resources/images/add.png',
		handler: function () {
			AM.app.getController('ClientHolidays').onCreateHoliday();
	    },
		text : 'Add Client Holiday'
	}],*/
	
});