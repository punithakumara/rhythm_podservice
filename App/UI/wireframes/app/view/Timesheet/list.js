Ext.define('AM.view.Timesheet.List',{
	extend : 'Ext.grid.Panel',
	title: 'Timesheet',
	layout : 'fit',
	width : '99%',
	minHeight : 200,
	loadMask: true,
	store: 'Timesheet',
	disableSelection: false,
	border : true,
	
	initComponent : function() {

		this.tools = [{
			xtype: 'boxselect',
			fieldLabel: 'For Week',
			labelCls : 'searchLabel',
			displayField: 'week', 
			valueField: 'id',
			store: Ext.create('Ext.data.Store', {
				fields: ['id', 'week'],
				data: [
				{'id': '1', 'week': '24-Jun-2019 to 30-Jun-19 (Week-26)'},
				{'id': '2', 'week': '1-Jul-2019 to 7-Jul-19 (Week-27)'},
				{'id': '3', 'week': '8-Jul-2019 to 14-Jul-19 (Week-28)'},
			]
			}),
			id: 'timeWeekRange',
			width: 380,
			emptyText: 'Select Week',
			value : '2',
		}];

		this.tbar = [{
			xtype: "button",
			text: 'Add Entry',
			id: 'addTimeRow',
			icon : AM.app.globals.uiPath+'resources/images/Add.png',
		}];

		this.bbar = [{
			xtype: "button",
			text: 'Save Changes',
			id: 'saveTimeRow',
			icon : AM.app.globals.uiPath+'resources/images/save.png',
		},{
			xtype: "button",
			text: 'Duplicate',
			icon : AM.app.globals.uiPath+'resources/images/duplicate.png',
			id: 'dupTimeRow',
		},{
			xtype: 'displayfield',
			id: "TotalMonthHours",
			name: "OverallTime",
			width : '32%',
			style : {
				'text-align' : 'center'
			}
		},{
			xtype: 'displayfield',
			id: "TotalWeekHours",
			name: "TotalHours",
			width : '32%',
			style : {
				'text-align' : 'center'
			}
		},{
			xtype: 'displayfield',
			value: '<span style="color:red">* marked column is mandatory</span>',
			width : '20%',
			style : {
				'text-align' : 'right'
			}
		}];

		this.columns = [ {
			xtype: 'checkcolumn',
			flex: 0.3,
		},{
			header : 'Client Name', 
			dataIndex : 'client_name', 
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1.3,
		},{
			header : 'Work Order Name',
			dataIndex : 'wor_name',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1.5,
		}, {
			header : 'Task',
			dataIndex : 'task_name',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1.3,
		}, {
			header : 'Sub Task',
			dataIndex : 'sub_task_name',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1.3,
		}, {
			header : 'Mon',
			dataIndex : 'mon',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Tue',
			dataIndex : 'tue',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Wed',
			dataIndex : 'wed',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Thu',
			dataIndex : 'thu',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Fri',
			dataIndex : 'fri',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Sat',
			dataIndex : 'sat',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		}, {
			header : 'Sun',
			dataIndex : 'sun',
			sortable: false,
			menuDisabled:true,
			draggable: false,
			flex: 1,
		},{
			header : 'Action', 
			xtype: 'actioncolumn',
			items: [{
				icon : AM.app.globals.uiPath+'/resources/images/add.png',
				tooltip : 'Add Utilization',
				getClass: function(v, meta, record) {

					if(record.data.task_name != 'Production Work')
					{
						return 'x-hide-display';
					}
					else
					{ 
						return 'rowVisible';
					}
				}
			}, {
				icon : AM.app.globals.uiPath+'/resources/images/delete.png',
				tooltip : 'Delete Row',
				getClass: function(v, meta, record) {

					if(record.data.task_name == 'Production Work')
					{
						return 'x-hide-display';
					}
					else
					{ 
						return 'rowVisible';
					}
				}
			}, {
				xtype: 'tbspacer'
			}, {
				icon : AM.app.globals.uiPath+'/resources/images/view.png',
				tooltip : 'Comments',
				getClass: function(v, meta, record) {

					if(record.data.task_name == 'Production Work')
					{
						return 'x-hide-display';
					}
					else
					{ 
						return 'rowVisible';
					}
				}
			}]
		}];

		this.callParent(arguments);
	},
	
});