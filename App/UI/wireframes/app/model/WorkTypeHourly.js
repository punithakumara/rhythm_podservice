Ext.define('AM.model.WorkTypeHourly',{
	extend : 'Ext.data.Model',
	fields : ['hours', 'shift', 'hours-min', 'hours-max', 'experience', 'skills', 'start_date', 'support', 'responsibilities']
});