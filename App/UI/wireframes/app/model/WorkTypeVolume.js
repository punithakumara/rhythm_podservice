Ext.define('AM.model.WorkTypeVolume',{
	extend : 'Ext.data.Model',
	fields : ['volume_coverage', 'shift', 'volume-min', 'volume-max', 'experience', 'skills', 'start_date', 'support', 'responsibilities']
});