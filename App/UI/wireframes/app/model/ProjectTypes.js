Ext.define('AM.model.ProjectTypes',{
	extend : 'Ext.data.Model',
	
	fields: ['project_name', 'project_description','total_roles']
});