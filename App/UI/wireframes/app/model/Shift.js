Ext.define('AM.model.Shift',{
	extend : 'Ext.data.Model',
	
	fields: ['shift_id', 'shift_code']
});