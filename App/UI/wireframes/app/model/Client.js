Ext.define('AM.model.Client',{
	extend : 'Ext.data.Model',
	
	fields : [{name : 'ClientID', dataType : 'int'},'Client_Name','Web','City','State','Country','Zip','Logo','imgLogo','CountryName','technologies','shifts','resourceCount','industryType','accManager','clientPartner','clientDescription','status', 'Created_Date','FirstName','FullName','LastName','inactiveDate','ClientStatusCombo','ForcastNRR','ForcastRDR','followAccount','Name','ProcessName','ClientTier']
});