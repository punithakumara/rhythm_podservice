Ext.define('AM.model.WorkTypeFTE',{
	extend : 'Ext.data.Model',
	fields : ['project_name', 'shift', 'duration-start', 'duration-end', 'experience', 'skills', 'start_date', 'support', 'responsibilities']
});