Ext.define('AM.model.WorkTypeFTE',{
	extend : 'Ext.data.Model',
	fields : ['no_of_fte', 'shift', 'experience', 'skills', 'start_date', 'support', 'responsibilities']
});