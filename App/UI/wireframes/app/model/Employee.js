Ext.define('AM.model.Employee',{
	extend : 'Ext.data.Model',
	
	
	fields : [{name : 'EmployID', dataType : 'int'},'CompanyEmployID','Primary_Lead_ID','FirstName','Name','LastName','Designation_Name','DesignationID','Email','Mobile','Address1',,'ServiceID','LocationID','DateOfBirth','DateOfJoin','Gender','Address2','City','State','Zip','ShiftID','ShiftCode','AlternateEmail','QualificationID','City1','State1','Zip1','AddressSame','Grade','UpdatedBy','UpdatedDate','VerticalID','RelieveFlag','RelieveDate','RelieveNotes','DeligateTo', 'PromoteTo', 'newjoinee_id','AssignedVertical','Resigned_Date','Resigned_Notes','Resign_Flag','moveonboard',
	          {	name: 'FullName', 
		convert: function(v, rec) { 
					var fullName = rec.get('FirstName');
					if(rec.get('LastName')!='NULL' && rec.get('LastName')!="" && rec.get('LastName')!='null' && rec.get('LastName')!==null)
						fullName += " "+rec.get('LastName');
					return fullName;
				 }
	          }]
});

// 