Ext.define('AM.model.WorTypes',{
	extend : 'Ext.data.Model',
	
	fields: ['name', 'description','total_projects']
});