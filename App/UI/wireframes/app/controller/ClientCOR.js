Ext.define('AM.controller.ClientCOR',{
	extend : 'Ext.app.Controller',
	stores : ['Clients','Responsibilities','SupportCoverageCOR','ClientCOR','Employees','Shifts','ChangeType','ChangeModel'],
	views: ['clientCOR.List','clientCOR.Add','clientCOR.Edit','clientCOR.View','clientCOR.ViewPopup','clientCOR.FTEList','clientCOR.HourlyList','clientCOR.VolumeBasedList','clientCOR.ProjectList'],

	initCOR : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var clients = this.getView('clientCOR.List').create();
		mainContent.removeAll();
		mainContent.add( clients );
		mainContent.doLayout();
	},
	
	onCreateCOR : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('clientCOR.Add').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
	},

	onEditCOR : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('clientCOR.Edit').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
	},
	
	onViewCOR : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('clientCOR.View').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
		Ext.getCmp('AddFTE').setVisible(false);
	},
	
	onViewPopup : function() {
		var view = Ext.widget('ViewPopup');
		view.setTitle('Add Client NRR Details');
	},
	
});
