Ext.define('AM.controller.Menu',{
	extend : 'Ext.app.Controller',
	views : ['Menu'],
	
	init: function() {
		scope : this,
        this.control({
		 
			'[action=worDashboardPage]' : {
				click : this.worDashboardList
			},
			
			'[action=corDashboardPage]' : {
				click : this.corDashboardList
			},
			
			'[action=utilizationPage]' : {
				click : this.utilizationList
			},
			
			'[action=insightPage]' : {
				click : this.insightList
			},
			
			'[action=fteReportPage]' : {
				click : this.fteReportList
			},
			
			'[action=hourlyReportPage]' : {
				click : this.hourlyReportList
			},
			
			'[action=clientHolidaysPage]' : {
				click : this.clientHolidaysList
			},
			
			'[action=shiftTransPage]' : {
				click : this.shiftTransPage
			},
			
			'[action=leadTransPage]' : {
				click : this.leadTransPage
			},
			
			'[action=releaseHrPage]' : {
				click : this.releaseHrPage
			},
			
			'[action=pullHrPage]' : {
				click : this.pullHrPage
			},
       });
    },
	
    viewMenu : function() {
    	var mainTool = Ext.ComponentQuery.query('#menuToolbar')[0];
		mainTool.removeAll();
        var menu = this.getView('Menu').create();
        mainTool.add( menu );
    },

	shiftTransPage : function(){
    	this.getController("AM.controller.Transition").viewShiftPage();
	},

	leadTransPage : function(){
    	this.getController("AM.controller.Transition").viewLeadPage();
	},

	releaseHrPage : function(){
    	this.getController("AM.controller.Transition").viewReleaseHrPage();
	},

	pullHrPage : function(){
    	this.getController("AM.controller.Transition").viewPullHrPage();
	},

	worDashboardList : function(){
    	this.getController("AM.controller.ClientWOR").initWOR();
	},
	
	corDashboardList : function(){
    	this.getController("AM.controller.ClientCOR").initCOR();
	},
	
	utilizationList : function(){
    	this.getController("AM.controller.Utilization").viewContent();
	},
	
	insightList : function(){
    	this.getController("AM.controller.insight").viewContent();
	},	
	
	fteReportList : function(){
    	this.getController("AM.controller.MonthlyBillabilityReport").viewFTEContent();
	},
	
	hourlyReportList : function(){
    	this.getController("AM.controller.MonthlyBillabilityReport").viewHourlyContent();
	},
	
	clientHolidaysList : function(){
    	this.getController("AM.controller.ClientHolidays").viewHolidaysList();
	},	

	timeEntryFields : function(){
    	this.getController("AM.controller.TimeEntryFields").viewTimeEntryFields();
	},

	worTypes : function(){
    	this.getController("AM.controller.WorTypes").viewWorTypes();
	},	

	projectTypes : function(){
    	this.getController("AM.controller.projectTypes").viewProjectTypes();
	},	

	roleTypes : function(){
    	this.getController("AM.controller.RoleTypes").viewRoleTypes();
	},	
	
	timesheetList : function(){
    	this.getController("AM.controller.Timesheet").viewContent();
	},
});