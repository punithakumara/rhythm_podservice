Ext.define('AM.controller.projectTypes',{
	extend : 'Ext.app.Controller',
	stores: ["ProjectTypes"],
	views: ['ProjectTypes.List'],
	
	viewProjectTypes : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var ProjectTypes = this.getView('ProjectTypes.List').create();
		mainContent.removeAll();
		mainContent.add(ProjectTypes);
		mainContent.doLayout();		
	},
	
	/*onCreateHoliday : function() {
		var view = Ext.widget('holidayAdd');
	},*/
	
});