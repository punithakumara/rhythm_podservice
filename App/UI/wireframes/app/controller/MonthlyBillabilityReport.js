Ext.define('AM.controller.MonthlyBillabilityReport',{
	extend : 'Ext.app.Controller',
	stores : ['FteReport','HourlyReport','Remarks'],
	views: ['MonthlyBillabilityReport.FteReport', 'MonthlyBillabilityReport.HourlyReport', 'MonthlyBillabilityReport.MonthField','MonthlyBillabilityReport.ReportRemarks'],

	init : function() {
		this.control({
			'MonthlyBillabilitygroupReportGrid #gridTrigger' : {
                keyup : this.onTriggerKeyUp,
                triggerClear : this.onTriggerClear
            }
		});
	},
	
	viewFTEContent : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var MonthlyBillabilityReport = this.getView('MonthlyBillabilityReport.FteReport').create();
		mainContent.removeAll();
		mainContent.add(MonthlyBillabilityReport);
		mainContent.doLayout();		
	},
	
	viewHourlyContent : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var MonthlyBillabilityReport = this.getView('MonthlyBillabilityReport.HourlyReport').create();
		mainContent.removeAll();
		mainContent.add(MonthlyBillabilityReport);
		mainContent.doLayout();		
	},
	
	viewRemarks : function(clientId) {			
		var view = Ext.widget('ReportRemarks');
		view.setTitle("Prorated FTE's");

		var myStore = this.getStore('Remarks');
		Ext.getCmp('remarks_grid').bindStore(myStore);
		myStore.load();		
	},
	
});