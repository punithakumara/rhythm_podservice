Ext.define('AM.controller.Timesheet',{
	extend : 'Ext.app.Controller',
	views: ['Timesheet.List'],
	stores: ['Timesheet'],
	
	viewContent : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var timesheet = this.getView('Timesheet.List').create();
		mainContent.removeAll();
		mainContent.add(timesheet);
		mainContent.doLayout();		
	},
	
	/*onCreateHoliday : function() {
		var view = Ext.widget('holidayAdd');
	},*/
	
});