Ext.define('AM.controller.ClientWOR',{
	extend : 'Ext.app.Controller',
	stores : ['Clients','Responsibilities','SupportCoverage','ClientWOR','Employees','Shifts','ChangeType','clientHolidayDetails'],
	views: ['clientWOR.List','clientWOR.Add','clientWOR.Edit','clientWOR.View','clientWOR.SummaryList','clientWOR.FTEList','clientWOR.HourlyList','clientWOR.VolumeBasedList','clientWOR.ProjectList','clientWOR.AssignClientHolidays','clientWOR.AddResources','clientWOR.ViewResources','clientWOR.ConfigureAttributes'],

	initWOR : function(clientID,from) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var clients = this.getView('clientWOR.List').create();
		mainContent.removeAll();
		mainContent.add( clients );
		mainContent.doLayout();
	},
	
	onCreateWOR : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('clientWOR.Add').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
	},

	onEditWOR : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('clientWOR.Edit').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
	},
	
	onViewWOR : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var wor = this.getView('clientWOR.View').create();
		mainContent.removeAll();
		mainContent.add( wor );
		mainContent.doLayout();
		// Ext.getCmp('AddFTE').setVisible(false);
	},
	
	onAddHoliday : function(grid, row, col) {
		var view = Ext.widget('AssignClientHolidays');
		view.setTitle("Assign Client Holidays");
	},
	
	onAssignResources : function() {
		var view = Ext.widget('AddResources');
	},

	onViewResources : function() {
		var view = Ext.widget('ViewResources');
	},
	
	onConfigureAttributes : function() {
		var view = Ext.widget('ConfigureAttributes');
	},

});
