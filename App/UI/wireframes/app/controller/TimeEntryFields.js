Ext.define('AM.controller.TimeEntryFields',{
	extend : 'Ext.app.Controller',
	stores: ["TimeEntryFields"],
	views: ['TimeEntryFields.List'],
	
	viewTimeEntryFields : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var TimeEntryFields = this.getView('TimeEntryFields.List').create();
		mainContent.removeAll();
		mainContent.add(TimeEntryFields);
		mainContent.doLayout();		
	},
	
	/*onCreateHoliday : function() {
		var view = Ext.widget('holidayAdd');
	},*/
	
});