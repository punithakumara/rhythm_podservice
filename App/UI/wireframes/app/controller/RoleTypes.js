Ext.define('AM.controller.RoleTypes',{
	extend : 'Ext.app.Controller',
	stores: ["RoleTypes"],
	views: ['RoleTypes.List'],
	
	viewRoleTypes : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var RoleTypes = this.getView('RoleTypes.List').create();
		mainContent.removeAll();
		mainContent.add(RoleTypes);
		mainContent.doLayout();		
	},
	
	/*onCreateHoliday : function() {
		var view = Ext.widget('holidayAdd');
	},*/
	
});