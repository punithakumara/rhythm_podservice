Ext.define('AM.controller.Transition',{
	extend : 'Ext.app.Controller',
	views: ['transition.transWizard','transition.transClientPage','transition.transPullHrPage','transition.transShiftPage','transition.transRelEmpThirdPage','transition.transShiftThirdPage','transition.transLeadThirdPage','transition.transPullHrEmpThirdPage','transition.transFourthPage'],
	stores:['Shifts'],
	
	viewShiftPage : function() 
	{
    	AM.app.transitionTargetPage ="shiftTrans";
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.layout.setActiveItem(2);
		wizTrans.setTitle('Shift Transition');
		
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
	},
	
	viewLeadPage : function() 
	{
    	AM.app.transitionTargetPage ="leadTrans";
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.setTitle('Supervisor Transition');
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
	},
	
	viewReleaseHrPage : function() 
	{
    	AM.app.transitionTargetPage ="releaseHr";
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.setTitle('Release to HR Pool');
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
	},
	
	viewPullHrPage : function() 
	{
    	AM.app.transitionTargetPage ="pullHr";
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];		
		var wizTrans = this.getView('transition.transWizard').create();
		wizTrans.layout.setActiveItem(1);
		wizTrans.setTitle('Pull from HR Pool');
		mainContent.removeAll();
		mainContent.add( wizTrans );
		mainContent.doLayout();
	},
	
	showTransSecondPage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');	
		
		if(AM.app.transitionTargetPage=="shiftTrans")
		{
			wizard.getLayout().setActiveItem('transitionShiftPage');
		}
		else
		{
			wizard.getLayout().setActiveItem('transitionClientPage');
		}
	},
	
	showTransThirdPage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');

		if(AM.app.transitionTargetPage=="shiftTrans")
		{
			wizard.getLayout().setActiveItem('transShiftThirdPage');
		}
		else if(AM.app.transitionTargetPage=="leadTrans")
		{
			wizard.getLayout().setActiveItem('transLeadThirdPage');
		}
		else if(AM.app.transitionTargetPage=="pullHr")
		{
			wizard.getLayout().setActiveItem('transPullHrEmpThirdPage');
		}
		else
		{
			wizard.getLayout().setActiveItem('transReleaseEmpThirdPage');
		}
	},
	
	onSavePage :function(btn) 
	{
		var wizard = btn.up('#wizardForm');

		wizard.getLayout().setActiveItem('transitionFourthPage');
	},

});
