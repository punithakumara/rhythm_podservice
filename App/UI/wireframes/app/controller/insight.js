Ext.define('AM.controller.insight',{
	extend : 'Ext.app.Controller',
	// stores : ['LogInViewerClients'],
	views: ['insight.List','insight.GeneralList','insight.WeekendList','insight.ClientHolidaysList','insight.TopPanel'],

	viewContent : function(clientID,from) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var insList = this.getView('insight.List').create();
		mainContent.removeAll();
		mainContent.add( insList );
		mainContent.doLayout();
	},
	
});
