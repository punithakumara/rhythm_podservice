Ext.define('AM.controller.WorTypes',{
	extend : 'Ext.app.Controller',
	stores: ["WorTypes"],
	views: ['WorTypes.List'],
	
	viewWorTypes : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var WorTypes = this.getView('WorTypes.List').create();
		mainContent.removeAll();
		mainContent.add(WorTypes);
		mainContent.doLayout();		
	},
	
	/*onCreateHoliday : function() {
		var view = Ext.widget('holidayAdd');
	},*/
	
});