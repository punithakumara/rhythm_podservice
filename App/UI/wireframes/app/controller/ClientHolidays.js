Ext.define('AM.controller.ClientHolidays',{
	extend : 'Ext.app.Controller',
	stores: ["clientHolidays"],
	views: ['ClientHolidays.List', 'ClientHolidays.Add'],
	
	viewHolidaysList : function() {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];	
		var holidaysList = this.getView('ClientHolidays.List').create();
		mainContent.removeAll();
		mainContent.add(holidaysList);
		mainContent.doLayout();		
	},
	
	onCreateHoliday : function() {
		var view = Ext.widget('holidayAdd');
	},
	
});