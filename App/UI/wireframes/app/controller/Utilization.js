Ext.define('AM.controller.Utilization',{
	extend : 'Ext.app.Controller',
	// stores : ['LogInViewerClients'],
	views: ['utilization.List','utilization.Add','utilization.Edit'],

	viewContent : function(clientID,from) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var utilList = this.getView('utilization.List').create();
		mainContent.removeAll();
		mainContent.add( utilList );
		mainContent.doLayout();
	},
	
	onUtilAdd : function() {
		var view = Ext.widget('utilizationAdd');
		
		var additionalElem = {
			items: [{
				xtype: 'boxselect',
				fieldLabel: "Billable Hrs",
				allowBlank: false
			}, {
				xtype: 'boxselect',
				fieldLabel: "Non Billable Hrs",
				allowBlank: false
			}, {
				xtype: 'boxselect',
				fieldLabel: "Admin Hrs",
				allowBlank: false
			}, {
				xtype: 'datefield',
				fieldLabel: "Campaign Start Date",
				allowBlank: false
			}, {
				xtype: 'textfield',
				fieldLabel: "Request Owners",
				allowBlank: false
			}]
		};
		Ext.getCmp('additionalFields').add(additionalElem);
		Ext.getCmp('utilizationForm').doLayout();
	},

	onUtilEdit : function(grid, row, col) {
		var mainContent = Ext.ComponentQuery.query('#mainContent')[0];
		var utilEdit = this.getView('utilization.Edit').create();
		mainContent.removeAll();
		mainContent.add( utilEdit );
		mainContent.doLayout();
	},
});
