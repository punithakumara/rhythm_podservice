Ext.define('Ext.ux.GroupComboBox', {
	extend: 'Ext.form.field.ComboBox',
	alias: ['widget.GroupComboBox'],
	/*
	 * @cfg groupField String value of field to groupBy, set this to any field in your model
	 */
	groupField: 'group',
	listConfig: {
		cls: 'grouped-list'
	},
	initComponent: function() {
		var me = this;
		me.tpl = new Ext.XTemplate([
			'{[this.currentGroup = null]}',
			'<tpl for=".">',
			'   <tpl if="this.shouldShowHeader('+me.groupField+')">',
			'       <div class="group-header">{[this.showHeader(values.'+me.groupField+')]}</div>',
			'   </tpl>',
			'   <div class="x-boundlist-item">{'+me.displayField+'}</div>',
			'</tpl>',
			{  
				shouldShowHeader: function(group){
					return this.currentGroup != group;
				},
				showHeader: function(group){
					this.currentGroup = group;
					return group;
				}
			}
		]);
		me.callParent(arguments);
	}
});