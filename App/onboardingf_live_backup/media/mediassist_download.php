<?php
require ('../config/mysql_crud.php');
require ("../config/excelwriter.class.php");

$db = new Database();

$excel=new ExcelWriter("Medi-Assist Details.xls");
if($excel==false)	
echo $excel->error;

$myArr=array("Employee ID","Employee Name","Date Of Join","Date of Birth","Marital Status","Blood Group","Location",
		"Qualification","Other Qualification","Emp Contact Number","Emergency Contact Number, Name & Relationship 1","Emergency Contact Number, Name & Relationship 2","PAN Details","Passport Number");
$excel->writeLine($myArr);

$sql="SELECT * from MediAssit";
$result = $db->sql($sql);
$result_array = $db->getResult();
if(is_array($result_array) && count($result_array)!=0)
{
	if(!array_key_exists(0, $result_array))
	{
		$tempResult = array();
		foreach($result_array as $k=>$v)
			$tempResult[0][$k] = ($v);
		$result_array = $tempResult;	
	}
	foreach($result_array as $key => $res)
	{
		$emergencyContactDetails1 = $res['EmergencyContactNumber1'].", ".$res['EmergencyContactName1'].", ".$res['EmergencyContactRelationship1'];
		$emergencyContactDetails2 = $res['EmergencyContactNumber2'].", ".$res['EmergencyContactName2'].", ".$res['EmergencyContactRelationship2'];
		
		$myArr=array($res['EmployID'], $res['EmployName'], date("d-m-Y",strtotime($res['DateOfJoin'])), date("d-m-Y",strtotime($res['DateOfBirth']))
					, $res['MaritalStatus'], $res['BloodGroup'], $res['Location'], $res['Qualification'], $res['OtherQualification']
					, $res['EmpContactNumber'], $emergencyContactDetails1, $emergencyContactDetails2, $res['PANDetails'], $res['PassportNumber']);
		$familyDetails = json_decode($res['FamilyDetails'], true);
		if(is_array($familyDetails))
		{
			foreach($familyDetails as $key => $val)
			{
				array_push($myArr, $val['Family Member'], $val['Family Member Name'], $val['Gender'],date("d-m-Y",strtotime($val['DOB'])));
			}
		}
		$excel->writeLine($myArr);
	}
}
?>