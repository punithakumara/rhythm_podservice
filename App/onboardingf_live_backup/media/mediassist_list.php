<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	<title>Theorem</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" language="javascript" src="../js/jquery-1.3.2.js"></script>
		<script type="text/javascript" language="javascript" src="../js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function() {
				$('#example').dataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bFilter": true,
					"bDestroy": true, 
					"sPaginationType": "full_numbers",
					"sDom": '<"top"lfp>rt<"bottom"ip><"clear">',
					"oLanguage": {
					"sLengthMenu": "Display _MENU_ records per page",
					"sZeroRecords": "No data found",
					"sInfo": "Showing _START_ to _END_ of _TOTAL_ records",
					"sInfoEmpty": "Showing 0 to 0 of 0 records",
					"sInfoFiltered": "(filtered from _MAX_ total records)",
					"sProcessing" : "",
					"oPaginate": {
						 "sFirst": "First",
						 "sLast": "Last",
						 "sPrevious": "Previous",
						 "sNext": "Next"
					}
				  },
					"sAjaxSource": "mediassist_server_processing.php"
					
				} );
			} );
		</script>
		<script language="javascript">
			function download()
			{
				$("#ajaxLoader").show();
				$.ajax({
					url:"mediassist_download.php",
					success:function(result){
						$("#ajaxLoader").hide();
						window.location='Medi-Assist Details.xls';
					}
				 });
					
			}
		</script>

	</head>
	
	<body id="dt_example">
	<div class="TopHeader">
		<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
	</div>
	<div id="container">
		<div class="theorem">
			<div class="exportBtn">
			<input id="Export To Excel" class="btTxt" type="button" value="Export To Excel" onClick="download();" /><div id="ajaxLoader" style="display:none;"><img src="../images/loader.gif" alt="Loading..."></div>
			</div>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
			<thead>
				<tr>
					<th width="10%">Employee ID</th>
					<th width="15%">Employee Name</th>
					<th width="10%">Date Of Join</th>
					<th width="10%">Date Of Birth</th>
					<th class="noSort" width="25%">Family Details</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="5" class="dataTables_empty">Loading data from server</td>
				</tr>
			</tbody>
		</table>
		</div>
	</div>

	<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>