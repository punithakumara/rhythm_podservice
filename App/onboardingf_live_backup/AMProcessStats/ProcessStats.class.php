<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '/usr/share/php5');
require_once ('../config/mysql_crud.php');
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');

class ProcessStats {
	public 				$db;
	public 				$tbody;
	public 				$mess;
	public function __construct() {
		$this->db	= new Database();

	}

	/**
	Function to fetch all associative managers email id and name.
	*/
	public function getAMDetails($amid = "") {
		$finaldata	= array();		
		$query		=	"SELECT GROUP_CONCAT(DISTINCT(p.ProcessID))			AS ProcessID,
						v.Name 						 						AS Vertical,
						p.Name						 						AS PROCESS,
						e.CompanyEmployID 				 					AS CompanyEmployID,
						e.EmployID					 						AS EmployIDs,
						CONCAT(e.FirstName, ' ', IFNULL(e.LastName, ''))	AS AmName,
						e.Email												AS MGEmail 
					 FROM employprocess ep,
						process p,
						employ e,
						vertical v,
						client c
					 WHERE ep.ProcessID 		= p.ProcessID
							AND p.ProcessStatus = 0
							AND ep.EmployID 	= e.EmployID
							AND e.RelieveFlag 	= 0
							AND v.VerticalID 	= p.VerticalID
							AND p.ClientID 		= c.ClientID
							AND ep.Grades 		= 4
							AND p.Name IS NOT NULL";

				if(isset($amid) && is_numeric($amid)) {
					$query	.= " AND e.CompanyEmployID = $amid ";
				}				
				$query		.= " GROUP BY e.CompanyEmployID ORDER  BY e.FirstName ";

				$result		= $this->db->sql($query);
				$rec		= $this->countRecursive($this->db->getResult());

				foreach ($rec as $key => $value) {
					if(isset($value['ProcessID'])) {

						$billcount	= $this->getAMBillableCount($value['ProcessID']);
						if(isset($billcount['billable']) && ($billcount['billable'] > 0)) {
							$finaldata[$key]['EmployIDs'] 			= $value['EmployIDs'];
							$finaldata[$key]['CompanyEmployID'] 	= $value['CompanyEmployID'];
							$finaldata[$key]['MGEmail'] 			= $value['MGEmail'];
							$finaldata[$key]['AmName'] 				= $value['AmName'];
						}
					}
				}
			if($amid == 'getall') {
				return $finaldata;
			} else {
				return array_shift($finaldata);
			}
		
	}


	public function getAMBillableCount($pid	= "") {
		$start_date			= date( 'Y-m-d', strtotime('-2 weeks sunday'));
		$end_date			= date( 'Y-m-d', strtotime('last saturday'));
		$rec				= "";
		if(isset($pid) && !empty($pid)) {
			$query			=  "SELECT SUM(Billable) AS billable 
							FROM   process_billablenonbillable_cron 
							WHERE  ProcessID IN ($pid) 
							       AND Date BETWEEN '".$start_date."' AND '".$end_date."' 
							       AND Billable != 0";
			$result		= $this->db->sql($query);
			$rec		= $this->db->getResult();
			if(!empty($rec)) {
				return $rec;
			} 
		}
	}


	/**
	Function to fetch all associative managers stats.
	*/

	public function getAmProcessStats($amid = "") {
			$query	=  "SELECT DISTINCT(p.ProcessID) AS ProcessID, v.Name AS Vertical, c.Client_Name AS CLIENT,
      						c.ClientID  AS ClientID,
      					p.Name   AS PROCESS, p.HeadCount  AS ApprovedFTE, p.ForcastNRR AS ForecastNRR,
      						p.ForcastRDR AS ForcastRDR,
      						(SELECT SUM(
      									CASE 
      										WHEN 
           										BillableType = 'FTE Contract' 
          									THEN 
           										IF(BillablePercentage = 100, 1,
           											IF(BillablePercentage  <= 25, 0.25,
           												IF(BillablePercentage = 50, 0.5, 0.75)
            										)
           										)
           									END)
           					FROM employprocess, employ WHERE Billable	= 1 AND PrimaryProcess = 1
       					AND employprocess.ProcessID = p.ProcessID AND employprocess.EmployID = employ.EmployID
       						AND employ.RelieveFlag = 0 AND employprocess.Grades < 4) AS FTEContractCount,
        				(SELECT 
        					SUM(IF(BillablePercentage = 100 ,1, 
        							IF((BillablePercentage IS NULL AND billable = 1) OR 
        						(BillableType = 'FTE' OR BillableType = 'Approved Buffer'),
        					IF(BillablePercentage = 25 ,0.25 , 
        						IF(BillablePercentage = 50 ,0.5, 
        					IF(BillablePercentage = 75, 0.75, 1) ) ),0 ))) 
         				AS Billablecount FROM employprocess,employ WHERE PrimaryProcess = 1 
       						AND Billable	= 1 AND ProcessID	= p.ProcessID 
       					AND employprocess.EmployID	= employ.EmployID AND employ.RelieveFlag = 0
       						AND BillableType IN ('FTE','Approved Buffer')) AS Billablecount,
           				e.CompanyEmployID AS EmployID, CONCAT(e.FirstName, ' ', IFNULL(e.LastName, '')) 
           					AS 'Associate Manager', e.Email FROM employprocess ep, process p, employ e,
            			vertical v, client c WHERE ep.ProcessID   = p.ProcessID AND p.ProcessStatus = 0
       						AND ep.EmployID	= e.EmployID AND e.RelieveFlag  = 0
       					AND v.VerticalID  	= p.VerticalID AND p.ClientID   = c.ClientID
       						AND ep.Grades	= 4 AND p.Name IS NOT NULL";
					
			if(isset($amid) && is_numeric($amid)) {
				$query	.= " AND e.CompanyEmployID = $amid ";
			}
			$query		.= " ORDER BY c.Client_Name,p.Name ";
			
			$result		 = $this->db->sql($query);
		
			return $this->countRecursive($this->db->getResult()); 
	}

	/**
	Function to fetch vertical manager name and vertical email id for the respective AM's
	*/

	public function getVMDetails($amid = "") {
		$query	= "SELECT Concat(Ifnull(employ.FirstName, ''), '', Ifnull(employ.LastName, '')) 
				    AS 
				    vmname, 
				    employ.Email 
				    AS vmemail 
					FROM   employ 
					       INNER JOIN employ st2 
					               ON ( employ.EmployID = st2.Primary_Lead_ID ) 
					WHERE  st2.CompanyEmployID IN ( $amid ) 
					LIMIT  0, 1 ";
				
			
		$result		= $this->db->sql($query);
		return $this->db->getResult();
	}

	/**
	Function to convert associative array to multidimensional array
	*/

	public function countRecursive($checkarray = '') {
		if(!empty($checkarray) && is_array($checkarray)) {
			if (count($checkarray) == count($checkarray, COUNT_RECURSIVE)) {
				$arraymodify	= array();
				$arraymodify[] 	= $checkarray;
				$checkarray		= $arraymodify;
			} else {
				$checkarray 	= $checkarray;
			}
			return $checkarray;
		}
	}
	
	public function getProcessStatsTableView($amid = "", $td = "") {
		$this->mess			= "";
		$Total_AFTE 		= 0;
		$Total_BC 			= 0;
		$Total_ActualHour	= 0;
		$Total_ExpHour 		= 0;
		$TotalNRR 			= 0;
		$TotalFTECont		= 0;
		$TotalRDR 			= 0;
		$totalRow			= "";
		$totalutilization	= 0;

		if(isset($amid) && (!empty($amid) && ($amid != 'getallrec'))) {
			$stats			= $this->countRecursive($this->getUtilizationStats($amid));
		} elseif(($amid == 'getallrec') || ($amid == "")) {
			$stats			= $this->countRecursive($this->getUtilizationStatsAdmin());
		}

		// constant values to calculate Total for last row
		
		if(!empty($stats) && is_array($stats)) {
			foreach ($stats as $result) {
				if($result['ApprovedFTE'] != 0 || $result['ForecastNRR'] != 0
					|| $result['ForcastRDR'] != 0 || $result['Billablecount'] !=0 ) {
					$intpart	= floor( $result['Billablecount'] );    
					$fraction	= $result['Billablecount'] - $intpart;

					if($fraction == 0) {
						$billableCount 		= floor($result['Billablecount']);
					} else {			
						$billableCount 		= $result['Billablecount'];
					}

					if(isset($result['FTEContractCount']) && !empty($result['FTEContractCount'])) {
						$fteContCount		= "(".$result['FTEContractCount'].")";
					} else {
						$fteContCount		= "";
					}	

					$result['actual_hour'] 			= isset($result['actual_hour'])?$result['actual_hour']:'-';
					$result['expected_hour'] 		= isset($result['expectedHours'])?$result['expectedHours']:'-';
					$result['AssociateManager']	 	= isset($result['Associate Manager'])?$result['Associate Manager']:'';
					$result['weekly_utilization']	= isset($result['weekly_utilization'])?$result['weekly_utilization']:'0';
					$td	= "";
					if($amid != 'getallrec') {
						if(isset($result['PROCESS'])) {
							$td	= "<td>".$result['PROCESS']."</td>";
						}
					}
					if(isset($result['VerticalName'])) {
						$vn	= '<td align="left">'.$result['VerticalName'].'</td>';
					} else {
						$vn	= "";
					}
					
					
					$diffCount					= $result['ApprovedFTE'] - $billableCount;				
					$row 						= '<tr>';
					$row_bg 					= '<tr style="background:yellow">';
					$this->mess			   	   .= (($diffCount!=0) ? $row_bg : $row).
													'<td>' .$result['CLIENT'].'</td>
													 '.$vn.'
											 		 '.$td.'
											 		<td align="center">'.$result['ApprovedFTE'].'</td>
											 		<td align="center">'.$result['ForecastNRR'].'</td>
											 		<td align="center">'.$result['ForcastRDR'].'</td>
											 		<td align="center">'.$billableCount.'&nbsp;'.$fteContCount.'</td>
											 		<td align="center">'.$diffCount.'</td>
											 		<td align="center">'.$result['actual_hour'].'</td>
											 		<td align="center">'.$result['expected_hour'].'</td>
											 		<td align="center">'.$result['weekly_utilization'].'%'.'</td>
												</tr>';
					

					$Total_AFTE 		+= $result['ApprovedFTE'];
					$TotalNRR 			+= $result['ForecastNRR'];
					$TotalRDR 			+= $result['ForcastRDR'];
					$Total_BC 			+= $result['Billablecount'];
					$TotalFTECont       += $result['FTEContractCount'];
					
					$Total_ActualHour 	+= $result['actual_hour'];
					if($result['actual_hour'] != '-') {
						$Total_ExpHour 		+= $result['expected_hour'];
					}
					
					$totalRow ="";
				}
			}

			if($Total_ExpHour > 0) {
				$totalutilization	= round(($Total_ActualHour/$Total_ExpHour)*100,2);
			}
			if(!empty($TotalFTECont)) {
					$TotalFTECont			= "&nbsp(".$TotalFTECont.")";
				} else {
					$TotalFTECont			= "";
				}	
			$totalRow .= '<tr><td colspan="2" align="center"><b>Total</b></td>';
			$totalRow .= '<td align="center"> <b>'.$Total_AFTE.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$TotalNRR.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$TotalRDR.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$Total_BC.$TotalFTECont.'</b> </td>';
			$totalRow .= '<td align="center"><b>'.($Total_AFTE - $Total_BC).'</b></td>';
			$totalRow .= '<td align="center"><b>'.$Total_ActualHour.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$Total_ExpHour.'</b></td>';
			$totalRow .= '<td align="center"><b>'.$totalutilization.'%</b></td>';
			$totalRow .= '</tr>';
			return $this->mess.$totalRow;
		}
	}



	public function getTaskData($data = array())
	{
		$actualhour		= array();
		$start_date		= date( 'Y-m-d', strtotime('-2 weeks sunday'));
		$end_date		= date( 'Y-m-d', strtotime('last saturday'));
		

		$query			= " SELECT  GROUP_CONCAT(DISTINCT TaskDate) AS taskDate, 
								SUM(TIME_TO_SEC(Num_Hours)) AS actual_hour,
							SUM(expectedHours) AS expectedHours
								FROM   	employwise_utilization_process eup 
							WHERE  	TaskDate  BETWEEN '".$start_date."' AND  '".$end_date."'
								AND clientId   = ".$data['ClientID']. "";

		if(isset($data['VerticalID']) && !empty($data['VerticalID'])) {
			$query		.= " AND VerticalID IN  (".$data['VerticalID'].")";
		}
		if(isset($data['ProcessID']) && !empty($data['ProcessID'])) {
			$query		.= " AND ProcessID  = ".$data['ProcessID']. "";
		}
		
		$result			= $this->db->sql($query);
		$resp			= $this->db->getResult();
		return $resp;
		
	}
	

	public function seconds2minutes($ss) {

		if(isset($ss) && !empty($ss)) {
			$init        = $ss;
			$hours       = floor($init / 3600);
			$minutes     = floor(($init / 60) % 60);
			$FinalHours  = $hours . '.' . $minutes;
			return $FinalHours;
		}
	}

	public function getUtilizationStats($amid = "")
	{
		$stats		= $this->getAmProcessStats($amid);
		$resp		= array();
		if(!empty($stats) && is_array($stats)) {
			foreach ($stats as &$statvalue) {
				$para	= array(
						'ClientID'		=> $statvalue['ClientID'], 
						'ProcessID'		=> $statvalue['ProcessID']
					);

				$resp 								= $this->getTaskData($para);
				$statvalue['actual_hour'] 			= $this->seconds2minutes($resp['actual_hour']);
				
				if(isset($resp['actual_hour']) && !empty($resp['actual_hour'])) {
					$statvalue['expectedHours']			= $resp['expectedHours'];
					if(isset($resp['expectedHours']) && $resp['expectedHours'] != 0) {
						$statvalue['weekly_utilization']	= round(($statvalue['actual_hour']/ 
																			  $resp['expectedHours']) * 100, 2);
					} else {
						$statvalue['weekly_utilization']	= 0;
					}
				}
			}
			return $stats;
		}
	}
/**
	STATS FOR SUPER ADMIN
*/	
	public function getAmProcessStatsAdmin()
	{
		$query = "SELECT DISTINCT ( c.ClientID )    AS ClientID, 
  p.ProcessID                             AS ProcessID, 
  v.Name                        AS Vertical,
  GROUP_CONCAT(DISTINCT v.VerticalID )        AS VerticalID, 
  GROUP_CONCAT(DISTINCT v.Name  )          AS VerticalName,   
  c.Client_Name                           AS CLIENT, 
  SUM(p.HeadCount)                        AS ApprovedFTE, 
  SUM(p.ForcastNRR)                       AS ForecastNRR, 
  SUM(p.ForcastRDR)                       AS ForcastRDR, 
  
  (CASE WHEN (SELECT TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM  SUM( IF(BillablePercentage = 100 ,1,  
           IF((BillablePercentage IS NULL AND Billable = 1) OR (BillableType = 'FTE' OR BillableType = 'Approved Buffer'),
    IF(BillablePercentage = 25 ,0.25 ,IF(BillablePercentage = 50 ,0.5, IF(BillablePercentage = 75, 0.75, 1) )
       ),0
    )         
                   ))  ))  AS Billedcount

  FROM employprocess ep,employ,
  process sp
  WHERE ep.PrimaryProcess = 1
  AND ep.Billable = 1
  AND ep.ProcessID = sp.ProcessID
  AND ep.EmployID = employ.EmployID
  AND employ.RelieveFlag = 0
  AND sp.ClientID = c.ClientID

  AND ep.BillableType IN ('FTE','Approved Buffer'))  IS NULL THEN 0 ELSE 
  (SELECT TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM  SUM( IF(BillablePercentage = 100 ,1,  
           IF((BillablePercentage IS NULL AND Billable = 1) OR (BillableType = 'FTE' OR BillableType = 'Approved Buffer'),
    IF(BillablePercentage = 25 ,0.25 ,IF(BillablePercentage = 50 ,0.5, IF(BillablePercentage = 75, 0.75, 1) )
       ),0
    )         
                   ))  ))  AS Billedcount

  FROM employprocess ep,employ,
  process sp
  WHERE ep.PrimaryProcess = 1
  AND ep.Billable = 1
  AND ep.EmployID = employ.EmployID
  AND employ.RelieveFlag = 0
  AND ep.ProcessID = sp.ProcessID
  AND sp.ClientID = c.ClientID
  AND ep.BillableType IN ('FTE','Approved Buffer'))

  END) AS Billablecount,
  (SELECT SUM(CASE 
          WHEN 
           sep.Billabletype = 'FTE Contract' 
          THEN 
           IF(sep.BillablePercentage = 100, 1,
            IF(sep.BillablePercentage  <= 25, 0.25,
             IF(sep.BillablePercentage = 50, 0.5, 0.75)
            )
           )  
         END)
  FROM   employprocess sep, 
  process sp1 
  WHERE  sep.Billable = 1 
  AND sep.PrimaryProcess = 1 
  AND sep.ProcessID = sp1.ProcessID 
  AND sp1.ClientID = c.ClientID) AS FTEContractCount 
  FROM   process p, 
  vertical v, 
  client c 
  WHERE  p.ProcessStatus = 0 
  AND v.VerticalID = p.VerticalID 
  AND p.ClientID = c.ClientID 

  GROUP  BY c.ClientID 
  ORDER  BY c.Client_Name";
		$result		 = $this->db->sql($query);
		return $this->countRecursive($this->db->getResult());
		
	}

	public function getUtilizationStatsAdmin($amid = "")
	{

		$stats		= $this->getAmProcessStatsAdmin();
		// echo "<pre>"; print_r($stats); exit;
		$resp		= array();
		if(!empty($stats) && is_array($stats)) {
			foreach ($stats as &$statvalue) {
				$para	= array(
						'ClientID'			=> $statvalue['ClientID'], 
						'VerticalID'		=> $statvalue['VerticalID']
					);
				$resp 									= $this->getTaskData($para);
			
				$statvalue['actual_hour'] 				= $this->seconds2minutes($resp['actual_hour']);
				if(isset($resp['actual_hour']) && !empty($resp['actual_hour'])) {
					$statvalue['expectedHours']			= $resp['expectedHours'];
					if(isset($resp['expectedHours']) && $resp['expectedHours'] != 0) {
						$statvalue['weekly_utilization']	= round(($statvalue['actual_hour']/ 
																			  $resp['expectedHours']) * 100, 2);
					} else {
						$statvalue['weekly_utilization']	= 0;
					}
				}
			}
			return $stats;
		}
	}
}

$obj	= new ProcessStats();

//generic for all the AM's
if(isset($_POST['assmgr']) && ($_POST['assmgr'] == 'getallrec')) {
	$statsdata	= $obj->getProcessStatsTableView($_POST['assmgr']);
	$encode		= array(
		'statsdata' => $statsdata
		);
	echo json_encode(array_map('utf8_encode', $encode));
}

else if(isset($_POST['assmgr']) && ($_POST['assmgr'] != 'getallrec')) {
// for single AM's stats
	$amdata		= $obj->getAMDetails($_POST['assmgr']);
	$vmdata		= $obj->getVMDetails($_POST['assmgr']);

	$statsdata	= $obj->getProcessStatsTableView($_POST['assmgr']);

	$encode		= array(
		'amdata' 	=> $amdata, 
		'vmdata' 	=> $vmdata, 
		'statsdata' => $statsdata
		);

	echo json_encode($encode);
}

?>