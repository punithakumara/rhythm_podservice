<?php
class AuthLDAP {
	
	public $connection;
	public $ldapErrorCode;
	public $ldapErrorText;
	public $host_name;
	public $result;
	public $domain_name;
	public $search_field;
	public $ldap_username;
	public $ldap_passowrd;
	public $ldap_search_fields;
	public $ldap_field_attribute = array();
	public $ldap_search_filter;
	public $dn;
	
	public function __construct() {
		// Load the configuration
		require '../config/ldap-config.php';
		$this->host_name = $host_name;
		$this->domain_name = $domain_name;
		$this->ldap_username = $ldap_username;
		$this->ldap_password = $ldap_password;
		$this->ldap_search_field = $ldap_search_field;
		$this->dn = $ldap_base_dn;
		$this->ldap_field_attribute = $attributes;
		$this->ldap_search_filter = $search_filter;
	}
	
	
	
	public function connect() {
		$this->connection = ldap_connect($this->host_name);
		if($this->connection)
			return true;
		else
		{
		   $this->ldapErrorCode = -1;
           $this->ldapErrorText = "Unable to connect to any server";
           return false;
		}
	}
	
	public function close() {
        if ( !@ldap_close( $this->connection)) {
            $this->ldapErrorCode = ldap_errno( $this->connection);
            $this->ldapErrorText = ldap_error( $this->connection);
            return false;
        } else {
            return true;
        }
    }
    
    public function authenticateUser($uname, $pass) {
    	$checkDn = "$uname$this->domain_name";
    	
		// return $checkDn;
		$this->result = @ldap_bind($this->connection,$checkDn,$pass);
    	if($this->result)
    	{
    		if($userInfo = $this->searchUser($uname,$this->ldap_search_fields))
    			return 	$userInfo;
    		else
    			return false;	
    	}
    	else {
    	   $this->ldapErrorCode = ldap_errno( $this->connection);
           $this->ldapErrorText = ldap_error( $this->connection);
           return false;
     	}  	
    }
    
    public function checkLoginID($userid)
    {
    	$checkDn = "$userid$this->domain_name";
    	// return $checkDn; 
		$this->result = @ldap_bind( $this->connection,$checkDn);
    	if($this->result)
    	{
    		if($userInfo = $this->searchUser($userid,$this->ldap_search_fields))
    			return 	$userInfo;
    		else
    			return false;	
    	}
    	else {
    	   $this->ldapErrorCode = ldap_errno( $this->connection);
           $this->ldapErrorText = ldap_error( $this->connection);
           return false;
     	}  	
    	
    }
    
    /**
     * Search the users from LDAP
     * @param $searchFor (Required) search the users based on the string
     * @param $seachBy (Required) means Search Field(givenname','mail','samaccountname') 
     * 		  
     */
    public function searchUser($searchFor,$seachBy) {
    	 ldap_set_option($this->connection, LDAP_OPT_PROTOCOL_VERSION, 3);  
 		 ldap_set_option($this->connection, LDAP_OPT_REFERRALS, 0);
 		 error_reporting (E_ALL ^ E_NOTICE);
 		
 		 if($seachBy=="")//if $seachBy is empty then we are getting field from ldap config
 		 	$searchField =  $this->ldap_search_field;
 		 else
 		 	$searchField = 	$seachBy;
 		 
 		 $this->result = @ldap_bind($this->connection,$this->ldap_username,$this->ldap_password);
 		 
 		 if($this->result) {
 		 	
			 $filter = "(&$this->ldap_search_filter ($searchField=$searchFor))"; //Wildcard is * Remove it if you want an exact match
 		 	 //$filter="($this->ldap_search_field=$searchFor*)"; //Wildcard is * Remove it if you want an exact match
 		 	 $search = @ldap_search($this->connection, $this->dn, $filter, $this->ldap_field_attribute);
  			 $entries = @ldap_get_entries($this->connection, $search);
  			 $name = $loginid = $email = "";
  		  	 for ($x=0; $x<$entries['count']; $x++)
       		 {
       		 	if(!empty($entries[$x]['givenname'][0]))
        			$name = trim($entries[$x]['givenname'][0]);
        		if(!empty($entries[$x]['samaccountname'][0]))	
        			$loginid =  trim($entries[$x]['samaccountname'][0]);
 				if(!empty($entries[$x]['mail'][0]))       		
        			$email = trim($entries[$x]['mail'][0]);
        		
        		$users[] =  array( 'Name'=>$name,"LoginID"=>$loginid,"Email"=>$email);
       		 }
       		 return ($x==0) ? false : $users;
 		 }
 		 else {
 		   $this->ldapErrorCode = ldap_errno( $this->connection);
           $this->ldapErrorText = ldap_error( $this->connection);
           return false;
 		 }
    }
}
$obj = new AuthLDAP();
?>