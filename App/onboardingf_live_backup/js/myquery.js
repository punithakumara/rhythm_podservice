jQuery(function(){
	// innovation validations	
	jQuery("#innova_emp_name").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#innova_emp_id").validate({
		expression: "if (!isNaN(VAL) && VAL) return true; else return false;"		
	});	
	jQuery("#innova_email").validate({
		expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;"
	});
	jQuery("#innova_email").validate({
		expression: "if (VAL != '@theoreminc.net') return true; else return false;"
	});+
	jQuery("#innova_vertical").validate({
		expression: "if (VAL) return true; else return false;"	
	});	
	jQuery("#innova_designation").validate({
		expression: "if (VAL) return true; else return false;"	
	});	
		
	//new joinee validations
	jQuery("#FirstName").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#LastName").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#MotherName").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#FatherName").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#BloodGroup").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#QualificationID").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#DateOfJoin").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Grade").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#releventYear").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#releventMonth").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Address1").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#City").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#State").validate({
		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
	});
	jQuery("#AlternateEmail").validate({
		expression: "if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/)) return true; else return false;"
	});
	jQuery("#Zip").validate({
		expression: "if (VAL) return true; else return false;"
	});
	jQuery("#Mobile").validate({
		expression: "if (VAL) return true; else return false;"
	});
	// same as above checkbox click
	$("#AddressSame").click(function(){
		if($(this).is(":checked"))
		{
			$("#Address2").val($("#Address1").val());
			$("#City1").val($("#City").val());
			$("#State1").val($("#State").val());
			$("#Zip1").val($("#Zip").val());
			$("#Address2, #City1, #State1, #Zip1").attr('readonly', true);
		}
		else
		{
			$("#Address2, #City1, #State1, #Zip1").val("");
			$("#Address2, #City1, #State1, #Zip1").attr('readonly', false);
		}
	});
	
	//attach keypress to input
    $('.number').keydown(function(event) {
        // Allow special chars + arrows 
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
            || event.keyCode == 27 || event.keyCode == 13 
            || (event.keyCode == 65 && event.ctrlKey === true) 
            || (event.keyCode >= 35 && event.keyCode <= 39)){
                return;
        }else {
            // If it's not a number stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
    $("#Age").focus(function(){
    	bDay = $("#DateOfBirth").val();
    	years = CalAge(bDay);
    	$("#Age").val(years);
    });
    $(".validTextarea").blur(function(){
    	isTextAreaNull();
    });
});

function CalAge(bDay) {
    var now = new Date();
    bD = bDay.split('/');
    if (bD.length == 3) {
    	born = new Date(bD[2], bD[1] * 1 - 1, bD[0]);
    	years = Math.floor((now.getTime() - born.getTime()) / (365.25 * 24 * 60 * 60 * 1000));
    	return years;
    }
}
function changeDesignation(grade)
{
	if(grade!="")
	{
		$.ajax({
			url:"index.php",
			type:'POST',
			data:{grade:grade},
			success:function(result){
		    	$("#DesignationID").html(result);
		    	jQuery("#DesignationID").validate({
		    		expression: "if (isNaN(VAL) && VAL) return true; else return false;"
		    	});
		  }
		});
	}
	else
	{
		$("#DesignationID").html("<option value=''>Select Grade First</option>");
	}
}
/*
 * This function checks if there is at-least one element fill in a group of textarea
 * @id: The ID of the textarea group
 */
function isTextAreaNull(){
    var ReturnVal = false;
    $("#ValidTextarea").find('textarea').each(function(){
       if ($(this).val())
       {
    	   validateTextbox(this.id);
    	   ReturnVal = true;
       }
    });
    if(ReturnVal){
    	$("#ValidTextarea").removeClass('ErrorField');
    	$(".textareaError").remove();
    }
    else{
    	$("#ValidTextarea").addClass('ErrorField');
    	if( !($('.textareaError').length) )
    		$("#ValidTextarea").after('<span class="ValidationErrors textareaError">Please fill atleast one</span>');
    }
    return ReturnVal;
}
function validateTextbox(id)
{
	if(id=="ValidTextarea_1")
	{
		if ($("#ValidTextarea_1").val()) 
		{
			 jQuery("#InputGroup1").validate({
                 expression: "if (isText(SelfID)) return true; else return false;",
                 message: "Please fill atleast one"
             });
		}
	}
	if(id=="ValidTextarea_2")
	{	
		if ($("#ValidTextarea_2").val()) 
		{
			 jQuery("#InputGroup2").validate({
                 expression: "if (isText(SelfID)) return true; else return false;",
                 message: "Please fill atleast one"
             });
		}
	}
	if(id=="ValidTextarea_3")
	{
		if ($("#ValidTextarea_3").val()) 
		{
			 jQuery("#InputGroup3").validate({
                 expression: "if (isText(SelfID)) return true; else return false;",
                 message: "Please fill atleast one"
             });
		}
	}
}
function isText(id){
    var ReturnVal = false;
    $("#" + id).find('input[type="text"]').each(function(){
    	if ($(this).val()) 
            ReturnVal = true;
    });
    return ReturnVal;
}