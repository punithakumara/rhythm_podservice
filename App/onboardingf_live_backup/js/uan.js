jQuery(function(){
	// validations	
	jQuery("#UAN").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#UAN_DocumentType").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	
	jQuery("#UAN_DocumentNumber").validate({
		expression: "if (VAL.match(/^[a-zA-Z0-9\\_\\-\\.\/]*$/)) return true; else return false;"	
		//expression: "if (VAL) return true; else return false;"		
	});
	
	jQuery("#UAN_Name").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#UAN_EducationalQualification").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#PhysicallyHandicapped").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#UAN_Gender").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#International_Worker").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	jQuery("#UAN_Martial_Status").validate({
		expression: "if (VAL) return true; else return false;"		
	});
	UAN_DocumentType = document.getElementById('UAN_DocumentType').value;
    if(UAN_DocumentType=="T" || UAN_DocumentType=="D") {
	jQuery("#UAN_ExpiryDate").validate({
		expression: "if (VAL) return true; else return false;"
	});
	}
});

function checkIFSC() {
	if(document.getElementById('UAN_IFSCCode').value != '') {
		$("#UAN_IFSCCode").removeClass('ErrorField');
	}
}

function checkHandicapType(){
 	if(document.getElementById('UAN_HandicapType').value != '') {	
		document.getElementById('UAN_HandicapType').style.border = "";				
	}
}
	
function ExpiryDate() {
		var expiry = document.getElementById('UAN_ExpiryDate').value;
		UAN_DocumentType = document.getElementById('UAN_DocumentType').value;
		if((UAN_DocumentType=="T" || UAN_DocumentType=="D") && expiry=="") {
			return false;
		}
		var ifscCode = document.getElementById('UAN_IFSCCode').value;
		var sptdate = String(expiry).split("/");
		var myDay = sptdate[0];
		var myMonth = sptdate[1];
		var myYear = sptdate[2];
		var expirydate = myYear + "/" + myMonth + "/" + myDay;
		var today = new Date(); 
		var dd = today.getDate(); 
		var mm = today.getMonth()+1;
		if(mm<10){mm='0'+mm}
		var yyyy = today.getFullYear();
		var currentDate = String(yyyy+"\/"+mm+"\/"+dd);
         UAN_DocumentType = document.getElementById('UAN_DocumentType') ;
		if (expirydate < currentDate){ 
			$("#UAN_ExpiryDate").addClass('ErrorField');
			return false;
		}
		else{
			$("#UAN_ExpiryDate").removeClass('ErrorField');
			return true;
		} 		   
	}
	   
	function findselected()
	{		
		$result = false;
		UAN_DocumentType = document.getElementById('UAN_DocumentType');		
		UAN_IFSCCode = document.getElementById('UAN_IFSCCode');		
		UAN_ExpiryDate = document.getElementById('UAN_ExpiryDate');		
		if (UAN_DocumentType.value == "B") 
		{
			$("#UAN_IFSCCode").addClass('ErrorField');
			UAN_IFSCCode.disabled = false ;										
		}
		else{	
		
			UAN_IFSCCode.disabled = true ;
			$("#UAN_IFSCCode").removeClass('ErrorField');
			document.getElementById('UAN_IFSCCode').value ="";			
			$result = true;			
		}
		if (UAN_DocumentType.value == "T" || UAN_DocumentType.value == "D" ) {			
				UAN_ExpiryDate.disabled = false	;				
				$("#UAN_ExpiryDate").addClass('ErrorField');
				document.getElementById('UAN_ExpiryDate').value ="";
		}
		else{				
			UAN_ExpiryDate.disabled = true;
			$("#UAN_ExpiryDate").removeClass('ErrorField');			
			$result = true;
		}	
		return $result ;		
	}
	
	
	function fillNameTextbox() 
	{
		var e = document.getElementById("UAN");
		var optionText = e.options[e.selectedIndex].text;
		document.getElementById('UAN_Name').value  = optionText.substring(14);
		$("#UAN_Name").removeClass('ErrorField');
	}
	function enableHandicapType()
	{
		UAN_HandicapType = document.getElementById('UAN_HandicapType');
		UAN_HandicapType.disabled = false;
	    document.getElementById('UAN_HandicapType').style.border = "solid 1px red";	   		
	}
	function disableHandicapType()
	{
		UAN_HandicapType = document.getElementById('UAN_HandicapType');		
	    UAN_HandicapType.disabled = true ;
	    document.getElementById('UAN_HandicapType').value ="";
	    document.getElementById('UAN_HandicapType').style.border = "";		   
	}

