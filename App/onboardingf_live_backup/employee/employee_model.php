<?php
//error_reporting(0);
session_start();
require ('../config/mysql_crud.php');

	function getEmployeegrade($EmployID)
    {
		$db = new Database();
        $sql = "SELECT d.grades,e.EmployID, e.DesignationID, e.email FROM employ e join designation d on d.DesignationID = e.DesignationID  where e.EmployID ='" . $EmployID . "'";
        
        $result = $db->sql($sql);
		$result_array = $db->getResult();
		if(is_array($result_array) && count($result_array)!=0)
		{
            return $result_array;
        }
        return '';
    }
    
    function getPreviousSupervisors($grades, $EmployID)
    {
        $db = new Database();
        $sql = "SELECT employ.`Email`, CONCAT(IFNULL(`employ`.`FirstName`, ''),' ',IFNULL(`employ`.`LastName`, '')) AS Name FROM employ
                    JOIN designation ON employ.designationid = designation.designationid
                    WHERE employ.RelieveFlag=0 AND designation.grades >" .$grades . "  ORDER BY FirstName";
        
        $db->sql($sql);      
        $data = getEmployeeSupervisors($EmployID); //function to get employee supervisors  
        
        //return $db->getResult();
        return $data;
    }
    
    function getCurrentSupervisors($grades, $EmployID)
    {
    	$reporteesID = array();
    	$reporteesID[] = getDirectSupervisor($EmployID);
    	
    	$reporteesID = array_unique($reporteesID);
    	if(!empty($reporteesID))
    	{
    		foreach ($reporteesID as $repID)
    		{
    			$finalData[] = getSupervisorDetails($repID); //get supervisors details
    		}
    	}
    	return $finalData;
    }
    
    //get Reporting Manager
    function getReportingManager($EmployID)
    {
    	$managerDetails  = array();
    	$reporteeID = getDirectSupervisor($EmployID);   	
    	$managerID =  getDirectSupervisor($reporteeID);
    	$managerDetails[] = getSupervisorDetails($managerID);    	
    	return $managerDetails;   	
    }
    
    /* function to get employee supervisors */
    function getEmployeeSupervisors($EmployID)
    {
    	$db = new Database();
    	$reporteesData = array();
    	$finalData = array();    	
    	$sql = "SELECT  transitionData FROM employ_transition  where employId ='" . $EmployID . "'";
    	$db->sql($sql);
    	$transitionData = $db->getResult();
    	
    	if (!empty($transitionData))
    	{    	                        	
        	$unserializedData = unserialize($transitionData['transitionData']);          	 
        	
        	$threeMonthsAgoDate = date("d-m-Y",mktime(0,0,0,date("m")-3,date("d")));         	
        	$dataLength = count($unserializedData);        	 
        	for($i=0; $i<$dataLength;$i++)
        	{       		
	        	if($unserializedData[$i]['transition_date'] != '')	
	        	{
	        		if(strtotime($unserializedData[$i]['transition_date']) >= strtotime($threeMonthsAgoDate))
	        		{
	        			if ($unserializedData[$i]['to_reporting'] != '')
	        			{
	        				//$reporteesID[ ] = $unserializedData[$i]['to_reporting'];
	        				$k=0;
	        				for($j=0; $j<$dataLength; $j++)
	        				{
	        					if($unserializedData[$j]['transition_date'] != '')
	        					{
	        						if(strtotime($unserializedData[$j]['transition_date']) >= strtotime($threeMonthsAgoDate))
	        						{
	        							if ($unserializedData[$i]['to_reporting'] != '')
	        							{
	        								if($unserializedData[$i]['to_reporting'] == $unserializedData[$k]['from_reporting'])
	        								{
	        									$fromDate = $unserializedData[$i]['transition_date'];
	        									$toDate = $unserializedData[$k]['transition_date'];
	        									$reportingId = $unserializedData[$i]['to_reporting'];
	        									
	        									if($fromDate <= $toDate)
	        									{
	        										$reporteesData[$k]['fromDate'] = $fromDate;
	        										$reporteesData[$k]['toDate'] = $toDate;
	        										
	        										$date1 = new DateTime($fromDate);
	        										$date2 = new DateTime($toDate);	        										
	        										$diff = $date2->diff($date1)->format("%a");	        										
	        										$reporteesData[$k]['diffDays'] = $diff;	        										
	        										$reporteesData[$k]['employID'] = $unserializedData[$i]['to_reporting'];;
	        										
	        									}	        										        									        								}
	        							}
	        						}
	        					}
	        					$k++;
	        				}	        					        				
	        			}
	        		}
	        	}	        		
        	}       	        	       	            	        
    	} 
    	    	   	       
    	$maxData = array_reduce($reporteesData, function ($a, $b) {
    		return @$a['diffDays'] > $b['diffDays'] ? $a : $b;
    	});   	
        
        if(!empty($reporteesData))
        {
        	foreach ($reporteesData as $key=>$value)
        	{        		
        		if( $maxData['diffDays'] == $value['diffDays'])
        		{
        			$finalData[] = getSupervisorDetails($value['employID']); //get supervisors details
        		}        		
        	}
        }               
        return $finalData;      		
    }
    
    //function to get supervisor details by $repID
    function getSupervisorDetails($repID)
    {
    	$db = new Database();
    	if($repID != '')
    	{
    		$sql = "SELECT `CompanyEmployID` as EmployID, CONCAT(IFNULL(`FirstName`, ''), ' ', IFNULL(`LastName`, '')) AS Name, `Email`  FROM `employ` WHERE `EmployID` ='" . $repID . "'";
    		$db->sql($sql);
    	    return $db->getResult();    	
    	}
    	 
    }
    
    //function to get direct supervisor ID
    function getDirectSupervisor($EmployID)
    {
    	$db = new Database();  	
    	$sql = "SELECT `Primary_Lead_ID` FROM `employ` WHERE `EmployID` ='" . $EmployID . "'";
    	$db->sql($sql);
    	$directSupervisorData = $db->getResult();    	
    	return $directSupervisorData['Primary_Lead_ID'];
    }
    
    
    function getNotificationDetails($notificationID)
    {
    	$db = new Database();
        $sql = "SELECT * FROM notification WHERE NotificationID=".$notificationID;
        
        $db->sql($sql);
        
        return $db->getResult();
    }
    
    function getEmployeesWorkingUnder($CompanyEmployID,$Email)
    {
    	$db = new Database();
    	$MyDatasql = "SELECT EmployID FROM employ WHERE CompanyEmployID = $CompanyEmployID AND Email = '$Email' AND RelieveFlag=0";
    	$db->sql($MyDatasql);
        $EmployID = $db->getResult();
        $empid = $EmployID['EmployID'];
    	if(isset($empid))
        {
        	$_SESSION['Cmpetency_Primary_Lead_ID'] = $empid;
        	$sql = "SELECT EmployID, CONCAT(IFNULL(`FirstName`, ''),' ',IFNULL(`LastName`, '')) AS Name FROM employ WHERE Primary_Lead_ID = $empid  AND RelieveFlag=0";
        
        	$db->sql($sql);
        	return $db->getResult();
        }
        else
        {
        	return false;
        }
    }
    
    function getCompetencyAssessID($EmployID)
    {
    	$db = new Database();
        $sql = "SELECT competencyID FROM competency_assessment WHERE EmployID = $EmployID";
        
        $db->sql($sql);
        
        return $db->getResult();
    }
	
	function getEmployName($EmployID)
    {
    	$db = new Database();
        $sql = "SELECT CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, '')) AS Name FROM employees WHERE employee_id = $EmployID";
        
        $db->sql($sql);
        
        return $db->getResult();
    }