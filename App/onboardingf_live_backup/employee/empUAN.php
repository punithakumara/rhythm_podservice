<?php
require_once('../config/mysql_crud.php');
session_start();
if(!isset($_SESSION['authemail'])) 
	header("Location: ../login/uanlogin.php");
	
$empId = isset($_SESSION['empId']) ?$_SESSION['empId'] :'';
$EmpName = isset($_SESSION['authname'])?$_SESSION['authname']:'';
$empName = substr($EmpName, 0, 3);
$id = isset($_REQUEST["id"])?$_REQUEST["id"]:null;
$cancel1 = 'uanlogin.php';
$cancel2 = 'listempuan.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Employee Details</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../css/jquery.validate.css" />
<script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/uan.js"></script>
<link rel="stylesheet" type="text/css" href="../css/tcal.css" />
<script type="text/javascript" src="../js/tcal.js"></script>  
<link href="../css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
body			{ padding:0px; margin:0px; font-family:Tahoma, Geneva, sans-serif; }
h1, h2, h3, h4			{ margin:0px; padding:0px; font-weight:normal; background-color:#005589; color:#ffffff; line-height:25px; height:25px; font-size:12px; width:100%;   }
input[type=text]	{ background-color:#f2f2f2; font-size:12px; font-family:Tahoma, Geneva, sans-serif; outline:none; height:20px; width:70%;resize:none; }
.Wrapper		{ width:1200px; min-height:300px; margin:0px auto; position:relative; margin-top:20px; }
.Wrapper h1		{ text-align:center; border-top-left-radius:5px; border-top-right-radius:5px; }
.Header			{ background:#005589; width:100%; height:45px; }
.Header span	{ width:auto; height:35px; background-color:#ffffff; padding-top:10px; float:left; margin-left:5px; }
.ContentWrapper	{ float:left; width:1198px; border:1px solid #005589; }
.ContentWrapper table	{ margin:5px;  border-right:none; border-bottom:none; }
.ContentWrapper td	{ padding:3px; font-size:12px; color:#333333; height:25px; line-height:20px; border-right:1px solid #cccccc; border-bottom:1px solid #cccccc; }

.ContentWrapper h2		{ padding-left:5px; width:100%; }
.ContentWrapper textarea	{ width:97%; height:50px; margin:5px; resize:none;}
.ContentWrapper h3, .ContentWrapper h4			{ font-weight:bold; background:none; color:#333333; margin-left:5px; width:98%; }
.ContentWrapper h4			{ margin:0px; height: auto; line-height: 17px; margin-left:5px; }
.NoBorder, .NoBorder td					{ border:none; }
.Benfits input[type=text]	{ width:98%;  margin-bottom: 3px; margin-left: 3px; }
.BenfitsTxtBox input[type=text]	{ width:97%; }
.ContentWrapper input[type=radio]	{ margin-left:4px; margin-top:4px; outline:none; }
.ContentWrapper span		{ float:left; }
.Footer 					{ width: 100%; height: 25px; background-color: #005589; float: left; color: #ffffff; font-size: 11px; text-align: center;
line-height: 25px; margin-top: 20px; }
.btTxt						{ background: none repeat scroll 0 0 #f8981f; border: medium none; color: #ffffff; margin: 5px auto; padding: 5px;
							  width: auto; }
.BtnOtr						{ text-align:center; float:left; width:100%; } 
.InputStyle input[type=text]	{ width:99.5%; }
.successful					{ display: block;  font-size: 25px; text-align: center; color: #008000; margin-top:15%; padding: 10px; width: 100%;}
ol, ul {
	list-style: none;
}
.RightOtr					{ width:auto; float:right; margin-right:5px; height:25px; text-align:right; margin-top:10px; }
.RightOtr span				{ font-family:Tahoma, Geneva, sans-serif; font-size:12px; color:#ffffff; line-height:25px; float:left; margin:0 5px; }
input[type=radio] {border: none !important;}
</style>

<style type="text/css">
body { font-size: 11px; font-family: "verdana"; }

pre { font-family: "verdana"; font-size: 10px; background-color: #FFFFCC; padding: 5px 5px 5px 5px; }
pre .comment { color: #008000; }
pre .builtin { color:#FF0000;  }

</style>
</head>
<body>

<div class="Header">
	<span>
		<img src="../images/theorem_logo.jpg" width="118" height="29" />
	</span>
	<div class="RightOtr">
		<span  style="background-color:#005589; padding-top:0px;">Hi &nbsp;<strong><?php echo $EmpName; ?></strong>&nbsp;</span>
		<a href="../login/logout.php?action=uan" title="Logout"><img width="16" height="22" border="0" src="../images/logout_btn.png"></a>
	</div>
</div>
<div class="Wrapper">
<?php
$db = new Database();
$physicallyHandicap = 'n';
$internationalWorker='n';
$documentType = '';
$expDate = null;
if($id){
 //fetch innovation date and add to form 
	 $result = $db->select('emp_uan','*','','id='.$id);	 
	 $result_array = $db->getResult();
	 foreach($result_array as $key => $val) {
		  $UAN = $result_array['UAN'];	
		  $documentType = $result_array['DocumentType'];
		  $documentNo = $result_array['DocumentNo'];
		  $IFSCcode = $result_array['IFSCcode'];
		  $name = $result_array['Name'];
		  $expiryDate = $result_array['Expirydate'];
		  if($expiryDate != ''){
			  $date = str_replace('-', '/', $expiryDate);
			  $expDate = date('d/m/Y', strtotime($date));
		  }
		  $educationQual = $result_array['EducationQual'];
		  $physicallyHandicap = $result_array['PhysicallyHandicap'];
		  $handicapType = $result_array['HandicapType'];
		  $gender =	 $result_array['Gender'];	
		  $internationalWorker = $result_array['InternationalWorker'];
		  $maritalStatus = $result_array['MAritalStatus'];	
		  $estId =	 $result_array['EstId'];//print "</pre>";	
	}
 }

	$where ="empName like '$empName%'";
	$result = $db->select("emp_uan_allotment","allotmentid,concat(allotmentid,' - ',empNAme) as emp",'',$where,'empNAme');
	$select_UAN = "<option value=''>Select</option>"; 
	$result_array = $db->getResult();
	foreach($result_array as $key => $val) {	
		if($id && $val['allotmentid']== $UAN){
			$select_UAN .= "<option value=".$val['allotmentid']." selected='selected'>".$val['emp']."</option>";
		}
		else{
			$select_UAN .= "<option value=".$val['allotmentid'].">".$val['emp']."</option>";
		}
    }

	$documentTypeArray = array('N'=>'National Population Register','A'=>'Adhar','P'=>'Permanent Account Number','B'=>'Bank Account Number'
							,'T'=>'Passport','D'=>'Driving Licence','E'=>'Election Card','R'=>'Ration Card');
	$select_documentType = "<option value=''>Select</option>"; 
	foreach($documentTypeArray as $key => $val) {	
		
		if(isset($documentType) && $key == $documentType){
		$select_documentType .= "<option value='".$key."' selected='selected' >".$val."</option>";
		}
		else
		{
		$select_documentType .= "<option value='".$key."'>".$val."</option>";
		}
    }						

	$educationQualArray = array('I'=>'Illiterate','N'=>'NonMetric','M'=>'Metric','S'=>'Service Secondary','G'=>'Graduate','P'=>'Post Graduate','D'=>'Doctarate');
	$select_educationQual = "<option value=''>Select</option>";
	foreach($educationQualArray as $key => $val) {	
		if(isset($educationQual) && $key == $educationQual){
			$select_educationQual .= "<option value='".$key."' selected='selected' >".$val."</option>";
		}
		else
		{
			$select_educationQual .= "<option value='".$key."'>".$val."</option>";
		}
    }		

	$genderArray = array('M'=>'Male','F'=>'FeMale','T'=>'TransGender');
	$select_gender =  "<option value=''>Select</option>";
	foreach($genderArray as $key => $val) {	
		if(isset($gender) && $key == $gender){
			$select_gender  .= "<option value='".$key."' selected='selected' >".$val."</option>";
		}
		else
		{
			$select_gender  .= "<option value='".$key."'>".$val."</option>";
		}
	}

	$handicapArray = array('L'=>'LocomotiveDisability','V'=>'Visual','H'=>'Hearing');
	$select_handicapType = "<option value=''>Select</option>";
	foreach($handicapArray as $key => $val) {	
		if(isset($handicapType) && $key == $handicapType){
			$select_handicapType  .= "<option value='".$key."' selected='selected' >".$val."</option>";
		}
		else
		{
			$select_handicapType .= "<option value='".$key."'>".$val."</option>";
		}
    }

	$maritalArray = array('M'=>'Married','U'=>'Unmarried','W'=>'Widow','D'=>'Divorcee');
	$select_marital = "<option value=''>Select</option>";
	foreach($maritalArray as $key => $val) {	
		if(isset($maritalStatus) && $key == $maritalStatus){
			$select_marital  .= "<option value='".$key."' selected='selected' >".$val."</option>";
		}
		else
		{
			$select_marital .= "<option value='".$key."'>".$val."</option>";
		}
    }

	$parameters = $_POST;
	if (isset($_POST['submit'])) {	
	
		$querydata =array();
		$querydata['UAN'] = $parameters['UAN'];
		$querydata['DocumentType'] =$parameters['UAN_DocumentType'];
		$querydata['DocumentNo'] =$parameters['UAN_DocumentNumber'];
		$querydata['IFSCcode'] = isset($parameters['UAN_IFSCCode']) ? $parameters['UAN_IFSCCode'] : '';
	
		$querydata['Name'] =$parameters['UAN_NAme'];
	
		if(isset($parameters['UAN_ExpiryDate']))
		{
			$date = DateTime::createFromFormat('d/m/Y', $parameters['UAN_ExpiryDate']);
			$querydata['Expirydate'] = $date->format('Y-m-d');
		}
		
		$querydata['EducationQual'] = $parameters['UAN_EducationalQualification'];
		$querydata['PhysicallyHandicap'] =$parameters['PhysicallyHandicapped'];
		$querydata['HandicapType'] = isset($parameters['UAN_HandicapType']) ?$parameters['UAN_HandicapType'] : '';
		$querydata['Gender'] =$parameters['UAN_Gender']; 	
		$querydata['InternationalWorker'] =$parameters['InternationalWorker'];
		$querydata['MAritalStatus'] =$parameters['UAN_Martial_Status']; 	
		$querydata['EstId'] =$parameters['estId']; 	
	
		if($parameters['emp_uan_id'] > 0)
		{
			$status = "updated";
			$where = 'id = '.$parameters['emp_uan_id'].'';
			$retval = $db->update("emp_uan", $querydata,$where);			
		}
		else{
			$status = "added";
			$querydata['createdOn'] =  date('Y-m-d');
			$retval = $db->insert("emp_uan", $querydata);
		}
		if(! $retval )
		{
			die('Could not enter data: ' . mysql_error());
		}
		else{
?>
		<label for="success" class="successful">
			Employee UAN <?php echo $status; ?> successfully !!!
		</label>				
	<?php 
		}
		
	}
	else{
		if (isset($_POST['submit'])) {	
		echo "gg";
		}
?>

<form name="UAN_form" class="UAN" method="post" action="empUan.php" onsubmit="return ExpiryDate();">
	<h1> UAN Allotment Details</h1>
    <div class="ContentWrapper">
		<table width="99%" cellpadding="0" cellspacing="0" border="0" style="border-top:1px solid #cccccc; border-left:1px  solid #cccccc;">
			<tr>
				<td width="80%" valign="top">
					<table style="align:center" width="99%" class="NoBorder"> 					
						<tr>
							<td>
								<table width="98%" class="NoBorder">
									<tr>
										<td>
											<table width="98%" class="NoBorder">
												<tr>
													<td width="100px" align="left" valign="top">UAN<font color=red>*</font></td>
													<td width="250px" >
														<select name="UAN" id="UAN"  tabindex="1" style="width:72%;" onchange="fillNameTextbox()">
														<?php echo $select_UAN;?>
														</select>
														
													</td>
												</tr>
												<tr>					
													<td  align="left"  valign="top">Document Type<font color=red>*</font></td>
													<td>											
														<select name="UAN_DocumentType" tabindex="2" id="UAN_DocumentType" style="width:72%" onChange="findselected()">
															<?php echo $select_documentType;?>							
														</select>
													</td>
												</tr>
												<tr>
													<td align="left"  valign="top">IFSC Code </td>
													<td>
														<input type="text" tabindex="4" maxlength="11" value="<?php  if(isset($IFSCcode)) echo $IFSCcode;?>" onchange="return checkIFSC();" name="UAN_IFSCCode" id="UAN_IFSCCode" disabled />
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">Educational Qualification<font color=red>*</font></td>
													<td>
														<select name="UAN_EducationalQualification" tabindex="6" id="UAN_EducationalQualification" style="width:72%">
															<?php echo $select_educationQual;?>
														</select>
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">Physically Handicapped</td>
													<td >
														<input type="radio" tabindex="9" name="PhysicallyHandicapped" id="PhysicallyHandicapped" value="y" <?php if($physicallyHandicap == 'y')  echo ' checked="checked"';?> style="float:left;" onchange="return enableHandicapType();"/><span>Yes</span>
														<input type="radio" tabindex="10" name="PhysicallyHandicapped" id="PhysicallyHandicapped" value="n" <?php if($physicallyHandicap == 'n')  echo ' checked="checked"';?> style="float:left;" onchange="return disableHandicapType();" /><span>NO</span>
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">Gender<font color=red>*</font></td>
													<td>
														<select name="UAN_Gender" tabindex="12" id="UAN_Gender" >
															<?php echo $select_gender; ?>
														</select>
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">Est Id</td>
													<td>
														<input type="text" tabindex="14" maxlength="50"  name="estId" id="estId" value="BGBNG0026309000" readonly/>
													</td>
												</tr>
											</table>
										</td>
										<td> 
											<table width="98%" class="NoBorder">
												<tr>
													<td width="100px" align="left" valign="top">Name <font color=red>*</font></td>
													<td width="200px" >
														<input type="text" maxlength="50" value="<?php  if(isset($name)) echo $name;?>" name="UAN_NAme" id="UAN_Name" />
													</td>
												</tr>
												<tr>
													<td align="left" valign="top">Document Number<font color=red>*</font></td>
													<td> 
														<input type="text" tabindex="3"  maxlength="30" value="<?php  if(isset($documentNo)) echo $documentNo;?>" name="UAN_DocumentNumber" id="UAN_DocumentNumber" />
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">ExpiryDate</td>
													<td>
														<input type="text" autocomplete="off" class="tcal" tabindex="5" name="UAN_ExpiryDate" id="UAN_ExpiryDate" <?php  if( $documentType != 'T' && $documentType !='D') { ?> disabled="disabled" <?php }?> value="<?php  if(isset($expDate)) echo $expDate;?>" >
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">International Worker</td>
													<td>
														<input type="radio" tabindex="7" name="InternationalWorker" id="InternationalWorker" value="y"  <?php if($internationalWorker == 'y')  echo ' checked="checked"';?> style="float:left;" /><span>Yes</span>
														<input type="radio" tabindex="8" name="InternationalWorker" id="InternationalWorker" value="n"  <?php if($internationalWorker == 'n')  echo ' checked="checked"';?> style="float:left;"  /><span>NO</span>														
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">Handicap Type</td>
													<td>
													<select name="UAN_HandicapType" tabindex="11" id="UAN_HandicapType" onchange="return checkHandicapType();" style="width:72%" <?php  if(!isset($handicapType)) { ?> disabled <?php }?> >
														<?php echo $select_handicapType; ?>
													</select>
													</td>
												</tr>
												<tr>
													<td valign="top" align="left">Martial Status<font color=red>*</font></td>
													<td>
														<select name="UAN_Martial_Status" tabindex="13" id="UAN_Martial_Status" >
															<?php echo $select_marital; ?>											
														</select>
													</td>
												</tr>												
											</table>
										</td>
									</tr>									
								</table>							
							</td>
						</tr>
	</div>
						<tr>
							<td>
								<div class="BtnOtr">
									<input id="saveForm" class="btTxt"  type="submit" name="submit" value="Submit"/>
									<input id="saveForm" class="btTxt"  type="button" name="reset" value="Cancel" onClick="window.location.href='<?php if(isset($id)) echo "$cancel2"; else echo $cancel1;?>'" />
									<input id="emp_uan_id" type="hidden" value=<?php if(isset($id)) echo $id; else echo '0';?>  name="emp_uan_id"/>
								</div>  
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</form>
<?php }?>
</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>