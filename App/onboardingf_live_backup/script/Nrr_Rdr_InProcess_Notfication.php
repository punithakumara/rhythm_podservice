<?php
ini_set('max_execution_time', 9000);
require ('../config/mysql_crud.php');

class NrrRdrInProcNotfic 
{
	public function __construct() {
		$this->db	= new Database();
		$this->rdrNotficHistory();
		$this->nrrNotficHistory();
		$this->deleteNotfic();
		$this->addNotfication($this->getRdrData());
		$this->addNotfication($this->getNrrData());
	}

/**
	Get NRR Data
*/
	public function getNrrData() {
		$resp	= "";
		$query	= "SELECT DISTINCT ( cr.NRR_ID )                                    AS 
		                NRR_ID, 
		                CONCAT(IFNULL(e.firstname, ''), '', IFNULL(e.lastname, '')) AS 
		                Empname, 
		                p.ProcessID                                                 AS 
		                processid, 
		                snrr.No_of_Resources 					     				AS 
		                No_of_Resources,
		                c.clientid,
		                c.Client_Name 												AS
		                ClientName,
		                p.name 														AS
		                ProcessName,  
		                ep.employid 						     					AS
		                AmEmpId, 
		                cr.Production_Date, 
		                ep.grades,
		                cr.Flag,
		                cr.nrr_notfication_count
				FROM   client_nrr AS cr 
		       			INNER JOIN employprocess AS ep 
		       			        ON ( cr.processid 	= ep.processid ) 
		       			INNER JOIN employ AS e 
		       			        ON ( ep.employid 	= e.employid ) 
		       			INNER JOIN process AS p 
		       			        ON ( cr.processid 	= p.processid ) 
		       			INNER JOIN client AS c 
		       			        ON ( cr.clientid	= c.clientid )
		       			INNER JOIN shifts_resources_nrr AS snrr
		       			        ON ( cr.NRR_ID 		= snrr.NRR_ID ) 
				WHERE  snrr.Production_Date < CURDATE() 
		       			AND cr.flag 	IN ( 'InProcess', 'Submitted' ) 
		       			AND ep.grades 	IN ( 4 )
		       			AND cr.nrr_notfication_count <= 3 
		       	GROUP BY snrr.NRR_ID";
		 //echo $query;
		$result		= $this->db->sql($query);
		$resp		= $this->countRecursive($this->db->getResult());
		if(!empty($resp)) {
			foreach ($resp as $key => $value) {
				$resp[$key]['VmEmpId']	= $this->getManager($resp[$key]['AmEmpId']);
				$resp[$key]['Type']		= 'NRR';
			}
		}
		return $resp;
	}

/**
	Get RDR Data
*/
	public function getRdrData() {
		$resp	= "";
		$query 	= "SELECT DISTINCT ( cr.rdr_id )                                    AS 
		                rdr_id, 
		                Concat(Ifnull(e.firstname, ''), '', Ifnull(e.lastname, '')) AS 
		                Empname, 
		                p.ProcessID                                                 AS 
		                processid, 
		                GROUP_CONCAT(srr.No_of_Resources) 							AS
		                No_of_Resources,
		                c.clientid, 
		                c.Client_Name 												AS
		                ClientName, 
		                p.name 														AS
		                ProcessName,
		                ep.employid 												AS
		                AmEmpId, 
		                cr.rampdown_date, 
		                ep.grades,
		                cr.Flag,
		                cr.rdr_notfication_count
				FROM   client_rdr AS cr 
		       			INNER JOIN employprocess AS ep 
		       			        ON ( cr.processid 	= ep.processid ) 
		       			INNER JOIN employ AS e 
		       			        ON ( ep.employid 	= e.employid ) 
		       			INNER JOIN process AS p 
		       			        ON ( cr.processid 	= p.processid ) 
		       			INNER JOIN client AS c 
		       			        ON ( cr.clientid	= c.clientid )
		       			INNER JOIN shifts_resources_rdr AS srr 
		       			        ON ( cr.rdr_id 		= srr.RDR_ID ) 
				WHERE  cr.rampdown_date < Curdate() 
		       			AND cr.flag 	IN ( 3, 1 ) 
		       			AND ep.grades 	IN ( 4 )
		       			AND cr.rdr_notfication_count <= 3
		       	GROUP BY srr.RDR_ID"; 
		
		$result		= $this->db->sql($query);
		$resp		= $this->countRecursive($this->db->getResult());
		
		if(!empty($resp)) {
			foreach ($resp as $key => $value) {
				$resp[$key]['VmEmpId']	= $this->getManager($resp[$key]['AmEmpId']);
				$resp[$key]['Type']		= 'RDR';
			}
		}
		return $resp;
	}

/**
	Get Manager Data For AM's
*/
	public function getManager($amid) {
		if(!empty($amid) && is_numeric($amid)) {
			$query		= " SELECT employ.EmployID AS vmempid
							FROM   employ INNER JOIN employ st2 
							              ON ( employ.employid = st2.primary_lead_id ) 
							WHERE  st2.EmployID IN ( $amid ) 
							LIMIT  0, 1 ";
			$result		= $this->db->sql($query);
			$resp		= $this->db->getResult();
			return $resp['vmempid'];
		}
	}
	
	public function deleteNotfic() {

		$query	= " DELETE FROM notification WHERE Type IN (7,8) ";
		$result	= $this->db->sql($query);
	}

/**
	Save In Notfication Table 
*/
	public function addNotfication($datas = "") {
		if(!empty($datas)) {
			date_default_timezone_set('Asia/Calcutta');
			$notfic				= array();
			$notification_data	= array();
			$Description		= "";
			$ReceiverEmployee	= "";
			$type				= "";
			$date				= new DateTime();
			if(!empty($datas)) {
				foreach ($datas as $key => $value) {
					if($datas[$key]['Type'] == 'RDR') {
						$notfic		= array(
							'Title'				=> 'Client RDR',
							'RDR_ID'			=> $datas[$key]['rdr_id'],
							'ClientID' 			=> $datas[$key]['clientid'],
							'ProcessID' 		=> $datas[$key]['processid'],
							'NoOFRes'			=> $datas[$key]['No_of_Resources'],
							'Flag' 				=> $datas[$key]['Flag']
						);

						$Description			= "Pending RDR - " .$value['ClientName'] . "-" . $value['ProcessName']."";

						if($datas[$key]['rdr_notfication_count'] == 3) {
							$ReceiverEmployee	= $value['AmEmpId'].','.$value['VmEmpId'] .','. 1296 .'';
						} else {
							$ReceiverEmployee	= $value['AmEmpId'].','.$value['VmEmpId'];
						}
						
						$type				= 7;

					} elseif($datas[$key]['Type'] == 'NRR') {
						$notfic		= array(
							'Title'		=> 'Client NRR',
							'NRR_ID'	=> $datas[$key]['NRR_ID'],
							'ClientID' 	=> $datas[$key]['clientid'],
							'ProcessID' => $datas[$key]['processid'],
							'Flag' 		=> $datas[$key]['Flag']
						);
						$Description	= "Pending NRR -" .$value['ClientName'] ."-" . $value['ProcessName']."";

						if($datas[$key]['nrr_notfication_count'] == 3) {
							$ReceiverEmployee	= $value['AmEmpId'].','.$value['VmEmpId'] .','. 1296 .'';
						} else {
							$ReceiverEmployee	= $value['AmEmpId'].','.$value['VmEmpId'];
						}
						$type			= 8;
					}

					$notification_data   	= array(
						'Title' 			=> json_encode($notfic),
						'Description' 		=> $Description,
						'SenderEmployee'	=> $value['AmEmpId'],
						'Type' 				=> $type,
						'ReceiverEmployee'	=> $ReceiverEmployee,
						'URL'				=> '',
						'NotificationDate'	=> $date->format('Y-m-d H:i:s')
					);

					$query		= " INSERT INTO notification (".implode(",", array_keys($notification_data)).") 
									VALUES ('".implode("','", $notification_data)."')";
					$result		= $this->db->sql($query);
				}
			}
			if($result) {
				echo "Notfication Data Inserted Successfully \n";
			} else {
				echo "Failed to Insert Notfication Data";
			}
		}
	}

/**
	Function to convert associative array to multidimensional array
*/
	public function countRecursive($checkarray = '') {
		if(!empty($checkarray)) {
				if (count($checkarray) == count($checkarray, COUNT_RECURSIVE)) {
				$arraymodify	= array();
				$arraymodify[] 	= $checkarray;
				$checkarray		= $arraymodify;
			} else {
				$checkarray = $checkarray;
			}
		}
		
		return $checkarray;
	}

	public function rdrNotficHistory() {
		$rdrdata	= $this->getRdrData();
		if(!empty($rdrdata)) {
			$notify_date	= date('Y-m-d');
			foreach ($rdrdata as $key => $rdrval) {
				$query	= " UPDATE client_rdr SET rdr_notfication_count = rdr_notfication_count + 1,
							rdr_notfication_date = '".mysql_real_escape_string($notify_date)."'
						    WHERE RDR_ID = ".$rdrval['rdr_id']."";
				$result	= $this->db->sql($query);
			}	
		}
	}

	public function nrrNotficHistory() {
		$nrrdata	= $this->getNrrData();
		if(!empty($nrrdata)) {
			$notify_date	= date('Y-m-d');
			foreach ($nrrdata as $nrrval) {
				$query	= " UPDATE client_nrr SET nrr_notfication_count = nrr_notfication_count + 1, 
								nrr_notfication_date	= '".mysql_real_escape_string($notify_date)."'
						    WHERE NRR_ID    = ".$nrrval['NRR_ID']."";

				$result	= $this->db->sql($query);
			}
		}		
	}
}

$obj	= new NrrRdrInProcNotfic();
?>