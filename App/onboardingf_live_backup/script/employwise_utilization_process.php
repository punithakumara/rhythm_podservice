<?php
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class EmploywiseUtlizationProcessCron extends MysqlConnect{
	
	public $expectedHours = 0;
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}
	
 
	function IsBillabiltyDataExist($empid)
	{
		$sql= "SELECT employID,billabilityData  FROM employ_billability WHERE employId = ".$empid;
		 $result = mysql_query($sql);
		
		if(mysql_num_rows($result) > 0) {
			return $result_arr=mysql_fetch_array($result);
		}
			
		return "";
	}
 
 
	/**
	* @description : Function to calculate expected hours based on employID and processID.
	*/
	function _getExpectedHours($empID,$processID,$today)
	{
		$this->expectedHours = 0;
		$BillblePercentage = 1;
		 $ExtTransLogData = $this->IsBillabiltyDataExist($empID);
		 if($ExtTransLogData != ""){
					 $data_attr = array();
					 $data_attr = unserialize($ExtTransLogData['billabilityData']);
					 					 
					 /* START modify expected hours code*/
					 
					 //check if array empty
					 if(count($data_attr)!=0){
					 						 						 	
					 	for ($i=count($data_attr)-1; $i>=0; $i--) // loop the arrays from desc order
					 	{					 							 						 		
					 		if ($data_attr[$i]['process_id'] == $processID)
					 		{
					 			if( ($data_attr[$i]['billability_type']=='FTE' || $data_attr[$i]['billability_type']=='Approved Buffer') && $data_attr[$i]['billability_percentage']!="")
						 		{							 			
							 		//IF only startDate available
							 		if($data_attr[$i]['start_date']!="" && $data_attr[$i]['end_date']==""){
							  			if(strtotime($data_attr[$i]['start_date']) <= strtotime($today))
							  			{
							  				$BillblePercentage 		= $data_attr[$i]['billability_percentage'] / 100;
							  				$this->expectedHours 	= 8 * $BillblePercentage;
							  				//echo $data_attr[$i]['start_date'];
							  				break;
							  			}
							  		}//IF   startDate and EndDate  available
							  		elseif($data_attr[$i]['start_date']!="" && $data_attr[$i]['end_date']!=""){
							  			if((strtotime($data_attr[$i]['start_date']) <= strtotime($today)) && (strtotime($data_attr[$i]['end_date']) >= strtotime($today)))
							  			{
							  				$BillblePercentage 		= $data_attr[$i]['billability_percentage'] / 100;
							  				$this->expectedHours 	= 8 * $BillblePercentage;
							  				break;
							  			}
							  		}
						 		}						 		
						 		//IF billability_percentage empty 100%
						 		elseif(($data_attr[$i]['billability_type']=='FTE' || $data_attr[$i]['billability_type']=='Approved Buffer') && $data_attr[$i]['billability_percentage']=="")
						 		{
						 			if($data_attr[$i]['start_date']!="" && $data_attr[$i]['end_date']!=""){
						 				if((strtotime($data_attr[$i]['start_date']) <= strtotime($today)) && (strtotime($data_attr[$i]['end_date']) >= strtotime($today)))
						 				{
						 					$BillblePercentage 		= $data_attr[$i]['billability_percentage'] / 100;
						 					$this->expectedHours 	= 8 * $BillblePercentage;
						 					break;
						 				}
						 			}
						 			else
						 			{
						 				$this->expectedHours = 8;
						 				break;
						 			}
						 		}		 		
					 		}
					 	}
 	
					 }
	 
					 /* END modify expected hours code*/					 					 					 
			}	 
	}
	/**
	* @description : deleting existing utilization data based on date
	*/
	private function  DeleteExistingUtilization($today)
	{
		if(!empty($today))
		{
			$sqldel = "DELETE FROM employwise_utilization_process WHERE TaskDate = '$today'";
			$result = mysql_query($sqldel) or die(mysql_error()."Query error");
		}
	}
	
	/**
	* @description : Function to insert utilization data into employwise_utilization_process table.
	*/
 	public function emp_utlization_process($today='')
	{
		 
		if(!empty($today))
		{	
			$utilizationSuccessCount = '';
			$Utilization = 0;
			$Insertvalues = array();
			
			$this->DeleteExistingUtilization($today);

			$query = "SELECT DISTINCT u1.UtilizationID, u1.TaskDate, u1.ProcessID,u1.employID , u1.WeekendSupport,u1.ClientHoliday,
					SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(u1.Hours) + SUM(u1.MIN) DIV 60 ,':',SUM(u1.MIN) MOD 60))) AS Total_Hours_Day,
					client.clientID AS ClientID,client.Client_Name AS clientName,vertical.NAME AS Vertical_Name, 
					vertical.VerticalID AS VerticalID,process.NAME AS Process_Name,v.cnt AS ERRORS,Emp.shiftID 
					FROM utilization u1   
					JOIN utilization u2 ON u1.UtilizationID = u2.UtilizationID
					LEFT JOIN process ON process.processID = u1.processID
					LEFT JOIN vertical ON vertical.verticalID = process.verticalID
					LEFT JOIN client  ON process.clientid = client.clientid 
					LEFT JOIN ( SELECT  shiftID,employID,processID FROM `employprocess` GROUP BY employID ) 
							Emp  ON Emp.employID = u1.employID AND  u1.processID=Emp.processID 
					LEFT JOIN ( SELECT COUNT(*) AS cnt,ProcessID,employID FROM `incidentlog` LEFT JOIN `incidentlog_employee` ON incidentlog_employee.LogID = incidentlog.LogID  WHERE  DATE = '".$today."' AND LogType = 'Error' GROUP BY ProcessID ) 
							v  ON v.ProcessID = u1.processid AND  u1.employID=v.employID 	 
					WHERE  u1.TaskDate= '".$today."' AND process.ProcessStatus=0  
					GROUP BY u1.ProcessID ,u1.employID 
					ORDER BY  u1.utilizationID";
			$result =  mysql_query($query) or die(mysql_error()."Query error");
		 
			while($res = mysql_fetch_array($result)) 
			{
				//Find Expected Hours By ProcessId&EmployId
				//$this->_getExpectedHours('997','315',$today);
				$this->_getExpectedHours($res['employID'],$res['ProcessID'],$today);
				
				//echo $this->expectedHours; exit;

				$UtilizationID 	= $res['UtilizationID'];
				$ProcessID 		= $res['ProcessID'];
				$Process_Name 	= $res['Process_Name'];
				$TaskDate 		= $res['TaskDate'];
				$employID 		= $res['employID'];
				$Total_Hours 	= empty($res['Total_Hours_Day']) ? 0 : $res['Total_Hours_Day'];
				$Total_Hour_util= empty($res['Total_Hours_Day']) ? 0 : str_replace(':', '.',$res['Total_Hours_Day']);
				$clientID 		= empty($res['ClientID']) ? 0 : $res['ClientID'];
				$clientName 	= $res['clientName'];
				$Vertical_Name 	= $res['Vertical_Name'];
				$VerticalID 	= $res['VerticalID'];
				$now 			= date('Y-m-d H:i:s');
				$Errors 		= empty($res['ERRORS']) ? 0 : $res['ERRORS'];
				$WeekendSupport = empty( $res['WeekendSupport']) ? 0 :  $res['WeekendSupport'];
				$ClientHoliday  = empty( $res['ClientHoliday']) ? 0 :  $res['ClientHoliday'];
				$shiftID 		= empty($res['shiftID']) ? 0 : $res['shiftID'];

				  if($this->expectedHours!=0)
				  {					
					  $Utilization =  ($Total_Hour_util / $this->expectedHours)*100;
				  }
				  else
				  {
					  $Utilization =  0 ;
				  }
				
				$expectedHours 	= $this->expectedHours;
				$Utilization 	= empty($Utilization) ? 0 : round($Utilization,2);				
				
				/*$Insertvalues[] =  "('".$UtilizationID."','".$ProcessID."','".$Process_Name."','".$clientID."','".$clientName."',$Utilization,'".$Total_Hours."',
						'". $employID."','". $shiftID."','".$TaskDate."','".$VerticalID."','".$Vertical_Name."','".$Errors."','".$WeekendSupport."','".$ClientHoliday."','".$expectedHours."','".$now."')";*/
				
				$Insertvalues[] =  "('".$UtilizationID."','".$ProcessID."','".$Process_Name."','".$clientID."','".$clientName."',$Utilization,'".$Total_Hours."',
						'". $employID."','". $shiftID."','".$TaskDate."','".$VerticalID."','".$Vertical_Name."','".$Errors."','".$WeekendSupport."','".$expectedHours."','".$now."')";
				$utilizationSuccessCount++;
			}

			//Inserting into employwise_utilization_process table
			 if( !empty($Insertvalues) ) 
			{
				/*$query = "INSERT INTO `employwise_utilization_process`(UtilizationID,ProcessID, ProcessName,clientID,clientName,Utilization, 
						Num_Hours,EmployID,shiftID,TaskDate,VerticalID,VerticalName,Errors,WeekendSupport,ClientHoliday,expectedHours,Created_date)
						VALUES ". implode(',',$Insertvalues); */
				$query = "INSERT INTO `employwise_utilization_process`(UtilizationID,ProcessID, ProcessName,clientID,clientName,Utilization, 
						Num_Hours,EmployID,shiftID,TaskDate,VerticalID,VerticalName,Errors,WeekendSupport,expectedHours,Created_date)
						VALUES ". implode(',',$Insertvalues);
	  
				if( mysql_query($query) )
				{
					echo "$utilizationSuccessCount  Utilization Added Successfully </br></br>";
				}
				else
				{
					echo "$UtilizationID Error in Utilization insertion";
				} 
			}
			else
			{
				echo "No Utilization data for ".$today;
			}  
		}
 	}
}
// for only one day
/*  $date="2016-05-06";
$obj  = new EmploywiseUtlizationProcessCron();
$obj->emp_utlization_process($date); */
//Between Two dates
// For mennually run query.
/*   $obj  = new EmploywiseUtlizationProcessCron();
$date = '2016-10-10';
$end_date = '2016-10-20';
while (strtotime($date) <= strtotime($end_date)) {
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->emp_utlization_process($date);
} */   
 
 
$obj = new EmploywiseUtlizationProcessCron();
$s_date = date('Y-m-d',date(strtotime("-8 day")));
$e_date = date("Y-m-d",strtotime("+1 day"));  
 
$begin = new DateTime($s_date);
$end = new DateTime($e_date);

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);


foreach ( $period as $dt )
{
	$obj->emp_utlization_process($dt->format("Y-m-d"));     
}  
 
?>

