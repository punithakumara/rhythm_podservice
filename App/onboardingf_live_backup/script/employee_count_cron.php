<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
//ini_set('xdebug.max_nesting_level', 200); 
require_once 'db_config.php';
class EmployeeCountCron extends MysqlConnect{

	public $empArray = array();
	public $count = false;
	public $inserted = 0;
	public $updated = 0;
	public $HR_POOL = 223;
	public $Onboarding = 221;

	
	/*
	 * @description : getting all the employee above grade 2
	 * @return : setting into employee 
	 */
	 private function _setEmpArray(){
		
		$sql = "SELECT employ.EmployID,employ.FirstName,employ.LastName FROM employ JOIN designation ON employ.DesignationID = designation.DesignationID WHERE designation.grades > 2 and employ.RelieveFlag = 0 ORDER BY employ.EmployID ";
		$result =  mysql_query($sql) or die(mysql_error()."getAllEmployee");
		$totalRecord = mysql_num_rows($result);
		if($totalRecord > 0)
		{
			while($row = mysql_fetch_array($result))
				$this->empArray[] = array('EmployID'=>$row['EmployID'],'FirstName'=>$row['FirstName'],'LastName'=>$row['LastName']);
			$this->empArray;
		} 
		return false;
	}
	
	/*
	 * @descritpion : check if employid exist in number_of_employee table
	 * @return : 'true' or 'false'
	 */
	private function _isEmpExist($empid)
	{
		$sql= "SELECT EmployID FROM number_of_employee WHERE EmployID = ".$empid;
		$result = mysql_query($sql);
		$status = (mysql_num_rows($result)==0) ? false : true;
		return $status;
	}
	
	/*
	 * @description : 
	 * First : Set the employee Array
	 * Second : get the Total Count for employee
	 * Third : if empid exist updating the query
	 * Fourth : if empid not exist inserting 
	 * 
	 */
	public function cron(){
		
		$this->_setEmpArray();//First
		if(is_array($this->empArray))
		{
			foreach($this->empArray as $key=>$emp){
				//echo "-->".$emp['EmployID']."<br>";
				$this->count = false;
				$totalCount  = $this->getTotalCount($emp['EmployID']);//Second
				//echo $emp['EmployID']."=>".$totalCount."<br />";
				if($this->_isEmpExist($emp['EmployID']))//Third
				{
				    $sql = "UPDATE number_of_employee SET TotalCount = ".$totalCount." where EmployID = ".$emp['EmployID'];
				    //echo $sql."<br>";
				    $result = mysql_query($sql);
				    $this->updated++;
				}
				else //Fourth
				{
				   $sql = "INSERT INTO number_of_employee (EmployID,TotalCount) VALUES (".$emp['EmployID'].",".$totalCount.")";
				   //echo $sql."<br>";
				   $result = mysql_query($sql);
				   $this->inserted++;
				}
				
			}
		}	
	}
	
	/*
	 * @description :  getting total count 
	 */
	public function getTotalCount($empid) {
		$sql= "SELECT EmployID FROM employ WHERE Primary_Lead_ID = ".$empid." AND RelieveFlag=0";
		$sql .= " AND EmployID NOT IN (SELECT EmployID FROM employprocess WHERE Grades <3 AND  ProcessID IN(".$this->HR_POOL.", ".$this->Onboarding."))  AND (TransitionStatus !=3)";
		$result = mysql_query($sql);
		$this->count+= mysql_num_rows($result);
		if(mysql_num_rows($result)!=0)
		{
			while($row = mysql_fetch_array($result))
			{
				$this->getTotalCount($row['EmployID']);
			}
		}
		return $this->count;
	}
}
$obj  = new EmployeeCountCron();
$obj->cron();
echo "<br>Total Record of Employee = ".count($obj->empArray);
echo "<br>Total Record Inserted = ".$obj->inserted;
echo "<br>Total Record Update = ".$obj->updated;

