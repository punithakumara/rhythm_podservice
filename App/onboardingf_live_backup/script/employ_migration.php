<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
require_once 'db_config.php';
class Employ extends MysqlConnect
{
	public static $intranetEmployee = array();
	
	/*
	 * @description : getting intranet employee record and setting to static variable 
	 */
	public function getIntranetEmployee()
	{
		$sql = "SELECT Email,DateOfBirth,DateOfJoin,Gender,Address1,Address2,City,State,Zip,LandLine,Mobile,AlternateEmail,Qualification,Aim,Yahoo,Skype,Msn FROM employ WHERE RelieveFlag = 0";
		$result = mysql_query($sql);
		while($row = mysql_fetch_assoc($result))
			self::$intranetEmployee[] = $row;
		//return $this->intranetEmployee;
	}
	
	/* reading excel data */
	public function getExcelValues($filename)
	{
		include 'excel_reader.php';
		$excel = new PhpExcelReader; 
		$excel->read($filename); 
		
		// traversing excel values 
		$x = 1;
		$cell = array();
  		while($x <= $excel->sheets[0]['numRows']) 
  		{
    		$y = 1;
    		while($y <= $excel->sheets[0]['numCols']) 
    		{
    			$name = $excel->sheets[0]['cells'][1][$y];
    			$cell[$x][$name] = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
      			$y++;
    		}	  
    		$x++;
  		}
  				
  		unset($cell[1]);
  		self::$intranetEmployee = $cell;
  		//echo "<pre>";print_r($cell);
		/*echo '<pre>';
			print_r($excel->sheets);
		echo '</pre>';
		exit;*/
	}
	
	
	/* @description : reading csv file */
	public function readCsvFile($filename)
	{
		include 'CSVReader.php';
		$csv = new CSVReader();
		self::$intranetEmployee = $csv->parse_file($filename,true);
	}
	
	
	/*	@description :  updating Rhythm emplyee table from csv file.*/
	public function updateEmployeeTable()
	{
		$RecordUpdated = 0;
		$RecordRejected = 0;
		foreach(self::$intranetEmployee as $key=>$value)
		{
			$query = "SELECT EmployID FROM employ WHERE trim(Email) = '".trim($value['Email'])."'";
			$result = mysql_query($query);
			if($row = mysql_fetch_array($result))
			{
				if($value['AddressSame']==1)
					$query = "UPDATE employ SET Gender = '".trim($value['Gender'])."',Address1 = '".trim($value['Address1'])."',City = '".trim($value['City'])."',State = '".trim($value['State'])."',Zip = '".trim($value['Zip'])."',AddressSame = ".trim($value['AddressSame']).",Address2 = '".trim($value['Address1'])."',City1 = '".trim($value['City'])."',State1 = '".trim($value['State'])."',Zip1 = '".trim($value['Zip'])."' WHERE EmployID = ".$row['EmployID'];
				else
					$query = "UPDATE employ SET Gender = '".trim($value['Gender'])."',Address1 = '".trim($value['Address1'])."',City = '".trim($value['City'])."',State = '".trim($value['State'])."',Zip = '".trim($value['Zip'])."',AddressSame = ".trim($value['AddressSame']).",Address2 = '".trim($value['Address2'])."',City1 = '".trim($value['City1'])."',State1 = '".trim($value['State1'])."',Zip1 = '".trim($value['Zip1'])."' WHERE EmployID = ".$row['EmployID'];

					$RecordUpdated ++;
				$result = mysql_query($query);	
			}
			else
				$RecordRejected++;
		}
		return "Total Record Updated = ".$RecordUpdated." and Tota Record Rejected ".$RecordRejected;
	}

	/* @description : updating Rhythm employee table */
	public function updateRhythmEmployee()
	{
		$RecordUpdated = 0;
		$RecordRejected = 0;
		foreach(self::$intranetEmployee as $key=>$value)
		{
			$query = "SELECT EmployID FROM employ WHERE trim(Email) = '".trim($value['Email'])."'";
			$result = mysql_query($query);
			if($row = mysql_fetch_array($result))
			{
				$query = "update employ set DateOfBirth = '".$value['DateOfBirth']."',DateOfJoin = '".$value['DateOfJoin']."',Gender = '".$value['Gender']."',Address1 = '".$value['Address1']."',Address2 = '".$value['Address2']."',City = '".$value['City']."',State = '".$value['State']."',Zip = '".$value['Zip']."',LandLine = '".$value['LandLine']."',Mobile = '".$value['Mobile']."',AlternateEmail = '".$value['AlternateEmail']."',Aim = '".$value['Aim']."',Yahoo = '".$value['Yahoo']."',Skype = '".$value['Skype']."',Msn = '".$value['Msn']."' where EmployID = ".$row['EmployID'];
				//$result = mysql_query($query);
				$RecordUpdated ++;
			}
			else
				$RecordRejected++;
		}
		
		return "Total Record Updated = ".$RecordUpdated." and Tota Record Rejected ".$RecordRejected;
	}	
}
$obj = new Employ();
/*$obj->getIntranetEmployee();
echo "<pre>";print_r(Employ::$intranetEmployee);

/*$obj2 = new Employ('rhythm_dev');
$value = $obj2->updateRhythmEmployee();

echo $value;
*////echo "<pre>";print_r($employee);

$obj->readCsvFile('completed.csv');
$value = $obj->updateEmployeeTable();

echo $value;
//echo "<pre>";print_r(Employ::$intranetEmployee);


?>