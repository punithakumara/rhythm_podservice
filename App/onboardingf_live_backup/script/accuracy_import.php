<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';
class AccuracyImport extends MysqlConnect{

	public $accuracyData;
	
	public $alreadyExist = 0;
	public $Failed = 0;
	public $Inseted=0;
	
	/* @description : reading csv file */
	public function readCsvFile($filename)
	{
		include 'CSVReader.php';
		$csv = new CSVReader();
		$this->accuracyData = $csv->parse_file($filename,true);
	} 
	
	/*
	 * @description : getting ProcessID
	 */
	private function _getProcessID($processName,$clientName)
	{
		$sql = "SELECT process.ProcessID, process.ClientID, processmanager.EmployID AS ProcessAM FROM process JOIN processmanager ON (process.ProcessID = processmanager.ProcessID) WHERE process.ProcessStatus=0 AND LOWER(TRIM(process.NAME)) = '".strtolower(trim($processName))."' AND process.`ClientID` = (SELECT ClientID FROM `client` WHERE LOWER(TRIM(Client_Name)) = '".strtolower(trim($clientName))."')";
		//echo $sql."<br><br><br>"; exit;
		$result = mysql_query($sql) or die(mysql_error()."_getProcessID");
		if(mysql_num_rows($result) == 0) 
			return FALSE;
		else
		{
			$row = mysql_fetch_array($result);
			return $row;
		}	
	}
	
	private function _getWeek($data)
	{
		//print_r($data); exit;
		if($data['AccuracyWeek']!="")
			return date('Y-m-d', strtotime($data['AccuracyWeek']));
		else
		{
			if(!is_numeric($data['Month']))
				$data['Month'] = date("n", strtotime($data['Month']));
			//echo $data['Month']; exit;
			switch($data['Week'])
			{
				case 1: $tc = strtotime('monday', mktime(0, 0, 0, $data['Month'], 1, $data['Year']));
				break;
				case 2: $tc = strtotime('First monday', mktime(0, 0, 0, $data['Month'], 1, $data['Year']));
				break;
				case 3: $tc = strtotime('Second monday', mktime(0, 0, 0, $data['Month'], 1, $data['Year']));
				break;
				case 4: $tc = strtotime('Third monday', mktime(0, 0, 0, $data['Month'], 1, $data['Year']));
				break;
				case 5: $tc = strtotime('Fourth monday', mktime(0, 0, 0, $data['Month'], 1, $data['Year']));
				break;
			}
			//echo date('Y-m-d', $tc); exit;
			return date('Y-m-d', $tc);
		}
	}
	
	public function insertAccuracy()
	{
		$r = 1;
		foreach($this->accuracyData as $k=> $accuracy)
		{
			//print_r($accuracy); exit;
			if($accuracy['Process']!="" && $accuracy['Accuracy']!="")
			{
				$array = $this->_getProcessID($accuracy['Process'],$accuracy['Client']);
				$week = $this->_getWeek($accuracy);
				if(is_array($array))
				{
					$sql = "SELECT * from accuracy WHERE ProcessID = ".$array['ProcessID']." AND ClientID=".$array['ClientID']." AND AccuracyWeek = '".$week."'";
					//echo $sql."<br/>"; 
					$result = mysql_query($sql) or die(mysql_error());
					if($row = mysql_num_rows($result)==0)
					{
						$EmployID = $array['ProcessAM'];
						$ClientID = $array['ClientID'];
						$ProcessID = $array['ProcessID'];
						$Accuracy = $string = str_replace('%', '', $accuracy['Accuracy']); ;
						$AccuracyWeek = $week;
						
						$query = "INSERT INTO accuracy(EmployID, ClientID, ProcessID, Accuracy, AccuracyWeek) VALUES (".$EmployID.", ".$ClientID.", ".$ProcessID.", '".$Accuracy."', '".$AccuracyWeek."')";
						//echo $query; exit;
						$rslt = mysql_query($query) or die(mysql_error()."insertAccuracy");
						$this->Inseted++;
					}
					else
					{
						$this->alreadyExist++;
						//echo $sql."<br \>";
					}	
				}
				else
				{
					$this->Failed++;
					echo $r." ==> Failed<br \> ";
				}
			}
			else
			{
				echo $r." ==> Values missing <br \>";
			}
			$r++;
		}
		//exit;
	}
}
$obj  = new AccuracyImport();
$obj->readCsvFile('accuracy-csv/accuracy.csv');
$obj->insertAccuracy();

echo "Inserted = ".$obj->Inseted."<br>";
echo "Failed = ".$obj->Failed."<br>";
echo "AlreadyExist = ".$obj->alreadyExist."<br>";
