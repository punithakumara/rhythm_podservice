<?php
require_once 'db_config.php';

class Hcm_update_cron extends MysqlConnect{

	public function cron(){
		// Query to update ShiftID in processlead table
		
		$path = "..\..\services\download\\";
		// $path = FCPATH."..\uploads\\";
		$filename = "hcm_".date("d_m_Y");
		$filename .= ".csv";
		
		$hcm_sql="SELECT * FROM hcm_table WHERE DATE(`Date`) = (CURDATE())";
		
		$result = mysql_query($hcm_sql);
		
		ob_start();
		$f = fopen($path.$filename, 'w') or show_error("Can't open php://output");
		$n = 0;        
		
		while( $row = mysql_fetch_assoc( $result)){
		
			$n++;
			if ( ! fputcsv($f, $row))
			{
				show_error("Can't write line $n: $row");
			}
		
		}
		fclose($f) or show_error("Can't close php://output");
		$str = ob_get_contents();
		ob_end_clean();
	}
}

$obj  = new Hcm_update_cron();
$obj->cron();


