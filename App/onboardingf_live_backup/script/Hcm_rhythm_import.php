<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
date_default_timezone_set('Asia/Kolkata');
require_once 'db_config.php';
class HCMFileImport extends MysqlConnect{

	public $CurrentMonthYear;
	public $CurrentDate;
	public $CurrentYear;
	public $CurrentDay;
	public $path;
	
	function __construct() {
	   parent::__construct("final");
       $this->CurrentMonth = date('m');
       $this->CurrentYear = date('Y');
       $this->CurrentMonthYear = date('Y-m');
	   $this->CurrentDay = date('Y-m-d');
	   $this->path = "hcm_files/";
	   
       
    }
	
	function downloadFile($filename)
	{
		//echo "in function\n";
		/* Make sure drive is mapped to directory */ 
		$letter='Q'; 
		$drive= $letter.':\\'; 
		
		/* Create COM object */ 
		$WshNetwork = new COM("WScript.Network"); 
		if(is_dir($drive)) 
		{     //echo "here\n";
			/* Drive is currently mapped, do nothing */ 
		} 
		else 
		{     //echo "now here\n";
			/* Map the drive */ 
			$WshNetwork->MapNetworkDrive( "$letter:" , '\\\hcm\\Interface\\Export' , FALSE, 'analytics\\hcm.rhythm' , 'TheoHCM33!' );// or die("unable to connect"); 
		} 
		//echo "reading\n";
		$path = '//hcm/Interface/Export/'; 
		copy($path.$filename, $this->path.$this->CurrentDay.$filename );
		echo $filename . " has been copied from HCM \n";
		/*$path = 'Q:\\';   */ 

	}
	
	function importCSV($type,$filename){
		
		if($type && $filename) {
			if($type == 'newjoinee'){			
				$tablename = 'employ';						
				$header = array(
							"CompanyEmployID","FirstName","LastName","Email","Gender","LocationID","DesignationID","DateOfBirth","DateOfJoin","Address1","City","State","Zip","Address2","City1","State1","Zip1","Mobile","QualificationID","Training_Status","AssignedVertical","TrainingNeeded","Transitionstatus"					
					);	
				$this->updateNewjoineeData($tablename,$header,$filename);
			}
			else if($type == 'exit'){
				$tablename = 'employ';						
				$this->updateExitData($tablename,$filename);
			}
			else if($type == 'grade'){
				$tablename = 'employ';						
				$header = array("");
				$this->updateGradeChangeData($tablename,$filename);
			}
			else{
			 echo 'Type mismatch';
			}
		}
		
	}

	function getQualificationId($qualification){
		$result = 30; //default Graduation
		if($qualification){
			$qsql = "SELECT qualificationId FROM  `qualifications` WHERE NAME = '".trim($qualification)."'";
			$query = mysql_query($qsql) or die(mysql_error());
			if(mysql_num_rows($query) == 0) 
			{
				$qsqlnone = "SELECT qualificationId FROM  `qualifications` WHERE NAME = 'Graduation'";			
				$query = mysql_query($qsqlnone) or die(mysql_error());	
			}	
			$r = mysql_fetch_array($query);
			$result = $r[0];				
		}
		return $result;
	}
	
	function getVerticalId($vertical)
	{
		$result = 0;
		if($vertical){
			$qsql = "SELECT VerticalID FROM `verticals` WHERE NAME = '".trim($vertical)."'";
			$query = mysql_query($qsql) or die(mysql_error());
			if(mysql_num_rows($query) == 0) 
			{
				$result = 0;
			} else {		
				$r = mysql_fetch_array($query);
				$result = $r[0];
			}
			
			return $result;
		}
	}
	
	function getEmployId($emp){
		$result = 0;
		if($emp){
			$qsql = "SELECT employId FROM  `employ` WHERE CompanyEmployID = '".trim($emp)."'";
			$query = mysql_query($qsql) or die(mysql_error());
			if(mysql_num_rows($query) == 0) 
			{
				$result = 0;
			}	
			$r = mysql_fetch_array($query);
			$result = $r[0];						
		}
		return $result;
	
	}
	function getLocationId($location){			
		$result = 2; // default location id Bengaluru
		if($location){
			$qsql = "SELECT locationid FROM `location` WHERE NAME = '".trim($location)."'";
			//print $qsql;
			$query = mysql_query($qsql) or die(mysql_error());
			if(mysql_num_rows($query) == 0) 
			{
				$qsqlnone = "SELECT locationid FROM `location` WHERE NAME = 'Bengaluru'";
				//print $qsqlnone;
				$query = mysql_query($qsqlnone) or die(mysql_error());	
			}
			$r = mysql_fetch_array($query);
			$result = $r[0];		
		}
		return $result;		
	}
	function getDesignationId($designation){
		$result = 0; //default Contract Resource
		if($designation){
			$qsql = "SELECT DesignationId FROM `designation` WHERE NAME = '".trim($designation)."'";
			//print $qsql;
			$query = mysql_query($qsql) or die(mysql_error());
			if(mysql_num_rows($query) == 0) 
			{
				$qsqlnone = "SELECT DesignationId FROM `designation` WHERE NAME = 'Contract Resource'";
				//print $qsqlnone;
				$query = mysql_query($qsqlnone) or die(mysql_error());	
			}
			$r = mysql_fetch_array($query);
			$result = $r[0];	
		}
		return $result;	
	}
	
	function getGradeByDesignationId($DesignationID){
		$result = 0; //default Contract Resource
		if($DesignationID){
			$qsql = "SELECT grades FROM `designation` WHERE DesignationId = ".trim($DesignationID);
			$query = mysql_query($qsql) or die(mysql_error());
			if(mysql_num_rows($query) == 0) 
			{
				$qsqlnone = "SELECT grades FROM `designation` WHERE NAME = 'Contract Resource'";
				$query = mysql_query($qsqlnone) or die(mysql_error());	
			}
			$r = mysql_fetch_array($query);
			$result = $r[0];	
		}
		return $result;	
		
		
	}
		
	private function updateGradeChangeData($tablename,$filename){
		// path where your CSV file is located		
		$csv_file = $this->path.$filename; 
		if (($handle = fopen($csv_file, "r")) !== FALSE) {
			fgetcsv($handle);   		
				
			/*Rowid Employee id Old Grade Old Designation New Grade New Designation Date Effective*/
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);				
				for ($c=0; $c < $num; $c++) {
					$col[$c] = $data[$c];
				}				
				$recordid= $col[0];
				$CompanyEmployID  = $col[1];
				$oldGrade= $col[2];
				$newGrade= $col[3];
				$oldDesignation= $this->getDesignationId($col[4]);
				$newDesignation= $this->getDesignationId($col[5]);
				
				$sql = "Update ".$tablename ." set DesignationID = ".$newDesignation.",Comments = 'Throug hcm import - designation updated' where CompanyEmployID = '".$CompanyEmployID."'";
				
				//print '\n'.$sql;
				$query = mysql_query($sql) or die(mysql_error());
				 $logArray            = array(
					'DesignationFrom' => $oldDesignation,
					'promoteto' => $newDesignation ,
					'GradeFrom' => $oldGrade,
					'Gradeto' => $newGrade			
				);
				$this->updateEmpLog($CompanyEmployID,$logArray );
				print '\n Employ record updated employid:'.$CompanyEmployID;
		
			}
			fclose($handle);	
		}
	
	}
	private function updateExitData($tablename,$filename){
		// path where your CSV file is located		
		$csv_file = $this->path.$filename; 
		if (($handle = fopen($csv_file, "r")) !== FALSE) {
			fgetcsv($handle);   		
				
			/*Rowid Employee id Resignation Date Last Working Date Grade at the time of exit*/
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);				
				for ($c=0; $c < $num; $c++) {
					$col[$c] = $data[$c];
				}				
				$recordid= $col[0];
				$CompanyEmployID  = $col[1];
				$ResignDate= $col[2];
				if($ResignDate != '')
					$ResignDate = DateTime::createFromFormat('d/m/Y', $col[2])->format('Y-m-d');
				$ReleaveDate=$col[3];
				if($ReleaveDate != ''){
					$ReleaveDate= DateTime::createFromFormat('d/m/Y', $col[3])->format('Y-m-d'); 
					if($ReleaveDate > date('Y-m-d'))
						$releaveFlag = 0;
					else
						$releaveFlag = 1;
				}
				else{
					$releaveFlag = 0;					
				}
				$grade= $col[4];
				$sql = "Update ".$tablename ." set RelieveFlag = ".$releaveFlag;
				if($ReleaveDate)
					$sql .= ",RelieveDate ='".$ReleaveDate."' ";
				$sql .= ",Resigned_Date ='".$ResignDate."',Resign_Flag = 1,Comments = 'Through hcm import - record Updated' where CompanyEmployID = '".$CompanyEmployID."'";
				
				print '\n'.$sql;
				
				$query = mysql_query($sql) or die(mysql_error());
			
				$logArray = array(
				'RelieveNotes' => '',
				'delegateTo' => '',
				'resignationDate' => $ResignDate,
				'relieveDate' => $ReleaveDate,
				'grade'=> $grade,
				'comments'=>'Record updation through HCM'
				);
				$this->updateEmpLog($CompanyEmployID,$logArray );
				
				print '\r\n Employ record updated employid:'.$CompanyEmployID;
		
			}
			fclose($handle);	
		}
			
	}
	
	private function updateNewjoineeData($tablename,$header,$filename){
		// path where your CSV file is located		
		$csv_file = $this->path.$filename; 
		if (($handle = fopen($csv_file, "r")) !== FALSE) {
			fgetcsv($handle);   
			// SQL header Query to insert data into DataBase
				$hsql = "INSERT INTO ".$tablename ."( ";
				foreach($header as $att){
					$hsql.= $att.",";
				}
				$hsql.=  "Comments) VALUES ";
				
			$colcount = count($header);
				
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);				
				for ($c=0; $c < $num; $c++) {
					$col[$c] = $data[$c];
				}

				$CompanyEmployID  = $col[0];
				$FirstName= $col[1];
				$LastName= $col[2];
				$Email= $col[3];
				$Gender=$col[4];
				$LocationID= $this->getLocationId($col[5]);				
				$DesignationID=$this->getDesignationId($col[6]);
				$grade = $this->getGradeByDesignationId($DesignationID);
				$DateOfBirth= DateTime::createFromFormat('d/m/Y', $col[8])->format('Y-m-d');
				$DateOfJoin= DateTime::createFromFormat('d/m/Y', $col[9])->format('Y-m-d');				
				$Address1=$col[10];
				$City=$col[11];
				$State=$col[12];
				$Zip=$col[13];
				$Address2=$col[14];
				$City1=$col[15];
				$State1=$col[16];
				$Zip1=$col[17];
				$Mobile=$col[18];
				$QualificationID= $this->getQualificationId($col[19]);
				$TrainingStatus= 0;
				$assignedVertical = ($col[20]!="")?$this->getVerticalId($col[20]):'';
				$TrainingNeeded = 1;
				$TransitionStatus = ($grade<4)?1:3; //default :1 for G4&above 3: onboarding
				//1:newjoinee, 2: available for transiton , 3 : onboarding
				
				if($CompanyEmployID != NULL){
					$employExist = $this->getEmployId($CompanyEmployID);
					//print 'Employ exist='.$employExist ;
					if(!$employExist){									
					    //"CompanyEmployID","FirstName","LastName","Email","Gender","LocationID","DesignationID","DateOfBirth","DateOfJoin","Address1","City","State","Zip","Address2","City1","State1","Zip1","Mobile","QualificationID,Training_Status,TrainingNeeded,Transitionstatus"				
						$sql =  "('".$CompanyEmployID."','".$FirstName."','".$LastName."','".$Email."'
						,'".$Gender."','".$LocationID."','".$DesignationID."','".$DateOfBirth."','".$DateOfJoin."'
						,'".$Address1."','".$City."','".$State."','".$Zip."','".$Address2."','".$City1."','".$State1."','".$Zip1."','".$Mobile."','".$QualificationID."','".$TrainingStatus."','".$assignedVertical."','".$TrainingNeeded."','".$TransitionStatus."','Throug hcm import')";
						$sql = $hsql.$sql;
						//print $sql;
						$query = mysql_query($sql) or die(mysql_error());
						$insertedEmpId = mysql_insert_id();
							$this->addEmployprocess($CompanyEmployID,$grade);
						$logData = array('Employ'=>$CompanyEmployID,
						'comments'=>'Record import from HCM');
						$this->updateEmpLog($CompanyEmployID,$logData);
						print '\r\n Employ record inserted employid:'.$insertedEmpId;
						
					}
					else{
						//update the records
						$logData = array('Employ'=>$CompanyEmployID,
										 'comments'=>'Record already exist -HCM import failed');
						$this->updateEmpLog($CompanyEmployID,$logData);
						echo '\r\nRecord already exist';
					}
					
				}
				else{
					echo "\r\nCompany employ id can't be null";
					$logData = array('Employ'=>$CompanyEmployID,
										 'comments'=>'Company employ id can not be null');
					$this->updateEmpLog($CompanyEmployID,$logData);
				}					
			}
			fclose($handle);
		}
		else{
			print '\nFile path is not readable';
		}
	}
	function addEmployprocess($compempid,$grade){
		if($compempid && $grade){
			$updatedby = 42; //Jana emp id
			$processid = ($grade<4)?223:221;
			$shiftid = 3;
			$employid = $this->getEmployId($compempid);
			$sql = "INSERT INTO `employprocess`(employid,processid,grades,shiftid)
			VALUES(".$employid.",".$processid.",".$grade.",".$shiftid.")";
			//print $sql;
			$query = mysql_query($sql) or die(mysql_error());			
			
		}
		
	}
	function updateEmpLog($compempid,$logData){
	
		if($compempid && $logData){
			$updatedby = 42; //Jana emp id
			$employid = $this->getEmployId($compempid);
		
			$sql = "INSERT INTO `emp_log`(empid,DATE,empActions,updatedby)
			VALUES(".$employid.",'".date('Y-m-d')."','".serialize($logData)."',".$updatedby.")";
			//print $sql;
			$query = mysql_query($sql) or die(mysql_error());			
			//print 'Employ log is updated for '.$compempid;
		}
	
	}
}

$newjoineefilename = 'EmployeeJoining.csv'; 
$exitemployfilename = 'EmployeeExit.csv'; 
$gradechangefilename = 'EmployeeGradeChange.csv'; 
echo "\r\n";
echo date("l jS \of F Y h:i:s A"); 

$obj = new HCMFileImport();
$obj->downloadFile($newjoineefilename);
$obj->importCSV('newjoinee',$obj->CurrentDay.$newjoineefilename);
$obj->downloadFile($exitemployfilename);
$obj->importCSV('exit',$obj->CurrentDay.$exitemployfilename);
$obj->downloadFile($gradechangefilename);
$obj->importCSV('grade',$obj->CurrentDay.$gradechangefilename);


print "\nHCM records are imported to Rhythm /".$obj->path;

?>


