<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';
class GetNightShiftEmployeeReport extends MysqlConnect{

	public function NightShiftEmployReport()
	{
		$today = date('Y-m-d');
		$start = ($_GET["Startdate"] !="" ? date('Y-m-d',strtotime($_GET["Startdate"])) : "");
		$end = ($_GET["Enddate"] != "" ? date('Y-m-d',strtotime($_GET["Enddate"])) : "");
		$formate = ($_GET["formate"] != "" ? strtolower($_GET["formate"]) : "");

		$sql='';
		$sql.= "SELECT employees.employee_id AS EmployID, employees.company_employ_id AS CompanyEmployID,CONCAT(IFNULL(employees.first_name, '') ,' ', IFNULL(employees.last_name,'')) AS Employ_Name, employees.email AS Email, shift_code AS ShiftCode,emp_shift.from_date AS FromDate,
				emp_shift.to_date AS ToDate,verticals.name AS Vertical_name
				FROM `emp_shift`
				JOIN employees ON employees.employee_id = emp_shift.employee_id
				JOIN employee_vertical ON employee_vertical.employee_id = emp_shift.employee_id
				JOIN verticals ON verticals.vertical_id = employee_vertical.vertical_id
				JOIN `shift` ON `shift`.`shift_id` = emp_shift.`shift_id`
				WHERE status!=6 AND shift_code NOT IN ('EMEA','IST') ";

		$sql .= " AND((from_date <= '".$end."' AND (to_date BETWEEN '".$start."' AND '".$end."')) OR
				(from_date <= '".$end."' AND to_date = '0000-00-00') AND from_date !=0000-00-00)";

		$query_result = mysql_query($sql);
		$totCount 	= mysql_num_rows($query_result);
		if($totCount > 0)
		{
			for($i = 0; $i < mysql_num_rows($query_result); $i++)
			{
				$r = mysql_fetch_array($query_result);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++)
				{
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x]))
					{
						if(mysql_num_rows($query_result) >= 1)
						{
							$result[$i][$key[$x]] = $r[$key[$x]];
						}
						else if(mysql_num_rows($query_result) < 1)
						{
							$result = null;
						}
					}
				}
			}

	 		$results 	= array_merge(array('totalCount' => $totCount),array('rows' => $result));
	 		if($formate=="json")
			{
	 			echo "json<pre>".print_r(json_encode($results));
	 		}
			else if($formate=="array")
			{
	 			echo "array<pre>".print_r($results);
	 		}
		}
		else
		{
			echo "No records to show between ".date('d-m-Y',strtotime($start))." and ".date('d-m-Y',strtotime($end));
		}
		//echo "<pre>".print_r(json_encode($results));
//		return $results;
	
	}
	
}
$obj = new getNightShiftEmployeeReport();
$obj->NightShiftEmployReport();
?>