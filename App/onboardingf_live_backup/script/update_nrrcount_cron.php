<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
date_default_timezone_set('Asia/Kolkata');
require_once 'db_config.php';
class NRRHeadCountUpdateCron extends MysqlConnect{
	private function _getProductionDate()
	{
		$today=date('Y-m-d');
		$sql = "SELECT client_nrr.NRR_ID,client_nrr.AddedBy , client_nrr.ClientID, 
				DATE_FORMAT(client_nrr.Date_Of_Request, '%d-%m-%Y') AS Date_Of_Request,
				CONCAT(IFNULL(employ.`FirstName`, ''),' ',IFNULL(employ.`LastName`, '')) AS RaisedName, 
				client_nrr.VerticalID, client_nrr.ProcessID, DATE_FORMAT(shifts_resources_nrr.Production_Date, '%d-%m-%Y') AS Production_Date,
				client_nrr.Description, client_nrr.Flag, client.Client_Name AS client_Name,
				vertical.Name AS Verticl_Name, process.Name AS Process_Name,
				shifts_resources_nrr.Resources AS Resources,
				shifts_resources_nrr.No_of_Resources AS No_of_Resources,
				shifts_resources_nrr.ShiftID AS ShiftID FROM client_nrr
				JOIN process ON client_nrr.ProcessID = process.ProcessID
				JOIN client ON client_nrr.ClientID = client.ClientID
				JOIN vertical ON client_nrr.VerticalID = vertical.VerticalID
				LEFT JOIN `employ` ON client_nrr.AddedBy = employ.EmployID
				LEFT JOIN `shifts_resources_nrr` ON client_nrr.NRR_ID =shifts_resources_nrr.NRR_ID
				WHERE 1=1  AND client_nrr.Flag NOT IN( 'Closed','Cancelled') AND shifts_resources_nrr.Production_Date ='".date('Y-m-d')."'";
		$query = mysql_query($sql) or die(mysql_error()."_getProductionDate");
		if(mysql_num_rows($query) == 0) 
			return FALSE;
		else
		{
			$result = array();
			for($i = 0; $i < mysql_num_rows($query); $i++){
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x])){
						if(mysql_num_rows($query) >= 1){
							$result[$i][$key[$x]] = $r[$key[$x]];
						}else if(mysql_num_rows($query) < 1){
							$result = null;
						}
					}
				}
			}
			print_r($result);
			return $result;
		}	
	}
	
	public function updateHeadCount()
	{
		$DataArray = $this->_getProductionDate();
		$total_resource_count = 0;
		if(is_array($DataArray) ) {
			foreach($DataArray as $key => $val)
			{
				if(strtotime($val['Production_Date']) == strtotime(date('Y-m-d')))
				{
					$total_resource_count=$val['No_of_Resources'];
					$sql  = "UPDATE process SET HeadCount = (HeadCount+".$total_resource_count."),
							ForcastNRR = IF(ForcastNRR >0,ForcastNRR-".$total_resource_count.",ForcastNRR)
							WHERE ProcessID=" . $val['ProcessID'] ;
					echo "\r\n".$sql;		
							
					$sql = "UPDATE process SET HeadCount = (HeadCount+'" . $total_resource_count . "'),ForcastNRR = (ForcastNRR-'" . $total_resource_count . "') WHERE ProcessID='" . $val['ProcessID'] . "'";
					$query = mysql_query($sql);
				}
				//echo "Updated Rows=>".$val['NRR_ID'];
			}		
		}
	}	
}
$obj = new NRRHeadCountUpdateCron();
echo "\r\n";
echo date("l jS \of F Y h:i:s A"); 
$obj->updateHeadCount();
?>
