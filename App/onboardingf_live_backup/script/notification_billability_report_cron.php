<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
//ini_set('xdebug.max_nesting_level', 200); 
require_once 'db_config.php';

class NotificationBillabilityReportCron extends MysqlConnect{
	
	public function cron()
	{
		$notificationSuccessCount = '';
		$notificationErrorCount = '';
		$supervisorsTotalCount = '';
		
		// an variables of parameters to insert notifications in to notification table
		$Title = "Billability Report";
		$Description = "Please Submit Your Billability Report";
		$SenderEmployee = 42;
		$NotificationDate = date("Y-m-d h:i:s");
		$ReadFlag = 0;
		$Type = 5;
		$DeleteFlag = 0;
		
		// query to fetch all the employees who have grade >= 4
		$grades = 3;
		$sql = "SELECT employ.`EmployID` FROM employ
		        JOIN designation ON employ.designationid = designation.designationid
		        WHERE employ.RelieveFlag=0 AND designation.grades >" .$grades . "  ORDER BY EmployID";
		
		$result =  mysql_query($sql) or die(mysql_error()."Query error");
		
		while($res = mysql_fetch_array($result))
		{
			$ReceiverEmployee = $res['EmployID'];
			// Adding notifications for the grade >= 4 employees
			$insertSql = "INSERT INTO notification ".
		       "(Title, Description, SenderEmployee, ReceiverEmployee, NotificationDate, ReadFlag, Type, DeleteFlag) ".
		       "VALUES ".
		       "('$Title','$Description','$SenderEmployee','$ReceiverEmployee','$NotificationDate','$ReadFlag','$Type','$DeleteFlag')";
			
			if(! mysql_query($insertSql) )
			{
				// counting error notifications
				$notificationErrorCount++;
				$notificationError[$notificationErrorCount] = $res['EmployID'];
				$supervisorsTotalCount++;
			}
			else
			{
				// counting success notifications
				$notificationSuccessCount++;
				$supervisorsTotalCount++;
			}
		}
		
		// Notification success message
		if(count($notificationSuccessCount) > 0)
		{
			echo "$notificationSuccessCount Out Of $supervisorsTotalCount Notifications Added Successfully </br></br>";
		}
		
		// Notification error message
		if(isset($notificationError))
		{
			echo "Error While Adding Notifications For The Following $notificationErrorCount EmployID(s)</br>";
			foreach($notificationError as $errNoti)
			{
				echo "$errNoti, ";
			}
		}
	}
}

$obj  = new NotificationBillabilityReportCron();
$obj->cron();