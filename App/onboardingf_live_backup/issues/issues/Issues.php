<?php
require ('../config/mysql_crud.php');
session_start();
if(!isset($_SESSION['authemail'])) 
	header("Location: Login.php");
	
$empId = isset($_SESSION['empId']) ?$_SESSION['empId'] :'';
$empName = isset($_SESSION['authname'])?$_SESSION['authname']:'';
$empEmail = isset($_SESSION['authemail'])?$_SESSION['authemail']:'';
$admin = isset($_SESSION['admin'])?$_SESSION['admin']: 0;
$id = isset($_REQUEST["id"])?$_REQUEST["id"]:null;
$db = new Database();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rhythm Issues</title>
<style type="text/css">
body			{ padding:0px; margin:0px; font-family:Tahoma, Geneva, sans-serif; }
h1, h2, h3, h4			{ margin:0px; padding:0px; font-weight:normal; background-color:#005589; color:#ffffff; line-height:25px; height:25px; font-size:12px; width:100%;   }
input[type=text]	{ background-color:#f2f2f2; font-size:12px; font-family:Tahoma, Geneva, sans-serif; outline:none; height:20px; width:30%;resize:none; }
.Wrapper		{ width:1200px; min-height:300px; margin:0px auto; position:relative; margin-top:20px; }
.Wrapper h1		{ text-align:center; border-top-left-radius:5px; border-top-right-radius:5px; }
.Header			{ background:#005589; width:100%; height:45px; }
.Header span	{ width:auto; height:35px; background-color:#ffffff; padding-top:10px; float:left; margin-left:5px; }
.ContentWrapper	{ float:left; width:1198px; border:1px solid #005589; }
.ContentWrapper table	{ margin:5px;  border-right:none; border-bottom:none; }
.ContentWrapper td	{ font-size:12px; color:#333333; height:25px; line-height:20px; border-right:1px solid #cccccc; border-bottom:1px solid #cccccc; }

.ContentWrapper h2		{ padding-left:5px; width:100%; }
.ContentWrapper textarea	{ width:97%; height:50px; margin:5px; resize:none;}
.ContentWrapper h3, .ContentWrapper h4			{ font-weight:bold; background:none; color:#333333; margin-left:5px; width:98%; }
.ContentWrapper h4			{ margin:0px; height: auto; line-height: 17px; margin-left:5px; }
.NoBorder, .NoBorder td					{ border:none; }
.Benfits input[type=text]	{ width:98%;  margin-bottom: 3px; margin-left: 3px; }
.BenfitsTxtBox input[type=text]	{ width:97%; }
.ContentWrapper input[type=radio]	{ margin-left:4px; margin-top:4px; outline:none; }
.ContentWrapper span		{ float:left; }
.Footer 					{ width: 100%; height: 25px; background-color: #005589; float: left; color: #ffffff; font-size: 11px; text-align: center;
line-height: 25px; margin-top: 20px; }
.btTxt						{ background: none repeat scroll 0 0 #f8981f; border: medium none; color: #ffffff; margin: 5px auto; padding: 5px;
							  width: auto; }
.BtnOtr						{ text-align:center; float:left; width:100%; } 
.InputStyle input[type=text]	{ width:99.5%; }
.successful					{ display: block;  font-size: 25px; text-align: center; color: #008000; margin-top:15%; padding: 10px; width: 100%;}
ol, ul {
	list-style: none;
}
.RightOtr					{ width:auto; float:right; margin-right:5px; height:25px; text-align:right; margin-top:10px; }
.RightOtr span				{ font-family:Tahoma, Geneva, sans-serif; font-size:12px; color:#ffffff; line-height:25px; float:left; margin:0 5px; }
</style>

<style type="text/css">
body { font-size: 11px; font-family: "verdana"; }

pre { font-family: "verdana"; font-size: 10px; background-color: #FFFFCC; padding: 5px 5px 5px 5px; }
pre .comment { color: #008000; }
pre .builtin { color:#FF0000;  }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../css/jquery.validate.css" />
    <script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
    <script src="../js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/myquery.js"></script>
   <link rel="stylesheet" type="text/css" href="../css/tcal.css" />
	<script type="text/javascript" src="../js/tcal.js"></script> 
<script type="text/javascript">
	function resetForm(){	
		document.getElementById("submodule").value ="";
		document.getElementById("module_name").value = "";
		document.getElementById("short_desc").value = "";
		document.getElementById("description").value = "";
		document.getElementById("fileInput").value = "";
	}
	function removeFileAttachment(){
		document.getElementById("fileInput").value = "";
	}
	function onChange(val){
	//'Dashboard',2=>'Ops Data',3=>'Master Data',4=>'Documents'
		submodule = document.getElementById('submodule');
		submodule.style.visibility = "visible";
		submodule.options = null;
		var length = submodule.options.length;
		for (i = 0; i < 7; i++) {
			submodule.options[i] = null;
		}
		submodule.options[0] = new Option('Select', 'Select');
		if(val == 'Dashboard'){
			submodule.options[1] = null;
			submodule.options[2] = null;
			submodule.options[3] = null;
			submodule.options[4] = null;
			submodule.options[5] = null;
			submodule.style.visibility = "hidden";	  
		}
		else if( val == 'Ops Data'){	
			submodule.options[1] = new Option('People Transition', 'People Transition');
			submodule.options[2] = new Option('People Billability', 'People Billability');
			submodule.options[3] = new Option('Metrics', 'Metrics');
			submodule.options[4] = new Option('Incident Log', 'Incident Log');
			submodule.options[5] = new Option('Meeting', 'Meeting');		
		}
		else if( val == 'Master Data'){
		/*Process,Employee,Technologies,Attributes*/
			submodule.options[1] = new Option('Process', 'Process');
			submodule.options[2] = new Option('Employee', 'Employee');
			submodule.options[3] = new Option('Technologies', 'Technologies');
			submodule.options[4] = new Option('Attributes', 'Attributes');	
			submodule.options[5] = null;
		}
		else if( val == 'Documents'){
		 /*Process Docs*/
			submodule.options[1] = new Option('Process Docs', 'Process Docs');
			submodule.options[2] = null;
			submodule.options[3] = null;
			submodule.options[4] = null;
			submodule.options[5] = null;
		}		
	}
</script> 
<script type="text/javascript">
jQuery(function(){
	jQuery("#module_name").validate({
        expression: "if (VAL != '') return true; else return false;",
        message: "Please Enter User Name."
    });
	jQuery("#short_desc").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter Password."
    });
});

$(document).ready(function(){
	$("#username").focus();
});
</script> 
</head>
<body>


<div class="Header">
	<span><img src="../images/theorem_logo.jpg" width="118" height="29" /></span>
	<div class="RightOtr">
    	<span style="background-color:#005589; padding-top:0px;">Hi &nbsp;<strong><?php echo $empName; ?> </strong>&nbsp;</span>
    	<a href="../login/logout.php?action=issue" title="Logout"><img width="16" height="22" border="0" src="../images/logout_btn.png"></a>
    </div>
</div>
<div class="Wrapper">
<?php 
if($id){
 //fetch innovation date and add to form 
 $result = $db->select('issues','*','','issue_id='.$id);
 
 $result_array = $db->getResult();
 foreach($result_array as $key => $val) {
 //set all variaables
	  $empId = $result_array['emp_id'];
	  $empName = $result_array['emp_name'];
	  $empEmail = $result_array['emp_email'];
	  $module_name = $result_array['module_name'];
	  $submodule = $result_array['submodule'];
	  $short_desc = $result_array['short_desc'];
	  $description = $result_array['description'];
	  $file_name = $result_array['filename'];
	  $path = $result_array['filepath'];	
      $status =	 $result_array['status_flag'];	
	  $version = $result_array['fixed_version'];		  
	}	
	$select_module = "<option value=''>Select</option>"; 
	$result_array = array('Dashboard'=>'Dashboard','Ops Data'=>'Ops Data','Master Data'=>'Master Data','Documents'=>'Documents');
	foreach($result_array as $key => $val) {	
		if($val == $module_name){
		$select_module .= "<option value='".$val."' selected='selected' >".$val."</option>";
		}
		else
		{
		$select_module .= "<option value='".$val."'>".$val."</option>";
		}
    }
	
	if($status == 'N'){
		$status_option = "<option value='N' selected='selected'>N</option>";
		$status_option .= "<option value='Y'>Y</option>";
	}
	else{
		$status_option = "<option value='Y' selected='selected'>Y</option>";
		$status_option .= "<option value='N'>N</option>";
	}
	
	if($submodule=='')
	{
		$select_submodule = "<option value=''>Select</option>";
	} 
	else{
		$select_submodule = "<option value='".$submodule."' selected='selected' >".$submodule."</option>"; 
	}
}
else{
	$select_module = "<option value=''>Select</option>"; 
	$result_array = array('Dashboard'=>'Dashboard','Ops Data'=>'Ops Data','Master Data'=>'Master Data','Documents'=>'Documents');
	foreach($result_array as $key => $val)		
	{
		$select_module .= "<option value='".$val."'>".$val."</option>";
	}
		$select_submodule = "<option value=''>Select</option>";


	$status_option = "<option value='N'>N</option>";
	$status_option .= "<option value='Y'>Y</option>";
}

$formDisplay = 1;
$parameters = $_POST;
$process = TRUE ;
if (isset($_POST['submit'])) {	
	$formDisplay = 0;
	$querydata =array();	
	$querydata['filename'] = null;
	$querydata['filepath'] = null;
	
	if(isset($_FILES["uploadedfile"]["name"])){	 
		if(!$_FILES['uploadedfile']['error'])
		{
			if (file_exists("issues/uploads/" . $_FILES["uploadedfile"]["name"])) {
				echo "Uploaded file <b>".$_FILES["uploadedfile"]["name"] . "</b> is already exists in the repository. ";
				$process = FALSE;
			} 
			else {

				$destination_path = getcwd().DIRECTORY_SEPARATOR;
				$target_path = $destination_path.'/uploads/' . basename($_FILES["uploadedfile"]["name"]); 

				move_uploaded_file($_FILES["uploadedfile"]["tmp_name"],  $target_path);	 
				
				
				$querydata['filename'] = $_FILES["uploadedfile"]["name"];
				$querydata['filepath'] = "uploads/" . $_FILES["uploadedfile"]["name"];	
			}
		}
		else{
			if($_FILES['uploadedfile']['error'] != 4) {
				echo "File Uploaded Error <b>".$_FILES['uploadedfile']['error'];			
				$process = FALSE;
			}
		}
	} 	
	$querydata['emp_id'] = $parameters['issue_emp_id'];
	$querydata['emp_name'] =$parameters['issue_emp_name'];
	$querydata['emp_email'] =$parameters['issue_email'];
	
	if(!$parameters["issue_id"]){
		$querydata['module_name'] =$parameters['module_name'];
		$querydata['submodule'] = $parameters['submodule'];
	}
	$querydata['short_desc'] = strlen($parameters['short_desc'] > 250) ? mysql_real_escape_string(substr($parameters['short_desc'],0,249)) : mysql_real_escape_string($parameters['short_desc']);
	$querydata['description'] = mysql_real_escape_string($parameters['description']);
	$querydata['submissiondate'] =  date('Y-m-d');
	if($process){
		if($parameters["issue_id"] > 0)
		{		
			$uquerydata['status_flag'] = ($parameters['status'] == "No")?"N":"Y";
			$uquerydata['fixed_version'] = strlen($parameters['fixed_version'] > 10) ? substr($parameters['fixed_version'],0,9):$parameters['fixed_version'];
			$where = 'issue_id = '.$parameters["issue_id"].'';
			$retval = $db->update("issues", $uquerydata,$where);		
			if(!$retval )
			{
				die('Could not update data: ' . mysql_error());
			}		
			else{
				echo "Issue is Updated successfully";
			}
		}
		else
		{
			$retval = $db->insert("issues", $querydata);	
			if(! $retval )
			{
				die('Could not enter data: ' . mysql_error());
			}		
			else{
				echo "Issue is Added successfully";				
				//$formDisplay = 1;
			}
		}		
		header( "Location:listIssues.php"); 
	}			
}	
if($formDisplay) {
?>
<form name="issue_form" id="issueList"class="innovative" method="post" action="Issues.php" enctype="multipart/form-data"> 
	<h1>Rhythm Proces</h1>
    <div class="ContentWrapper">
		<table class="formTable" width="99%" cellpadding="0" cellspacing="0" border="0" style="border-top:1px solid #cccccc; border-left:1px solid #cccccc;">
			<tr>
				<td width="80%" valign="top"> 
					<table width="99%" class="NoBorder"> 
						<tr> 
							<td align="left" valign="top">Employee ID  <font color=red>*</font></td>
							<td>
								<input type="text" tabindex=1 value="<?php  if(isset($empId)) echo $empId;?>" readonly name="issue_emp_id" id="issue_emp_id" />
							</td>
						</tr>
						<tr>					
							<td width="155" align="left" valign="top">Employee Name <font color=red>*</font></td>
							<td>
								<input type="text" tabindex=2 value="<?php  if(isset($empName)) echo $empName;?>"  readonly maxlength="25" name="issue_emp_name" id="issue_emp_name" />
							</td>
						</tr>									
						<tr>
							<td align="left" valign="top">Employee Email  <font color=red>*</font></td>
							<td>
								<input type="text" tabindex=3 maxlength="50" value="<?php if(isset($empEmail)) echo $empEmail; else echo '@theoreminc.net'; ?>" readonly name="issue_email" id="issue_email" />
							</td>
						</tr>									
						<tr>
							<td align="left" valign="top">Module  <font color=red>*</font></td>
							<td>
								<select name="module_name" tabindex="4" id="module_name" onChange="onChange(this.value);"style="float:left; width:292px;" <?php  if(isset($id)) { echo "disabled";}?>>				
									<?php echo $select_module; ?>
								</select>
							</td>
						</tr>
						<tr>
						  <td align="left" valign="top">Sub module  <font color=red>*</font></td>
							<td>
								<select name="submodule" tabindex="5" id="submodule" style="width:292px;" <?php  if(isset($id)) { echo "disabled";}?>>
									<?php echo $select_submodule; ?>
								</select>
						  </td>
					   </tr>				 
						<tr>
							<td colspan="2">
								<h2>Issue details: <font color=red>*</font></h2>
							</td>
						</tr>
						<tr>
							<td width="20" align="left" >Short Description <font color=red>*</font></td>
							<td width="653" align="left" valign="middle"><input tabindex="6" maxlength="250" style="width: 96%;" type="text" value="<?php if(isset($short_desc)) echo  $short_desc ;?>" <?php  if(isset($id)) { echo "readonly";}?> name="short_desc" id="short_desc" /></td>
						</tr>
						<tr>
							<td width="20" align="left">Description</td>
							<td valign="top" align="left">
								<textarea tabindex="7" name="description" id="description"  style="height:78px; width: 96%; margin:0px;" <?php  if(isset($id)) { echo "readonly";}?>><?php if(isset($description)) echo $description;?></textarea>
							</td>									
						</tr>
						<tr>
							<td width="20" align="left" >File Attachment</td>
							<?php  if(!$id){?>
							<td valign="top" align="left">
								 <input style="width: 31%;" tabindex="8" name="uploadedfile" type="file" id="fileInput"  <?php  if(isset($id)) { echo "readonly";}?> />
									
								 <img onclick="removeFileAttachment();" tabindex="9" src='images/delete2.jpg' style="height:20px; width:20px">
							</td>			<?php } else { ?>
							<td valign="top" align="left">				
								<a href='<?php echo $path;?>' target="_blank" alt=<?php echo $file_name;?>><?php echo $file_name;?></a>
							</td>
							<?php } ?>
						</tr>
						<tr>
							<td colspan="2">
								<div id="resolutionDetails" <?php if (!$admin){?>style="display:none"<?php } ?>>
									<table class="formTable" width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:0px;">
										<tr>
											<td width="100%" valign="top" align="left"> 
												<table width="100%" class="NoBorder" style="margin:0px;"> 
													<tr> 
														<td colspan="2" >
															<h2>Resolution details: <font color=red>*</font></h2>
														</td>								
													</tr>			
													<tr>
														<td width="225" align="left" >Fixed Status</td>
														<td valign="top" align="left">
															<select name="status" tabindex="10" id="status" style="width:292px;" >
																<?php echo $status_option; ?> 										
															</select>
														</td>									
													</tr>
													<tr>
														<td width="20" align="left" >Version</td>
														<td valign="top" align="left">
															<input type="text" tabindex=11 value="<?php  if(isset($version)) echo $version;?>"  maxlength="10" name="fixed_version" id="fixed_version" />
														</td>									
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>	
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>		
	</div>	
	<div class="BtnOtr">
		<input id="saveForm" class="btTxt"  type="submit" name="submit" value="Submit"/>
		<input id="saveForm" class="btTxt"  type="button" name="reset" value="Cancel" onClick="window.location.href='listIssues.php'" />
		<input id="issue_id" type="hidden" value=<?php if(isset($id)) echo $id; else echo '0';?>  name="issue_id"/>
	</div>		
</form>

<?php
}
?>
</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>