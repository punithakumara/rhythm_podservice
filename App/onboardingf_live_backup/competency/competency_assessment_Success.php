<?php
session_start();
include('../employee/employee_model.php');
$EmployID = $_REQUEST['id'];
$name= '';
if($EmployID){
	$Empdetails = getEmployName($EmployID);
	$name = $Empdetails['Name'];	
}

$query_exe = mysql_query("SELECT employee_id AS EmployID, company_employ_id AS CompanyEmployID FROM employees WHERE employee_id = '".$_SESSION['user_employ_id']."' AND status!=6");
$query_result = mysql_fetch_assoc($query_exe);

$sql = mysql_query("SELECT employee_id AS EmployID, CONCAT(IFNULL(`first_name`, ''),' ',IFNULL(`last_name`, '')) AS Name FROM employees WHERE primary_lead_id = '".$_SESSION['user_employ_id']."' AND status!=6 "); //AND `DateOfJoin` BETWEEN '2017-01-01' AND '2017-06-30'
$num_rows = mysql_num_rows($sql);

$sql_comp = mysql_query("SELECT EmployID FROM competency_assessment WHERE Primary_Lead_ID = ".$query_result['EmployID']); //AND `DateOfJoin` BETWEEN '2017-01-01' AND '2017-06-30'

while($compdetails = mysql_fetch_array($sql_comp))
{
	$comp_array[] = $compdetails['EmployID'];
}

if($num_rows > 0)
{
	$_SESSION['Cmpetency_Primary_Lead_ID'] = $query_result['EmployID'];
	
	$Employees = "<option value=''>-- Select Employee --</option>";;
	while($Empdetails = mysql_fetch_array($sql))
	{
		if(!in_array($Empdetails['EmployID'], $comp_array))
		{
			$Employees .= "<option value='".$Empdetails['EmployID']."'>".$Empdetails['Name']."</option>";
		}
	}
	
	$_SESSION['competency_Employees'] = $Employees;
}


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Competency Assessment</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
    <script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <script>
		function goBack() {
		    window.history.back();
		}
	</script>
	
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
	<div class="logout"><a href="logout.php?action=logout">Logout</a></div>
</div>
<div id="container">
<!--<h1>Survey Form</h1>-->
	<table class="formTable">
		<tr>
			<td colspan="2">
				<h1>Competency Assessment Success</h1>
			</td>
		</tr>
		<tr>
			<td>
				<table class="formTable" style="width:600px !important;">
					<tr>
						<td>
							<label class="desc">Competency Assessment data updated succesfully for <?php echo $name; ?>.</label>
						</td>
					</tr>
					<tr>
						<td>
							<button class="btTxt" onclick="goBack()">Assess another team member</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</div>
<div class="Footer">&copy; 2018-19 Theorem</div>
</body>
</html>