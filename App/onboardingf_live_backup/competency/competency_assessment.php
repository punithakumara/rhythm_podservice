<?php 
error_reporting(0);
session_start();
if(!isset($_SESSION['Cmpetency_Primary_Lead_ID']))
{
	header('Location: competency_user.php');
	exit;
}
include('../employee/employee_model.php');

	$db = new Database();
	$Employees = "";
	if(isset($_SESSION['competency_Employees']) && isset($_SESSION['Cmpetency_Primary_Lead_ID']))
	{
		$Employees = $_SESSION['competency_Employees'];
		 
		//unset($_SESSION['competency_Employees']);
	}

	if(isset($_POST['saveForm']) && count($_POST)==2)
	{
		header("Location: competency_assessment_Error.php?data=0");
		exit;
	}
	else if(isset($_POST['saveForm']) && count($_POST)>2)
	{
		$EmployID = $_POST['EmployID'];
		$Primary_Lead_ID = $_SESSION['Cmpetency_Primary_Lead_ID'];
		
		$Communication_1 = $_POST['Communication_1']?$_POST['Communication_1']: '';
		$Communication_2 = $_POST['Communication_2']?$_POST['Communication_2']: '';
		$Communication_3 = $_POST['Communication_3']?$_POST['Communication_3']: '';
		$Communication_4 = $_POST['Communication_4']?$_POST['Communication_4']: '';
		$Communication_5 = $_POST['Communication_5']?$_POST['Communication_5']: '';
		$Communication_6 = $_POST['Communication_6']?$_POST['Communication_6']: '';
		$Communication_7 = $_POST['Communication_7']?$_POST['Communication_7']: '';
		$Communication_8 = $_POST['Communication_8']?$_POST['Communication_8']: '';
		$Communication_9 = $_POST['Communication_9']?$_POST['Communication_9']: '';
		
		
		$Customer_Orientation_1 = $_POST['Customer_Orientation_1']?$_POST['Customer_Orientation_1']: '';
		$Customer_Orientation_2 = $_POST['Customer_Orientation_2']?$_POST['Customer_Orientation_2']: '';
		$Customer_Orientation_3 = $_POST['Customer_Orientation_3']?$_POST['Customer_Orientation_3']: '';
		$Customer_Orientation_4 = $_POST['Customer_Orientation_4']?$_POST['Customer_Orientation_4']: '';
		$Customer_Orientation_5 = $_POST['Customer_Orientation_5']?$_POST['Customer_Orientation_5']: '';
		$Customer_Orientation_6 = $_POST['Customer_Orientation_6']?$_POST['Customer_Orientation_6']: '';
		$Customer_Orientation_7 = $_POST['Customer_Orientation_7']?$_POST['Customer_Orientation_7']: '';
		$Customer_Orientation_8 = $_POST['Customer_Orientation_8']?$_POST['Customer_Orientation_8']: '';
		$Customer_Orientation_9 = $_POST['Customer_Orientation_9']?$_POST['Customer_Orientation_9']: '';
		$Customer_Orientation_10 = $_POST['Customer_Orientation_10']?$_POST['Customer_Orientation_10']: '';
		
		$Integrity_Work_Ethics_1 = $_POST['Integrity_Work_Ethics_1']?$_POST['Integrity_Work_Ethics_1']: '';
		$Integrity_Work_Ethics_2 = $_POST['Integrity_Work_Ethics_2']?$_POST['Integrity_Work_Ethics_2']: '';
		$Integrity_Work_Ethics_3 = $_POST['Integrity_Work_Ethics_3']?$_POST['Integrity_Work_Ethics_3']: '';
		$Integrity_Work_Ethics_4 = $_POST['Integrity_Work_Ethics_4']?$_POST['Integrity_Work_Ethics_4']: '';
		$Integrity_Work_Ethics_5 = $_POST['Integrity_Work_Ethics_5']?$_POST['Integrity_Work_Ethics_5']: '';
		$Integrity_Work_Ethics_6 = $_POST['Integrity_Work_Ethics_6']?$_POST['Integrity_Work_Ethics_6']: '';
		$Integrity_Work_Ethics_7 = $_POST['Integrity_Work_Ethics_7']?$_POST['Integrity_Work_Ethics_7']: '';
		$Integrity_Work_Ethics_8 = $_POST['Integrity_Work_Ethics_8']?$_POST['Integrity_Work_Ethics_8']: '';
		
		$Dependable_Accountable_1 = $_POST['Dependable_Accountable_1']?$_POST['Dependable_Accountable_1']: '';
		$Dependable_Accountable_2 = $_POST['Dependable_Accountable_2']?$_POST['Dependable_Accountable_2']: '';
		$Dependable_Accountable_3 = $_POST['Dependable_Accountable_3']?$_POST['Dependable_Accountable_3']: '';
		$Dependable_Accountable_4 = $_POST['Dependable_Accountable_4']?$_POST['Dependable_Accountable_4']: '';
		$Dependable_Accountable_5 = $_POST['Dependable_Accountable_5']?$_POST['Dependable_Accountable_5']: '';
		$Dependable_Accountable_6 = $_POST['Dependable_Accountable_6']?$_POST['Dependable_Accountable_6']: '';
		$Dependable_Accountable_7 = $_POST['Dependable_Accountable_7']?$_POST['Dependable_Accountable_7']: '';
		$Dependable_Accountable_8 = $_POST['Dependable_Accountable_8']?$_POST['Dependable_Accountable_8']: '';
		
		$Team_Collaboration_1 = $_POST['Team_Collaboration_1']?$_POST['Team_Collaboration_1']: '';
		$Team_Collaboration_2 = $_POST['Team_Collaboration_2']?$_POST['Team_Collaboration_2']: '';
		$Team_Collaboration_3 = $_POST['Team_Collaboration_3']?$_POST['Team_Collaboration_3']: '';
		$Team_Collaboration_4 = $_POST['Team_Collaboration_4']?$_POST['Team_Collaboration_4']: '';
		$Team_Collaboration_5 = $_POST['Team_Collaboration_5']?$_POST['Team_Collaboration_5']: '';
		$Team_Collaboration_6 = $_POST['Team_Collaboration_6']?$_POST['Team_Collaboration_6']: '';
		$Team_Collaboration_7 = $_POST['Team_Collaboration_7']?$_POST['Team_Collaboration_7']: '';
		$Team_Collaboration_8 = $_POST['Team_Collaboration_8']?$_POST['Team_Collaboration_8']: '';
        $Team_Collaboration_9 = $_POST['Team_Collaboration_9']?$_POST['Team_Collaboration_9']: '';
		$Team_Collaboration_10 = $_POST['Team_Collaboration_10']?$_POST['Team_Collaboration_10']: '';
		$Team_Collaboration_11 = $_POST['Team_Collaboration_11']?$_POST['Team_Collaboration_11']: '';
		$Team_Collaboration_12 = $_POST['Team_Collaboration_12']?$_POST['Team_Collaboration_12']: '';
		$Team_Collaboration_13 = $_POST['Team_Collaboration_13']?$_POST['Team_Collaboration_13']: '';
		
		$Influencing_Ability_1 = $_POST['Influencing_Ability_1']?$_POST['Influencing_Ability_1']: '';
		$Influencing_Ability_2 = $_POST['Influencing_Ability_2']?$_POST['Influencing_Ability_2']: '';
		$Influencing_Ability_3 = $_POST['Influencing_Ability_3']?$_POST['Influencing_Ability_3']: '';
		$Influencing_Ability_4 = $_POST['Influencing_Ability_4']?$_POST['Influencing_Ability_4']: '';
		$Influencing_Ability_5 = $_POST['Influencing_Ability_5']?$_POST['Influencing_Ability_5']: '';
		$Influencing_Ability_6 = $_POST['Influencing_Ability_6']?$_POST['Influencing_Ability_6']: '';
		$Influencing_Ability_7 = $_POST['Influencing_Ability_7']?$_POST['Influencing_Ability_7']: '';
        $Influencing_Ability_8 = $_POST['Influencing_Ability_8']?$_POST['Influencing_Ability_8']: '';
		$Influencing_Ability_9 = $_POST['Influencing_Ability_9']?$_POST['Influencing_Ability_9']: '';
		$Influencing_Ability_10 = $_POST['Influencing_Ability_10']?$_POST['Influencing_Ability_10']: '';



		
		$Coaching_Mentoring_1 = $_POST['Coaching_Mentoring_1']?$_POST['Coaching_Mentoring_1']: '';
		$Coaching_Mentoring_2 = $_POST['Coaching_Mentoring_2']?$_POST['Coaching_Mentoring_2']: '';
		$Coaching_Mentoring_3 = $_POST['Coaching_Mentoring_3']?$_POST['Coaching_Mentoring_3']: '';
		$Coaching_Mentoring_4 = $_POST['Coaching_Mentoring_4']?$_POST['Coaching_Mentoring_4']: '';
		$Coaching_Mentoring_5 = $_POST['Coaching_Mentoring_5']?$_POST['Coaching_Mentoring_5']: '';
		$Coaching_Mentoring_6 = $_POST['Coaching_Mentoring_6']?$_POST['Coaching_Mentoring_6']: '';
		
		

        $planning_organization_1 = $_POST['planning_organization_1']?$_POST['planning_organization_1']: '';
        $planning_organization_2 = $_POST['planning_organization_2']?$_POST['planning_organization_2']: '';
        $planning_organization_3 = $_POST['planning_organization_3']?$_POST['planning_organization_3']: '';
        $planning_organization_4 = $_POST['planning_organization_4']?$_POST['planning_organization_4']: '';
        $planning_organization_5 = $_POST['planning_organization_5']?$_POST['planning_organization_5']: '';
        $planning_organization_6 = $_POST['planning_organization_6']?$_POST['planning_organization_6']: '';
        $planning_organization_7 = $_POST['planning_organization_7']?$_POST['planning_organization_7']: '';
        $planning_organization_8 = $_POST['planning_organization_8']?$_POST['planning_organization_8']: '';
        $planning_organization_9 = $_POST['planning_organization_9']?$_POST['planning_organization_9']: '';
        $planning_organization_10 = $_POST['planning_organization_10']?$_POST['planning_organization_10']: '';
        
                   
 	
		$Innovation_1 = $_POST['Innovation_1']?$_POST['Innovation_1']: '';
		$Innovation_2 = $_POST['Innovation_2']?$_POST['Innovation_2']: '';
		$Innovation_3 = $_POST['Innovation_3']?$_POST['Innovation_3']: '';
		$Innovation_4 = $_POST['Innovation_4']?$_POST['Innovation_4']: '';
		$Innovation_5 = $_POST['Innovation_5']?$_POST['Innovation_5']: '';
		$Innovation_6 = $_POST['Innovation_6']?$_POST['Innovation_6']: '';
		$Innovation_7 = $_POST['Innovation_7']?$_POST['Innovation_7']: '';
		$Innovation_8 = $_POST['Innovation_8']?$_POST['Innovation_8']: '';
		$Innovation_9 = $_POST['Innovation_9']?$_POST['Innovation_9']: '';
        $Innovation_10 = $_POST['Innovation_10']?$_POST['Innovation_10']: '';
		$Innovation_11 = $_POST['Innovation_11']?$_POST['Innovation_11']: '';

		
		$Initiative_tenacity_1 = $_POST['Initiative_tenacity_1']?$_POST['Initiative_tenacity_1']: '';
		$Initiative_tenacity_2 = $_POST['Initiative_tenacity_2']?$_POST['Initiative_tenacity_2']: '';
		$Initiative_tenacity_3 = $_POST['Initiative_tenacity_3']?$_POST['Initiative_tenacity_3']: '';
		$Initiative_tenacity_4 = $_POST['Initiative_tenacity_4']?$_POST['Initiative_tenacity_4']: '';
		$Initiative_tenacity_5 = $_POST['Initiative_tenacity_5']?$_POST['Initiative_tenacity_5']: '';
		$Initiative_tenacity_6 = $_POST['Initiative_tenacity_6']?$_POST['Initiative_tenacity_6']: '';
		$Initiative_tenacity_7 = $_POST['Initiative_tenacity_7']?$_POST['Initiative_tenacity_7']: '';
        $Initiative_tenacity_8 = $_POST['Initiative_tenacity_8']?$_POST['Initiative_tenacity_8']: '';
		$Initiative_tenacity_9 = $_POST['Initiative_tenacity_9']?$_POST['Initiative_tenacity_9']: '';


		
		$Outcome_Orientation_1 = $_POST['Outcome_Orientation_1']?$_POST['Outcome_Orientation_1']: '';
		$Outcome_Orientation_2 = $_POST['Outcome_Orientation_2']?$_POST['Outcome_Orientation_2']: '';
		$Outcome_Orientation_3 = $_POST['Outcome_Orientation_3']?$_POST['Outcome_Orientation_3']: '';
		$Outcome_Orientation_4 = $_POST['Outcome_Orientation_4']?$_POST['Outcome_Orientation_4']: '';
		$Outcome_Orientation_5 = $_POST['Outcome_Orientation_5']?$_POST['Outcome_Orientation_5']: '';
		$Outcome_Orientation_6 = $_POST['Outcome_Orientation_6']?$_POST['Outcome_Orientation_6']: '';
		
		$Cultural_Agility_1 = $_POST['Cultural_Agility_1']?$_POST['Cultural_Agility_1']: '';
		$Cultural_Agility_2 = $_POST['Cultural_Agility_2']?$_POST['Cultural_Agility_2']: '';
		$Cultural_Agility_3 = $_POST['Cultural_Agility_3']?$_POST['Cultural_Agility_3']: '';
		$Cultural_Agility_4 = $_POST['Cultural_Agility_4']?$_POST['Cultural_Agility_4']: '';
		$Cultural_Agility_5 = $_POST['Cultural_Agility_5']?$_POST['Cultural_Agility_5']: '';
		$Cultural_Agility_6 = $_POST['Cultural_Agility_6']?$_POST['Cultural_Agility_6']: '';
		$Cultural_Agility_7 = $_POST['Cultural_Agility_7']?$_POST['Cultural_Agility_7']: '';
		
		$People_Leadership_1 = $_POST['People_Leadership_1']?$_POST['People_Leadership_1']: '';
		$People_Leadership_2 = $_POST['People_Leadership_2']?$_POST['People_Leadership_2']: '';
		$People_Leadership_3 = $_POST['People_Leadership_3']?$_POST['People_Leadership_3']: '';
		$People_Leadership_4 = $_POST['People_Leadership_4']?$_POST['People_Leadership_4']: '';
		$People_Leadership_5 = $_POST['People_Leadership_5']?$_POST['People_Leadership_5']: '';
		$People_Leadership_6 = $_POST['People_Leadership_6']?$_POST['People_Leadership_6']: '';
		$People_Leadership_7 = $_POST['People_Leadership_7']?$_POST['People_Leadership_7']: '';
		$People_Leadership_8 = $_POST['People_Leadership_8']?$_POST['People_Leadership_8']: '';
		$People_Leadership_9 = $_POST['People_Leadership_9']?$_POST['People_Leadership_9']: '';
		
		$Execution_Excellence_1 = $_POST['Execution_Excellence_1']?$_POST['Execution_Excellence_1']: '';
		$Execution_Excellence_2 = $_POST['Execution_Excellence_2']?$_POST['Execution_Excellence_2']: '';
		$Execution_Excellence_3 = $_POST['Execution_Excellence_3']?$_POST['Execution_Excellence_3']: '';
		$Execution_Excellence_4 = $_POST['Execution_Excellence_4']?$_POST['Execution_Excellence_4']: '';
		$Execution_Excellence_5 = $_POST['Execution_Excellence_5']?$_POST['Execution_Excellence_5']: '';
		$Execution_Excellence_6 = $_POST['Execution_Excellence_6']?$_POST['Execution_Excellence_6']: '';
		$Execution_Excellence_7 = $_POST['Execution_Excellence_7']?$_POST['Execution_Excellence_7']: '';
		
		$Commercial_Financial_Acumen_1 = $_POST['Commercial_Financial_Acumen_1']?$_POST['Commercial_Financial_Acumen_1']: '';
		$Commercial_Financial_Acumen_2 = $_POST['Commercial_Financial_Acumen_2']?$_POST['Commercial_Financial_Acumen_2']: '';
		$Commercial_Financial_Acumen_3 = $_POST['Commercial_Financial_Acumen_3']?$_POST['Commercial_Financial_Acumen_3']: '';
		$Commercial_Financial_Acumen_4 = $_POST['Commercial_Financial_Acumen_4']?$_POST['Commercial_Financial_Acumen_4']: '';
		$Commercial_Financial_Acumen_5 = $_POST['Commercial_Financial_Acumen_5']?$_POST['Commercial_Financial_Acumen_5']: '';
		$Commercial_Financial_Acumen_6 = $_POST['Commercial_Financial_Acumen_6']?$_POST['Commercial_Financial_Acumen_6']: '';
		$Commercial_Financial_Acumen_7 = $_POST['Commercial_Financial_Acumen_7']?$_POST['Commercial_Financial_Acumen_7']: '';
		$Commercial_Financial_Acumen_8 = $_POST['Commercial_Financial_Acumen_8']?$_POST['Commercial_Financial_Acumen_8']: '';
		
		$Sales_Brand_Promotion_1 = $_POST['Sales_Brand_Promotion_1']?$_POST['Sales_Brand_Promotion_1']: '';
		$Sales_Brand_Promotion_2 = $_POST['Sales_Brand_Promotion_2']?$_POST['Sales_Brand_Promotion_2']: '';
		$Sales_Brand_Promotion_3 = $_POST['Sales_Brand_Promotion_3']?$_POST['Sales_Brand_Promotion_3']: '';
		$Sales_Brand_Promotion_4 = $_POST['Sales_Brand_Promotion_4']?$_POST['Sales_Brand_Promotion_4']: '';
		$Sales_Brand_Promotion_5 = $_POST['Sales_Brand_Promotion_5']?$_POST['Sales_Brand_Promotion_5']: '';
		
		$Strategic_Alignment_1 = $_POST['Strategic_Alignment_1']?$_POST['Strategic_Alignment_1']: '';
		$Strategic_Alignment_2 = $_POST['Strategic_Alignment_2']?$_POST['Strategic_Alignment_2']: '';
		$Strategic_Alignment_3 = $_POST['Strategic_Alignment_3']?$_POST['Strategic_Alignment_3']: '';
		$Strategic_Alignment_4 = $_POST['Strategic_Alignment_4']?$_POST['Strategic_Alignment_4']: '';
		$Strategic_Alignment_5 = $_POST['Strategic_Alignment_5']?$_POST['Strategic_Alignment_5']: '';
		$Strategic_Alignment_6 = $_POST['Strategic_Alignment_6']?$_POST['Strategic_Alignment_6']: '';
	
		
		$Foresight_1 = $_POST['Foresight_1']?$_POST['Foresight_1']: '';
		$Foresight_2 = $_POST['Foresight_2']?$_POST['Foresight_2']: '';
		$Foresight_3 = $_POST['Foresight_3']?$_POST['Foresight_3']: '';
		$Foresight_4 = $_POST['Foresight_4']?$_POST['Foresight_4']: '';
		$Foresight_5 = $_POST['Foresight_5']?$_POST['Foresight_5']: '';
		$Foresight_6 = $_POST['Foresight_6']?$_POST['Foresight_6']: '';
		$Foresight_7 = $_POST['Foresight_7']?$_POST['Foresight_7']: '';
		
		$competencyID = getCompetencyAssessID($EmployID);
		
		if($competencyID)
		{
			$db = new Database();
			$sql = "UPDATE competency_assessment 
					SET Primary_Lead_ID = $Primary_Lead_ID
					, Communication_1 = '$Communication_1', Communication_2 = '$Communication_2', Communication_3 = '$Communication_3', Communication_4 = '$Communication_4', Communication_5 = '$Communication_5', Communication_6 = '$Communication_6', Communication_7 = '$Communication_7', Communication_8 = '$Communication_8', Communication_9 = '$Communication_9'
					, Customer_Orientation_1 = '$Customer_Orientation_1', Customer_Orientation_2 = '$Customer_Orientation_2', Customer_Orientation_3 = '$Customer_Orientation_3', Customer_Orientation_4 = '$Customer_Orientation_4', Customer_Orientation_5 = '$Customer_Orientation_5', Customer_Orientation_6 = '$Customer_Orientation_6', Customer_Orientation_7 = '$Customer_Orientation_7', Customer_Orientation_8 = '$Customer_Orientation_8', Customer_Orientation_9 = '$Customer_Orientation_9', Customer_Orientation_10 = '$Customer_Orientation_10' 
					, Integrity_Work_Ethics_1 = '$Integrity_Work_Ethics_1', Integrity_Work_Ethics_2 = '$Integrity_Work_Ethics_2', Integrity_Work_Ethics_3 = '$Integrity_Work_Ethics_3', Integrity_Work_Ethics_4 = '$Integrity_Work_Ethics_4', Integrity_Work_Ethics_5 = '$Integrity_Work_Ethics_5', Integrity_Work_Ethics_6 = '$Integrity_Work_Ethics_6', Integrity_Work_Ethics_7 = '$Integrity_Work_Ethics_7', Integrity_Work_Ethics_8 = '$Integrity_Work_Ethics_8'
					, Dependable_Accountable_1 = '$Dependable_Accountable_1', Dependable_Accountable_2 = '$Dependable_Accountable_2', Dependable_Accountable_3 = '$Dependable_Accountable_3', Dependable_Accountable_4 = '$Dependable_Accountable_4', Dependable_Accountable_5 = '$Dependable_Accountable_5', Dependable_Accountable_6 = '$Dependable_Accountable_6', Dependable_Accountable_7 = '$Dependable_Accountable_7', Dependable_Accountable_8 = '$Dependable_Accountable_8'
					, Team_Collaboration_1 = '$Team_Collaboration_1', Team_Collaboration_2 = '$Team_Collaboration_2', Team_Collaboration_3 = '$Team_Collaboration_3', Team_Collaboration_4 = '$Team_Collaboration_4', Team_Collaboration_5 = '$Team_Collaboration_5', Team_Collaboration_6 = '$Team_Collaboration_6', Team_Collaboration_7 = '$Team_Collaboration_7', Team_Collaboration_8 = '$Team_Collaboration_8',
				     Team_Collaboration_9 = '$Team_Collaboration_9', Team_Collaboration_10 = '$Team_Collaboration_10', Team_Collaboration_11 = '$Team_Collaboration_11', Team_Collaboration_12 = '$Team_Collaboration_12', Team_Collaboration_13 = '$Team_Collaboration_13'	
					, Influencing_Ability_1 = '$Influencing_Ability_1', Influencing_Ability_2 = '$Influencing_Ability_2', Influencing_Ability_3 = '$Influencing_Ability_3', Influencing_Ability_4= '$Influencing_Ability_4', Influencing_Ability_5 = '$Influencing_Ability_5', Influencing_Ability_6 = '$Influencing_Ability_6', Influencing_Ability_7 = '$Influencing_Ability_7',
					 Influencing_Ability_8 = '$Influencing_Ability_8', Influencing_Ability_9 = '$Influencing_Ability_9',
					  Influencing_Ability_10 = '$Influencing_Ability_10'

			    	, Coaching_Mentoring_1 = '$Coaching_Mentoring_1', Coaching_Mentoring_2 = '$Coaching_Mentoring_2', Coaching_Mentoring_3 = '$Coaching_Mentoring_3', Coaching_Mentoring_4 = '$Coaching_Mentoring_4', Coaching_Mentoring_5 = '$Coaching_Mentoring_5', Coaching_Mentoring_6 = '$Coaching_Mentoring_6'
			    	, planning_organization_1 = '$planning_organization_1',planning_organization_2 = '$planning_organization_2',planning_organization_3 = '$planning_organization_3',planning_organization_4 = '$planning_organization_4',planning_organization_5 = '$planning_organization_5',planning_organization_6 = '$planning_organization_6',planning_organization_7 = '$planning_organization_7',planning_organization_8 = '$planning_organization_8',planning_organization_9 = '$planning_organization_9',planning_organization_10 = '$planning_organization_10'
					, Innovation_1 = '$Innovation_1', Innovation_2 = '$Innovation_2', Innovation_3 = '$Innovation_3', Innovation_4 = '$Innovation_4', Innovation_5 = '$Innovation_5', Innovation_6 = '$Innovation_6', Innovation_7 = '$Innovation_7', Innovation_8 = '$Innovation_8', Innovation_9 = '$Innovation_9',Innovation_10 = '$Innovation_10', Innovation_11 = '$Innovation_11'
					, Initiative_tenacity_1 = '$Initiative_tenacity_1', Initiative_tenacity_2 = '$Initiative_tenacity_2', Initiative_tenacity_3 = '$Initiative_tenacity_3', Initiative_tenacity_4 = '$Initiative_tenacity_4', Initiative_tenacity_5 = '$Initiative_tenacity_5', Initiative_tenacity_6 = '$Initiative_tenacity_6', Initiative_tenacity_7 = '$Initiative_tenacity_7',Initiative_tenacity_8 = '$Initiative_tenacity_8',Initiative_tenacity_9 = '$Initiative_tenacity_9'
					, Outcome_Orientation_1 = '$Outcome_Orientation_1', Outcome_Orientation_2 = '$Outcome_Orientation_2', Outcome_Orientation_3 = '$Outcome_Orientation_3', Outcome_Orientation_4 = '$Outcome_Orientation_4', Outcome_Orientation_5 = '$Outcome_Orientation_5', Outcome_Orientation_6 = '$Outcome_Orientation_6'
					, Cultural_Agility_1 = '$Cultural_Agility_1', Cultural_Agility_2 = '$Cultural_Agility_2', Cultural_Agility_3 = '$Cultural_Agility_3', Cultural_Agility_4 = '$Cultural_Agility_4', Cultural_Agility_5 = '$Cultural_Agility_5', Cultural_Agility_6 = '$Cultural_Agility_6', Cultural_Agility_7 = '$Cultural_Agility_7'
					, People_Leadership_1 = '$People_Leadership_1', People_Leadership_2 = '$People_Leadership_2', People_Leadership_3 = '$People_Leadership_3', People_Leadership_4 = '$People_Leadership_4', People_Leadership_5 = '$People_Leadership_5', People_Leadership_6 = '$People_Leadership_6', People_Leadership_7 = '$People_Leadership_7', People_Leadership_8 = '$People_Leadership_8', People_Leadership_9 = '$People_Leadership_9'
					, Execution_Excellence_1 = '$Execution_Excellence_1', Execution_Excellence_2 = '$Execution_Excellence_2', Execution_Excellence_3 = '$Execution_Excellence_3', Execution_Excellence_4 = '$Execution_Excellence_4', Execution_Excellence_5 = '$Execution_Excellence_5', Execution_Excellence_6 = '$Execution_Excellence_6', Execution_Excellence_7 = '$Execution_Excellence_7'
					, Commercial_Financial_Acumen_1 = '$Commercial_Financial_Acumen_1', Commercial_Financial_Acumen_2 = '$Commercial_Financial_Acumen_2', Commercial_Financial_Acumen_3 = '$Commercial_Financial_Acumen_3', Commercial_Financial_Acumen_4 = '$Commercial_Financial_Acumen_4', Commercial_Financial_Acumen_5 = '$Commercial_Financial_Acumen_5', Commercial_Financial_Acumen_6 = '$Commercial_Financial_Acumen_6', Commercial_Financial_Acumen_7 = '$Commercial_Financial_Acumen_7', Commercial_Financial_Acumen_8 = '$Commercial_Financial_Acumen_8'
					, Sales_Brand_Promotion_1 = '$Sales_Brand_Promotion_1', Sales_Brand_Promotion_2 = '$Sales_Brand_Promotion_2', Sales_Brand_Promotion_3 = '$Sales_Brand_Promotion_3', Sales_Brand_Promotion_4 = '$Sales_Brand_Promotion_4', Sales_Brand_Promotion_5 = '$Sales_Brand_Promotion_5'
					, Strategic_Alignment_1 = '$Strategic_Alignment_1', Strategic_Alignment_2 = '$Strategic_Alignment_2', Strategic_Alignment_3 = '$Strategic_Alignment_3', Strategic_Alignment_4 = '$Strategic_Alignment_4', Strategic_Alignment_5 = '$Strategic_Alignment_5', Strategic_Alignment_6 = '$Strategic_Alignment_6'
					, Foresight_1 = '$Foresight_1', Foresight_2 = '$Foresight_2', Foresight_3 = '$Foresight_3', Foresight_4 = '$Foresight_4', Foresight_5 = '$Foresight_5', Foresight_6 = '$Foresight_6', Foresight_7 = '$Foresight_7'
					where EmployID = $EmployID";
			
			if($db->sql($sql))
			{
				header("Location: competency_assessment_Success.php?id=$EmployID");
				exit;
			}
			else 
			{
				header("Location: competency_assessment_Error.php?data=1");
				exit;
			}
		}
		else 
		{
			$db = new Database();
			$sql = "INSERT INTO competency_assessment
			 	   (EmployID,Primary_Lead_ID
			 	    , Communication_1, Communication_2, Communication_3, Communication_4, Communication_5, Communication_6, Communication_7, Communication_8, Communication_9
					, Customer_Orientation_1, Customer_Orientation_2, Customer_Orientation_3, Customer_Orientation_4, Customer_Orientation_5, Customer_Orientation_6, Customer_Orientation_7, Customer_Orientation_8, Customer_Orientation_9, Customer_Orientation_10
					, Integrity_Work_Ethics_1, Integrity_Work_Ethics_2, Integrity_Work_Ethics_3, Integrity_Work_Ethics_4, Integrity_Work_Ethics_5, Integrity_Work_Ethics_6, Integrity_Work_Ethics_7, Integrity_Work_Ethics_8
					, Dependable_Accountable_1, Dependable_Accountable_2, Dependable_Accountable_3, Dependable_Accountable_4, Dependable_Accountable_5, Dependable_Accountable_6, Dependable_Accountable_7, Dependable_Accountable_8
					, Team_Collaboration_1, Team_Collaboration_2, Team_Collaboration_3, Team_Collaboration_4, Team_Collaboration_5, Team_Collaboration_6, Team_Collaboration_7, Team_Collaboration_8,Team_Collaboration_9, Team_Collaboration_10, Team_Collaboration_11, Team_Collaboration_12, Team_Collaboration_13
					, Influencing_Ability_1, Influencing_Ability_2, Influencing_Ability_3, Influencing_Ability_4, Influencing_Ability_5, Influencing_Ability_6, Influencing_Ability_7, Influencing_Ability_8, Influencing_Ability_9, Influencing_Ability_10
					, Coaching_Mentoring_1, Coaching_Mentoring_2, Coaching_Mentoring_3, Coaching_Mentoring_4, Coaching_Mentoring_5, Coaching_Mentoring_6
                    , planning_organization_1, planning_organization_2, planning_organization_3, planning_organization_4, planning_organization_5,planning_organization_6, planning_organization_7, planning_organization_8, planning_organization_9, planning_organization_10
                    , Innovation_1, Innovation_2, Innovation_3, Innovation_4, Innovation_5, Innovation_6, Innovation_7, Innovation_8, Innovation_9, Innovation_10, Innovation_11
					, Initiative_tenacity_1, Initiative_tenacity_2, Initiative_tenacity_3, Initiative_tenacity_4, Initiative_tenacity_5, Initiative_tenacity_6, Initiative_tenacity_7,Initiative_tenacity_8,Initiative_tenacity_9 
					, Outcome_Orientation_1, Outcome_Orientation_2, Outcome_Orientation_3, Outcome_Orientation_4, Outcome_Orientation_5, Outcome_Orientation_6
					, Cultural_Agility_1, Cultural_Agility_2, Cultural_Agility_3, Cultural_Agility_4, Cultural_Agility_5, Cultural_Agility_6, Cultural_Agility_7
					, People_Leadership_1, People_Leadership_2, People_Leadership_3, People_Leadership_4, People_Leadership_5, People_Leadership_6, People_Leadership_7, People_Leadership_8, People_Leadership_9
					, Execution_Excellence_1, Execution_Excellence_2, Execution_Excellence_3, Execution_Excellence_4, Execution_Excellence_5, Execution_Excellence_6, Execution_Excellence_7
					, Commercial_Financial_Acumen_1, Commercial_Financial_Acumen_2, Commercial_Financial_Acumen_3, Commercial_Financial_Acumen_4, Commercial_Financial_Acumen_5, Commercial_Financial_Acumen_6, Commercial_Financial_Acumen_7, Commercial_Financial_Acumen_8
					, Sales_Brand_Promotion_1, Sales_Brand_Promotion_2, Sales_Brand_Promotion_3, Sales_Brand_Promotion_4, Sales_Brand_Promotion_5
					, Strategic_Alignment_1, Strategic_Alignment_2, Strategic_Alignment_3, Strategic_Alignment_4, Strategic_Alignment_5, Strategic_Alignment_6
					, Foresight_1, Foresight_2, Foresight_3, Foresight_4, Foresight_5, Foresight_6, Foresight_7
					) 
			 	   VALUES ($EmployID,$Primary_Lead_ID
			 	   , '$Communication_1', '$Communication_2', '$Communication_3', '$Communication_4', '$Communication_5', '$Communication_6', '$Communication_7', '$Communication_8', '$Communication_9'
					, '$Customer_Orientation_1', '$Customer_Orientation_2', '$Customer_Orientation_3', '$Customer_Orientation_4', '$Customer_Orientation_5', '$Customer_Orientation_6', '$Customer_Orientation_7', '$Customer_Orientation_8', '$Customer_Orientation_9', '$Customer_Orientation_10' 
					, '$Integrity_Work_Ethics_1', '$Integrity_Work_Ethics_2', '$Integrity_Work_Ethics_3', '$Integrity_Work_Ethics_4', '$Integrity_Work_Ethics_5', '$Integrity_Work_Ethics_6', '$Integrity_Work_Ethics_7', '$Integrity_Work_Ethics_8'
					, '$Dependable_Accountable_1', '$Dependable_Accountable_2', '$Dependable_Accountable_3', '$Dependable_Accountable_4', '$Dependable_Accountable_5', '$Dependable_Accountable_6', '$Dependable_Accountable_7', '$Dependable_Accountable_8'
					, '$Team_Collaboration_1', '$Team_Collaboration_2', '$Team_Collaboration_3', '$Team_Collaboration_4', '$Team_Collaboration_5', '$Team_Collaboration_6', '$Team_Collaboration_7', '$Team_Collaboration_8', '$Team_Collaboration_9', '$Team_Collaboration_10', '$Team_Collaboration_11', '$Team_Collaboration_12', '$Team_Collaboration_13'
					, '$Influencing_Ability_1', '$Influencing_Ability_2', '$Influencing_Ability_3', '$Influencing_Ability_4', '$Influencing_Ability_5', '$Influencing_Ability_6', '$Influencing_Ability_7', '$Influencing_Ability_8',  '$Influencing_Ability_9',  '$Influencing_Ability_10'
					, '$Coaching_Mentoring_1', '$Coaching_Mentoring_2', '$Coaching_Mentoring_3', '$Coaching_Mentoring_4', '$Coaching_Mentoring_5', '$Coaching_Mentoring_6'
					, '$planning_organization_1', '$planning_organization_2', '$planning_organization_3', '$planning_organization_4', '$planning_organization_5', '$planning_organization_6', '$planning_organization_7', '$planning_organization_8', '$planning_organization_9', '$planning_organization_10'
					, '$Innovation_1', '$Innovation_2', '$Innovation_3', '$Innovation_4', '$Innovation_5', '$Innovation_6', '$Innovation_7', '$Innovation_8', '$Innovation_9', '$Innovation_10', '$Innovation_11' 
					, '$Initiative_tenacity_1', '$Initiative_tenacity_2', '$Initiative_tenacity_3', '$Initiative_tenacity_4', '$Initiative_tenacity_5', '$Initiative_tenacity_6', '$Initiative_tenacity_7', '$Initiative_tenacity_8', '$Initiative_tenacity_9' 
					, '$Outcome_Orientation_1', '$Outcome_Orientation_2', '$Outcome_Orientation_3', '$Outcome_Orientation_4', '$Outcome_Orientation_5', '$Outcome_Orientation_6'
					, '$Cultural_Agility_1', '$Cultural_Agility_2', '$Cultural_Agility_3', '$Cultural_Agility_4', '$Cultural_Agility_5', '$Cultural_Agility_6', '$Cultural_Agility_7'
					, '$People_Leadership_1', '$People_Leadership_2', '$People_Leadership_3', '$People_Leadership_4', '$People_Leadership_5', '$People_Leadership_6', '$People_Leadership_7', '$People_Leadership_8', '$People_Leadership_9'
					, '$Execution_Excellence_1', '$Execution_Excellence_2', '$Execution_Excellence_3', '$Execution_Excellence_4', '$Execution_Excellence_5', '$Execution_Excellence_6', '$Execution_Excellence_7'
					, '$Commercial_Financial_Acumen_1', '$Commercial_Financial_Acumen_2', '$Commercial_Financial_Acumen_3', '$Commercial_Financial_Acumen_4', '$Commercial_Financial_Acumen_5', '$Commercial_Financial_Acumen_6', '$Commercial_Financial_Acumen_7', '$Commercial_Financial_Acumen_8'
					, '$Sales_Brand_Promotion_1', '$Sales_Brand_Promotion_2', '$Sales_Brand_Promotion_3', '$Sales_Brand_Promotion_4', '$Sales_Brand_Promotion_5'
					, '$Strategic_Alignment_1', '$Strategic_Alignment_2', '$Strategic_Alignment_3', '$Strategic_Alignment_4', '$Strategic_Alignment_5', '$Strategic_Alignment_6'
					, '$Foresight_1', '$Foresight_2', '$Foresight_3', '$Foresight_4', '$Foresight_5', '$Foresight_6', '$Foresight_7'
			 	   )";
			
			// echo $sql;exit;
			if($db->sql($sql))
			{
				header("Location: competency_assessment_Success.php?id=$EmployID");
				exit;
			}
			else 
			{
				header("Location: competency_assessment_Error.php?data=1");
				exit;
			}
		}
	}
	 
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Competency Assessment</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/jquery.validate.css" />
    <script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
    <script src="../js/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <script type="text/javascript" src="../js/script_edit.js"></script>
	
	<script type="text/javascript">
	 function validateForm()
	 {
		$("form :input").each(function(index, elm){			 
			 var element = document.getElementsByName(elm.name);
			  if(element.length==4)
			  {
				  if(element[0].checked==false && element[1].checked==false && element[2].checked==false && element[3].checked==false)
				  {
				  if(element[0].offsetWidth>0 && element[1].offsetWidth>0 && element[2].offsetWidth>0 && element[3].offsetWidth>0)
				  {
					  $("#saveForm").before("<font color='red'>All fields are mandatory</font>");
					  //console.log("error");
					  return false;
				  }
				  else {
					  console.log("succ"+elm.name);
					   return true;
				  }
				  }
				  else {
					  console.log("succ"+elm.name);
					   return true;
				  }
			  }
		});
	 }
		function displayss(obj) 
		{
			 
			var empidselected = obj.options[obj.selectedIndex].value;
			$.ajax({
				
		        url: "competency_assement_emp_grade.php",
		        type: "post",
		        sync: true,
		        data: {empid: empidselected},
			    success: function(grade) {
				    employdetails(empidselected, grade);
		        	if(grade == 1)
		    		{
		        		document.getElementById('communication_block').style.display = 'block';
						document.getElementById('customer_orientation_block').style.display = 'block';
						document.getElementById('integrity_work_ethics_block').style.display = 'block';
                        document.getElementById('dependability_accountability_block').style.display = 'block';
						document.getElementById('team_collaboration_block').style.display = 'block';
						document.getElementById('innovation_creativity_block').style.display = 'block';
                        document.getElementById('interpersonal_skills_block').style.display = 'block';
						document.getElementById('coaching_mentor_block').style.display = 'none';
						document.getElementById('resilience_tenacity_block').style.display = 'none';
						document.getElementById('planning_organization_block').style.display = 'none';
						document.getElementById('cultural_adaptability_block').style.display = 'none';
						document.getElementById('people_leadership_block').style.display = 'none';
						document.getElementById('execution_excellence_block').style.display = 'none';
						document.getElementById('strategic_alignment_block').style.display = 'none';
						document.getElementById('foresight_block').style.display = 'none';

		        		//document.getElementById('grade2').style.display = 'none';
		        		//document.getElementById('grade3').style.display = 'none';
						//document.getElementById('grade4').style.display = 'none';
		        		document.getElementById('saveForm').style.display = 'block';

		        		$("#innovation_grade_one_intelectual").show();
		        		$("#innovation_grade_one_efficiently").show();
		        		$("#innovation_grade_one_techologies").show();
					      $("#interpersonal_skills_amicably").show();
					      $("#interpersonal_skills_authority").show(); 
                        $("#innovation_grade_two_ingenious").hide();
                        $("#innovation_grade_two_pinpoint").hide();
                        $("#innovation_grade_two_scrums").hide();
                        $("#innovation_grade_two_ideas").hide();
                        $("#innovation_grade_two_challenges").hide();
                        $("#innovation_grade_two_generates").hide();
                        $("#innovation_grade_two_leads").hide();
                        $("#innovation_grade_two_hesitate").hide();




		    		    $("#depend_account_grade_two").hide();
		    		    $("#depend_account_grade_two_extra").show();
		    		    $("#cust_ori_grade_two").hide();
						$("#cust_ori_grade_two_opp").hide();
						$("#integrity_work_ethics_grade_two").hide();
		    		    $("#team_collaboration_grade_two").hide();
						$("#team_collaboration_grade_two_will").hide();
						
						$("#coaching_professional").hide();
						$("#coaching_mentor").hide();
						$("#comm_6").hide();
						$("#comm_7").hide();
						$("#comm_8").hide();
						$("#comm_9").hide();
                       
                        $("#cust_ori_grade_three_id").hide();
						$("#integrity_work_ethics_grade_three").hide();
						$("#integrity_work_ethics_grade_three_breach").hide();
						$("#depend_account_grade_two_schedule").hide();
						$("#team_collaboration_grade_three_demons").hide();
						$("#team_collaboration_grade_three_agenda").hide();
						$("#team_collaboration_grade_three_pressure").hide();
						$("#team_collaboration_grade_three_success").hide();
						$("#team_collaboration_grade_three_mission").hide();
						$("#team_collaboration_grade_three_dysf").hide();
						
                       
                       
                        

                        $("#resilience_tenacity_obstacles").hide();
                        $("#resilience_tenacity_difficult").hide();
                        $("#resilience_tenacity_victim").hide();
                        $("#planning_organization_self").hide();
		        		$("#planning_organization_handle").hide();
		        		$("#planning_organization_errors").hide();
		        		$("#planning_organization_colleague").hide();
		        		$("#cultural_adaptability_situation").hide();
		        		$("#execution_excellence_holistic").hide();
		        		$("#cust_ori_grade_two_custo").hide();
                        $("#cust_ori_grade_two_time").hide();
                        $("#people_leadership_block").hide();
						$("#interpersonal_skills_offers").hide();
						$("#interpersonal_skills_positively").hide();
						$("#interpersonal_skills_attitude").hide();
                        $("#interpersonal_skills_feedback").hide();
                        $("#execution_excellence_quid_quo").hide();

					}
				    if(grade == 2)
		    		{
				    	//document.getElementById('grade1').style.display = 'none';
		        		document.getElementById('communication_block').style.display = 'block';
						document.getElementById('customer_orientation_block').style.display = 'block';
						document.getElementById('integrity_work_ethics_block').style.display = 'block';
						document.getElementById('dependability_accountability_block').style.display = 'block';
						document.getElementById('team_collaboration_block').style.display = 'block';
						document.getElementById('innovation_creativity_block').style.display = 'block';
						document.getElementById('interpersonal_skills_block').style.display = 'block';
						document.getElementById('coaching_mentor_block').style.display = 'block';
						document.getElementById('resilience_tenacity_block').style.display = 'block';
						document.getElementById('planning_organization_block').style.display = 'block';
						document.getElementById('cultural_adaptability_block').style.display = 'block';
						document.getElementById('people_leadership_block').style.display = 'block';
						document.getElementById('execution_excellence_block').style.display = 'block';
						document.getElementById('strategic_alignment_block').style.display = 'none';
						document.getElementById('foresight_block').style.display = 'none';
						//document.getElementById('comm_6').style.display = 'block';
						//document.getElementById('comm_7').style.display = 'block';
						//document.getElementById('comm_8').style.display = 'none';
						//document.getElementById('comm_9').style.display = 'none';
		        		//document.getElementById('grade3').style.display = 'none';
						//document.getElementById('grade4').style.display = 'none';
		        		document.getElementById('saveForm').style.display = 'block';
						
						$("#comm_6").show();
						$("#comm_7").show();
						$("#cust_ori_grade_two").show();
						$("#cust_ori_grade_two_opp").show();
						$("#integrity_work_ethics_grade_two").show();
						$("#depend_account_grade_two").show();
						$("#depend_account_grade_two_extra").show();
						$("#team_collaboration_grade_two").show();
						$("#team_collaboration_grade_two_will").show();
						$("#innovation_grade_two_ingenious").show();
						$("#innovation_grade_two_pinpoint").show();
						$("#innovation_grade_two_scrums").show();
						$("#innovation_grade_two_ideas").show();
						$("#innovation_grade_two_challenges").show();
						$("#interpersonal_skills_offers").show();
						$("#interpersonal_skills_positively").show();
						$("#innovation_grade_two_generates").hide();
						 $("#innovation_grade_two_leads").hide();
						  $("#innovation_grade_two_hesitate").hide();

						
						
                        $("#innovation_grade_one_intelectual").hide();
		        		$("#innovation_grade_one_efficiently").hide();
		        		$("#innovation_grade_one_techologies").hide();
		        		$("#interpersonal_skills_amicably").hide(); 
		        		$("#interpersonal_skills_authority").hide();
		        		$("#interpersonal_skills_attitude").hide();
		        		$("#interpersonal_skills_feedback").hide();
 
						$("#coaching_professional").hide();
						$("#coaching_mentor").hide();
						
						$("#comm_8").hide();
						$("#comm_9").hide();
						
						$("#comm_9").hide();
						$("#comm_9").hide();
						$("#cust_ori_grade_three_id").hide();
						$("#integrity_work_ethics_grade_three").hide();
						$("#integrity_work_ethics_grade_three_breach").hide();
						 $("#depend_account_grade_two_schedule").hide();
						 $("#team_collaboration_grade_three_demons").hide();
						 $("#team_collaboration_grade_three_agenda").hide();
						 $("#team_collaboration_grade_three_pressure").hide();
						 $("#team_collaboration_grade_three_success").hide();
						 $("#team_collaboration_grade_three_mission").hide();
						  $("#team_collaboration_grade_three_dysf").hide();
						  $("#execution_excellence_quid_quo").hide();
						  
						

                      

                        
                       

                        $("#resilience_tenacity_obstacles").hide();
                        $("#resilience_tenacity_difficult").hide();
                        $("#resilience_tenacity_victim").hide();
                        $("#planning_organization_self").hide();
		        		$("#planning_organization_handle").hide();
		        		$("#planning_organization_errors").hide();
		        		$("#planning_organization_colleague").hide();
		        		$("#cultural_adaptability_situation").hide();
		        		$("#execution_excellence_holistic").hide();

		        		$("#cust_ori_grade_two_custo").hide();
                        $("#cust_ori_grade_two_time").hide();
                        $("#people_leadership_block_leverage").hide();

		    		}
				    if(grade >= 3 && grade < 4)
		    		{
				    	//document.getElementById('grade1').style.display = 'none';
		        		//document.getElementById('grade2').style.display = 'none';
		        		document.getElementById('communication_block').style.display = 'block';
						document.getElementById('customer_orientation_block').style.display = 'block';
						document.getElementById('integrity_work_ethics_block').style.display = 'block';
						document.getElementById('dependability_accountability_block').style.display = 'block';
						document.getElementById('team_collaboration_block').style.display = 'block';
						document.getElementById('innovation_creativity_block').style.display = 'block';
						document.getElementById('interpersonal_skills_block').style.display = 'block';
						document.getElementById('coaching_mentor_block').style.display = 'block';
						document.getElementById('resilience_tenacity_block').style.display = 'block';
						document.getElementById('planning_organization_block').style.display = 'block';
						document.getElementById('cultural_adaptability_block').style.display = 'block';
						document.getElementById('people_leadership_block').style.display = 'block';
						document.getElementById('execution_excellence_block').style.display = 'block';
						document.getElementById('strategic_alignment_block').style.display = 'block';
						document.getElementById('foresight_block').style.display = 'none';
						
						//document.getElementById('comm_7').style.display = 'block';
						//document.getElementById('comm_8').style.display = 'block';
						//document.getElementById('comm_9').style.display = 'block';
						//document.getElementById('grade4').style.display = 'none';
		        		document.getElementById('saveForm').style.display = 'block';

		        		$("#comm_6").show();
						$("#comm_7").show();
						$("#comm_8").show();
						$("#comm_9").show();
						$("#cust_ori_grade_three").show();
						$("#innovation_grade_two_ingenious").show();

		        		$("#integrity_work_ethics_grade_three").show();
		        		$("#integrity_work_ethics_grade_three_breach").show();

                        $("#depend_account_grade_two_schedule").show();

                        $("#team_collaboration_grade_three_demons").show();
                        $("#team_collaboration_grade_three_agenda").show();
                        $("#team_collaboration_grade_three_pressure").show();
                        $("#team_collaboration_grade_three_success").show();
                        $("#team_collaboration_grade_three_mission").show();
                        $("#team_collaboration_grade_three_dysf").show();
                        $("#innovation_grade_two_generates").show();
                         $("#innovation_grade_two_leads").show();
                          $("#innovation_grade_two_hesitate").show();
                          $("#interpersonal_skills_offers").show();
                          $("#interpersonal_skills_positively").show();
                          $("#coaching_professional").show();
                          $("#coaching_mentor").show();


                      

                       
                        
                        

                      
		        		$("#resilience_tenacity_obstacles").show();
		        		$("#resilience_tenacity_difficult").show();
		        		$("#resilience_tenacity_victim").show();
		        		
		        		$("#planning_organization_self").show();
		        		$("#planning_organization_handle").show();
		        		$("#planning_organization_errors").show();
		        		$("#planning_organization_colleague").show();

                        $("#cultural_adaptability_situation").show();

                        $("#execution_excellence_holistic").show();

                        $("#cust_ori_grade_two_custo").show();
                        $("#cust_ori_grade_two_time").show();

                        $("#innovation_grade_two_ideas").show();
                        $("#innovation_grade_two_challenges").show();

                        
                         $("#innovation_grade_one_intelectual").hide();
		        		$("#innovation_grade_one_efficiently").hide();
		        		$("#innovation_grade_one_techologies").hide();
		        		$("#interpersonal_skills_amicably").hide();
		        		$("#interpersonal_skills_authority").hide();  

		        		$("#depend_account_grade_two_extra").hide();
		        		$("#team_collaboration_grade_three").hide();
		        		$("#team_collaboration_grade_three_contributes").hide();
                        $("#team_collaboration_grade_three_focus").hide();
                        $("#team_collaboration_grade_three_follow").hide();
                        $("#team_collaboration_grade_two").hide();
                        $("#interpersonal_skills_attitude").show();
                        $("#interpersonal_skills_feedback").show();


                       

                        

                        $("#resilience_tenacity_treat").hide();
						$("#resilience_tenacity_group").hide();

						$("#planning_organization_engages").hide();
						$("#planning_organization_sys").hide();

                        $("#cultural_adaptability_culture").hide();
 						$("#cultural_adaptability_caste").hide();

                        $("#execution_excellence_quid").show();
                        $("#cust_ori_grade_two_display").hide();
                        $("#cust_ori_grade_two_opp").hide();
                        $("#people_leadership_pro_quo").hide();
                        $("#innovation_grade_two_pinpoint").hide();
                        $("#innovation_grade_two_scrums").hide();
                        $("#execution_excellence_quid_quo").hide();
                       
                  		//$("#grade1").remove();
						//$("#grade2").remove();
						//$("#grade4").remove();
		    		}
					if(grade >= 4)
		    		{
				    	//document.getElementById('grade1').style.display = 'none';
		        		//document.getElementById('grade2').style.display = 'none';
		        		//document.getElementById('grade3').style.display = 'none';
						document.getElementById('communication_block').style.display = 'block';
						document.getElementById('customer_orientation_block').style.display = 'block';
						document.getElementById('integrity_work_ethics_block').style.display = 'block';
						document.getElementById('dependability_accountability_block').style.display = 'block';
						document.getElementById('team_collaboration_block').style.display = 'block';
						document.getElementById('innovation_creativity_block').style.display = 'block';
						document.getElementById('interpersonal_skills_block').style.display = 'block';
						document.getElementById('coaching_mentor_block').style.display = 'block';
						document.getElementById('resilience_tenacity_block').style.display = 'block';
						document.getElementById('planning_organization_block').style.display = 'block';
						document.getElementById('cultural_adaptability_block').style.display = 'block';
						document.getElementById('people_leadership_block').style.display = 'block';
						document.getElementById('execution_excellence_block').style.display = 'block';
						document.getElementById('strategic_alignment_block').style.display = 'block';
						document.getElementById('foresight_block').style.display = 'block';
						//document.getElementById('comm_7').style.display = 'block';
						//document.getElementById('comm_8').style.display = 'block';
						//document.getElementById('comm_9').style.display = 'block';
						//document.getElementById('grade4').style.display = 'none';
		        		document.getElementById('saveForm').style.display = 'block';
						$("#comm_6").show();
						$("#comm_7").show();
						$("#comm_8").show();
						$("#comm_9").show();
						$("#cultural_adaptability_situation").show();
						$("#people_leadership_block_leverage").show();
						$("#innovation_grade_two_ingenious").show();
						$("#innovation_grade_two_ideas").show();
						$("#innovation_grade_two_challenges").show();
						 $("#innovation_grade_two_generates").show();
						  $("#innovation_grade_two_leads").show();
						   $("#innovation_grade_two_hesitate").show();
						   $("#interpersonal_skills_offers").show();
						   $("#interpersonal_skills_positively").show();
						   $("#interpersonal_skills_attitude").show();
						   $("#interpersonal_skills_feedback").show();
						    $("#coaching_professional").show();
						     $("#coaching_mentor").show();

						  
						  

						   $("#interpersonal_skills_authority").hide(); 

						

						$("#planning_organization_engages").hide();
                        $("#planning_organization_sys").hide();
                         $("#cultural_adaptability_culture").hide();
                          $("#cultural_adaptability_caste").hide();
                          $("#execution_excellence_quid").show();
                          $("#cust_ori_grade_two_display").hide();
                           $("#cust_ori_grade_two_opp").hide();
                           $("#depend_account_grade_two_extra").hide();
                           $("#team_collaboration_grade_three").hide();

                           $("#team_collaboration_grade_three_focus").hide();
                           $("#team_collaboration_grade_three_follow").hide();
                           $("#team_collaboration_grade_two").hide();
                           $("#team_collaboration_grade_three_contributes").hide();
                           $("#interpersonal_skills_amicably").hide(); 
                        
						
						   $("#resilience_tenacity_treat").hide();
						   $("#resilience_tenacity_group").hide();
						   $("#people_leadership_pro_quo").hide();

						    $("#innovation_grade_one_intelectual").hide();
		        		$("#innovation_grade_one_efficiently").hide();
		        		$("#innovation_grade_one_techologies").hide();
		        		$("#innovation_grade_two_pinpoint").hide();
		        		$("#innovation_grade_two_scrums").hide();
		        		$("#execution_excellence_quid_quo").hide();
						  




                        



						//$("#grade1").remove();
						//$("#grade2").remove();
						//$("#grade3").remove();
		    		}
			    }
		   });
			return status;
		}
		
	</script>
<style>

</style>
</head>
<body onload= displayss(document.getElementById("EmployID"));>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
	<div class="logout"><a href="../login/logout.php?action=logout">Logout</a></div>
</div>
<div id="container">
<!--<h1>Survey Form</h1>-->
<form class="theorem" action="competency_assessment.php" method="POST" id="competencyAssessmentForm">
<table class="formTable">
	<tr>
		<td colspan="2">
			<h1>Competency Assessment</h1>
		</td>
	</tr>
	<tr>
		<td>
			<table class="formTable" style="width:400px !important;">
				<tr>
					<td>
						<label class="desc">Employee.<font color='red'>*</font></label>
						
						<select id="EmployID" tabindex="1" name="EmployID" onchange="displayss(this);">
							<?php echo $Employees;?>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<h1>Organization Wide Competency Mapping</h1>
		</td>
	</tr>
	
	<tr>
		<td>						
			<table class="formTable" id="communication_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Communication </h1>
						<p class="assessdesc">
							Communication: The act and ability to convey information through the exchange of thoughts, messages, or information; as by speech, visuals, signals, writing, or behavior in an articulate manner. It is the meaningful exchange of information between and among individuals.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee express his/her ideas clearly and seeks clarification, without hesitation whenever necessary.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_1" id="Communication_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_1" id="Communication_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_1" id="Communication_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_1" id="Communication_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee exhibits an enthusiastic and pleasant body language while interacting with peers, supervisors and clients.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Communication_2" id="Communication_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_2" id="Communication_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_2" id="Communication_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_2" id="Communication_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee drafts emails and converses in grammatically correct English.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_3" id="Communication_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_3" id="Communication_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_3" id="Communication_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_3" id="Communication_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee exhibits confidence and clarity while speaking.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Communication_4" id="Communication_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_4" id="Communication_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_4" id="Communication_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_4" id="Communication_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee listens to the questions actively, comprehends and answers to the context in a specific and accurate manner ('Right Information' to the 'Right Person' at the 'Right Time' in the 'Right Way").<font color='red'>*</font></label>							
					</td>
					<td>
						<input type="radio" name="Communication_5" id="Communication_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_5" id="Communication_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_5" id="Communication_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_5" id="Communication_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="comm_6">
					<td>
						<label class="desc">Employee uses data and facts in a clear and constructive manner, to support arguments and gain agreement.<font color='red'>*</font></label>							
					</td>
					<td>
					   <input type="radio" name="Communication_6" id="Communication_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_6" id="Communication_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_6" id="Communication_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_6" id="Communication_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="comm_7">
					<td>
						<label class="desc">Employee is able to tweek his/her communication style according to the person or situation he/she is in.<font color='red'>*</font></label>							
					</td>
					<td>
						<input type="radio" name="Communication_7" id="Communication_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_7" id="Communication_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_7" id="Communication_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_7" id="Communication_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="comm_8">
					<td>
						<label class="desc">Employee drives a culture of two-way communication that facilitates 'win-win' opportunities.<font color='red'>*</font></label>							
					</td>
					<td>
					   <input type="radio" name="Communication_8" id="Communication_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_8" id="Communication_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_8" id="Communication_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_8" id="Communication_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="comm_9">
					<td>
						<label class="desc">Employee focuses on the positives even when situations are unfavourable and uncertain. He/She spreads the good word about the other unlimited possibilities and oppurtunities among team members and reportees.<font color='red'>*</font></label>							
					</td>
					<td>
					    <input type="radio" name="Communication_9" id="Communication_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="Communication_9" id="Communication_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Communication_9" id="Communication_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="Communication_9" id="Communication_9_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>				
	
		<tr>
		<td>						
			<table class="formTable" id="customer_orientation_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Customer Orientation </h1>
						<p class="assessdesc">
							Customer Orientation is an aligned organization-wide approach to customer satisfaction and service, leading to customer loyalty and advocacy. The result is sustainable profitability. In a Customer Focused organization, Leadership, Processes and People are customer-aligned and aid in every action being shaped by a relentless commitment to meeting and exceeding customer expectations regarding product and service quality. Every employee understands what he/she must do in order to maintain and add value to every relationship with both the paying customer and those within the organization that rely on them for the work they do.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee comprehends the customer’s requests/requirement accurately and ensures appropriate action is taken.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee consistently meets the client's TAT.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="cust_ori_grade_two">
					<td>
						<label class="desc">Employee probes the supervisor or client whenever he/she has doubts about the assigned project/process or task.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee uses courteous/polite phrases and vocabulary, while communicating with clients even when they are being difficult.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Champion" value="Champion">Champion
					</td>
				</tr>

                <tr class="rowodd" id="cust_ori_grade_three_id">
					<td>
						<label class="desc">Employee identifies and acts on opportunities to enhance customer service delivery systems.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Champion" value="Champion">Champion
					</td>
				</tr>


				<tr class="roweven">
					<td>
						<label class="desc">Employee keeps clients up-to-date about the progress/delays concerning the service/task/process on hand.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="cust_ori_grade_two_display">
					<td>
						<label class="desc">Employee displays a 'Client First Attitude'.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Champion" value="Champion">Champion
					</td>
				</tr>	
				<tr class="roweven" id="cust_ori_grade_two_opp">
					<td>
						<label class="desc">Employee identifies and acts on opportunities to enhance customer service.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="cust_ori_grade_two_custo">
					<td>
						<label class="desc">Employee creates mutually supportive and loyal relationships with all major clients and customers.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Champion" value="Champion">Champion
					</td>
				</tr>	
				<tr class="roweven" id="cust_ori_grade_two_time">
					<td>
						<label class="desc">Employee understands the levels of service that is valued by customers and ensures that these are the priority at all times.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Beginners" value="Beginner">Beginner
						<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Advanced" value="Advanced">Advanced
						<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Champion" value="Champion">Champion
					</td>
				</tr>
				
			</table>	
		</td>
	</tr>
	
	<tr>
		<td>						
			<table class="formTable" id="integrity_work_ethics_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Integrity and Work Ethics</h1>
						<p class="assessdesc">
							Integrity and Work Ethics involves; representing oneself truthfully at all times, conducting oneself ethically and professionally, acknowledging the work of others, and being accountable for one’s actions. It fosters trust between employees and employers, something that is absolutely essential in order to realize the full potential of employees work term. Acts consistently with corporate values, code of conduct and standards of integrity.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee adheres to the organization's processes, policies and work ethics.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is self-disciplined, pushing self to complete work/tasks without supervision.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee does not manipulate other employees or the process for his/her advantage.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee does not indulge in spreading negativity about the organization among peers/co-workers.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee maintains honesty and confidentiality, regardless of pressure.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="integrity_work_ethics_grade_two">
					<td>
						<label class="desc">Employee admits mistakes in spite of the potential for negative consequences.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="integrity_work_ethics_grade_three">
					<td>
						<label class="desc">Employee does not indulge in favouritism.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Champion" value="Champion">Champion
					</td>
				</tr>
					
					<tr class="roweven" id="integrity_work_ethics_grade_three_breach">
					<td>
						<label class="desc">Employee does not breach the policies of the client and establishes the same for the team.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>

	<tr>
		<td>						
			<table class="formTable" id="dependability_accountability_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Dependability and Accountability</h1>
						<p class="assessdesc">
							Dependability means; reliable, steady & trustworthy. An employee with this ability can carry out tasks and can always be counted on, to own an action or task in full. Being accountable is an attribute which demonstrates owning up one’s actions and being able to take responsibility for it. This attribute couples up as a valuable competency for an organization & its people to foster a healthy environment & culture to propel growth. Employee consistently delivers on commitments, takes full responsibility for the work and is accountable for all his/her decisions.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee accepts personal responsibility, for the assigned task and does not shift the blame to others.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee plans his/her holidays without causing inconvenience to the team or client.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="depend_account_grade_two">
					<td>
						<label class="desc">Employee is relied upon by client/supervisiors/co-workers/and team as a source of valid data and process knowledge on whom they can confidently depend upon for assistance and other work related requirements.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr class="roweven" id="depend_account_grade_two_extra">
					<td>
						<label class="desc">Employee works extra hours if that’s what it takes to get the job done right.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee holds self-accountable for meeting objectives, honouring promises and keeping commitments.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee takes up all the assigned projects to a logical conclusion.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Champion" value="Champion">Champion
					</td>
				</tr>	
				<!--<tr class="rowodd">
					<td>
						<label class="desc">Employee does not indulge in spreading negativity about the organization or coworkers among other peers/co-workers.<font color='red'>*</font></label>
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Champion" value="Champion">Champion
					</td>
				</tr>-->
				<tr class="rowodd" id="depend_account_grade_two_schedule">
					<td>
						<label class="desc">Employee maintains consistent and predictable task-time schedule.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td>						
			<table class="formTable" id="team_collaboration_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Team Collaboration</h1>
						<p class="assessdesc">
							Team Collaboration is working with each other to complete a task and to achieve shared goals. It is a recursive process wherein two or more people or organizations work together to realize shared goals, this is more than the intersection of common goals seen in co-operative ventures, but a deep, collective determination to reach an identical objective — for example, an endeavor that is creative in nature—by sharing knowledge, learning and building consensus. In particular, teams that work collaboratively can obtain greater resources, recognition and reward when facing competition for finite resources.
						</p>
					</td>
				</tr>
				<tr class="rowodd" id="team_collaboration_grade_three">
					<td>
						<label class="desc">Employee actively participates in all kinds of team activities.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="team_collaboration_grade_three_demons">
					<td>
						<label class="desc">Employee demonstrates the ability to be effective in drawing out the opinions and ideas of team members.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="team_collaboration_grade_three_agenda">
					<td>
						<label class="desc">Employee puts team’s agenda ahead of personal needs.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="team_collaboration_grade_three_contributes">
					<td>
						<label class="desc">Employee contributes willingly towards, the accomplishment of own and team goals doing his/her share of the work.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee fosters friendly and cooperative relationships among team members.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="team_collaboration_grade_three_pressure">
					<td>
						<label class="desc">Employee takes specific steps to keep morale and levels of performance high during intense work pressure.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Champion" value="Champion">Champion
					</td>
					</tr>
					<tr class="roweven" id="team_collaboration_grade_three_success">
					<td>
						<label class="desc">Employee gives recognition and credit to people who have contributed to the team's success.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="team_collaboration_grade_three_mission">
					<td>
						<label class="desc">Employee establishes goals for the team that are aligned to the organization’s strategy and mission.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="team_collaboration_grade_three_dysf">
					<td>
						<label class="desc">Employee optimistically resolves dysfunctional conflict (if any) in the team and ensures success.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_9" id="Team_Collaboration_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_9" id="Team_Collaboration_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_9" id="Team_Collaboration_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_9" id="Team_Collaboration_9_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="team_collaboration_grade_three_focus">
					<td>
						<label class="desc">Employee focuses on the strengths of working in a team, rather than creating islands within the team.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_10" id="Team_Collaboration_10_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_10" id="Team_Collaboration_10_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_10" id="Team_Collaboration_10_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_10" id="Team_Collaboration_10_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="team_collaboration_grade_three_follow">
					<td>
						<label class="desc">Employee follows through on commitments made to other team members.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_11" id="Team_Collaboration_11_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_11" id="Team_Collaboration_11_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_11" id="Team_Collaboration_11_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_11" id="Team_Collaboration_11_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="team_collaboration_grade_two">
					<td>
						<label class="desc">Employee offers to resolve interpersonal/work related issues whenever necessary.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_12" id="Team_Collaboration_12_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_12" id="Team_Collaboration_12_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_12" id="Team_Collaboration_12_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_12" id="Team_Collaboration_12_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="team_collaboration_grade_two_will">
					<td>
						<label class="desc">Employee willingly shares knowledge with team members for the team's betterment.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Team_Collaboration_13" id="Team_Collaboration_13_Beginners" value="Beginner">Beginner
						<input type="radio" name="Team_Collaboration_13" id="Team_Collaboration_13_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Team_Collaboration_13" id="Team_Collaboration_13_Advanced" value="Advanced">Advanced
						<input type="radio" name="Team_Collaboration_13" id="Team_Collaboration_13_Champion" value="Champion">Champion
					</td>
				</tr>	
			</table>	
		</td>
	</tr>
	<tr>
		<td>						
			<table class="formTable" id="innovation_creativity_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Innovation & Creativity</h1>
						<p class="assessdesc">
							Innovation and Creativity: The process of translating an idea or invention into a tangible outcome, thus creating value to the organization. Innovation and Creativity involve deliberate application of information, imagination & initiative in deriving greater or differential outcome. 
						</p>
					</td>
				</tr>
				<tr class="rowodd" id="innovation_grade_one_intelectual">
					<td>
						<label class="desc">Employee is intellectually curious to know why things are done 'The Way They Are' and also he/she has proactive suggestions and thoughts about  "How to do it better".<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_1" id="Innovation_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_1" id="Innovation_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_1" id="Innovation_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_1" id="Innovation_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="innovation_grade_one_efficiently">
					<td>
						<label class="desc">Employee looks for ingenious ways to complete tasks efficiently.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_2" id="Innovation_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_2" id="Innovation_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_2" id="Innovation_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_2" id="Innovation_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="innovation_grade_one_techologies">
					<td>
						<label class="desc">Employee is enthusiastic to learn about how the process and technologies work, asks questions when he/she does not understand something.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_3" id="Innovation_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_3" id="Innovation_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_3" id="Innovation_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_3" id="Innovation_3_Champion" value="Champion">Champion
					</td>
				</tr>
                <tr class="rowodd" id="innovation_grade_two_ingenious">
					<td>
						<label class="desc">Employee solves problems in an unique or ingenious way.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_4" id="Innovation_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_4" id="Innovation_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_4" id="Innovation_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_4" id="Innovation_4_Champion" value="Champion">Champion
					</td>
				</tr>

                 <tr class="roweven" id="innovation_grade_two_generates">
					<td>
						<label class="desc">Employee generates ideas that create breakthrough opportunities and changes, which are just not extensions of the past initiatives.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_5" id="Innovation_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_5" id="Innovation_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_5" id="Innovation_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_5" id="Innovation_5_Champion" value="Champion">Champion
					</td>
				</tr>



				<tr class="roweven" id="innovation_grade_two_pinpoint">
					<td>
						<label class="desc">Employee can pinpoint the actual cause and dynamics of underlying problems.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_6" id="Innovation_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_6" id="Innovation_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_6" id="Innovation_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_6" id="Innovation_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="innovation_grade_two_scrums">
					<td>
						<label class="desc">Employee suggests viable new ideas during scrums and meetings.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_7" id="Innovation_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_7" id="Innovation_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_7" id="Innovation_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_7" id="Innovation_7_Champion" value="Champion">Champion
					</td>
				</tr>
                <tr class="roweven" id="innovation_grade_two_ideas">
					<td>
						<label class="desc">Employee listens to suggestions and ideas given by others and is ready to try them out.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_8" id="Innovation_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_8" id="Innovation_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_8" id="Innovation_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_8" id="Innovation_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="innovation_grade_two_challenges">
					<td>
						<label class="desc">Employee challenges the status-quo by continuously reviewing work processes.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_9" id="Innovation_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_9" id="Innovation_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_9" id="Innovation_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_9" id="Innovation_9_Champion" value="Champion">Champion
					</td>
				</tr>
                  <tr class="roweven" id="innovation_grade_two_leads">
					<td>
						<label class="desc">Employee leads initiatives to promote creativity and innovation among team members.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_10" id="Innovation_10_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_10" id="Innovation_10_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_10" id="Innovation_10_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_10" id="Innovation_10_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="innovation_grade_two_hesitate">
					<td>
						<label class="desc">Employee does not hesitate to try new things at work which is bold & not the usual norm. He/She has an appetite to tread on unchartered path.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Innovation_11" id="Innovation_11_Beginners" value="Beginner">Beginner
						<input type="radio" name="Innovation_11" id="Innovation_11_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Innovation_11" id="Innovation_11_Advanced" value="Advanced">Advanced
						<input type="radio" name="Innovation_11" id="Innovation_11_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
     <tr>
        <td>						
			<table class="formTable" id="interpersonal_skills_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2" >
						<h1>Interpersonal Skills</h1>
						<p class="assessdesc">
						Interpersonal Skills; this important competency enables an employee to be able to persuade others and negotiate to reach an agreement. This attribute is an enabler in change management, it initiates & forms a vital ingredient essential to accommodate a robust pace of change an organization goes through from time to time.
						</p>
					</td>
				</tr>
				<tr class="rowodd" >
					<td>
						<label class="desc">Employee develops strong and trusting relationships with colleagues and supervisors.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to get his/her points across in a calm, but assertive manner.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="interpersonal_skills_amicably">
					<td>
						<label class="desc">Employee is able to resolve conflicts and disagreements among team members amicably.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Champion" value="Champion">Champion
					</td>
				</tr>
                <tr class="rowodd" id="interpersonal_skills_offers">
					<td>
						<label class="desc">Employee offers to resolve conflicts and disagreements among team members amicably.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="interpersonal_skills_positively">
					<td>
						<label class="desc">Employee is able to influence the team members positively.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="interpersonal_skills_authority">
					<td>
						<label class="desc">Employee is able to influence the team without using authority.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee manages his/her emotions intelligently during conflict situations without instances of raised voices during conversations.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Champion" value="Champion">Champion
					</td>
				</tr>

                <tr class="rowodd" id="interpersonal_skills_attitude">
					<td>
						<label class="desc">Employee works to build a sense of common purpose across all work groups, avoiding a “we versus them” attitude.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_8" id="Influencing_Ability_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_8" id="Influencing_Ability_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_8" id="Influencing_Ability_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_8" id="Influencing_Ability_8_Champion" value="Champion">Champion
					</td>

				<tr class="roweven">
					<td>
						<label class="desc">Employee accurately interprets the moods, feelings and reactions of others and adjusts own behaviour to fit the situation.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_9" id="Influencing_Ability_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_9" id="Influencing_Ability_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_9" id="Influencing_Ability_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_9" id="Influencing_Ability_9_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="interpersonal_skills_feedback" >
					<td>
						<label class="desc">Employee is able to handle criticism positively & is open to feedback for improvement.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Influencing_Ability_10" id="Influencing_Ability_10_Beginners" value="Beginner">Beginner
						<input type="radio" name="Influencing_Ability_10" id="Influencing_Ability_10_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Influencing_Ability_10" id="Influencing_Ability_10_Advanced" value="Advanced">Advanced
						<input type="radio" name="Influencing_Ability_10" id="Influencing_Ability_10_Champion" value="Champion">Champion
					</td>
				</tr>
				
			</table>	
		</td>
	</tr>

	<tr>
        <td>						
			<table class="formTable" id="coaching_mentor_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Coaching and Mentoring</h1>
						<p class="assessdesc">
						Coaching and Mentoring, enables co-workers to grow and succeed through feedback, instruction, and encouragement. It helps build positive support and effectiveness of an individual, team & organization. 
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee takes initiative to guide and develop team members.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee identifies potential individuals and their strengths; works with them to build on it.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to apply different strategies to coach and mentor low performers, mediocre performers & high performers.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee builds consensus with teammates so that coaching efforts are received in a positive, developmental manner.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Champion" value="Champion">Champion
					</td>
				</tr>

				<tr class="rowodd" id="coaching_professional">
					<td>
						<label class="desc">Employee provides leadership and resources for creating an environment that is conducive to the professional development of employees.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="coaching_mentor">
					<td>
						<label class="desc">Employee is committed to don the role of a mentor, when the situation demands.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Champion" value="Champion">Champion
					</td>
				</tr>
			
				
				
			</table>	
		</td>
	</tr>

	<tr>
        <td>						
			<table class="formTable" id="resilience_tenacity_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Resilience and Tenacity</h1>
						<p class="assessdesc">
						Resilience and Tenacity: The ability to persist in resistant situations and continue to work on projects/assignments in the face of obstacles and difficulties. Resilience is not a passive quality, but an active process. Resilient people are better able to deal with the demands placed upon them, especially where those demands might require them to be dealing with constantly changing priorities and heavy workload.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee bounces back with ease and enthusiasm, whenever he/she faces failure.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr class="roweven">
					<td>
						<label class="desc">Employee does not loose focus, on the task at hand during challenging situations.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="resilience_tenacity_obstacles">
					<td>
						<label class="desc">Employee re-engineers/creates new processes and systems, to get around obstacles.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="resilience_tenacity_treat">
					<td>
						<label class="desc">Employee treats challenges at work as a learning process.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="resilience_tenacity_difficult">
					<td>
						<label class="desc">Employee helps team members to deal with disappointment/rejection positively when progress is slow or difficult.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="resilience_tenacity_group">
					<td>
						<label class="desc">Employee stays with group & cheers the team even when progress is slow or difficult.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employees displays an attitude, "when the going gets tough, the tough get going."<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="resilience_tenacity_victim">
					<td>
						<label class="desc">Employee presents self as a survivor and not a victim, when crisis arises.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_8" id="Initiative_tenacity_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_8" id="Initiative_tenacity_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_8" id="Initiative_tenacity_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_8" id="Initiative_tenacity_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is prepared to take calculated risks, come out of his/her comfort zone for the larger benefit of the organization even when others are apprehensive.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Initiative_tenacity_9" id="Initiative_tenacity_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="Initiative_tenacity_9" id="Initiative_tenacity_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Initiative_tenacity_9" id="Initiative_tenacity_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="Initiative_tenacity_9" id="Initiative_tenacity_9_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
        <td>						
			<table class="formTable" id="planning_organization_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Planning and Organization</h1>
						<p class="assessdesc">
						Planning and Organization: The ability to plan and organize enables employees to be highly productive and efficient. This skills establishes a systematic course of action for self and others in order to accomplish objectives; determines priorities and allocates resources and time effectively.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is internally motivated; He plans and prioritises own workload to meet agreed deadlines, despite changing circumstances.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="planning_organization_1" id="planning_organization_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_1" id="planning_organization_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_1" id="planning_organization_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_1" id="planning_organization_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
				   <td>
					<label class="desc">Employee effectively handles multiple demands and competing priorities.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="planning_organization_2" id="planning_organization_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_2" id="planning_organization_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_2" id="planning_organization_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_2" id="planning_organization_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee checks for errors to ensure work is delivered on a high standard, first time.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="planning_organization_3" id="planning_organization_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_3" id="planning_organization_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_3" id="planning_organization_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_3" id="planning_organization_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee advises colleagues or manager in advance of obstacles to work delivery.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="planning_organization_4" id="planning_organization_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_4" id="planning_organization_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_4" id="planning_organization_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_4" id="planning_organization_4_Champion" value="Champion">Champion
					</td>
				</tr>
                    <tr class="rowodd" id="planning_organization_self">
					<td>
						<label class="desc">Employee delegates authority to match responsibility and holds employee/team accountable, for agreed upon commitments.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="planning_organization_5" id="planning_organization_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_5" id="planning_organization_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_5" id="planning_organization_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_5" id="planning_organization_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="planning_organization_handle">
				   <td>
					<label class="desc">Employee uses knowledge of process and client to help formulate appropriate plans.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="planning_organization_6" id="planning_organization_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_6" id="planning_organization_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_6" id="planning_organization_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_6" id="planning_organization_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="planning_organization_errors">
					<td>
						<label class="desc">Employee pays attention to vital details for smooth implementation of tasks.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="planning_organization_7" id="planning_organization_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_7" id="planning_organization_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_7" id="planning_organization_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_7" id="planning_organization_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="planning_organization_colleague">
					<td>
						<label class="desc">Employee goes beyond the call of duty to make his/her plans work.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="planning_organization_8" id="planning_organization_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_8" id="planning_organization_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_8" id="planning_organization_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_8" id="planning_organization_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="planning_organization_engages" >
					<td>
						<label class="desc">Employee engages the scheduled time and resources effectively.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="planning_organization_9" id="planning_organization_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_9" id="planning_organization_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_9" id="planning_organization_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_9" id="planning_organization_9_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="planning_organization_sys">
					<td>
						<label class="desc">Employee uses systems to keep track of information; keeps detailed records as necessary.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="planning_organization_10" id="planning_organization_10_Beginners" value="Beginner">Beginner
						<input type="radio" name="planning_organization_10" id="planning_organization_10_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="planning_organization_10" id="planning_organization_10_Advanced" value="Advanced">Advanced
						<input type="radio" name="planning_organization_10" id="planning_organization_10_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>

	<tr>
        <td>						
			<table class="formTable" id="cultural_adaptability_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Cultural Adaptability</h1>
						<p class="assessdesc">
						Cultural Adaptability: This attribute helps in better diversity, adaptability & open mindedness which enhances team collaboration.
						</p>
					</td>
				</tr>
				 
				<tr class="rowodd" id="cultural_adaptability_situation">
					<td>
						<label class="desc">Employee does not hold prejudices & is not judgemental about his co-workers or situations.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="cultural_adaptability_culture">
					<td>
						<label class="desc">Employee has the ability to work respectfully, knowledgeably and effectively with employees from all regions/religion & cultures.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
				   <td>
					<label class="desc">Employee is sensitive about client's business culture and behaves appropriately.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="cultural_adaptability_caste">
					<td>
						<label class="desc">Employee understands and handles sensitive issues pertaining to an individual’s  culture/religion/caste/colour appropriately.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is eager to empower him/her self with information, knowledge and technology provided by the organization through several programs.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee imbibes 'Meritocracy', one of the core values of the organization and sets goals to be the top performer in his professional endeavors.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee does not micro manage his/her reportees instead empowers them with inputs and data to take the right decisions whenever and wherever necessary.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
        <td>						
			<table class="formTable" id="people_leadership_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>People Leadership</h1>
						<p class="assessdesc">
						People Leadership: When one stimulates extraordinary effort & results with a group of people or team members. Takes true interest in people who work with the organization, celebrates people & organization successes and is a role model in people practices.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee creates work teams whose members have complementary skills or experience.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
				   <td>
					<label class="desc">Employee has high ethical and moral standards.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="People_Leadership_2" id="People_Leadership_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee acknowledges and celebrates team members accomplishments.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_3" id="People_Leadership_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands when the group is off-track and redirects the conversation towards productive channels.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_4" id="People_Leadership_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="people_leadership_pro_quo">
					<td>
						<label class="desc">Employee doesn’t promote or display favoritism (quid pro quo).<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_5" id="People_Leadership_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="people_leadership_block_leverage" >
					<td>
						<label class="desc">Employee doesn’t leverage his/her designation to practice or promote quid pro quo (a favour for a favour).<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_6" id="People_Leadership_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee serves as a role model to others and is committed to achieving challenging objectives.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_7" id="People_Leadership_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee builds support and enthusiasm for the accomplishment of stated goals.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_8" id="People_Leadership_8_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee in the leadership role freely and openly communicates with the team members without prejudice or bias.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="People_Leadership_9" id="People_Leadership_9_Beginners" value="Beginner">Beginner
						<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Advanced" value="Advanced">Advanced
						<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>

<tr>
        <td>						
			<table class="formTable" id="execution_excellence_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Execution Excellence</h1>
						<p class="assessdesc">
						Execution Excellence: Employee is single-mindedly focused on enabling an excellent outcome in all that one does, demonstrating excellence in every aspect of work involving communication, work execution, people bonding and not limited to these alone.
						</p>
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee has a ‘can do’ attitude at all times.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
				   <td>
					<label class="desc">Employee sets stretched goals/targets (challenging- but achievable) with relevant metrics for self and others; works hard to meet them.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee constantly strives to complete tasks in a commendable manner, sets an example for colleagues to follow, while receiving accolades and appreciation for the tasks.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee takes pride in work and encourages others to do the same; effectively balances quality, service, and productivity.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="execution_excellence_quid_quo">
					<td>
						<label class="desc">Employee doesn’t promote or display favoritism (quid pro quo).<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee applies a range of strategies to solve problems.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven" id="execution_excellence_holistic">
					<td>
						<label class="desc">Employee has a holistic perspective of the entire system and takes action to improve the efficiency and quality of the process.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
        <td>						
			<table class="formTable" id="strategic_alignment_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Strategic Alignment</h1>
						<p class="assessdesc">
						Strategic Alignment: Employee is able to consistently demonstrate abilities towards aligning one’s objectives & outcomes to that of the organization.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee studies the market place challenges and demands consequently educates and upskills self and team to match the requirement.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
				   <td>
					<label class="desc">Employee is committed to pursuing organization's goals and vision.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee understands organization's philosophy & is willing to make midcourse corrections when required.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee translates the organization's vision for change into concrete specifics that enable others to implement it.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd" id="execution_excellence_quid">
					<td>
						<label class="desc">Employee has the competency to analyze the discrepancies between expected and actual results and is able to determine their cause and find a solution.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is oriented to change, which is required for the organization's growth.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<tr>
        <td>						
			<table class="formTable" id="foresight_block" style="width:90% !important; display: none;">
								
				<tr>
					<td colspan="2">
						<h1>Foresight</h1>
						<p class="assessdesc">
						Foresight: Thinks & acts mindful of long term consequences on all that one does. Ability to generate consistent innovative ideas to foster & better processes & practices. One can develop a foresight, based on the past experiences on similar situations with an intution of the future based on the knowledge gained.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee interprets and analyses data to make future decisions and policies.<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Foresight_1" id="Foresight_1_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_1" id="Foresight_1_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_1" id="Foresight_1_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_1" id="Foresight_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
				   <td>
					<label class="desc">Employee understands the strategic aims of other players in the sector and plans resources accordingly.<font color='red'>*</font></label>
					</td>
				   <td>
					   <input type="radio" name="Foresight_2" id="Foresight_2_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_2" id="Foresight_2_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_2" id="Foresight_2_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_2" id="Foresight_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is capable of predicting situations & taking measures to overcome that.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Foresight_3" id="Foresight_3_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_3" id="Foresight_3_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_3" id="Foresight_3_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_3" id="Foresight_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is capable of anticipating, planning & executing short term, midterm & long term goals.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Foresight_4" id="Foresight_4_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_4" id="Foresight_4_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_4" id="Foresight_4_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_4" id="Foresight_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to analyse historical data.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Foresight_5" id="Foresight_5_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_5" id="Foresight_5_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_5" id="Foresight_5_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_5" id="Foresight_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to foresee attrition & plan accordingly.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Foresight_6" id="Foresight_6_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_6" id="Foresight_6_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_6" id="Foresight_6_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_6" id="Foresight_6_Champion" value="Champion">Champion
					</td>
				</tr>
                <tr class="rowodd">
					<td>
						<label class="desc">Employee plans ahead for the succession of critical resources.<font color='red'>*</font></label>
							
					</td>
					<td>
					   <input type="radio" name="Foresight_7" id="Foresight_7_Beginners" value="Beginner">Beginner
						<input type="radio" name="Foresight_7" id="Foresight_7_Intermediate" value="Intermediate">Intermediate
						<input type="radio" name="Foresight_7" id="Foresight_7_Advanced" value="Advanced">Advanced
						<input type="radio" name="Foresight_7" id="Foresight_7_Champion" value="Champion">Champion
					</td>
				</tr>
			</table>	
		</td>
	</tr>
	<!--<tr>
		<td colspan="2">
			<h1>Organization Wide Competency Mapping</h1>
		</td>
	</tr>
	
	<tr>
		<td>
			<table class="formTable" id="grade1" style="width:90% !important; display: none;">
			<tr>
				<td colspan="2">
					<h1>Communication </h1>
					<p class="assessdesc">
						Communication is the activity of conveying information through the exchange of thoughts, messages, or information, as by speech, visuals, signals, writing, or behaviour. It is the meaningful exchange of information between two or more living creatures.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Able to express the thoughts, ideas effectively:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_1" id="Communication_1_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_1" id="Communication_1_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_1" id="Communication_1_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_1" id="Communication_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Express his/her ideas without any difficulties:<font color='red'>*</font></label>
							
					</td>
					<td>
					    <input type="radio" name="Communication_2" id="Communication_2_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_2" id="Communication_2_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_2" id="Communication_2_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_2" id="Communication_2_Champion" value="Champion">Champion
					
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Sentences are clear and concise:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_3" id="Communication_3_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_3" id="Communication_3_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_3" id="Communication_3_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_3" id="Communication_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Is organized,  methodical while communicating with peers, superiors and external teams:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_4" id="Communication_4_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_4" id="Communication_4_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_4" id="Communication_4_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_4" id="Communication_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Draft emails to customers with Grammar & punctuation in place:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_5" id="Communication_5_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_5" id="Communication_5_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_5" id="Communication_5_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_5" id="Communication_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Calm and focused with clarity of speech:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_6" id="Communication_6_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_6" id="Communication_6_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_6" id="Communication_6_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_6" id="Communication_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Appreciate different points of views and acknowledge them:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_7" id="Communication_7_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_7" id="Communication_7_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_7" id="Communication_7_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_7" id="Communication_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Seek necessary information without hesitation:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_8" id="Communication_8_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_8" id="Communication_8_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_8" id="Communication_8_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_8" id="Communication_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Can converse in English fluently:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_9" id="Communication_9_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_9" id="Communication_9_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_9" id="Communication_9_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_9" id="Communication_9_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Provide complete and correct information:<font color='red'>*</font></label>
							
					</td>
					<td>
						<input type="radio" name="Communication_10" id="Communication_10_Knowledgeable" value="Knowledgeable">Knowledgeable
						<input type="radio" name="Communication_10" id="Communication_10_Practitioner" value="Practitioner">Practitioner
						<input type="radio" name="Communication_10" id="Communication_10_Proficient" value="Proficient">Proficient
						<input type="radio" name="Communication_10" id="Communication_10_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h1>Customer Orientation</h1>
						<p class="assessdesc">
							Customer Orientation is an aligned organization-wide approach to customer satisfaction and service, leading to customer loyalty and advocacy. The result is sustainable profitability. In a Customer Focused organization, Leadership, Processes and People are customer-aligned and aid in every action being shaped by a relentless commitment to meeting and exceeding customer expectations regarding product and service quality. Every employee understands what he/she must do in order to maintain and add value to every relationship with both the paying customer and those within the organization that rely on them for the work they do.
						</p>
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee understands customers requirements:<font color='red'>*</font></label></td>
					<td>	<!--  Customer_Orientation_1 this ID was there in Early-- -->
						<!--	<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_1" id="Customer_Orientation_1_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands the importance of adherence to TAT:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_2" id="Customer_Orientation_2_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee understands the effects of errors & trys to keep it to the minimum:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_3" id="Customer_Orientation_3_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands customers, circumstances, problems & expectations & plans his/her work schedule accordingly:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_4" id="Customer_Orientation_4_Champion"  value="Champion">Champion
					</td>	
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is open to feedback from their client & shares key points with supervisors:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_5" id="Customer_Orientation_5_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands the effect of over commitments, hence avoids over commitment:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_6" id="Customer_Orientation_6_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Wherever possible employee willingly orients the client since he/she is the subject matter expert:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_7" id="Customer_Orientation_7_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee clarifies when he/she has doubts in regards to the assigned project:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_8" id="Customer_Orientation_8_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee understands the importance of responsiveness:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_9" id="Customer_Orientation_9_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands the importance of sharing information:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Customer_Orientation_10" id="Customer_Orientation_10_Champion"  value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Integrity & Work Ethics</h1>
					<p class="assessdesc">
						Integrity involves representing oneself truthfully at all times, conducting oneself ethically and professionally, acknowledging the work of others, and being accountable for one's actions. It fosters trust between employees and employers, something that is absolutely essential in order to realize the full potential of an employees work term. Acts consistently with corporate values, code of conduct and standards of integrity
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee has professional attitude towards work:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_1" id="Integrity_Work_Ethics_1_Champion"  value="Champion">Champion
					</td>
					
				</tr>
				<tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee respects Co-workers and is able to Build Trust:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_2" id="Integrity_Work_Ethics_2_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee exhibits Responsible Behavior with out any Supervision:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_3" id="Integrity_Work_Ethics_3_Champion"  value="Champion">Champion
					</td>	
				</tr>
				<tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is committed to organization's process & policies:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_4" id="Integrity_Work_Ethics_4_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee does not compromise value system at any given point of time:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_5" id="Integrity_Work_Ethics_5_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is not manipulative:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_6" id="Integrity_Work_Ethics_6_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee aligns to his /her personal ethics to work ethics:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_7" id="Integrity_Work_Ethics_7_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee does not discriminate based on age, gender, language, region & religion:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Integrity_Work_Ethics_8" id="Integrity_Work_Ethics_8_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				
				<td colspan="2">
					<h1>Dependable & Accountable</h1>
					<p class="assessdesc">
						Dependable competency is when an employee is "reliable, steady & trustworthy" to be able to carry out the tasks and always can be counted on to own an activity in full. Being accountable is an attribute which demonstrates owning up one's actions and being able to take responsibility, couples up as a valuable competency for an organization & its people to foster a healthy environment & culture to propel towards growth. Consistently delivers on commitments. Takes full responsibility for the work and is accountable for all his/her decisions.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee always keeps his/her word:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_1" id="Dependable_Accountable_1_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Regardless of the situation, employee will make his work a priority:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_2" id="Dependable_Accountable_2_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to handle obstacles, pressures, and demands positively:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_3" id="Dependable_Accountable_3_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee always aims for his/her 100% contribution:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_4" id="Dependable_Accountable_4_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee believes in helping others, but not at his cost:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_5" id="Dependable_Accountable_5_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee works extra hours if that is what it takes to get the job done right:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_6" id="Dependable_Accountable_6_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee shows steady performance to show dependability:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_7" id="Dependable_Accountable_7_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee takes up all the assigned projects to logical conclusion:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_8" id="Dependable_Accountable_8_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee takes ownership which shows accountability:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Dependable_Accountable_9" id="Dependable_Accountable_9_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Dependable_Accountable_9" id="Dependable_Accountable_9_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Dependable_Accountable_9" id="Dependable_Accountable_9_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Dependable_Accountable_9" id="Dependable_Accountable_9_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<td colspan="2">
					<h1>Team Collaboration</h1>
					<p class="assessdesc">
						Collaboration is working with each other to do a task and to achieve shared goals. It is a recursive process where two or more people or organizations work together to realize shared goals, (this is more than the intersection of common goals seen in co-operative ventures, but a deep, collective determination to reach an identical objective - for example, an endeavor that is creative in nature-by sharing knowledge, learning and building consensus. In particular, teams that work collaboratively can obtain greater resources, recognition and reward when facing competition for finite resources
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee actively participates in all kind of projects allocated:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_1" id="Team_Collaboration_1_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee cooperates and pitches in to help when needed:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_2" id="Team_Collaboration_2_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee believes & shows commitment towards the team:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_3" id="Team_Collaboration_3_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee communicates constructively & shares knowledge openly and willingly:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_4" id="Team_Collaboration_4_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee treats others in a respectful and supportive manner:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_5" id="Team_Collaboration_5_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to work as a problem-solver & collaborates within the team & other teams:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_6" id="Team_Collaboration_6_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee breeds positive thinking in the team:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_7" id="Team_Collaboration_7_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee focus on the strengths of working in a team rather than creating islands with in the team:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Practitioner"  value="Practitioner">Practitioner
							<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Proficient"  value="Proficient">Proficient
							<input type="radio" name="Team_Collaboration_8" id="Team_Collaboration_8_Champion"  value="Champion">Champion
					</td>
				</tr>
				
				
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table class="formTable" id="grade2" style="width:90% !important; display: none;">
			<tr>
				<td colspan="2">
					<h1>Influencing Ability</h1>
					<p class="assessdesc">
						This important competency enables an employee to be able to persuade others and negotiate to reach an agreement. This attribute is a enabler in change management ,initiatives & forms a vital ingredient essential to accommodate a robust pace of change an organization goes through from time to time.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to develop a line of reasoned argument:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_1" id="Influencing_Ability_1_Champion"   value="Champion">Champion
					</td>
					
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to back up his/her points with logical data and in positive language:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_2" id="Influencing_Ability_2_Champion"   value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to emphasize on the positive points of his proposal:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_3" id="Influencing_Ability_3_Champion"   value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to get his/her points across in a calm, but assertive manner:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_4" id="Influencing_Ability_4_Champion"   value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to resolve conflicts and disagreements among team members:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_5" id="Influencing_Ability_5_Champion"   value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to influence the team without using authority:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_6" id="Influencing_Ability_6_Champion"   value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee has an insight into what team members value:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Knowledgeable"   value="Knowledgeable">Knowledgeable
							<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Practitioner"   value="Practitioner">Practitioner
							<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Proficient"   value="Proficient">Proficient
							<input type="radio" name="Influencing_Ability_7" id="Influencing_Ability_7_Champion"   value="Champion">Champion
					</td>
				</tr>
				
			<tr>
				<td colspan="2">
					<h1>Coaching & Mentoring</h1>
					<p class="assessdesc">
						An expander competency helps build positive support, feedback and advice to an individual or group basis to improve the effectiveness & ability of an individual, team & organization to guide and result a positive outcome and foster a culture of expanding best practices & knowledge
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to focus on improving performance and developing Team members:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_1" id="Coaching_Mentoring_1_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to recognize potential High Performer & coach them to become a HIPO employee:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_2" id="Coaching_Mentoring_2_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee as different strategies to coaching average employees & high performers:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_3" id="Coaching_Mentoring_3_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee guides team members to boost his/her confidence when he/she is struggling with a difficult task, & helps them to develop independence to meet his/her responsibilities:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_4" id="Coaching_Mentoring_4_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is genuinely willing to work with a team member who is a low performer or in PIP, who is making a good faith effort to improve:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_5" id="Coaching_Mentoring_5_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is willing to coach & Mentor any team member who is in need:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_6" id="Coaching_Mentoring_6_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee motivates team members. He uses different approach with each team member:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Coaching_Mentoring_7" id="Coaching_Mentoring_7_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Coaching_Mentoring_7" id="Coaching_Mentoring_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Coaching_Mentoring_7" id="Coaching_Mentoring_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Coaching_Mentoring_7" id="Coaching_Mentoring_7_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr>
				<td colspan="2">
					<h1>Innovation</h1>
					<p class="assessdesc">
						The process of translating an idea or invention into a tangible outcome creating value to the organization. Innovation involves deliberate application of information, imagination & initiative in deriving greater or differential outcome.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to come up with new ideas & implement them:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_1" id="Innovation_1_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_1" id="Innovation_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_1" id="Innovation_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_1" id="Innovation_1_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee thinks out of the box to solve problems within the team:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_2" id="Innovation_2_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_2" id="Innovation_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_2" id="Innovation_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_2" id="Innovation_2_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee encourages all creative ideas & is patient about the outcome:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_3" id="Innovation_3_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_3" id="Innovation_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_3" id="Innovation_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_3" id="Innovation_3_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee gathers information from a wide variety of sources to stay current with what is happening in his field of work:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_4" id="Innovation_4_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_4" id="Innovation_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_4" id="Innovation_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_4" id="Innovation_4_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee sees problems, complaints  and bottlenecks as Innovative opportunities rather than as issues:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_5" id="Innovation_5_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_5" id="Innovation_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_5" id="Innovation_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_5" id="Innovation_5_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee as a deeper insight into Creativity & Innovation:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_6" id="Innovation_6_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_6" id="Innovation_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_6" id="Innovation_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_6" id="Innovation_6_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee does not hesitate to work with new ideas by being bothered about consequences:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_7" id="Innovation_7_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_7" id="Innovation_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_7" id="Innovation_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_7" id="Innovation_7_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is confident that they can develop creative ideas to solve problems, and are motivated to implement solutions:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_8" id="Innovation_8_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_8" id="Innovation_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_8" id="Innovation_8_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_8" id="Innovation_8_Champion"  value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is not shy of failure:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Innovation_9" id="Innovation_9_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Innovation_9" id="Innovation_9_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Innovation_9" id="Innovation_9_Proficient" value="Proficient">Proficient
							<input type="radio" name="Innovation_9" id="Innovation_9_Champion"  value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Initiative & tenacity</h1>
					<p class="assessdesc">
					 	Identifies, seeks and encourages areas for improvement. Eager to take on additional responsibilities and challenging tasks. Identifies automation/improvement opportunities and ensures implementation.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee actively seeks and seizes opportunities to contribute to and achieve goals:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_1" id="Initiative_tenacity_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee maintains a sense of purpose, value and ownership of work:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_2" id="Initiative_tenacity_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee works independently with little direction or supervision:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_3" id="Initiative_tenacity_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee maintains a level of energy and work actively to achieve goals:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_4" id="Initiative_tenacity_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee stays with group & cheers the team even when progress is slow or difficult:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_5" id="Initiative_tenacity_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee address difficulties & draws on skills, knowledge & understanding to find solutions to the problem:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_6" id="Initiative_tenacity_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee maintains an energetic & focused approach to new or repeated challenges:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Initiative_tenacity_7" id="Initiative_tenacity_7_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Outcome Orientation</h1>
					<p class="assessdesc">
						Ability or mindset to get things done with focus & energy displayed in ample measure to be able to drive a result from within & teams if a people manager
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is a self-starter. Usually completes projects before the deadline, and takes great pride in the work that he does:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Outcome_Orientation_1" id="Outcome_Orientation_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Outcome_Orientation_1" id="Outcome_Orientation_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Outcome_Orientation_1" id="Outcome_Orientation_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Outcome_Orientation_1" id="Outcome_Orientation_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee weighs in mediocrity & meritocracy. Promotes meritocracy:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Outcome_Orientation_2" id="Outcome_Orientation_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Outcome_Orientation_2" id="Outcome_Orientation_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Outcome_Orientation_2" id="Outcome_Orientation_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Outcome_Orientation_2" id="Outcome_Orientation_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is willing to share the best practices with others:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Outcome_Orientation_3" id="Outcome_Orientation_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Outcome_Orientation_3" id="Outcome_Orientation_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Outcome_Orientation_3" id="Outcome_Orientation_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Outcome_Orientation_3" id="Outcome_Orientation_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is constantly striving to learn more about clients in order to improve his performance:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Outcome_Orientation_4" id="Outcome_Orientation_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Outcome_Orientation_4" id="Outcome_Orientation_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Outcome_Orientation_4" id="Outcome_Orientation_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Outcome_Orientation_4" id="Outcome_Orientation_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is highly motivated to express an outward result of his/her efforts:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Outcome_Orientation_5" id="Outcome_Orientation_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Outcome_Orientation_5" id="Outcome_Orientation_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Outcome_Orientation_5" id="Outcome_Orientation_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Outcome_Orientation_5" id="Outcome_Orientation_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is very goal directed and feels a great sense of accomplishment when he is able to achieve his goal:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Outcome_Orientation_6" id="Outcome_Orientation_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Outcome_Orientation_6" id="Outcome_Orientation_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Outcome_Orientation_6" id="Outcome_Orientation_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Outcome_Orientation_6" id="Outcome_Orientation_6_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Cultural Agility</h1>
					<p class="assessdesc">
						The attribute of an employee with cultural agility helps in better diversity, adaptability & open mindedness.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee has the ability to work respectfully, knowledgeably and effectively with employees from all regions & cultures:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_1" id="Cultural_Agility_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee has a open mind towards diversity:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_2" id="Cultural_Agility_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is not partial towards certain religion & culture:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_3" id="Cultural_Agility_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee does not use derogative language while talking to people:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_4" id="Cultural_Agility_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee maintains professionalism while working or outside the purview of work atmosphere:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_5" id="Cultural_Agility_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands delicate issues pertaining to org culture:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_6" id="Cultural_Agility_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee appreciates different points of view:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Cultural_Agility_7" id="Cultural_Agility_7_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>People Leadership</h1>
					<p class="assessdesc">
						Is one who stimulates extraordinary effort & results with a group of people or team members. Takes true interest in people who work with the organization, celebrates people & organization successes and are a role model in people practices.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is energized when the team counts on his leadership:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_1" id="People_Leadership_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee believes to lead from the front during crises:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_2" id="People_Leadership_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is capable of complimenting his/her team, when progress is made:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_3" id="People_Leadership_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee cheers the team, when times are good and when times are bad:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_4" id="People_Leadership_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee does not promote or display favoritism (quid pro quo):<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_5" id="People_Leadership_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is a team player:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_6" id="People_Leadership_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee Leads by example:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_7" id="People_Leadership_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to motivate his/her team:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_8" id="People_Leadership_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is true people leader rather that an autocrat:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Proficient" value="Proficient">Proficient
							<input type="radio" name="People_Leadership_9" id="People_Leadership_9_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Execution Excellence</h1>
					<p class="assessdesc">
						Is single-mindedly focused on enabling an excellent outcome in all that one does, demonstrating excellence in every aspect of work involving communication, work execution, people bonding and not limited to these alone
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee consistently achieves TAT & Quality:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_1" id="Execution_Excellence_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands the importance of client satisfaction & business continuity:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_2" id="Execution_Excellence_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee always believes in  driving best practices:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_3" id="Execution_Excellence_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee always communicates the importance of TAT, SLA & Quality to the team:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_4" id="Execution_Excellence_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee believes that work is always above & beyond set standards:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_5" id="Execution_Excellence_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">When employee is working with Clients or Management, he/she proactively keeps them informed regardless of good news or bad news:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_6" id="Execution_Excellence_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is focused on energizing & enthusing the team towards executive excellence:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_7" id="Execution_Excellence_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee demonstrates excellence in every aspect of work involving communication:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Execution_Excellence_8" id="Execution_Excellence_8_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Execution_Excellence_8" id="Execution_Excellence_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Execution_Excellence_8" id="Execution_Excellence_8_Proficient" value="Proficient">Proficient
							<input type="radio" name="Execution_Excellence_8" id="Execution_Excellence_8_Champion" value="Champion">Champion
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table class="formTable" id="grade3" style="width:90% !important; display: none;">
			<tr>
				<td colspan="2">
					<h1>Commercial, Financial Acumen & Business Acumen</h1>
					<p class="assessdesc">
						Is keenness and quickness in understanding and dealing with a business situation in a manner that is likely to lead to a good outcome. It also is the knowledge and understanding of the financial, accounting, marketing and operational functions of an organization& the ability to make good judgments and quick decisions. Business acumen is broadly ability to 
						a) See the "big picture" of your organization-how the key drivers of business relate to each other, work together to produce profitable growth, and relate to the job you do each day 
						b) Understand important company communication and data, including financial statements 
						c) Understand how your actions and decisions impact key company measures and the objectives of your company's leadership 
						d) Effectively communicate your ideas to other employees, managers, executives, and the public
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is accurate with billing and avoids loss of billing:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_1" id="Commercial_Financial_Acumen_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_1" id="Commercial_Financial_Acumen_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_1" id="Commercial_Financial_Acumen_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_1" id="Commercial_Financial_Acumen_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands & adherence to SLA:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_2" id="Commercial_Financial_Acumen_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_2" id="Commercial_Financial_Acumen_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_2" id="Commercial_Financial_Acumen_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_2" id="Commercial_Financial_Acumen_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee maximize billing potential:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_3" id="Commercial_Financial_Acumen_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_3" id="Commercial_Financial_Acumen_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_3" id="Commercial_Financial_Acumen_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_3" id="Commercial_Financial_Acumen_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee does accurate resources allocation:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_4" id="Commercial_Financial_Acumen_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_4" id="Commercial_Financial_Acumen_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_4" id="Commercial_Financial_Acumen_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_4" id="Commercial_Financial_Acumen_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee Doesn't lose out on the opportunity to up-sell Theorem Services:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_5" id="Commercial_Financial_Acumen_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_5" id="Commercial_Financial_Acumen_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_5" id="Commercial_Financial_Acumen_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_5" id="Commercial_Financial_Acumen_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands the nuances of Profit & Loss statements:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_6" id="Commercial_Financial_Acumen_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_6" id="Commercial_Financial_Acumen_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_6" id="Commercial_Financial_Acumen_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_6" id="Commercial_Financial_Acumen_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to manage operative cost at reasonable levels:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_7" id="Commercial_Financial_Acumen_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_7" id="Commercial_Financial_Acumen_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_7" id="Commercial_Financial_Acumen_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_7" id="Commercial_Financial_Acumen_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands budgetary provisions:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Commercial_Financial_Acumen_8" id="Commercial_Financial_Acumen_8_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Commercial_Financial_Acumen_8" id="Commercial_Financial_Acumen_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Commercial_Financial_Acumen_8" id="Commercial_Financial_Acumen_8_Proficient" value="Proficient">Proficient
							<input type="radio" name="Commercial_Financial_Acumen_8" id="Commercial_Financial_Acumen_8_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Sales & Brand Promotion</h1>
					<p class="assessdesc">
						Displays the ability to communicate & propagate awareness on Theorem's practices & businesses to internal & external stakeholders consistently.
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee brands self & Theorem in a positive way:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Sales_Brand_Promotion_1" id="Sales_Brand_Promotion_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Sales_Brand_Promotion_1" id="Sales_Brand_Promotion_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Sales_Brand_Promotion_1" id="Sales_Brand_Promotion_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Sales_Brand_Promotion_1" id="Sales_Brand_Promotion_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee uses every opportunity to do positive branding:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Sales_Brand_Promotion_2" id="Sales_Brand_Promotion_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Sales_Brand_Promotion_2" id="Sales_Brand_Promotion_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Sales_Brand_Promotion_2" id="Sales_Brand_Promotion_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Sales_Brand_Promotion_2" id="Sales_Brand_Promotion_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee communicates & broadcast awareness on Theorem's practices & businesses to internal & external stakeholders consistently:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Sales_Brand_Promotion_3" id="Sales_Brand_Promotion_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Sales_Brand_Promotion_3" id="Sales_Brand_Promotion_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Sales_Brand_Promotion_3" id="Sales_Brand_Promotion_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Sales_Brand_Promotion_3" id="Sales_Brand_Promotion_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is aware of all the products & services offered by Theorem:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Sales_Brand_Promotion_4" id="Sales_Brand_Promotion_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Sales_Brand_Promotion_4" id="Sales_Brand_Promotion_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Sales_Brand_Promotion_4" id="Sales_Brand_Promotion_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Sales_Brand_Promotion_4" id="Sales_Brand_Promotion_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee embraces & encompass all qualities of the Organizations during his interactions with internal & external stakeholders:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Sales_Brand_Promotion_5" id="Sales_Brand_Promotion_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Sales_Brand_Promotion_5" id="Sales_Brand_Promotion_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Sales_Brand_Promotion_5" id="Sales_Brand_Promotion_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Sales_Brand_Promotion_5" id="Sales_Brand_Promotion_5_Champion" value="Champion">Champion
					</td>
				</tr>
				
				<tr>
				<td colspan="2">
					<h1>Strategic Alignment</h1>
					<p class="assessdesc">
						Is able to consistently demonstrate abilities towards aligning one's objectives & outcomes to that of the organization
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is aware of organization's market place challenges & our capabilities:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_1" id="Strategic_Alignment_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands the Organization's Vision & Core Values:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_2" id="Strategic_Alignment_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is committed to pursuing Organization's Goal:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_3" id="Strategic_Alignment_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee plans, establishes the objectives, processes and necessary skills to deliver the desired results:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_4" id="Strategic_Alignment_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">When required employee implements new processes in-line with Company's strategies:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_5" id="Strategic_Alignment_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee has the competency to analyze the discrepancies between expected and actual results to determine their cause:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_6" id="Strategic_Alignment_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee has an orientation towards Change that is required for Organization's growth:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_7" id="Strategic_Alignment_7_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_7" id="Strategic_Alignment_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_7" id="Strategic_Alignment_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_7" id="Strategic_Alignment_7_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee understands Organization's Philosophy & is willing to take mid-course corrections when required:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_8" id="Strategic_Alignment_8_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_8" id="Strategic_Alignment_8_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_8" id="Strategic_Alignment_8_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_8" id="Strategic_Alignment_8_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee has the understanding of all the departments:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_9" id="Strategic_Alignment_9_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_9" id="Strategic_Alignment_9_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_9" id="Strategic_Alignment_9_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_9" id="Strategic_Alignment_9_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee has way-with-all to accept challenges:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Strategic_Alignment_10" id="Strategic_Alignment_10_Knowledgeable"  value="Knowledgeable">Knowledgeable
							<input type="radio" name="Strategic_Alignment_10" id="Strategic_Alignment_10_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Strategic_Alignment_10" id="Strategic_Alignment_10_Proficient" value="Proficient">Proficient
							<input type="radio" name="Strategic_Alignment_10" id="Strategic_Alignment_10_Champion" value="Champion">Champion
					</td>
				</tr>
							
				<tr>
				<td colspan="2">
					<h1>Foresight</h1>
					<p class="assessdesc">
						Thinks & acts mid to long term on all that one does and able to generate consistent innovative ideas to foster & better processes & practices
					</p>
				</td>
			</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is capable of assessing a particular situation & act accordingly:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_1" id="Foresight_1_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_1" id="Foresight_1_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_1" id="Foresight_1_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_1" id="Foresight_1_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to detect problems in advance:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_2" id="Foresight_2_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_2" id="Foresight_2_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_2" id="Foresight_2_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_2" id="Foresight_2_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is capable of predicting crisis situations & taking measures to overcome that:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_3" id="Foresight_3_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_3" id="Foresight_3_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_3" id="Foresight_3_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_3" id="Foresight_3_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is capable of anticipating, planning & executing short term, mid term & long term goals:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_4" id="Foresight_4_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_4" id="Foresight_4_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_4" id="Foresight_4_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_4" id="Foresight_4_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee is able to analyze historical data:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_5" id="Foresight_5_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_5" id="Foresight_5_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_5" id="Foresight_5_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_5" id="Foresight_5_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="roweven">
					<td>
						<label class="desc">Employee is able to foresee attrition & plan accordingly:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_6" id="Foresight_6_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_6" id="Foresight_6_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_6" id="Foresight_6_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_6" id="Foresight_6_Champion" value="Champion">Champion
					</td>
				</tr>
				<tr class="rowodd">
					<td>
						<label class="desc">Employee understands the importance of succession plan of critical resources:<font color='red'>*</font></label></td>
					<td>
							<input type="radio" name="Foresight_7" id="Foresight_7_Knowledgeable" value="Knowledgeable">Knowledgeable
							<input type="radio" name="Foresight_7" id="Foresight_7_Practitioner" value="Practitioner">Practitioner
							<input type="radio" name="Foresight_7" id="Foresight_7_Proficient" value="Proficient">Proficient
							<input type="radio" name="Foresight_7" id="Foresight_7_Champion" value="Champion">Champion
					</td>
				</tr>
				
			</table>
		</td>
	</tr>  -->
	
	<tr>
		<td>
			<input id="saveForm" name="saveForm" class="btTxt" type="submit" value="Submit" style="display:none;" onclick="return validateForm()"/>
		</td>
	</tr> 
</table>
</form>

</div>
<div class="Footer">&copy; 2019-12 Theorem</div>
</body>
</html>