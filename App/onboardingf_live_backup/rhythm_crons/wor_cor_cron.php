<?php

	ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
	ini_set('memory_limit', '-1');
	
	require ('../config/mysql_crud.php');
	require_once('../phpmailer/class.phpmailer.php');
	date_default_timezone_set('Asia/Kolkata');
		
	$db = new Database();
	$autoEmails = array('2 MONTH','9 WEEK', '8 WEEK', '7 WEEK', '6 WEEK', '5 WEEK', '4 WEEK', '3 WEEK', '2 WEEK', '1 WEEK');
	
	/*An existing WOR auto closes on the expiry date */	
	$worstatussql = "UPDATE wor SET `status` = 'Closed', `Description` = 'WOR End Date is Today, so Automatically Expired' WHERE `status` = 'Open' AND `end_date` =  DATE(NOW());";
	$db->sql($worstatussql);
		
	foreach ($autoEmails as $autoEmail)
	{
		$getWorQuery = "SELECT * FROM `wor` WHERE wor.status = 'Open' AND `end_date` = DATE(NOW())+INTERVAL ".$autoEmail;
		$db->sql($getWorQuery);
		$worData = $db->getResult();
						
		if (!empty($worData))
		{									
			
			if (count($worData) == count($worData, COUNT_RECURSIVE))
			{
				$NotificationDate = date("Y-m-d H:i:s");
				$Title = 'Client WOR';
				$wor_enddate = date_create($worData['end_date']);
				$Description = $worData['work_order_id']. " Expires on " . DATE_FORMAT($wor_enddate, 'd-M-Y');
				$ReceiverEmployee = $worData['client_partner'].','.$worData['engagement_manager'].','.$worData['delivery_manager'];
				$DeleteFlag = 0;
				$SenderEmployee = 1917;
				$ReadFlag = 0;
				$Type = 1;
				
				$notificationsql = "INSERT INTO notification
				(Title, Description, SenderEmployee, ReceiverEmployee, NotificationDate, ReadFlag, Type, DeleteFlag)
				VALUES ('$Title', '$Description', '$SenderEmployee', '$ReceiverEmployee', '$NotificationDate', $ReadFlag, $Type, $DeleteFlag)";
				
				$db->sql($notificationsql);
				
				$message	= "<span style='font-family:arial; font-size:12px;'> <b>Message : ".$Description."</b> <br/> <br/>
											Thanks<br/>
											Rhythm Support
										</span>";
				$subject 			= 'WORK ORDER : '.$Description.'';
				$email = 'ganavi.jagadeesh@theoreminc.net';
				$cc = "rhythmsupport@theoreminc.net";
				sendMail($email,$subject,$message,$cc);
			}
			else
			{
				foreach ($worData as $worinfo)
				{
					$NotificationDate = date("Y-m-d H:i:s");
					$Title = 'Client WOR';
					$wor_enddate = date_create($worinfo['end_date']);
					$Description = $worinfo['work_order_id']. " Expires on " . DATE_FORMAT($wor_enddate, 'd-M-Y');
					$ReceiverEmployee = $worinfo['client_partner'].','.$worinfo['engagement_manager'].','.$worinfo['delivery_manager'];
					$DeleteFlag = 0;
					$SenderEmployee = 1917;
					$ReadFlag = 0;
					$Type = 1;
					
					$notificationsql = "INSERT INTO notification
					(Title, Description, SenderEmployee, ReceiverEmployee, NotificationDate, ReadFlag, Type, DeleteFlag)
					VALUES ('$Title', '$Description', '$SenderEmployee', '$ReceiverEmployee', '$NotificationDate', $ReadFlag, $Type, $DeleteFlag)";
					
					$db->sql($notificationsql);
					
					$message	= "<span style='font-family:arial; font-size:12px;'> <b>Message : ".$Description."</b> <br/> <br/>
											Thanks<br/>
											Rhythm Support
										</span>";
					$subject 			= 'WORK ORDER : '.$Description.'';
					$email = 'ganavi.jagadeesh@theoreminc.net';
					$cc = "rhythmsupport@theoreminc.net";
					sendMail($email,$subject,$message,$cc);
				}
			}										
		}
	}
	
	function sendMail($email,$subject,$message,$cc)
	{
		$mail = new PHPMailer(true);
		$mail->IsSMTP();                       // telling the class to use SMTP
		$mail->SMTPDebug = 0;
	
		// 0 = no output, 1 = errors and messages, 2 = messages only.
		$mail->SMTPDebug = 1;
		$mail->SMTPAuth = true;                // enable SMTP authentication
		$mail->SMTPSecure = "tls";              // sets the prefix to the servier
		$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
		$mail->Port = 587;                    // set the SMTP port for the GMAIL
		$mail->Username = "rhythmsupport";  // Gmail username
		$mail->Password = "rhy_th499";      // Gmail password
		$mail->ContentType = 'text/html';
		$mail->IsHTML(true);
		$mail->CharSet = 'windows-1250';
		$mail->From = 'rhythmsupport@theoreminc.net';
		$mail->FromName = 'Rhythmsupport@theoreminc.net';
		$mail->addReplyTo('Rhythmsupport@theoreminc.net');
		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->MsgHTML($message);
		$mail->AddAddress ($email);
		$mail->AddCC($cc);
		$error_message = "";
		// you may also use this format $mail->AddAddress ($recipient);
		if(!$mail->Send())
		{
			$error_message = "Error in Sending Email Mailer Error: " . $mail->ErrorInfo;
		} else {
			$error_message = "Mail Successfully sent!"; echo '</br>';
		}
		echo $error_message;
	}
	
	
	