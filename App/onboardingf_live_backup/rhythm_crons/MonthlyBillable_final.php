<?php

//http://localhost:8082/onboarding/rhythm_crons/MonthlyBillable_final.php

ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class MothlyBillabiltyClientCron extends MysqlConnect{
	
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}
	
	
	

	function getWorID($client_id,$today)
	{
		$query = "SELECT GROUP_CONCAT(wor_id) as wor_id FROM `wor` WHERE CLIENT = '$client_id' AND `status` ='Open' AND `start_date` <='$today' GROUP BY CLIENT";
		
		$result = mysql_query($query);

		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$wor = $res['wor_id'];
				
			}
			return $wor;
		}
	}
	function getCorID($client_id,$today)
	{
		$query = "SELECT GROUP_CONCAT(cor_id) as cor_id FROM `cor` WHERE CLIENT = '$client_id' AND `status` <> 'In-Progress' AND `change_request_date` <='$today' GROUP BY CLIENT";
		
		$result = mysql_query($query);

		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$cor = $res['cor_id'];
				
			}
			return $cor;
		}
	}

	function getWorModelIds($wor_id,$primary_key,$table,$today)
	{
		$query = "SELECT GROUP_CONCAT($primary_key) AS Ids FROM $table WHERE fk_wor_id IN ($wor_id) AND fk_cor_id IS NULL AND anticipated_date <= '$today'";
		
		$result = mysql_query($query);

		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$wor_fte = $res['Ids'];
			}
			return $wor_fte;
		}
	}
	
	function getCorModelIds($cor_id,$primary_key,$table,$today)
	{
		$query = "SELECT GROUP_CONCAT($primary_key) AS Ids FROM $table WHERE fk_cor_id IN ($cor_id) AND anticipated_date <='$today'";
		$result = mysql_query($query);

		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$cor_fte = $res['Ids'];
			}
			return $cor_fte;
		}
	}
	
	function getFteCount($ids,$client_id,$today)
	{
		
		$query = "SELECT shift.time_zone AS Shift, SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS FTE ,job_responsibilities.name AS responsibilities,fte_model.responsibilities as resp_id FROM fte_model  JOIN `shift` ON shift.shift_id = fte_model.shift LEFT JOIN job_responsibilities ON job_responsibilities.job_responsibilities_id = fte_model.responsibilities WHERE fte_id IN ($ids) GROUP BY responsibilities ,shift ";
		$result = mysql_query($query);
		$myrec  = array();
		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
					$fte[] = $res;	
			}
			
			foreach($fte as $key => $value){
				
					$myrec[$key]['count'] = $value['FTE'];
					$myrec[$key]['Shift'] = "fte_".$value['Shift'];
					$myrec[$key]['resp_id'] = $value['resp_id'];
					$myrec[$key]['responsibilities'] = $value['responsibilities'];
					$myrec[$key]['client_id'] = $client_id;
					// $myrec[$key]['date'] = $today;
				
			}			
			return $myrec;
		}
	}
	
	
	function getResponsibiltyCount($ids,$client_id,$today,$model)
	{
		
		if($model == 'FTE'){
		$query = "SELECT  SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS FTE , SUM(CASE change_type WHEN 1 THEN appr_buffer WHEN 2 THEN -appr_buffer  END) AS appr_buffer ,fte_model.responsibilities as resp_id, job_responsibilities.name AS responsibilities FROM fte_model LEFT JOIN job_responsibilities ON job_responsibilities.job_responsibilities_id = fte_model.responsibilities   WHERE fte_id IN ($ids) GROUP BY responsibilities  ";
		}
		if($model == 'hourly'){
			
		$query = "SELECT SUM(CASE change_type WHEN 1 THEN max_hours  WHEN 2 THEN -max_hours END) AS max_hours , SUM(CASE change_type WHEN 1 THEN min_hours WHEN 2 THEN -min_hours END) AS min_hours  ,responsibilities As resp_id, job_responsibilities.name AS responsibilities FROM `hourly_model` LEFT JOIN job_responsibilities ON job_responsibilities.job_responsibilities_id = hourly_model.responsibilities   WHERE hourly_id IN ($ids) GROUP BY responsibilities ";
		}
		if($model == 'volume'){
			
		$query = "SELECT SUM(CASE change_type WHEN 1 THEN max_volume  WHEN 2 THEN -max_volume END) AS max_volume , SUM(CASE change_type WHEN 1 THEN min_volume WHEN 2 THEN -min_volume END) AS min_volume  ,responsibilities As resp_id, job_responsibilities.name AS responsibilities FROM `volume_based_model` LEFT JOIN job_responsibilities ON job_responsibilities.job_responsibilities_id = volume_based_model.responsibilities   WHERE vb_id IN ($ids) GROUP BY responsibilities ";
		}
		
		$result = mysql_query($query);
		$myresp  = array();
		if(mysql_num_rows($result) > 0) {
			
			while($res = mysql_fetch_assoc($result)) 
			{ 
					$resp[] = $res;	
			}
			
			foreach($resp as $key => $value){
				
				if($model == 'FTE'){
					if($value['FTE'] <= 0){
						$final_fte = 0;
					}else{
						$final_fte = $value['FTE'];
					}
					$myresp[$key]['hc'] = $final_fte;
					
					if($value['appr_buffer'] <= 0){
						$final_appr_buffer = 0;
					}else{
						$final_appr_buffer = $value['appr_buffer'];
					}
					$myresp[$key]['final_appr_buffer'] = $final_appr_buffer;
				}
				if($model == 'hourly'){
					if($value['max_hours'] <= 0){
						$final_max_hours = 0;
					}else{
						$final_max_hours = $value['max_hours'];
					}
					if($value['min_hours'] <= 0){
						$final_min_hours = 0;
					}else{
						$final_min_hours = $value['min_hours'];
					}
					$myresp[$key]['max_hours'] = $final_max_hours;
					$myresp[$key]['min_hours'] = $final_min_hours;
				}
				if($model == 'volume'){
					if($value['max_volume'] <= 0){
						$final_max_volume = 0;
					}else{
						$final_max_volume = $value['max_volume'];
					}
					if($value['min_volume'] <= 0){
						$final_min_volume = 0;
					}else{
						$final_min_volume = $value['min_volume'];
					}
					
					$myresp[$key]['max_volume'] = $final_max_volume;
					$myresp[$key]['min_volume'] = $final_min_volume;
				}
				
					$myresp[$key]['responsibilities'] = $value['responsibilities'];
					$myresp[$key]['resp_id'] = $value['resp_id'];
					$myresp[$key]['client_id'] = $client_id;
					$myresp[$key]['date'] = $today;
			}			
			
			// echo "<PRE>";print_r($myresp);
			// return ;
			if(is_array($myresp)){
 
			$DataArr = array();
			foreach($myresp as $row){
				
					$date = $row['date'];
					$client_id = $row['client_id'];
					$responsibility = $row['resp_id'];
					
					if($model == 'FTE'){
						$resp_hc = $row['hc'];
						$resp_approved_buffer = $row['final_appr_buffer'];
						$fte_ids = $ids;
					}
					
					if($model == 'hourly'){
						$max_hours = $row['max_hours'];
						$min_hours = $row['min_hours'];
					}
					if($model == 'volume'){
						$max_volume = $row['max_volume'];
						$min_volume = $row['min_volume'];
					}
					
					
					$chk_record = $this->chk_resp_record($client_id,$responsibility,$date);
					
					
					/**
					0 => record Exist
					1 => No record Exist
					*/
					if($model == 'FTE'){
						if($chk_record != 0){
							$DataArr = "('$date', '$client_id', '$resp_hc', '$responsibility', '$resp_approved_buffer', '$fte_ids')";
							$insert_record =  $this->InsertRecord($DataArr,$model);	
						}else{
							$update_record =  $this->UpdateRecord($resp_hc,'responsibility_hc',$responsibility,$client_id,$date);
							$update_record =  $this->UpdateRecord($resp_approved_buffer,'responsibility_ab',$responsibility,$client_id,$date);
						}
					}
					if($model == 'hourly'){
						if($chk_record != 0){
							$Hourly_Array = "('$date', '$client_id', '$max_hours', '$min_hours', '$responsibility')";
							$insert_record =  $this->InsertRecord($Hourly_Array,$model);	
						}else{
							$update_record =  $this->UpdateHourlyRecord($max_hours,$min_hours,'hourly_min','hourly_max',$responsibility,$client_id,$date);
						}
					}
					if($model == 'volume'){
						if($chk_record != 0){
							$Volume_Array = "('$date', '$client_id', '$max_volume', '$min_volume', '$responsibility')";
							$insert_record =  $this->InsertRecord($Volume_Array,$model);	
						}else{
							$update_record =  $this->UpdateHourlyRecord($max_volume,$min_volume,'volume_min','volume_max',$responsibility,$client_id,$date);
						}
					}
				
				}
			}
			return '';
		}
	}
	
	
	
	
	function UpdateRecord($count,$column,$resp_id,$client_id,$today)
	{
		$update_headcount = "UPDATE billability_report SET $column = $count WHERE client_id = $client_id  AND responsibility_id  =   $resp_id  AND date =  '$today' " ;
		
		
		if( mysql_query($update_headcount) )
		{
			$msg =  "Rec Updated Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  Updating";
		} 
		
		return $msg;
	}
	function UpdateHourlyRecord($max,$min,$min_column,$max_column,$resp_id,$client_id,$today)
	{
		$update_headcount = "UPDATE billability_report SET $max_column = $max ,$min_column = $min WHERE client_id = $client_id  AND responsibility_id  =   $resp_id  AND date =  '$today' " ;
		
		
		if( mysql_query($update_headcount) )
		{
			$msg =  "Rec Updated Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  Updating";
		} 
		
		return $msg;
		
	}
	
	function InsertRecord($DataArr,$model)
	{
		
		if($model == 'FTE'){
			$sql = "INSERT INTO billability_report (date, client_id, responsibility_hc ,responsibility_id,responsibility_ab,fte_id) values ";
		}
		if($model == 'hourly'){
			$sql = "INSERT INTO billability_report (date, client_id, hourly_max, hourly_min ,responsibility_id) values ";
		}
		if($model == 'volume'){
			$sql = "INSERT INTO billability_report (date, client_id, volume_max, volume_min ,responsibility_id) values ";
		}
		$sql .= $DataArr;
		
		
		 if(mysql_query($sql))
		{
			$msg =  "Rec Added Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  insertion";
		}  
		return $msg;
	}
	
	function chk_resp_record($client_id,$resp_id,$date)
	{
		$sql = "SELECT * FROM `billability_report` WHERE client_id  = $client_id AND `responsibility_id` = $resp_id AND `date` = '$date' ";
		
		$result = mysql_query($sql);
		
		if(mysql_num_rows($result) > 0) {
			 return 0;
		}else{
			return 1;
		}
				
	}
	
 	public function client_billability($today='')
	{
		$client = array();
		$fte_shift = array();
		// $today = date('Y-m-d', strtotime("2018-01-05"));//'2018-01-03';
		// echo $today;echo "<BR>";
		
		$this->DeleteExistingRecords($today);
		
		$query ="SELECT client_id,client_name FROM `client` WHERE STATUS =1 ";
		
		
		$result =  mysql_query($query) or die(mysql_error()."Query error");
		 
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$res['wor']= $this->getWorID($res['client_id'],$today);
				$res['cor']= $this->getCorID($res['client_id'],$today);
				if (!empty($res['wor'])) {
					$client[] = $res;
				}
			}
			
				
		
			foreach($client as $key => $value){
					$client[$key]['wor_fte_id'] = $this->getWorModelIds($value['wor'],'fte_id','fte_model',$today);
					$client[$key]['cor_fte_id'] = $this->getCorModelIds($value['cor'],'fte_id','fte_model',$today);
					
					$client[$key]['wor_hourly_id'] = $this->getWorModelIds($value['wor'],'hourly_id','hourly_model',$today);
					$client[$key]['cor_hourly_id'] = $this->getCorModelIds($value['cor'],'hourly_id','hourly_model',$today);
				
					$client[$key]['wor_volume_id'] = $this->getWorModelIds($value['wor'],'vb_id','volume_based_model',$today);
					$client[$key]['cor_volume_id'] = $this->getCorModelIds($value['cor'],'vb_id','volume_based_model',$today);
				
				}
		
			
			
			foreach ($client as $key => $value){
				$fte_ids = '';
				if($value['wor_fte_id']!='' && $value['cor_fte_id'] != ''){
					$fte_ids = $value['wor_fte_id'].','.$value['cor_fte_id'];
				}elseif($value['wor_fte_id'] !='' &&  $value['cor_fte_id'] == '' ){
					$fte_ids = $value['wor_fte_id'];
				}elseif($value['wor_fte_id'] =='' &&  $value['cor_fte_id'] != '' ){
					$fte_ids = $value['cor_fte_id'];
				}
				
				$client[$key]['FTEIDS'] = $fte_ids;
				
				$hourly_ids = '';
				if($value['wor_hourly_id']!='' && $value['cor_hourly_id'] != ''){
					$hourly_ids = $value['wor_hourly_id'].','.$value['cor_hourly_id'];
				}elseif($value['wor_hourly_id'] !='' &&  $value['cor_hourly_id'] == '' ){
					$hourly_ids = $value['wor_hourly_id'];
				}elseif($value['wor_hourly_id'] =='' &&  $value['cor_hourly_id'] != '' ){
					$hourly_ids = $value['cor_hourly_id'];
				}
				
				$client[$key]['HOURLYIDS'] = $hourly_ids;
				
				$volume_ids = '';
				if($value['wor_volume_id']!='' && $value['cor_volume_id'] != ''){
					$volume_ids = $value['wor_volume_id'].','.$value['cor_volume_id'];
				}elseif($value['wor_volume_id'] !='' &&  $value['cor_volume_id'] == '' ){
					$volume_ids = $value['wor_volume_id'];
				}elseif($value['wor_volume_id'] =='' &&  $value['cor_volume_id'] != '' ){
					$volume_ids = $value['cor_volume_id'];
				}
				
				$client[$key]['VOLUMEIDS'] = $volume_ids;
			}
			// echo "<PRE>";print_r($client);
			// return ''; 
			foreach ($client as $key => $value){
				if($value['FTEIDS'] != ""){
					$fte[$key] = $this->getResponsibiltyCount($value['FTEIDS'],$value['client_id'],$today,'FTE');
					$fte_shift[$key] = $this->getFteCount($value['FTEIDS'],$value['client_id'],$today);
				}
				if($value['HOURLYIDS'] != ""){
					$hourly[$key] = $this->getResponsibiltyCount($value['HOURLYIDS'],$value['client_id'],$today,'hourly');
					
				}
				if($value['VOLUMEIDS'] != ""){
					$hourly[$key] = $this->getResponsibiltyCount($value['VOLUMEIDS'],$value['client_id'],$today,'volume');
					
				}
				
			}
								
			// echo "<PRE>";print_r($client);
			// return ''; 
			foreach ($fte_shift as $key => $value){
								
			if(is_array($value)){
				foreach ($value as $k1 => $v1){
						$update_record = $this->UpdateRecord($v1['count'],$v1['Shift'],$v1['resp_id'],$v1['client_id'],$today);
					}
									
				}
			}
			echo "<PRE>";print_r($client);
			return ;
			exit;
			
		
	
	}
	
	private function  DeleteExistingRecords($today)
	{
		if(!empty($today))
		{
			$sqldel = "DELETE FROM billability_report  WHERE date = '$today'";
			$result = mysql_query($sqldel) or die(mysql_error()."Query error");
		}
	}
}

// for only one day
// $date="2017-10-16";

/*Its better to run the cron in daily basis to avoid the overwriting of records which were available in the past
*/

/* $date = date("Y-m-d");
$obj  = new MothlyBillabiltyClientCron();
$obj->client_billability($date);   */

/*
For Looping Multiple Days 
Warnings 

1. It will Look in to Current days Count and will  Overwrite all the Records available in the Past Date
	
	Scenario 1 
	If the WOR has been Closed on previous date this loop will not take that WOR in to the account
	
	Scenario 2 
	If the client has been cahnged from active to In active status it will consider the client as In active currently & it Recodrs will not be available.
	

*/
   
 
 $obj  = new MothlyBillabiltyClientCron();
$date = '2019-02-28';
$end_date = '2019-03-31';

while (strtotime($date) <= strtotime($end_date)) {
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->client_billability($date);
}    
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>

