<?php

//http://localhost:8082/onboarding/rhythm_crons/MonthlyBillable_Utilization_update.php

ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class MothlyBillabiltyClientUtilizationCron extends MysqlConnect{
	
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}
	
	
		
	function UpdateHourlyRecord($client_id,$resp_id,$total_hrs,$total_hrs_column,$appr_hrs,$appr_hrs_column,$nonappr_hrs,$nonappr_hrs_column,$weekend_support,$weekend_hrs_column,$holiday_support,$holiday_support_column,$models,$today,$creatives,$creatives_column)
	{
		if($models == "_H_" ){
			$update_headcount = "UPDATE billability_report SET $total_hrs_column = '$total_hrs' ,$appr_hrs_column = '$appr_hrs',  $nonappr_hrs_column = '$nonappr_hrs', $creatives_column = '$creatives',  $weekend_hrs_column = '$weekend_support',  $holiday_support_column = '$holiday_support' WHERE client_id = '$client_id'  AND responsibility_id  = '$resp_id'  AND date =  '$today' " ;
		}elseif($models == "_V_" ){
			$update_headcount = "UPDATE billability_report SET $total_hrs_column = '$total_hrs' ,$appr_hrs_column = '$appr_hrs',  $nonappr_hrs_column = '$nonappr_hrs' , $creatives_column = '$creatives' WHERE client_id = '$client_id'  AND responsibility_id  = '$resp_id'  AND date =  '$today' " ;
			
		}	
		
		if( mysql_query($update_headcount) )
		{
			$msg =  "Rec Updated Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  Updating";
		} 
		
		return $msg;
		
	}
	
	function chk_resp_record($client_id,$resp_id,$date)
	{
		$sql = "SELECT * FROM `billability_report` WHERE client_id  = $client_id AND `responsibility_id` = $resp_id AND `date` = '$date' ";
		
		$result = mysql_query($sql);
		
		if(mysql_num_rows($result) > 0) {
			 return 0;
		}else{
			return 1;
		}
				
	}
	
 	public function client_Utilization($today='',$models)
	{
		
		// $today = date('Y-m-d', strtotime("2018-02-05"));//'2018-01-03';
		echo $today;echo "<BR>";echo $models;echo "<BR>";
		// $models = array("%_H_%","%_V_%");
		
		// foreach($models as $model){
			$client = array();
			$query ="SELECT client_name,utilization.client_id,task_date, project_code, job_responsibilities.name,job_responsibilities.job_responsibilities_id,SUM(no_of_creatives) AS total_creatives,SEC_TO_TIME(SUM(TIME_TO_SEC(`total_hrs`))) AS Totalhrs, IFNULL(SEC_TO_TIME(SUM(CASE WHEN approved = 1 THEN TIME_TO_SEC(`total_hrs`) END)),'00:00:00') AS approved_hrs, IFNULL(SEC_TO_TIME(SUM(CASE WHEN approved =0 OR approved =2  THEN TIME_TO_SEC(`total_hrs`)  END)),'00:00:00') AS non_approved_hrs , IFNULL(SEC_TO_TIME(SUM(CASE WHEN support_type = 1 THEN TIME_TO_SEC(`total_hrs`) END)),'00:00:00') AS Total_Weekend_hrs, IFNULL(SEC_TO_TIME(SUM(CASE WHEN support_type = 2 THEN TIME_TO_SEC(`total_hrs`)   END)),'00:00:00') AS Total_Holiday_coverage_hrs, IFNULL(SEC_TO_TIME(SUM(CASE WHEN approved = 3 THEN TIME_TO_SEC(`total_hrs`) END)),'00:00:00') AS approved_Weekend_hrs, IFNULL(SEC_TO_TIME(SUM(CASE WHEN approved = 4 THEN TIME_TO_SEC(`total_hrs`) END)),'00:00:00') AS approved_HolidayCoverage_hrs FROM utilization  JOIN PROCESS ON utilization.project_code = process.name JOIN job_responsibilities ON process.job_responsibilities_id = job_responsibilities.`job_responsibilities_id` JOIN CLIENT ON  utilization.client_id = client.client_id WHERE project_code REGEXP '$models' AND task_date = '$today'  GROUP BY utilization.client_id,process.job_responsibilities_id ORDER BY client_name"; 		
		
		// echo $query;return ;
			$result =  mysql_query($query) or die(mysql_error()."Query error");
		 
			while($res = mysql_fetch_assoc($result)) 
			{ 
				$client[] = $res;
			}
			
			if (!empty($client)) 
			{ 
				foreach($client as $key => $value)
				{
					$chk_record = $this->chk_resp_record($value['client_id'],$value['job_responsibilities_id'],$today);
					
					/**
					0 => record Exist
					1 => No record Exist
					*/
					if($chk_record != 0){
						echo  "NO record for the ".$today ." for the Client ". $value['client_name'] ." Run the Monthly Billbilty corn for the day ";echo "<BR>";
						
					}else{
						  if($models == "_H_" ){
							//echo "HOURLY";echo "<BR>";
							$update_record = $this->UpdateHourlyRecord($value['client_id'],$value['job_responsibilities_id'],$value['Totalhrs'],'hourly_actual',$value['approved_hrs'],'approved_hrs',$value['non_approved_hrs'],'non_approved_hrs',$value['approved_Weekend_hrs'],'approved_Weekend_hrs',$value['approved_HolidayCoverage_hrs'],'approved_holiday_coverage',$models,$today,$value['total_creatives'],'total_creatives_hourly');
						}
						  if($models == "_V_" ){
							//echo "Volume";echo "<BR>";
							$update_record = $this->UpdateHourlyRecord($value['client_id'],$value['job_responsibilities_id'],$value['Totalhrs'],'volume_actual',$value['approved_hrs'],'approved_volume',$value['non_approved_hrs'],'non_approved_volume',0,'approved_Weekend_hrs',0,'approved_holiday_coverage',$models,$today,$value['total_creatives'],'total_creatives_volume');
						}   
						echo $update_record." FOR " .$today ; echo "<BR>";
					}
								
				}
			}
				
			echo "<PRE>";print_r($client);
			return '';
	}

}

// for only one day
// $date="2017-10-16";

/*Its better to run the cron in daily basis to avoid the overwriting of records which were available in the past
*/

/* $date = date("Y-m-d");
$obj  = new MothlyBillabiltyClientUtilizationCron();
$obj->client_Utilization($date); 
 */

   

$obj  = new MothlyBillabiltyClientUtilizationCron();
$date = '2019-01-31';
$end_date = '2019-02-28';

while (strtotime($date) <= strtotime($end_date)) {
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$models = array("_H_","_V_");
	foreach($models as $model){
	$obj->client_Utilization($date,$model);
	}
}    
  
   
/*    
$obj = new MothlyBillabiltyClientUtilizationCron();

$s_date = date('Y-m-d',date(strtotime("-6 day")));
$e_date = date("Y-m-d",strtotime("+1 day"));  

$begin = new DateTime($s_date);
$end = new DateTime($e_date);
 
$interval = DateInterval::createFromDateString('1 day');

$period = new DatePeriod($begin, $interval, $end);
     
foreach ( $period as $dt )
{
	$obj->client_Utilization($dt->format("Y-m-d"));     
}  
  
  
  
  */ 
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>

