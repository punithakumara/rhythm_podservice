<?php

//http://rhythm.theoreminc.net/onboarding/rhythm_crons/MonthlyBillableReport_FTE.php

ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class MothlyBillabiltyClientCron extends MysqlConnect{
	
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}
	

	function getWorID($client_id,$today)
	{
		$query = "SELECT GROUP_CONCAT(wor_id) as wor_id FROM `wor` WHERE client = '$client_id' AND `status` ='Open' AND `start_date` <='$today' GROUP BY CLIENT";
		
		$result = mysqli_query($this->con,$query);

		if(mysqli_num_rows($result) > 0) {
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$wor = $res['wor_id'];
			}
			return $wor;
		}
	}
	
	
	function getCorID($client_id,$today)
	{
		$query = "SELECT GROUP_CONCAT(cor_id) as cor_id FROM `cor` WHERE CLIENT = '$client_id' AND `status` <> 'In-Progress' AND `change_request_date` <='$today' GROUP BY CLIENT";
		
		$result = mysqli_query($this->con,$query);

		if(mysqli_num_rows($result) > 0) {
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$cor = $res['cor_id'];
				
			}
			return $cor;
		}
	}

	function getWorModelIds($wor_id,$primary_key,$table,$today)
	{
		$query = "SELECT GROUP_CONCAT($primary_key) AS Ids FROM $table WHERE fk_wor_id IN ($wor_id) AND fk_cor_id IS NULL AND anticipated_date <= '$today'";
		
		$result = mysqli_query($this->con,$query);

		if(mysqli_num_rows($result) > 0) {
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$wor_fte = $res['Ids'];
			}
			return $wor_fte;
		}
	}
	
	function getCorModelIds($cor_id,$primary_key,$table,$today)
	{
		$query = "SELECT GROUP_CONCAT($primary_key) AS Ids FROM $table WHERE fk_cor_id IN ($cor_id) AND anticipated_date <='$today'";
		$result = mysqli_query($this->con,$query);

		if($result) {
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$cor_fte = $res['Ids'];
			}
			return $cor_fte;
		}
	}
	
	function getFteCount($ids,$client_id,$today)
	{
		$query = "SELECT shift.time_zone AS Shift, SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS FTE ,
		role_types.role_type AS responsibilities,fte_model.responsibilities as resp_id,fk_wor_id,wor_type_id,fk_project_type_id, role_type_id AS fk_roles_id 
		FROM fte_model 
		LEFT JOIN wor ON wor.wor_id = fte_model.fk_wor_id 
		JOIN `shift` ON shift.shift_id = fte_model.shift 
		LEFT JOIN role_types ON role_types.role_type_id = fte_model.responsibilities 
		WHERE fte_id IN ($ids) GROUP BY responsibilities,shift,fk_wor_id,fk_project_type_id ";
		// echo $query; exit;
		$result = mysqli_query($this->con,$query);
		$myrec  = array();
		if(mysqli_num_rows($result) > 0) {
			
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$fte[] = $res;	
			}
			
			foreach($fte as $key => $value)
			{
				$myrec[$key]['count'] = $value['FTE'];
				$myrec[$key]['Shift'] = "fte_".$value['Shift'];
				$myrec[$key]['resp_id'] = $value['resp_id'];
				$myrec[$key]['responsibilities'] = $value['responsibilities'];
				$myrec[$key]['client_id'] = $client_id;
				$myrec[$key]['fk_wor_id'] = $value['fk_wor_id'];
				$myrec[$key]['wor_type_id'] = $value['wor_type_id'];
				$myrec[$key]['fk_project_type_id'] = $value['fk_project_type_id'];
				$myrec[$key]['fk_roles_id'] = $value['fk_roles_id'];
				// $myrec[$key]['date'] = $today;
			}			
			return $myrec;
		}
	}
	
	
	function getResponsibiltyCount($ids,$client_id,$today,$model)
	{
		if($model == 'FTE')
		{
			$query = "SELECT SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte  END) AS FTE, 
			SUM(CASE change_type WHEN 1 THEN appr_buffer WHEN 2 THEN -appr_buffer  END) AS appr_buffer,fte_model.responsibilities as resp_id, 
			role_types.role_type AS responsibilities,fk_wor_id,wor_type_id,fk_project_type_id, role_type_id AS fk_roles_id 
			FROM fte_model 
			LEFT JOIN wor ON wor.wor_id = fte_model.fk_wor_id 
			LEFT JOIN role_types ON role_types.role_type_id = fte_model.responsibilities 
			WHERE fte_id IN ($ids) GROUP BY responsibilities, fk_wor_id, fk_project_type_id ";
		}
		
		$result = mysqli_query($this->con,$query);
		$myresp  = array();
		if(mysqli_num_rows($result) > 0) 
		{	
			while($res = mysqli_fetch_assoc($result)) 
			{ 
				$resp[] = $res;
			}
			
			foreach($resp as $key => $value)
			{
				if($model == 'FTE')
				{
					if($value['FTE'] <= 0){
						$final_fte = 0;
					}else{
						$final_fte = $value['FTE'];
					}
					$myresp[$key]['hc'] = $final_fte;
					
					if($value['appr_buffer'] <= 0){
						$final_appr_buffer = 0;
					}else{
						$final_appr_buffer = $value['appr_buffer'];
					}
					$myresp[$key]['final_appr_buffer'] = $final_appr_buffer;
				}
				
				$myresp[$key]['fk_wor_id'] = $value['fk_wor_id'];
				$myresp[$key]['wor_type_id'] = $value['wor_type_id'];
				$myresp[$key]['fk_project_type_id'] = $value['fk_project_type_id'];
				$myresp[$key]['fk_roles_id'] = $value['fk_roles_id'];
				$myresp[$key]['responsibilities'] = $value['responsibilities'];
				$myresp[$key]['resp_id'] = $value['resp_id'];
				$myresp[$key]['client_id'] = $client_id;
				$myresp[$key]['date'] = $today;
			}			
			
			// echo "<PRE>";print_r($myresp); exit;
			// return ;
			if(is_array($myresp))
			{
				$DataArr = array();
				foreach($myresp as $row)
				{	
					$date = $row['date'];
					$client_id = $row['client_id'];
					$responsibility = $row['resp_id'];
					$fk_wor_id = $row['fk_wor_id'];
					$fk_type_id = $row['wor_type_id'];
					$fk_project_id = $row['fk_project_type_id'];
					
					if($model == 'FTE')
					{
						$resp_hc = $row['hc'];
						$resp_approved_buffer = $row['final_appr_buffer'];
						$fte_ids = $ids;
					}
					
					$chk_record = $this->chk_resp_record($client_id,$fk_wor_id,$fk_type_id,$fk_project_id,$responsibility,$date);
					
					/**
					0 => record Exist
					1 => No record Exist
					*/
					if($model == 'FTE')
					{
						if($chk_record != 0)
						{
							$DataArr = "('$date', '$client_id', '$fk_wor_id', '$fk_type_id', '$fk_project_id', '$responsibility', '$resp_hc', '$resp_approved_buffer')";
							$insert_record =  $this->InsertRecord($DataArr,$model);	
						}
						else
						{
							$update_record =  $this->UpdateRecord($resp_hc,'responsibility_hc',$client_id,$fk_wor_id,$fk_type_id,$fk_project_id,$responsibility,$date);
							$update_record =  $this->UpdateRecord($resp_approved_buffer,'responsibility_ab',$client_id,$fk_wor_id,$fk_type_id,$fk_project_id,$responsibility,$date);
						}
					}
					
				}
			}
			return '';
		}
	}
	
	
	function UpdateRecord($count,$column,$client_id,$fk_wor_id,$fk_type_id,$fk_project_id,$resp_id,$today)
	{
		$update_headcount = "UPDATE fte_billability_report SET $column = $count WHERE fk_client_id = $client_id AND fk_wor_id = $fk_wor_id AND fk_type_id = $fk_type_id AND fk_project_id = $fk_project_id AND fk_roles_id = $resp_id AND date = '$today' " ;
		
		if( mysqli_query($this->con,$update_headcount) )
		{
			$msg =  "Rec Updated Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  Updating";
		} 
		
		return $msg;
	}
	
	
	function InsertRecord($DataArr,$model)
	{
		if($model == 'FTE'){
			$sql = "INSERT INTO fte_billability_report (date,fk_client_id,fk_wor_id,fk_type_id,fk_project_id,fk_roles_id,fte_total_hc,fte_total_buffer) VALUES ";
		}
		$sql .= $DataArr;
		// echo $sql; exit;
		if(mysqli_query($this->con,$sql))
		{
			$msg =  "Rec Added Successfully </br></br>";
		}
		else
		{
			$msg = " Error in  insertion";
		}  
		return $msg;
	}
	
	
	function chk_resp_record($client_id,$fk_wor_id,$fk_type_id,$fk_project_id,$resp_id,$date)
	{
		$sql = "SELECT * FROM `fte_billability_report` WHERE fk_client_id = $client_id AND fk_wor_id = $fk_wor_id AND fk_type_id = $fk_type_id AND fk_project_id = $fk_project_id AND `fk_roles_id` = $resp_id AND `date` = '$date' ";
		
		$result = mysqli_query($this->con,$sql);
		
		if(mysqli_num_rows($result) > 0) 
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	
	
 	public function client_billability($today='')
	{
		$client = array();
		$fte_shift = array();
		// $today = date('Y-m-d', strtotime("2018-01-05"));//'2018-01-03';
		// echo $today;echo "<BR>";
		
		$this->DeleteExistingRecords($today);
		
		$query ="SELECT client_id,client_name FROM `client` WHERE status =1 ";
		$result =  mysqli_query($this->con,$query) or die(mysqli_error($this->con)."Query error");

		while($res = mysqli_fetch_assoc($result)) 
		{ 
			$res['wor']= $this->getWorID($res['client_id'],$today);
			$res['cor']= $this->getCorID($res['client_id'],$today);
			if (!empty($res['wor'])) {
				$client[] = $res;
			}
		}
		
		foreach($client as $key => $value)
		{
			$client[$key]['wor_fte_id'] = $this->getWorModelIds($value['wor'],'fte_id','fte_model',$today);
			$client[$key]['cor_fte_id'] = $this->getCorModelIds($value['cor'],'fte_id','fte_model',$today);
		}
		
		
		foreach ($client as $key => $value)
		{
			$fte_ids = '';
			if($value['wor_fte_id']!='' && $value['cor_fte_id'] != '')
			{
				$fte_ids = $value['wor_fte_id'].','.$value['cor_fte_id'];
			}
			else if($value['wor_fte_id'] !='' &&  $value['cor_fte_id'] == '' )
			{
				$fte_ids = $value['wor_fte_id'];
			}
			else if($value['wor_fte_id'] =='' &&  $value['cor_fte_id'] != '' )
			{
				$fte_ids = $value['cor_fte_id'];
			}
			
			$client[$key]['FTEIDS'] = $fte_ids;
		}
		// return ''; 
		foreach ($client as $key => $value)
		{
			if($value['FTEIDS'] != ""){
				$fte[$key] = $this->getResponsibiltyCount($value['FTEIDS'],$value['client_id'],$today,'FTE');
				$fte_shift[$key] = $this->getFteCount($value['FTEIDS'],$value['client_id'],$today);
			}
		}
		
		foreach ($fte_shift as $key => $value)
		{
			if(is_array($value))
			{
				foreach ($value as $k1 => $v1)
				{
					$update_record = $this->UpdateRecord($v1['count'],$v1['Shift'],$v1['client_id'],$v1['fk_wor_id'],$v1['wor_type_id'],$v1['fk_project_type_id'],$v1['resp_id'],$today);
				}
			}
		}
		echo "<pre>".$today." ".date("Y-m-d H:i:s");
		return ;
		exit;
	}
	
	
	private function  DeleteExistingRecords($today)
	{
		if(!empty($today))
		{
			$sqldel = "DELETE FROM fte_billability_report  WHERE date = '$today'";
			$result = mysqli_query($this->con,$sqldel) or die(mysqli_error($this->con)."Query error");
		}
	}
}

$obj  = new MothlyBillabiltyClientCron();
$date = date("Y-m-t", strtotime("last month"));
$end_date = date("Y-m-t");

while (strtotime($date) <= strtotime($end_date)) {
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->client_billability($date);
}    
  
error_reporting(E_ALL);
ini_set('display_errors', 1);	

?>

