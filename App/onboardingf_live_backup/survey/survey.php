<?php 
error_reporting(E_ALL & ~E_NOTICE);
session_start();
include('../employee/employee_model.php');

	$db = new Database();
	if(isset($_GET['NotificationID']))
		$notificationID = $_GET['NotificationID'];
	
	if(isset($notificationID))
	{
		$notificationDetails = getNotificationDetails($notificationID);
		if(isset($notificationDetails) && is_array($notificationDetails) && count($notificationDetails)!=0)
		{
			$survey_Link = $notificationDetails['Description'];
			$EmployID = $notificationDetails['ReceiverEmployee'];
			$survey_Link = $notificationDetails['Description'];
			$Empdetails	= getEmployeegrade($EmployID);
			$Email = $Empdetails['email'];
			$grades = $Empdetails['grades'];
			$linkurl = parse_url($survey_Link);
			$path = explode('/', $linkurl['path']);
			$table = "lime_tokens_".$path[5];
			$token = $path[7];
			$ReadFlag = $notificationDetails['ReadFlag'];
			//$currentReporters = "<option value=''>-- Select Current Supervisor --</option>";
			//$PreviousReporters = "<option value=''>-- Select Previous Supervisor --</option>";
			$result_array = getCurrentSupervisors($grades, $EmployID);
			$prev_supervisor_array = getPreviousSupervisors($grades, $EmployID);
			$getReportingManagerArray = getReportingManager($EmployID);
						
			$flag=0;
			$mflag =0;
			
			if(is_array($getReportingManagerArray) && count($getReportingManagerArray)!=0)
			{
				foreach($getReportingManagerArray as $mgrkey => $mgrVal)
				{
					if($mgrVal != '')
					{
						$currentManager .= "<option value='".$mgrVal['Email']."'>".$mgrVal['Name']."</option>";	
						$mflag++;					
					}
				}
			}
			
			if(is_array($result_array) && count($result_array)!=0)
			{
				foreach($result_array as $key => $res)
				{
					$currentReporters .= "<option value='".$res['Email']."'>".$res['Name']."</option>";
				}
			}
			
			if(is_array($prev_supervisor_array) && count($prev_supervisor_array)!=0)
			{
				foreach($prev_supervisor_array as $key => $value)
				{
					if($value != '')
					{
						$PreviousReporters .= "<option value='".$value['Email']."'>".$value['Name']."</option>";
						$flag++;
					}					
				}
			}
			
		}
	}	

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Theorem Survey</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/jquery.validate.css" />
    <script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
    <script src="../js/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/script.js"></script>
	
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
</div>
<div id="container">
<!--<h1>Survey Form</h1>-->
<?php if(isset($notificationDetails) && is_array($notificationDetails) && count($notificationDetails)!=0){?>
<?php 

	if($flag == 0){
		header("Location: ".$survey_Link);
	}
	else
	{
?>
<form class="theorem" action="survey_success.php" method="POST">
<table class="formTable">
	<tr>
		<td colspan="2">
			<h1>Survey Details</h1>
		</td>
	</tr>
	<tr>
		<td>
			<table class="formTable" style="width:400px !important;">
				<tr>
					<td>
						<label class="desc">Survey Link:</label>
						
						<label class="desc"><?//php echo $survey_Link;?></label>
					</td>
				</tr>
				<?php if(is_array($getReportingManagerArray) && count($getReportingManagerArray)!=0){?>				
				<tr>
				<td>
					<label class="desc">Current Manager:</label>
					<select id="currentManager" tabindex="1" name="currentManager" style="background: lightgrey">
						<?php echo $currentManager;?>
					</select>
				</td>
				</tr>
				<?php } ?>
				<tr>
					<td>												
						<input type="hidden" name="EmployID" id="EmployID" value="<?php echo $EmployID;?>">
						<input type="hidden" name="Email" id="$Email" value="<?php echo $Email;?>">
						<input type="hidden" name="grades" id="grades" value="<?php echo $grades;?>">
						<input type="hidden" name="survey_Link" id="survey_Link" value="<?php echo $survey_Link;?>">
						<input type="hidden" name="table" id="table" value="<?php echo $table;?>">
						<input type="hidden" name="token" id="token" value="<?php echo $token;?>">
						<label class="desc">Current Supervisor:</label>
						<select id="currentReporter" tabindex="1" name="currentReporter" style="background: lightgrey">
							<?php echo $currentReporters;?>
						</select>						
						
						
					</td>
				</tr>
				<?php if(is_array($prev_supervisor_array) && count($prev_supervisor_array)!=0){?>				
				<tr>
				<td>
					<label class="desc">Previous Supervisor:</label>
					<select id="previousReporter" tabindex="1" name="previousReporter" style="background: lightgrey">
						<?php echo $PreviousReporters;?>
					</select>
				</td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<input id="saveForm" class="btTxt" type="submit" tabindex="2" value="Take Survey" />
		</td>
	</tr>
</table>
</form>
<?php } }else{?>
	<table class="formTable">
		<tr>
			<td colspan="2">
				<h1>Survey Details</h1>
			</td>
		</tr>
		<tr>
			<td>
				<table class="formTable" style="width:600px !important;">
					<tr>
						<td>
							<label class="desc">Invaid Notification / The survey token you have provided is either not valid, or has already been used.</label>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php }?>

</div>
<div class="Footer">&copy; 2015-16 Theorem</div>
</body>
</html>