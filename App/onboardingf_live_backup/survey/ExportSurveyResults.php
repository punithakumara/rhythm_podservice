<?php

	ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
	ini_set('memory_limit', '-1');
	
	require ('../config/mysql_crud.php');
	require ("../config/excelwriter.class.php");
	
	$db = new Database();
	$data = '';
	
	$excel=new ExcelWriter("Survey Results.xls");
	if($excel==false)	
	$data = $excel->error;
	
	$surveyID = $_GET['SurveyID'];
	
	$mysqli = mysqli_connect(SURVEY_HOST,SURVEY_NAME,SURVEY_PASSWORD,SURVEY_DATABASE);
	// Check connection
	if (mysqli_connect_errno())
	{
		$data = "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$surveysql = "select * from lime_survey_".$surveyID;
	$result = $mysqli->query($surveysql);
	
	if ($result->num_rows > 0) 
	{
	    while($row = $result->fetch_assoc()) 
	    {
	    	
	    	$fields = array_keys($row);
	    	
	    	for($i=5; $i<=47; $i++)
	    	{
	    		$fi = $fields[$i];
	    		$fi = explode('X',$fi);
	    		$fieldValue = $fi[2];
	    		
	    		$rest_other = strstr($fieldValue, 'other');
	    		
	    		if($rest_other == 'other')
	    		{
		    		$rest_qid_arr = explode('other', $fieldValue, -2);
		    		foreach($rest_qid_arr as $re)
		    		{
		    			$rest_qid = $re[0];
		    		}
	    		}
	    		else
	    		{
	    			$rest_qid = $fieldValue;
	    		}
	    		
	    		if($rest_qid != '')
	    		{
		    		$questionSql = "select question from lime_questions where qid=".$rest_qid;
					$questionResult = $mysqli->query($questionSql);
					$row = $questionResult->fetch_assoc();
					
		    		$fields[$i] = $row['question'];
					
		    		if($rest_other == 'other')$fields[$i] = $row['question']." [Other]";
	    		}
	    	}
	    	$fields[48] = 'First name';
	    	$fields[49] = 'Last name';
	    	$fields[50] = 'Email address';
	    	$fields[51] = 'Manager';
	    	
	    	 break;
	    }
	    
	    $excel->writeLine($fields);
	    
	    $finalResults['Questions'] = $fields;
	    
		while($row = $result->fetch_assoc()) 
	    {
	    	$finalResults['Answers_'.$row['id']] = $row;
	    	
	    	$fieldssql = mysql_list_fields('survey', 'lime_tokens_'.$surveyID);
			$columns = mysql_num_fields($fieldssql);
	    	for ($i = 0; $i < $columns; $i++) {$field_array[] = mysql_field_name($fieldssql, $i);}
	    	
	    	if (!in_array('attribute_1', $field_array))
	    	$answerSql = "select firstname, lastname, email from lime_tokens_".$surveyID. " where token='".$row['token']."'";
			else
	    	$answerSql = "select firstname, lastname, email, attribute_1 from lime_tokens_".$surveyID. " where token='".$row['token']."'";
	    	
	    	$answerResult = $mysqli->query($answerSql);
			$answerRow = $answerResult->fetch_assoc();
	    		
	    	$finalResults['Answers_'.$row['id']]['firstname'] = $answerRow['firstname'];
	    	$finalResults['Answers_'.$row['id']]['lastname'] = $answerRow['lastname'];
	    	$finalResults['Answers_'.$row['id']]['email'] = $answerRow['email'];
	    	$finalResults['Answers_'.$row['id']]['attribute_1'] = $answerRow['attribute_1'];
	    	
	    	$excel->writeLine($finalResults['Answers_'.$row['id']]);
	    }
	    
		$data = "Survey results exported successfully.";
	} 
	else 
	{
	    $data = "0 results";
	}
	
	$mysqli->close();
	
	echo $data;exit;