<?php
require_once('../config/config.php');
session_start() ;
$post_user = isset($_REQUEST["username"])?$_REQUEST["username"]:null;
$post_pass = isset($_REQUEST["password"])?$_REQUEST["password"]:null; //
$post_project_type = isset($_REQUEST["projectType"])?$_REQUEST["projectType"]:null;
if(isset($_POST['username']) && isset($_POST['password'])){
	$authData = authenticate($_POST['username'],$_POST['password']);	
	if(is_array($authData)){	
		$_SESSION['empId'] = $authData["EmpId"];
		$_SESSION['authname']  = $authData["Name"];
		$_SESSION['authemail'] = $authData['Email'];		
		if($authData['Email'] == ADMIN_MAIL){
			$_SESSION['admin'] = 1;		
		}
		checkUserList($authData['Email'], $post_project_type);		
    } 
	else {	
		header( "Location:login.php?r=1");     
    }
}   
else{    
	header( "Location:login.php?r=1");
}

function authenticate($uname,$pass){
	$result =FALSE;	
	$config['host_name'] = LDAP_HOSTNAME;
	$config['ldap_base_dn'] = LDAP_BASE_DN;
	$config['domain_name'] = LDAP_DOMAINNAME;
	$config['attributes'] = array('givenname','mail','samaccountname','description');
	$ldap = ldap_connect($config['host_name']);
    $username = trim($uname);
    $password = trim($pass);
	$checkDn = "$username".$config['domain_name'];
	//$ldaprdn = 'mydomain' . "\\" . $username;
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
	$empid = 0;
    $bind = @ldap_bind($ldap, $checkDn, $password);
	if ($bind) {
		$filter="(sAMAccountName=$username)";
        $result = ldap_search($ldap,$config['ldap_base_dn'],$filter,$config['attributes']);
		//,$filter,$config['attributes']);		
        ldap_sort($ldap,$result,"sn");
        $entries = ldap_get_entries($ldap, $result);		
	
		for($x=0; $x<$entries['count']; $x++)
		 {
			if(!empty($entries[$x]['givenname'][0]))
				$name = trim($entries[$x]['givenname'][0]);
			if(!empty($entries[$x]['samaccountname'][0]))	
				$loginid =  trim($entries[$x]['samaccountname'][0]);
			if(!empty($entries[$x]['mail'][0]))       		
				$email = trim($entries[$x]['mail'][0]);   
			if(!empty($entries[$x]['description'][0]))       		
				$empid = trim($entries[$x]['description'][0]); 
			$users =  array( 'Name'=>$name,"LoginID"=>$loginid,"Email"=>$email,"EmpId"=>$empid);
		 }		
		$result = ($x==0) ? false : $users;
		@ldap_close($ldap);		
	} 	
	return $result;
}

function checkUserList($email, $post_project_type){
 	$result = 'login.php?r=2';
	global $HRpeopleArray;
	global $ManagerArray;
	global $FinanaceArray;
		
	if($email){
		
		if ($post_project_type == 'issues')
		{
			$result = '../issues/listIssues.php';
		}
				
		/*if(in_array($email, $HRpeopleArray)){
		    $result = '../innovation/listInnovation.php';
		}
		else if(in_array($email, $ManagerArray)){
			$result = '../issues/listIssues.php';				
		}
		else if(in_array($email, $FinanaceArray)){
			$result = '../employee/listempUAN.php';				
		}		
		else {
			$result = '../employee/empUAN.php';				
		}*/
	}	
	header( "Location:".$result);  
}
?>