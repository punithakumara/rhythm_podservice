<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes

// http://localhost:8082/onboarding/script/update_reporting_lead.php

class Leads
{
	function __construct() {
		$this->con = mysqli_connect('rhythmdb.ctzwque3zyis.us-east-1.rds.amazonaws.com','root_admin','Rhythm_Aw5!!') or die("Unable to connect to MySQL");
		mysqli_select_db($this->con, 'rhythm_neiuiuyw');
		$this->path = "hcm_files/leadChanges/";
		$this->requestedBy = 1534;
    }
	
	function updateHistory($compempid, $logData)
	{
		if($compempid && $logData)
		{
			$updatedby = $this->getEmployeeId($this->requestedBy);
			$employ = $this->getEmployeeId($compempid);
		
			$sql = "INSERT INTO `employee_transition`(employee_id,from_reporting,to_reporting,transition_date,comment,modified_by) 
			VALUES(".$employ['employee_id'].",'".$logData['from_reporting']."','".$logData['to_reporting']."','".$logData['transition_date']."','Supervisor Changed',".$updatedby['employee_id'].")";
			// echo "<br>Employees Transition Table: ".$sql;
			$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
		}
	}
	
	
	function getEmployeeId($employeeId)
	{
		$result = null; 
		if($employeeId)
		{
			$qsql = "SELECT DISTINCT employees.employee_id FROM `employees` WHERE company_employ_id = '".trim($employeeId)."'";
			$query = mysqli_query($this->con, $qsql) or die(mysqli_error($this->con));
			
			$result = mysqli_fetch_assoc($query);
		}
		return $result;	
	}
	
	
	function updateLead($filename)
	{
		// path where your CSV file is located		
		$csv_file = $this->path.$filename; 
		
		if (($handle = fopen($csv_file, "r")) !== FALSE) 
		{
			fgetcsv($handle);   		
			$recordid = "";
			/*Rowid Employee id Old Grade Old Designation New Grade New Designation Date Effective*/
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				$num = count($data);				
				for ($c=0; $c < $num; $c++) 
				{
					$col[$c] = $data[$c];
				}				
				$recordid = $col[0];
				$CompanyEmployID  = $col[1];
				$employee = $this->getEmployeeId($col[1]);
				$oldSupervisor = $this->getEmployeeId($col[2]);
				$newSupervisor = $this->getEmployeeId($col[3]);
				$dateEffective = date('Y-m-d', strtotime($col[4]));
				
				$empSql = "UPDATE employees SET primary_lead_id = ".$newSupervisor['employee_id']." WHERE company_employ_id = '".$CompanyEmployID."'";
				$query = mysqli_query($this->con,$empSql) or die(mysqli_error($this->con));
				//echo "Employees Table: ".$empSql;
				$empClientSql = "UPDATE employee_client SET primary_lead_id = ".$newSupervisor['employee_id']." WHERE employee_id = '".$employee['employee_id']."'";
				$query = mysqli_query($this->con,$empClientSql) or die(mysqli_error($this->con));
				//echo "<br>Employees Client Table: ".$empClientSql; exit;
				$logArray = array(
					'from_reporting' => $oldSupervisor['employee_id'],
					'to_reporting' => $newSupervisor['employee_id'],			
					'transition_date' => $dateEffective,			
				);
				$this->updateHistory($CompanyEmployID, $logArray);
				echo "Updated Records id : $recordid <br>";
			}
			fclose($handle);
		}
	}
}

$obj = new Leads();
$obj->updateLead('LeadChange_29Aug2018-jannet.csv');
?>