<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';
class HCMFileExtractCron extends MysqlConnect{

	public $CurrentMonthYear;
	public $CurrentDate;
	public $CurrentYear;
	public $CurrentDay;
	public $path;
	
	function __construct() {
	   parent::__construct("final");
       $this->CurrentMonth = date('m');
       $this->CurrentYear = date('Y');
       $this->CurrentMonthYear = date('Y-m');
	   $this->CurrentDay = date('Y-m-d');
	   $this->path = "rhythm_files/";
	   
       /*$this->CurrentDay = '2015-08-25';
       $this->CurrentYear = '2015';
       $this->CurrentMonthYear = '2015-08';
	   $this->path = ".";*/
    }
	
	private function _getShiftchangeRecord($changeDate)
	{ 
		//Rowid	Employee id	Old Shift	New Shift	Record Change DateTime
		$header_array = array("Rowid","Employee id","Old Shift","New Shift","Record Change DateTime");
		$data = array();
		$sql = "SELECT h.id AS Rowid,e.CompanyEmployID AS 'Employee id',os.ShiftCode AS 'Old Shift',ns.ShiftCode AS 'New Shift',	
				DATE_FORMAT(h.date, '%m/%d/%Y') AS 'Record Change DateTime'
				FROM `hcm_table` h 
				JOIN `employ` e on e.employid = h.employid  
				JOIN  `shift`  os ON h.oldvalue = os.shiftid
				JOIN  `shift`  ns ON h.newvalue = ns.shiftid
				WHERE e.relieveflag = 0 AND TYPE = 'Shift' AND H.DATE LIKE '".$changeDate."%' ORDER BY h.id ";	
		//print $sql;
		$query = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($query) == 0) 
			return FALSE;
		else
		{
			$data = array();
			for($i = 0; $i < mysql_num_rows($query); $i++){
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x])){
						if(mysql_num_rows($query) >= 1){
							$data[$i][$key[$x]] = $r[$key[$x]];
						}else if(mysql_num_rows($query) < 1){
							$data = null;
						}
					}
				}
			}			
		}
		$result = array_merge(array($header_array), $data);
		return array($result);
	}
	
	private function _getVerticalchangeRecord($changeDate)
	{ 
		//Row id	Employee id	old vertical	new vertical	record change datetime
		$header_array = array("Row id","Employee id","old vertical","new vertical","record change datetime");
		$data = array();
		$sql = "SELECT h.id AS Rowid,e.CompanyEmployID AS 'Employee id',ov.name AS 'OLD vertical',nv.name AS 'NEW vertical',	
				DATE_FORMAT(h.date, '%m/%d/%Y') AS 'Record CHANGE DATETIME'
				FROM `hcm_table` h 
				JOIN `employ` e on e.employid = h.employid  
				JOIN  `vertical`  ov ON h.oldvalue = ov.verticalid
				JOIN  `vertical`  nv ON h.newvalue = nv.verticalid
				WHERE e.relieveflag = 0 AND TYPE = 'Vertical' AND H.DATE LIKE '".$changeDate."%' ORDER BY h.id";						
		//print $sql;
		
		$query = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($query) == 0) 
			return FALSE;
		else
		{
			$data = array();
			for($i = 0; $i < mysql_num_rows($query); $i++){
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x])){
						if(mysql_num_rows($query) >= 1){
							$data[$i][$key[$x]] = $r[$key[$x]];
						}else if(mysql_num_rows($query) < 1){
							$data = null;
						}
					}
				}
			}			
		}
		$result = array_merge(array($header_array), $data);
		return array($result);
	}
	
	private function _getDesignationchangeRecord($changeDate)
	{	
		$header_array = array("Rowid","Employee id","OLD Designation","NEW Designation","Record CHANGE DATETIME");
		$data = array();
		$sql = "SELECT h.id AS Rowid, e.CompanyEmployID AS 'Employee id',od.name AS 'OLD Designation',nd.name AS 'NEW Designation',	
				DATE_FORMAT(h.date, '%m/%d/%Y') AS 'Record CHANGE DATETIME'
				FROM `hcm_table` h 
				JOIN `employ` e on e.employid = h.employid  
				JOIN  `designation`  od ON h.oldvalue = od.designationid
				JOIN  `designation`  nd ON h.newvalue = nd.designationid
				WHERE e.relieveflag = 0 AND TYPE = 'Designation' AND H.DATE LIKE '".$changeDate."%' ORDER BY h.id";						
		//print $sql;
		
		$query = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($query) == 0) 
			return FALSE;
		else
		{
			$data = array();
			for($i = 0; $i < mysql_num_rows($query); $i++){
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x])){
						if(mysql_num_rows($query) >= 1){
							$data[$i][$key[$x]] = $r[$key[$x]];
						}else if(mysql_num_rows($query) < 1){
							$data = null;
						}
					}
				}
			}			
		}
		$result = array_merge(array($header_array), $data);
		return array($result);	
	}
	
	private function _getSupervisorchangeRecord($changeDate)
	{	
		//Rowid	Employee id	Old supervisor id	New supervisor id	Record Change DateTime
		$header_array = array("Rowid","Employee id","Old supervisor id","New supervisor id","Record Change DateTime");
		$data = array();
		$sql = "SELECT h.id AS Rowid,e.CompanyEmployID AS 'Employee id',
				SUBSTRING_INDEX(GROUP_CONCAT(os.CompanyEmployID ),',',-1) AS 'Old supervisor id',
				SUBSTRING_INDEX(GROUP_CONCAT(ns.CompanyEmployID),',',-1) AS 'New supervisor id',
				DATE_FORMAT(h.date, '%m/%d/%Y') AS 'Record Change DateTime'
				FROM `hcm_table` h 
				JOIN `employ` e on e.employid = h.employid 
				LEFT JOIN  `employ`  os ON h.oldvalue = os.employid
				LEFT JOIN  `employ`  ns ON h.newvalue = ns.employid
				WHERE e.relieveflag = 0 AND TYPE = 'Supervisor' AND H.DATE LIKE '".$changeDate."%' 
				GROUP BY e.employid
				ORDER BY h.id";						
		//print $sql;
		
		$query = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($query) == 0) 
			return FALSE;
		else
		{
			$data = array();
			for($i = 0; $i < mysql_num_rows($query); $i++){
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x])){
						if(mysql_num_rows($query) >= 1){
							$data[$i][$key[$x]] = $r[$key[$x]];
						}else if(mysql_num_rows($query) < 1){
							$data = null;
						}
					}
				}
			}			
		}
		$result = array_merge(array($header_array), $data);
		return array($result);	
	}
	
	function downloadFile($type,$filename,$changeDate){
		$filecontent = '';
		if($type == 'vertical')
			$filecontent = $this->_getVerticalchangeRecord($changeDate);
		else if($type == 'shift')
			$filecontent = $this->_getShiftchangeRecord($changeDate);
		else if($type == 'designation')	
			$filecontent = $this->_getDesignationchangeRecord($changeDate);
		else if($type == 'supervisor')
			$filecontent = $this->_getSupervisorchangeRecord($changeDate);
		else
			print 'File type mismatch';
		
		if($filecontent && $filename)
		{
			ob_start();
			if(file_exists($this->path.$filename)){
				//check if file is already exist				
				chmod($this->path.$filename, 0777);
				unlink ($this->path.$filename); 
			}
			
	        $f = fopen($this->path.$filename, 'w') or die("Can't open file://".$path.$filename);
	        $n = 0;        
	        foreach ($filecontent[0] as $line)
	        {
				print_r($line);
	            $n++;
	            if ( ! fputcsv($f, $line))
	            {
	                die("Can't write line $n: $line");
	            }
	        }
	        fclose($f) or die("Can't close php://output");
	        $str = ob_get_contents();
	        ob_end_clean();
			
			$this->uploadFile($filename);
		}
	     return true;   
	}
	
	function uploadFile($filename)
	{
		echo "in function\n";
		/* Make sure drive is mapped to directory */ 
		$letter='Q'; 
		$drive= $letter.':\\'; 
		
		/* Create COM object */ 
		$WshNetwork = new COM("WScript.Network"); 
		if(is_dir($drive)) 
		{     echo "here\n";
			/* Drive is currently mapped, do nothing */ 
		} 
		else 
		{     echo "now here\n";
			/* Map the drive */ 
			$WshNetwork->MapNetworkDrive( "$letter:" , '\\\hcm\\Interface\\Import' , FALSE, 'analytics\\hcm.rhythm' , 'TheoHCM33!' ) ;//or die("unable to connect"); 
		} 
		echo "reading\n";
		$path = '//hcm/Interface/Import/'; 
		copy($this->path.$filename, $path.$filename);
		echo $filename . " has been copied to HCM";
		/*$path = 'Q:\\';   */ 

	}
	
}

$shiftfilename = 'Shift_Change_'.date('mdY').'.csv';
$verticalfilename = 'Vertical_Movement_'.date('mdY').'.csv';
//$designationfilename = 'Designation_Change_'.date('mdY').'.csv';
$supervisorfilename = 'Manager_Change_'.date('mdY').'.csv';

$obj = new HCMFileExtractCron();
$obj->downloadFile('vertical',$verticalfilename,$obj->CurrentDay);
$obj->downloadFile('shift',$shiftfilename,$obj->CurrentDay);
//$obj->downloadFile('designation',$designationfilename,$obj->CurrentDay);
$obj->downloadFile('supervisor',$supervisorfilename,$obj->CurrentDay);

print "\nHCM files are uploaded to Server \n";

?>


