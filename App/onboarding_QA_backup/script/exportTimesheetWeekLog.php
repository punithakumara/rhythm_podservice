<?php
//http://localhost:8082/onboarding/script/exportTimesheetWeekLog.php

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportTimesheet extends MysqlConnect
{
	function exportTime()
	{
		$filename = "Timesheet.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}
	
	function downloadCsv() 
	{
		$index =0;
		$data = array();
		
		$sql = "SELECT employees.employee_id, employees.company_employ_id, 
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
		designation.name AS designation, hcm_grade AS grade, verticals.name AS Vertical,
		CONCAT(IFNULL(sup.first_name, '') ,' ', IFNULL(sup.last_name,'')) AS SupervisorName,
		day1,comment1,day2,comment2,day3,comment3,day4,comment4,day5,comment5,day6,comment6,day7,comment7,
		CASE timesheet_task_mapping.status
		   WHEN '0' THEN 'Rejected'
		   WHEN '1' THEN 'Pending'
		   WHEN '2' THEN 'Approved'
		END AS Status,
		timesheet_task_mapping.created_on, timesheet_task_mapping.approved_on, timesheet_task_mapping.updated_on
		FROM employees 
		LEFT JOIN designation on designation.designation_id = employees.designation_id
		LEFT JOIN employees sup ON sup.employee_id = employees.primary_lead_id 
		LEFT JOIN verticals on verticals.vertical_id = employees.assigned_vertical 
		LEFT JOIN timesheet_task_mapping on timesheet_task_mapping.emp_id = employees.employee_id
		WHERE employees.status!=6 AND employees.location_id!=4 AND task_week_id=58 AND employees.assigned_vertical=3";		
		// echo $sql; exit;
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{	
			$data[$index]['Vertical'] 		= $val['Vertical'];
			$data[$index]['Employee ID'] 	= $val['company_employ_id'];
			$data[$index]['Name'] 			= $val['FullName'];
			$data[$index]['Grade'] 			= $val['grade'];
			$data[$index]['Designation'] 	= $val['designation'];
			$data[$index]['Supervisor'] 	= $val['SupervisorName'];
			$data[$index]['Mon'] 			= $val['day1'];
			$data[$index]['comment1'] 		= '"'.$val['comment1'].'"';
			$data[$index]['Tue'] 			= $val['day2'];
			$data[$index]['comment2'] 		= '"'.$val['comment2'].'"';
			$data[$index]['Wed'] 			= $val['day3'];
			$data[$index]['comment3'] 		= '"'.$val['comment3'].'"';
			$data[$index]['Thu'] 			= $val['day4'];
			$data[$index]['comment4'] 		= '"'.$val['comment4'].'"';
			$data[$index]['Fri'] 			= $val['day5'];
			$data[$index]['comment5'] 		= '"'.$val['comment5'].'"';
			$data[$index]['Sat'] 			= $val['day6'];
			$data[$index]['comment6'] 		= '"'.$val['comment6'].'"';
			$data[$index]['Sun'] 			= $val['day7'];
			$data[$index]['comment7'] 		= '"'.$val['comment7'].'"';
			$data[$index]['Status'] 		= $val['Status'];
			$data[$index]['Created Date'] 	= $val['created_on'];
			$data[$index]['Updated Date'] 	= $val['updated_on'];
			$data[$index]['Approved Date'] 	= $val['approved_on'];
			
			$index++;
		}
		
		$header = array_keys($data[0]);
		$result_result = array_merge(array(array_unique($header)), $data);
		return array($result_result);
	}
	
}

$obj = new exportTimesheet();
$obj->exportTime();
?>