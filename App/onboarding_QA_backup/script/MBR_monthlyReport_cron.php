<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
//ini_set('xdebug.max_nesting_level', 200); 
require_once 'db_config.php';

class MbrMonthlyReportCron extends MysqlConnect{

	public function cron(){
		 $lastdayofmonth = date("Y-m-t");
		 if(strtotime(date('Y-m-d')) == strtotime($lastdayofmonth)){
			?>
			<script src="../AMProcessStats/jquery.js"></script>
			<div></div>
			<script>
			$.ajax({
				  url: "http://" + window.location.host + "/services/index.php/monthlyBillabilityReport/DownloadReportCsv",
				  data:{'Report':1,'SendMail':1},
				  success: function(data) { 			 
					  document.write("<?php echo "Report Sent Successfully"; ?>"); 			  		    
				  }
				});
			</script>		
		<?php 
	
		 }else{
		 	echo "Report will be sent on last day of the month";
		 }
	}
}
$obj  = new MbrMonthlyReportCron();
$obj->cron();
