<?php
//http://localhost:8082/onboarding/script/exportUtilizationWeekLog.php

// error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportUtilizationWeek extends MysqlConnect
{
	function exportUtil()
	{
		$filename = "Utilization.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}
	
	function downloadCsv() 
	{
		$data = array();
		$return_array = array();
		
		$sql = "SELECT employees.company_employ_id As Employee_id, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `Full_name`, utilization.utilization_id , DATE_FORMAT(utilization.task_date,'%d-%b-%Y') as task_date,client.client_name As Client , wor.work_order_id  ,utilization.client_id ,utilization.project_code , task_codes.task_code AS task_code ,utilization.sub_task ,utilization.brand ,request_region.reg_name As request_region ,DATE_FORMAT(utilization.request_date,'%d-%b-%Y') as request_date ,  DATE_FORMAT(utilization.request_start_date,'%d-%b-%Y') as request_start_date , DATE_FORMAT(utilization.request_complete_date,'%d-%b-%Y') as request_complete_date , utilization.request_name ,utilization.no_of_campaigns ,utilization.no_of_placement ,utilization.no_of_creatives ,utilization.total_hrs as client_hours, utilization.actual_hrs as actual_hours, remarks,utilization.support_type ,utilization.approved FROM  `utilization`
		LEFT JOIN `employees` ON `utilization`.`employee_id` = `employees`.`employee_id`
		LEFT JOIN `client` ON `client`.`client_id` = `utilization`.`client_id`
		LEFT JOIN `wor` ON utilization.wor_id = `wor`.`wor_id` 
		LEFT JOIN request_region  ON request_region.reg_id = utilization.request_region
		LEFT JOIN task_codes  ON task_codes.task_id = utilization.task_code
		WHERE utilization.client_id='222' AND utilization.employee_id =1143 ";

		$sql .= " AND utilization.task_date BETWEEN '2019-06-01' AND '2019-06-01'";

		echo $sql; exit;

		$query = mysql_query($sql);

		$client_attributes = $this->Client_Attriutes(222);
		$client_attributes = array_keys($client_attributes);
		foreach($client_attributes as $key => $value)
		{
			$client_attrib[$value] = "";
		}

		$result = mysql_fetch_assoc($query);
		
		$project= array();
		$task_code_array = array();

		while($value = mysql_fetch_array($query))
		{
			$additional_attributes = $this->GetAttributesValues($value['client_id'] , $value['utilization_id'],'export');
			if (empty($additional_attributes)) 
			{
				$result[$key]['AdditionalAttributes'] =$client_attrib;
			}
			else
			{
				$result[$key]['AdditionalAttributes'] =$additional_attributes;
			}

			/*Regex added to avoid Unwanted sting passing in the Query*/				
			$regex="/^[0-9,]+$/";
			if(!preg_match($regex,$value['sub_task']))
			{
				$sub_task = "";
			}
			else
			{
				$sub_task = $value['sub_task'];
			}

			$result[$key]['sub_task']	= $this->getSubtaskNames($sub_task);
			$task_code_array = $this->get_task_detils($value['task_code']);
			$project = $this->get_pro_detils($value['project_code']);
		}

		foreach($result as $k => $v)
		{
			foreach($v as $k1 => $v1)
			{
				if($k1 == "AdditionalAttributes"){
					foreach($v1 as $k12 => $v12)
					{
						$result[$k][$k12] = $v12;
					}
				}
			}
		}	
		
		foreach($result as $key => $value)
		{
			if($value['support_type'] == 1){
				$sc = "Weekend Support";
			}
			if($value['support_type'] == 2){
				$sc = "Holiday Coverage";
			}
			if($value['approved'] == 0){
				$v1 = "Pending";
			}
			if($value['approved'] ==1){
				$v1 = "Approved";
			}
			if($value['approved'] ==2){
				$v1 = "Rejected";
			}
		// $result[$key]['Support Coverage'] = $sc; 
			$result[$key]['Task Status'] = $v1; 

			unset($result[$key]['AdditionalAttributes'] );
			unset($result[$key]['utilization_id'] );
			unset($result[$key]['client_id'] );
			unset($result[$key]['approved'] );
			unset($result[$key]['support_type'] );
		}

		foreach($result as $key => $value)
		{
			$return_array = array_keys($value);
			$return_array = str_replace("_"," ",$return_array);	
			$return_array = array_map('ucfirst', $return_array);
		}	
		
		$result_result = array_merge(array(array_unique($return_array)), $result);
		return array($result_result);
	}
	
	function Client_Attriutes($clientID)
	{
		/* added to display in order of Process Attributes*/
		$sqlattri = "SELECT label_name,field_name FROM client_attributes  LEFT JOIN new_attributes ON new_attributes.attributes_id = client_attributes.attributes_id WHERE client_id = ".$clientID;
		$attrires = mysql_query($sqlattri);
		$attribuites = array();
		while($attri = mysql_fetch_array($attrires))
		{
			$attribuites[$attri['field_name']] = $attri['label_name'];

		}

		return $attribuites;
	}
		
	function GetAttributesValues($client_id,$uitlization_id,$type) 
	{
		$sqlattri = "SELECT utilization_client_attributes.attributes_id, label_name, field_name, attributes_value FROM utilization_client_attributes JOIN new_attributes ON new_attributes.attributes_id = utilization_client_attributes.attributes_id  WHERE utilization_id  =".$uitlization_id;
		$attrires = mysql_query($sqlattri);
		$attribuites = array();
		while($attri = mysql_fetch_array($attrires))
		{
			if($type == "export")
			{
				$attribuites[$attri['field_name']] = $attri['attributes_value'];
			}
			else
			{	
				$attribuites["addtional_".$attri['attributes_id']] = $attri['attributes_value'];
			}
		}

		return $attribuites;
	}
	
	function getSubtaskNames($SubArr)
	{
		$SubtaskNameArr = str_replace(",", "','", $SubArr);
		if($SubArr != '')
		{
			$empRst = mysql_query("SELECT GROUP_CONCAT(sub_tasks.`task_name`) AS sub_tasks FROM sub_tasks 	WHERE sub_task_id IN ($SubArr)");
			$SubtaskNameArr = mysql_fetch_assoc($empRst);
			return $SubtaskNameArr['sub_tasks'];
		}
		else
		{
			return '';
		}
	}
	
	function get_pro_detils($project_code)
	{
		$query = mysql_query("SELECT pr.name AS project_code, cl.client_name,pr.model,st.shift_code,jr.name AS job_responsibility, sc.support_type FROM process AS pr JOIN client `cl` ON pr.fk_client_id = cl.client_id JOIN shift `st` ON pr.shift_id = st.shift_id JOIN job_responsibilities AS jr ON jr.job_responsibilities_id = pr.job_responsibilities_id JOIN `support_coverage` `sc` ON pr.support_id = sc.support_id WHERE pr.name =  '$project_code'  LIMIT 1");
		$row = mysql_fetch_row($query);

		return $row ;
	}

	function get_task_detils($task_code)
	{
		$query = mysql_query("SELECT task_code,task_category,task_name,request_type,complexity,priority FROM task_codes WHERE  task_code= '$task_code'  LIMIT 1");
		$row = mysql_fetch_row($query);

		return $row ;
	}
	
}

$obj = new exportUtilizationWeek();
$obj->exportUtil();
?>