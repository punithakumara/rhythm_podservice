<?php
//http://localhost:8082/onboarding/script/exportTimesheetWeek-v2.php

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportTimesheet extends MysqlConnect
{
	function exportTime()
	{
		$filename = "Timesheet.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}	
	
	function secToHours($data1,$data2,$data3)
	{
		$totalSecs = $data1 + $data2 + $data3;
		$t = round($totalSecs);
		
		$time_str = sprintf('%02d:%02d', ($t/3600),($t/60%60));
		return sprintf("%0.02f", $this->time_to_float($time_str));
	}

	function time_to_float($time) {
		$timeArr = explode(":", $time);
		return $timeArr[0] + ($timeArr[1] / 60);
	}
	
	function downloadCsv() 
	{
		$index =0;
		$data = array();
		
		$sql = "SELECT employees.employee_id, employees.company_employ_id, 
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
		designation.name AS designation, hcm_grade AS grade, GROUP_CONCAT(DISTINCT verticals.name) AS Vertical,
		CONCAT(IFNULL(sup.first_name, '') ,' ', IFNULL(sup.last_name,'')) AS SupervisorName 
		FROM employees 
		LEFT JOIN designation on designation.designation_id = employees.designation_id
		LEFT JOIN employees sup ON sup.employee_id = employees.primary_lead_id 
		LEFT JOIN employee_vertical ON employee_vertical.employee_id = employees.employee_id 
		LEFT JOIN verticals on verticals.vertical_id = employee_vertical.vertical_id 
		WHERE employees.status!=6 AND employees.location_id!=4
		GROUP BY employees.employee_id";		
		// echo $sql; exit;
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{
			$timesheetApprovedHrs = $this->getTimeTaken($val['employee_id'],'66',2,'Mon');
			$timesheetPendingHrs = $this->getTimeTaken($val['employee_id'],'66',1,'Mon');
			$appliedUtilHrs = $this->getProductionTime($val['employee_id'],'2019-04-01','0,1,3,4');
			$apprUtilHrs = $this->getProductionTime($val['employee_id'],'2019-04-01','1,3,4');

			$timesheetApprovedHrs2 = $this->getTimeTaken($val['employee_id'],'66',2,'Tue');
			$timesheetPendingHrs2 = $this->getTimeTaken($val['employee_id'],'66',1,'Tue');
			$appliedUtilHrs2 = $this->getProductionTime($val['employee_id'],'2019-04-02','0,1,3,4');
			$apprUtilHrs2 = $this->getProductionTime($val['employee_id'],'2019-04-02','1,3,4');

			$timesheetApprovedHrs3 = $this->getTimeTaken($val['employee_id'],'66',2,'Wed');
			$timesheetPendingHrs3 = $this->getTimeTaken($val['employee_id'],'66',1,'Wed');
			$appliedUtilHrs3 = $this->getProductionTime($val['employee_id'],'2019-04-03','0,1,3,4');
			$apprUtilHrs3 = $this->getProductionTime($val['employee_id'],'2019-04-03','1,3,4');

			$timesheetApprovedHrs4 = $this->getTimeTaken($val['employee_id'],'66',2,'Thu');
			$timesheetPendingHrs4 = $this->getTimeTaken($val['employee_id'],'66',1,'Thu');
			$appliedUtilHrs4 = $this->getProductionTime($val['employee_id'],'2019-04-04','0,1,3,4');
			$apprUtilHrs4 = $this->getProductionTime($val['employee_id'],'2019-04-04','1,3,4');

			$timesheetApprovedHrs5 = $this->getTimeTaken($val['employee_id'],'66',2,'Fri');
			$timesheetPendingHrs5 = $this->getTimeTaken($val['employee_id'],'66',1,'Fri');
			$appliedUtilHrs5 = $this->getProductionTime($val['employee_id'],'2019-04-05','0,1,3,4');
			$apprUtilHrs5 = $this->getProductionTime($val['employee_id'],'2019-04-05','1,3,4');

			$timesheetApprovedHrs6 = $this->getTimeTaken($val['employee_id'],'66',2,'Sat');
			$timesheetPendingHrs6 = $this->getTimeTaken($val['employee_id'],'66',1,'Sat');
			$appliedUtilHrs6 = $this->getProductionTime($val['employee_id'],'2019-04-06','0,1,3,4');
			$apprUtilHrs6 = $this->getProductionTime($val['employee_id'],'2019-04-06','1,3,4');

			$timesheetApprovedHrs7 = $this->getTimeTaken($val['employee_id'],'66',2,'Sun');
			$timesheetPendingHrs7 = $this->getTimeTaken($val['employee_id'],'66',1,'Sun');
			$appliedUtilHrs7 = $this->getProductionTime($val['employee_id'],'2019-04-07','0,1,3,4');
			$apprUtilHrs7 = $this->getProductionTime($val['employee_id'],'2019-04-07','1,3,4');

			$timesheetApprovedHrs8 = $this->getTimeTaken($val['employee_id'],'67',2,'Mon');
			$timesheetPendingHrs8 = $this->getTimeTaken($val['employee_id'],'67',1,'Mon');
			$appliedUtilHrs8 = $this->getProductionTime($val['employee_id'],'2019-04-08','0,1,3,4');
			$apprUtilHrs8 = $this->getProductionTime($val['employee_id'],'2019-04-08','1,3,4');

			$timesheetApprovedHrs9 = $this->getTimeTaken($val['employee_id'],'67',2,'Tue');
			$timesheetPendingHrs9 = $this->getTimeTaken($val['employee_id'],'67',1,'Tue');
			$appliedUtilHrs9 = $this->getProductionTime($val['employee_id'],'2019-04-09','0,1,3,4');
			$apprUtilHrs9 = $this->getProductionTime($val['employee_id'],'2019-04-09','1,3,4');

			$timesheetApprovedHrs10 = $this->getTimeTaken($val['employee_id'],'67',2,'Wed');
			$timesheetPendingHrs10 = $this->getTimeTaken($val['employee_id'],'67',1,'Wed');
			$appliedUtilHrs10 = $this->getProductionTime($val['employee_id'],'2019-04-10','0,1,3,4');
			$apprUtilHrs10 = $this->getProductionTime($val['employee_id'],'2019-04-10','1,3,4');

			$timesheetApprovedHrs11 = $this->getTimeTaken($val['employee_id'],'67',2,'Thu');
			$timesheetPendingHrs11 = $this->getTimeTaken($val['employee_id'],'67',1,'Thu');
			$appliedUtilHrs11 = $this->getProductionTime($val['employee_id'],'2019-04-11','0,1,3,4');
			$apprUtilHrs11 = $this->getProductionTime($val['employee_id'],'2019-04-11','1,3,4');

			$timesheetApprovedHrs12 = $this->getTimeTaken($val['employee_id'],'67',2,'Fri');
			$timesheetPendingHrs12 = $this->getTimeTaken($val['employee_id'],'67',1,'Fri');
			$appliedUtilHrs12 = $this->getProductionTime($val['employee_id'],'2019-04-12','0,1,3,4');
			$apprUtilHrs12 = $this->getProductionTime($val['employee_id'],'2019-04-12','1,3,4');

			$timesheetApprovedHrs13 = $this->getTimeTaken($val['employee_id'],'67',2,'Sat');
			$timesheetPendingHrs13 = $this->getTimeTaken($val['employee_id'],'67',1,'Sat');
			$appliedUtilHrs13 = $this->getProductionTime($val['employee_id'],'2019-04-13','0,1,3,4');
			$apprUtilHrs13 = $this->getProductionTime($val['employee_id'],'2019-04-13','1,3,4');

			$timesheetApprovedHrs14 = $this->getTimeTaken($val['employee_id'],'67',2,'Sun');
			$timesheetPendingHrs14 = $this->getTimeTaken($val['employee_id'],'67',1,'Sun');
			$appliedUtilHrs14 = $this->getProductionTime($val['employee_id'],'2019-04-14','0,1,3,4');
			$apprUtilHrs14 = $this->getProductionTime($val['employee_id'],'2019-04-14','1,3,4');
			
			$timesheetApprovedHrs15 = $this->getTimeTaken($val['employee_id'],'68',2,'Mon');
			$timesheetPendingHrs15 = $this->getTimeTaken($val['employee_id'],'68',1,'Mon');
			$appliedUtilHrs15 = $this->getProductionTime($val['employee_id'],'2019-04-15','0,1,3,4');
			$apprUtilHrs15 = $this->getProductionTime($val['employee_id'],'2019-04-15','1,3,4');
			
			$timesheetApprovedHrs16 = $this->getTimeTaken($val['employee_id'],'68',2,'Tue');
			$timesheetPendingHrs16 = $this->getTimeTaken($val['employee_id'],'68',1,'Tue');
			$appliedUtilHrs16 = $this->getProductionTime($val['employee_id'],'2019-04-16','0,1,3,4');
			$apprUtilHrs16 = $this->getProductionTime($val['employee_id'],'2019-04-16','1,3,4');
			
			$timesheetApprovedHrs17 = $this->getTimeTaken($val['employee_id'],'68',2,'Wed');
			$timesheetPendingHrs17 = $this->getTimeTaken($val['employee_id'],'68',1,'Wed');
			$appliedUtilHrs17 = $this->getProductionTime($val['employee_id'],'2019-04-17','0,1,3,4');
			$apprUtilHrs17 = $this->getProductionTime($val['employee_id'],'2019-04-17','1,3,4');
			
			$timesheetApprovedHrs18 = $this->getTimeTaken($val['employee_id'],'68',2,'Thu');
			$timesheetPendingHrs18 = $this->getTimeTaken($val['employee_id'],'68',1,'Thu');
			$appliedUtilHrs18 = $this->getProductionTime($val['employee_id'],'2019-04-18','0,1,3,4');
			$apprUtilHrs18 = $this->getProductionTime($val['employee_id'],'2019-04-18','1,3,4');
			
			$timesheetApprovedHrs19 = $this->getTimeTaken($val['employee_id'],'68',2,'Fri');
			$timesheetPendingHrs19 = $this->getTimeTaken($val['employee_id'],'68',1,'Fri');
			$appliedUtilHrs19 = $this->getProductionTime($val['employee_id'],'2019-04-19','0,1,3,4');
			$apprUtilHrs19 = $this->getProductionTime($val['employee_id'],'2019-04-19','1,3,4');
			
			$timesheetApprovedHrs20 = $this->getTimeTaken($val['employee_id'],'68',2,'Sat');
			$timesheetPendingHrs20 = $this->getTimeTaken($val['employee_id'],'68',1,'Sat');
			$appliedUtilHrs20 = $this->getProductionTime($val['employee_id'],'2019-04-20','0,1,3,4');
			$apprUtilHrs20 = $this->getProductionTime($val['employee_id'],'2019-04-20','1,3,4');
			
			$timesheetApprovedHrs21 = $this->getTimeTaken($val['employee_id'],'68',2,'Sun');
			$timesheetPendingHrs21 = $this->getTimeTaken($val['employee_id'],'68',1,'Sun');
			$appliedUtilHrs21 = $this->getProductionTime($val['employee_id'],'2019-04-21','0,1,3,4');
			$apprUtilHrs21 = $this->getProductionTime($val['employee_id'],'2019-04-21','1,3,4');
			
			$timesheetApprovedHrs22 = $this->getTimeTaken($val['employee_id'],'69',2,'Mon');
			$timesheetPendingHrs22 = $this->getTimeTaken($val['employee_id'],'69',1,'Mon');
			$appliedUtilHrs22 = $this->getProductionTime($val['employee_id'],'2019-04-22','0,1,3,4');
			$apprUtilHrs22 = $this->getProductionTime($val['employee_id'],'2019-04-22','1,3,4');
			
			$timesheetApprovedHrs23 = $this->getTimeTaken($val['employee_id'],'69',2,'Tue');
			$timesheetPendingHrs23 = $this->getTimeTaken($val['employee_id'],'69',1,'Tue');
			$appliedUtilHrs23 = $this->getProductionTime($val['employee_id'],'2019-04-23','0,1,3,4');
			$apprUtilHrs23 = $this->getProductionTime($val['employee_id'],'2019-04-23','1,3,4');
			
			$timesheetApprovedHrs24 = $this->getTimeTaken($val['employee_id'],'69',2,'Wed');
			$timesheetPendingHrs24 = $this->getTimeTaken($val['employee_id'],'69',1,'Wed');
			$appliedUtilHrs24 = $this->getProductionTime($val['employee_id'],'2019-04-24','0,1,3,4');
			$apprUtilHrs24 = $this->getProductionTime($val['employee_id'],'2019-04-24','1,3,4');
			
			$timesheetApprovedHrs25 = $this->getTimeTaken($val['employee_id'],'69',2,'Thu');
			$timesheetPendingHrs25 = $this->getTimeTaken($val['employee_id'],'69',1,'Thu');
			$appliedUtilHrs25 = $this->getProductionTime($val['employee_id'],'2019-04-25','0,1,3,4');
			$apprUtilHrs25 = $this->getProductionTime($val['employee_id'],'2019-04-25','1,3,4');
			
			$timesheetApprovedHrs26 = $this->getTimeTaken($val['employee_id'],'69',2,'Fri');
			$timesheetPendingHrs26 = $this->getTimeTaken($val['employee_id'],'69',1,'Fri');
			$appliedUtilHrs26 = $this->getProductionTime($val['employee_id'],'2019-04-26','0,1,3,4');
			$apprUtilHrs26 = $this->getProductionTime($val['employee_id'],'2019-04-26','1,3,4');
			
			$timesheetApprovedHrs27 = $this->getTimeTaken($val['employee_id'],'69',2,'Sat');
			$timesheetPendingHrs27 = $this->getTimeTaken($val['employee_id'],'69',1,'Sat');
			$appliedUtilHrs27 = $this->getProductionTime($val['employee_id'],'2019-04-27','0,1,3,4');
			$apprUtilHrs27 = $this->getProductionTime($val['employee_id'],'2019-04-27','1,3,4');
			
			$timesheetApprovedHrs28 = $this->getTimeTaken($val['employee_id'],'69',2,'Sun');
			$timesheetPendingHrs28 = $this->getTimeTaken($val['employee_id'],'69',1,'Sun');
			$appliedUtilHrs28 = $this->getProductionTime($val['employee_id'],'2019-04-28','0,1,3,4');
			$apprUtilHrs28 = $this->getProductionTime($val['employee_id'],'2019-04-28','1,3,4');
			
			$timesheetApprovedHrs29 = $this->getTimeTaken($val['employee_id'],'70',2,'Mon');
			$timesheetPendingHrs29 = $this->getTimeTaken($val['employee_id'],'70',1,'Mon');
			$appliedUtilHrs29 = $this->getProductionTime($val['employee_id'],'2019-04-29','0,1,3,4');
			$apprUtilHrs29 = $this->getProductionTime($val['employee_id'],'2019-04-29','1,3,4');
			
			$timesheetApprovedHrs30 = $this->getTimeTaken($val['employee_id'],'70',2,'Tue');
			$timesheetPendingHrs30 = $this->getTimeTaken($val['employee_id'],'70',1,'Tue');
			$appliedUtilHrs30 = $this->getProductionTime($val['employee_id'],'2019-04-30','0,1,3,4');
			$apprUtilHrs30 = $this->getProductionTime($val['employee_id'],'2019-04-30','1,3,4');
			
			$data[$index]['Employee ID'] 	= $val['company_employ_id'];
			$data[$index]['Employee Name'] 	= $val['FullName'];
			$data[$index]['Vertical'] 		= $val['Vertical'];
			$data[$index]['Grade'] 			= $val['grade'];
			$data[$index]['Designation'] 	= $val['designation'];
			$data[$index]['Supervisor'] 	= $val['SupervisorName'];


			$data[$index]['01-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs,$timesheetPendingHrs,$appliedUtilHrs)."";
			$data[$index]['01-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs,$apprUtilHrs)."";
			$data[$index]['02-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs2,$timesheetPendingHrs2,$appliedUtilHrs2)."";
			$data[$index]['02-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs2,$apprUtilHrs2)."";
			$data[$index]['03-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs3,$timesheetPendingHrs3,$appliedUtilHrs3)."";
			$data[$index]['03-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs3,$apprUtilHrs3)."";
			$data[$index]['04-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs4,$timesheetPendingHrs4,$appliedUtilHrs4)."";
			$data[$index]['04-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs4,$apprUtilHrs4)."";
			$data[$index]['05-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs5,$timesheetPendingHrs5,$appliedUtilHrs5)."";
			$data[$index]['05-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs5,$apprUtilHrs5)."";
			$data[$index]['06-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs6,$timesheetPendingHrs6,$appliedUtilHrs6)."";
			$data[$index]['06-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs6,$apprUtilHrs6)."";
			$data[$index]['07-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs7,$timesheetPendingHrs7,$appliedUtilHrs7)."";
			$data[$index]['07-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs7,$apprUtilHrs7)."";
			$data[$index]['08-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs8,$timesheetPendingHrs8,$appliedUtilHrs8)."";
			$data[$index]['08-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs8,$apprUtilHrs8)."";
			$data[$index]['09-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs9,$timesheetPendingHrs9,$appliedUtilHrs9)."";
			$data[$index]['09-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs9,$apprUtilHrs9)."";
			$data[$index]['10-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs10,$timesheetPendingHrs10,$appliedUtilHrs10)."";
			$data[$index]['10-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs10,$apprUtilHrs10)."";
			$data[$index]['11-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs11,$timesheetPendingHrs11,$appliedUtilHrs11)."";
			$data[$index]['11-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs11,$apprUtilHrs11)."";
			$data[$index]['12-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs12,$timesheetPendingHrs12,$appliedUtilHrs12)."";
			$data[$index]['12-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs12,$apprUtilHrs12)."";
			$data[$index]['13-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs13,$timesheetPendingHrs13,$appliedUtilHrs13)."";
			$data[$index]['13-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs13,$apprUtilHrs13)."";
			$data[$index]['14-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs14,$timesheetPendingHrs14,$appliedUtilHrs14)."";
			$data[$index]['14-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs14,$apprUtilHrs14)."";
			$data[$index]['15-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs15,$timesheetPendingHrs15,$appliedUtilHrs15)."";
			$data[$index]['15-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs15,$apprUtilHrs15)."";
			$data[$index]['16-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs16,$timesheetPendingHrs16,$appliedUtilHrs16)."";
			$data[$index]['16-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs16,$apprUtilHrs16)."";
			$data[$index]['17-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs17,$timesheetPendingHrs17,$appliedUtilHrs17)."";
			$data[$index]['17-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs17,$apprUtilHrs17)."";
			$data[$index]['18-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs18,$timesheetPendingHrs18,$appliedUtilHrs18)."";
			$data[$index]['18-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs18,$apprUtilHrs18)."";
			$data[$index]['19-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs19,$timesheetPendingHrs19,$appliedUtilHrs19)."";
			$data[$index]['19-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs19,$apprUtilHrs19)."";
			$data[$index]['20-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs20,$timesheetPendingHrs20,$appliedUtilHrs20)."";
			$data[$index]['20-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs20,$apprUtilHrs20)."";
			$data[$index]['21-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs21,$timesheetPendingHrs21,$appliedUtilHrs21)."";
			$data[$index]['21-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs21,$apprUtilHrs21)."";
			$data[$index]['22-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs22,$timesheetPendingHrs22,$appliedUtilHrs22)."";
			$data[$index]['22-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs22,$apprUtilHrs22)."";
			$data[$index]['23-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs23,$timesheetPendingHrs23,$appliedUtilHrs23)."";
			$data[$index]['23-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs23,$apprUtilHrs23)."";
			$data[$index]['24-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs24,$timesheetPendingHrs24,$appliedUtilHrs24)."";
			$data[$index]['24-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs24,$apprUtilHrs24)."";
			$data[$index]['25-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs25,$timesheetPendingHrs25,$appliedUtilHrs25)."";
			$data[$index]['25-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs25,$apprUtilHrs25)."";
			$data[$index]['26-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs26,$timesheetPendingHrs26,$appliedUtilHrs26)."";
			$data[$index]['26-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs26,$apprUtilHrs26)."";
			$data[$index]['27-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs27,$timesheetPendingHrs27,$appliedUtilHrs27)."";
			$data[$index]['27-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs27,$apprUtilHrs27)."";
			$data[$index]['28-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs28,$timesheetPendingHrs28,$appliedUtilHrs28)."";
			$data[$index]['28-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs28,$apprUtilHrs28)."";
			$data[$index]['29-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs29,$timesheetPendingHrs29,$appliedUtilHrs29)."";
			$data[$index]['29-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs29,$apprUtilHrs29)."";
			$data[$index]['30-Apr-19(Applied)'] 	= "".$this->secToHours($timesheetApprovedHrs30,$timesheetPendingHrs30,$appliedUtilHrs30)."";
			$data[$index]['30-Apr-19(Approved)'] 	= "".$this->secToHours($timesheetApprovedHrs30,$apprUtilHrs30)."";
			
			$index++;
		}
		
		$header = array_keys($data[0]);
		$result_result = array_merge(array(array_unique($header)), $data);
		return array($result_result);
	}
	
	public function getTimeTaken($employee_id,$week_code_id, $status, $day)//0-Rejected, 1-Pending, 2-Approved
	{
		$select = "SUM(TIME_TO_SEC(day1)) AS status";
		if($day=="Tue")
		{
			$select = "SUM(TIME_TO_SEC(day2)) AS status";
		}
		if($day=="Wed")
		{
			$select = "SUM(TIME_TO_SEC(day3)) AS status";
		}
		if($day=="Thu")
		{
			$select = "SUM(TIME_TO_SEC(day4)) AS status";
		}
		if($day=="Fri")
		{
			$select = "SUM(TIME_TO_SEC(day5)) AS status";
		}
		if($day=="Sat")
		{
			$select = "SUM(TIME_TO_SEC(day6)) AS status";
		}
		if($day=="Sun")
		{
			$select = "SUM(TIME_TO_SEC(day7)) AS status";
		}
		$sql = "SELECT $select	
			FROM `timesheet_task_mapping` 
			WHERE task_week_id = '".$week_code_id."' AND emp_id='".$employee_id."'"; 
		if($status!="")
		{
			$sql .= " AND status IN (".$status.")"; 
		}
	// echo $sql; exit;
		$result = mysql_query($sql);
		$result = mysql_fetch_row($result);
		
		return $result[0];
	}
	
	public function getProductionTime($employee_id, $start_date, $status)//'0'=> Pending, "1" => Approved , "2"=> Rejected,"3"=> Weekend Billable,"4"=> Client Holiday Billable
	{
		$sql = "SELECT SUM(TIME_TO_SEC(theorem_hrs)) AS TimeTaken
			FROM `utilization` 
			WHERE employee_id='".$employee_id."' AND status IN (".$status.") AND task_date LIKE '%".$start_date."%'"; 

	// echo $sql; exit;
		$result = mysql_query($sql);
		$result = mysql_fetch_row($result);
		
		return $result[0];
	}
}

$obj = new exportTimesheet();
$obj->exportTime();
?>