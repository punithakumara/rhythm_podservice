<?php
//http://localhost:8082/onboarding/script/exportTimesheet.php?startDate=2018-12-01&endDate=2018-12-31

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportTimesheet extends MysqlConnect
{
	function exportTime()
	{
		$filename = "Timesheet.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}	

	
	function downloadCsv() 
	{
		$index =0;
		$data = array();
		
		$sql = "SELECT employees.employee_id, employees.company_employ_id, 
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,verticals.name AS Vertical
		FROM employees 
		LEFT JOIN verticals on verticals.vertical_id = employees.assigned_vertical 
		WHERE employees.status!=6 AND company_employ_id!=0";		
		
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{
			$timeData = $this->getTimeDetails($val['employee_id']);
			$prodData = $this->getProdutionDetails($val['employee_id']);
			$data[$index]['Vertical'] 		= $val['Vertical'];
			$data[$index]['EmployID'] 		= $val['company_employ_id'];
			$data[$index]['Name'] 			= $val['FullName'];
			$data[$index]['TimeTaken(hrs)'] = "".$this->secToHours($timeData,$prodData)."";
			
			$index++;
		}
		
		$header = array_keys($data[0]);
		$result_result = array_merge(array(array_unique($header)), $data);
		
		return array($result_result);
	}
	
	
	function secToHours($timeData,$prodData)
	{
		$totalSecs = $timeData + $prodData;
		$t = round($totalSecs);
		
		return sprintf('%02d:%02d', ($t/3600),($t/60%60));
	}
	
	
	private function getProdutionDetails($employee_id)
	{
		$sql = "SELECT SUM(TIME_TO_SEC(actual_hrs)) AS TimeTaken
			FROM `utilization` 
			WHERE utilization.task_date BETWEEN '".$_GET['startDate']."' AND '".$_GET['endDate']."' AND employee_id = '".$employee_id."' AND approved IN (0,1,3,4) 
			GROUP BY employee_id";//'0'=> Pending, "1" => Approved , "2"=> Rejected,"3"=> Weekend Billable,"4"=> Client Holiday Billable

		$result = mysql_query($sql);
		$val = mysql_fetch_row($result);
		
		return $val[0];
	}
	
	public function getTimeDetails($employee_id)
	{
		$sql = "SELECT week_name,week_code_id FROM `task_week` WHERE week_date BETWEEN '".$_GET['startDate']."' AND '".$_GET['endDate']."'";

		$data = array();
		$result = mysql_query($sql);
		$timeTaken = 0;
		while($val = mysql_fetch_array($result))
		{
			$timeTaken2 = $this->getTimeTaken($employee_id,$val['week_name'],$val['week_code_id']);
			$timeTaken += $timeTaken2;
		}
		
		return $timeTaken;
	}
	
	public function getTimeTaken($employee_id,$week_name,$week_code_id)
	{
		$select = "SUM(TIME_TO_SEC(day1)) AS TimeTaken";
		if($week_name=="Tue")
		{
			$select = "SUM(TIME_TO_SEC(day2)) AS TimeTaken";
		}
		if($week_name=="Wed")
		{
			$select = "SUM(TIME_TO_SEC(day3)) AS TimeTaken";
		}
		if($week_name=="Thu")
		{
			$select = "SUM(TIME_TO_SEC(day4)) AS TimeTaken";
		}
		if($week_name=="Fri")
		{
			$select = "SUM(TIME_TO_SEC(day5)) AS TimeTaken";
		}
		if($week_name=="Sat")
		{
			$select = "SUM(TIME_TO_SEC(day6)) AS TimeTaken";
		}
		if($week_name=="Sun")
		{
			$select = "SUM(TIME_TO_SEC(day7)) AS TimeTaken";
		}
		$sql = "SELECT $select
			FROM `timesheet_task_mapping` 
			WHERE task_week_id = '".$week_code_id."' AND emp_id='".$employee_id."' AND status IN(1,2)"; //0-Rejected, 1-Pending, 2-Approved
			
		$result = mysql_query($sql);
		$result = mysql_fetch_row($result);
		
		return $result[0];
	}
}

$obj = new exportTimesheet();
$obj->exportTime();
?>