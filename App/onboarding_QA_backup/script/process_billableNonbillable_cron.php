<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';

class Process_billableNonbillableCount extends MysqlConnect{
	// public function cron()
	// {
		// $query = "SELECT 
					// process.ProcessID, 
					// process.ClientID, 
					// IF((billable_view.Billable IS NOT NULL),billable_view.Billable,0) AS Billable,
					// IF((nonbillable_view.NonBillable IS NOT NULL),nonbillable_view.NonBillable,0) AS NonBillable
				// FROM PROCESS
				// LEFT JOIN billable_view ON (process.processID = billable_view.processID)
				// LEFT JOIN nonbillable_view ON (process.processID = nonbillable_view.processID)
				// WHERE process.ProcessStatus = 0";
		// $query_exe = mysql_query($query);
		// $values = '';
		// $date = date('Y-m-d');
		// if(mysql_num_rows($query_exe) > 0)
		// {
			// while($row = mysql_fetch_assoc($query_exe))
			// {
				// $values .= '('.$row['ProcessID'].','.$row['ClientID'].','.$row['Billable'].','.$row['NonBillable'].',"'.$date.'"),';
			// };
			// $values = rtrim($values, ',');
			// mysql_query('INSERT INTO process_billablenonbillable_cron (ProcessID, ClientID, Billable, NonBillable, Date) VALUES '.$values);
		// }
	// }
	public function cron1()
	{
		$values = ''; 
		$date = date('Y-m-d');
		
		$query = mysql_query("SELECT ProcessID, ClientID FROM Process WHERE Process.ProcessStatus != 1");
		while($ProcessRec = mysql_fetch_assoc($query))
		{
			$NonBillable = 0;
			$Billable = 0;
			$leadership = 0;

			$resQuery = "SELECT Process.ProcessID, Process.ClientID, temp.EmployID, temp.Billable, temp.BillablePercentage, designation.grades FROM 
				(SELECT Billable, ProcessID, EmployID, BillablePercentage FROM employprocess WHERE ProcessID = '".$ProcessRec['ProcessID']."'
				) temp
				JOIN PROCESS ON (Process.ProcessID = temp.ProcessID AND Process.ProcessStatus != 1)
				JOIN employ ON (employ.EmployID = temp.EmployID AND employ.RelieveFlag != 1)
				JOIN designation ON (designation.DesignationID = employ.DesignationID)";
		$query_exe = mysql_query($resQuery);
		if(mysql_num_rows($query_exe) > 0)
		{
			while($row = mysql_fetch_assoc($query_exe))
			{
				$BillablePercentage = $row['BillablePercentage'] ? $row['BillablePercentage'] : 100;
				$Billablity = $row['Billable'];
				$grades = $row['grades'];
				
				if($Billablity == 1){
					$Billable += ($Billablity / 100) * $BillablePercentage;
					//$NonBillable += ($BillablePercentage < 100) ? 0.5 : 0;
					$NonBillable += ($BillablePercentage <= 25) ? 0.25 : (($BillablePercentage <= 50) ? 0.5 :(($BillablePercentage <= 75) ? 0.75 : 0));
				}
				else if($grades < 3){
					$NonBillable += 1;
				}
				else{
					$leadership += 1;
				}
			}
			$values .= '('.$ProcessRec['ProcessID'].','.$ProcessRec['ClientID'].','.$Billable.','.$NonBillable.','.$leadership.',"'.$date.'"),';
		}
		}
			$values = rtrim($values, ',');
			mysql_query('INSERT INTO process_billablenonbillable_cron (ProcessID, ClientID, Billable, NonBillable, Leadership, Date) VALUES '.$values);
	}
}

$obj  = new Process_billableNonbillableCount();
$obj->cron1();
