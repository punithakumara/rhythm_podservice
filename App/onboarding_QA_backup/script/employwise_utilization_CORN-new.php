<?php
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class EmploywiseUtlizationProcessCron extends MysqlConnect{
	
	public $expectedHours = 0;
	public $CurrentMonthYear;
	public $CurrentMonthYearDate;

	function __construct( ) 
	{
		parent::__construct( );
		$this->CurrentMonthYear = date('Y-m');
		$this->CurrentMonthYearDate = date('Y-m-d');
	}
	
 
	function IsBillabiltyDataExist($client_id,$employee_id,$today)
	{
		
			$query = "SELECT employee_id, client_id FROM `employee_client` WHERE employee_id= '$employee_id' AND client_id = '$client_id' ";
$result = mysql_query($query);

		if(mysql_num_rows($result) > 0) {
			$sql1= "SELECT client_id,head_count  FROM client_billable_history_cron WHERE client_id = ".$client_id;
			$result1 = mysql_query($sql1);
			return $result_arr = mysql_fetch_assoc($result1);
		
		}
			
		return "";
	}
 
 
	/**
	* @description : Function to calculate expected hours based on employID and Client.
	*/
	function _getExpectedHoursByClient($client_id,$employee_id,$today)
	{
		
		$this->expectedHours = 0;
		$BillblePercentage = 1;
		$this->billableResource=0;
		// echo "IsBillabiltyDataExist(  $client_id,$employee_id)" ;echo "<BR>";
		$Billable_count_by_process = $this->IsBillabiltyDataExist($client_id,$employee_id,$today);
		 // echo "<PRE>";print_r($Billable_count_by_process);
		if($Billable_count_by_process['head_count'] != ""){
			
			$this->expectedHours = $Billable_count_by_process['head_count'] * 8;
		 	$this->billableResource = $Billable_count_by_process['head_count'];
			 echo $client_id."----------> ".$this->expectedHours ." billableResource "."</br>";
		}
		 
		
		/* if($Billable_count_by_process['Billable'] != ""){
		 	$this->expectedHours = $Billable_count_by_process['Billable'] * 8;
		 	$this->billableResource = $Billable_count_by_process['Billable'];
		 echo $client_id."----------> ".$this->expectedHours ." billableResource "."</br>"; 
		 	 
		 }	 */
	}
	
	/**
	* @description : deleting existing utilization data based on date
	*/
	private function  DeleteExistingUtilization($today)
	{
		if(!empty($today))
		{
			$sqldel = "DELETE FROM utilization_employeewise WHERE task_date = '$today'";
			$result = mysql_query($sqldel) or die(mysql_error()."Query error");
		}
	}
	
	/**
	* @description : Function to insert utilization data into employwise_utilization_process table.
	*/
 	public function emp_utlization_process($today='')
	{
		// $today =  '2017-09-25' ;
		if(!empty($today))
		{	
			$utilizationSuccessCount = '';
			$Utilization = 0;
			$Insertvalues = array();
			
			$this->DeleteExistingUtilization($today);

				$query ="SELECT DISTINCT u1.utilization_id, u1.task_date, u1.process_id,employees.first_name,u1.vertical_id,u1.vertical_name,u1.support,
				employees.shift_id,shift.shift_code, u1.approved ,
				u1.employee_id , u1.weekend_support,u1.client_holiday,
				SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(u1.hours) + SUM(u1.min) DIV 60 ,':',SUM(u1.min) MOD 60))) AS Total_Hours_Day,
				client.client_id AS client_id,client.client_name AS clientName
				FROM utilization u1   
				LEFT JOIN CLIENT  ON client.client_id = u1.client_id 
				LEFT JOIN employees  ON employees.employee_id = u1.employee_id 
				LEFT JOIN shift  ON shift.shift_id = employees.shift_id 
				WHERE  u1.task_date= '".$today."'
				GROUP BY u1.employee_id
				ORDER BY  u1.utilization_id";
				
				/*$query ="SELECT  u1.task_date,
GROUP_CONCAT(DISTINCT (u1.utilization_id)) AS utilization_id,
GROUP_CONCAT(DISTINCT (u1.employee_id)) AS employee_id,
GROUP_CONCAT(DISTINCT (u1.vertical_id)) AS vertical_id,
GROUP_CONCAT(DISTINCT (u1.vertical_name)) AS vertical_name,
GROUP_CONCAT(DISTINCT (u1.support)) AS support,
employees.shift_id,shift.shift_code, u1.approved , u1.weekend_support,u1.client_holiday,
SEC_TO_TIME(TIME_TO_SEC(CONCAT(SUM(u1.hours) + SUM(u1.min) DIV 60 ,':',SUM(u1.min) MOD 60))) AS Total_Hours_Day,
 client.client_id AS client_id,client.client_name AS clientName FROM utilization u1 LEFT JOIN CLIENT ON client.client_id = u1.client_id 
LEFT JOIN employees ON employees.employee_id = u1.employee_id 
LEFT JOIN shift ON shift.shift_id = employees.shift_id 
WHERE u1.task_date= '".$today."' 
GROUP BY u1.client_id 
ORDER BY u1.utilization_id";*/
							// echo $query;
			$result =  mysql_query($query) or die(mysql_error()."Query error");
		 
			while($res = mysql_fetch_assoc($result)) 
			{ 
				echo "<PRE>";print_r($res);
				$this->_getExpectedHoursByClient($res['client_id'],$res['employee_id'],$today);
						// echo $this->expectedHours;	
				
				$utilization_id = $res['utilization_id'];
				$client_id 		= empty($res['client_id']) ? 0 : $res['client_id'];
				$vertical_id 	= $res['vertical_id'];
				$vertical_name 	= $res['vertical_name'];
				$task_date 		= $res['task_date'];
				$employee_id 	= $res['employee_id'];
				$num_hours 	= empty($res['Total_Hours_Day']) ? 0 : $res['Total_Hours_Day'];
				$Total_Hour_util= empty($res['Total_Hours_Day']) ? 0 : str_replace(':', '.',$res['Total_Hours_Day']);
				$now 			= date('Y-m-d H:i:s');
				$weekend_support = empty( $res['weekend_support']) ? 0 :  $res['weekend_support'];
				$client_holiday  = empty( $res['client_holiday']) ? 0 :  $res['client_holiday'];
				$shift_id 		= empty($res['shift_id']) ? 0 : $res['shift_id'];
				$approved 		= empty($res['approved']) ? 0 : $res['approved'];
				$support 		= empty($res['support']) ? 0 : $res['support'];

				  if($this->expectedHours!=0)
				  {					
					  $Utilization =  ($Total_Hour_util / 8)*100;
				  }
				  else
				  {
					  $Utilization =  0 ;
				  }
				
				$expectedHours 	= $this->expectedHours;
				$billable_resource 	= $this->billableResource;
				$Utilization 	= empty($Utilization) ? 0 : round($Utilization,2);				
				
				$Insertvalues[] =  "('".$utilization_id."','".$client_id."','".$approved."','".$Utilization."','".$num_hours."','". $employee_id."','". $shift_id."','".$task_date."','".$vertical_id."','".$support."','".$vertical_name."','".$weekend_support."','".$client_holiday."','".$expectedHours."','".$billable_resource."','".$now."')";
				$utilizationSuccessCount++;
			}
		
			echo "<PRE>";print_r($Insertvalues);

			//Inserting into utilization_employeewise table
			 if( !empty($Insertvalues) ) 
			{
				  $query = "INSERT INTO `utilization_employeewise`(utilization_id ,client_id,approved,utilization, num_hours,employee_id,shift_id,task_date,vertical_id,support,vertical_name,weekend_support,client_holiday,client_exp_hours,billable_resource,created)
						VALUES ". implode(',',$Insertvalues);  
						
						// echo $query;
				if( mysql_query($query) )
				{
					echo "$utilizationSuccessCount  Utilization Added Successfully </br></br>";
				}
				else
				{
					echo "$utilization_id Error in Utilization insertion";
				} 
			}
			else
			{
				echo "No Utilization data for ".$today;
			}  
		}
 	}
}
// for only one day
/*  $date="2016-05-06";
$obj  = new EmploywiseUtlizationProcessCron();
$obj->emp_utlization_process($date); */
//Between Two dates
// For mennually run query.

$obj  = new EmploywiseUtlizationProcessCron();
$date = '2017-08-17';
$end_date = '2017-09-07';
// echo "CHANGE DATABASE";exit;
while (strtotime($date) <= strtotime($end_date)) {
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$obj->emp_utlization_process($date);
}    
 
 error_reporting(E_ALL);
ini_set('display_errors', 1);	
/*
$obj = new EmploywiseUtlizationProcessCron();

$s_date = date('Y-m-d',date(strtotime("-8 day")));
$e_date = date("Y-m-d",strtotime("+1 day"));  

$begin = new DateTime($s_date);
$end = new DateTime($e_date);
 
$interval = DateInterval::createFromDateString('1 day');

$period = new DatePeriod($begin, $interval, $end);

 // $today =  '2017-09-25' ;
// $obj->emp_utlization_process($today);     
foreach ( $period as $dt )
{
	$obj->emp_utlization_process($dt->format("Y-m-d"));     
}  
 */
?>

