<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
//ini_set('xdebug.max_nesting_level', 200);
require_once 'db_config.php';

class  UtilizationProcessNotificationCron extends MysqlConnect{

	public function cron()
	{
		$notificationSuccessCount = '';
		$notificationErrorCount = '';
		$Insertvalues = array();

		$query="SELECT p.name AS Process_name ,cl.Client_Name,v.EmployID AS MgrId 
				FROM utilization_process u 
				LEFT JOIN PROCESS p ON u.processId = p.processId
				LEFT JOIN CLIENT  cl ON cl.clientID=p.clientID  
				LEFT JOIN verticalmanager v ON v.verticalID=p.verticalID
				WHERE 
				u.Utilization >= 80 AND u.MONTH=MONTH(CURDATE()) AND u.YEAR=YEAR(CURDATE())";
		
		
		$result =  mysql_query($query) or die(mysql_error()."Query error");
		while($res = mysql_fetch_array($result)) {
			 //	$Title = "Process:".$res['Process_name']."- ClientName-".$res['Client_Name'];
			 	$Title ="Process-Utilization > 80% ";
				$Description = "The process ".$res['Process_name']." has been greater than 80 percent utilization for the client- ".$res['Client_Name'];
				$SenderEmployee = 42;
				$NotificationDate = date("Y-m-d h:i:s");
				$ReadFlag = 0;
				$Type = 7; // cross check the type 
				$DeleteFlag = 0;
				$ReceiverEmployee = $res['MgrId'];

					$Insertvalues[] =  "('".$Title."','".$Description."','".$SenderEmployee."','". $ReceiverEmployee."','".$NotificationDate."','".$ReadFlag."','".$Type."','".$DeleteFlag."')";
					// counting success notifications
					$notificationSuccessCount++;
		}
		//Inserting into notification table
		if( !empty($Insertvalues) ) {
			$query = "INSERT INTO `notification` (Title, Description, SenderEmployee, ReceiverEmployee, NotificationDate, ReadFlag, Type, DeleteFlag)
					 VALUES ". implode(',',$Insertvalues);
				if( mysql_query($query) )
				{
					//Show success status
					echo "$notificationSuccessCount  Notifications Added Successfully </br></br>";
				}
				else{
					echo "Error in Notification insertion";
				}
		}
	
	}
}

$obj  = new UtilizationProcessNotificationCron();
$obj->cron();