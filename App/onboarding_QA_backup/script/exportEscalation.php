<?php 
//http://localhost:8082/onboarding/script/exportEscalation.php?startDate=2018-04-01&endDate=2018-08-31
error_reporting(0);

ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';


class exportEscalation extends MysqlConnect
{
	function exportEsc()
	{
		$startDate = $_GET['startDate'];	
		$endDate = $_GET['endDate'];
		
		$filename = "EscalationData.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}

	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}
	
	function downloadCsv() 
	{
		$select = "SELECT error_id,DATE_FORMAT(mitigation_plan_date,'%d-%b-%Y') AS mitigation_plan_date,wor.work_order_id as wor_name, 
		DATE_FORMAT(escalation_date,'%d-%b-%Y') AS escalation_date,incidentlog_escalation.primary_lead_id as manager , production_member,
		client.client_name AS client,problem_summary, impact, impact_business, impact_revenue, impact_relationship, root_cause.name AS rootcause, 
		rootcause_analysis,mitigation_plan,
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS created_by,reviewed_by, approved_by";
		$from = " FROM incidentlog_escalation";
		$join = " LEFT JOIN client ON incidentlog_escalation.client_id = client.client_id";
        $join .= " LEFT JOIN root_cause ON incidentlog_escalation.rootcause_id = root_cause.id ";
		$join .= " LEFT JOIN employees ON incidentlog_escalation.created_by = employees.employee_id";
		$join .= " LEFT JOIN wor ON wor.wor_id = incidentlog_escalation.wor_id";
		$where = " WHERE incidentlog_escalation.status = 0 AND escalation_date BETWEEN '".$_GET['startDate']."' AND '".$_GET['endDate']."'
		AND incidentlog_escalation.client_id IN(SELECT client.client_id FROM client,client_vertical WHERE client.client_id = client_vertical.client_id AND status=1 AND vertical_id='9' )";
		
		$order = " ORDER BY incidentlog_escalation.escalation_date DESC ";
		
		$sql = $select . $from . $join . $where . $order;
		// echo $sql; exit;
		$result = mysql_query($sql);
		$key =0;
		$data = array();
		while($entry = mysql_fetch_array($result))
		{
			$data[$key]['EscalationId']			= $entry['error_id'];
			$data[$key]['Clients']				= $entry['client'];
			$data[$key]['WorkOrder']			= $entry['wor_name'];
			$data[$key]['Managers']				= str_replace(","," & ",$this->getEmployeeNames($entry['manager']));
			$data[$key]['Employees']			= str_replace(","," & ",$this->getEmployeeNames($entry['production_member']));
			$data[$key]['EscalationDate']		= $entry['escalation_date'];
			$data[$key]['EscalationStatement']	= $this->getFormatString($entry['problem_summary']);
			$data[$key]['EscalationSummary']	= $this->getFormatString($entry['impact']);
			$data[$key]['ImpactBusiness']		= ucfirst($entry['impact_business']);
			$data[$key]['ImpactRevenue']		= $entry['impact_revenue'];
			$data[$key]['ImpactRelationship']	= ucfirst($entry['impact_relationship']);
			$data[$key]['Rootcause']			= ucfirst($entry['rootcause']);
			$data[$key]['RootcauseAnalysis']	= $this->getFormatString($entry['rootcause_analysis']);
			$data[$key]['MitigationPlan']		= $this->getFormatString($entry['mitigation_plan']);
			$data[$key]['MitigationDate']		= $entry['mitigation_plan_date'];
			$data[$key]['ReviewedBy']			= str_replace(","," & ",$this->getEmployeeNames($entry['reviewed_by']));
			$data[$key]['ApprovedBy']			= str_replace(","," & ",$this->getEmployeeNames($entry['approved_by']));
			
			$key++;
		}
		
		$header = array_keys($data[0]);

		$result_result = array_merge(array(array_unique($header)), $data);
		// echo "<pre>"; print_r($result_result); exit;
		return array($result_result);
	}
	
	private function getFormatString($val)
	{
		$val = str_replace(",", "", $val);
		$val = str_replace(array("\r\n","\n","\r"), "", $val);
		
		return $val;
	}
	
	/**
	* Function to get employee names
	*/
	private function getEmployeeNames($empArr)
	{
		$empNameArr = str_replace(",", "','", $empArr);
		if($empArr != '')
		{
			$sql = "SELECT GROUP_CONCAT(IFNULL(employees.`first_name`, ''), ' ', IFNULL(employees.`last_name`, '')) AS empname 
					FROM employees 
					WHERE employee_id IN ($empArr)";
			$result = mysql_query($sql);
			$val = mysql_fetch_row($result);
		
			return $val[0];
		}
		else
		{
			return '';
		}
	}
}

$obj = new exportEscalation();
$obj->exportEsc();
?>