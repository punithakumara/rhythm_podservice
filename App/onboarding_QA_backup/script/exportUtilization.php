<?php
//http://localhost:8082/onboarding/script/exportUtilization.php?startDate=2018-12-01&endDate=2018-12-31

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportUtilization extends MysqlConnect
{
	function exportUtil()
	{
		$startDate = $_GET['startDate'];	
		$endDate = $_GET['endDate'];
		
		$filename = "UtilizationData.csv";

		$retVal = $this->downloadCsv($startDate,$endDate);
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}	

	
	function downloadCsv($startDate,$endDate) 
	{
		if($startDate!="" && $endDate!="") 
		{
			$sql = "SELECT utilization.wor_id,client.client_name As Client, wor.work_order_id,SUM(TIME_TO_SEC(utilization.total_hrs)) AS totalSecs 
			FROM `utilization`
			LEFT JOIN `client` ON `client`.`client_id` = `utilization`.`client_id`
			LEFT JOIN `wor` ON utilization.wor_id = `wor`.`wor_id` 
			WHERE utilization.task_date BETWEEN '".$startDate."' AND '".$endDate."' AND approved IN (0,1,3,4) 
			GROUP BY `utilization`.`client_id`,`utilization`.`wor_id`
			ORDER BY client_name ASC";//'0'=> Pending, "1" => Approved , "2"=> Rejected,"3"=> Weekend Billable,"4"=> Client Holiday Billable
			
			$result = mysql_query($sql);
			$loop =0;
			$data = array();
			while($val = mysql_fetch_array($result))
			{
				$data[$loop]['ClientName'] 			= $val['Client'];
				$data[$loop]['WorkOrderId'] 		= $val['work_order_id'];
				$data[$loop]['ActualHours'] 		= " ".$this->secToHours($val['totalSecs'])."";
				$data[$loop]['ApprovedFTE'] 		= $this->getFTECount($val['wor_id']);
				
				$loop++;
			}
			
			$header = array_keys($data[0]);

			$result_result = array_merge(array(array_unique($header)), $data);
			return array($result_result);
		}
	}
	
	function secToHours($totalSecs)
	{
		$t = round($totalSecs);
		return sprintf('%02d:%02d', ($t/3600),($t/60%60));
	}
	
	function getFTECount($wor_id)
	{
		$query = "SELECT SUM(CASE change_type WHEN 1 THEN no_of_fte WHEN 2 THEN -no_of_fte END) AS FTE  
		FROM fte_model 
		WHERE fk_wor_id IN ($wor_id) AND anticipated_date <= '".$_GET['endDate']."'";

		$result = mysql_query($query);
		$val = mysql_fetch_row($result);
		
		return $val[0];
	}
}

$obj = new exportUtilization();
$obj->exportUtil();
?>