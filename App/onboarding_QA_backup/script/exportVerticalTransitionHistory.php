<?php
//http://localhost:8082/onboarding/script/exportVerticalTransitionHistory.php?Month=2018-12-%

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportVerticalTransitionHistory extends MysqlConnect
{
	function exportHistory()
	{
		$filename = "verticalHistory.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}	

	
	function downloadCsv() 
	{
		$index =0;
		$data = array();
		
		$sql = "SELECT DISTINCT employee_id,vertical_id,end_date FROM `employee_vertical_history` WHERE end_date LIKE '".$_GET['Month']."'";		
		
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{
			$empData = $this->getEmployeeDetails($val['employee_id']);
			$data[$index]['EmpID'] 			= $empData['EmpID'];
			$data[$index]['EmpName'] 			= $empData['EmpName'];
			$data[$index]['PreviousVertical'] 	= $this->getVerticalName($val['vertical_id']);
			$data[$index]['CurrentVertical'] 	= $empData['CurrentVertical'];
			$data[$index]['TransitionDate'] 	= date("d-M-Y", strtotime($val['end_date']));
			
			$index++;
		}
		
		$header = array_keys($data[0]);
		$result_result = array_merge(array(array_unique($header)), $data);
		
		return array($result_result);
	}	
	
	private function getEmployeeDetails($employee_id)
	{
		$sql = "SELECT employee_id,company_employ_id AS EmpID,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `EmpName`,name AS CurrentVertical 
				FROM employees 
				JOIN verticals ON verticals.vertical_id = employees.assigned_vertical 
				WHERE status!=6 AND employee_id ='".$employee_id."'";

		$result = mysql_query($sql);
		$val = mysql_fetch_array($result);
		
		return $val;
	}
	
	public function getVerticalName($vert_id)
	{
		$sql = "SELECT name FROM `verticals` WHERE vertical_id IN($vert_id)";

		$result = mysql_query($sql);
		$val = mysql_fetch_row($result);
		
		return $val[0];
	}
	
}

$obj = new exportVerticalTransitionHistory();
$obj->exportHistory();
?>