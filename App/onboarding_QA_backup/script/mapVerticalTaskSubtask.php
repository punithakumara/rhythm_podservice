<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes

// http://localhost:8082/onboarding/script/mapVerticalTaskSubtask.php

class mapVerticalTaskSubtask
{
	function __construct() 
	{
		$this->con = mysqli_connect('localhost','root','') or die("Unable to connect to MySQL");
		mysqli_select_db($this->con, 'rhythm_live');
    }
	
	
	function update($filename)
	{
		// path where your CSV file is located		
		$csv_file = $filename; 
		
		if (($handle = fopen($csv_file, "r")) !== FALSE) 
		{
			fgetcsv($handle);   		
			$recordid = 0;
			/*Rowid Employee id Old Grade Old Designation New Grade New Designation Date Effective*/
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				$num = count($data);				
				for ($c=0; $c < $num; $c++) 
				{
					$col[$c] = $data[$c];
				}
				$client = ($col[0]=="Theorem")?221:0;
				$taskId = $this->getTaskId($col[1]);
				$subTaskId = $this->getSubTaskId($col[2]);
				$categoryId = $this->getCategoryId($col[3]);
				
				if($col[4]=="Y") //Adtech
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",6,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[5]=="Y") //Martech
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",9,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[6]=="Y") //Search
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",11,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[7]=="Y") //Reporting
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",8,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[8]=="Y") //Media Review
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",12,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[9]=="Y") //Tech Services
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",7,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[10]=="Y") //PEG
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",3,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[11]=="Y") //CAM
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",22,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[12]=="Y") //Admin
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",4,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[13]=="Y") //IT
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",2,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[14]=="Y") //HR
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",10,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[15]=="Y") //DelEX
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",5,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[16]=="Y") //PMO & Compliance
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",14,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[17]=="Y") //Finance
				{
					$sql = "INSERT INTO `timesheet_vertical_task_subtask_map`(client,vertical_id,timesheet_task_code_id,timesheet_subtask_id,timesheet_category_id) 
					VALUES(".$client.",13,".$taskId.",'".$subTaskId."','".$categoryId."')";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				$recordid ++;
			}
			echo "Inserted Records id : $recordid";
			
			fclose($handle);
		}
	}

	function getTaskId($task_name)
	{
		$sqlTaskcode = "SELECT timesheet_task_code_id FROM timesheet_task_codes WHERE task_name ='".$task_name."'";
		$query = mysqli_query($this->con,$sqlTaskcode);
		$result = mysqli_fetch_assoc($query);
		
		return $result['timesheet_task_code_id'];
	}

	function getSubTaskId($subtask_name)
	{
		$sql = "SELECT timesheet_subtask_id FROM timesheet_subtasks WHERE sub_task_name ='".$subtask_name."'";
		$query = mysqli_query($this->con,$sql);
		$result = mysqli_fetch_assoc($query);
		
		return $result['timesheet_subtask_id'];
	}

	function getCategoryId($category_name)
	{
		$sql = "SELECT category_id FROM timesheet_categories WHERE category_name ='".$category_name."'";
		$query = mysqli_query($this->con,$sql);
		$result = mysqli_fetch_assoc($query);
		
		return $result['category_id'];
	}
}

$obj = new mapVerticalTaskSubtask();
$obj->update('Rhythm_Vertical_Task_Subtasks.csv');
?>