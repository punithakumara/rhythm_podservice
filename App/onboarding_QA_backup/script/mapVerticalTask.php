<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes

// http://localhost:8082/onboarding/script/mapVerticalTask.php

class mapVerticalTask
{
	function __construct() 
	{
		$this->con = mysqli_connect('localhost','root','') or die("Unable to connect to MySQL");
		mysqli_select_db($this->con, 'rhythm_live');
    }
	
	
	function update($filename)
	{
		// path where your CSV file is located		
		$csv_file = $filename; 
		
		if (($handle = fopen($csv_file, "r")) !== FALSE) 
		{
			fgetcsv($handle);   		
			$recordid = 0;
			/*Rowid Employee id Old Grade Old Designation New Grade New Designation Date Effective*/
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				$num = count($data);				
				for ($c=0; $c < $num; $c++) 
				{
					$col[$c] = $data[$c];
				}
				
				$taskId = $this->getTaskId($col[0]);
				
				if($col[1]=="Y") //Adtech
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(6,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[2]=="Y") //Martech
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(9,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[3]=="Y") //Search
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(11,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[4]=="Y") //Reporting
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(8,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[5]=="Y") //Media Review
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(12,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[6]=="Y") //Tech Services
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(7,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[7]=="Y") //PEG
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(3,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[8]=="Y") //CAM
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(22,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[9]=="Y") //Admin
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(4,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[10]=="Y") //IT
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(2,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[11]=="Y") //HR
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(10,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[12]=="Y") //DelEX
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(5,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[13]=="Y") //PMO & Compliance
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(14,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				if($col[14]=="Y") //Finance
				{
					$sql = "INSERT INTO `timesheet_vertical_task_map`(vertical_id,timesheet_task_code_id) 
					VALUES(13,".$taskId.")";
					$query = mysqli_query($this->con,$sql) or die(mysqli_error($this->con));
				}
				$recordid ++;
			}
			echo "Inserted Records id : $recordid";
			
			fclose($handle);
		}
	}

	function getTaskId($task_name)
	{
		$sqlTaskcode = "SELECT timesheet_task_code_id FROM timesheet_task_codes WHERE task_name ='".$task_name."'";
		$query = mysqli_query($this->con,$sqlTaskcode);
		$result = mysqli_fetch_assoc($query);
		
		return $result['timesheet_task_code_id'];
	}
}

$obj = new mapVerticalTask();
$obj->update('Rhythm_Vertical_Task.csv');
?>