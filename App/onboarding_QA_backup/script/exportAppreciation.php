<?php 
//http://localhost:8082/onboarding/script/exportAppreciation.php?startDate=2018-04-01&endDate=2018-08-31
error_reporting(0);

ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';


class exportAppreciation extends MysqlConnect
{
	function exportAppr()
	{
		$startDate = $_GET['startDate'];	
		$endDate = $_GET['endDate'];
		
		$filename = "AppreciationData.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}

	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}
	
	function downloadCsv() 
	{
		$select = "SELECT appr_id,DATE_FORMAT(appr_date,'%d-%b-%Y') AS appr_date, wor.work_order_id as wor_name,
				responsible_member,incidentlog_appreciations.team_lead as team_leader,client.client_name AS Client,appr_by,
				incidentlog_appreciations.description,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS createdBy,
				CONCAT(IFNULL(atl.`first_name`, ''),' ',IFNULL(atl.`last_name`, '')) AS team_lead_name,attachment,createdOn";
		$from = " FROM incidentlog_appreciations";
		$join = " LEFT JOIN client ON incidentlog_appreciations.client_id = client.client_id";
		$join .= " LEFT JOIN employees ON incidentlog_appreciations.createdBy = employees.employee_id";
		$join .= " LEFT JOIN employees atl ON incidentlog_appreciations.team_lead = atl.employee_id";
		$join .= " LEFT JOIN wor ON wor.wor_id = incidentlog_appreciations.wor_id";
		$where = " WHERE incidentlog_appreciations.status=0 AND appr_date BETWEEN '".$_GET['startDate']."' AND '".$_GET['endDate']."'
		AND incidentlog_appreciations.client_id IN(SELECT client.client_id FROM client,client_vertical WHERE client.client_id = client_vertical.client_id AND status=1 AND vertical_id='9' )";
		
		$order = " ORDER BY appr_date DESC ";
		
		$sql = $select . $from . $join . $where . $order;
		// echo $sql; exit;
		$result = mysql_query($sql);
		$key =0;
		$data = array();
		while($entry = mysql_fetch_array($result))
		{
			$data[$key]['AppreciationId']	= $entry['appr_id'];
			$data[$key]['Clients']			= $entry['Client'];
			$data[$key]['WorkOrder']		= $entry['wor_name'];
			$data[$key]['TeamLead']			= $entry['team_lead_name'];
			$data[$key]['Employees']		= str_replace(","," & ",$this->getEmployeeNames($entry['responsible_member']));
			$data[$key]['AppreciatedDate']	= $entry['appr_date'];
			$data[$key]['AppreciatedBy']	= $entry['appr_by'];
			$data[$key]['Description']		= $this->getFormatString($entry['description']);
			
			$key++;
		}
		
		$header = array_keys($data[0]);

		$result_result = array_merge(array(array_unique($header)), $data);
		// echo "<pre>"; print_r($result_result); exit;
		return array($result_result);
	}
	
	private function getFormatString($val)
	{
		$val = str_replace(",", "", $val);
		$val = str_replace(array("\r\n","\n","\r"), "", $val);
		
		return $val;
	}
	
	/**
	* Function to get employee names
	*/
	private function getEmployeeNames($empArr)
	{
		$empNameArr = str_replace(",", "','", $empArr);
		if($empArr != '')
		{
			$sql = "SELECT GROUP_CONCAT(IFNULL(employees.`first_name`, ''), ' ', IFNULL(employees.`last_name`, '')) AS empname 
					FROM employees 
					WHERE employee_id IN ($empArr)";
			$result = mysql_query($sql);
			$val = mysql_fetch_row($result);
		
			return $val[0];
		}
		else
		{
			return '';
		}
	}
}

$obj = new exportAppreciation();
$obj->exportAppr();
?>