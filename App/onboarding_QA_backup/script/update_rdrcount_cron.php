<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
date_default_timezone_set('Asia/Kolkata');
require_once 'db_config.php';
class RDRHeadCountUpdateCron extends MysqlConnect{
	private function _getRampDownDownDate()
	{
		$sql = "SELECT client_rdr.RDR_ID,client_rdr.AddedBy , client_rdr.ClientID, 
			        	DATE_FORMAT(client_rdr.Date_Of_Request, '%d-%m-%Y') AS Date_Of_Request,
						CONCAT(IFNULL(employ.`FirstName`, ''),' ',IFNULL(employ.`LastName`, '')) AS RaisedName, 
			        	client_rdr.VerticalID, client_rdr.ProcessID, DATE_FORMAT(client_rdr.Rampdown_Date, '%d-%m-%Y') AS Rampdown_Date,
						client_rdr.Reason, client_rdr.Theorem_Loss, client_rdr.Flag, client.Client_Name AS client_Name,
					  	vertical.Name AS Verticl_Name, process.Name AS Process_Name,
						GROUP_CONCAT(shifts_resources_rdr.Resources,'@') AS Resources,
					    GROUP_CONCAT(shifts_resources_rdr.No_of_Resources) AS No_of_Resources,
					    GROUP_CONCAT(shifts_resources_rdr.ShiftID) AS ShiftID 
						FROM client_rdr
        				JOIN PROCESS ON client_rdr.ProcessID = process.ProcessID
						JOIN CLIENT ON client_rdr.ClientID = client.ClientID
						JOIN vertical ON client_rdr.VerticalID = vertical.VerticalID
                        LEFT JOIN `employ` ON client_rdr.AddedBy = employ.EmployID
						LEFT JOIN `shifts_resources_rdr` ON client_rdr.RDR_ID =shifts_resources_rdr.RDR_ID
						WHERE 1=1  AND client_rdr.Flag not in(0,4) AND client_rdr.rampdown_date = '".date('Y-m-d')."' GROUP BY client_rdr.RDR_ID";
		$query = mysql_query($sql) or die(mysql_error()."_getRampDownDownDate");
		if(mysql_num_rows($query) == 0) 
			return FALSE;
		else
		{
			$result = array();
			for($i = 0; $i < mysql_num_rows($query); $i++){
				$r = mysql_fetch_array($query);
				$key = array_keys($r);
				for($x = 0; $x < count($key); $x++){
					// Sanitizes keys so only alphavalues are allowed
					if(!is_int($key[$x])){
						if(mysql_num_rows($query) >= 1){
							$result[$i][$key[$x]] = $r[$key[$x]];
						}else if(mysql_num_rows($query) < 1){
							$result = null;
						}
					}
				}
			}
			print_r($result);
			return $result;
		}	
	}
	
	public function updateHeadCount()
	{
		$DataArray = $this->_getRampDownDownDate();
		$j=0;
		$total_resource_count = 0;
		if(is_array($DataArray) ) 
		{
			foreach($DataArray as $key => $val)
			{
				if(strtotime($val['Rampdown_Date']) == strtotime(date('Y-m-d')))
				{
					$previous_cnt_sql   = "SELECT SUM(No_of_Resources) AS No_of_Resources FROM `shifts_resources_rdr` WHERE RDR_ID='" . $val['RDR_ID'] . "'";
					echo $previous_cnt_sql;
					$previous_cnt_query = mysql_query($previous_cnt_sql);
					if(mysql_num_rows($previous_cnt_query) > 0) 			
					{
							$Resource = mysql_fetch_row ($previous_cnt_query);
							$total_resource_count  = $Resource[0];
							echo $total_resource_count;
							$sql = "UPDATE PROCESS SET HeadCount = (HeadCount-" . $total_resource_count . "),
									ForcastRDR = IF(ForcastRDR > 0, ForcastRDR - ".$total_resource_count.",ForcastRDR)
									WHERE ProcessID='" . $val['ProcessID'] . "'";
						   echo "\r\n".$sql;
							$query = mysql_query($sql);
							$j++;
					}			
				}
				//echo "Updated Rows=>".$val['RDR_ID'];
			}
		}
		
	}
	
}
$obj = new RDRHeadCountUpdateCron();
echo "\r\n";
echo date("l jS \of F Y h:i:s A") ;
$obj->updateHeadCount();
?>