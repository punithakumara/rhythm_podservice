<?php
//http://localhost:8082/onboarding/script/new_joiners_notification.php

require_once('../phpmailer/class.phpmailer.php');

$mysqli = new mysqli("localhost", "root", "", "task");

$arr_insert=array();
$inc_insert=0;
$message_insert = "";
$query="select employee.employeeID,employee.name,employee.email,employee_vertical.vertical,employee_designation.designation,alteredstatus.oldstatus,
alteredstatus.newstatus,alteredstatus.operation,alteredstatus.num 
from alteredstatus 
inner join employee on employee.employeeID=alteredstatus.employeeID 
inner join employee_vertical on employee.vertical=employee_vertical.verticalID
inner join employee_designation on employee.designation=employee_designation.designationID
WHERE mailstatus='no' AND operation='insert'";
$result=$mysqli->query($query);
 while($row = mysqli_fetch_assoc($result)){
	
		$arr_insert[$inc_insert]['name']=$row['name'];
        $arr_insert[$inc_insert]['employeeID']=$row['employeeID']; 
        $arr_insert[$inc_insert]['email']=$row['email']; 
        $arr_insert[$inc_insert]['designation']=$row['designation'];
		$arr_insert[$inc_insert]['vertical']=$row['vertical'];
		$arr_insert[$inc_insert]['num']=$row['num'];
		$inc_insert++;
		
 }

if($inc_insert==0)
{
	echo "no new insertion";
}
else
{
	$message_insert .= "
	<html>
	<head>
		<title>Insertion
		</title>
		<meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <style>
  table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
	align:left;
  }
  th{
  background-color:#007bff;
  color:white;
  }
  td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
  }
  
  tr:nth-child(even) {
	background-color: #dddddd;
  }
  .container{
  font-size:1rem;
  font-weight:400;
  line-height:1.5;
  color:#212529;
  text-align:left;
  background-color:#fff;
  }
  .foot{
  height:50px;
  text-align:center;
  color:white;
  text-decoration:none;
  background-color:#007bff;
  font-family:SFMono-Regular,Menlo,Monaco,Consolas,'Liberation Mono','Courier New',monospace;
  font-size:1em;
  margin-top:0;
  margin-bottom:1rem;
  overflow:auto;
  }
  .caption{
  padding-top:.75rem;
  padding-bottom:.75rem;
  background-color:#6c757d;
  text-align:left;
  caption-side:bottom;
  text-align:inherit;
  display:inline-block;
  margin-bottom:.5rem;
  }

</style>
	</head>
	<body>
	
	
	<div class='jumbotron text-center'  style='background-color:light-blue;height:200px'>
	<img align='left' src='https://rhythm.theoreminc.net/resources/images/rhythm_logo_new.png' /></br></br></br>
	
	
	<p style='text-align:left;'>Dear HR Team,</p>
	<p  style='text-align:left;'>New Joined Employees has been successfully added.</p>
	
		
	</div>
	<div class='container text-center'>
		

<table>
  <tr>
    
    <th>EmployeeID</th>
	<th>Employee Name</th>
    <th>Email</th>
	<th>Designation</th>
	<th>Vertical</th>
  </tr>
 
";
  
  for($i=0;$i<$inc_insert;$i++){
 $message_insert.=" <tr>
    <td>".$arr_insert[$i]['employeeID']."</td>
    <td>".$arr_insert[$i]['name']."</td>
    <td>".$arr_insert[$i]['email']."</td>
    <td>".$arr_insert[$i]['designation']."</td>
    <td>".$arr_insert[$i]['vertical']."</td>
   
  </tr>";
  }
  
 $message_insert.="</table>

	</div>
	</br>
	</br>
	<p >    Regards,</br>
	<b>Rhythm Support Team</b></p>
	</br>
	</br>
	</br>
	
	<footer class='page-footer font-small special-color-dark'>
<div class='container'>

<div class='foot p-3 mb-2 bg-primary text-white footer-copyright text-center  py-3 fixed-bottom'>&copy; 2019 Theorem Inc., -All Rights Reserved v3.3
  </div>
 </div>

</footer>
	</body>
</html>


";
}


$mail = new PHPMailer(true);
$mail->IsSMTP();                       // telling the class to use SMTP
$mail->SMTPDebug = 0;                  

// 0 = no output, 1 = errors and messages, 2 = messages only.
$mail->SMTPDebug = 1; 
$mail->SMTPAuth = true;                // enable SMTP authentication 
$mail->SMTPSecure = "tls";              // sets the prefix to the servier
$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
$mail->Port = 587;                    // set the SMTP port for the GMAIL 
$mail->Username = "rhythmsupport";  // Gmail username
$mail->Password = "rhy_th499";      // Gmail password
$mail->ContentType = 'text/html'; 
$mail->IsHTML(true);
$mail->CharSet = 'windows-1250';
$mail->From = 'rhythmsupport@theoreminc.net';
$mail->FromName = 'Rhythmsupport@theoreminc.net';
$mail->addReplyTo('Rhythmsupport@theoreminc.net');
$mail->Subject = 'Rhythm | Employee - New Joiner(s) Detail';
$mail->Body = $message_insert;			
$mail->AddAddress("sathish.kumar@theoreminc.net");
$result=$mail->Send();
echo "<pre>";
print_r($result);

/*for($i=0;$i<$inc_insert;$i++)
{
	$query="update alteredstatus set mailstatus='yes' where num='".$arr_insert[$i]['num']."'";
	$result=$mysqli->query($query);
}*/



?>