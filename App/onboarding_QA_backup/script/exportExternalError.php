<?php 
//http://localhost:8082/onboarding/script/exportExternalError.php?startDate=2018-08-01&endDate=2018-08-31
error_reporting(0);

ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';
	
class exportExternalError extends MysqlConnect
{
	function exportError()
	{
		$startDate = $_GET['startDate'];	
		$endDate = $_GET['endDate'];
		
		$filename = "ErrorData.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}

	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}
	
	function downloadCsv() 
	{
		$select = "SELECT error_id,error_categories.category_name as cat_name,err_cat.category_name AS subcat_name, wor.work_order_id as wor_name, DATE_FORMAT(error_date,'%d-%b-%Y') AS error_date,
				qa_member,DATE_FORMAT(error_notified_date,'%d-%b-%Y') AS error_notified_date,
				DATE_FORMAT(correction_date,'%d-%b-%Y') AS correction_date,qa_member AS responsibleQA,
				DATE_FORMAT(corrective_action_date,'%d-%b-%Y') AS corrective_action_date,production_member AS responsiblePerson,
				error_categories.error_category_id,production_member,client.client_name AS Client,activity,problem_summary,
				units_impacted,impact,impact_business,impact_revenue,impact_relationship,root_cause.name AS rootcause,rootcause_analysis,correction, 
				reviewed_by, approved_by,corrective_action,CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS createdBy, 
				external_errors.team_lead as team_leader, external_errors.associate_manager, 
				CONCAT(IFNULL(etl.`first_name`, ''),' ',IFNULL(etl.`last_name`, '')) AS team_lead_name,
				CONCAT(IFNULL(eam.`first_name`, ''),' ',IFNULL(eam.`last_name`, '')) AS associate_manager_name,createdOn ";
		$from = " FROM external_errors";
		$join = " LEFT JOIN error_categories ON external_errors.error_category = error_categories.error_category_id";
        $join .= " LEFT JOIN error_categories AS err_cat ON external_errors.error_subcategory = err_cat.error_category_id";
        $join .= " LEFT JOIN root_cause ON external_errors.rootcause_id = root_cause.id ";
		$join .= " LEFT JOIN client ON external_errors.client_id = client.client_id";
		$join .= " LEFT JOIN wor ON wor.wor_id = external_errors.wor_id";
		$join .= " LEFT JOIN employees ON external_errors.createdBy = employees.employee_id";
		$join .= " LEFT JOIN employees etl ON external_errors.team_lead = etl.employee_id";
		$join .= " LEFT JOIN employees eam ON external_errors.associate_manager = eam.employee_id";
		$where = " WHERE external_errors.status=0 AND error_date BETWEEN '".$_GET['startDate']."' AND '".$_GET['endDate']."'
		AND external_errors.client_id IN(SELECT client.client_id FROM client,client_vertical WHERE client.client_id = client_vertical.client_id AND status=1 AND vertical_id='9' )";

		$order = " ORDER BY external_errors.error_date DESC";
		
		$sql = $select . $from . $join . $where . $order;
		// echo $sql; exit;
		$result = mysql_query($sql);
		$key =0;
		$data = array();
		while($entry = mysql_fetch_array($result))
		{
			$data[$key]['ErrorId']				= $entry['error_id'];
			$data[$key]['Client']				= $entry['Client'];
			$data[$key]['ErrorCategory']		= $entry['cat_name'];
			$data[$key]['ErrorSubCategory']		= $entry['subcat_name'];
			$data[$key]['WorkOrder']			= $entry['wor_name'];
			$data[$key]['AssociateManager']		= $entry['associate_manager_name'];
			$data[$key]['TeamLead']				= $entry['team_lead_name'];
			$data[$key]['ProductionMember']		= str_replace(","," & ",$this->getEmployeeNames($entry['responsiblePerson']));
			$data[$key]['QAMember']				= str_replace(","," & ",$this->getEmployeeNames($entry['responsibleQA']));
			$data[$key]['ErrorDate']			= $entry['error_date'];
			$data[$key]['ErrorNotifiedDate']	= $entry['error_notified_date'];
			$data[$key]['ErrorStatement']		= $this->getFormatString($entry['problem_summary']);
			$data[$key]['ErrorSummary']			= $this->getFormatString($entry['impact']);
			$data[$key]['UnitsImpacted']		= $entry['units_impacted'];
			$data[$key]['ImpactBusiness']		= ucfirst($entry['impact_business']);
			$data[$key]['ImpactRevenue']		= $entry['impact_revenue'];
			$data[$key]['ImpactRelationship']	= ucfirst($entry['impact_relationship']);
			$data[$key]['Rootcause']			= ucfirst($entry['rootcause']);
			$data[$key]['RootcauseAnalysis']	= $this->getFormatString($entry['rootcause_analysis']);
			$data[$key]['CorrectionDate']		= $entry['correction_date'];
			$data[$key]['CorrectiveActionDate']	= $entry['corrective_action_date'];
			$data[$key]['Correction']			= $this->getFormatString($entry['correction']);
			$data[$key]['CorrectiveAction']		= $this->getFormatString($entry['corrective_action']);
			$data[$key]['ReviewedBy']			= $this->getEmployeeNames($entry['reviewed_by']);
			$data[$key]['ApprovedBy']			= $this->getEmployeeNames($entry['approved_by']);
			
			$key++;
		}
		
		$header = array_keys($data[0]);

		$result_result = array_merge(array(array_unique($header)), $data);
		// echo "<pre>"; print_r($result_result); exit;
		return array($result_result);
	}
	
	private function getFormatString($val)
	{
		$val = str_replace(",", "", $val);
		$val = str_replace(array("\r\n","\n","\r"), "", $val);
		
		return $val;
	}
	
	/**
	* Function to get employee names
	*/
	private function getEmployeeNames($empArr)
	{
		$empNameArr = str_replace(",", "','", $empArr);
		if($empArr != '')
		{
			$sql = "SELECT GROUP_CONCAT(IFNULL(employees.`first_name`, ''), ' ', IFNULL(employees.`last_name`, '')) AS empname 
					FROM employees 
					WHERE employee_id IN ($empArr)";
			$result = mysql_query($sql);
			$val = mysql_fetch_row($result);
		
			return $val[0];
		}
		else
		{
			return '';
		}
	}

}

$obj = new exportExternalError();
$obj->exportError();
?>