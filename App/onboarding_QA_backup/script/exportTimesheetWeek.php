<?php
//http://localhost:8082/onboarding/script/exportTimesheetWeek.php

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
require_once 'db_config.php';

class exportTimesheet extends MysqlConnect
{
	function exportTime()
	{
		$filename = "Timesheet.csv";

		$retVal = $this->downloadCsv();
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}	
	
	function secToHours($timeData,$prodData)
	{
		$totalSecs = $timeData + $prodData;
		$t = round($totalSecs);
		
		return sprintf('%02d:%02d', ($t/3600),($t/60%60));
	}
	
	function downloadCsv() 
	{
		$index =0;
		$data = array();
		
		$sql = "SELECT employees.employee_id, employees.company_employ_id, 
		CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
		designation.name AS designation, hcm_grade AS grade, verticals.name AS Vertical,
		CONCAT(IFNULL(sup.first_name, '') ,' ', IFNULL(sup.last_name,'')) AS SupervisorName 
		FROM employees 
		LEFT JOIN designation on designation.designation_id = employees.designation_id
		LEFT JOIN employees sup ON sup.employee_id = employees.primary_lead_id 
		LEFT JOIN employee_vertical ON employee_vertical.employee_id = employees.employee_id 
		LEFT JOIN verticals on verticals.vertical_id = employee_vertical.vertical_id 
		WHERE employees.status!=6 AND employees.location_id!=4";		
		// echo $sql; exit;
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{
			$timesheetApprovedHrs = $this->getTimeTaken($val['employee_id'],'67',2);
			$timesheetPendingHrs = $this->getTimeTaken($val['employee_id'],'67',1);
			$utlizationHrs = $this->getProductionTime($val['employee_id'],'2019-04-08','2019-04-14','0,1,3,4');
			$totalHrs = $timesheetApprovedHrs + $timesheetPendingHrs + $utlizationHrs;
			
			$data[$index]['Vertical'] 		= $val['Vertical'];
			$data[$index]['Employee ID'] 	= $val['company_employ_id'];
			$data[$index]['Name'] 			= $val['FullName'];
			$data[$index]['Grade'] 			= $val['grade'];
			$data[$index]['Designation'] 	= $val['designation'];
			$data[$index]['Supervisor'] 	= $val['SupervisorName'];
			$data[$index]['Approved(hrs)'] 	= "".$this->secToHours($timesheetApprovedHrs,$utlizationHrs)."";
			$data[$index]['Pending(hrs)'] 	= sprintf('%02d:%02d', ($timesheetPendingHrs/3600),($timesheetPendingHrs/60%60));
			$data[$index]['Total(hrs)'] 	= sprintf('%02d:%02d', ($totalHrs/3600),($totalHrs/60%60));
			
			$index++;
		}
		
		$header = array_keys($data[0]);
		$result_result = array_merge(array(array_unique($header)), $data);
		return array($result_result);
	}
	
	public function getTimeTaken($employee_id,$week_code_id, $status)//0-Rejected, 1-Pending, 2-Approved
	{
		$sql = "SELECT SUM(TIME_TO_SEC(day1))+SUM(TIME_TO_SEC(day2))+SUM(TIME_TO_SEC(day3))+SUM(TIME_TO_SEC(day4))+SUM(TIME_TO_SEC(day5))+SUM(TIME_TO_SEC(day6))+SUM(TIME_TO_SEC(day7))	AS status
			FROM `timesheet_task_mapping` 
			WHERE task_week_id = '".$week_code_id."' AND emp_id='".$employee_id."'"; 
		if($status!="")
		{
			$sql .= " AND status IN (".$status.")"; 
		}
	// echo $sql; exit;
		$result = mysql_query($sql);
		$result = mysql_fetch_row($result);
		
		return $result[0];
	}
	
	public function getProductionTime($employee_id, $start_date, $end_date, $status)//'0'=> Pending, "1" => Approved , "2"=> Rejected,"3"=> Weekend Billable,"4"=> Client Holiday Billable
	{
		$sql = "SELECT SUM(TIME_TO_SEC(theorem_hrs)) AS TimeTaken
			FROM `utilization` 
			WHERE employee_id='".$employee_id."' AND status IN (".$status.") AND task_date BETWEEN '".$start_date."' AND '".$end_date."'"; 

	// echo $sql; exit;
		$result = mysql_query($sql);
		$result = mysql_fetch_row($result);
		
		return $result[0];
	}
}

$obj = new exportTimesheet();
$obj->exportTime();
?>