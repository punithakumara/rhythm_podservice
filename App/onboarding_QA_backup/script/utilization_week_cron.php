<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';
class UtlizationWeekCron extends MysqlConnect {
	
	public $expectedHours = 0;
	public $actualHours = 0;
	public $processArray = array();
	public $CurrentWeek;
	public $CurrentYear;
	
    function __construct() {
       parent::__construct();
       $this->CurrentWeek = date('W');
       $this->CurrentYear = date('Y');
       /*$this->CurrentWeek = '10';
       $this->CurrentYear = '2014'; */
    }
	
	/* @description : Setting All Process for Current Month */
	public function setProcessArray(){
		// $sql = "SELECT ProcessID,TaskDate FROM utilization WHERE WEEK(TaskDate) =  '".$this->CurrentWeek."' AND YEAR(TaskDate) = '".$this->CurrentYear."' GROUP BY ProcessID ";
		$sql = "SELECT * FROM (
				   SELECT ProcessID,TaskDate FROM utilization WHERE WEEK(TaskDate) =  '".$this->CurrentWeek."' AND YEAR(TaskDate) = '".$this->CurrentYear."' 
				) AS t1
				GROUP BY t1.ProcessID";
		
		$result = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($result)==0) return false;
		while ($row = mysql_fetch_array($result)){
    		$this->processArray[] = $row['ProcessID'];
		}
		// echo '<pre>';
		// print_r($this->processArray);exit;
	}
	
	/* @description : 
	 * step 1 - loop through process array
	 * step 2 - getting all employee for each process.
	 * step 3 - looping employee array and calculating expected and actual hours 
	 * step 4 - caculating utilization percent
	 *  */
	public function calculateExpectedandToalHours()
	{
		if(count($this->processArray)>0)
		{
			foreach($this->processArray as $key=>$processID)
			{
				$this->expectedHours = 0;
				$this->actualHours = 0;
				//$sql = "SELECT DISTINCT EmployID FROM utilization WHERE ProcessID = ".$processID." AND DATE_FORMAT(TaskDate,'%Y-%m ') =  DATE_FORMAT('2014-05-01','%Y-%m')";
				$sql = "SELECT DISTINCT EmployID FROM employprocess WHERE ProcessID = $processID";
				$result =  mysql_query($sql);
				$totalEmp = mysql_num_rows($result);
				if(mysql_num_rows($result)!=0)
				{
					while ($row = mysql_fetch_array($result)){
		    			$this->_setExpectedHours($row['EmployID'],$processID);
		    			$this->_setActualHours($row['EmployID'],$processID); 
					}
					$this->_updateUtilizationProcess($totalEmp,$processID);
				}	
			}
		}
	}
	
	/*@description : setting expected Hours (count of total workingdays * 8 ) */
	private function _setExpectedHours($empID,$processID){
		
		$sql = "SELECT  COUNT(DISTINCT(TaskDate)) AS totalWorkedDays  FROM utilization WHERE EmployID = $empID AND ProcessID = $processID AND WEEK(TaskDate) = '".$this->CurrentWeek."' AND YEAR(TaskDate) = '".$this->CurrentYear."'";
		$result = mysql_query($sql) or die(mysql_error());
		if($row = mysql_fetch_array($result))
		{
			if($row['totalWorkedDays']> 0)
				$this->expectedHours += $row['totalWorkedDays'] * 8;		
		}
		// echo $this->expectedHours;
		// echo '<br>';
	}
	/*@description : setting Actual Hours entered bu employee */
	private function _setActualHours($empID,$processID){
		$sql = "SELECT SUM(MIN / 60) + SUM(Hours) AS totalHour FROM utilization WHERE  EmployID = ".$empID." AND  ProcessID = ".$processID." AND WEEK(TaskDate) = '".$this->CurrentWeek."' AND YEAR(TaskDate) = '".$this->CurrentYear."'";
		$result = mysql_query($sql);
		if(mysql_num_rows($result)==0) return false;
		$row = mysql_fetch_array($result);
		$this->actualHours += $row['totalHour']; 
		// echo $this->actualHours;
		// echo '<br>';
	}
	
	/*
	 * @description :
	 * Step1 - calucating final expected and actual hours by dividing total employee for each procces
	 * Step2 - If processID already exist then updating else inserting into database
	 */
	private function _updateUtilizationProcess($totalEmp,$processID)
	{
		$totalUtlization = 0;
		$fianlExpectedHours =  ($this->expectedHours / $totalEmp);
		$finalActualHours = ($this->actualHours / $totalEmp);
		
		if($fianlExpectedHours!=0)
		{
			$totalUtlization =  ($finalActualHours / $fianlExpectedHours)*100;

			$sql = "SELECT UID,ProcessID FROM utilization_weekly_process WHERE  ProcessID = $processID AND WEEK =  '".$this->CurrentWeek."' AND YEAR ='".$this->CurrentYear."'";
			$result = mysql_query($sql);
			if($row = mysql_fetch_array($result))
			{
				$query = "UPDATE utilization_weekly_process SET ProcessID = ".$processID.", Week='".$this->CurrentWeek."', Year='".$this->CurrentYear."',ExpectedHours= '".round($fianlExpectedHours,2)."',ActualHours= '".round($finalActualHours,2)."',Utilization=".round($totalUtlization,2)." where UID = ".$row['UID'];
				echo $query;
			}
			else
			{
				$query = "insert into utilization_weekly_process (ProcessID,Week,Year,ExpectedHours,ActualHours,Utilization)values (".$processID.",'".$this->CurrentWeek."', '".$this->CurrentYear."' ,'".round($fianlExpectedHours,2)."','".round($finalActualHours,2)."','".round($totalUtlization,2)."')";
				// echo $sql;
			}
			$rslt = mysql_query($query) or die(mysql_error()."_updateUtilizationProcess");
		}
	}
}
$obj  = new UtlizationWeekCron();
$obj->setProcessArray();
$obj->calculateExpectedandToalHours();
?>

