<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
require_once 'db_config.php';
class UtlizationImport extends MysqlConnect{

	public $Data;
	public $alreadyExist = 0;
	public $Failed = 0;
	public $Inseted=0;
	public $empIdMissing = 0;
	public $clientIDMissing = 0;
	public $processIDMissing = 0;
	public $failedProcessArray =  array();
	public $failedClientArray =  array();
	
	/* @description : reading csv file */
	public function readCsvFile($filename)
	{
		include 'CSVReader.php';
		$csv = new CSVReader();
		$this->Data = $csv->parse_file($filename,true);
	} 
	
	
	/*
	* @description : getting employID
	*/
	private function _getEmployID($companyEmpID){
		if($companyEmpID!="" && $companyEmpID!='NA' && is_numeric($companyEmpID))
		{
			$sql = "SELECT EmployID FROM employ WHERE CompanyEmployID = ".$companyEmpID;
			$result = mysql_query($sql);
			if(mysql_num_rows($result) == 0)
				return FALSE;
			else
			{
				$row = mysql_fetch_array($result);
				return $row['EmployID'];
			}	
		}
		else
			return FALSE;
	}
	
	
   /*
	* @description : getting clientID
	*/
	private function _getClientID($clientName) {
		if($clientName!="")
		{
			$sql = "SELECT ClientID FROM CLIENT WHERE LOWER(TRIM(Client_Name))='".mysql_real_escape_string(strtolower(trim($clientName)))."'";
			$result = mysql_query($sql);
			if(mysql_num_rows($result) == 0)
			{
				array_push($this->failedClientArray,$clientName);
				return FALSE;
			}
			else
			{
				$row = mysql_fetch_array($result);
				return $row['ClientID'];
			}
		}	
	}
	
	
	/*
	 * @description : getting ProcessID
	 */
	private function _getProcessID($processName,$clientName)
	{
		$sql = "SELECT ProcessID FROM PROCESS WHERE LOWER(TRIM(NAME)) = '".mysql_real_escape_string(strtolower(trim($processName)))."' AND `ClientID` = (SELECT ClientID FROM `Client` WHERE LOWER(TRIM(Client_Name))  = '".mysql_real_escape_string(strtolower(trim($clientName)))."')";
		$result = mysql_query($sql) or die(mysql_error()."_getProcessID");
		if(mysql_num_rows($result) == 0)
		{
			array_push($this->failedProcessArray,$processName); 
			return FALSE;
		}
		else
		{
			$row = mysql_fetch_array($result);
			return $row['ProcessID'];
		}	
	}
	
	/*
	 * Note : All the headings of csv file should be changed ( Client,Process,Year,Month,ExpectedHours,ActualHours,Utilization  )
	 * 		  All the months converted to number e.g 'january' to '1'
	 */
	
	public function insertUtilization()
	{
		//echo "<pre>";print_r($this->utilizationData);
		//exit;
		
		foreach($this->Data as $k=>$utilization)
		{
			$clientID = $this->_getClientID($utilization['Client']); 
			if($clientID)
			{
				$processID = $this->_getProcessID($utilization['Process'],$utilization['Client']);
				if($processID)
				{
					$sql = "SELECT UID,ProcessID FROM utilization_process WHERE  ProcessID = ".$processID." AND MONTH = ".trim($utilization['Month'])." AND YEAR = ".trim($utilization['Year'])."";
					$result = mysql_query($sql) or die(mysql_error());
					if($row = mysql_num_rows($result)==0)
					{
						$Utilization = ($utilization['Utilization']!="")?$utilization['Utilization'] : 0;
						$ExpectedHours = ($utilization['ExpectedHours']!="")?$utilization['ExpectedHours'] : 0;
						$ActualHours = ($utilization['ActualHours']!="")?$utilization['ActualHours'] : 0;
						
						$query = "INSERT INTO utilization_process(ProcessID,MONTH,YEAR,ExpectedHours,ActualHours,Utilization) VALUES (".$processID.",".$utilization['Month'].",".$utilization['Year'].",'".round($ExpectedHours,2)."','".round($ActualHours,2)."',".round($Utilization,2).")";
						$rslt = mysql_query($query) or die(mysql_error()."insertUtilization");
						$this->Inseted++;
					}
					else
					{
						$this->alreadyExist++;
					}	
				}
				else
				{
					$this->processIDMissing++;
				}
			}
			else
			{
				$this->clientIDMissing;
			}
		}
	}
	
	
	
	private function _convertMonth($month){
		$monthArray = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
		
		if(is_string($month))
		{
			$month = ucwords(substr($month,0,3));
			if(in_array($month,$monthArray))
			{
				return array_search($month,$monthArray);
			}
		}
		else
			return $month;
		
	}
	
	
	/*
	 * @descrtipion : checking if $year varibale has full date format
	 * else we are forming date with year and month coulumn and settign date as 1
	 * @return :  date (Y-m-d) format
	 */
	private function _getDate($year,$month) {
		
		$yearArray =  explode("-",$year);
		if(count($yearArray)==3)
		{
			$d = $yearArray[0];
			$m = $this->_convertMonth($yearArray[1]);
			$y = $yearArray[2];
		}
		else
		{
			$y = $year;
			$m = $this->_convertMonth($month);
			$d = 1;
		}
		return $y."-".$m."-".$d;			
		
	}
	
	/*
	 * @description : getting "RootCauseID" if matching else inserting "rootcause description"
	 * @return : RootCauseID
	 */
	private function _getRootCauseID($description)
	{
		if($description!="")
		{
			$sql = "SELECT RootCauseID FROM incidentlog_root_cause WHERE LOWER(TRIM(Description)) = '".mysql_real_escape_string(strtolower(trim($description)))."'";
			$result = mysql_query($sql) or die(mysql_error()."rootCause");
			if($row = mysql_fetch_array($result))
				return $row['RootCauseID'];
			else
			{
				$sql = "INSERT INTO incidentlog_root_cause (Description) VALUES ('".mysql_real_escape_string($description)."')";
				$result =  mysql_query($sql) or die(mysql_error()."rootCause");
				return mysql_insert_id();
			}	
		}	
	}
	
	
	/*
	 * @description : 
	 * First -  getting employID
	 * Second - getting clientID
	 * Third -  getting processID
	 * Fourth - checking record type "Error" or " Esclation " if  "Error" adding "rootcauseID and Severity"
	 * Fifth -  insert into incidentlog
	 * Sixth -  insert into incidentlog_employee
	 */
	public function insertIncidentLog()
	{
		if(is_array($this->Data))
		{
			foreach($this->Data as $k=>$incident)
			{
				$clientID = $this->_getClientID($incident['Client']); //First
				$employID = $this->_getEmployID($incident['EmpId']); //Second
				if($employID)
				{
					if($clientID)
					{
						$processID = $this->_getProcessID($incident['Process'],$incident['Client']); // Third
						$date = $this->_getDate($incident['Year'],$incident['Month']);
						if($processID)
						{
							$fields="";
							$fieldValues = "";
							//Fourth
							if(strtolower(trim($incident['RecordType']))=='error'){
								if($incident['RootCause']!="")
								{
									$fields .= ",RootCauseID ";
									$rootCalueID = $this->_getRootCauseID(strtolower(trim($incident['RootCause'])));
									$fieldValues.= ",".$rootCalueID; 
						
								}
								if($incident['Severity']!="")
								{
									$fields.= ($fields!="")?", Severity":" Severity";
									$fieldValues.= ($fieldValues!="") ? ",'".$incident['Severity']."'":"'".$incident['Severity']."'";
								}
							}
							//Fifth
							$sql= "INSERT INTO incidentlog (ClientID,ProcessID,Date,LogType,Description $fields ) VALUES ( ".$clientID.",".$processID.",'".$date."','".$incident['RecordType']."','".mysql_real_escape_string($incident['Description'])."' $fieldValues )";
							$result = mysql_query($sql) or die(mysql_error()."incidentlog");
							$logID = mysql_insert_id();
							//Sixth
							$query = "insert into incidentlog_employee (LogID,EmployID) VALUES (".$logID.",".$employID.")";
							$rslt = mysql_query($query) or die(mysql_error()."incidentlog_employee");
							
							$this->Inseted++;
						}
						else
							$this->processIDMissing++;
					}
					else
						$this->clientIDMissing ++;
				}
				else
					$this->empIdMissing++;				
			}
		}		
	}
}
$obj  = new UtlizationImport();

/*************** UTILIZATION IMPORT *************************************/
$obj->readCsvFile('utilization-csv/new/Rhythm - Data Upload - Media Review.csv');
$obj->insertUtilization();

echo "Inserted = ".$obj->Inseted."<br>";
echo "clientIDMissing = ".$obj->clientIDMissing."<br>";
echo "processIDMissing = ".$obj->processIDMissing."<br>";
echo "AlreadyExist = ".$obj->alreadyExist."<br>";


/***************INCIDENT IMPORT *************************************/
/*$obj->readCsvFile('incident-csv/Rhythm - Data Upload - Search - incident.csv');
$obj->insertIncidentLog();


echo "Total Records = ".count($obj->Data)."<br>";
echo "Inserted = ".$obj->Inseted."<br>";
echo "empIdMissing = ".$obj->empIdMissing."<br>";
echo "clientIDMissing = ".$obj->clientIDMissing."<br>";
echo "processIDMissing = ".$obj->processIDMissing."<br>";*/



