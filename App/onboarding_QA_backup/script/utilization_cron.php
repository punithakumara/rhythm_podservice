<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';
class UtlizationCron extends MysqlConnect {
	
	public $expectedHours = 0;
	public $actualHours = 0;
	public $processArray = array();
	public $CurrentMonthYear;
	public $CurrentMonth;
	public $CurrentYear;
	
    function __construct($dbname) {
       parent::__construct($dbname);
       $this->CurrentMonth = date('m');
       $this->CurrentYear = date('Y');
       $this->CurrentMonthYear = date('Y-m');
       /*$this->CurrentMonth = '08';
       $this->CurrentYear = '2014';
       $this->CurrentMonthYear = '2014-08';*/
    }
	
	/* @description : Setting All Process for Current Month */
	public function setProcessArray(){
		$sql = "SELECT ProcessID,TaskDate FROM utilization WHERE DATE_FORMAT(TaskDate,'%Y-%m ') =  '".$this->CurrentMonthYear."' GROUP BY ProcessID ";
		$result = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($result)==0) return false;
		while ($row = mysql_fetch_array($result)){
    		$this->processArray[] = $row['ProcessID'];
		}
	}
	
	/* @description : 
	 * step 1 - loop through process array
	 * step 2 - getting all employee for each process.
	 * step 3 - looping employee array and calculating expected and actual hours 
	 * step 4 - caculating utilization percent
	 *  */
	public function calculateExpectedandToalHours()
	{
		if(count($this->processArray)>0)
		{
			foreach($this->processArray as $key=>$processID)
			{
				$this->expectedHours = 0;
				$this->actualHours = 0;
				//$sql = "SELECT DISTINCT EmployID FROM utilization WHERE ProcessID = ".$processID." AND DATE_FORMAT(TaskDate,'%Y-%m ') =  DATE_FORMAT('2014-05-01','%Y-%m')";
				$sql = "SELECT DISTINCT employprocess.ProcessID,employprocess.EmployID,ProcessType FROM employprocess
						JOIN `process` ON `process`.`ProcessID`=employprocess.`ProcessID` 
						WHERE employprocess.ProcessID = $processID";
				$result =  mysql_query($sql);
				$totalEmp = mysql_num_rows($result);
				if(mysql_num_rows($result)!=0)
				{
					while ($row = mysql_fetch_array($result)){
						if($row['ProcessType'] == 'Hourly')
		    				$this->_getHourlyTypeExpectedHours($processID);
						else														
		    				$this->_setExpectedHours($row['EmployID'],$processID);
						
		    			$this->_setActualHours($row['EmployID'],$processID); 
					}
					$this->_updateUtilizationProcess($totalEmp,$processID);
				}	
			}
		}
	}
	
	/*@description : setting expected Hours (count of total workingdays * 8 ) */
	private function _setExpectedHours($empID,$processID){
				
		$BillblePercentage = 1;
		$sql = "SELECT  COUNT(DISTINCT(TaskDate)) AS totalWorkedDays  FROM utilization WHERE EmployID = $empID AND ProcessID = $processID AND DATE_FORMAT(TaskDate,'%Y-%m ') = '".$this->CurrentMonthYear."'";
		$result = mysql_query($sql) or die(mysql_error());
		if($row = mysql_fetch_array($result))
		{
			if($row['totalWorkedDays']> 0)
			{
				/*** Updated By Amesh **/
				$sql = "SELECT ProcessID, EmployID, BillablePercentage FROM employprocess WHERE EmployID = $empID AND ProcessID = $processID";
				$q_exe = mysql_query($sql) or die(mysql_error());
				if($q_res = mysql_fetch_array($q_exe))
				{
					if(!empty($q_res['BillablePercentage']))
					{
						$BillblePercentage = $q_res['BillablePercentage'] / 100;
					}
				}
				/***End Updated By Amesh **/
				$this->expectedHours += $row['totalWorkedDays'] * 8 * $BillblePercentage;
			}				
		}
	}
	
	/*@description : setting Hourly Type expected Hours (count of total workingdays * 8 ) */
	private function _getHourlyTypeExpectedHours($processID){
		
		$sql = "SELECT  minHours  FROM PROCESS WHERE ProcessID ='".$processID."'";
		//echo $sql;//exit;
		$result = mysql_query($sql) or die(mysql_error());
		if($row = mysql_fetch_array($result))
		{
			if($row['minHours']> 0)
				$this->expectedHours += $row['minHours'];		
		}
	}

	/*@description : setting Hourly Type Actual Hours entered bu employee */
		private function _setActualHours($empID,$processID){
		$sql = "SELECT SUM(MIN / 60) + SUM(Hours) AS totalHour FROM utilization WHERE  EmployID = ".$empID." AND  ProcessID = ".$processID." AND DATE_FORMAT(TaskDate,'%Y-%m ') = '".$this->CurrentMonthYear."'";
		$result = mysql_query($sql);
		if(mysql_num_rows($result)==0) return false;
		$row = mysql_fetch_array($result);
		$this->actualHours += $row['totalHour']; 
	}
	
	/*
	 * @description :
	 * Step1 - calucating final expected and actual hours by dividing total employee for each procces
	 * Step2 - If processID already exist then updating else inserting into database
	 */
	private function _updateUtilizationProcess($totalEmp,$processID)
	{
		$totalUtlization = 0;
		$fianlExpectedHours =  ($this->expectedHours / $totalEmp);
		$finalActualHours = ($this->actualHours / $totalEmp);
		
		if($fianlExpectedHours!=0)
		{
			$totalUtlization =  ($finalActualHours / $fianlExpectedHours)*100;

			$sql = "SELECT UID,ProcessID FROM utilization_process WHERE  ProcessID = $processID AND MONTH =  '".$this->CurrentMonth."' AND YEAR ='".$this->CurrentYear."'";
			$result = mysql_query($sql);
			if($row = mysql_fetch_array($result))
				$query = "UPDATE utilization_process SET ProcessID = ".$processID.", Month='".$this->CurrentMonth."', Year='".$this->CurrentYear."',ExpectedHours= '".round($fianlExpectedHours,2)."',ActualHours= '".round($finalActualHours,2)."',Utilization=".round($totalUtlization,2)." where UID = ".$row['UID'];
			else
				$query = "insert into utilization_process (ProcessID,Month,Year,ExpectedHours,ActualHours,Utilization)values (".$processID.",'".$this->CurrentMonth."', '".$this->CurrentYear."' ,'".round($fianlExpectedHours,2)."','".round($finalActualHours,2)."','".round($totalUtlization,2)."')";
			
			$rslt = mysql_query($query) or die(mysql_error()."_updateUtilizationProcess");
		}
	}
}
$obj  = new UtlizationCron();
$obj->setProcessArray();
$obj->calculateExpectedandToalHours();
?>

