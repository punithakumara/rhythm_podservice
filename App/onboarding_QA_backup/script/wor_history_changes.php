<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
//ini_set('xdebug.max_nesting_level', 200); 
require_once 'db_config.php';
class WorHistoryChanges extends MysqlConnect
{
	public $inserted = 0;
	
	/*
	* @description : getting all the employee above grade 2
	* @return : setting into employee 
	*/
	private function _setEmpArray()
	{
		$sql = "SELECT employees.employee_id,employees.first_name,employees.last_name FROM employees JOIN designation ON employees.designation_id = designation.designation_id WHERE designation.grades > 2 AND employees.status != 6 ORDER BY employees.employee_id ";
		$result =  mysql_query($sql) or die(mysql_error()."getAllEmployee");
		$totalRecord = mysql_num_rows($result);
		if($totalRecord > 0)
		{
			while($row = mysql_fetch_array($result))
				$this->empArray[] = array('employee_id'=>$row['employee_id'],'first_name'=>$row['first_name'],'last_name'=>$row['last_name']);
			$this->empArray;
		} 
		return false;
	}
	
	
	/*
	 * @description : 
	 * First : Set the employee Array
	 * Second : get the Total Count for employee
	 * Third : if empid exist updating the query
	 * Fourth : if empid not exist inserting 
	 * 
	 */
	public function tlHistory()
	{	
		$this->_setEmpArray();//First
		if(is_array($this->empArray))
		{
			foreach($this->empArray as $key=>$emp)
			{
				$sql = "INSERT INTO wor_tl_history (wor_id,team_lead,start_date,created_by) VALUES (".$emp['wor_id'].",".$emp['team_lead'].",".$emp['start_date'].",".$emp['created_by'].")";
				//echo $sql."<br>";
				$result = mysql_query($sql);
				$this->inserted++;
			}
		}	
	}
}

$obj  = new WorHistoryChanges();
$obj->tlHistory();

echo "<br>Total Record of Employee = ".count($obj->empArray);
echo "<br>Total Record Inserted = ".$obj->inserted;
echo "<br>Total Record Update = ".$obj->updated;

