SELECT employees.employee_id,verticals.name AS Vertical, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
			employees.company_employ_id, designation.hcm_grade AS Grade, designation.name AS Designation, shift_code,
			CONCAT(IFNULL(supervisor.`first_name`, ''),' ',IFNULL(supervisor.`last_name`, '')) AS `SupervisorName`, 
			CONCAT(IFNULL(supervisor2.`first_name`, ''),' ',IFNULL(supervisor2.`last_name`, '')) AS `SupervisorName2`,
			CONCAT(IFNULL(supervisor3.`first_name`, ''),' ',IFNULL(supervisor3.`last_name`, '')) AS `SupervisorName3`, client_name,
			TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') AS TimeTaken,
			CASE 
			   WHEN utilization.status IN(1,3,4) THEN 'Approved'
			   WHEN utilization.status = 0 THEN 'Pending'
			   WHEN utilization.status = 2 THEN 'Rejected'
			END AS Status
			FROM `utilization` 
			JOIN client ON client.client_id = utilization.fk_client_id
			JOIN `employees` ON employees.employee_id = utilization.employee_id
			JOIN `shift` ON shift.shift_id = employees.shift_id
			JOIN `employee_vertical` ON employee_vertical.employee_id = employees.employee_id
			JOIN `verticals` ON `verticals`.`vertical_id` = employee_vertical.vertical_id
			JOIN `designation` ON `designation`.`designation_id` = employees.designation_id
			JOIN `employees` supervisor ON `supervisor`.`employee_id` = employees.primary_lead_id
			JOIN `employees` supervisor2 ON `supervisor2`.`employee_id` = supervisor.primary_lead_id
			JOIN `employees` supervisor3 ON `supervisor3`.`employee_id` = supervisor2.primary_lead_id
			WHERE task_date = '".$date."' AND utilization.status IN(0,1,2,3,4) AND
			GROUP BY employees.employee_id";
			
			
			
	
SELECT employees.employee_id,verticals.name AS Vertical, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
			employees.company_employ_id, designation.hcm_grade AS Grade, designation.name AS Designation,shift_code,task_code_id,
			CONCAT(IFNULL(supervisor.`first_name`, ''),' ',IFNULL(supervisor.`last_name`, '')) AS `SupervisorName`, 
			CONCAT(IFNULL(supervisor2.`first_name`, ''),' ',IFNULL(supervisor2.`last_name`, '')) AS `SupervisorName2`,
			CONCAT(IFNULL(supervisor3.`first_name`, ''),' ',IFNULL(supervisor3.`last_name`, '')) AS `SupervisorName3`,task_name, sub_task_name, 
			category_name, client_name,sub_task_id,timesheet_task_mapping.client_id,
			CASE 
			   WHEN timesheet_task_mapping.client_id=221 && task_code_id IN(18) THEN 'Non Productive'
			   WHEN timesheet_task_mapping.client_id=221 && task_code_id IN(12,23) THEN 'Personal'
			   WHEN timesheet_task_mapping.client_id != 221 THEN 'Client Not-Billed'
			   WHEN timesheet_task_mapping.client_id = 221 THEN 'Non Billable'
			END AS billability,
			CASE 
			   WHEN task_code_id IN(18) THEN 'Non Productive'
			   WHEN task_code_id IN(12,23) THEN 'Personal'
			   WHEN timesheet_task_mapping.client_id != 221 THEN 'Client Productive'
			   WHEN timesheet_task_mapping.client_id = 221 THEN 'Theorem Productive'
			END AS task_type,
			CASE timesheet_task_mapping.status
			   WHEN '0' THEN 'Rejected'
			   WHEN '1' THEN 'Pending'
			   WHEN '2' THEN 'Approved'
			END AS Status
			FROM `timesheet_task_mapping` 
			JOIN task_week ON task_week.week_code_id = timesheet_task_mapping.task_week_id
			JOIN timesheet_task_codes ON timesheet_task_codes.timesheet_task_code_id = timesheet_task_mapping.task_code_id
			LEFT JOIN timesheet_subtasks ON timesheet_subtasks.timesheet_subtask_id = timesheet_task_mapping.sub_task_id
			JOIN timesheet_categories ON timesheet_categories.category_id = timesheet_task_mapping.category_id
			JOIN client ON client.client_id = timesheet_task_mapping.client_id
			JOIN `employees` ON employees.employee_id = timesheet_task_mapping.emp_id
			JOIN `shift` ON shift.shift_id = employees.shift_id
			JOIN `employee_vertical` ON employee_vertical.employee_id = employees.employee_id
			JOIN `verticals` ON `verticals`.`vertical_id` = employee_vertical.vertical_id
			JOIN `designation` ON `designation`.`designation_id` = employees.designation_id
			JOIN `employees` supervisor ON `supervisor`.`employee_id` = employees.primary_lead_id
			JOIN `employees` supervisor2 ON `supervisor2`.`employee_id` = supervisor.primary_lead_id
			JOIN `employees` supervisor3 ON `supervisor3`.`employee_id` = supervisor2.primary_lead_id
			WHERE employees.status!=6 AND week_date = '".$date."' AND designation.grades<7   
			GROUP BY employees.employee_id,timesheet_task_mapping.client_id,timesheet_task_code_id,sub_task_id";	
			