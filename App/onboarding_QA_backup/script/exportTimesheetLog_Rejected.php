<?php
//http://localhost:8082/onboarding/script/exportTimesheetLog.php?startDate=2019-04-01&endDate=2019-04-20
//http://localhost:8082/onboarding/script/exportTimesheetLog.php?startDate=2019-03-11&endDate=2019-03-20
//http://localhost:8082/onboarding/script/exportTimesheetLog.php?startDate=2019-03-21&endDate=2019-03-31

//http://localhost:8085/onboarding/script/exportTimesheetLog_Rejected.php?startDate=2019-09-01&endDate=2019-09-30

error_reporting(0);
ini_set('max_execution_time', 0); //infinite time
ini_set('memory_limit', '-1'); //infinite time
require_once 'db_config.php';

class exportTimesheetLog extends MysqlConnect
{
	function exportTime()
	{
		$startDate = $_GET['startDate'];	
		$endDate = $_GET['endDate'];
		
		$filename = "Timesheet.csv";

		$retVal = $this->downloadCsv($startDate,$endDate);
		
		//$retVal[0][count($retVal[0])] = array('','','',"Theorem Inc. Confidential(Not To Be Shared)");
		
		$mime = 'application/csv'; 	

		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		$this->ExportFile($retVal[0]);
		exit();	
		ob_end_clean();	        
	}
		
	function ExportFile($records) 
	{
		$heading = false;
        if(!empty($records))
			foreach($records as $row) 
			{
				if(!$heading) {            
					echo implode(",", array_keys($row[0])) . "\n";
					$heading = true;
				}
				echo implode(",", array_values($row)) . "\n";
			}
        exit;
	}	

	
	function downloadCsv($startDate,$endDate) 
	{
		if($startDate!="" && $endDate!="") 
		{
			$index =0;
			$data = array();
			
			$sql = "SELECT week_name,week_date FROM `task_week` WHERE week_date BETWEEN '".$startDate."' AND '".$endDate."'";			
			$result = mysql_query($sql);
			while($val = mysql_fetch_array($result))
			{
				$timeData = $this->getTimeDetails($val['week_name'],$val['week_date']);
				$prodData = $this->getProdutionDetails($val['week_date']);
				$data = array_merge($data, $timeData);
				$data = array_merge($data, $prodData);
				$index++;
			}
			
			$header = array_keys($data[0]);
			$result_result = array_merge(array(array_unique($header)), $data);
			
			return array($result_result);
		}
	}
	
	private function getProdutionDetails($date)
	{
		//'0'=> Pending, "1" => Approved , "2"=> Rejected,"3"=> Weekend Billable,"4"=> Client Holiday Billable
		$loop =0;
		$sql = "SELECT employees.employee_id,verticals.name AS Vertical, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
			employees.company_employ_id, designation.hcm_grade AS Grade, designation.name AS Designation, shift_code,
			CONCAT(IFNULL(supervisor.`first_name`, ''),' ',IFNULL(supervisor.`last_name`, '')) AS `SupervisorName`, 
			CONCAT(IFNULL(supervisor2.`first_name`, ''),' ',IFNULL(supervisor2.`last_name`, '')) AS `SupervisorName2`,
			CONCAT(IFNULL(supervisor3.`first_name`, ''),' ',IFNULL(supervisor3.`last_name`, '')) AS `SupervisorName3`, client_name,
			TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(theorem_hrs))), '%H:%i') AS TimeTaken,
			CASE 
			   WHEN utilization.status IN(1,3,4) THEN 'Approved'
			   WHEN utilization.status = 0 THEN 'Pending'
			   WHEN utilization.status = 2 THEN 'Rejected'
			END AS Status
			FROM `utilization` 
			JOIN client ON client.client_id = utilization.fk_client_id
			JOIN `employees` ON employees.employee_id = utilization.employee_id
			JOIN `shift` ON shift.shift_id = employees.shift_id
			JOIN `employee_vertical` ON employee_vertical.employee_id = employees.employee_id
			JOIN `verticals` ON `verticals`.`vertical_id` = employee_vertical.vertical_id
			JOIN `designation` ON `designation`.`designation_id` = employees.designation_id
			JOIN `employees` supervisor ON `supervisor`.`employee_id` = employees.primary_lead_id
			JOIN `employees` supervisor2 ON `supervisor2`.`employee_id` = supervisor.primary_lead_id
			JOIN `employees` supervisor3 ON `supervisor3`.`employee_id` = supervisor2.primary_lead_id
			WHERE task_date = '".$date."' AND utilization.status IN(2)
			GROUP BY employees.employee_id";
			// echo $sql; exit;
		$data = array();
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{
			$data[$loop]['Vertical'] 		= $val['Vertical'];
			$data[$loop]['EmployID'] 		= $val['company_employ_id'];
			$data[$loop]['Name'] 			= $val['FullName'];
			$data[$loop]['Grade'] 			= $val['Grade'];
			$data[$loop]['Designation'] 	= $val['Designation'];
			$data[$loop]['Shift'] 			= $val['shift_code'];
			$data[$loop]['Manager L1'] 		= $val['SupervisorName'];
			$data[$loop]['Manager L2'] 		= $val['SupervisorName2'];
			$data[$loop]['Manager L3'] 		= $val['SupervisorName3'];
			$data[$loop]['Date of Entry'] 	= date("d-M-Y", strtotime($date));
			$data[$loop]['Week of Entry'] 	= "Week".date("W", strtotime($date));
			$data[$loop]['Month of Entry'] 	= date("M", strtotime($date));
			$data[$loop]['Client'] 			= "".$val['client_name']."";
			$data[$loop]['Task'] 			= 'Production Work';
			$data[$loop]['SubTask'] 		= '';
			$data[$loop]['Category'] 		= 'Client Mgmt.';
			$data[$loop]['Billability'] 	= 'Client Billable';
			$data[$loop]['TaskType'] 		= 'Client Productive';
			$data[$loop]['Hours'] 			= $val['TimeTaken'];
			$data[$loop]['Status'] 			= $val['Status'];
			$data[$loop]['Comments'] 		= '';
			
			$loop++;
		}
		return $data;
	}
	
	private function getTimeDetails($week_name,$date)
	{
		//0-Rejected, 1-Pending, 2-Approved
		$loop =0;
		$sql = "SELECT employees.employee_id,verticals.name AS Vertical, CONCAT(IFNULL(employees.`first_name`, ''),' ',IFNULL(employees.`last_name`, '')) AS `FullName`,
			employees.company_employ_id, designation.hcm_grade AS Grade, designation.name AS Designation,shift_code,task_code_id,
			CONCAT(IFNULL(supervisor.`first_name`, ''),' ',IFNULL(supervisor.`last_name`, '')) AS `SupervisorName`, 
			CONCAT(IFNULL(supervisor2.`first_name`, ''),' ',IFNULL(supervisor2.`last_name`, '')) AS `SupervisorName2`,
			CONCAT(IFNULL(supervisor3.`first_name`, ''),' ',IFNULL(supervisor3.`last_name`, '')) AS `SupervisorName3`,task_name, sub_task_name, 
			category_name, client_name,sub_task_id,timesheet_task_mapping.client_id,
			CASE 
			   WHEN timesheet_task_mapping.client_id=221 && task_code_id IN(18) THEN 'Non Productive'
			   WHEN timesheet_task_mapping.client_id=221 && task_code_id IN(12,23) THEN 'Personal'
			   WHEN timesheet_task_mapping.client_id != 221 THEN 'Client Not-Billed'
			   WHEN timesheet_task_mapping.client_id = 221 THEN 'Non Billable'
			END AS billability,
			CASE 
			   WHEN task_code_id IN(18) THEN 'Non Productive'
			   WHEN task_code_id IN(12,23) THEN 'Personal'
			   WHEN timesheet_task_mapping.client_id != 221 THEN 'Client Productive'
			   WHEN timesheet_task_mapping.client_id = 221 THEN 'Theorem Productive'
			END AS task_type,
			CASE timesheet_task_mapping.status
			   WHEN '0' THEN 'Rejected'
			   WHEN '1' THEN 'Pending'
			   WHEN '2' THEN 'Approved'
			END AS Status
			FROM `timesheet_task_mapping` 
			JOIN task_week ON task_week.week_code_id = timesheet_task_mapping.task_week_id
			JOIN timesheet_task_codes ON timesheet_task_codes.timesheet_task_code_id = timesheet_task_mapping.task_code_id
			LEFT JOIN timesheet_subtasks ON timesheet_subtasks.timesheet_subtask_id = timesheet_task_mapping.sub_task_id
			JOIN timesheet_categories ON timesheet_categories.category_id = timesheet_task_mapping.category_id
			JOIN client ON client.client_id = timesheet_task_mapping.client_id
			JOIN `employees` ON employees.employee_id = timesheet_task_mapping.emp_id
			JOIN `shift` ON shift.shift_id = employees.shift_id
			JOIN `employee_vertical` ON employee_vertical.employee_id = employees.employee_id
			JOIN `verticals` ON `verticals`.`vertical_id` = employee_vertical.vertical_id
			JOIN `designation` ON `designation`.`designation_id` = employees.designation_id
			JOIN `employees` supervisor ON `supervisor`.`employee_id` = employees.primary_lead_id
			JOIN `employees` supervisor2 ON `supervisor2`.`employee_id` = supervisor.primary_lead_id
			JOIN `employees` supervisor3 ON `supervisor3`.`employee_id` = supervisor2.primary_lead_id
			WHERE employees.status=0 AND week_date = '".$date."' AND designation.grades<7   
			GROUP BY employees.employee_id,timesheet_task_mapping.client_id,timesheet_task_code_id,sub_task_id";
		
		$data = array();
		$result = mysql_query($sql);
		while($val = mysql_fetch_array($result))
		{
			$timeTaken = "";
			$timeTaken = $this->getTimeTaken($week_name,$date,$val['employee_id'],$val['task_code_id'],$val['sub_task_id'],$val['client_id']);

			if($timeTaken[0]!="00:00" && $timeTaken[0]!="")
			{
				$data[$loop]['Vertical'] 		= $val['Vertical'];
				$data[$loop]['EmployID'] 		= $val['company_employ_id'];
				$data[$loop]['Name'] 			= $val['FullName'];
				$data[$loop]['Grade'] 			= $val['Grade'];
				$data[$loop]['Designation'] 	= $val['Designation'];
				$data[$loop]['Shift'] 			= $val['shift_code'];
				$data[$loop]['Manager L1'] 		= $val['SupervisorName'];
				$data[$loop]['Manager L2'] 		= $val['SupervisorName2'];
				$data[$loop]['Manager L3'] 		= $val['SupervisorName3'];
				$data[$loop]['Date of Entry'] 	= date("d-M-Y", strtotime($date));
				$data[$loop]['Week of Entry'] 	= "Week".date("W", strtotime($date));
				$data[$loop]['Month of Entry'] 	= date("M", strtotime($date));
				$data[$loop]['Client'] 			= $val['client_name'];
				$data[$loop]['Task'] 			= $val['task_name'];
				$data[$loop]['SubTask'] 		= '"'.$val['sub_task_name'].'"';
				$data[$loop]['Category'] 		= $val['category_name'];
				$data[$loop]['Billability'] 	= $val['billability'];
				$data[$loop]['TaskType'] 		= $val['task_type'];
				$data[$loop]['Hours'] 			= $timeTaken[0];
				$data[$loop]['Status'] 			= $val['Status'];
				$data[$loop]['Comments'] 		= '"'.$timeTaken[1].'"';
				
				$loop++;
			}
			
		}
		return $data;
	}
	
	private function getTimeTaken($week_name,$date,$employee_id,$task_code_id,$sub_task_id,$client_id)
	{
		$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day1))), '%H:%i') AS TimeTaken,comment1 AS comments";
		if($week_name=="Tue")
		{
			$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day2))), '%H:%i') AS TimeTaken,comment2 AS comments";
		}
		if($week_name=="Wed")
		{
			$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day3))), '%H:%i') AS TimeTaken,comment3 AS comments";
		}
		if($week_name=="Thu")
		{
			$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day4))), '%H:%i') AS TimeTaken,comment4 AS comments";
		}
		if($week_name=="Fri")
		{
			$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day5))), '%H:%i') AS TimeTaken,comment5 AS comments";
		}
		if($week_name=="Sat")
		{
			$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day6))), '%H:%i') AS TimeTaken,comment6 AS comments";
		}
		if($week_name=="Sun")
		{
			$select = "TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(day7))), '%H:%i') AS TimeTaken,comment7 AS comments";
		}
		$sql = "SELECT $select
			FROM `timesheet_task_mapping` 
			JOIN task_week ON task_week.week_code_id = timesheet_task_mapping.task_week_id
			WHERE week_date = '".$date."' AND client_id='".$client_id."' AND emp_id='".$employee_id."' AND task_code_id='".$task_code_id."' AND sub_task_id = '".$sub_task_id."'";
		// echo $sql; exit;
		$result = mysql_query($sql);
		$result = mysql_fetch_row($result);
		
		return $result;
	}
}

$obj = new exportTimesheetLog();
$obj->exportTime();
?>