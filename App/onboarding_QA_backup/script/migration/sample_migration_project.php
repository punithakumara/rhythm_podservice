<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
include('helper.php');
error_reporting(E_ALL);
// http://localhost:8082/onboarding/script/migration/sample_migration_project.php

class migration
{
	function __construct() 
	{
		$this->con_local = mysqli_connect('localhost','root','','rhythm_live') or die(mysqli_connect_error()." Unable to connect to Local");
		$this->con_live = mysqli_connect('localhost','root','','rhythm_dev2') or die(mysqli_connect_error()."Unable to connect to Live");
	}
	
	function update($filename)
	{	
		$csv_file = $filename; 
		
		if (($handle = fopen($csv_file, "r")) !== FALSE) 
		{
			fgetcsv($handle);   		
			$recordid = 0;
			
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				$num = count($data);				
				for ($c=0; $c < $num; $c++) 
				{
					$col[$c] = $data[$c];
				}


				$wor_name = $col[2];
				$wor_type_id = $this->getWorTypeId($col[3]);
				$wor_data = $this->getWorData($col[1]);
				
				if(empty($wor_data))
				{
					echo "The WOR data is not found for $col[1]";
				}
				else
				{
					$wor_id = $this->putWorData($wor_data,$wor_name,$wor_type_id);

					$job_resp_id = $this->getJobRespId($col[13]);
					$role_type_id = $this->getRoleTypeId($col[15]);
					$role_short_code = $this->getRoleShortCode($role_type_id);
					$shift_short_code = $this->getShiftShortCode($col[9]);
					$support_coverage = $col[10];
					$support_coverage_data = $this->getSupportCoverageData($support_coverage);
					$support_coverage_code = $support_coverage_data['short_code'];
					$process_name = 'P_'.$shift_short_code.'_'.$role_short_code.'_'.$support_coverage_code;
					$model = 'Project';
					$shift = $this->getShiftId($col[9]);
					$support_coverage_id = $support_coverage_data['support_id'];
					
					$process_id = $this->putProcessData($wor_data['client'],$process_name,$model,$shift,$support_coverage_id,$role_type_id);
					
					$project_type_id = $this->getProjectTypeId($col[14]);
					$project_data = $this->getProjectData($wor_data['wor_id'],$shift,$support_coverage_id,$job_resp_id);
					$project_name = $project_data['project_name'];
					$project_description = $project_data['project_description'];
					$experience = $project_data['experience'];

					$start_duration = date('Y-m-d',strtotime($col[7]));
					$end_duration = date('Y-m-d',strtotime($col[8]));
					$skills = $col[12];
					$anticipated_date = date('Y-m-d',strtotime($col[11]));
					
					$this->putProjectData($wor_id,$project_type_id,$project_name,$shift,$start_duration,$end_duration,$experience,$skills,$anticipated_date,$support_coverage_id,$role_type_id,$project_description,$process_id);
				}
			}
		}

	}

	function getWorTypeId($wor_type)
	{
		$sql = "SELECT wor_type_id FROM wor_types WHERE wor_type ='$wor_type'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		// debug($result);exit;
		return $result['wor_type_id'];

	}

	function getProjectTypeId($project_type)
	{
		$sql = "SELECT project_type_id FROM project_types WHERE project_type ='$project_type'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result['project_type_id'];
	}

	function getJobRespId($job_responsibilities)
	{
		$sql = "SELECT job_responsibilities_id FROM job_responsibilities WHERE name ='$job_responsibilities'";
		$query = mysqli_query($this->con_live,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result['job_responsibilities_id'];
	}

	function getRoleTypeId($role_type)
	{
		$sql = "SELECT role_type_id FROM role_types WHERE role_type ='$role_type'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result['role_type_id'];
	}

	function getShiftId($shift_code)
	{
		$sql = "SELECT shift_id FROM shift WHERE shift_code ='$shift_code'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result['shift_id'];
	}

	function getRoleShortCode($role_type_id)
	{
		$sql = "SELECT short_code FROM role_types WHERE role_type_id='$role_type_id'";
		$query = $query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result['short_code'];
	}

	function getShiftShortCode($shift_name)
	{
		$sql = "SELECT time_zone FROM shift WHERE shift_code='$shift_name'";
		$query = $query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result['time_zone'];
	}

	function getSupportCoverageData($support_coverage)
	{
		$sql = "SELECT support_id,short_code FROM support_coverage WHERE support_type='$support_coverage'";
		$query = $query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$result = mysqli_fetch_assoc($query);
		return $result;
	}

	function getWorData($work_order_id)
	{
		$sql = "SELECT * FROM wor WHERE work_order_id ='$work_order_id'";
		$query = mysqli_query($this->con_live,$sql) or die(mysqli_error($this->con_live));
		$result = mysqli_fetch_assoc($query);
		return $result;

	}

	function getProjectData($wor_id,$shift,$support_coverage,$role_type_id)
	{
		$sql = "SELECT DISTINCT project_name,experience,project_description
		FROM project_model 
		WHERE fk_wor_id ='$wor_id' AND shift='$shift' AND support_coverage='$support_coverage' AND responsibilities='$role_type_id'"; 
		$query = mysqli_query($this->con_live,$sql) or die(mysqli_error($this->con_live));
		$result = mysqli_fetch_assoc($query);
		return $result;
	}

	function putWorData($wor_data,$wor_name,$wor_type_id)
	{
		// debug($wor_data);exit;
		$work_order_id = $wor_data['work_order_id'];
		$parent_work_order_id = $wor_data['parent_work_order_id'];
		$client = $wor_data['client'];
		$client_partner = $wor_data['client_partner'];
		$associate_manager = $wor_data['associate_manager'];
		$engagement_manager = $wor_data['engagement_manager'];
		$delivery_manager = $wor_data['delivery_manager'];
		$team_lead = $wor_data['team_lead'];
		$stake_holder = $wor_data['stake_holder'];
		$time_entry_approver = isset($wor_data['time_entry_approver'])?$wor_data['time_entry_approver']:'';
		$start_date = $wor_data['start_date'];
		$end_date = $wor_data['end_date'];
		$status = $wor_data['status'];
		$description = $wor_data['description'];
		$created_by = $wor_data['created_by'];

		$sql_check = "SELECT wor_id FROM wor WHERE work_order_id='$work_order_id'";
		$query_check = mysqli_query($this->con_local,$sql_check) or die(mysqli_error($this->con_local));
		if(mysqli_num_rows($query_check)>0)
		{
			$result_check = mysqli_fetch_assoc($query_check);
			$existing_wor_id = $result_check['wor_id'];
			echo "$work_order_id already exists with wor id:$existing_wor_id<br>";
			$wor_id = $existing_wor_id;
		}
		else
		{
			$sql = "INSERT INTO wor (work_order_id,wor_type_id,wor_name,parent_work_order_id,client,client_partner,associate_manager,engagement_manager,delivery_manager,team_lead,stake_holder,time_entry_approver,start_date,end_date,status,description,created_by)
			VALUES('$work_order_id','$wor_type_id','$wor_name','$parent_work_order_id','$client','$client_partner','$associate_manager','$engagement_manager','$delivery_manager','$team_lead','$stake_holder','$time_entry_approver','$start_date','$end_date','$status','$description','$created_by')";
			$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
			$new_wor_id = $this->con_local->insert_id;
			echo "$work_order_id is inserted with insert id:$new_wor_id<br>";
			$wor_id = $new_wor_id;
		}

		return $wor_id;
	}

	function putProjectData($wor_id,$project_type_id,$project_name,$shift,$start_duration,$end_duration,$experience,$skills,$anticipated_date,$support_coverage,$role_type_id,$project_description,$process_id)
	{

		$sql = "INSERT INTO project_model (fk_wor_id,fk_project_type_id,project_name,shift,start_duration,end_duration,experience,skills,anticipated_date,support_coverage,responsibilities,change_type,project_description,process_id)
		VALUES('$wor_id','$project_type_id','$project_name','$shift','$start_duration','$end_duration','$experience','$skills','$anticipated_date','$support_coverage','$role_type_id','1','$project_description','$process_id')";
		// echo $sql;exit;
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		echo "Project data is inserted for wor_id:$wor_id<br>--------------------------------------------------------<br>";
	}

	function putProcessData($client_id,$process_name,$model,$shift_id,$support_id,$role_type_id)
	{
		$sql_check = "SELECT process_id FROM process WHERE fk_client_id='$client_id' AND name='$process_name'";
		$query_check = mysqli_query($this->con_local,$sql_check) or die(mysqli_error($this->con_local));
		if(mysqli_num_rows($query_check)>0)
		{
			$result_check = mysqli_fetch_assoc($query_check);
			$existing_process_id = $result_check['process_id'];
			echo "$existing_process_id already exists for the client id:$client_id<br>";
			$process_id = $existing_process_id;
		}
		else
		{
			$sql = "INSERT INTO process (fk_client_id,name,model,shift_id,support_id,role_type_id)
			VALUES('$client_id','$process_name','$model','$shift_id','$support_id','$role_type_id')";
			$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
			$process_id = $this->con_local->insert_id;
			echo "Process data is inserted for client_id:$client_id, process ID:$process_id<br>";
		}
		return $process_id;
	}
	
}

$obj = new migration();
$obj->update('workorder-migration-template_19-Mar-2019_project.csv');
?>