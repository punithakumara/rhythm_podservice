<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
include('helper.php');
error_reporting(E_ALL);
// http://localhost:8082/onboarding/script/migration/utilizationMigration_fte.php

class utilizationMigration
{

	public function __construct() 
	{
		$this->con_local = mysqli_connect('localhost','root','','rhythm_live') or die(mysqli_connect_error()." Unable to connect to Local");
		$this->con_live = mysqli_connect('localhost','root','','rhythm_dev2') or die(mysqli_connect_error()."Unable to connect to Live");
	}

	public function export($filename)
	{
		$csv_file = $filename; 
		
		if (($handle = fopen($csv_file, "r")) !== FALSE) 
		{
			fgetcsv($handle);
			$recordid = 0;
			
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				$num = count($data);				
				for ($c=0; $c < $num; $c++) 
				{
					$col[$c] = $data[$c];
				}
				
				$csc = $this->getClientShortData($col[0]);
				$ssc = $this->getShiftShortData($col[9]);
				$rsc = $this->getResponsibilitiesShortData($col[13]);
				$rtsc = $this->getRoleTypeShortData($col[15]);
				$scsc = $this->getSupportCoverageShortData($col[10]);
				
				$project_code = $csc['short_code']."_F_".$ssc['short_code']."_".$rsc['short_code']."_".$scsc['short_code'];
				$role_code = "F_".$ssc['short_code']."_".$rtsc['short_code']."_".$scsc['short_code'];

				$wor_data		= $this->getWorId($col[1]);
				$hourly_data	= $this->getHourlyWor($wor_data['wor_id'], $ssc['shift_id'], $rtsc['role_type_id'], $scsc['support_id']);
				// debug($project_code);exit;
				$UtilData = $this->getUtilData($col[1], $project_code);
				// debug($UtilData);exit;
				foreach ($UtilData as $key => $value) 
				{
					$employee_id	= $value['employee_id'];
					$task_date		= $value['task_date'];
					$client_id		= $value['client_id'];
					$wor_id			= $wor_data['wor_id'];
					$type_id 		= $wor_data['wor_type_id'];
					$project_id		= $hourly_data['fk_project_type_id'];
					$role_id		= $hourly_data['process_id'];
					$task_id		= $value['task_code'];
					$sub_task_id	= $value['sub_task'];
					$request_name	= utf8_decode($value['request_name']);//single inverted commas are different for Cavender’s & Draper's
					$request_name	= str_replace("?","'",$request_name);
					$request_name	= mysqli_real_escape_string($this->con_local,$request_name);
					$client_hrs 	= $value['total_hrs'];
					$theorem_hrs	= $value['actual_hrs'];
					$support_type	= $value['support_type'];
					$remarks		= utf8_decode($value['remarks']);//single inverted commas are different for Cavender’s & Draper's
					$remarks		= str_replace("?","'",$remarks);
					$remarks		= mysqli_real_escape_string($this->con_local,$remarks);
					$attributes		= $value['attributes'];
					$status 		= $value['approved'];
					$created_on 	= $value['created_on'];
					$created_by 	= $value['created_by'];
					$updated_on 	= $value['updated_on'];
					$updated_by 	= $value['updated_by'];
					$approved_on 	= $value['approved_on'];
					$approved_by 	= $value['approved_by'];

					$sql = "INSERT INTO utilization (employee_id, task_date, fk_client_id, fk_wor_id, fk_type_id, fk_project_id, fk_role_id, fk_task_id, fk_sub_task_id, request_name, client_hrs, theorem_hrs, support_type, remarks, additional_attributes, status, created_on, created_by, updated_on, updated_by, approved_on, approved_by)
					VALUES('$employee_id', '$task_date', '$client_id', '$wor_id', '$type_id', '$project_id', '$role_id', '$task_id', '$sub_task_id', '$request_name', '$client_hrs', '$theorem_hrs', '$support_type', '$remarks', '$attributes', '$status', '$created_on', '$created_by', '$updated_on', '$updated_by', '$approved_on', '$approved_by')";

					$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
					if(mysqli_affected_rows($this->con_local))
					{
						$utilization_id = $this->con_local->insert_id;
						echo "Utilization data is inserted with Utilization ID: $utilization_id for Client ID:$client_id, Wor ID:$wor_id, Wor Type ID:$type_id, Project Type ID:$project_id, Role Type:$role_id<br>";
					}	
				}
			}
		}
	}

	private function getUtilData($wor_id, $project_code)
	{
		$sql = "SELECT utilization.*, work_order_id 
				FROM utilization 
				LEFT JOIN wor ON utilization.wor_id = wor.wor_id
				WHERE work_order_id='$wor_id' AND project_code='$project_code'";
		// echo $sql;exit;
		$query = mysqli_query($this->con_live,$sql) or die(mysqli_error($this->con_live));
		$result = array();
		if (mysqli_num_rows($query) > 0) 
		{
			while($row = mysqli_fetch_assoc($query))
			{
				$result[] = $row;
			}
		}
		// debug($result);exit;
		return $result;
		
	}

	private function getWorId($work_order_id)
	{
		$sql = "SELECT wor_id,wor_type_id FROM wor WHERE work_order_id = '$work_order_id'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$wor_data = mysqli_fetch_assoc($query);
		// debug($wor_data);
		return $wor_data;
	}

	private function getHourlyWor($wor_id, $shift_id, $responsibilities, $support_coverage)
	{
		$sql = "SELECT fk_project_type_id,process_id 
		FROM fte_model 
		WHERE fk_wor_id = '$wor_id' AND shift='$shift_id' AND support_coverage='$support_coverage' AND responsibilities='$responsibilities'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
	
		$hourly_data = mysqli_fetch_assoc($query);
		// debug($hourly_data);exit;
		return $hourly_data;
	}
	
	private function getClientShortData($client_name)
	{
		$sql = "SELECT client_id,short_code FROM client WHERE client_name = '$client_name'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$client_data = mysqli_fetch_assoc($query);
		// print_r($client_data); exit;
		return $client_data;
	}
	
	private function getShiftShortData($shift_name)
	{
		$sql = "SELECT shift_id,time_zone AS short_code FROM shift WHERE shift_code = '$shift_name'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$shift_data = mysqli_fetch_assoc($query);
		// debug($shift_data);
		return $shift_data;
	}
	
	private function getResponsibilitiesShortData($resp_name)
	{
		$sql = "SELECT job_responsibilities_id,short_code FROM job_responsibilities WHERE name = '$resp_name'";
		$query = mysqli_query($this->con_live,$sql) or die(mysqli_error($this->con_live));
		$client_data = mysqli_fetch_assoc($query);
		// debug($client_data);
		return $client_data;
	}
	
	private function getRoleTypeShortData($role_name)
	{
		$sql = "SELECT role_type_id,short_code FROM role_types WHERE role_type = '$role_name'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$client_data = mysqli_fetch_assoc($query);
		// debug($client_data);
		return $client_data;
	}
	
	private function getSupportCoverageShortData($coverage_name)
	{
		$sql = "SELECT support_id,short_code FROM support_coverage WHERE support_type = '$coverage_name'";
		$query = mysqli_query($this->con_local,$sql) or die(mysqli_error($this->con_local));
		$coverage_data = mysqli_fetch_assoc($query);
		// debug($coverage_data);
		return $coverage_data;
	}
	
}
$obj = new utilizationMigration();
$obj->export('workorder-migration-template_19-Mar-2019_fte.csv');
?>