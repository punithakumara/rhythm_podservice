<?php
//http://localhost:8082/onboarding/script/status_notification.php

  require_once('../phpmailer/class.phpmailer.php');

  $mysqli = new mysqli("localhost", "root", "", "task");
  $inc_update=0;
  $arr_update=array();

  $query="select employee.employeeID,employee.name,employee.email,employee_vertical.vertical,employee_designation.designation,alteredstatus.oldstatus,
  alteredstatus.newstatus,alteredstatus.operation,alteredstatus.num 
  from alteredstatus 
  inner join employee on employee.employeeID=alteredstatus.employeeID 
  inner join employee_vertical on employee.vertical=employee_vertical.verticalID
  inner join employee_designation on employee.designation=employee_designation.designationID
  WHERE mailstatus='no' AND operation='update'";
  $result=$mysqli->query($query);
  while($row = mysqli_fetch_assoc($result)){
		  $arr_update[$inc_update]=array();
      $arr_update[$inc_update]['name']=$row['name'];
      $arr_update[$inc_update]['employeeID']=$row['employeeID']; 
      $arr_update[$inc_update]['email']=$row['email']; 
      $arr_update[$inc_update]['designation']=$row['designation'];
		  $arr_update[$inc_update]['vertical']=$row['vertical'];
		  $arr_update[$inc_update]['oldstatus']=$row['oldstatus'];
		  $arr_update[$inc_update]['newstatus']=$row['newstatus'];
		  $arr_update[$inc_update]['num']=$row['num'];
		  $inc_update++;
	
 }
if($inc_update==0)
{
	echo "no updates";
}
else{
	$message_update = "
<html>
<head>
<title>Status Update</title>
<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<style type='text/css'>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  align:left;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
th{
background-color:#007bff;
color:white;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
.container{
font-size:1rem;
font-weight:400;
line-height:1.5;
color:#212529;
text-align:left;
background-color:#fff;
}
.foot{
height:50px;
text-align:center;
color:white;
text-decoration:none;
background-color:#007bff;
font-family:SFMono-Regular,Menlo,Monaco,Consolas,'Liberation Mono','Courier New',monospace;
font-size:1em;
margin-top:0;
margin-bottom:1rem;
overflow:auto;
}
.caption{
padding-top:.75rem;
padding-bottom:.75rem;
background-color:#6c757d;
text-align:left;
caption-side:bottom;
text-align:inherit;
display:inline-block;
margin-bottom:.5rem;
}

</style>
	</head>
	<body>
	
	
	<div class='jumbotron text-center' style='background-color:white; height:200px'>
	<img src='https://rhythm.theoreminc.net/resources/images/rhythm_logo_new.png' width='240' height='53' align='left'>
	<br><br><br><br>
	<p style='text-align:left;'>Dear HR Team,</p>
	<p  style='text-align:left;'>Here is the list of employees with their respective status updates.</p>
		
	</div>
	<div class='container text-center'>
		

<table>
  <tr>
    <th>Employee ID</th>
    <th>Employee Name</th>
    <th>Email</th>
    <th>Designation</th>
    <th>Vertical</th>
    <th>Old Status</th>
    <th>New Status</th>
  </tr>";
  
   for($i=0;$i<$inc_update;$i++){
 $message_update.= "<tr>
    <td>".$arr_update[$i]['employeeID']."</td>
    <td>".$arr_update[$i]['name']."</td>
    <td>".$arr_update[$i]['email']."</td>
    <td>".$arr_update[$i]['designation']."</td>
    <td>".$arr_update[$i]['vertical']."</td>
	<td>".$arr_update[$i]['oldstatus']."</td>
	<td>".$arr_update[$i]['newstatus']."</td>
   </tr>";
  }
  
 $message_update.="</table>
</div>
<p >    Regards,</br>
	<b>Rhythm Support Team</b></p>
<br><br><br>
<footer class='page-footer font-small special-color-dark'>
<div class='container'>

  <div class='foot p-3 mb-2 bg-primary text-white footer-copyright text-center  py-3 fixed-bottom'>&copy; 2019 Theorem Inc. -All Rights Reserved v3.3
  </div>
 </div>

</footer>
	</body>
</html>

";
}

$mail = new PHPMailer(true);
$mail->IsSMTP();                       // telling the class to use SMTP
$mail->SMTPDebug = 0;                  

// 0 = no output, 1 = errors and messages, 2 = messages only.
$mail->SMTPDebug = 1; 
$mail->SMTPAuth = true;                // enable SMTP authentication 
$mail->SMTPSecure = "tls";              // sets the prefix to the servier
$mail->Host = "cas.theoreminc.net";        // sets Gmail as the SMTP server
$mail->Port = 587;                    // set the SMTP port for the GMAIL 
$mail->Username = "rhythmsupport";  // Gmail username
$mail->Password = "rhy_th499";      // Gmail password
$mail->ContentType = 'text/html'; 
$mail->IsHTML(true);
$mail->CharSet = 'windows-1250';
$mail->From = 'rhythmsupport@theoreminc.net';
$mail->FromName = 'Rhythmsupport@theoreminc.net';
$mail->addReplyTo('Rhythmsupport@theoreminc.net');
$mail->Subject = 'Rhythm | Employee Status';
$mail->Body = $message_update;			
$mail->AddAddress("sathish.kumar@theoreminc.net");
$result=$mail->Send();
echo "<pre>";
print_r($result);
/*for($i=0;$i<$inc_update;$i++)
{
	$query="update alteredstatus set mailstatus='yes' where num='".$arr_update[$i]['num']."'";
	$result=$mysqli->query($query);
}*/




?>