<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
require_once 'db_config.php';

class Resourcewise_billable_nonbillableCount extends MysqlConnect
{
    public function cron()
    {
        $month = date('m');//(date('n') - 1) % 12;
        $year  = date('Y');
        
        $delquery = 'DELETE FROM resource_billable_nonbillable_cron WHERE Month = ' . $month . ' AND Year = ' . $year;
        mysql_query($delquery);
        
        $query     = "SELECT employ.EmployID,employ.Primary_Lead_ID AS ProcessLead,
					(SELECT Primary_Lead_ID FROM employ WHERE employID = ProcessLead) AS AssociateManager,
					(SELECT Primary_Lead_ID FROM employ WHERE employID = AssociateManager) AS VerticalManager,
					(SELECT Primary_Lead_ID FROM employ WHERE employID = VerticalManager) AS Manager,
					temp.Billable,
					temp.ProcessID 
					FROM employ 
					JOIN `designation` ON (designation.`DesignationID` = employ.DesignationID AND designation.`grades` < 6)
					JOIN (				
					(SELECT EmployID,Billable,p.ProcessID FROM employprocess e
					JOIN PROCESS p ON e.ProcessID = p.processID AND ProcessStatus = 0 AND e.Grades <= 6
					GROUP BY EmployID ORDER BY Billable DESC)
					) temp ON (temp.EmployID = employ.EmployID)									
					WHERE employ.`RelieveFlag`=0
				";
        $query_exe = mysql_query($query);
        $values    = '';
        
        if (mysql_num_rows($query_exe) > 0) {
            while ($row = mysql_fetch_assoc($query_exe)) {
                $values .= '(' . $row['EmployID'] . ',' . ($row['ProcessLead'] == '' ? 'NULL' : $row['ProcessLead']) . ',' . ($row['AssociateManager'] == '' ? 'NULL' : $row['AssociateManager']) . ',' . ($row['VerticalManager'] == '' ? 'NULL' : $row['VerticalManager']) . ',' . ($row['Manager'] == '' ? 'NULL' : $row['Manager']) . ',' . $row['Billable'] . ',' . $row['ProcessID'] . ',' . $month . ',' . $year . '),';
            }
            ;
            $values = rtrim($values, ',');
            //echo $values;
            //echo 'INSERT INTO resource_billable_nonbillable_cron (EmployID, ProcessLead, AssociateManager, VerticalManager, Manager, Billable, Month, Year) VALUES '.$values;
            mysql_query('INSERT INTO resource_billable_nonbillable_cron (EmployID, ProcessLead, AssociateManager, VerticalManager, Manager, Billable, ProcessID, Month, Year) VALUES ' . $values);
            //echo "Inserted";
        }
    }
}

$obj = new Resourcewise_billable_nonbillableCount();
$obj->cron();