<?php
ini_set('max_execution_time', 9000); //9000 seconds = 30 minutes
ini_set('memory_limit', '-1');
//ini_set('xdebug.max_nesting_level', 200); 
require_once 'db_config.php';
class EmployeeCountCron extends MysqlConnect{

	public $empArray = array();
	public $count = false;
	public $inserted = 0;
	public $updated = 0;
	public $HR_POOL = 223;
	public $Onboarding = 221;

	
	/*
	* @description : getting all the employee above grade 2
	* @return : setting into employee 
	*/
	private function _setEmpArray()
	{
		$sql = "SELECT employees.employee_id,employees.first_name,employees.last_name FROM employees JOIN designation ON employees.designation_id = designation.designation_id WHERE designation.grades > 2 AND employees.status != 6 ORDER BY employees.employee_id ";
		$result =  mysql_query($sql) or die(mysql_error()."getAllEmployee");
		$totalRecord = mysql_num_rows($result);
		if($totalRecord > 0)
		{
			while($row = mysql_fetch_array($result))
				$this->empArray[] = array('employee_id'=>$row['employee_id'],'first_name'=>$row['first_name'],'last_name'=>$row['last_name']);
			$this->empArray;
		} 
		return false;
	}
	
	/*
	 * @description : check if employ id exist in number_of_employee table
	 * @return : 'true' or 'false'
	 */
	private function _isEmpExist($empid)
	{
		$sql= "SELECT employee_id FROM number_of_employee WHERE employee_id = ".$empid;
		$result = mysql_query($sql);
		$status = (mysql_num_rows($result)==0) ? false : true;
		return $status;
	}
	
	/*
	 * @description : 
	 * First : Set the employee Array
	 * Second : get the Total Count for employee
	 * Third : if empid exist updating the query
	 * Fourth : if empid not exist inserting 
	 * 
	 */
	public function cron()
	{	
		$this->_setEmpArray();//First
		if(is_array($this->empArray))
		{
			foreach($this->empArray as $key=>$emp)
			{
				$this->count = false;
				$totalCount  = $this->getTotalCount($emp['employee_id']);//Second
				
				if($this->_isEmpExist($emp['employee_id']))//Third
				{
				    $sql = "UPDATE number_of_employee SET total_count = ".$totalCount." WHERE employee_id = ".$emp['employee_id'];
				    //echo $sql."<br>";
				    $result = mysql_query($sql);
				    $this->updated++;
				}
				else //Fourth
				{
				   $sql = "INSERT INTO number_of_employee (employee_id,total_count) VALUES (".$emp['employee_id'].",".$totalCount.")";
				   //echo $sql."<br>";
				   $result = mysql_query($sql);
				   $this->inserted++;
				}
			}
		}	
	}
	
	/*
	* @description :  getting total count 
	*/
	public function getTotalCount($empid) 
	{
		$sql= "SELECT employee_id FROM employees WHERE primary_lead_id = ".$empid." AND status IN(3,5,7)";
		// $sql .= " AND employee_id NOT IN (SELECT EmployID FROM employprocess WHERE Grades <3 AND  ProcessID IN(".$this->HR_POOL.", ".$this->Onboarding."))  AND (TransitionStatus !=3)";
		$result = mysql_query($sql);
		$this->count += mysql_num_rows($result);
		if(mysql_num_rows($result)!=0)
		{
			while($row = mysql_fetch_array($result))
			{
				$this->getTotalCount($row['employee_id']);
			}
		}
		return $this->count;
	}
}

$obj  = new EmployeeCountCron();
$obj->cron();

echo "<br>Total Record of Employee = ".count($obj->empArray);
echo "<br>Total Record Inserted = ".$obj->inserted;
echo "<br>Total Record Update = ".$obj->updated;

