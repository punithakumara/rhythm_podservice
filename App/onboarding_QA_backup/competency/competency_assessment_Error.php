
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Competency Assessment</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
    <script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
    <script type="text/javascript" src="../js/script.js"></script>
	<script>
		function goBack() {
		    window.history.back();
		}
		// var id=<?php echo $_GET['data']; ?>;
		// if(id==0) {
			// document.getElementById("error_desc").innerHTML = "Please select any option before submitting";
		// }
	</script>
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
	<div class="logout"><a href="logout.php?action=logout">Logout</a></div>
</div>
<div id="container">
<!--<h1>Survey Form</h1>-->
	<table class="formTable">
		<tr>
			<td colspan="2">
				<h1>Competency Assessment Error</h1>
			</td>
		</tr>
		<tr>
			<td>
				<table class="formTable" style="width:600px !important;">
					<tr>
						<td>
							<label class="desc">Error while inserting/updating Competency Assessment data.</label>
							<!--<label class="desc">All fields are mandatory.</label>-->
						</td>
					</tr>
					<tr>
						<td>
							<button class="btTxt" onclick="goBack()">Go Back</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>