<?php
require ('../config/mysql_crud.php');
require ("../config/excelwriter.class.php");

$db = new Database();

$excel=new ExcelWriter("Employee Details.xls");
if($excel==false)	
echo $excel->error;

$myArr=array("First Name","Last Name","Qualification","Grade","Designation","Father Name","Mother Name","Date Of Birth","Age","Date Of Join","Gender","Blood Group","Marital Status","Permanent Address","City","State","Zip","Present Address","City","State","Zip","Email","Mobile","Emergency Contact","Name Of Contact","Relationship With Contact","PAN Number","Passport Number","Relevant Experience");
$excel->writeLine($myArr);

$sql="SELECT t1.FirstName, t1.LastName, t1.DateOfBirth, t1.Age, t1.DateOfJoin, t1.Gender, t1.Address1, t1.City, t1.State, t1.Zip, t1.Address2, t1.City1, t1.State1, t1.Zip1, t2.Name AS QualificationsName, t3.Name AS DesignationName, t1.AlternateEmail, t1.Mobile, t1.EmergencyContact, t1.NameOfContact, t1.RelationshipWithContact, t1.PANNumber, t1.PassportNumber, t1.RelevantExperience, t1.BloodGroup, t1.MaritalStatus, t1.FatherName, t1.MotherName, t1.Grade FROM new_joinees AS t1
JOIN qualifications AS t2 ON (t1.QualificationID = t2.QualificationID)
JOIN designation AS t3 ON (t1.DesignationID = t3.DesignationID)";
$result = $db->sql($sql);
$result_array = $db->getResult();
if(is_array($result_array) && count($result_array)!=0)
{
	if(!array_key_exists(0, $result_array))
	{
		$tempResult = array();
		foreach($result as $k=>$v)
			$tempResult[0][$k] = ($v);
		$result_array = $tempResult;	
	}
	foreach($result_array as $key => $res)
	{
		$myArr=array($res['FirstName'],$res['LastName'],$res['QualificationsName'],$res['Grade'],$res['DesignationName'],$res['FatherName'],$res['MotherName'],$res['DateOfBirth']
		,$res['Age'],$res['DateOfJoin'],$res['Gender'],$res['BloodGroup'],$res['MaritalStatus'],$res['Address1'],$res['City']
		,$res['State'],$res['Zip'],$res['Address2'],$res['City1'],$res['State1']
		,$res['Zip1'],$res['AlternateEmail'],$res['Mobile'],$res['EmergencyContact'],$res['NameOfContact'],$res['RelationshipWithContact'],$res['PANNumber'],$res['PassportNumber'],$res['RelevantExperience']);
		$excel->writeLine($myArr);
	}
}
?>