<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Theorem</title>
	<link href="../css/style.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/jquery.validate.css" />
    <script src="../js/jquery-1.11.1.js" type="text/javascript"/></script>
    <script src="../js/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/tcal.css" />
	<script type="text/javascript" src="../js/tcal.js"></script> 
	
</head>
<body>
<div class="TopHeader">
	<div class="LogoOtr1"><img src="../images/theorem_logo.jpg" height="29" width="118"></div>
</div>
<div id="container">
<!--<h1>Medi-Assist Form</h1>-->
<form class="theorem" action="mediassist_success.php" method="POST">
<table class="formTable">
	<tr>
		<td colspan="2">
			<h1>Employee Details</h1>
		</td>
	</tr>
<tr>
	<td>
		<table class="formTable" style="width:400px !important;">
			<tr>
				<td>
					<label class="desc">Employee Name:</label>
					<input id="EmployName" type="text" tabindex="1" name="EmployName" value=""/>
				</td>
			</tr>
			<tr>
				<td>
					<label class="desc" for="EmployID">Employee ID:</label>
					<input type="text" name="EmployID" tabindex="2" id="EmployID" onblur="EmployeeDetails(this.value);">
				</td>
			</tr>
			<tr>
				<td>
					<label class="desc">Date of Join:(DD/MM/YYYY)</label>
					<input type="text" autocomplete="off" readonly class="tcal" name="DateOfJoin" tabindex="4" id="DateOfJoin">
				</td>
			</tr>
			<tr>
				<td>	
					<label class="desc">Date Of Birth:(DD/MM/YYYY)</label>
					<input type="text" autocomplete="off" readonly class="tcal" name="DateOfBirth" tabindex="5" id="DateOfBirth">
				</td>
			</tr>
			<tr>
				<td>
					<label class="desc">Marital Status:</label>
					<select id="MaritalStatus" tabindex="6" name="MaritalStatus">
						<option value="">-- Select --</option>
						<option>Single</option>
						<option>Married</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label class="desc">Blood Group:</label>
					<select id="BloodGroup" tabindex="7" name="BloodGroup">
						<option value="">-- Select --</option>
						<option>O +ve</option>
						<option>O -ve</option>
						<option>A +ve</option>
						<option>A -ve</option>
						<option>B +ve</option>
						<option>B -ve</option>
						<option>AB +ve</option>
						<option>AB -ve</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<label class="desc">Location:</label>
					<select id="Location" tabindex="8" name="Location">
						<option value="">-- Select --</option>
						<option>Bangalore</option>
						<option>Mysore</option>
					</select>
				</td>
			</tr>
		</table>
	</td>
	<td>
		<table class="formTable" style="width:400px !important;">
			
			<tr>
				<td colspan="3">
					<label class="desc">Qualification:</label>
					<select id="Qualification" tabindex="9" name="Qualification" onchange="otherQualification(this.value);">
						<option value="">-- Select --</option>
						<option>Bachelor of Architecture (B.Arch.)</option>
						<option>Bachelor of Arts (B.A.)</option>
						<option>Bachelor of Business Administration (B.B.A.)</option>
						<option>Bachelor of Commerce (B.Com.)</option>
						<option>Bachelor of Computer Applications (B.C.A.)</option>
						<option>Bachelor of Computer Science (B.Sc. (Computer Science))</option>
						<option>Bachelor of Design (B.Des. - B.D.)</option>
						<option>Bachelor of Education (B.Ed.)</option>
						<option>Bachelor of Engineering - Bachelor of Technology (B.E./B.Tech.)</option>
						<option>Bachelor of Fine Arts (BFA - BVA)</option>
						<option>Bachelor of Home Science (B.A. - B.Sc. (Home Science))</option>
						<option>Bachelor of Physical Education (B.P.Ed.)</option>
						<option>Bachelor of Science (B.Sc.)</option>
						<option>BBM- Bachelor of Business Management</option>
						<option>Diploma</option>
						<option>Graduation</option>
						<option>Master in Home Science (M.A. - M.Sc. (Home Science))</option>
						<option>Master of Architecture (M.Arch.)</option>
						<option>Master of Arts (M.A.)</option>
						<option>Master of Business Administration (M.B.A.)</option>
						<option>Master of Commerce (M.Com.)</option>
						<option>Master of Computer Applications (M.C.A.)</option>
						<option>Master of Design (M.Des. -  M.Design.)</option>
						<option>Master of Education (M.Ed.)</option>
						<option>Master of Engineering - Master of Technology (M.E./M.Tech.)</option>
						<option>Master of Fine Arts (MFA - MVA)</option>
						<option>Master of Mass Communications (M.M.C - M.M.M.)</option>
						<option>Master of Physical Education (M.P.Ed. -  M.P.E.)</option>
						<option>Master of Science (M.Sc.)</option>
						<option>Pre University</option>
						<option value="Other">Other</option>
					</select>
				</td>
			</tr>
			<tr id="OtherQualificationTr" style="display:none">
				<td>
					<label class="desc" for="OtherQualification">Other Qualification:</label>
					<input type="text" name="OtherQualification" tabindex="10" id="OtherQualification">
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<label class="desc">Emp Contact Number:</label>
					<input id="EmpContactNumber" type="text" tabindex="10" name="EmpContactNumber" value=""/>
				</td>
			</tr>
			
			<tr>
				<td>
					<fieldset>
						<legend>Emergency Contact 1</legend>
							
						<table>
							<tr>
								<td>
									<label class="desc">Phone No:</label>
									<input id="EmergencyContactNumber1" type="text" tabindex="11" name="EmergencyContactNumber1" value=""/>
								</td>
								<td>
									<label class="desc">Name:</label>
									<input id="EmergencyContactName1" type="text" tabindex="11" name="EmergencyContactName1" value=""/>
								</td>
								<td>
									<label class="desc">Relationship:</label>
									<select id="EmergencyContactRelationship1" tabindex="11" name="EmergencyContactRelationship1">
										<option value="">-- Select --</option>
										<option>Father</option>
										<option>Mother</option>
										<option>Spouse</option>
										<option>Brother</option>
										<option>Sister</option>
										<option>Guardian</option>
										<option>Other</option>
									</select>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset>
						<legend>Emergency Contact 2</legend>
							
						<table>
							<tr>
								<td>
									<label class="desc">Phone No:</label>
									<input id="EmergencyContactNumber2" type="text" tabindex="11" name="EmergencyContactNumber2" value=""/>
								</td>
								<td>
									<label class="desc">Name:</label>
									<input id="EmergencyContactName2" type="text" tabindex="11" name="EmergencyContactName2" value=""/>
								</td>
								<td>
									<label class="desc">Relationship:</label>
									<select id="EmergencyContactRelationship2" tabindex="11" name="EmergencyContactRelationship2">
										<option value="">-- Select --</option>
										<option>Father</option>
										<option>Mother</option>
										<option>Spouse</option>
										<option>Brother</option>
										<option>Sister</option>
										<option>Guardian</option>
										<option>Other</option>
									</select>
								</td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<label class="desc">PAN Details:</label>
					<input id="PANDetails" type="text" tabindex="12" name="PANDetails" value=""/>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<label class="desc">Passport Number:</label>
					<input id="PassportNumber" type="text" tabindex="13" name="PassportNumber" value=""/>
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<table class="formTable">
<tr><td colspan='4'><h1>Family Details</h1></td></tr>
<tr><td><label class="desc">Family Member</label></td><td><label class="desc">Family Member Name</label></td><td><label class="desc">Gender</label></td><td><label class="desc">DOB</label></td></tr>
<tr><td><input type="text" name="RelationShip[]" readonly value="Father"/></td><td><input type="text" name="FamilyName[]" tabindex="14" value=""/></td><td><input type="text" name="Gender[]" tabindex="15" value="Male"/></td><td><input type="text" autocomplete="off" readonly class="tcal" name="MemberDateOfBirth[]" tabindex="16" id="MemberDateOfBirth"></td></tr>
<tr><td><input type="text" name="RelationShip[]" readonly value="Mother"/></td><td><input type="text" name="FamilyName[]" tabindex="17" value=""/></td><td><input type="text" name="Gender[]" tabindex="18" value="Female"/></td><td><input type="text" autocomplete="off" readonly class="tcal" name="MemberDateOfBirth[]" tabindex="19" id="MemberDateOfBirth"></td></tr>
<tr><td><input type="text" name="RelationShip[]" readonly value="Spouse"/></td><td><input type="text" name="FamilyName[]" tabindex="20" value=""/></td><td><select tabindex="21" name="Gender[]"><option value="0">-- Select --</option><option>Male</option><option>Female</option></select></td><td><input type="text" autocomplete="off" readonly class="tcal" name="MemberDateOfBirth[]" tabindex="22" id="MemberDateOfBirth"></td></tr>
<tr><td><input type="text" name="RelationShip[]" readonly value="1st Kid"/></td><td><input type="text" name="FamilyName[]" tabindex="23" value=""/></td><td><select tabindex="24" name="Gender[]"><option value="0">-- Select --</option><option>Male</option><option>Female</option></select></td><td><input type="text" autocomplete="off" readonly class="tcal" name="MemberDateOfBirth[]" tabindex="25" id="MemberDateOfBirth"></td></tr>
<tr><td><input type="text" name="RelationShip[]" readonly value="2nd Kid"/></td><td><input type="text" name="FamilyName[]" tabindex="26" value=""/></td><td><select tabindex="27" name="Gender[]"><option value="0">-- Select --</option><option>Male</option><option>Female</option></select></td><td><input type="text" autocomplete="off" readonly class="tcal" name="MemberDateOfBirth[]" tabindex="28" id="MemberDateOfBirth"></td></tr>
<tr>
	<td><input id="saveForm" class="btTxt" type="submit" tabindex="44" value="Submit" /></td>
</tr>
</table>
</form>

</div>
<div class="Footer">&copy; 2013-14 Theorem</div>
</body>
</html>